"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReplitDatabase = void 0;
const tslib_1 = require("tslib");
const database_1 = tslib_1.__importDefault(require("@replit/database"));
class ReplitDatabase {
    constructor() {
        Object.defineProperty(this, "db", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.db = new database_1.default();
    }
    async setItem(key, value) {
        await this.db.set(key, value);
    }
    async getAllItemKeys() {
        return await this.db.list();
    }
    async getItem(key) {
        return (await this.db.get(key));
    }
    async deleteItem(key) {
        await this.db.delete(key);
    }
}
exports.ReplitDatabase = ReplitDatabase;
//# sourceMappingURL=replit_storage.js.map