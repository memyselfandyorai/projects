package General;


import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Canvas;

import Lights.AreaLight;
import Lights.Light;
import Lights.PointLight;
import Main.Utilities;
import Objects.Surface;

/**
 * The class represents a scene. It consists of camera, objects, lights, background..
 *
 */
public class Scene implements Iterable<Light>{

	public static final boolean supportOnlyPNG=false;
	
	
	private Set<Surface> primitives;
	private Set<Light> lights;
	
	private Camera camera;
	
	private Canvas canvas;
	
	public static String chosenPath;
	
	private int superSampleWidth;
	
	private boolean accelerate;
	
	private Color backColor;
	private String backTex;
	/*	default = null.
	This can be either a full path or just a filename, in which case you need to take the path from the filename of the text file loaded.
	 */
	
	private ImageData textureImg;
	
	private Color ambientLight; // I AL
	
		
	private BSPTree intersectionTree;
	
	public Scene()
	{
		primitives=new HashSet<Surface>();
		lights=new HashSet<Light>();
		backTex=null;
		backColor=new Color(0,0,0);
		ambientLight=new Color(0,0,0);
		accelerate=false;
		superSampleWidth=1;
	}
	
	
	public void setBackColor(Color backColor) {
		this.backColor = backColor;
	}

	public void setBackTex(String backTex) {
		this.backTex = backTex;
	}


	public Camera getCamera() {
		return camera;
	}


	public void setCamera(Camera camera) {
		this.camera = camera;
	}
	
	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}


	public void setAccelerate(boolean accelerate) {
		this.accelerate = accelerate;
	}

	public void setSuperSampleWidth(int superSampleWidth) {
		this.superSampleWidth = superSampleWidth;
	}

	public void setAmbientLight(Color ambientLight) {
		this.ambientLight = ambientLight;
	}


	public void add(Surface p)
	{
		primitives.add(p);
	}
	
	public void add(Light l)
	{
		lights.add(l);
	}
	
	public void add(AreaLight area)
	{
		for(PointLight p:area)
			lights.add(p);
	}
	
	
	public Color getAmbientLight() {
		return ambientLight;
	}

	/**
	 * sets the chosen path given file-path
	 * */
	public static void setPath(String p)
	{
		chosenPath=p.substring(0, p.lastIndexOf("\\")+1);
	}
	
	
	/**
	 * returns true if texture file is undefined, or exists.
	 * 
	 * */
	public boolean checkTexFile()
	{
		if(backTex==null)
			return true;
		File f=new File(backTex);
		if(!f.exists())
		{
			backTex=chosenPath+backTex;
			f=new File(backTex);
										
			if(!f.exists())
				return false;
		}
		
		if(!backTex.toLowerCase().endsWith(".png")&&supportOnlyPNG)
			return false;
		
		//type is texture, and it's ok
		textureImg = new ImageData(backTex);
		
		textureImg=textureImg.scaledTo(canvas.getBounds().width, canvas.getBounds().height);
		
		return true;
	}
	
	private Color getBackColor(int x,int y)
	{
		if(backTex==null)
			return getBackColor();
		
		Color color=Utilities.getColorFromImage(textureImg, x, y);
		
		return color;
	}
	
	
	public Color getBackColor()
	{
		return backColor;		
	}
	
	
	
	/**
	 * returns intersection given a ray
	 * */
	public Intersection findIntersection(Ray ray)
	{
		if(!accelerate)
			return findIntersectionNaive(this,primitives,ray);
		
		return findIntersectionAccelerated(ray);
	}
	
	/**
	 * naive intersection: slow . goes through every object
	 * */
	public static Intersection findIntersectionNaive(Scene scene,Set<Surface> primitives,Ray ray)
	{
		double min_d=Double.POSITIVE_INFINITY;
		Intersection min_i=null;
		
		if(primitives.isEmpty())
			return null;
		
				
		for(Surface object:primitives)
		{
			
			Intersection inter=object.intersect(scene,ray);
			
			if(inter!=Intersection.MISS)
			{
				double d=inter.getDistance();
				
				if(d<min_d)
				{
					min_d=d;
					min_i=inter;
				}
			}
		}
					
		return min_i;
	}
	
	

	/**
	 * smart intersection: fast . uses a special data structure
	 * 
	 * for now: calls naive.
	 * 
	 *  acceleration part not finished yet. if it'll be finished, the comment will become the method 
	 * */
	public Intersection findIntersectionAccelerated(Ray ray)
	{
		
		/*return intersectionTree.findIntersection(this, ray);*/
		
		return findIntersectionNaive(this, primitives, ray); //the acceleration isn't finished
	}
	
	
	public Color getColor(int x,int y)
	{
		
		//super sampling. turning x,y to double!
		double superW=1.0/superSampleWidth; //(0,0.5) ; ( 0, 1/3 , 2/3) ... 
		
		Color[][] colors=new Color[superSampleWidth][superSampleWidth];

		for(int i=0;i<superSampleWidth;i++)
			for(int j=0;j<superSampleWidth;j++)
			{
				double dx=x+j*superW;
				double dy=y+i*superW;
				Ray ray=camera.rayThroughPixel(dx,dy);
				Intersection hit=findIntersection(ray);
				if(hit!=Intersection.MISS)
					colors[i][j]=hit.getColor(this);
				else
					colors[i][j]=getBackColor(x,y);
			}
					
		return Color.average(colors,superSampleWidth);
	}
	
	
	/**
	 * returns true iff ray towards light (from object) is not blocked by any other object
	 * 
	 * (ray should be in the direction of N, otherwise it's an inner point)
	 *
	 * */
	public boolean hits(Ray ray,Light light,NormalVector N)
	{
		if(ray.getVector().dotProduct(N)<0)
			return false;
		
		double distance=light.getDistance(ray.getPoint());
				
		for(Surface object:primitives)
		{
			Intersection inter=object.intersect(this,ray);
				
			if(inter!=Intersection.MISS)
				if(inter.getDistance()<distance)
					return false;
		}
		
		return true;
	}
	
	
	public Iterator<Light> iterator()
	{
		return lights.iterator();
	}
	
	
	/**
	 * last settings of scene before rendering
	 * */
	public void commit()
	{
		intersectionTree=new BSPTree(primitives);
	}
	
	
}
