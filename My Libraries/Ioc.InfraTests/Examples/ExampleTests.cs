﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ioc.Infra.Ioc;
using Ioc.InfraTests.TestObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ioc.InfraTests.Examples
{
    [TestClass()]
    public class ExampleTests
    {
        [TestMethod()]
        public void Example_RunSlave_NoError()
        {
            new Main().RunProgram(new[] { "SLAVE" });
        }

        [TestMethod()]
        public void Example_RunMaster_NoError()
        {
            new Main().RunProgram(args:null);
        }
    }
}
