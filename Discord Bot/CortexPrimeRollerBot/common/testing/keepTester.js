const controller = require("../controller.js");
const { TestCase, createIocContainer } = require("./common.js");
const {
    prepareConstantSaveDices,
    getRequiredRollResults,
    prepareConstantRoll,
    CONSTANT_ROLL_DEFAULT,
    CONSTANT_ROLL_SIMPLE,
    CONSTANT_ROLL_ADVANCED,
    CONSTANT_ROLL_SAVE,
    CONSTANT_ROLL_REROLL_STEPUP,
    CONSTANT_POOL_ROLL,
    CONSTANT_POOL_ROLL_ADVANCED,
    CONSTANT_ROLL_CACHE_KEY,
    SAVE_NAME,
    SINGLE_SAVE_NAME,
    GUILD_ID,
    USER_ID,
} = require("./constantRolls.js");
const saverTester = require("./saverTester.js");

module.exports = {
    TestCases: createTestCases(),
};

function createTestCases() {
    return [
        new TestCase({
            name: "testControllerKeepCortexPrime_noPreviousRoll_noCachedMessage",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                cache_key: "empty",
                actualRollResults: [1, 2, 3, 4],
            },
            expectedResult: ["No last roll found"],
        }),

        new TestCase({
            name: "testControllerKeepCortexPrime_keep1_sameAsDefault",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 1,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D4 1D6 2D8 1D10",
                "D4 : 4",
                "D6 : **(1)**",
                "D8 : **(2)** 3",
                "D10 : 5",
                "Results ordered by best total:",
                "Score: 5 (5) with Increased-Effect: D10",
                "Score: 4 (4) with Increased-Effect: D12",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrime_keep2_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 2,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D4 1D6 2D8 1D10",
                "D4 : 4",
                "D6 : **(1)**",
                "D8 : **(2)** 3",
                "D10 : 5",
                "Results ordered by best total:",
                "Score: 9 (4+5) with Increased-Effect: D10",
                "Score: 7 (4+3) with Increased-Effect: D12",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrime_keep0_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 0,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D4 1D6 2D8 1D10",
                "D4 : 4",
                "D6 : **(1)**",
                "D8 : **(2)** 3",
                "D10 : 5",
                "Results ordered by best total:",
                "Score: 0 () with Increased-Effect: D12",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrime_keep3_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 3,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D4 1D6 2D8 1D10",
                "D4 : 4",
                "D6 : **(1)**",
                "D8 : **(2)** 3",
                "D10 : 5",
                "Results ordered by best total:",
                "Score: 12 (4+3+5) with Increased-Effect: D6",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrime_keep4_sameAs3",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 4,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D4 1D6 2D8 1D10",
                "D4 : 4",
                "D6 : **(1)**",
                "D8 : **(2)** 3",
                "D10 : 5",
                "Results ordered by best total:",
                "Score: 12 (4+3+5) with Increased-Effect: D6",
            ],
        }),

        new TestCase({
            name: "testControllerKeepCortexPrime_afterSaverKeep2_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 2,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_ROLL_SAVE,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D4 2D6 4D8 1D10",
                "D4 : 4",
                "D6 : **(1)** 3",
                "D8 : **(2)** 3 3 3",
                "D10 : 5",
                "Results ordered by best total:",
                "Score: 9 (4+5) with Increased-Effect: D10",
                "Score: 7 (4+3) with Increased-Effect: D12",
            ],
        }),

        new TestCase({
            name: "testControllerKeepCortexPrime_extraEffectDieKeep0_allEffects",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 0,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_ROLL_ADVANCED,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 2D6 2D8 1D12",
                "D6 : **(1)** 2",
                "D8 : **(1)** 8",
                "D12 : 3",
                "Results ordered by best total:",
                "Score: 0 () with Effects: D12,D8,D6",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrime_extraEffectDieKeep1_someEffects",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 1,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_ROLL_ADVANCED,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 2D6 2D8 1D12",
                "D6 : **(1)** 2",
                "D8 : **(1)** 8",
                "D12 : 3",
                "Results ordered by best total:",
                "Score: 8 (8) with Effects: D12,D6,D4",
                "Score: 2 (2) with Effects: D12,D8,D4",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrime_extraEffectDieKeep3_defaultEffect",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 3,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_ROLL_ADVANCED,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 2D6 2D8 1D12",
                "D6 : **(1)** 2",
                "D8 : **(1)** 8",
                "D12 : 3",
                "Results ordered by best total:",
                "Score: 13 (2+8+3) with Effects: D4,D4,D4",
            ],
        }),

        new TestCase({
            name: "testControllerKeepCortexPrime_afterRerollStepUp_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 3,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_ROLL_REROLL_STEPUP,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 2D12",
                "Rolling (dice pool unusable for hitch or effect): 1D4 1D6",
                "D4 : [4]",
                "D6 : [1]",
                "D12 : **(1)** 6",
                "Results ordered by best total:",
                "Score: 11 (4+1+6) with Effect: D4",
            ],
        }),

        new TestCase({
            name: "testControllerKeepCortexPrime_afterPoolRoll_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 2,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_POOL_ROLL,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D6 1D8 2D10 1D12",
                "D6 : 3",
                "D8 : 2",
                "D10 : **(1)** **(1)**",
                "D12 : 6",
                "Results ordered by best total:",
                "Score: 9 (3+6) with Effect: D8",
                "Score: 5 (3+2) with Effect: D12",
            ],
        }),

        new TestCase({
            name: "testControllerKeepCortexPrime_afterAdvancedPoolRollKeep1_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 1,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_POOL_ROLL_ADVANCED,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D6 3D8 2D10",
                "D6 : 2",
                "D8 : 3 4 6",
                "D10 : 5 5",
                "Results ordered by best total:",
                "Score: 6 (6) with Effects: D10,D10,D8",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrime_afterAdvancedPoolRollKeep3_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 3,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_POOL_ROLL_ADVANCED,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D6 3D8 2D10",
                "D6 : 2",
                "D8 : 3 4 6",
                "D10 : 5 5",
                "Results ordered by best total:",
                "Score: 16 (6+5+5) with Effects: D8,D8,D6",
                "Score: 15 (4+6+5) with Effects: D10,D8,D6",
                "Score: 13 (3+4+6) with Effects: D10,D10,D6",
                "Score: 12 (2+4+6) with Effects: D10,D10,D8",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrime_afterAdvancedPoolRollKeep4_goodMessages",
            functionToRun: testControllerKeepCortexPrime,
            inputArgs: {
                keep: 4,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_POOL_ROLL_ADVANCED,
                actualRollResults: [],
            },
            expectedResult: [
                "Rolling: 1D6 3D8 2D10",
                "D6 : 2",
                "D8 : 3 4 6",
                "D10 : 5 5",
                "Results ordered by best total:",
                "Score: 20 (4+6+5+5) with Effects: D8,D6,D4",
                "Score: 18 (3+4+6+5) with Effects: D10,D6,D4",
                "Score: 17 (2+4+6+5) with Effects: D10,D8,D4",
                "Score: 15 (2+3+4+6) with Effects: D10,D10,D4",
            ],
        }),
        new TestCase({
            name: "testControllerKeepCortexPrimeThenAppend_keep1_thenAppend_keepResultNotStored",
            functionToRun: testControllerKeepCortexPrimeThenAppend,
            inputArgs: {
                keep: 1,
                cache_key: CONSTANT_ROLL_CACHE_KEY,
                prepare_mod: CONSTANT_ROLL_SIMPLE,
                actualRollResults: [5, 10],
                appendDice: "2D10",
            },
            expectedResult: [
                "Rolling: 1D4 2D6 1D8 1D12",
                "D4 : 4",
                "D6 : **(1)** 2",
                "D8 : 2",
                "D12 : 6",
                "Results ordered by best total:",
                "Score: 6 (6) with Effect: D8",
                "Score: 4 (4) with Effect: D12",
                "Rolling: 1D4 2D6 1D8 2D10 1D12",
                "D4 : 4",
                "D6 : **(1)** 2",
                "D8 : 2",
                "D10 : 5 10",
                "D12 : 6",
                "Results ordered by best total:",
                "Score: 16 (10+6) with Effect: D10",
                "Score: 15 (5+10) with Effect: D12",
            ],
        }),
    ];
}

/*
input: {
  keep, 
  cache_key,
  prepare_mod - one of CONSTANT_ROLL_....
  actualRollResults - list of random results,
}
*/
async function testControllerKeepCortexPrime(input) {
    let constantRolls = getRequiredRollResults(input.prepare_mod);
    let actualRollResults = [...constantRolls, ...input.actualRollResults];
    let iocContainer = createIocContainer(actualRollResults);

    await prepareConstantRoll(input.prepare_mod, iocContainer);

    let keepInput = {
        keep: input.keep,
        cache_key: input.cache_key,
    };
    let messageContentResult = await controller.RollCortexPrimeKeep(
        keepInput,
        iocContainer,
    );

    return messageContentResult;
}

/*
input: {
  keep, 
  cache_key,
  prepare_mod - one of CONSTANT_ROLL_....
  appendDice - dice format to append,
  actualRollResults - list of random results,
}
*/
async function testControllerKeepCortexPrimeThenAppend(input) {
    let constantRolls = getRequiredRollResults(input.prepare_mod);
    let actualRollResults = [...constantRolls, ...input.actualRollResults];
    let iocContainer = createIocContainer(actualRollResults);

    await prepareConstantRoll(input.prepare_mod, iocContainer);

    let keepInput = {
        keep: input.keep,
        cache_key: input.cache_key,
    };
    let keepResult = await controller.RollCortexPrimeKeep(
        keepInput,
        iocContainer,
    );

    let appendInput = {
        dice: input.appendDice,
        cache_key: input.cache_key,
    };
    let appendResult = await controller.RollCortexPrimeAppend(
        appendInput,
        iocContainer,
    );

    return keepResult.concat(appendResult);
}
