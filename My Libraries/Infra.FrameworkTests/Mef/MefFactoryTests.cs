﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Infra.FrameworkTests.TestObjects;
using System.Collections.Generic;
using System.Linq;
using Infra.Framework.Ioc;
using Moq;
using System.Reflection;

namespace Infra.Framework.Mef.Tests
{
    [TestClass()]
    public class MefFactoryTests
    {
        [TestMethod()]
        public void MefFactoryImplementation_SingletonGetByType_TypeIsKeyAndSameInstance()
        {
            ITypeFactory factory = new SimpleTypeFactory(Lifecycle.Singleton);

            var impl1 = factory.GetByType(MyType.TypeA);
            var impl2 = factory.GetByType(MyType.TypeA);

            Assert.AreEqual(MyType.TypeA, impl1.Type);
            Assert.AreEqual(impl2, impl1);
        }

        [TestMethod()]
        public void MefFactoryImplementation_TransientGetByType_TypeIsKeyAndDifferentInstance()
        {
            ITypeFactory factory = new SimpleTypeFactory(Lifecycle.Transient);

            var impl1 = factory.GetByType(MyType.TypeA);
            var impl2 = factory.GetByType(MyType.TypeA);

            Assert.AreEqual(MyType.TypeA, impl1.Type);
            Assert.AreEqual(MyType.TypeA, impl2.Type);
            Assert.AreNotEqual(impl2, impl1);
        }

        [TestMethod()]
        public void MefFactoryImplementation_SingletonGetByType_TwoExistsOneMissing()
        {
            ITypeFactory factory = new SimpleTypeFactory(Lifecycle.Singleton);

            var implA = factory.GetByType(MyType.TypeA);
            var implB = factory.GetByType(MyType.TypeB);
            var implC = factory.GetByType(MyType.TypeC); // Should be missing since no ExportForMef exists.

            Assert.AreEqual(MyType.TypeA, implA.Type);
            Assert.AreEqual(MyType.TypeB, implB.Type);
            Assert.IsNull(implC);
        }

        [TestMethod()]
        public void MefFactoryImplementation_SingletonGetAll_AllAreTheSame()
        {
            ITypeFactory factory = new SimpleTypeFactory(Lifecycle.Singleton);

            var implA = factory.GetByType(MyType.TypeA);
            var implB = factory.GetByType(MyType.TypeB);

            CollectionAssert.AreEquivalent(new List<IKeyedObject> { implA, implB }, factory.GetAll().ToList());
            Assert.AreEqual(-1, implA.Num);
            Assert.AreEqual(-1, implB.Num);
        }

        [TestMethod()]
        public void MefFactoryImplementation_AssembliesProvider_AllAreTheSame()
        {
            var mock = new Mock<IAssembliesProvider>();
            mock.Setup(p => p.GetAllProjectAssemblies()).Returns(new[] { Assembly.GetExecutingAssembly() });

            ITypeFactory factory = new SimpleTypeFactory(mock.Object);

            var impl1 = factory.GetByType(MyType.TypeA);
            var impl2 = factory.GetByType(MyType.TypeA);

            Assert.AreEqual(MyType.TypeA, impl1.Type);
            Assert.AreEqual(impl2, impl1);
        }

        [TestMethod()]
        public void MefFactoryImplementation_AssembliesProviderEmptyAssemblies_ReturnNone()
        {
            var mock = new Mock<IAssembliesProvider>();
            mock.Setup(p => p.GetAllProjectAssemblies()).Returns(new Assembly[] { });

            ITypeFactory factory = new SimpleTypeFactory(mock.Object);

            var impl = factory.GetByType(MyType.TypeA);

            Assert.IsNull(impl);
        }
    }

    [ExportForMef(typeof(IKeyedObject))]
    public class ImplA : IKeyedObject
    {
        public MyType Type => MyType.TypeA;

        public int Num { get; private set; } = -1;

        // For Mef
        public ImplA()
        {

        }

        // For Mef as singleton
        public ImplA(IIocContainer optionalContainer)
        {
            Num = optionalContainer.Resolve<ISimpleInterface>().ID;
        }

    }

    [ExportForMef(typeof(IKeyedObject))]
    public class ImplB : IKeyedObject
    {
        public MyType Type => MyType.TypeB;

        public int Num { get; private set; } = -1;

        // For Mef
        public ImplB()
        {

        }

        // For Mef as singleton
        public ImplB(IIocContainer optionalContainer)
        {
            Num = optionalContainer.Resolve<ISimpleInterface>().ID;
        }
    }
}
