
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;

public class Game extends JPanel implements ActionListener{
	
	
	private Pack pack,deck;
	
	private Vector players;
	
	//private JPanel player;
	
	//system.exit(0) : close
	
	//private JComboBox plCards;
	
	private JFrame frame;
	
	private int order;
	
	private JPanel pOrder;
	
	public Game(JFrame f)
	{
	super();	
	pack=new Pack(false);
	players=new Vector();
	frame=f;
	
	deck=new Pack(true);
	
	order=0;
	
	setLayout(new GridLayout(3,3));
	//setLayout(new BorderLayout());
	
	
	String[] o=new String[4];
	o[0]="1";
	o[1]="2";
	o[2]="3";
	o[3]="4";
	
	

	
	
	//String pp=(String)(JOptionPane.showOptionDialog(this,"How many players?","Choose:",0,JOptionPane.QUESTION_MESSAGE,null,o,o[0]));
	//JOptionPane.showInputDialog(this,"How many players?","Choose:",JOptionPane.QUESTION_MESSAGE,null,o,o[0]));
	
	//int p=Integer.parseInt(pp);
	int hp=JOptionPane.showOptionDialog(this,"How many human players?","Choose:",0,JOptionPane.QUESTION_MESSAGE,null,o,o[0]);
	hp++;
//	String pp=(String)(JOptionPane.showInputDialog(this,"How many players?","Choose:",JOptionPane.QUESTION_MESSAGE,null,o,o[0]));
	
	//Object[] b=pack.cards().toArray();
	//JOptionPane.showInputDialog(this,"","",0,null,b,b[0]);
	
	for(int i=0;i<hp;i++)
	{
	String pln="Player "+(i+1);
	String n=JOptionPane.showInputDialog(this,"Enter the name of a human player:",pln);
		if(n==null)
		n=pln;
		else
			if(n.equals(""))
			n=pln;
	
	players.add(new Player(n,true));		
	}
	
	
	
	int cp=4-hp;
	
	if(cp>0)
	{
		if(hp!=1)
		{
		o=new String[cp+1];
		for(int i=0;i<cp+1;i++)
		o[i]=""+i;
		}
		else
		{
		o=new String[cp];
		for(int i=0;i<cp;i++)
		o[i]=""+(i+1);
		}
		
	cp=JOptionPane.showOptionDialog(this,"How many computer players?","Choose:",0,JOptionPane.QUESTION_MESSAGE,null,o,o[0]);
	
	if(hp==1)
	cp++;
	}
	
	for(int i=0;i<cp;i++)
	{
	String n="Computer "+(i+1);
	players.add(new Player(n,false));		
	}
	//System.out.println(hp+","+cp); V
	
	//ImageIcon m=new ImageIcon("pics\\ac.gif");
	//add(new JButton(m));
	
	
	for(int i=0;i<players.size();i++)
	{
	Player p=(Player)players.elementAt(i);
	p.dealStart(pack);
	}
	//JLabel l=new JLabel(m);
	//setBackground(Color.red);
	//add(l);
	
	order=checkHand();
	
	
	
	pOrder=new JPanel();
	pOrder.setLayout(new GridLayout(2,1));
	
	JPanel por=new JPanel();
	por.setLayout(new GridLayout(0,1));
	
	JLabel lbb=new JLabel("The order of the game:");
	lbb.setForeground(Color.blue.darker().darker());
	
	por.add(lbb);
	int z=players.size();
	int i=order;
	
	while(true)
	{
	Player p=(Player)players.elementAt(i);
	
	JLabel lb=new JLabel(p.name());
	lb.setForeground(Color.blue);
	
	por.add(lb);
	
		i++;
		if(i>=z)
		{
		i=0;	
		}
	
	if(i==order)
	break;
	
	}
	
	pOrder.add(por);
	/*
	System.out.println(order+"");
	for(int i=0;i<players.size();i++)
	{
	Player p=(Player)players.elementAt(i);
	System.out.println(p.cards());
	}
	*/

	//Player cur=(Player)players.elementAt(order);
	
	//cur.drawCard(pack,10);	
	//cur.hit(deck,0);
	
	
	//AI();
		
	}
	
	
	public void start()
	{
	//pack.cards().clear();	
	AI();	
	}
	
	
	public void paintComponent(Graphics g)
	{
	super.paintComponent(g);	
	
	//ImageIcon m=new ImageIcon("pics\\ac.gif");
	//m.paintIcon(this,g,0,0);
	}
	
	
//drawImage(Image img, int x, int y, Color bgcolor, ImageObserver observer) 
  //        Draws as much of the specified image as is currently available.
	
	public Dimension getPreferredSize()
	{
	return new Dimension(300,300);	
	}
	
	
	public void next(boolean missed)
	{
	//if winning!
	Player p=(Player)players.elementAt(order);
	if(p.cards().size()==0&&p.noEx())
	{
		setVisible(false);
		JOptionPane.showMessageDialog(this,p.name()+" finished all of his cards.","Winning",JOptionPane.PLAIN_MESSAGE);
		setVisible(true);
		players.remove(p);
		if(order>=players.size())
		order=0;
		
		if(players.size()==1)
		{
		String lost=((Player)players.elementAt(order)).name();
		JOptionPane.showMessageDialog(this,"Game over. "+lost+" is the shit head.","Game over",JOptionPane.PLAIN_MESSAGE);
		System.exit(0);	
		}
			
	}
	else
	{
		order++;
		if(order>=players.size())
		order=0;
			if(missed)
			{
			Player mis=(Player)players.elementAt(order);
			setVisible(false); //works? or see his cards?
			JOptionPane.showMessageDialog(this,mis.name()+" has missed his turn.","Skip",JOptionPane.PLAIN_MESSAGE);
			setVisible(true);
			}
	}
	
	}
	
	public int checkHand()
	{
	int n=15;
	int s=-1;
	int play=0;
	
	for(int i=0;i<players.size();i++)
	{
	Player p=(Player)players.elementAt(i);
	Vector c=p.cards();
		for(int j=0;j<c.size();j++)
		{
		Card d=(Card)c.elementAt(j);
		int num=d.num();
		int sn=d.sn();
			if(num!=2&&num!=3&&num<n)
			{
			play=i;	
			n=num;	
			s=sn;
			}
			else
				if(num==n)
				if(sn>s)
				{
				play=i;	
				n=num;	
				s=sn;
				}
		}
	}	
	
	
	return play;	
	}
	
	
	public void update(Player player)
	{
	setVisible(false);	
		
		int p=players.size()-1;
		int ind=players.indexOf(player);
		int pp=p;
		
		removeAll();
		
		JOptionPane.showMessageDialog(this,player.name()+"'s turn.","Your turn",JOptionPane.PLAIN_MESSAGE);
		
		
		add(pOrder);
		
		JPanel lab[]=new JPanel[p];
		
		for(int i=0;i<players.size();i++)
		if(i!=ind)
		{
		p--;
		lab[p]=new JPanel();
		lab[p].setLayout(new GridLayout(2,1));
		JPanel up=new JPanel();
		up.setLayout(new GridLayout(0,1));
		Player r=(Player)players.elementAt(i);	
		up.setToolTipText(r.name()+" cards.");
		up.add(new JLabel("Player: "+r.name()));	
		//up.add(new JLabel("Cards:"));	
		up.add(new JLabel("Cards in hand: "+r.cards().size()));
		up.add(new JLabel("Cards hidden: "+r.getHid()));
		up.add(new JLabel("Cards seen:"));
		lab[p].add(up);
		JPanel down=new JPanel();
		down.setLayout(new GridLayout(1,0));
		r.addLabels(down);
		lab[p].add(down);
		//lab[p].setBackground(Color.blue);
		//System.out.println(""+p);
		//lab[p].setPreferredSize(new Dimension(400,400));
		}
		
		
		//deck
		
		JPanel dec=new JPanel();
		dec.setLayout(new GridLayout(2,1));
		
		JPanel tips=new JPanel();
		tips.setLayout(new GridLayout(0,1));
		tips.setToolTipText("The stack");
		Vector v=deck.cards();
		
		JLabel inst=new JLabel(deck.inst());
		inst.setForeground(Color.red);
		inst.setFont(new Font("",0,22));
		tips.add(new JLabel("Number of cards left in the deck: "+pack.cards().size()));
		tips.add(new JLabel("Number of cards in the stack: "+v.size()));
		tips.add(inst);
		
		String row="";
		if(v.size()>0)
		row=" ("+deck.row()+" in a row";
		
		String r3=deck.rowf();
		
		if(!r3.equals(""))
		row=row+r3;
		
		row=row+")";
		
		if(row.equals(")"))
		tips.add(new JLabel("Top card:"));
		else
		tips.add(new JLabel("Top card:"+row));
		
		dec.add(tips);
		
		JLabel top;
		if(v.size()>0)
		top=new JLabel(((Card)v.elementAt(0)).getIcon());
		else
		top=new JLabel("Empty");
		
		top.setToolTipText("The stack");
		
		dec.add(top);
				
		dec.setPreferredSize(new Dimension(70,70));
		
		//add(dec,BorderLayout.CENTER);
		///dec		
		
		
		
		if(pp==1)
		{
		add(lab[0]);
		add(new JPanel());
		add(new JPanel());
		add(dec);
		add(new JPanel());
		//add(new JPanel());
		}
		//add(lab[0],BorderLayout.NORTH);
		
		if(pp==2)
		{
		add(new JPanel());
		add(new JPanel());
		add(lab[0]);
		add(dec);
		add(lab[1]);
		//add(new JPanel());
		//add(lab[0],BorderLayout.EAST);
		//add(lab[1],BorderLayout.WEST);
		}
		
		if(pp==3)
		{
		add(lab[0]);
		add(new JPanel());
		add(lab[1]);
		add(dec);
		add(lab[2]);
		//add(new JPanel());
		//add(lab[0],BorderLayout.EAST);
		//add(lab[1],BorderLayout.WEST);
		//add(lab[2],BorderLayout.NORTH);
		}
		
		
		
		//player
		
		player.addButtons(this,true,this);
		
		JPanel pl=new JPanel();
		pl.setLayout(new GridLayout(0,1));
		pl.setToolTipText("Your cards");
		//pl.add(new JLabel("Player: "+player.name()));	
		
		Vector card=player.cards();
		
		/*
		JPanel car=new JPanel();
		car.setLayout(new GridLayout(0,5));
		
		for(int i=0;i<card.size();i++)
		{
		Card ca=(Card)card.elementAt(i);
		JButton b=new JButton(ca.getIcon());
		b.setToolTipText("Your cards");
		b.addActionListener(this);
		//b.setSize(200,200);
		car.add(b);			
		}*/
		
		JComboBox plCards=new JComboBox(card);
		plCards.setToolTipText("Choose a card");
		plCards.setMaximumRowCount(3);
		plCards.addActionListener(this);
		
		if(card.size()==0)
		plCards.setEnabled(false);
		else
		plCards.setRenderer(new ListCellRenderer(){
								
									public Component getListCellRendererComponent(
							         JList list,
							         Object value,
							         int index,
							         boolean isSelected,
							         boolean cellHasFocus)
							     {
							     	
							     	//make it list?
							     	//need different color!
							     	
							     	Card d=(Card)value;
							     	//JLabel c=new JLabel(d.getIcon());
							     	JButton c=new JButton(d.getIcon());
							     	
							     	//JLabel c=(JLabel)(getListCellRendererComponent(list,value,index,isSelected,cellHasFocus));
							     	//System.out.println("value: "+value);
							         //c.setText(value.toString());
							         if(isSelected)
							         c.setBackground(Color.blue);
							         //c.setBackground(isSelected ? Color.blue : Color.white);
							         
							         //setForeground(Color.blue);
							         
							         //setBackground(list.getSelectionBackground());
									 //setForeground(list.getSelectionForeground());

							         					         
							         //if(((Char)(value)).attacked())
							         //c.setForeground(isSelected ? Color.magenta : Color.red);
							         //if(value instanceof 							         
							         //c.setToolTipText();
							         return c;
							     }
								
										
									});
		
		
		//JScrollPane scr=new JScrollPane(car);
		pl.add(plCards);
		
		//JButton hit=new JButton("Choose");
		//if(player.cards().size()==0)
		//hit.setEnabled(false);
		
		//hit.addActionListener(this);
		
		JButton take=new JButton("Take stack");
		take.addActionListener(this);
		
		if(deck.cards().size()==0)
		take.setEnabled(false);
		
		JPanel but=new JPanel();
		but.setLayout(new GridLayout(1,2));
		//but.add(hit);
		but.add(take);		
		
		JPanel plb=new JPanel();
		plb.setLayout(new GridLayout(2,1));
		//pl.setPreferredSize(new Dimension(400,400));
		plb.add(but);
		plb.add(pl);
		
		//add(pl,BorderLayout.SOUTH);
		add(plb);
		
		///player
		
		player.addButtons(this,false,this);
		
		//add(new JPanel());
		
				
		//scrollpane for cards? (check more cards)
			
	setVisible(true);
	
	}
	
	
	public void actionPerformed(ActionEvent e)
	{
	
	
	
	//JButton b=(JButton)e.getSource();
	//String text=b.getText();
	Player player=(Player)players.elementAt(order);
	
	if(e.getSource() instanceof JComboBox)
	{
	JComboBox plCards=(JComboBox)e.getSource();
	//System.out.println(plCards.getSelectedItem().getClass());
	//JButton c=(JButton)plCards.getSelectedItem();
	int index=plCards.getSelectedIndex();
	//ImageIcon m=(ImageIcon)c.getIcon();
	//Card card=Card.getCard(m.getDescription());
	
	Card card=(Card)plCards.getSelectedItem();
	int n=deck.getNum();
	
	boolean ok=true;
	String mes="You can't put that card. (needs to be special or equals or ";
		if(n!=7)
		{
			if(card.num()<n&&card.num()!=2&&card.num()!=3&&card.num()!=10)
			{
				ok=false;
				mes=mes+"higher from "+Card.getNum(n)+")";
			}
		}
		else
		{
			if(card.num()>n&&card.num()!=2&&card.num()!=3&&card.num()!=10)
			{
			ok=false;
			mes=mes+"lower from "+Card.getNum(n)+")";
			}
			
		}
		
		
		
		if(ok)
		{
			int hm=player.howMany(card.num());	
			int num=1;			
			
			//System.out.println(hm);
			
			if(hm>1)
			{
			String[] o=new String[hm];
			for(int i=0;i<hm;i++)
			o[i]=""+(1+i);
				
			num=JOptionPane.showOptionDialog(this,"How many cards of this kind do you want to put in the stack?","Choose:",0,JOptionPane.QUESTION_MESSAGE,null,o,o[0]);
			num++;
			}
			
			//System.out.println(num);
			
			if(num==1)
			player.hit(deck,index);
			else
			player.hit(deck,card.num(),num);
			//several cards same time
						
			//burns
			order=deck.burn(this,player,order,players.size());
			
			
			//8, skips
			
			while(player.cards().size()<3&&pack.cards().size()>0)
			player.drawCard(pack);
			
						
			//update(player); //del
			
			if(card.num()==8&&deck.cards().size()>0)
			{
			for(int i=0;i<num;i++)
			next(true);
			
			//setVisible(false);
			//JOptionPane.showMessageDialog(this,num+" players missed their turn","Skips (card number 8)",JOptionPane.PLAIN_MESSAGE);
			//setVisible(true);
			}
						
			next(false);
			
			AI();				
			
		}
		else
		JOptionPane.showMessageDialog(this,mes,"Invalid card",JOptionPane.PLAIN_MESSAGE);
		
		
		
	}
	
	
	
	if(e.getSource() instanceof JButton)
	{
	JButton b=(JButton)e.getSource();
	String text=b.getText();
	
		if(text.equals("Take stack"))
		{
			while(deck.cards().size()>0)
			player.drawCard(deck);	


			//update(player);
								
			next(false);
			AI();						
		}
			
		
		if(text.equals(""))
		{
			boolean sen=!((ImageIcon)b.getIcon()).getDescription().equals("back");	
			int index=Integer.parseInt(b.getActionCommand());
			Card card=player.getCard(sen,index);
		int n=deck.getNum();
		boolean ok=true;
		String mes="You can't put that card. (needs to be special or equals or ";
		if(n!=7)
		{
			if(card.num()<n&&card.num()!=2&&card.num()!=3&&card.num()!=10)
			{
				ok=false;
				mes=mes+"higher from "+Card.getNum(n)+")";
			}
		}
		else
		{
			if(card.num()>n&&card.num()!=2&&card.num()!=3&&card.num()!=10)
			{
			ok=false;
			mes=mes+"lower from "+Card.getNum(n)+")";
			}
			
		}
		
		
		if(!sen) //try if work!
		{
			b.setIcon(card.getIcon());
		}
			
			if(ok)	
			{
				int hm=player.howManyS(card.num());	
				int num=1;			
				
				//System.out.println(hm);
				if(!sen)
				JOptionPane.showMessageDialog(this,"You chose a valid card.","Valid card",JOptionPane.PLAIN_MESSAGE);
				
				if(hm>1)
				{
				String[] o=new String[hm];
				for(int i=0;i<hm;i++)
				o[i]=""+(1+i);
					
				num=JOptionPane.showOptionDialog(this,"How many cards of this kind do you want to put in the stack?","Choose:",0,JOptionPane.QUESTION_MESSAGE,null,o,o[0]);
				num++;
				}
				
				//System.out.println(num);
				
				if(num==1)
				player.hitT(deck,index,sen);
				else
				player.hitT(deck,card.num(),num);
				//several cards same time
							
				//burns
				order=deck.burn(this,player,order,players.size());
				
							
				if(card.num()==8&&deck.cards().size()>0)
				{
				for(int i=0;i<num;i++)
				next(true);
				
				//setVisible(false);
				//JOptionPane.showMessageDialog(this,num+" players missed their turn","Skips (card number 8)",JOptionPane.PLAIN_MESSAGE);
				//setVisible(true);
				}
			
			next(false);
			AI();
			}
			else //invalid card
			{
				String ex="";
				boolean ai=false;
				if(!sen)
				{
				player.hitT(deck,index,false);
				ex="\n\nYou took the whole stack.";
					while(deck.cards().size()>0)
					player.drawCard(deck);
				next(false);
				ai=true;
				}
				JOptionPane.showMessageDialog(this,mes+ex,"Invalid card",JOptionPane.PLAIN_MESSAGE);
				
				if(ai)
				AI();
			}
		}
		
		
	}
		
	}
	
	
	public void AI()
	{
	Player player=(Player)players.elementAt(order);
	
	if(player.human())
	update(player);	//del
	else //AI
	{
	
	//setEnabled(false);
	//JOptionPane.showMessageDialog(this,player.name()+"'s turn.","Turn passed",JOptionPane.PLAIN_MESSAGE);
	//setEnabled(true);
	
		if(player.cards().size()!=0)
		{
			int ind=player.choose(deck.getNum());
			
			if(ind!=-1)
			{
			int num=1;
			int m=((Card)player.cards().elementAt(ind)).num();
			int hm=player.howMany(m);			
			
				if(hm>1)
				{
					if((m!=3&&m!=2&&m!=10)||player.hasOnlySp()) //not really right
					num=hm;					
				}
			
			if(num==1)
			player.hit(deck,ind);
			else
			player.hit(deck,m,num);
		
		
			
			while(player.cards().size()<3&&pack.cards().size()>0)
			player.drawCard(pack);
			
			showMiddle(player,num);
			
			order=deck.burn(this,player,order,players.size());
									
			if(m==8&&deck.cards().size()>0)
			{
			for(int i=0;i<num;i++)
			next(true);
			
			//setVisible(false); //works? or see his cards?
			//JOptionPane.showMessageDialog(this,num+" players missed their turn","Skips (card number 8)",JOptionPane.PLAIN_MESSAGE);
			//setVisible(true);
			}
			
			}
			else
			{
			while(deck.cards().size()>0)
			player.drawCard(deck);
			
			setVisible(false); //works? or see his cards?
			JOptionPane.showMessageDialog(this,player.name()+" took the whole stack","Invalid card",JOptionPane.PLAIN_MESSAGE);
			setVisible(true);	
			}
			
		}
		else
		{
				
			if(!player.noSe())
			{
				int ind=player.choose(deck.getNum(),true);
			
				if(ind!=-1)
				{
				int num=1;
				int m=player.getSeen(ind);
				int hm=player.howManyS(m);			
				
					if(hm>1)
					{
						if((m!=3&&m!=2&&m!=10)||player.hasOnlySp())
						num=hm;					
					}
				
				if(num==1)
				player.hitT(deck,ind,true);
				else
				player.hitT(deck,m,num);
			
				showMiddle(player,num);
				
				order=deck.burn(this,player,order,players.size());
										
				if(m==8&&deck.cards().size()>0)
				{
				for(int i=0;i<num;i++)
				next(true);
				
				//setVisible(false); //works? or see his cards?
				//JOptionPane.showMessageDialog(this,num+" players missed their turn","Skips (card number 8)",JOptionPane.PLAIN_MESSAGE);
				//setVisible(true);
				}
				
				}
				else
				{
				while(deck.cards().size()>0)
				player.drawCard(deck);
				
				setVisible(false); //works? or see his cards?
				JOptionPane.showMessageDialog(this,player.name()+" took the whole stack","Invalid card",JOptionPane.PLAIN_MESSAGE);
				setVisible(true);	
				}

			
			}
			else
			{
				int ind=player.choose(deck.getNum(),false);
				
				if(ind<0)
				{
				player.hitT(deck,-ind,false);
					while(deck.cards().size()>0)
					player.drawCard(deck);
				
				setVisible(false); //works? or see his cards?
				JOptionPane.showMessageDialog(this,player.name()+" took the whole stack","Invalid card",JOptionPane.PLAIN_MESSAGE);
				setVisible(true);	
				}
				else
				{
				int m=player.getHidden(ind);
				player.hitT(deck,ind,false);
				
				showMiddle(player,1);
				
				order=deck.burn(this,player,order,players.size());
				
										
				if(m==8&&deck.cards().size()>0)
				next(true);
				
				
				}
				
								
			}
		}
		
		next(false);
		AI();
	}
	}
	
	
	public void showMiddle(Player player,int num)
	{
		
	setVisible(false);	
		
		removeAll();
		
		setLayout(new GridLayout(3,3));
		
		for(int i=0;i<4;i++)
		add(new JPanel());
		
		
				
		//deck
		
				
		JPanel dec=new JPanel();
		dec.setLayout(new GridLayout(2,1));
		
		//dec.setBackground(Color.red);
		
		JPanel bot=new JPanel();
		
		Vector v=deck.cards();
		
		//String ofa="";
						
		JLabel ms=new JLabel("The card chosen:");
		ms.setForeground(Color.blue);
		ms.setFont(new Font("",0,20));
				
		bot.add(ms);
		
		if(num>1)
		bot.add(new JLabel("("+num+" of a kind)"));
		
		dec.add(bot);
		
		//4 of a kind?
		
		JPanel top=new JPanel();
		if(v.size()>0)
		{
		for(int i=0;i<num;i++)
		top.add(new JLabel(((Card)v.elementAt(i)).getIcon()));
		}
		else
		top.add(new JLabel("Empty"));
		
		dec.add(top);
						
		add(dec);
		
			
		for(int i=0;i<4;i++)
		add(new JPanel());
		
		setVisible(true);	
		JOptionPane.showMessageDialog(this,player.name()+" has made his turn.","Turn passed",JOptionPane.PLAIN_MESSAGE);
	
	}
		
	
	public static void main(String[] args) {
		// TODO: Add your code here
		
		
		
		
		
		JFrame f=new JFrame();
		
		
				
		
		//f.setSize(700,700);
				
		Game g=new Game(f);
		
			
		//System.out.println("ok");
		
		f.getContentPane().add(g);
		
		f.setVisible(true);
		
		f.setExtendedState(Frame.MAXIMIZED_BOTH);
		
		//Dimension screenSize=f.getToolkit().getDefaultToolkit().getScreenSize();

        //f.setBounds(0,0,screenSize.width,screenSize.height);
	
		
		//GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(f);
	
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		g.start();
		
	}	
}
