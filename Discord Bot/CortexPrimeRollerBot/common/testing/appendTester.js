const controller = require('../controller.js');
const {TestCase,createIocContainer  }= require('./common.js');
const {
  prepareConstantSaveDices, 
  getRequiredRollResults, prepareConstantRoll, 
  CONSTANT_ROLL_DEFAULT, CONSTANT_ROLL_ADVANCED ,CONSTANT_ROLL_SAVE ,  CONSTANT_ROLL_REROLL_STEPUP, CONSTANT_POOL_ROLL , CONSTANT_POOL_ROLL_ADVANCED,
  CONSTANT_ROLL_CACHE_KEY, 
  SAVE_NAME, SINGLE_SAVE_NAME ,
  GUILD_ID, USER_ID,
 } = require('./constantRolls.js');

module.exports = {
  TestCases: createTestCases(),
};

function createTestCases() {
  return [
    new TestCase({
      name: 'testControllerAppendCortexPrime_noPreviousRoll_noCachedMessage',
      functionToRun: testControllerAppendCortexPrime,
      inputArgs: {
        dice: '6 8',
        cache_key: 'empty',
        actualRollResults: [1, 2, 3, 4],
      },
      expectedResult: [
        'No last roll found',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerAppendCortexPrime_dice_appended',
      functionToRun: testControllerAppendCortexPrime,
      inputArgs: {
        dice: '6 8',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        actualRollResults :[3,7],
      },
      expectedResult: [        
        'Rolling: 1D4 2D6 3D8 1D10',
        'D4 : 4',
        'D6 : **(1)** 3',
        'D8 : **(2)** 3 7',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 7 (7) with Increased-Effect: D12',
      ],    
    }),        
    
    new TestCase({
      name: 'testControllerAppendCortexPrime_afterSaverWithoutDice_goodMessages',
      functionToRun: testControllerAppendCortexPrime,
      inputArgs: {
        dice: '',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_ROLL_SAVE,
        actualRollResults :[],
      },
      expectedResult: [        
        'Rolling: 1D4 2D6 4D8 1D10',
        'D4 : 4',
        'D6 : **(1)** 3',
        'D8 : **(2)** 3 3 3',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D10',
        'Score: 4 (4) with Increased-Effect: D12',
      ],    
    }),     
    
     new TestCase({
      name: 'testControllerAppendCortexPrime_advancedRollDie4_goodMessage',
      functionToRun: testControllerAppendCortexPrime,
      inputArgs: {
        dice: '4',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_ROLL_ADVANCED ,
        actualRollResults: [4],
      },
      expectedResult: [        
        'Rolling: 1D4 2D6 2D8 1D12',    
        'D4 : 4',
        'D6 : **(1)** 2',
        'D8 : **(1)** 8',
        'D12 : 3',
        'Results ordered by best total:',
        'Score: 12 (4+8) with Effects: D12,D6,D4',
        'Score: 6 (4+2) with Effects: D12,D8,D4',
      ],    
    }),     
    
    new TestCase({
      name: 'testControllerAppendCortexPrime_afterRerollStepUp_goodMessages',
      functionToRun: testControllerAppendCortexPrime,
      inputArgs: {
        dice: '2D8',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_ROLL_REROLL_STEPUP,
        actualRollResults :[1, 2],
      },
      expectedResult: [
        'Rolling: 2D8 2D12',
        'Rolling (dice pool unusable for hitch or effect): 1D4 1D6',
        'D4 : [4]',    
        'D6 : [1]',
        'D8 : **(1)** 2',
        'D12 : **(1)** 6',
        'Results ordered by best total:',
        'Score: 10 (4+6) with Effect: D8',
        'Score: 6 (4+2) with Effect: D12',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerAppendCortexPrime_afterPoolRoll_goodMessages',
      functionToRun: testControllerAppendCortexPrime,
      inputArgs: {
        dice: '4',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_POOL_ROLL ,
        actualRollResults :[3],
      },
      expectedResult: [
        'Rolling: 1D4 1D6 1D8 2D10 1D12',
        'D4 : 3',
        'D6 : 3',
        'D8 : 2',
        'D10 : **(1)** **(1)**',
        'D12 : 6',
        'Results ordered by best total:',
        'Score: 6 (6) with Effect: D8',
        'Score: 3 (3) with Effect: D12',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerAppendCortexPrime_afterAdvancedPoolRoll_goodMessages',
      functionToRun: testControllerAppendCortexPrime,
      inputArgs: {
        dice: '8',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_POOL_ROLL_ADVANCED,
        actualRollResults :[8],
      },
      expectedResult: [
        'Rolling: 1D6 4D8 2D10',
        'D6 : 2',
        'D8 : 3 4 6 8',
        'D10 : 5 5',
        'Results ordered by best total:',
        'Score: 14 (6+8) with Effects: D10,D10,D8',
      ],    
    }),    
    
  ]
}


/*
input: {
  dice - string, 
  cache_key,
  prepare_mod - one of CONSTANT_ROLL_....
  actualRollResults - list of random results,
}
*/
async function testControllerAppendCortexPrime(input) {  
  let constantRolls = getRequiredRollResults(input.prepare_mod)
  let actualRollResults = [...constantRolls, ...input.actualRollResults]
  let iocContainer = createIocContainer(actualRollResults)
  
  await prepareConstantRoll(input.prepare_mod, iocContainer)  
  
  let appendInput = {
    dice: input.dice,
    cache_key: input.cache_key,
  }
  let messageContentResult = await controller.RollCortexPrimeAppend(appendInput, iocContainer)
  
  return messageContentResult
}
