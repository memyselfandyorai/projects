
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

//,II,IS,IA. what needed.

//!!need to add things. like hiding. [need to check if the "invisible" things work... - diseases]

//change: color of weapons selection etc

//affected: spells. etc.

//labels- smaller. (when South, it will be fine?)


//tooltip. too much on a skill- mod ok?


public class Page extends JPanel implements ActionListener{ //character page

/**
 *The player
 */
private Character chara;

/**
 *Right panel
 */
private JPanel right;
/**
 *Left panel
 */
private JPanel left;
/**
 *Main menu
 */
private Main main;
/**
 *Selected indexes of items, skills and spells.
 */
private int II,IS,IA; //index- item,skills,abilities(spells)
/**
 *If player searched in this place already, and a parameter that helps in further functions
 */
private boolean inds,searched;
/**
 *Combo boxes for weapons, shields and armors
 */
private JComboBox wea,shi,arm;

//public Page(Character chara)

/**
*Constructs a new character page
*@param main : player's main menu
*/
public Page(Main main)
{
super();
//this.chara=chara;
this.main=main;

inds=false;
II=-1;
IS=-1;
IA=-1;

searched=false;

setBackground(Color.YELLOW);

setLayout(new GridLayout(0,2)); //2,0?

/*
up=new JPanel();
up.setLayout(new BorderLayout());
//all 5 things. armor. shield. weapon.  (head? legs?)
//head: elf\human?
//leges... ? class?

down=new JPanel();

add(up);
add(down);

*/
}




public void paintComponent(Graphics g)
{
super.paintComponent(g);
//update(g);
}
	
	
	
/*
 *returns a Dimension
 */
public Dimension getPreferredSize()
{
	int dim=250;
	return new Dimension(dim,dim);
}
	

public void setCh(Character c)
{
	chara=c;
}


public void setSearched(boolean b)
{
searched=b;	
}

public boolean isSearched()
{
return searched;	
}

/**
*Updates the page
*/
public void update(Character c)
{
	chara=c;
	
	
	if(chara!=null)
	{
//	//System.out.println("here");
	
	//setSize(100,100);
//	setLayout(new BorderLayout());

	main.setVisible(false);
	removeAll();
	right=new JPanel();
	//right.removeAll();
	right.setBackground(Color.yellow);
	right.setLayout(new BorderLayout());
	//all 5 things. armor. shield. weapon.  (head? legs?)
	//head: elf\human?
	//leges... ? class?
	
	left=new JPanel();
	//right.removeAll();
	String gen;
	if(chara.male())
	gen="male";
	else
	gen="female";
	JLabel name=new JLabel(chara.name()+"- "+chara.getRace()+" "+chara.getClas()+" "+chara.lvl()+" ("+gen+") ["+chara.getX()+","+chara.getY()+"]");
	//remove x and y?
	right.add(BorderLayout.NORTH,name);	
	
	JPanel rc=new JPanel();
	
	rc.setLayout(new GridLayout(0,1));
	
	rc.add(label("<html><font size=5 color=rgb(0,255,255)>HP: "+(chara.getHp()-chara.getDam())+"/"+chara.getHp(),"Life/Max Health Points"));
	rc.add(label("<html><font size=5 color=rgb(0,255,255)>AC: "+chara.AC(),"Armor Class"));
	rc.add(label("<html><font size=5 color=green>Base Attack: "+chara.bab(),"Base attack bonus and number of attacks"));
		
	right.add(BorderLayout.CENTER,rc);	
	//left: items ,middle: skills ,right: spells
	
	//chara.addItem(new Item("Silk"));
	
	JPanel south=new JPanel();
	south.setLayout(new GridLayout(3,1));
	
	//chara.getItems().add(Weapon.create("Longsword",chara)); //del
	
	Vector v=new Vector(chara.getItems());
	
	JComboBox combo=new JComboBox(v);
	combo.setToolTipText("Items");
	
	combo.setActionCommand("Items");
	combo.addActionListener(this);
	if(II!=-1 && v.size()>0)	
	{
		//ind=true;
		combo.setSelectedIndex(II);
	}
	
	///* come back here !
	if(v.size()>0)
	combo.setRenderer(new ListCellRenderer(){
								
									public Component getListCellRendererComponent(
							         JList list,
							         Object value,
							         int index,
							         boolean isSelected,
							         boolean cellHasFocus)
							     {
							     	
							     DefaultListCellRenderer def = new DefaultListCellRenderer();
							     	
							     	//JLabel c=new JLabel();
							     	
							     	JLabel c = (JLabel)def.getListCellRendererComponent(list, value, index, isSelected,cellHasFocus);
							     	//JLabel c=(JLabel)(getListCellRendererComponent(list,value,index,isSelected,cellHasFocus));
							     	////System.out.println("value: "+value);
							         c.setText(value.toString());
							         //c.setBackground(isSelected ? Color.blue : Color.white);
							         
							         //setBackground(list.getBackground());
							         
							         setBackground(list.getSelectionBackground());
									 setForeground(list.getSelectionForeground());

							         					         
							         //if(((Char)(value)).attacked())
							         //c.setForeground(isSelected ? Color.magenta : Color.red);
							         //if(value instanceof 							         
							         c.setToolTipText(((Item)(value)).info());
							         return c;
							     }
								
										
									});
	//*/
	south.add(combo);
	//combo.setSelectedIndex(-1);
		
	//right.add(BorderLayout.EAST,combo);
	
	//label- starting label on combo?
	//combos if not tooltiptext
	
	//v.add(0,"Items:"); //?
	
	//for(int i=0;i<20;i++)
	//v.add("items "+i);
	
	//scrollable? + to not choose more than one. (maybe MAIN one exists?)
	
	//JList items=new JList(v);
	//items.setVisibleRowCount(5);
	
	//right.add(BorderLayout.WEST,items);
	
			
	//tooltip- list?
	//JComboBox skill=new JComboBox(chara.getItems()); COMBO- for other things.
	
	//JList lis=new JList(chara.getSkills().getVector());
	
	combo=new JComboBox(chara.getSkills().getVector());
	
	combo.setMaximumRowCount(10);
	
	if(chara.skil()==0)
	combo.setToolTipText("Skills");
	else
	combo.setToolTipText("You have "+chara.skil()+" skill points left");
	
	combo.setActionCommand("Skills");
	combo.addActionListener(this);
	if(IS!=-1)	
	{
		inds=true;
		combo.setSelectedIndex(IS);
	}
	
	south.add(combo);
	
	
	v=new Vector(chara.getSpells().getVector());
	combo=new JComboBox(v);
	combo.setToolTipText("Spells and Special Abilities");
	
	combo.setActionCommand("Spells");
	combo.addActionListener(this);
	if(IA!=-1)	
	{
	//ind=true;
	combo.setSelectedIndex(IA);
	}
	
	if(v.size()>0)
	combo.setRenderer(new ListCellRenderer(){
								
									public Component getListCellRendererComponent(
							         JList list,
							         Object value,
							         int index,
							         boolean isSelected,
							         boolean cellHasFocus)
							     {
							     	
							     DefaultListCellRenderer def = new DefaultListCellRenderer();
							     	
							     	//JLabel c=new JLabel();
							     	
							     	JLabel c = (JLabel)def.getListCellRendererComponent(list, value, index, isSelected,cellHasFocus);
							     	//JLabel c=(JLabel)(getListCellRendererComponent(list,value,index,isSelected,cellHasFocus));
							     	////System.out.println("value: "+value);
							         c.setText(value.toString());
							         //c.setBackground(isSelected ? Color.blue : Color.white);
							         
							         //setBackground(list.getBackground());
							         
							         setBackground(list.getSelectionBackground());
									 setForeground(list.getSelectionForeground());

							         					         
							         //if(((Char)(value)).attacked())
							         //c.setForeground(isSelected ? Color.magenta : Color.red);
							         //if(value instanceof 							         
							         c.setToolTipText(value.toString());
							         return c;
							     }
								
										
									});
	
	
	south.add(combo);
	
	//description?
	
	
	//lis.setBackground(Color.blue);
	
	//tooltip! + spells.
	
	
	right.add(BorderLayout.SOUTH,south);
	
	
	left.setLayout(new BorderLayout());
	
	JPanel up=new JPanel(),center=new JPanel(),down=new JPanel();
	
	
	
	//JLabel l=new JLabel("<html><body bgcolor=green><font color=blue>Sparky the Dog</font></body></html>");
	//http://www.w3schools.com/html/html_colors.asp
	//for light green as well
	
	up.setLayout(new GridLayout(0,3));
	
	
	if(chara.abil()==0)
	{
	
	JLabel l=new JLabel("<html><font color=red>Strength: "+chara.getSTR());
	l.setToolTipText("Modifier: "+chara.str());
	up.add(l);
		
	l=new JLabel("<html><font color=rgb(255,100,0)>Dexterety: "+chara.getDEX());
	l.setToolTipText("Modifier: "+chara.dex());
	up.add(l);
	l=new JLabel("<html><font color=green>Constitution: "+chara.getCON());
	l.setToolTipText("Modifier: "+chara.con());
	up.add(l);
	l=new JLabel("<html><font color=purple>Intelligence: "+chara.getINT());
	l.setToolTipText("Modifier: "+chara.inl());
	up.add(l);
	l=new JLabel("<html><font color=blue>Wisdom: "+chara.getWIS());
	l.setToolTipText("Modifier: "+chara.wis());
	up.add(l);
	l=new JLabel("<html><font color=blue>Charisma: "+chara.getCHA());
	l.setToolTipText("Modifier: "+chara.cha());
	up.add(l);
	}
	else
	{
	JButton l=new JButton("<html><font color=red>Strength: "+chara.getSTR());
	l.setToolTipText("You have "+chara.abil()+" ability points left");
	l.setActionCommand("str");
	l.addActionListener(this);
	up.add(l);
		
	l=new JButton("<html><font color=rgb(255,100,0)>Dexterety: "+chara.getDEX());
	l.setToolTipText("You have "+chara.abil()+" ability points left");
	l.setActionCommand("dex");
	l.addActionListener(this);
	up.add(l);
	l=new JButton("<html><font color=green>Constitution: "+chara.getCON());
	l.setToolTipText("You have "+chara.abil()+" ability points left");
	l.setActionCommand("con");
	l.addActionListener(this);
	up.add(l);
	l=new JButton("<html><font color=purple>Intelligence: "+chara.getINT());
	l.setToolTipText("You have "+chara.abil()+" ability points left");
	l.setActionCommand("int");
	l.addActionListener(this);
	up.add(l);
	l=new JButton("<html><font color=blue>Wisdom: "+chara.getWIS());
	l.setToolTipText("You have "+chara.abil()+" ability points left");
	l.setActionCommand("wis");
	l.addActionListener(this);
	up.add(l);
	l=new JButton("<html><font color=blue>Charisma: "+chara.getCHA());
	l.setToolTipText("You have "+chara.abil()+" ability points left");
	l.setActionCommand("cha");
	l.addActionListener(this);
	up.add(l);
		
		
	}
	
	//combo?
	
	
//	String a="Unarmed"; //del?
/*	if(chara.hasW())
	a=chara.getWeapon().toString();
	else
	{
	Dice m;
	if(chara.getRace().equals("Dwarf")||chara.getRace().equals("Halfling"))
	m=new Dice(1,2);
	else
	m=new Dice(1,3);
	
	
	
	int bbb=Char.mod(chara.getSTR());
	int a1=Item.howMany(chara.getItems(),"Ring of Damage");
	if(a1>3)
	a1=3;
	bbb+=a1;
	
	a=a+" - attack: "+chara.attack()+", damage: "+m.getString(bbb);	
	}
*/	


	
	
	
	
	center.setLayout(new GridLayout(0,1));
	
	//center.add(label("<html><body bgcolor=rgb(0,255,0)>Weapon: "+a));

	Vector vv=new Vector(Item.getAllWeapons(chara.getItems()));
	if(chara.getWeapon().getName().equals("Unarmed Strike"))
	vv.add(0,chara.getWeapon());
	else
	vv.add(0,Weapon.create("Unarmed Strike",chara));
	
	
	//System.out.println(vv);
	
	wea=new JComboBox(vv);
	wea.setSelectedItem(chara.getWeapon());
	wea.setBackground(new Color(108,88,188)); //?
	wea.addActionListener(this);
	wea.setToolTipText("Choose your weapon:");
	center.add(wea);
	
	
//	String t="";//tool tip
	
	
	/*if(chara.getShield()==null)
	a="none";
	else
	{
		a=chara.getShield().toString();
		
		t="armor bonus, ";
		if(chara.getShield().isMagic())
		t+="magic bonus, ";
		t+="check penalty, spell failure";
	}
	*/
				
	//center.add(label("<html><body bgcolor=rgb(0,255,255)>Shield: "+a,t));
	
	vv=new Vector(Item.getAllShields(chara.getItems()));
	vv.add(0,"None");

	shi=new JComboBox(vv);
	
	if(chara.getShield()!=null)
	shi.setSelectedItem(chara.getShield());
	else
	shi.setSelectedIndex(0);
	
	shi.setBackground(new Color(208,88,255)); //?
	shi.addActionListener(this);
	shi.setToolTipText("Choose your shield:");
	center.add(shi);
	
	
/*	
	t="";
	if(chara.getArmor()==null)
	a="none";
	else
	{
	a=chara.getArmor().toString();
	
	t="armor bonus, ";
	if(chara.getArmor().isMagic())
	t+="magic bonus, ";
	t+="check penalty, spell failure, max dexterety bonus (to AC)";
	}
	
	center.add(label("<html><body bgcolor=#FAEBD7>Armor: "+a,t));
*/


	vv=new Vector(Item.getAllArmors(chara.getItems()));
	vv.add(0,"None");

	arm=new JComboBox(vv);
	
	if(chara.getArmor()!=null)
	arm.setSelectedItem(chara.getArmor());
	else
	arm.setSelectedIndex(0);
	
	arm.setBackground(new Color(88,208,188)); //?
	arm.addActionListener(this);
	arm.setToolTipText("Choose your armor:");
	center.add(arm);


	
	
	down.setLayout(new GridLayout(0,3));
	
	down.add(label("<html><font color=rgb(255,50,50)>Level: "+chara.lvl()));
	down.add(label("<html><font color=rgb(255,50,50)>Experience: "+chara.getXP()));
	down.add(label("<html><font color=rgb(255,50,50)>Need: "+chara.getNeed(),"Experience Needed for the next level"));
	
	//chara.setBA(10);
	
	//down.add(label("<html><font color=rgb(0,255,255)>HP: "+(chara.getHp()-chara.getDam())+"/"+chara.getHp(),"Life/Max Health Points"));
		
	//down.add(label("<html><font color=#A52A2A>AC: "+chara.AC(),"Armor Class"));
	//down.add(label("<html><font color=green>Base Attack: "+chara.bab(),"Base attack bonus and number of attacks"));
	down.add(label("<html><font color=rgb(0,255,0)>Speed: "+chara.speed(),"Number of turns until you need to rest"));
	down.add(label("<html><font color=blue>Spell-Resistance: "+chara.getRes()));
	down.add(label("<html><font color=red>Damage: "+chara.getDam(),"How much you are hurt."));
	
	down.add(label("<html><font color=red>Hunger: "+chara.getHunger(),"How hungry you are"));
	down.add(label("<html><font color=rgb(100,100,200)>Moved: "+chara.getMoved(),"Number of turns you had this day"));
	down.add(label("<html><font color=#A52A2A>Money: "+chara.gp(),"Amount of GP (gold pieces) you have"));
	
	down.add(label("<html><font color=black>Reflex save: "+chara.getRef()));
	down.add(label("<html><font color=black>Will save: "+chara.getWill()));
	down.add(label("<html><font color=black>Fortitude save: "+chara.getFort()));
		
	//the last 3? (or the last 2?)
	if(chara.getDamr()!=0)
	down.add(label("<html><font color=white>Damage-Reduction: "+chara.getDamr(),"Damage reduced from every hit")); //0=not seen
	if(chara.natural()!=0)
	down.add(label("<html><font color=green>Natural: "+chara.natural(),"Natural Armor")); //0=not seen
	
//	chara.getSpells().addDisease("Posion",5);	
		
	Vector aff=chara.getSpells().getAffected();
	JComboBox aff2=new JComboBox(aff);
	aff2.setToolTipText("Spells and Abilities that affect you"); //diseases
	if(aff.size()>0)
	down.add(aff2);


	left.add(BorderLayout.NORTH,up);
	left.add(BorderLayout.CENTER,center);
	left.add(BorderLayout.SOUTH,down);
	
	add(left);
	add(right);

	
	
	//ac,hp,dam,damr,etc. items! (tooltip-tostring to all?) List,JList? ; or combo box
	
	main.setVisible(true);
	//repaint();
	//update();
	}
	
		
}



public JLabel label(String a)
{
return new JLabel(a);
}



public JLabel label(String a,String t)
{
JLabel l=new JLabel(a);
l.setToolTipText(t);
////System.out.println(l.getText()+" "+l.getToolTipText());
return l;
}



public void unable(boolean tr)
{
if(wea!=null)
{
wea.setEnabled(!tr);	
arm.setEnabled(!tr);
shi.setEnabled(!tr);
}
}


public void actionPerformed(ActionEvent e)
{
			////System.out.println("check ");	
	//limit, shields etc.
	
if(e.getSource() instanceof JComboBox)
{	
	
	JComboBox combo=(JComboBox)(e.getSource());
	
			
	if(combo==wea)
	{	
	Weapon w=(Weapon)(combo.getSelectedItem());
	
		if(((w.isRanged()&&!(w.getName().equals("Sling")))||(w.getName().indexOf("Great")!=-1))&&(chara.getShield()!=null))
		{
			if(!chara.getShield().getName().equals("Buckler"))
			{
				main.setMes("You can't put that weapon (it is two handed)");
				update(chara);
			}
			else
			{
				chara.setWeapon(w);
				update(chara);
				if(main.isConnected())
				main.sendCPlace(); //will does cause a bug? (synchronized?)
			}
		}
		else
		{
		chara.setWeapon(w);
		update(chara);
		if(main.isConnected())
		main.sendCPlace();
		}
	}
	
	if(combo==shi)
	{
	Weapon w=(Weapon)(chara.getWeapon());
		
	if(((w.isRanged()&&!(w.getName().equals("Sling")))||(w.getName().indexOf("Great")!=-1))&&(combo.getSelectedIndex()!=0))	
	{
		if(!((Shield)(combo.getSelectedItem())).getName().equals("Buckler"))
		{
			main.setMes("You can't put that shield (you have a two handed weapon)");
			update(chara);
		}
		else
		{
			chara.setShield((Shield)(combo.getSelectedItem()));
			update(chara);
			if(main.isConnected())
			main.sendCPlace();
		}
	}	
	else
	{
		if(combo.getSelectedIndex()!=0)
		chara.setShield((Shield)(combo.getSelectedItem()));
		else
		chara.setShield(null);
	
	update(chara);
	if(main.isConnected())
	main.sendCPlace();
	}
	
	}
		
	
	if(combo==arm)
	{
		if(combo.getSelectedIndex()!=0)
		chara.setArmor((Armor)(combo.getSelectedItem()));
		else
		chara.setArmor(null);
		
		update(chara);
		if(main.isConnected())
		main.sendCPlace();
	}
	
	
	if(combo.getActionCommand().equals("Items"))
	{
		II=combo.getSelectedIndex();
	}
	
	if(combo.getActionCommand().equals("Spells"))
	{
		IA=combo.getSelectedIndex();
	}
	
	if(combo.getActionCommand().equals("Skills")&&!inds)
	{
		
		IS=combo.getSelectedIndex();
		
		if(chara.skil()>0)
		{
			String s=(String)(combo.getSelectedItem());
				int t1=s.indexOf(':'),t2=s.indexOf('(');
				if(t2<t1&&t2!=-1)
				s=s.substring(0,t2-1);
				else
				s=s.substring(0,t1);
				
			//String cho=JOptionPane.showInputDialog(main.getFrame(),"You have "+chara.skil()+" skill points left.\nHow many points do you want to add to your "+s+"?","Improving Skills",JOptionPane.QUESTION_MESSAGE);
			String cho=JOptionPane.showInputDialog(main.getFrame(),"You have "+chara.skil()+" skill points left.\nHow many points do you want to add to your "+s+"?","1");
									
				
				if(isNatural(cho))
				{
				
				int im=Integer.parseInt(cho);
				
					if(im!=0)
					{
					if(im>chara.skil())
					im=chara.skil();
						
					chara.getSkills().add(s,im);
					chara.addSkil(-im);
								
					
					update(chara);
					
					if(main.isConnected())
					main.sendCPlace();
					}
				}
		}
				
	}
	inds=false;
	
}

if(e.getSource() instanceof JButton)
{
	
	JButton b=(JButton)(e.getSource());
	String s=b.getActionCommand();
	
	if(s.equals("str"))
	{
	chara.addStr(1);
	chara.addAbil(-1);	
	}
	
	if(s.equals("dex"))
	{
	chara.addDex(1);
	chara.addAbil(-1);	
	}
	
	if(s.equals("con"))
	{
	chara.addCon(1);
	chara.addAbil(-1);	
	}
	
	if(s.equals("wis"))
	{
	chara.addWis(1);
	chara.addAbil(-1);	
	}
	
	if(s.equals("int"))
	{
	chara.addInt(1);
	chara.addAbil(-1);	
	}
	
	if(s.equals("cha"))
	{
	chara.addCha(1);
	chara.addAbil(-1);	
	}
	
	chara.getSpells().updateSpells(); //efficient?
	
	update(chara);
	
	
	if(main.isConnected())
	main.sendCPlace();
	
	
}
	
} //end action


public int getIS()
{
return IS;	
}


public int getIA()
{
return IA;	
}

/**
*Checks if a string is a natural number
*@param s : the string
*/
public static boolean isNatural(String s)
{
if(s==null||s.equals(""))
return false;

char[] c=s.toCharArray();	
	
	for(int i=0;i<c.length;i++)
	if(c[i]<'0'||c[i]>'9')
	return false;
	
return true;	
}


public void reset()
{
	II=-1;
	IS=-1;
	IA=-1;
	
}

}
