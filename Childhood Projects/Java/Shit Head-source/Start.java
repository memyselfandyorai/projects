	import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;

public class Start {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		
		//System.out.println("a".compareTo("c")); -2
		
		JFrame f=new JFrame();
		
		String[] options=new String[3];
		options[0]="Play";
		options[1]="Open Server";
		options[2]="Exit";
		
		int ch=JOptionPane.showOptionDialog(f,"What do you want to do?","",0,JOptionPane.PLAIN_MESSAGE,null, options, 0);
		
		if(ch==0)
			Client.main(null);
		if(ch==1)
			Server.main(null);
		if(ch==2||ch==-1)
			System.exit(0);
		
	}

}
