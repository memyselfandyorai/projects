const {POOL_CLEAR}= require('../../../../common/pools.js');
const {ModifyPools}= require('../../../../common/poolsController.js');

let input = {
  action: POOL_CLEAR,
}
await ModifyPools(input, context.params.event)

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "pool-clear",
  "description": "Removes a pool.",
  "options": [
    {
      "type": 3,
      "name": "names",
      "description": "Comma separates list of pool names. Use * to delete all.",
      "required": true
    }
  ]
});
*/