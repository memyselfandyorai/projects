package Objects;


import org.eclipse.swt.graphics.*;



import java.io.File;

import General.Color;
import General.Intersection;
import General.NormalVector;
import General.Plane;
import General.Point;
import General.Point2D;
import General.Ray;
import General.Scene;
import General.Vector;
import Lights.Light;
import Main.Utilities;


/**
 * 
 * represents a surface
 *
 */
public abstract class Surface {


	protected String type;
	protected Color diffuse; // K D
	protected Color specular; // K S
	protected Color ambient; // K A
	protected Color emission; // I E
	protected int shininess; // n 
	
	//checkers
	protected double size; // for 10X10 square 
	protected Color diffuse1;
	protected Color diffuse2; 
	
	protected String texture; //full path or filename
		
	protected double reflectance ; //K T : (K S, actually. double use in slides of same name)
	

	private ImageData textureImg; 	
	
	private static final int maxIteration=5;
	
	protected Surface()
	{
		type="flat";
		diffuse=new Color(0.8,0.8,0.8);
		specular=new Color(1,1,1);
		ambient=new Color(0.1,0.1,0.1);
		emission=new Color(0,0,0);
		shininess=100;
		size=0.1;
		diffuse1=new Color(1,1,1); //white
		diffuse2=new Color(0.1,0.1,0.1); //dark gray
		texture=null;
		reflectance=0.0;
		
		//reflectance=0 - default of java is 0
	}

	

	public void setType(String type) {
		this.type = type;
	}

	public void setDiffuse(Color diffuse) {
		this.diffuse = diffuse;
	}

	public void setSpecular(Color specular) {
		this.specular = specular;
	}

	public void setAmbient(Color ambient) {
		this.ambient = ambient;
	}

	public void setEmission(Color emission) {
		this.emission = emission;
	}

	public void setShininess(int shininess) {
		this.shininess = shininess;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public void setDiffuse1(Color diffuse1) {
		this.diffuse1 = diffuse1;
	}

	public void setDiffuse2(Color diffuse2) {
		this.diffuse2 = diffuse2;
	}


	public void setTexture(String texture) {	
		this.texture = texture;
	}

	
	public void setReflectance(double reflectance) {
		this.reflectance = reflectance;
	}
	
	public Color getDiffuse() {
		return diffuse;
	}

	public Color getSpecular() {
		return specular;
	}

	public Color getAmbient() {
		return ambient;
	}

	public Color getEmission() {
		return emission;
	}

	public int getShininess() {
		return shininess;
	}

	public double getSize() {
		return size;
	}

	public String getTexture() {
		return texture;
	}

	public double getReflectance() {
		return reflectance;
	}
	
	public Color getDiffuse1() {
		return diffuse1;
	}

	public Color getDiffuse2() {
		return diffuse2;
	}
	
	
	
	/**
	 * returns the intersection of this surface with the given ray in the given scene
	 * */
	public abstract Intersection intersect(Scene scene,Ray ray);
	
	/**
	 * returns true iff this surface has all needed values
	 * */
	public abstract boolean hasAllValues();
	
	/**
	 * transforms the given 3D point into a 2D point, so it can be used to access a flat texture
	 * @param point
	 * @return
	 */
	public abstract Point2D parametrization(Point point); 
	
	/**
	 * returns true iff: type is "texture" -> texture is a file 
	 * [also checks if it's a full path or only filename and then adds the path]
	 * 
	 * and creates an imagedata using the file
	 * */
	public boolean checkTexture()
	{
		if(type.equals("texture"))
		{
			
			if(texture==null)
				return false;
			File f=new File(texture);
			if(!f.exists())
			{
				texture=Scene.chosenPath+texture;
				f=new File(texture);
											
				if(!f.exists())
					return false;
			}
			
			if(!texture.toLowerCase().endsWith(".png")&&Scene.supportOnlyPNG)
				return false;
			
			//type is texture, and it's ok
			this.textureImg = new ImageData(this.texture);
		}
		
		
		return true;
	}
		
	/**
	 * gets the diffuse element
	 */
	public Color getDiffuseColor(Point2D point){
		if(type.equals("checkers")){
			return getCheckersDiffuse(point);
		}
		else{
			if(type.equals("texture")){
					return getTextureDiffuse(point);
				}
		}
	
		return diffuse;
	}
	
	

	/**
	 * returns the correct checker diffuse color
	 */
	private Color getCheckersDiffuse(Point2D point) {
		if(isChecker1Diffuse(point)){
			return getDiffuse1();
		}else{
			return getDiffuse2();
		}
	}
	
	/**
	 * returns true iff the point belongs to checker 1
	 *
	 */
	private boolean isChecker1Diffuse(Point2D point){
		double x = point.getX();
		double y = point.getY();
		double M = 1/size;
		int colorX = (int)Math.floor(x*M)%2;
		int colorY = (int)Math.floor(y*M)%2;
		int result = colorX ^ colorY;
		if(result == 0)
			return false;
		else
			return true;
	}
	
	
	/**
	 * gets the diffuse of the texture in 'point'
	 */
	private Color getTextureDiffuse(Point2D point) {
		if(this.textureImg != null){
			
			int x = (int)(point.getX()*(textureImg.width-1));
			int y = (int)(point.getY()*(textureImg.height-1));
			
			Color color=Utilities.getColorFromImage(textureImg, x, y);
			
			return color;
		}else{
			return diffuse;
		}
	}
	
	/**
	 * returns the color of 'this' surface
	 * */
	public Color getColor(Scene scene,Intersection intersection){
		
		return getColor(scene,intersection,0);
	}
	
	
	/**
	 * returns if object is on the right of a given plane.
	 * 
	 * assumes plane and object NOT intersecting
	 * 
	 * */
	public abstract boolean onRight(Plane plane);
	

	
	/**
	 * returns the color of this surface, given its iteration
	 * (if > maxIteration, doesn't ray trace: only ray-cast
	 * */
	private Color getColor(Scene scene,Intersection intersection,int iteration){
		
		Ray ray=intersection.getRay();
		Point hit=intersection.getHit();
		
		//double distance=intersection.getDistance();
		
		Color I=new Color();
		
		I.add(emission); //I+= I E
		I.add(scene.getAmbientLight().mult(ambient)); //I += K A * I AL
		
			
		NormalVector N=intersection.getNormal();
		NormalVector V=ray.getVector().negative();
		
		Point2D hit2D = parametrization(hit);
		Color newDiffuse = getDiffuseColor(hit2D);
		
		int temp=0;
		
		for(Light light:scene)
		{
			NormalVector L=light.getDirection(hit);
			NormalVector R=getReflectance(N,L,hit);
			Color IL=light.getColor(hit);
			
			Ray rayTowardsLight=new Ray(hit,L);
			rayTowardsLight.addEpsilon();
			
			if(scene.hits(rayTowardsLight,light,N))
			{
				temp++;
				I.add(newDiffuse.mult(N.dotProduct(L)).mult(IL)); //I+=K D * (N*L)* I L
				
				double scalar=V.dotProduct(R);
				if(scalar<0)
					scalar=0;
				
				scalar=Math.pow(scalar, shininess); // (V*R)^n

				
				I.add(specular.mult(scalar).mult(IL)); //I+= K S * (V*R)^n * I L
				
			}
		}
		
		if(reflectance!=0&&iteration<maxIteration)
		{
			Ray rayTrace; //ray of trace
			Color IR=null; //I R
			rayTrace=new Ray(hit,getReflectance(N,V,hit)); //R v
			rayTrace.addEpsilon();
			Intersection traceInter=scene.findIntersection(rayTrace);
			
			if(traceInter!=Intersection.MISS)
			{
				Surface interObject=traceInter.getObject();
				
				IR=interObject.getColor(scene,traceInter,iteration+1);
			}
			
			if(IR==null)
				IR=scene.getBackColor();
				
			I.add(IR.mult(reflectance));
		}
		
		return I;
	}
	
	
	/**
	 * returns the reflectance of vector light by the normal vector (originating from hit)
	 * */
	public static NormalVector getReflectance(NormalVector N,NormalVector L,Point hit)
	{
		/*Vector NXL=N.crossProduct(new Vector(L));
		double sin=NXL.norm(); // = |NXL| = sin(theta)
		
		
		//double cos=N.dotProduct(L); //=N*L = cos (theta)
		
		NormalVector surfaceDirection=new NormalVector(N.crossProduct(NXL));
		//if(!Ray.getPoint(hit.add(L.toPoint()),surfaceDirection,sin).equals(Ray.getPoint(hit,N,cos)))
		//	surfaceDirection=surfaceDirection.negative();
		
		if(L.dotProduct(surfaceDirection)>=0) //should be the other way. angle > 90 degrees
			surfaceDirection=surfaceDirection.negative();
		
		Point R=Ray.getPoint(hit.add(L.toPoint()),surfaceDirection,2*sin);
		
		return new NormalVector(R.toVector());*/
		
		
		/*here we found more simple solution:*/
		
		Vector N2= N.product(L.dotProduct(N));  //N2 = (L*N) * N
		Vector R= N2.product(2).toPoint().add(L.negative().toPoint()).toVector(); //R=2*N2-L
		
		return new NormalVector(R);
	}
	
	

	
}
