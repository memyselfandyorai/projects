import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


//do a dunction that calcs the weight. (in Item?) : for swim.
//(and to see if you can carry more? another function to see how much you can carry?) ["remove item" button?]


//place: dc lower (a chance in dc, 2 from 3?)


//bug with attacks- not always 2 attacks.

//bug when dying from hunger- can still attack you.

public class Server implements Runnable{


  /**
  *This socket
  */
  private Socket socket;
  /**
  *Write in the client-server "conversation"
  */
  private BufferedWriter out;
  /**
  *Reader in the client-server "conversation"
  */
  private BufferedReader in;
  /**
  *The universal map
  */
  private Map map;
  /**
  *All the sockets that entered the server
  */
  private Vector players;
  /**
  *The String the server gets from client
  */
  private String line;

	/**
	*Constructs a Server
	*@param socket : the socket to deal with
	*@param map : the map of the game
	*@param v : the vector of the players' sockets
	*/
  public Server(Socket socket,Map map,Vector v){
    this.socket=socket;
  	this.map=map;
  	players=v;
  }

	/**
	*Starts the read-write prosses with the client
	*/
  public void run(){
     try{
        
        out =new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        in = new BufferedReader(new InputStreamReader (socket.getInputStream()));
        
        //out.write("What is your name?\n");
        //out.flush();
        
        //send map.
        String m=map.write()+"\n";
       
        //System.out.println("m: "+m);
        //System.out.println("len: "+m.length());
        
        //server has 1 more (oh. \n).
        
        out.write(m);
        out.flush();
        
        players.add(socket); //instead of when created.
        
        
        
        //have to synchronize!!!
        
        synchronized(this)
        {
        while (( line=in.readLine())!=null)
        {
           if (!line.equals("exit")&&!line.equals("quit")){
            // loop- info for all players.
            
            //System.out.println("ser-line: "+line);
            
            //System.out.println("st: "+line.substring(0,7));
            
            //System.out.println(line.substring(0,5));
            
            if(line.indexOf("PC*")==0)
            {
            String s=line.substring(3);
            int x,y;
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf("*P");
            Place p=Place.read(s.substring(0,b+1));
            s=s.substring(b+3);
                               	
            map.setPlace(y,x,p);
            
            
            //System.out.println("pc....");
            
            		for(int i=0;i<players.size();i++)
            		{
            		//System.out.println("nim!"); 
            		Socket sock=(Socket)(players.elementAt(i));
            			if(sock!=socket)
            			{
	            		BufferedWriter out2 =new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
	            		out2.write(line+"\n");
	            		out2.flush();
	            		//System.out.println("line: "+line);
            			}
            		}
            
            //System.out.println("y,x: "+y+","+x);
            //System.out.println("pc: "+map.getPlace(y,x).getChars());
            
            }
            
            
            
            if(line.indexOf("UPDATE*")==0)
            {
            String s=line.substring(7);
            int x,y;
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf("*P");
            Place p=Place.read(s.substring(0,b+1));
            s=s.substring(b+3);
                               	
            map.setPlace(y,x,p);
            
            		for(int i=0;i<players.size();i++)
            		{
            		//System.out.println("nim!"); 
            		Socket sock=(Socket)(players.elementAt(i));
            			if(sock!=socket)
            			{
	            		BufferedWriter out2 =new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
	            		out2.write(line+"\n");
	            		out2.flush();
	          			}
            		}
                                          
            }
            
            
            
            
            if(line.indexOf("PCC*")==0)
            {
            String s=line.substring(4);
            int x,y,add;
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            b=s.indexOf(",");
			add=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf("*P");
            Place p=Place.read(s.substring(0,b+1));
            s=s.substring(b+3);
                               	
            map.setPlace(y,x,p,add);
            
            		for(int i=0;i<players.size();i++)
            		{
            		//System.out.println("nim!"); 
            		Socket sock=(Socket)(players.elementAt(i));
            			if(sock!=socket)
            			{
	            		BufferedWriter out2 =new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
	            		out2.write(line+"\n");
	            		out2.flush();
	            		//System.out.println("line: "+line);
            			}
            		}
            
            }
            
            /*
            if(line.indexOf("ATTACK*")==0)
            {
            //System.out.println(line);
            	          	          	
            	
            	for(int i=0;i<players.size();i++)
            	{
            	Socket sock=(Socket)(players.elementAt(i));
            	//System.out.println("socket: "+i);
            		if(sock!=socket)
            		{
          			//System.out.println("line: "+line);
	            	BufferedWriter out2 =new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
	            	out2.write(line+"\n");
	            	out2.flush();
	            	}
            	}
            }*/
            
            
            if(line.indexOf("CHANGE*")==0)
            {
            //System.out.println("line: "+line);
            
            String s=line.substring(7);
            int x,y,ind;
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            b=s.indexOf(",");
            ind=Integer.parseInt(s.substring(0,b));
            s=s.substring(b+1);
            
            
            b=s.indexOf("*P");
            Place p=Place.read(s.substring(0,b+1));
            s=s.substring(b+3);
            
            map.setPlace(y,x,p);
            
            sendPlace(p);
               
            Place[][] places=map.getPlaces();
            places[y][x].change(ind,this);
            }
            
            
            if(line.indexOf("MOVE*")==0)
            {
            //System.out.println(line);
            String s=line.substring(5);
            int x,y,ind,xx,yy;
            int b=s.indexOf(",");
			xx=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf(",");
			yy=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			ind=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            
            Place place=map.getPlace(y,x);
            //System.out.println("cs place: "+place.getChars()+"  ; ind: "+ind);
            Character player=(Character)(place.getChars().elementAt(ind));
            
            int hun=player.addMoved();//hopefully won't cause bugs.
              
            
            //System.out.println("moved: "+player.getMoved());
            //System.out.println(player.write());
                       
            int add=Place.move(player,map.getPlaces(),xx,yy);
			ind=player.getPlace().getChars().indexOf(player);
			
			
			
			//System.out.println(player.hp()+","+player.getDam()+","+hun); 
			
			if(!player.isDead()) //and dying?
			player.getPlace().change(ind,this);
			else //plz...?
			{
			Place place2=player.getPlace();
			int k=place2.getChars().indexOf(player);
			
			sendHunger(player.getX(),player.getY(),k,hun); //bugs?.   need to be before sendcharacter.
			sendCharacter(player,k,place2);
					
			place2.getFighters().remove(player);	
			place2.removeChar(player);
			place2.dropItems(player);
			}			
			
			//System.out.println(player.write());
			
			//System.out.println(player.getPlace().getChars());
			sendCPlace(player,player.getY()-yy,player.getX()-xx,add);
			sendCPlace(player);	
			
								
						
			//System.out.println("hun: "+hun);
			//System.out.println("dam: "+player.getDam());
			
			if(hun>0&&!player.isDead())
            sendHunger(player.getX(),player.getY(),player.getPlace().getChars().indexOf(player),hun); //bugs?
            
			
			
			//sendChange(player.getX(),player.getY(),player.getPlace().getChars().indexOf(player));
			
            	
            }
            
            
            if(line.indexOf("SLEEP*")==0)
            {
            //System.out.println(line);
            String s=line.substring(6);
            int x,y,ind;
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			ind=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			Place p=map.getPlace(y,x);
			
			p.change(ind,this);
				
            }
            
            if(line.indexOf("TALK*")==0)
            {
            String s=line.substring(5);
        
		    int x,y,ind;
		   	boolean priv;
		   	String mes;
		   	
		    int b=s.indexOf(",");
		    x=Integer.parseInt(s.substring(0,b));
		    s=s.substring(b+1);	
			
			b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			b=s.indexOf(",");
			ind=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			b=s.indexOf(",");
			if(s.substring(0,b).equals("true"))
			priv=true;
			else
			priv=false;
			s=s.substring(b+1);	
			
			b=s.indexOf("*TALK");
			mes=s.substring(0,b-1);
			s=s.substring(b+1);	
					
			String send1="TALK*"+x+","+y+",",send2=","+mes+"*TALK";
			//x,y,ind,mes: send1+ind+send2
					
			if(priv)
			sendMessage(send1+ind+send2);
			else
			{
			Place p=map.getPlace(y,x);
			Vector chs=p.getChars();
			Vector chars=p.getCharacters();
			
				for(int i=0;i<chars.size();i++)
				{
					int in=chs.indexOf(chars.elementAt(i));
					sendMessage(send1+in+send2);					
				}	
			}
			
			} //end talk.
            
            
            
            if(line.indexOf("PICK*")==0)
            {
            //System.out.println(line);
            String s=line.substring(5);
            int x,y,pla,sel;
            String chos;
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			chos=s.substring(0,b);
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			pla=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			sel=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			
			Place p=map.getPlace(y,x);
			
			Character player=(Character)(p.getChars().elementAt(pla));
			Char opo=(Char)(p.getChars().elementAt(sel));
			
			String mes=p.pickPocket(player,opo,chos.equals("Money"));
			
			sendPick(mes,player,player);
						
			if(mes.indexOf("seen")!=-1)
			{
				if(opo instanceof Character)
				sendPick(player.name()+" has tried to pick pocket you and failed. Do you want to attack "+player.gender()+"?",(Character)(opo),player);
				else
				p.charAttack(opo,player,this);
								
				//try to attack. work on this. !!!
				
				//need to find a way to attack like this.
				
			}
			
			sendCPlace(player);		
			
        	}//end pick
            
            if(line.indexOf("RUN*")==0)
            {
            String s=line.substring(4);
            int x,y,pla;
            
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			pla=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			Place p=map.getPlace(y,x);
			
			Character player=(Character)(p.getFighters().elementAt(pla));
			
			p.run(player,this);
			
			
			}
            
            
            if(line.indexOf("ATTACK*")==0)
            {
            //System.out.println("ATTACK*...");
            
            String s=line.substring(7);
        
		    int x,y,ser,lsv; //list selected value
		   
		    int b=s.indexOf(",");
		    x=Integer.parseInt(s.substring(0,b));
		    s=s.substring(b+1);	
			
			b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
		   
		    b=s.indexOf(",");
			ser=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
		   
		    b=s.indexOf(",");
			lsv=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	         
            
            
                
            Place[][] places=map.getPlaces();
            Place place=places[y][x];
            
            place.battle(ser,lsv,-1,this);
                        	
            } //end ATTACK
            
            
            
            if(line.indexOf("ABILITY*")==0)
            {
                        
            String s=line.substring(8);
        
		    int x,y,ser,ss,lsv; //list selected value
		   
		    int b=s.indexOf(",");
		    x=Integer.parseInt(s.substring(0,b));
		    s=s.substring(b+1);	
			
			b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
		   
		    b=s.indexOf(",");
			ser=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			b=s.indexOf(",");
			ss=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
		   
		    b=s.indexOf(",");
			lsv=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	         
            
            
                
            Place[][] places=map.getPlaces();
            Place place=places[y][x];
            
            Character player=(Character)(place.getChars().elementAt(ser));
            Char enm=(Char)(place.getChars().elementAt(lsv));
            
            //ss- index of spell  
            if(player.getSpells().getSpell(ss).indexOf("Cure")!=-1&&!player.attacked())
            {
            player.getSpells().useSpell(ss,enm);
            sendCPlace(player);
            }
            else
            place.battle(ser,lsv,ss,this);
                        	
            }
            
            
            //out.write(line);
            
            //out.flush();
            
            //System.out.println("l: "+line);
            
            
            //System.out.println("end of line");
            
           }
           else{
           	players.remove(socket);
            out.write("Disconnected.\n"); 
            out.flush();
            //System.out.println("Disconnected (by choice).");
             break;
           }
       }
       }
       
       players.remove(socket);
       out.write("Disconnected.\n"); 
       System.out.println("Disconnected.");
       out.flush();
       
       //System.out.println("Disconnected.");
       
        out.close();
        in.close();
        socket.close();

    } catch (IOException e){
        e.printStackTrace();
      }
  }   
	
	
	
	/**
	*Sends an hunger message to client
	*@param x : the x coordination
	*@param y : the y coordination
	*@param ind : the player's index
	*@param hun : the damage the hunger inflicted
	*/
	public void sendHunger(int x,int y,int ind,int hun)
	{
	String s="HUNGER*";
	s+=x+",";
	s+=y+",";	
	s+=ind+",";
	s+=hun+",";
	//System.out.println(s); 
	sendMessage(s+"*HUNGER");
	}
	
	/**
	*Sends a the result of the pick pocket attemp to client
	*@param mes : the message (result)
	*@param pl : the one who tried to pick pocket
	*@param orig : the player
	*/
	public void sendPick(String mes,Character pl,Character orig) //can really cause bugs.
	{
	String s="PICK*";
	s+=pl.getX()+",";
	s+=pl.getY()+",";
	s+=pl.getPlace().getChars().indexOf(pl)+",";
	s+=pl.getPlace().getChars().indexOf(orig)+",";
	s+=mes+",";
	sendMessage(s+"*PICK");
	}
	
	/**
	*Sends a converted place to client
	*@param p : the place
	*/
	public void sendPlace(Place p)
	{
		Place[][] pl=map.getPlaces();
			
			
			//System.out.println("send place, fighters:"); 
			//System.out.println(p.getFighters()); 
			
			
			//p.writeSeen();
			
			String s="PC*";
			s+=Place.getXY(pl,p);
			
			//System.out.println("sendp: "+s); 
			
			s+=p.write();
			
			
			//System.out.println("sending p...");
			
			sendMessage(s+"*PC");
	}
	
	
	/**
	*Sends a converted place to client, and the index of a character that left it
	*@param p : the place
	*@param k : the index
	*/
	public void sendPlace(Place p,int k)
	{
		Place[][] pl=map.getPlaces();
		
			//System.out.println("send place, fighters:"); 
			//System.out.println(p.getFighters()); 
				
			String s="PCC*";
			s+=Place.getXY(pl,p);
			
			//System.out.println("sendpk: "+s); 
			
			s+=k+",";
			s+=p.write();
			sendMessage(s+"*PCC");
	}
	
	
	public void sendChange(int x,int y,int ind)
	{
		String s="CHANGE*";
		s=s+x+",";
		s=s+y+",";
		s=s+ind+",";
		
		Vector c=map.getPlace(y,x).getChars();
			
		s=s+map.getPlace(y,x).write();
		
		sendMessage(s+"*CHANGE");
	}
	
	
	
	
	public void sendCPlace(Character player)
	{
		String s="PC*";
		
		//map.getPlace(player.getY(),player.getX()).writeSeen();
		
		s+=player.getX()+",";
		s+=player.getY()+",";
		s+=map.getPlace(player.getY(),player.getX()).write();
		
		//System.out.println("cpl");
		
		//System.out.println("sending cp...");
		
		//System.out.println(map.getPlace(player.getY(),player.getX()).write());
		
		sendMessage(s+"*PC");
	}	
	
	
	public void sendCPlace(Character player,int i,int j,int add)
	{
		String s="PCC*";
		if(i>=0&&j>=0&&i<Map.dim&&j<Map.dim&&add>=0)
		{
		s+=j+",";
		s+=i+",";
		s+=add+",";
		
		s+=map.getPlace(i,j).write();
		
		
		//System.out.println("chars. "+map.getPlace(i,j).getChars());
		
		sendMessage(s+"*PCC");
		}
					
	}	
	
	
	/**
	*Sends a message so the client will know if the player can play
	*@param d : the player's index
	*@param plthis : his place
	*/
	public void sendAttack(int d,Place plthis)
	{
		
		//sendMessage("ATTACK*"+Place.getXY(map.getPlaces(),plthis)+d+","+"*ATTACK");
		
		Character c=(Character)(plthis.getFighters().elementAt(d));
		sendMessage("ATTACK*"+c.getX()+","+c.getY()+","+d+","+"*ATTACK");
	}
	/*
	public void sendBattle(String s,Place plthis)
	{
		//System.out.println("sending battle...");
		String mes="BATTLE*"+Place.getXY(map.getPlaces(),plthis)+s;
		mes=mes+"*BATTLE";
		//System.out.println("mes: "+mes);
		sendMessage(mes);
	}*/
	
	/**
	*Sends a converted battle to client
	*@param s : the converted battle
	*@param plthis : the place
	*@param x : the x coordination
	*@param y : the y coordination
	*/
	public void sendBattle(String s,Place plthis,int x,int y)
	{
		//System.out.println("sending battle...");
		String mes="BATTLE*"+x+","+y+","+s;
		mes=mes+"*BATTLE";
		//System.out.println("mes: "+mes);
		sendMessage(mes);
	}
	
	
	public void sendBattleToSocket(String s,Place plthis,int x,int y)
	{
		//System.out.println("sending battle...");
		String mes="BATTLE*"+x+","+y+","+s;
		mes=mes+"*BATTLE";
		//System.out.println("mes: "+mes);
		sendToSocket(mes);
	}
	
	
	public void sendRun(Character player,String mes)
	{
		String s="RUN*";
		s=s+player.getX()+",";
		s=s+player.getY()+",";
		s=s+player.getPlace().getChars().indexOf(player)+",";
		s=s+mes+",";
				
		sendMessage(s+"*RUN");
	}
	
	/**
	*Sends a converted player to client
	*@param c : the player
	*@param ind : his index
	*@param p : his place
	*/
	public void sendCharacter(Character c,int ind,Place p)
	{
		String s="CHAR*";
		Place[][] pl=map.getPlaces();
		s+=Place.getXY(pl,p);
		s+=ind+",";
		s+=c.write();
		sendMessage(s+"*CHAR");
	}
	
	public static boolean isNatural(String s)
	{
	if(s==null||s.equals(""))
	return false;
		
	char[] c=s.toCharArray();	
		
		for(int i=0;i<c.length;i++)
		if(c[i]<'0'||c[i]>'9')
		return false;
		
	return true;	
	}
	
	/**
	*Sends a message to client
	*@param line : the line to send
	*/
	public synchronized void sendMessage(final String line)
	{
		//System.out.println("send "+line.substring(0,4));
		
		for(int i=0;i<players.size();i++)
        {
         try{
         	Socket sock=(Socket)(players.elementAt(i));
         	BufferedWriter out2 =new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
	     	out2.write(line+"\n");
	     	out2.flush();
	     	}catch(IOException e){System.out.println(e);}
	    }
	}
	
	
	public synchronized void sendToSocket(final String line)
	{
		try{
      	out.write(line+"\n");
    	out.flush();
     	}catch(IOException e){System.out.println(e);}
	}
	
	

  public static void main(String [] args){
  	  
  	  //System.out.println(0.1*6);!!!
  	  
 	  /*
  	 Vector v=new Vector();
  	 v.add(new Item("asda"));
  	 //Item[] i=(Item[])v.toArray();
  	  Item[] m=new Item[1];
  	  m[0]=new Item("ghf");
  	  Object o[]=v.toArray();//(Object[])(m);
  	  	
  	  System.out.println(o[0]);
  	  Item[] i=(Item[])(o); 	   
  	   */
  	/*
  	Attack a=new Attack();
  	a.addText("dsfds");
  	a.addText("4thgfd");
  	a.addText("hgfhgf6");
  	String s=a.convert();
  	System.out.println(s+"\n");
  	a.convert(s);
  	System.out.println(a.convert()+"\n");
  	*/
 	/*
  	Place p=new Place("Town");
  	p.addSur(p.getChar(0));
  	p.addSur(p.getChar(1));
  	p.addFighter((Char)(p.getChars().elementAt(0)));
  	p.addFighter((Char)(p.getChars().elementAt(1)));
  	String s=p.write();
  	System.out.println(s+"\n\n");
  	System.out.println("fighters: "+p.getFighters()+"\n\n");
  	p=Place.read(s);
  	String s2=p.write();
  	System.out.println(s2+"\n\n");
  	System.out.println("fighters: "+p.getFighters()+"\n\n");
  	if(s.equals(s2))
  	System.out.println("true");
  	*/
  	 /*
  	 Character d=new Character("Wilson",8,11,12,"Elf",true,10,13,14,9,"Paladin",new Skills(null,10,13,14,9,10,13,14,9,2,1));
     d.setWeapon(Weapon.create("Longsword",d));
     d.getSkills().setOwner(d);
     Place p=new Place("Town");
     d.setPlace(p);
     d.getSeen().add(p.getChars().elementAt(1));
     d.addMoved();
     d.addMoved();
     //System.out.println(d);
     String s=d.write();
     System.out.println(s);
     d=Character.readC(s,p);
     //System.out.println(d);
     s=d.write();
     System.out.println(s);
  	 */
  	try{
  	System.out.println("Host: "+InetAddress.getLocalHost().getHostName());
  	}catch(IOException e){
        System.out.println("An error has occured while finding host name");
        e.printStackTrace();
        } 
  	
  	
  	System.out.print("Enter Port: ");
  	
  	args=new String[1];
    try{
  	BufferedReader port=new BufferedReader(new InputStreamReader(System.in)) ;
    args[0]=port.readLine();
  	}catch(IOException e){
        System.out.println("An error has occured while doing I/O");
        e.printStackTrace();
        } 
  	
  	if(!isNatural(args[0]))
  	{
  		args[0]="4000"; 
  		System.out.println("Default Port: 4000");
  	}
	
    ServerSocket ssock;
    Map map=new Map();
    Vector pl=new Vector(); //sockvector
    try{
      ssock = new ServerSocket(Integer.parseInt(args[0]));

	  System.out.println("Online");
                       
      while (true){
        Socket sock = ssock.accept();
        System.out.println("Connection accepted");
        //pl.add(sock); after map.
        Server st=new Server(sock,map,pl);
		Thread t=new Thread(st); 
        t.start();
        }
    } catch (IOException e){
        System.out.println("Could not connect.");
        System.exit(1);        
     }

  }
}