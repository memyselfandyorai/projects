﻿using FolderSyncronizer.Core.Watchers;
using FolderSyncronizer.Debugging;
using System;
using System.Collections.Generic;
using System.IO;
using IFileSystemPubsub = FolderSyncronizer.Core.Data.IPubsub<FolderSyncronizer.Core.Data.FileWatcherArgs>;

namespace FolderSyncronizer.Core.Data
{
    public class FileWatcherCollection : IFileWatcherCollection
    {
        private readonly ModificationsContainer _modificationsContainer;

        private readonly IFileSystemPubsub _fileModificationPubsub;
        private readonly WatcherHandlerFactory _watcherHandlerFactory;
        private readonly List<FileWatcher> _fileWatchers;
        private readonly IFolderFilter _folderFilter;

        private bool _disposedValue;

        /// <summary>
        /// Triggered when a new file action is added.
        /// </summary>
        public event EventHandler<FileActionChangedEventArgs> NewFileAction;

        public FileWatcherCollection(ModificationsContainer container, IEnumerable<FolderSource> folderSources)
        {
            _modificationsContainer = container;
            _folderFilter = new SubFolderFilter();
            _fileWatchers = new List<FileWatcher>();

            _watcherHandlerFactory = new WatcherHandlerFactory();
            _fileModificationPubsub = new TaskBasedPubsub<FileWatcherArgs>(parallelism: 1);
            _fileModificationPubsub.Subscribe("FileWatcherCollection", NewFileChangedReceived);

            foreach (var folder in folderSources)
            {
                _folderFilter.AddSourceFolder(folder.Source);
                var watcher = new FileWatcher(_fileModificationPubsub, folder.Source, folder.Target);
                _fileWatchers.Add(watcher);
            }
        }

        private void NewFileChangedReceived(FileWatcherArgs fileWatcherArgs)
        {
            FileSystemEventArgs fileSystemEvent = fileWatcherArgs.FileEventsArgs;
            Singleton.Logger.LogDebug($"Change occured: {fileSystemEvent.ChangeType} for '{fileSystemEvent.FullPath}'");

            var handler = _watcherHandlerFactory.GetHandler(fileSystemEvent.ChangeType);
            if (handler == null)
            {
                // This should not happen, but just to be safe.
                Singleton.Logger.LogError($"Handler doesn't exist for change {fileSystemEvent.ChangeType}: '{fileSystemEvent.FullPath}'");
                return;
            }

            if (!_folderFilter.ShouldHandleFile(fileWatcherArgs.SourceDirectory, fileSystemEvent.FullPath))
            {
                Singleton.Logger.LogDebug($"Folder filter skipped '{fileSystemEvent.FullPath}'");
                // The file's folder is handled by a different file watcher.
                return;
            }

            var actions = handler.HandleChange(fileWatcherArgs.SourceDirectory, fileWatcherArgs.TargetDirectory, fileSystemEvent);
            foreach (var action in actions)
            {
                _modificationsContainer.Add(action);
                NewFileAction?.Invoke(this, new FileActionChangedEventArgs(action));
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    foreach (var watcher in _fileWatchers)
                    {
                        watcher.Dispose();
                    }

                    _fileWatchers.Clear();
                    _folderFilter.Clear();
                    _fileModificationPubsub.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
