import type {
    // CacheLimitationError,
    StorageItem,
    DataStorage,
} from "../api/storage";
import type { Client } from "discord.js";

export interface BasicDataStorage {
    setItem(key: string, value: StorageItem): Promise<void>;

    getItem(key: string): Promise<StorageItem | null | undefined>;

    getAllItemKeys(): Promise<string[]>;

    deleteItem(key: string): Promise<void>;
}

export interface FlushableDataStorage extends BasicDataStorage {
    // Stores state if the setting is lazy.
    flush(): Promise<void>;
}

export interface StorageInitParams {
    channelName: string;
    serverId: string;
    discordBotClient: Client;
}

export class BaseDatabase implements DataStorage {
    undelyingStorage: BasicDataStorage;
    name: string;
    constructor(name: string, undelyingStorage: BasicDataStorage) {
        this.undelyingStorage = undelyingStorage;
        this.name = name;
        // This is an ugly workaround for reasons beyond my understanding (this is no longer needed):
        /*
        this.cacheValue = this.cacheValue.bind(this);
        this.getCacheValue = this.getCacheValue.bind(this);
        this.getCacheEntries = this.getCacheEntries.bind(this);
        this.clearCacheKey = this.clearCacheKey.bind(this);
        this.getCachedItem = this.getCachedItem.bind(this);
        this.isExpired = this.isExpired.bind(this);
        */
    }

    async cacheValue(
        key: string,
        value: any,
        ttlSeconds?: number,
    ): Promise<void> {
        const item = <StorageItem>{
            key,
            value,
            ttlSeconds: ttlSeconds,
            entryTime: new Date().getTime(),
        };
        console.log(`${this.name}- Caching value: ` + JSON.stringify(item));
        await this.undelyingStorage.setItem(key, item);
    }

    async getCacheValue(key: string, defaultValue?: any): Promise<any> {
        const storageItem = await this.getCachedItem(key);
        if (!storageItem) {
            return defaultValue;
        }

        return storageItem.value;
    }

    async getCacheEntries(): Promise<StorageItem[]> {
        const allKeys = await this.undelyingStorage.getAllItemKeys();
        const items = await Promise.all(
            allKeys.map(async (key) => await this.getCachedItem(key)),
        );

        return items.filter((x) => x !== undefined) as StorageItem[];
    }

    // returns the previous key.
    async clearCacheKey(key: string): Promise<any> {
        const item = await this.getCachedItem(key);
        if (item) {
            console.log(`${this.name}- Clearing key from input: ` + key);
            return await this.clearCachedItem(item);
        }
        console.log(`${this.name}- Not clearing key. Not found: ` + key);
        return undefined;
    }

    /** Returns the storage item or undefined if non existent or expired. If item is expired, deletes it. */
    async getCachedItem(key: string): Promise<StorageItem | undefined> {
        console.log(`${this.name}- Get cache key: ` + key);
        const item = await this.undelyingStorage.getItem(key);
        if (item) {
            console.log(`${this.name}- Got cache key value: ` + JSON.stringify(item));
            // ""&& item.key" is to make up for bugs when creating items without keys.
            if (!this.isExpired(item) && item.key) {
                return item;
            } else {
                await this.clearCachedItem(item);
            }
        }
        return undefined;
    }

    async clearCachedItem(item: StorageItem) {
        console.log(`${this.name}- Clearing key: ` + item.key);
        await this.undelyingStorage.deleteItem(item.key);
        return item.value;
    }

    async flushState(): Promise<void> {
        if (this.undelyingStorage && "flush" in this.undelyingStorage) {
            const flushable = this.undelyingStorage as FlushableDataStorage;
            await flushable.flush();
        }
    }

    isExpired(item: StorageItem): boolean {
        if (!item.ttlSeconds) {
            return false;
        }

        const expirationTimeMilliseconds = item.entryTime + item.ttlSeconds * 1000;
        const nowMilliseconds = new Date().getTime();
        return nowMilliseconds > expirationTimeMilliseconds;
    }
}
