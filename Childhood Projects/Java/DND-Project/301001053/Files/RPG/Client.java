
import java.io.*;
import java.net.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;

//((InetSocketAddress)socket.getRemoteSocketAddress()).getHostName()

//InetAddress.getLocalHost().getHostName();

//at end: delete all "//del" (// del?)

//http://www.wizards.com/default.asp?x=dnd/glossary&term=Glossary_dnd_falling&alpha=

class Reading implements Runnable{
	/**
	 *This socket
	 */
  Socket socket;
  	/**
	 *The main menu
	 */
  Main main;

//static final int meg=5;
//static String server="MEG"+meg;
	
	/**
	 *The server to connect to
	 */
static String server=""; //local

//static final String server="YORAI";

//static final String server=((InetSocketAddress)socket.getRemoteSocketAddress()).getHostName();
	
	/**
	*Constructs a Reading- an helpful class for client
	*@param socket : the socket to deal with
	*@param m : the main menu
	*/
 public Reading(Socket socket,Main m){
   this.socket=socket;
   main=m;
    
 }
 	/**
	*Starts the read-write prosses with the server
	*/
 public void run(){
   try{
   
   	BufferedWriter out=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
   	
    BufferedReader in= new BufferedReader(new InputStreamReader(socket.getInputStream()));
    String SString;


	//System.out.println("socket: "+socket);
	
   synchronized(this)
   {
   
   while ((SString=in.readLine())!=null)
   {
   	
   	//System.out.println("s:");
   	//System.out.println(SString);
   	
   	//System.out.println("gets info...."+SString.substring(0,7));	
   	
   	//need to know if the one that attacked is the WF
   	//for that:
   	/*
   	in character and char "string attack": before the return,
   	send that he attacked. "ATTACK*(x)*ATTACK", x=number of the on attacked.
   	(do a function like the "one" in Main.CATTACK)
   	*/
  	
  	//System.out.println("getting info....");
  	
  	//System.out.println(SString.substring(0,5));
  	
   	if(SString.indexOf("M*")==0)
   	{
   	//System.out.println("ss:\n"+SString);          //Server says...	   		
   	//System.out.println("length: "+SString.length()); 
   	main.setMap(SString);
   	
   	//out.write(s+"\n");
   	//out.flush();
   	
   	//main.setmap- string
   	
   	}
   	 
   	//System.out.println(SString+" :string"); 
   	
   	
   	
   	if(SString.indexOf("HUNGER*")==0)
   	{
   		//System.out.println(SString); 
   	    String s=SString.substring(7);
        int x,y,ind,hun;
        int b=s.indexOf(",");
		x=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
        b=s.indexOf(",");
		y=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
   		b=s.indexOf(",");
		ind=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
   		b=s.indexOf(",");
		hun=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
   		
   		//System.out.println("b-same player (hun)");
   		
   		//System.out.println(main.getPlayer().getX()+","+main.getPlayer().getY());
   		
   		if(main.samePlayer(x,y,ind))
   		{
   		//	System.out.println("same player (hun)");
   		//	System.out.println("hung"); 
   			//System.out.println(main.getPlayer().hp()+","+main.getPlayer().getDam()+","+hun); 
   			
   			JOptionPane.showMessageDialog(main.getFrame(),"You are hungry.\nYou were damaged by "+hun+" points.","You need meals",JOptionPane.WARNING_MESSAGE);	
   			if(main.getPlayer().hp()-main.getPlayer().getDam()<=-5)
   			{
   				main.getMap().dead();
   				JOptionPane.showMessageDialog(main.getFrame(),"You died from hunger","Dead",JOptionPane.ERROR_MESSAGE);	
   			}
   			
   			if(main.getPlayer().isDying())
   			{
   			main.getMap().waitFC(true);
   			main.getMap().setDirs(false);
   			main.setMes("You are unconscious and are dying.");
   			}
   			
   			
   		}
   		
   		//System.out.println("a-same player (hun)");
   }
   	
   	if(SString.indexOf("CHAR*")==0)
   	{
   	    String s=SString.substring(5);
        int x,y,ind;
        int b=s.indexOf(",");
		x=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
        b=s.indexOf(",");
		y=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
   		b=s.indexOf(",");
		ind=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
   	
   		if(main.samePlayer(x,y,ind))
   		{
   		Place p=main.getPlace(y,x);	
   		
   		//System.out.println("same player (ch)");
   		
   		b=s.indexOf("*R");
        Character c=Character.readC(s.substring(0,b+1),p);
        s=s.substring(b+3);	   			
   		
   		main.setPlayer(c);
   		
   		
   		//System.out.println("ch "+c.getHp()+"-"+c.getDam());
   		}

  	}
   	
   	
   	if(SString.indexOf("PC*")==0)
    {
    	//System.out.println("nim!"); 
    	//System.out.println(SString); 
    	//System.out.println("ssss"); 
    	//System.out.println("recieving pc..."); 
        String s=SString.substring(3);
        int x,y;
        
       	//System.out.println("\nb: "+main.getPlayer().getSeen()+"\n"); 
        
        //System.out.println(SString); 
                
        int b=s.indexOf(",");
		x=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
        b=s.indexOf(",");
		y=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
    	
    	
    	   	
        b=s.indexOf("*P");
        //System.out.println(s.substring(0,b+1));
        Place p=Place.read(s.substring(0,b+1));
        s=s.substring(b+3);
                          	
        //here is only when someone enters?
        
        //p.writeSeen();
        
        
        //System.out.println(p.write());
        
        main.setPlace(y,x,p); //changed this function
        
        //System.out.println("movedas: "+main.getPlayer().getMoved());
        
        //System.out.println("\na: "+main.getPlayer().getSeen()+"\n");  // are those needed?
        
        //System.out.println("hiding: "+main.getPlayer().getHiding());
                       
        
        if(main.samePlace(x,y))
        {
        main.update();
        }
        
        
        //System.out.println("pc "+main.getPlayer().getHp()+"-"+main.getPlayer().getDam());
        
        //System.out.println("pseen: "+main.getPlayer().getSeen());
        
        //System.out.println("end pc");
        
   }
   	
   	if(SString.indexOf("UPDATE*")==0)
    {
    	
    	//System.out.println(SString);
    	
        String s=SString.substring(7);
        int x,y;
               	                          
        int b=s.indexOf(",");
		x=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
        b=s.indexOf(",");
		y=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
            
        b=s.indexOf("*P");
        Place p=Place.read(s.substring(0,b+1));
        s=s.substring(b+3);
                          	
                
        main.setUPlace(y,x,p); 
                              
        
        if(main.samePlace(x,y))
        {
        main.update();
        }
        
        
   }
   
   
   
   	if(SString.indexOf("PCC*")==0)
    {
    	//System.out.println("nim!"); 
    	//System.out.println(SString); 
    	//System.out.println("ssss"); 
    	//System.out.println("recieving pcc..."); 
        String s=SString.substring(4);
        int x,y,add;
        int b=s.indexOf(",");
		x=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
        b=s.indexOf(",");
		y=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
        b=s.indexOf(",");
		add=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
            
        b=s.indexOf("*P");
        Place p=Place.read(s.substring(0,b+1));
        s=s.substring(b+3);
        
        
        //System.out.println("p: "+p.write());
                          	
        main.setPlace(y,x,p,add);
    	
    	
    	if(main.samePlace(x,y))
        {
        main.update();
        }
    	    
   }
   
   /*
   if(SString.indexOf("ATTACK*")==0)
   {
   //System.out.println(SString);	
   
   String s=SString.substring(7);
        
   int x,y,ser;
   
    int b=s.indexOf(",");
    x=Integer.parseInt(s.substring(0,b));
    s=s.substring(b+1);	
	
	b=s.indexOf(",");
	y=Integer.parseInt(s.substring(0,b));;
	s=s.substring(b+1);	
   
    b=s.indexOf(",");
	ser=Integer.parseInt(s.substring(0,b));;
	s=s.substring(b+1);	
    
   
   //+add the thing on "attack" (battle)
     
   if(main.getPlayer().getX()==x&&main.getPlayer().getY()==y)
   {
  
   //System.out.println("works?");	
   
   int order=main.getMap().getOrder()-1;
   
   if(order<0)
   order=main.getMap().getFighters().size()-1;    //plz?
   
   
   
   System.out.println("order: "+order+"\nser: "+ser);
   
   System.out.println("fighters: "+main.getMap().getFighters());
   
       	
   if(order==ser) //or is it more? (maybe order is the next, it differs)
   {
   	
   	System.out.println("notifying...");
   	
   	synchronized(this)
   	{
   		System.out.println("...");
   		notifyAll();}
   }
   	
   }
      
   	
   }
   */
   
   			if(SString.indexOf("PICK*")==0)
            {
            //System.out.println(line);
            String s=SString.substring(5);
            int x,y,ind,orig;
            String mes;
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			ind=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			orig=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf(",");
			mes=s.substring(0,b);
			s=s.substring(b+1);		
            
            	if(main.samePlayer(x,y,ind))
            	{
            	int op=-1;
	            	if(orig==ind)
            		JOptionPane.showMessageDialog(main.getFrame(),mes,"Pick Pocket attemp...",JOptionPane.INFORMATION_MESSAGE);	            
            		else
            		op=JOptionPane.showConfirmDialog(main.getFrame(),mes,"Pick Pocket attemp...",JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE);  
            		
            		if(op==JOptionPane.YES_OPTION)
            		{
            		main.getMap().attackChar(orig);
            		}
            	

            	{
            		
            		
            	}
            	
            	}
            
  			}
  			
  			
   if(SString.equals("*main.battle(true)*"))
   main.battle(true,true);
   
   if(SString.indexOf("BATTLE*")==0)
   {
   	//System.out.println(SString);
   	
   	String s=SString.substring(7);
   	   	  	
   	int x,y;
   	String bt;
   	
   	int b=s.indexOf(",");
    x=Integer.parseInt(s.substring(0,b));
    s=s.substring(b+1);	
	
	b=s.indexOf(",");
	y=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);	
   	
   	b=s.indexOf("*BATTLE");
   	//System.out.println("battle:\nb- "+b+"\ns- "+s);
	bt=s.substring(0,b);
	s=s.substring(b+1);	
   	

   	//without the attacked, doesn't get the last feed.
   	
   	
   	//&&main.getPlayer().attacked()     [with or without? without will cause bugs?]
   	//may still be not ok... who cares...
   	//System.out.println("battle");
   	
   	
   	boolean ba=false;
   	if(main.samePlace(x,y)&&!main.getPlayer().isDead()&&!main.getBattle().getTip().equals("Conversation")) //need to work.
   	{
   		ba=true;
   		//System.out.println("battle2");
   	}
   	
   	
   	//System.out.println("\nbat:");
   	//if(!ba)
	//System.out.println("not supposed to be");

   	if(main.samePlace(x,y)&&main.getPlayer().isDead()&&!ba)
   	{
   	//check in battle or getmes if dead, if yes, don't send, else send (not dead completely)	
   		if(!main.getMes().equals("You are dead"))
   		ba=true;
   	}
   	 
   	
   	if(ba)
   	main.getMap().updateBattle(bt);
   		
   }
   
   			
   			
   			if(SString.indexOf("TALK*")==0)
  			{
            String s=SString.substring(5);
        
		    int x,y,ind;
		   	String mes;
		   	
		    int b=s.indexOf(",");
		    x=Integer.parseInt(s.substring(0,b));
		    s=s.substring(b+1);	
			
			b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			b=s.indexOf(",");
			ind=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			//System.out.println(s);
					
			b=s.indexOf("*TALK");
			mes=s.substring(0,b); //there's no ",".
			s=s.substring(b+1);	
			
   			//System.out.println(mes);
			
			if(main.samePlayer(x,y,ind))
			{
			main.getMap().talk(mes);			
			}
			
  			}
   
   			if(SString.indexOf("RUN*")==0)
            {
            String s=SString.substring(4);
            int x,y,pla;
            String mes;
            
            int b=s.indexOf(",");
			x=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
            
            b=s.indexOf(",");
			y=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);		
            
            b=s.indexOf(",");
			pla=Integer.parseInt(s.substring(0,b));
			s=s.substring(b+1);	
			
			b=s.indexOf(",");
			mes=s.substring(0,b);
			s=s.substring(b+1);	
			
			if(main.samePlayer(x,y,pla))
			JOptionPane.showMessageDialog(main.getFrame(),mes,"Running...",JOptionPane.INFORMATION_MESSAGE);	
						
			}
   
   if(SString.indexOf("ATTACK*")==0)
   {
   //System.out.println(SString);
   
   String s=SString.substring(7);
        
   int x,y,ser;
   
    int b=s.indexOf(",");
    x=Integer.parseInt(s.substring(0,b));
    s=s.substring(b+1);	
	
	b=s.indexOf(",");
	y=Integer.parseInt(s.substring(0,b));;
	s=s.substring(b+1);	
   
    b=s.indexOf(",");
	ser=Integer.parseInt(s.substring(0,b));;
	s=s.substring(b+1);	
             
   if(main.getPlayer().getX()==x&&main.getPlayer().getY()==y)
   {
        
   int porder=main.getMap().getPlayerOrder();
   
   main.ifAttacked();
   
   //System.out.println("x: "+x+", y: "+y);
   
   //System.out.println("waking in my place. fighters: "+main.getPlayer().getPlace().getFighters().size());
   
   if(porder==ser||(main.getPlayer().getPlace().getFighters().size()==0&&!main.getPlayer().isDead()))
   {
   	//System.out.println("waking me. "+x+","+y+","+porder);
   		if(!main.getPlayer().isDying())
   		{
   		main.getMap().waitFC(false);
   		main.unable(false);
   		main.setMes("Your turn.");
   		main.getMap().checkAttacks();
   		}
   		else
   		{
   		main.getMap().waitFC(true);
   		main.getMap().setDirs(false);
   		main.setMes("You are unconscious and are dying.");
   		}
   }
   else
   	if(main.getPlayer().attacked())
   	{
   		//System.out.println("attacked.");
   		main.getMap().waitFC(true);
   		main.setMes("Wait your turn.");
   	}
   //else
   //System.out.println("porder: "+porder+", ser: "+ser);
    
   }	
   
   }//end attack
   
   
   //System.out.println("waiting for another line...");
   
   if(main.getPlayer().isDying()&&!main.getPlayer().attacked()) //dying
   {
   	//put dirs and FC?
   	
   	Character player=main.getPlayer();
   	Place p=player.getPlace();
   	Vector pls=p.getCharacters();
   	
   	
  	if(pls.size()==1)
  	{
  		boolean dead=false;
  		
  		//hunger - forest
  		
  		if(!p.hasHealer())
  		{
  		dead=true;
  		}
  		else //healer heals
  		{
  		int heal=Dice.roll(2,6,2);
  		 	if(Dice.roll(20)+heal>=15)
  		 	{
  		 	player.stable(true);
  		 	int h=heal*2;
  		 	player.heal(h);
  		 	JOptionPane.showMessageDialog(main.getFrame(),heal+" has stabilized you and healed up to "+h+" damage.","Healed...",JOptionPane.INFORMATION_MESSAGE);	
  		 	main.getMap().useTurns(player.getSpeed());
  		 	}
  		 		
  		}
  		
  		if(dead)
  		{
  		player.dealDam(6);
  		int ind=p.removeChar(player);
		p.dropItems(player);
		main.sendCPlace(player.getY(),player.getX(),ind);
  		main.dead();
  		}
  		
  	}//checking if dead 	
   	else
   	{
   	main.getMap().waitFC(true);
   	main.getMap().setDirs(false);
   	main.setMes("You are unconscious and are dying.");
   	}
   	
   }
   
   
   
   } //end while (loop)
   
  } //end synch
   
  }catch(IOException e){
	  e.printStackTrace();
  }
  System.out.println("Disconnected from server");
  System.exit(0);
 }
}


//when disconnects- remove from map.


class Client implements Runnable{

	/**
	 *This socket
	 */
Socket socket;
	/**
	 *Reader in the client-server "conversation"
	 */
BufferedReader in;
	/**
	 *Write in the client-server "conversation"
	 */
BufferedWriter out;
	/**
	 *Main menu
	 */
static Main main;
	/**
	 *If connected to server
	 */
boolean connected;

	/**
	*Constructs a Client
	*/
public Client(){
	connected=false;
	final JFrame f=new JFrame();
	main=new Main(f,this);
	f.getContentPane().add(main);
	f.setSize(Main.dimx,Main.dimy);
	f.setVisible(true);
	
	//f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
	f.addWindowListener(new WindowListener(){
	
	
	public void windowClosing(WindowEvent e)
	{
	if(connected&&!main.getPlayer().attacked())
	sendMessage("exit"); //better than quit?
	
	if(!connected)	
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void windowClosed(WindowEvent e)
	{
		
	}
	
	public void windowIconified(WindowEvent e)
	{
		
	}
	
	public void windowDeiconified(WindowEvent e)
	{
		
	}
	
	public void windowDeactivated(WindowEvent e)
	{
		
	}
	
	public void windowActivated(WindowEvent e)
	{
		
	}
	
	public void windowOpened(WindowEvent e)
	{
		
	}
		
	});	
	
}



public void enterServer(String port,String host)
{
	try{ 
    
    if(host.equals("Local Host"))
    {
    	if(Reading.server=="")
    	{
		    try{
	    	Reading.server=InetAddress.getLocalHost().getHostName();
	    	}catch(UnknownHostException e){}
		}
	}
	else 
	Reading.server=host;
	
    socket=new Socket(Reading.server,Integer.parseInt(port));
    //socket=new Socket("YORAI",4000);
   
    
    out=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    in= new BufferedReader(new InputStreamReader(socket.getInputStream()));
    connected=true;
    
    //sendMessage("nfdsdf");
    
    }
  catch(IOException e){
     System.err.println("Couldn't get I/O for the connection to server.");
     System.exit(1);
  }
  
}


public boolean isConnected()
{
return connected;	
}

public void disconnect(String st,boolean playerDead)
{
connected=false;


/*try{
out.write(st+"\n");
out.flush();
out.write("quit\n");
out.flush();
}catch(IOException e){e.printStackTrace();}
*/
if(!playerDead)
sendMessage(st);

sendMessage("quit");

}

public void start(){
      Thread logRead=new Thread(this, "client");
      logRead.start();
}

public void run(){
 //try{
  //BufferedReader keyboard=new BufferedReader(new InputStreamReader(System.in));
  String SString;
  String UString;
  
  boolean tr=true;
  
  
   //System.out.println("nimm");
  
  while(tr)
  {
  	if(connected)
  	{
  	Reading readThread=new Reading(socket,main);
  	Thread read=new Thread(readThread,"read");
  	read.start();
  	boolean degel=true;
  	
  	tr=false;
  	}
  }
 /*  
 while (degel)
    {
      String line=keyboard.readLine();
      out.write(line+"\n");
      out.flush();
	}



}catch (IOException e){
       System.err.println("Couldn't read or write");
       System.exit(1);
       } 
 */ 
}

	/**
	*Sends a string to the server
	*@param messer : the string to send
	*/
public synchronized void sendMessage(final String messer){
      Thread send =new Thread(new Runnable(){
        public void run(){
			try{
		   //System.out.println("sends ["+messer.substring(0,4)+"] (client)....");	
		   
		   //System.out.println("message: "+messer);	
		   //System.out.println("sending message....");
		   out.write(messer+"\n");
           out.flush();
           }catch(IOException e){}
        }
      });
      send.start();
}


public static void main(String [] arg){
 Client t=new Client();
     t.start();
      
     
     //check:        
   /*  
     Place p=new Place("Town");
     
     p.tryAdd(new Char("Guardian",p),1);
     p.tryAdd(new Enemy("Skeleton",p),2);
     p.tryAdd(new Enemy("Orc",p),3);
     String s=p.write();
     System.out.println(s);
     
     Place p2=Place.read(s);
     
     String s2=p2.write();
     
     System.out.println("\n"+s2);
    
     if(s.equals(s2))
     System.out.println("\n\nOK");
     
     */
     
    /*
    
     Character d=new Character("Wilson",8,11,12,"Elf",true,10,13,14,9,"Paladin",new Skills(null,10,13,14,9,10,13,14,9,2,1));
     d.setWeapon(Weapon.create("Longsword",d));
     d.getSkills().setOwner(d);
     Place p=new Place("Town");
     d.setPlace(p);
     d.getSeen().add(p.getChars().elementAt(1));
     d.addMoved();
     d.addMoved();
     //System.out.println(d);
     String s=d.write();
     System.out.println(s);
     d=Character.readC(s,p);
     //System.out.println(d);
     s=d.write();
     System.out.println(s);
	
	*/

/*
	 Place p=new Place("Town");
	 Enemy d=new Enemy("Orc",p);
     d.getSeen().add(p.getChars().elementAt(1));
     //System.out.println(d);
     String s=d.write(),s2;

	 System.out.println(d.getItems());
		
	 System.out.println(s);
     d=Enemy.readE(s,p);
     
     //System.out.println("dsgfdsf: "+d.getInv());
     
     s2=d.write();
     System.out.println(s2);
	
	 if(s.equals(s2))
     System.out.println("true");
     else
     System.out.println("false");
	
*/

/*

	 Place d=new Place("Town");
     String s=d.write();
     System.out.println(s);
     
     
     Vector v=d.getChars();
     String cc="",cc2="";
     for(int i=0;i<v.size();i++)
     cc+=((Char)(v.elementAt(i))).write();
     //System.out.println("\n"+((Char)(d.getChars().elementAt(0))).write()+"\n");
          
     d=Place.read(s);
     String s2=d.write();
     System.out.println(s2);
     
     v=d.getChars();
     for(int i=0;i<v.size();i++)
     cc2+=((Char)(v.elementAt(i))).write();
     //System.out.println("\n"+((Char)(d.getChars().elementAt(0))).write()+"\n");
     
     
     System.out.println(cc);
     System.out.println(cc2);
     
     if(s.equals(s2))
     System.out.println("true");
     else
     System.out.println("false");
   

  	 */
  	  	 
  	 /*
  	 Character c=new Character("Wilson",8,11,12,"Elf",true,10,13,14,9,"Paladin",new Skills(null,10,13,14,9,10,13,14,9,2,1));
     c.setWeapon(Weapon.create("Longsword",c));
     c.getSkills().setOwner(c);
  
  	 Map d=new Map();
  	 
  	 d.setPlayer(c);
  	 //d.update(c);
  	 
  	 //d.nisui();
  	 
  	 System.out.println("s.");
  	 
     String s=d.write();
     System.out.println("ss: "+s);
     d.read(s);
     
     System.out.println("and: \n\n\n");
     
     String s2=d.write();
     System.out.println(s2);
     
     if(s.equals(s2))
     System.out.println("true");
     else
     System.out.println("false");
  
     System.out.println("s: "+s.length());
     System.out.println("s2: "+s2.length());
     */
     
     
     /*
     Item i=new Item("Tent");
  	String s=i.write();
  	System.out.println(s+"    "+i);
  	Item ii=Item.read(s);
  	System.out.println(ii.write()+"    "+ii);
     */
     
 }
}