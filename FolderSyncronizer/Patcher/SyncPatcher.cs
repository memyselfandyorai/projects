﻿using FolderSyncronizer.Debugging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Infra.Framework.Extensions;
using System.Linq;

namespace Patcher
{
    class SyncPatcher
    {
        public static void PatchFileCreation(string path)
        {
            foreach (var file in Directory.EnumerateFiles(path, "*", SearchOption.AllDirectories).Where(Program.IsImage))
            {
                var targetCreationTime = File.GetCreationTime(file);
                var targetModifiedTime = File.GetLastWriteTime(file);
                if (targetCreationTime > targetModifiedTime)
                {
                    // Fix issue with iPhone time: when moving files the creation time is "now", and then iPhone shows all photos at the same time.
                    File.SetCreationTime(file, targetModifiedTime);
                    File.SetLastWriteTime(file, targetCreationTime);
                    Singleton.Logger.LogDebug($"Fixed time of file {file}");
                    Console.WriteLine($"Fixed time of file {file}");
                }
            }
        }

        internal static void CopyMissedFiles(FolderSyncResult sourceResult, FolderSyncResult targetResult)
        {
            foreach (var filePathsPair in sourceResult.FileNamesToPathList)
            {
                string name = filePathsPair.Key;
                var paths = filePathsPair.Value;

                if(targetResult.FileNamesToPathList.ContainsKey(name))
                {
                    Singleton.Logger.LogDebug($"Existing: '{name}' - in '{targetResult.FileNamesToPathList[name].JoinToString("','")}'");
                    continue;
                }

                foreach (var sourcePath in paths)
                {
                    string relative = Path.GetRelativePath(sourceResult.RootPath, sourcePath);
                    string targetPath = Path.Combine(targetResult.RootPath, relative);
                    Singleton.Logger.LogDebug($"Copying: '{name}' - from '{sourcePath}' to '{targetPath}'");
                    File.Copy(sourcePath, targetPath, false);
                }
            }
        }
    }
}
