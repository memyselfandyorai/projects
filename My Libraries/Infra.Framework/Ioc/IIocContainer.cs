﻿using System;

namespace Infra.Framework.Ioc
{
    /// <summary>
    /// A container for Inversion of Control registrations.
    /// </summary>
    public interface IIocContainer : IDisposable
    {
        /// <summary>
        /// Registers the an implementation type to the interface.
        /// </summary>
        /// <typeparam name="TService">The base type or interface to register.</typeparam>
        /// <typeparam name="TConcrete">The type of implementation to register to. Should only have one public ctor.</typeparam>
        /// <param name="lifecycle">The lifecycle of the created object.</param>
        void Register<TService, TConcrete>(Lifecycle lifecycle = Lifecycle.Singleton)
            where TService : class
            where TConcrete : class, TService;

        /// <summary>
        /// Registers the instance as singleton to the interface.
        /// </summary>
        /// <typeparam name="TService">The base type or interface to register.</typeparam>
        /// <param name="instance">The instance to register.</param>
        void Register<TService>(TService instance)
            where TService : class;

        /// <summary>
        /// Registers the result of a creator function to as singleton to the interface.
        /// </summary>
        /// <typeparam name="TService">The base type or interface to register.</typeparam>
        /// <param name="creator">The function that creates the instance.</param>
        /// <param name="lifecycle">The lifecycle of the created object.</param>
        void Register<TService>(Func<TService> creator, Lifecycle lifecycle = Lifecycle.Singleton)
            where TService : class;

        /// <summary>
        /// Returns an instance of TService by its registration. Will throws exception if type is unregistered.
        /// </summary>
        /// <typeparam name="TService">Type to return</typeparam>
        /// <returns>A concrete instance of TService resolved by its registation</returns>
        TService Resolve<TService>()
            where TService : class;

    }

    /// <summary>
    /// Lifecycle of an implementation object: How long should it live throught a run?
    /// </summary>
    public enum Lifecycle
    {
        Singleton, // Creates one object and saves it. Its Dispose will be called when IIocContainer is disposed.
        Transient // Creates a new object on each Resolve call. Caller is responsible to dispose it.
    }
}