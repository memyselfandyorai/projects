﻿using FolderSyncronizer.Core.Watchers.Actions;
using FolderSyncronizer.Debugging;
using Infra.Framework.Extensions;
using Infra.Framework.Mef;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FolderSyncronizer.Core.Watchers.Handlers
{
    [ExportForMef(typeof(IWatcherHandler))]
    class NewFileHandler : IWatcherHandler
    {
        public WatcherChangeTypes SupportedType => WatcherChangeTypes.Created;

        // Note there's a bug when creating a new directory and then renaming it. Workaround: Create directory somewhere else
        // TODO: Solution - for created directories, I should add a "CreareDirectoryAction" susceptible to renames and deletions.
        public IEnumerable<IFileAction> HandleChange(string sourceFolder, string targetFolder, FileSystemEventArgs eventArgs)
        {
            string fullPath = eventArgs.FullPath;
            if (Directory.Exists(fullPath))
            {
                var files = Directory.GetFiles(fullPath, "*", SearchOption.AllDirectories);
                Singleton.Logger.LogDebug($"Sub files count for '{fullPath}': {files.Length}");
                return files.Select(path => HandleFile(sourceFolder, targetFolder, path));
            }

            return HandleFile(sourceFolder, targetFolder, fullPath).ToSingleton();
        }

        private static IFileAction HandleFile(string sourceFolder, string targetFolder, string fileInSource)
        {
            string relative = Path.GetRelativePath(sourceFolder, fileInSource);
            return new CopyAction(fileInSource, Path.Combine(targetFolder, relative), overwrite: false);
        }
    }
}
