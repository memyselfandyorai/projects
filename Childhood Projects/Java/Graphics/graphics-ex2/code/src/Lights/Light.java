package Lights;

import General.Color;
import General.NormalVector;
import General.Point;
/**
 * 
 * The class represents light
 *
 */
public abstract class Light {
	
	protected Color color; // I 0
	
	

	public Light()
	{
		color=new Color(1,1,1);
	}
	
	/**for debugging*/
	public Color getColor()
	{
		return color;
	}
	
	
	
	/**
	 * returns true iff this color has all needed values
	 * */
	public abstract boolean hasAllValues();
	
	
	/**
	 * returns I L
	 * 
	 * */
	public abstract Color getColor(Point hit);
	
	/**
	 * returns distance between light and hit-point
	 * 
	 * */
	public abstract double getDistance(Point hit);
		

	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * returns the direction FROM the hit-point TO this light 
	 * */
	public abstract NormalVector getDirection(Point hit);
	

}
