const controller = require("../../../../common/controller.js");
const {
    ReplyToCommand,
    ReceivedCommand,
} = require("../../../../common/commands.js");

module.exports = {
    async execute(commandInteraction) {
        await handleCommand(commandInteraction);
    },
};

async function handleCommand(commandInteraction) {
    await ReceivedCommand(commandInteraction);

    let inputOptions = parseInput(commandInteraction);

    let messageContent = await controller.RunSafe(() =>
        controller.RollCortexPrimeKeep(
            inputOptions,
            commandInteraction.getIocContainer(),
        ),
    );

    await ReplyToCommand(commandInteraction, messageContent);
}

// Returns input: {keep, cache_key}
function parseInput(commandInteraction) {
    var inputMap = commandInteraction.getInputOptionsMap();

    return {
        keep: inputMap.get("keep") ?? 3, // defaults to 3
        cache_key: controller.FormatChannelKey(commandInteraction),
    };
}

/*
Command:

const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "keep",
  "description": "Keeps another number of dice from the previous roll",
  "options": [
    {
      "type": 4,
      "name": "keep",
      "description": "Number of dice to keep",
      "required": true
    }
  ]
});

*/
