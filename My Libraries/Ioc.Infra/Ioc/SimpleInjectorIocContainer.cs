﻿using System;
using SimpleInjector;
using Ioc.Infra.Exceptions;

namespace Ioc.Infra.Ioc
{
    public class SimpleInjectorIocContainer : IIocContainer
    {
        private readonly Container _container;

        public SimpleInjectorIocContainer()
        {
            _container = new Container();
        }

        public void Register<TService, TConcrete>(Lifecycle lifecycle = Lifecycle.Singleton)
            where TService : class
            where TConcrete : class, TService
        {
            _container.Register<TService, TConcrete>(TransformLifestyle(lifecycle));
        }

        public void Register<TService>(TService instance)
            where TService : class
        {
            Register(() => instance, Lifecycle.Singleton);
        }

        public void Register<TService>(Func<TService> creator, Lifecycle lifecycle = Lifecycle.Singleton)
            where TService : class
        {
            _container.Register<TService>(creator, TransformLifestyle(lifecycle));
        }

        public TService Resolve<TService>() where TService : class
        {
            return (TService)_container.GetInstance<TService>();
        }

        private Lifestyle TransformLifestyle(Lifecycle lifecycle)
        {
            switch (lifecycle)
            {
                case Lifecycle.Singleton:
                    return Lifestyle.Singleton;
                case Lifecycle.Transient:
                    return Lifestyle.Transient;
                default:
                    throw new InvalidActionException("Lifecycle not mapped: " + lifecycle);
            }
        }

        public void Dispose()
        {
            _container.Dispose();
        }
    }

}