// Prod: https://discord.com/oauth2/authorize?client_id=947923931615088682&scope=identify%20bot%20applications.commands&permissions=2214669313
//
// https://discord.com/oauth2/authorize?client_id=947923931615088682&scope=identify%20bot%20applications.commands&permissions=2146958591
// https://discord.com/oauth2/authorize?client_id=947923931615088682&scope=identify%20bot%20applications.commands&permissions=2214660097
// https://discord.com/oauth2/authorize?client_id=947923931615088682&scope=identify%20bot%20applications.commands&permissions=2214661121

// 50001

module.exports = {
  async execute(prefixCommandInteraction, msgContent) {
    await handleCommand(prefixCommandInteraction, msgContent);
  },
};

async function handleCommand(prefixCommandInteraction, msgContent) {
  // (currently not needed) Requires GUILD_MEMBERS intent: https://discord.com/developers/applications/947923931615088682/bot

  // APIs: https://autocode.com/lib/discord/

  // https://discord.com/developers/docs/topics/permissions
  // CREATE_INSTANT_INVITE, SEND_MESSAGES, READ_MESSAGE_HISTORY, CHANGE_NICKNAME, VIEW_CHANNEL, USE_APPLICATION_COMMANDS, MANAGE_MESSAGES (mandatory)
  /*

// let info = await lib.discord.users["@0.1.4"].me.list();
let id = prefixCommandInteraction.botId();
  
  permissions =
    BigInt(0x1) |
    BigInt(0x400) |
    BigInt(0x800) |
    BigInt(0x10000) |
    BigInt(0x4000000) |
    BigInt(0x400) |
    BigInt(0x80000000) |
    BigInt(0x2000);

  let link =
    `https://discord.com/oauth2/authorize?client_id=${id}&scope=identify%20bot%20applications.commands&permissions=` +
    permissions;*/
  const link = prefixCommandInteraction.generateInvite();
  console.log(link);

  const options = {
    reply_to_message_id: prefixCommandInteraction.getMessageId(),

    /**
     * embed object ({title, type, color, ... })
     * probably like this: https://discord-api-types.dev/api/discord-api-types-v10/interface/APIEmbed.
     * passes as-is to the API.
     */
    embed: {
      type: "rich",
      title: `Invite link`,
      description: `Hey, I appreciate you inviting me to your server!`,
      color: 0x62ff00,
    },
    extra_data: {
      tts: false,
      components: [
        {
          type: 1,
          components: [
            {
              style: 5,
              label: `Invite Link`,
              url: link,
              disabled: false,
              type: 2,
            },
          ],
        },
      ],
    },
  };

  await prefixCommandInteraction.send({ content: "" }, options);
}
