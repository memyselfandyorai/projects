﻿using System.Collections.Generic;
using System.IO;

namespace FolderSyncronizer.Core.Watchers
{
    interface IWatcherHandler
    {
        WatcherChangeTypes SupportedType { get; }

        /// <summary>
        /// Returns empty if action is not supported.
        /// </summary>
        /// <param name="sourceFolder"></param>
        /// <param name="targetFolder"></param>
        /// <param name="eventArgs"></param>
        /// <returns></returns>
        IEnumerable<IFileAction> HandleChange(string sourceFolder, string targetFolder, FileSystemEventArgs eventArgs);
    }
}
