
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.io.*;


public class Place implements Serializable {
	
	/**
 	*Name (type) of the place
 	*/
	private String name;
	/**
 	*Difficulty class of skill uses in this place
 	*/
	private int DC; //difficulty class
	/**
 	*Items in this place
 	*/
	private Vector items; //items are quite hidden
	/**
 	*Characters in this place
 	*/
	private Vector chars;
	/**
 	*Money in this place
 	*/
	private double gp;
	/**
 	*Order of fight
 	*/
	private int order;
	/**
 	*Fighters (by order) and their initiation score
 	*/
	private Vector fighters,nums;
	//private Color color;
	/**
 	*Fighters in the surprize round
 	*/
	private Vector surprize;
	
	//add vector "surprize", when someone attacks, then he's removed.
	
	public Place(String n)
	{
		name=n; //town(orange?), village(brown), cave(black),forest(green), mountain(red), river\lake(blue), mines (yellow?)
		
		//color=new Color(5,100,200);
		
		surprize=new Vector();
		
		order=0;
		fighters=new Vector();
		nums=new Vector();
		
		gp=Dice.roll(30)+Dice.roll(2,6)-15;
		if(gp<0)
		gp=0;
		
		int p=Dice.roll(100),c=0;
		if(p<7)
		c=1;
		if(p>=7&&p<25)
		c=2;
		if(p>=25&&p<65)
		c=3;
		if(p>=65&&p<93)
		c=4;
		if(p>=93)
		c=5;
				
		DC=c*5;
		
		items=new Vector();
		chars=new Vector();
		//items, enemies and NPCs:
					
		if(n.equals("Town"))
		{
			//color=Color.cyan; //change
			
			if(c==1)
			DC+=4;
			if(c==2)
			DC+=2;
			if(c>2)
			DC-=3;
			
			int gua=Dice.roll(100)-20;
			gua=gua/30;
			for(int i=0;i<gua;i++)
			chars.add(new Char("Guardian",this));
			
			if(Dice.roll(2)==1)
			chars.add(new Merchant("Blacksmith",Dice.roll(5),this));
			if(Dice.roll(2)==1)
			chars.add(new Merchant("Merchant",Dice.roll(5),this));
			//which merchant, also random. (merchant, brewer(?), blacksmith) two of the, or 3 here?
			
			if(Dice.roll(5)>2)
			chars.add(new Char("Wizard",this));
			
			if(Dice.roll(2)==1)
			for(int i=0;i<2;i++)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=4)
			race="Human";
			if(r>=5&&r<=6)
			race="Elven";
			if(r>=7&&r<=9)
			race="Dwarven";
			if(r==10)
			race="Halfling";
			
			chars.add(new Char(race+" Civilian",this));
			}
			
			if(Dice.roll(100)>80)
			{
				chars.add(new Char("Royal Guard",this));
				if(Dice.roll(100)>65)
				{
					chars.add(new Char("Royal Guard",this));
					chars.add(new Char("Royalty",this));
				}
			}
			
			int nob=Dice.roll(3)-1;
			for(int i=0;i<nob;i++)
			chars.add(new Char("Noble",this));
			
			if(Dice.roll(3)>1)
			chars.add(new Char("Healer",this));
			
			//vampire,zombie
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Vampire",this));		
			if(en>80&&en<95)
			chars.add(new Enemy("Zombie",this));		
					
			
			int it=Dice.roll(100);
			if(it>90)
			items.add(Item.getRing());
			if(it>80&&it<90)
			items.add(Weapon.getWeaponM());
			if(it>65&&it<80)
			items.add(Shield.getShield());
			if(it>50&&it<70)
			items.add(Weapon.getWeaponS());
			if(it>45&&it<52)
			items.add(Armor.getArmor());
			if(it<25)
			items.add(Item.getItemT());
						
			//chars.add(new Enemy("Skeleton",this)); thief?												
			
			gp=gp+c*5;
			
					
		}
		
		
		if(n.equals("Village"))
		{
			//color=Color.cyan; //change
			if(c>2)
			DC-=4;
			
			int gua=Dice.roll(100);
			gua=gua/50;
			for(int i=0;i<gua;i++)
			chars.add(new Char("Guardian",this));
			
			if(Dice.roll(3)!=1)
			chars.add(new Merchant("Merchant",Dice.roll(4),this));
			//which merchant, also random. (merchant, brewer(?), blacksmith)
			
			if(Dice.roll(3)==1)
			chars.add(new Char("Wizard",this));
						
			int civ=Dice.roll(4);
			
			if(Dice.roll(2)==1)
			for(int i=0;i<civ;i++)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=4)
			race="Human";
			if(r>=5&&r<=6)
			race="Elven";
			if(r>=7&&r<=9)
			race="Dwarven";
			if(r==10)
			race="Halfling";
			
			chars.add(new Char(race+" Civilian",this));
			}
						
			if(Dice.roll(2)==1)
			chars.add(new Char("Healer",this));
			
			//snake,wraith
			int en=Dice.roll(100);
			if(en>92)
			chars.add(new Enemy("Wraith",this));		
			if(en>83&&en<92)
			chars.add(new Enemy("Snake",this));		
					
							
			int it=Dice.roll(100);
			if(it>90)
			items.add(Weapon.getWeaponM());
			if(it>75&&it<90)
			items.add(Shield.getShield());
			if(it>60&&it<80)
			items.add(Weapon.getWeaponS());
			if(it>55&&it<62)
			items.add(Armor.getArmor());
			if(it<45)
				for(int i=0;i<it/5+1;i++)
				items.add(Item.getItemV());
						
			//chars.add(new Enemy("Skeleton",this)); thief?												
			
			gp=gp+c*2;
			
					
		}
		
		if(n.equals("Mines"))
		{
			//color=Color.cyan; //change
			if(c<5)
			DC+=3;
			
			if(Dice.roll(100)>80)
			chars.add(new Char("Guardian",this));
			
			if(Dice.roll(4)==1)
			chars.add(new Merchant("Blacksmith",Dice.roll(4),this));
			if(Dice.roll(3)==1)
			chars.add(new Merchant("Merchant",Dice.roll(5),this));
			//which merchant, also random. (merchant, brewer(?), blacksmith)//change
			
			
			if(Dice.roll(3)==1)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=4)
			race="Human";
			if(r>=5&&r<=9)
			race="Dwarven";
			if(r==10)
			race="Halfling";
			
			chars.add(new Char(race+" Civilian",this));
			}
									
			int it=Dice.roll(100);
			if(it>90)
			items.add(Weapon.getWeaponM());
			if(it>70&&it<90)
			items.add(Shield.getShield());
			if(it>55&&it<80)
			items.add(Weapon.getWeaponS());
			if(it>48&&it<58)
			items.add(Armor.getArmor());
			if(it<26)
				for(int i=0;i<it/5+2;i++)
				items.add(Item.getItemM());
			
			if(Dice.roll(4)==1)
			chars.add(new Char("Healer",this));
			
			//ogre,orc,goblin,gnome,gnoll
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Ogre",this));		
			if(en>70&&en<90)
			chars.add(new Enemy("Orc",this));		
			if(en>60&&en<80)
			chars.add(new Enemy("Goblin",this));
			if(en>50&&en<65)
			chars.add(new Enemy("Gnome",this));
			if(en>45&&en<60)
			chars.add(new Enemy("Gnoll",this));		
			if(en>35&&en<45)
			chars.add(new Enemy("Imp",this));		
			
			gp=gp+DC*2;
							
		}
			
			
		if(n.equals("Cave"))
		{
			//color=Color.cyan; //change
			if(c<5)
			DC+=4;
					
			if(Dice.roll(4)==1)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=4)
			race="Human";
			if(r>=5&&r<=10)
			race="Dwarven";
					
			chars.add(new Char(race+" Civilian",this));
			}
			
			if(Dice.roll(4)==1)
			chars.add(new Char("Wizard",this));
								
			int it=Dice.roll(100);
			
			if(it>90)
			items.add(Armor.getArmor());
			if(it>70&&it<90)
			items.add(Shield.getShield());
			if(it>55&&it<75)
			items.add(Weapon.getWeaponS());
			if(it<25)
				for(int i=0;i<it/5+1;i++)
				items.add(Item.getItemC());
			
			if(Dice.roll(5)==1)
			chars.add(new Char("Healer",this));
			
			//ogre,ogre mage,skeleton,ghoul,osyluth,quasit
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Ogre Mage",this));		
			if(en>78&&en<90)
			chars.add(new Enemy("Ogre",this));		
			if(en>60&&en<80)
				for(int i=0;i<(en-55)/5;i++)
				chars.add(new Enemy("Skeleton",this));
			if(en>50&&en<65)
				for(int i=0;i<(en-45)/6;i++)
				chars.add(new Enemy("Ghoul",this));
			if(en>10&&en<23)
			chars.add(new Enemy("Quasit",this));		
			if(en<10)
			chars.add(new Enemy("Osyluth",this));		
			
			gp=gp-c*2;
								
		}	
		
		if(n.equals("River"))
		{
			//color=Color.cyan; //change
			if(c<3)
			DC+=3;
			if(c>3)
			DC-=3;
					
			if(Dice.roll(2)==1)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=2)
			race="Human";
			if(r==3)
			race="Dwarven";
			if(r>=4&&r<=8)
			race="Halfling";
			if(r>=9&&r<=10)
			race="Elven";
					
			chars.add(new Char(race+" Civilian",this));
			}
			
			if(Dice.roll(5)>3)
			chars.add(new Char("Wizard",this));
			
			if(Dice.roll(2)==1)
			chars.add(new Merchant("Merchant",Dice.roll(5),this));
			if(Dice.roll(5)==1)
			chars.add(new Merchant("Blacksmith",Dice.roll(5),this));
			
			int it=Dice.roll(100);
			
			if(it>95)
			items.add(Armor.getArmor());
			if(it>85&&it<95)
			items.add(Shield.getShield());
			if(it>75&&it<90)
			items.add(Weapon.getWeaponS());
			if(it<40)
				for(int i=0;i<it/5;i++)
				items.add(Item.getItemR());
			
			if(Dice.roll(3)==1)
			chars.add(new Char("Healer",this));
			
			//ogre,orc+archer,goblin,dretch
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Dretch",this));		
			if(en>75&&en<90)
			chars.add(new Enemy("Ogre",this));		
			if(en>55&&en<75)
			chars.add(new Enemy("Orc Archer",this));
			if(en>45&&en<65)
			chars.add(new Enemy("Orc",this));
			if(en>40&&en<50)
			chars.add(new Enemy("Goblin",this));		
			if(en>35&&en<40)
			chars.add(new Enemy("Wyvern",this));
			
			gp=gp-c*3;
								
		}
		
		if(n.equals("Mountain"))
		{
			//orange
			if(c<4)
			DC+=4;
			
			
			if(Dice.roll(3)==1)
			chars.add(new Merchant("Blacksmith",Dice.roll(3),this));
			if(Dice.roll(4)==1)
			chars.add(new Merchant("Merchant",Dice.roll(5),this));
			//which merchant, also random. (merchant, brewer(?), blacksmith)//change
			
			if(Dice.roll(5)==1)
			chars.add(new Char("Wizard",this));
										
			int it=Dice.roll(100);
			
			if(it>95)
			items.add(Armor.getArmor());
			if(it>85&&it<95)
			items.add(Shield.getShield());
			if(it>75&&it<90)
			items.add(Weapon.getWeaponS());
			if(it<20)
				for(int i=0;i<it/5+1;i++)
				items.add(Item.getItemN());
				
			//orc archer,goblin,gnoll,red+black dragon
			int en=Dice.roll(100);
			if(en>95)
			chars.add(new Enemy("Red Dragon",this));		
			if(en>75&&en<90)
			chars.add(new Enemy("Gnoll",this));		
			if(en>55&&en<75)
			chars.add(new Enemy("Goblin",this));
			if(en>45&&en<65)
			chars.add(new Enemy("Orc Archer",this));
			if(en>40&&en<45)
			chars.add(new Enemy("Black Dragon",this));		
			
			gp=gp-c*4;
								
		}
		
		if(n.equals("Forest"))
		{
			//green
								
			if(Dice.roll(2)==1)
			{
			int civ=Dice.roll(3);
				for(int i=0;i<civ;i++)
				{
				int r=Dice.roll(10);
				String race="";
				if(r>=1&&r<=2)
				race="Human";
				if(r==3)
				race="Dwarven";
				if(r>=4&&r<=8)
				race="Elven";
				if(r>=9&&r<=10)
				race="Halfling";
						
				chars.add(new Char(race+" Civilian",this));
				}
			}
			
			
			if(Dice.roll(5)>3)
			chars.add(new Char("Wizard",this));
			
			if(Dice.roll(4)==1)
			chars.add(new Merchant("Merchant",Dice.roll(3),this));
			
									
			int it=Dice.roll(100);
			
			if(it>90)
			items.add(Weapon.getWeaponM());
			if(it>80&&it<95)
			items.add(Shield.getShield());
			if(it>70&&it<85)
			items.add(Weapon.getWeaponS());
			if(it<30)
				for(int i=0;i<it/7+1;i++)
				items.add(Item.getItemF());
			
			if(Dice.roll(5)>3)
			chars.add(new Char("Healer",this));
			
			//orc+archer,lemure,wolf,ghoul,skeleton
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Ghoul",this));		
			if(en>80&&en<95)
			chars.add(new Enemy("Skeleton",this));		
			if(en>60&&en<80)
			chars.add(new Enemy("Orc",this));
			if(en>50&&en<70)
			chars.add(new Enemy("Orc Archer",this));
			if(en>35&&en<50)
			chars.add(new Enemy("Lemure",this));		
			if(en>25&&en<40)
			chars.add(new Enemy("Wolf",this));
								
		}
		
	if(gp<0)
	gp=0;
	
	gp/=10;
	
	if(DC>17)
	if(Dice.roll(3)>1)
	DC-=5;
	
	}
	
	
	public void addItem(Item item) //+do removeItem
	{
	boolean b=true;
	
	for(int i=0;i<items.size();i++)
	{
	Item m=(Item)(items.elementAt(i));
		if(item.getName().equals(m.getName()))
		{
			m.setAmount(m.getAmount()+item.getAmount());
			b=false;
		}
	}
	
	if(b)
	items.add(item);
	
	}
	
	public void removeItem(String n,int m)
	{
	for(int i=0;i<items.size();i++)
	{
		Item t=(Item)(items.elementAt(i));
		if(n.equals(t.getName()))	
		{
			t.setAmount(t.getAmount()-m);
			if(t.getAmount()<=0)
			items.remove(t);
		}
	}
	
	}
	
	public void removeItem(Item m)
	{
	items.remove(m);	
	}
	
	
	public double gp()
	{
		return gp;
	}
	
	
	public int dc()
	{
		return DC;
	}
	
	public Color getColor()
	{
		//http://www.pitt.edu/~nisg/cis/web/cgi/rgb.html
		
		if(name.equals("Town"))
		return new Color(255,250,205);
		
		if(name.equals("Village"))
		return Color.orange;
				
		if(name.equals("Mines"))
		return Color.yellow;//new Color(240,255,255);
				
		if(name.equals("Cave"))
		return Color.black;
		
		if(name.equals("River"))
		return Color.blue;
		
		if(name.equals("Mountain"))
		return Color.red;
		
		//if(name.equals("Forest"))
		return Color.green; //forest
		
		//return new Color(50,200,100);
	}
	
	
	
	/*
	public String getRGB()
	{
	
	return "rgb("+color.getRed()+","+color.getGreen()+","+color.getBlue()+")";
	}
	*/
	
	public void addSur(Char c) //to surprize
	{
		surprize.add(c);
	}
	
	
	public String getName()
	{
		return name;
	}
	
	
	public static int move(Character c,Place m[][],int x,int y) //data is correct.
	{
		//int d=Map.dim; 
				
		int xx=c.getX(),yy=c.getY();
		
		int add=m[yy][xx].chars.indexOf(c);
		
		m[yy][xx].chars.remove(c);
		m[yy+y][xx+x].chars.add(c);
		
		c.move(x,y,m[c.getY()+y][c.getX()+x]);
		
		//Place cp=m[c.getY()][c.getX()];
		
		
		
		
		
		//more options on move()?
		
		return add;
	}

	
		
	public static int movens(Character c,Place m[][],int x,int y) //data is correct.
	{
		//no seen
				
		int xx=c.getX(),yy=c.getY();
		
		int add=m[yy][xx].chars.indexOf(c);
		
		m[yy][xx].chars.remove(c);
		m[yy+y][xx+x].chars.add(c);
		
		c.movens(x,y,m[c.getY()+y][c.getX()+x]);
		
		//more options on move()?
		
		return add;
	}
	
	
	public int removeChar(Char c)
	{
	int ind=chars.indexOf(c);
	chars.remove(c);
	for(int i=0;i<chars.size();i++)
	{
	Char r=(Char)(chars.elementAt(i));	
	//if(r instanceof Character)
	//((Character)(r)).getSeen().remove(c);
	r.getSeen().remove(c);
	}
	return ind;
	}
	
	
	public void updateSeen() //some disappear, so needs to update
	{
		for(int i=0;i<chars.size();i++) //all chars in place
		{
		Char r=(Char)(chars.elementAt(i));	
		Vector s=r.getSeen(); //char's seen list
			for(int j=0;j<s.size();j++) //all char's seen
			{
				Char c=(Char)(s.elementAt(j));
				if(chars.indexOf(c)==-1)
				{
					////System.out.println("update seen.");
					s.remove(c);
					//if(c!=null)
					////System.out.println("him: "+r.write()+"\n\nremoves him: "+c.write()+"\n\nthis place: "+this.write());
				}
			}
		}
	}
	
	
	public Vector getEnemies()
	{
		Vector v=new Vector();
		for(int i=0;i<chars.size();i++)
		{
			Char c=(Char)(chars.elementAt(i));
			if(c instanceof Enemy)
			v.add(c);
		}
	return v;
	}
	
	public Vector getCharacters()
	{
		Vector v=new Vector();
		for(int i=0;i<chars.size();i++)
		{
			Char c=(Char)(chars.elementAt(i));
			if(c instanceof Character)
			v.add(c);
		}
	return v;
	}
	
	
	public Vector getFighters()
	{
		return fighters;
	}
	
	public int getOrder()
	{
		return order;
	}
	
	public Vector getNums()
	{
		return nums;
	}
		
	public void tryAdd(Char c,int b)
	{
		chars.add(c);
		fighters.add(c);
		nums.add(b+"");
	}
	
	public Vector getGuardians()
	{
		Vector v=new Vector();
		for(int i=0;i<chars.size();i++)
		{
			Char c=(Char)(chars.elementAt(i));
			if(c.getClass().toString().equals("class Char"))
			if(c.name().equals("Guardian")||c.name().equals("Royal Guard"))
			v.add(c);
		}
	return v;
	}
	
	/*
	public void sendCAttack(Char c)
	{
		int ind=chars.indexOf(c);
					
		String s="ATTACK*";
		s=s+Main.map.getI(this)+",";
		s=s+Main.map.getJ(this)+",";
		s=s+ind+",";
		Main.sendMessage(s+"*ATTACK");
		
		//sends only to those who are in this place.
		
	}
	*/
	
	
	/*
	public void change(int ind,Server server)
	{
		Character player=(Character)(chars.elementAt(ind));
		
		
		boolean b=true;
		
		
		Vector enms=getEnemies();
			
		Vector v=new Vector();
		Vector num=new Vector();
		Vector surprise=new Vector(); //if not seen.
		
		
		Attack battle=new Attack();
		
		//change it if spotting one another is important
		for(int i=0;i<enms.size();i++) //spots, and surprises.
		{
			Char no=(Char)(enms.elementAt(i));
			//no.spot(enms);
			no.spot(chars);
			if(no.seen(player)&&Char.npc(no))
			{
			////System.out.println("saw: "+no);
			
			////System.out.println("nim? "+no.seen(player));
			
				if((!player.seen(no))&&!(player.getWeapon().isRanged()))
				surprise.add(no);
				
				//else
				player.addSeen(no);
			
			if(v.indexOf(player)==-1)
			v.add(player);
		
			v.add(no);
								
			}
		}
		
			if(v.size()!=0) //fighting.     //to try and run?
			{
			fighters=Char.initiation(v,num); //check this after few attack. with "for"?
			
			server.sendMessage("*main.battle(true)*");
			
			order=0;
			int d=fighters.indexOf(player);
					
					if(surprise.size()!=0) //surprise attack
					{
						roundCons(d,battle,player,server,surprise);
					}
					
					/*for(int i=0;i<surprise.size();i++)
					{
					Char current=(Char)(surprise.elementAt(i));
					battle.addText("(surprise) "+current.attack(player));//thread, sleep
					////System.out.println("attacker (S): "+current+" , order: "+i+"  , size: "+surprise.size());
					player.addSeen(current);
					checkLife();
					}
										
					roundCon(d,battle,player,server);
					
			}
		
			
	}
	
	
	
	
	
	public void roundCon(final int dd,final Attack bat,final Character pl,final Server server)//round continue
	{
	final Place plthis=this;
	Thread tr=new Thread(new Runnable(){
	
	int d=dd;
		
	Attack battle=bat;
	
	Character player=pl;
	
	public void run()
	{   
	
		d=fighters.indexOf(player);
		
		//System.out.println("run-con\nd: "+d+"   ,   order: "+order);
		
		//System.out.println("fighters: "+fighters);
		
		//while(order!=d&&d>=0)
		while(true)
		{
		Char current=(Char)(fighters.elementAt(order));
		battle.addText(current.attack(player));//thread, sleep
										
		d=fighters.indexOf(player);
		
		order++;
		if(order>=fighters.size())
		order=0;
		
		//System.out.println("d: "+d+" , order: "+order);
		
		if(current instanceof Character)
		{
		
		//System.out.println("character");
		
		checkLife();
		
		//wake(plthis,d,battle,server); //not needed, it's outside the while
		
		
		break;
		//break? (check with alla)
		}
		
		
		
			
		}
					
	checkLife();
	
	if(fighters!=null)
		if(fighters.size()==1)
		{
			//main.battle(false);
			
			checkLife();
			//fighters=null;
			fighters=new Vector();
			player.attacked(false);
			order=0;
			player.attacks(0);
		}
	
	
	wake(plthis,d,battle,server);
		
	
	//a check for- if there are character left. (if not, stop.) [istn't  it the "fighter.size==1"?]
	
	
	}	
		
	});	
	
	tr.start();
		
	}
	
	
	
	public void roundCons(final int dd,final Attack bat,final Character pl,final Server server,final Vector surp)//round continue+surprize
	{
	final Place plthis=this;
	Thread tr=new Thread(new Runnable(){
	
	int d=dd;
			
	Attack battle=bat;
	
	Character player=pl;
	
	public void run()
	{   
	
	Vector n_fighters=fighters;
	fighters=surp;
	
		d=fighters.indexOf(player);
		
		//System.out.println("run-con\nd: "+d+"   ,   order: "+order);
		
		//System.out.println("fighters: "+fighters);
		
		//while(order!=d&&d>=0)
		while(true)
		{
		Char current=(Char)(fighters.elementAt(order));
		battle.addText(current.attack(player));//thread, sleep
										
		d=fighters.indexOf(player);
		
		order++;
		if(order>=fighters.size())
		order=0;
		
		//System.out.println("d: "+d+" , order: "+order);
		
		if(current instanceof Character)
		{
		
		//System.out.println("character");
		
		checkLife();
		
		//wake(plthis,d,battle,server); //not needed, it's outside the while
		
		
		break;
		//break? (check with alla)
		}
		
		
		
			
		}
					
	checkLife();

	fighters=n_fighters;

	wake(plthis,d,battle,server);
		
	
	//a check for- if there are character left. (if not, stop.) [istn't  it the "fighter.size==1"?]
	
	}	
		
	});	
	
	tr.start();
		
	}
	
	
	public void wake(Place plthis,int d,Attack battle,Server server)
	{
		//what if "add"? (PCC?)
		server.sendPlace(plthis);
		server.sendAttack(d,plthis); //send awaking
		//send battle:
		String bf=battle.convert();//battle feedback 
		server.sendBattle(bf,plthis);
		
		
		//break?
		
	}
*/
	public static String getXY(Place[][] map,Place p)
	{
	for(int i=0;i<Map.dim;i++)
		for(int j=0;j<Map.dim;j++)
		if(map[i][j]==p)
		return j+","+i+",";
		
		return "";	
	}

	
	public Vector getItems()
	{
	return items;	
	}
	
	public Vector getChars()
	{
	return chars;	
	}
	
	public Char getChar(int a)
	{
		if(chars.size()>a)
		return (Char)(chars.elementAt(a));
		
	return null;
	}
	
	public void addMoney(double a)
	{
	gp=gp+a;	
	}
	
	public int getOrder(Char c)
	{
		if(fighters!=null)
		return fighters.indexOf(c);
		
		return -1;
	}
	
	public boolean hasHealer()
	{
	for(int i=0;i<chars.size();i++)
	{
	Char c=(Char)chars.elementAt(i);
	if(c.getClass().toString().equals("class Char"))
	if(c.name().equals("Healer"))
	return true;
	}
	return false;
	}
	
	
	public void addFighter(Char c)
	{
	c.attacked(true);
	
	int m=c.initiative();
	
	boolean b=true;

		for(int j=0;j<nums.size()&&b;j++)
		{
			int g=Integer.parseInt((String)(nums.elementAt(j)));
			if(m>g)
			{
			fighters.add(j,c);
			b=false;
			}
			
			if(m==g)
			{
				int d=((Char)(fighters.elementAt(j))).getDEX();
				int cd=c.getDEX();
				if(cd>d)
				{
				fighters.add(j,c);
				b=false;
				}
				else
					if(cd==d)
						if(Dice.roll(2)==1)
						{
						fighters.add(j,c);
						b=false;
						}	
			}
		}
		
	if(b)
	{
	fighters.add(c);
	}
	
	nums.add(fighters.indexOf(c),""+m); //bug if inserted last or fifth?
	
	}
	
	
	public void checkLife()
	{
	for(int i=0;i<fighters.size();i++)	
	{
	Char c=(Char)(fighters.elementAt(i));
	
		if(c.isDead()) //later
		{
	
			fighters.remove(c);	
			removeChar(c);
			//list.remove(c);
			
			//drop items, get XP (if this player only, or all?)
		}
	}
	
	if(order>=fighters.size())
	order=0;
	
	}
	
	
	
	/*
	public void battle(int serP,int lsvE,Server server) //serial player, selected enemy
	{
		
		Attack battle=new Attack();
				
		Character player=(Character)(chars.elementAt(serP));
                        
        Char sel=(Char)(chars.elementAt(lsvE));//selected value
        
        //needs to do this totally, not only in "while"
        //System.out.println("battle");
		
            //update these later?
            
            		if(fighters.size()==0) //the fighting begins
					{		
					////System.out.println("nim");
					
					
					//don't fit here, update later if needed
					//(dfh)
					//JComponent del=battle;
					//battle=new Attack();
					//main.addEast(battle,del);
					
					Vector v=new Vector();
					v.add(player);
					v.add(sel);
					
					nums=new Vector();
					
					////System.out.println("v: "+v+"\n");
					fighters=Char.initiation(v,nums); //check this after few attack. with "for"?
					
					//main.battle(true); (dfh)
					
					////System.out.println("order: "+fighters);
					////System.out.println("nums: "+nums);
					
					
					Char current=(Char)(fighters.elementAt(order)),enm;		
						
					//attacks(false);
					
					//if emeny has ranged etc.
					
					boolean style; //player first=true
					
					enm=sel;
											
					if(player==current) //player first
					{
					if(player.getWeapon().isRanged()||!enm.getWeapon().isRanged())
					style=true;
					else
					style=false;
					}
					else
					{
					if(enm.getWeapon().isRanged()||!player.getWeapon().isRanged())
					style=false;
					else
					style=true;
					}
				
				
				if(enm.seen(player))				
				{
					if(style) //player first
					{
					battle.addText(player.attack(enm));
					checkLife();
						if(!enm.isDead()&&player.attacks()>=player.NoA())
						{
						battle.addText(enm.attack(player));
						checkLife();
						}
					}
					else //enm first
					{
					battle.addText(enm.attack(player));
					checkLife();
					if(!player.isDead())
					{
					battle.addText(player.attack(enm));
					checkLife();
					}
						if(!enm.isDead()&&player.attacks()>=player.NoA())
						{
						battle.addText(enm.attack(player));
						checkLife();
						}
					order++;
					if(order>=fighters.size())
						order=0;
					}
				}
				else
				{
					battle.addText("(surprise) "+player.attack(enm));
					checkLife();
				}
					//order=0
				
					//player.attacks(0);
					//attacks(true);
								
					} //(fighters=null)
					else //there are fighters.
					{
					
						//attacks(false);
						int d=order;				
						
						Char opon=sel;
						
						if(opon.attacked())
						battle.addText(player.attack(opon)); //adds to attacks.
						else //attacking new opponent.
						{
						addFighter(opon);
						
						if(opon.seen(player))
						{						
							if(player.getWeapon().isRanged())
							battle.addText(player.attack(opon));
							else
							{
							battle.addText(opon.attack(player));
							checkLife();
							battle.addText(player.attack(opon));
							checkLife();
							}
						}
						else //surprise attack	
						{
							battle.addText("(surprise) "+player.attack(opon));
							checkLife();
						}  //nort sneaking in middle of battle.
						
						}
						////System.out.println("attack: "+txt);
											
						checkLife();				
						
						////System.out.println("NoA:" +player.attacks()+","+player.NoA());
						
						////System.out.println("attacks: "+player.attacks()+", noa: "+player.NoA());
						
						////System.out.println("attacks: "+player.attacks()+", noa: "+player.NoA());
						////System.out.println("f: "+fighters+"     , order: "+order);
						
						if(player.attacks()>=player.NoA()) //number of attacks
						{
						player.attacks(0);
						//order++;
																	
						d=fighters.indexOf(player); //plz?
						
						order=d+1;
						if(order>=fighters.size())
						order=0;
						////System.out.println("order: "+order+", d: "+d+", I W: "+fighters.indexOf(fighters.elementAt(order)));
						
						roundCon(d,battle,player,server);
						/*
							while(order!=d&&d>=0)
							{
							Char current=(Char)(fighters.elementAt(order));
							
							
							battle.addText(current.attack(player));//thread, sleep
							////System.out.println("attack-e: "+txt);
							
							////System.out.println("ch: "+current+", order: "+order);
							//guardian, order: 1,2,3
														
							d=fighters.indexOf(player);
							
							////System.out.println("d: "+d+" , order: "+order); //1,1    -1,0
							
							//why after dead attacks himself?  ;  check if attacks 2, this works?
							
							checkLife();
							
							order++;
								if(order>=fighters.size())
								order=0;
							}
							
						}
						
						//attacks(true); //THIS is the function that ruins it!!!
						
						checkLife();
						
						//if(player.isDead())
						//attacks(false);
						
						//battle.addText(s);
								
					} //fighting (ARE fighters) done.
		
		if(fighters!=null)
		if(fighters.size()==1)
		{
//			main.battle(false);
			
			checkLife();
			//fighters=null;
			fighters=new Vector();
			player.attacked(false);
			order=0;
			player.attacks(0);
		}

		
			
		//send things:	(?)
		//wake(this,order,battle,server); //plthis=this,d=order
		
		roundCon(order,battle,player,server); //d=order
				
		
		//do- attack new opponent
		
		//do- when dead\dying\fainted etc.

	}
	
	*/
	
	
	
	//change all of this.
	
	public void dropItems(Char c)
	{
	Vector pits=c.getItems();
	Vector its=new Vector(pits);
	
	for(int i=0;i<its.size();i++)
	{
	Item it=(Item)(its.elementAt(i));	
	pits.remove(it);
	addItem(it);
	}	
	
	double g=c.gp();
	gp=gp+g;
	c.addMoney(-g);
	
	pits.clear();
	
	}
	
	public int NoF(String clas) //number of fighters
	{
	int c=0;
	for(int i=0;i<fighters.size();i++)
	if(fighters.elementAt(i).getClass().toString().equals("class "+clas))
	c++;
	return c;	
	}
	
	public void writeSeen() //only characters
	{
	//System.out.println("SEEN:");
		for(int i=0;i<chars.size();i++)
		{
			if(chars.elementAt(i) instanceof Character)
			{
			Character c=(Character)(chars.elementAt(i));
			//System.out.println(c+": "+c.getSeen());
			}
					
		}
		
	//System.out.println("");		
	}
	
	
	
	
	public String write() //name not at start.
	{
		String s="P*";
		s+=DC+",";
		s+=name+",";
		
		s+=";";
		for(int i=0;i<items.size();i++)
		{
		Item m=(Item)(items.elementAt(i));
		s+=m.write();
		}
		s+=";";
										
		s+=gp+",";
		
		s+=order+",";
		
		s+=";";
		for(int i=0;i<fighters.size();i++)
		{
		int a=chars.indexOf(fighters.elementAt(i));
		s+=a+",";
		}
		s+=";";
		
		s+=";";
		for(int i=0;i<surprize.size();i++)
		{
		int a=chars.indexOf(surprize.elementAt(i));
		s+=a+",";
		}
		s+=";";
		
		
		s+=";";
		for(int i=0;i<nums.size();i++)
		{
		String a=(String)(nums.elementAt(i));
		s+=a+",";
		}
		s+=";";
		
		s+=";";
		for(int i=0;i<chars.size();i++)
		{
		////System.out.println("char: "+items.elementAt(i));
		Char m=(Char)(chars.elementAt(i));
		s+=m.write();
		}
		s+=";";
		
		
		return s+"*P";
	}
	
	
	
	
	
	
	public static Place read(String s)
	{
		Place d=new Place("");
		s=s.substring(2);
	
		int b=s.indexOf(",");
		d.DC=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
		b=s.indexOf(",");
		d.name=s.substring(0,b);
		s=s.substring(b+2);	
		
		
	b=s.indexOf(";");
	String i=s.substring(0,b);
	//items:
	while(i.indexOf("*")!=-1)
	{
	Item t=null;	
	int c=0;
	
	if(i.indexOf("I")==0)
	{
		c=i.indexOf("*I");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*I")+2;	
		}
		
		t=Item.read(i.substring(0,c+1));
	}
	
	if(i.indexOf("W")==0)
	{	
		c=i.indexOf("*W");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*W")+2;	
		}
		t=Weapon.readW(i.substring(0,c+1));
	}
	
	if(i.indexOf("A")==0)
	{
		c=i.indexOf("*A");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*A")+2;	
		}
		t=Armor.readA(i.substring(0,c+1));	
	}
	
	if(i.indexOf("S")==0)
	{
		c=i.indexOf("*S");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*S")+2;	
		}
		t=Shield.readS(i.substring(0,c+1));
	}
	
	d.items.add(t);
	i=i.substring(c+2);
	}
	//end items
	
	////System.out.println("\ns1: "+s+"\n");
	
	s=s.substring(b+1); //+2?
	
	////System.out.println("\ns2: "+s+"\n");
	
	b=s.indexOf(",");
	d.gp=Double.parseDouble(s.substring(0,b));
	s=s.substring(b+1);	

	b=s.indexOf(",");
	d.order=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+2);	
	

	Vector temp=new Vector();
	
	while(s.indexOf(";")!=0)
	{
		b=s.indexOf(",");
		int ind=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
		//d.fighters.add(d.chars.elementAt(ind));
		temp.add(ind+"");
	}
	
	s=s.substring(2);
	
	Vector temp2=new Vector();
	
	while(s.indexOf(";")!=0)
	{
		b=s.indexOf(",");
		int ind=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
		//d.fighters.add(d.chars.elementAt(ind));
		temp2.add(ind+"");
	}
	
	s=s.substring(2);
	
	
	while(s.indexOf(";")!=0)
	{
		b=s.indexOf(",");
		int num=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
		d.nums.add(num+"");
	}
	
	s=s.substring(2);
	
	
	b=s.lastIndexOf(";");
	
	i=s.substring(0,b);
	//chars:
	
	
	////System.out.println("\ns: "+s);
	////System.out.println("\nchars: "+i);
	
	
	Vector seen=new Vector();
	
	while(i.indexOf("*")!=-1)
	{
	Char t=null;	
	int c=0;
	
	if(i.indexOf("C")==0)
	{
		c=i.indexOf("*C");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*C")+2;	
		}
		
		t=Char.read(i.substring(0,c+1),d,seen);
	}
	
	if(i.indexOf("R")==0)
	{	
		c=i.indexOf("*R");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*R")+2;	
		}
		t=Character.readC(i.substring(0,c+1),d,seen);
	}
	
	if(i.indexOf("H")==0)
	{
		c=i.indexOf("*H");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*H")+2;	
		}
		////System.out.println("\n\nstr: "+i.substring(0,c+1+1));
		t=Merchant.readM(i.substring(0,c+1),d,seen);	
	}
	
	if(i.indexOf("E")==0)
	{
		c=i.indexOf("*E");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*E")+2;	
		}
		t=Enemy.readE(i.substring(0,c+1),d,seen);
	}
	
	//quester also?
	
	
	d.chars.add(t);
	i=i.substring(c+2);
	}
	//end chars
	
	//add the rest:
	
	for(int j=0;j<seen.size();j++)
	{
		Vector sen=(Vector)(seen.elementAt(j));
		Char c=(Char)(d.chars.elementAt(j));	
		c.createSeen(sen);				
	}
	
		
	
	
	s=s.substring(b+1);
	
	
	for(int j=0;j<temp.size();j++)
	{
		String in=(String)(temp.elementAt(j));
		int ind=Integer.parseInt(in);
		d.fighters.add(d.chars.elementAt(ind));
	}
	
	for(int j=0;j<temp2.size();j++)
	{
		String in=(String)(temp2.elementAt(j));
		int ind=Integer.parseInt(in);
		d.surprize.add(d.chars.elementAt(ind));
	}
	
			
	return d;
	}
		
}
