const { ShowCounters } = require("../../../../common/countersController.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  await ShowCounters({}, commandInteraction);
}

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "counter-show",
  "description": "Shows all existing counters.",
  "options": []
});
*/
