﻿using FolderSyncronizer.Debugging.Logs;

namespace FolderSyncronizer.Debugging
{
    public class Singleton
    {
        public const bool IsDebugMode = false;

        public static ICoreLogger Logger = new DebugLogger();
    }
}
