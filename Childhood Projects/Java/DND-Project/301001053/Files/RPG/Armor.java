

import java.io.*;

public class Armor extends Shield implements Serializable{
	
	
	/**
	 *Maximum dexterety bonus for the one who uses this
	 */
	private int md; //max dex (to ac)
	
	
	/**
	*constructs a new armor
	*@param n : the armor's name
	*@param lb : the armor's weight
	*@param val : the armor's value
	*@param abb : the armor's bonus
	*@param penn : the armor's penalty
	*@param sff : the armor's spell failure
	*@param m : the armor's maximum dexterety bonus
	*/
	Armor(String n,double lb,double val,int abb,int penn,int sff,int m)
	{
		super(n,lb,val,abb,penn,sff);
		md=m;
	}
	
	
	/**
	*constructs a new armor
	*@param n : the armor's name
	*@param lb : the armor's weight
	*@param val : the armor's value
	*@param abb : the armor's bonus
	*@param penn : the armor's penalty
	*@param sff : the armor's spell failure
	*@param m : the armor's maximum dexterety bonus
	*@param o : the armor's owner
	*/
	Armor(String n,double lb,double val,int abb,int penn,int sff,int m,Char o)
	{
		super(n,lb,val,abb,penn,sff,o);
		md=m;
	}
	
	
	/**
	*constructs a new armor
	*@param n : the armor's name
	*/
	
	public Armor(String n)
	{
	super(n);
		
		if(n.equals("Leather Armor"))
		{
			value=10;
			lbs=15;
			ab=2;
			pen=0;
			sf=10;
			md=6;
		}
		
		if(n.equals("Chain Shirt"))
		{
			value=80;
			lbs=25;
			ab=4;
			pen=-2;
			sf=20;
			md=4;
		}
		
		if(n.equals("Scale Mail"))
		{
			value=50;
			lbs=40;
			ab=3;
			pen=-4;
			sf=25;
			md=5;
		}
		
		if(n.equals("Chainmail"))
		{
			value=130;
			lbs=40;
			ab=5;
			pen=-5;
			sf=30;
			md=2;
		}
		
		if(n.equals("Splint Mail"))
		{
			value=180;
			lbs=45;
			ab=6;
			pen=-7;
			sf=40;
			md=0;
		}
		
		if(n.equals("Half Plate"))
		{
			value=250;
			lbs=50;
			ab=7;
			pen=-7;
			sf=40;
			md=0;
		}
		
		if(n.equals("Full Plate"))
		{
			value=700;
			lbs=50;
			ab=8;
			pen=-6;
			sf=35;
			md=1;
		}
		
	}
	
	
	/**
	*Returns a random armor
	*/
	public static Armor getArmor()
	{
	int p=Dice.roll(100);
	
	if(p>=1&&p<=25)
	return new Armor("Leather Armor");
	if(p>=25&&p<=45)
	return new Armor("Scale Mail");
	if(p>=46&&p<=65)
	return new Armor("Chain Shirt");
	if(p>=66&&p<=85)
	return new Armor("Chainmail");

	//86-100
	return new Armor("Splint Mail");

	}
	
	
	
	
	
	
	/**
	*Returns an armor by name with an owner
	*@param n : the armor's name
	*@param o : the owner
	*/
	public static Armor createa(String n,Char o)
	{
	Armor a=new Armor(n);
	a.setOwner(o);
	return a;	
	}
	
	public static Armor createa(String n,int num)
	{
	Armor a=new Armor(n);
	a.amount*=num;
	return a;	
	}
	
	public static Armor createImprovedA(String n,int arBo) //armor bonus
	{
	Armor a=new Armor(n);
	
	a.name=a.name+" +"+arBo;
	
	a.ab+=arBo;
					
	return a;	
	}
	
	
	//here
	
	public int getMaxD()
	{
		return md;
	}
	
	public static boolean existsInList(String n)
	{
		Armor a=new Armor(n);
		if(a.ab==0)
		return false;
		
		return true;
	}
	
	
	/**
	*Connverts an armor to a String (coded)
	*/
	public String write()
	{
	String s=super.write();	
	s="A"+s.substring(1,s.length()-3)+",";
	
	s+=md+",";
	s+="*A";
	return s;
	}
	
	
	/**
	*Returns an armor by a coded String
	*@param s : the coded String
	*/
	public static Armor readA(String s) //careful of "A*A"
	{
		Armor d=new Armor("");
				
		s=s.substring(2);
		
		int b=s.indexOf(",");
		int a;
		d.lbs=Double.parseDouble(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.name=s.substring(0,b);
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.value=Double.parseDouble(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		a=Integer.parseInt(s.substring(0,b));
		d.amount=a;
		s=s.substring(b+1);
		
		//shield
	
		b=s.indexOf(",");
		d.mab=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.ab=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.pen=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.sf=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		//armor
		b=s.indexOf(",");
		d.md=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
				
		return d;
	}
	
	
	public String toString()
	{
	String s=super.toString();	
	//s=s+", maximum dexterety bonus: "+md;	
	s=s+", max: "+md;	
	return s;
	}
	
}
