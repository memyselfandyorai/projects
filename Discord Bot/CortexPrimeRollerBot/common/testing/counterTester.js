const controller = require('../controller.js');
const {TestCase, createIocContainer}= require('./common.js');
const {
    prepareConstantCounters,
    COUNTER_WHO,
    COUNTER_NAME, 
    SECOND_COUNTER_WHO,
    SECOND_COUNTER_NAME, 
    OTHER_COUNTER_WHO,
    OTHER_COUNTER_NAME,
    GUILD_ID, USER_ID,
 } = require('./constantRolls.js');

const INVALID_NAME = ''

module.exports = {
  TestCases: createTestCases(),
};


function createTestCases() {
  return [
    new TestCase({
      name: 'testControllerSetCounter_sets_works',
      functionToRun: testControllerSetCounter,
      inputArgs: {
        who: COUNTER_WHO ,
        counter_name: COUNTER_NAME ,
        value: 3,
      },
      expectedResult: [
        `Set counter *${COUNTER_NAME}* to **3** for *${COUNTER_WHO}*.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: 3`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSetCounter_invalidName_invalid',
      functionToRun: testControllerSetCounter,
      inputArgs: {
        who: INVALID_NAME,
        counter_name: COUNTER_NAME ,
        value: 3,
      },
      expectedResult: [
        `Name is empty for this user. Please set a value so the default isn't chosen.`,        
        `No counters.`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSetCounter_setExistingCounter_works',
      functionToRun: testControllerSetCounter,
      inputArgs: {
        who: COUNTER_WHO ,
        counter_name: COUNTER_NAME ,
        value: 10,
        prepareInput: {},
      },
      expectedResult: [
        `Set counter *${COUNTER_NAME}* to **10** for *${COUNTER_WHO}*.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `-- *${COUNTER_WHO}*: 10`,
        `- **${SECOND_COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: -1`,
      ],    
    }),
    
     new TestCase({
      name: 'testControllerShowCounters_existing_works',
      functionToRun: testControllerShowCounters,
      inputArgs: {
        prepareInput: {},
      },
      expectedResult: [
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `-- *${COUNTER_WHO}*: 2`,
        `- **${SECOND_COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: -1`,
      ],    
    }),
    new TestCase({
      name: 'testControllerShowCounters_nonExisting_empty',
      functionToRun: testControllerShowCounters,
      inputArgs: {
      },
      expectedResult: [
        `No counters.`,
      ],    
    }),
    
    
    new TestCase({
      name: 'testControllerModifyCounter_existingCounter_works',
      functionToRun: testControllerModifyCounter,
      inputArgs: {
        who: COUNTER_WHO ,
        counter_name: SECOND_COUNTER_NAME,
        amount: 3,
        prepareInput: {},
      },
      expectedResult: [
        `Modified counter *${SECOND_COUNTER_NAME}* to **2** for *${COUNTER_WHO }*.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `-- *${COUNTER_WHO}*: 2`,
        `- **${SECOND_COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: 2`,
      ],    
    }),
    new TestCase({
      name: 'testControllerModifyCounter_existingCounterNegative_works',
      functionToRun: testControllerModifyCounter,
      inputArgs: {
        who: COUNTER_WHO ,
        counter_name: COUNTER_NAME,
        amount: -4,
        prepareInput: {},
      },
      expectedResult: [
        `Modified counter *${COUNTER_NAME}* to **-2** for *${COUNTER_WHO }*.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `-- *${COUNTER_WHO}*: -2`,
        `- **${SECOND_COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: -1`,
      ],    
    }),
    new TestCase({
      name: 'testControllerModifyCounter_existingCounterZero_works',
      functionToRun: testControllerModifyCounter,
      inputArgs: {
        who: SECOND_COUNTER_WHO ,
        counter_name: COUNTER_NAME,
        amount: 0,
        prepareInput: {},
      },
      expectedResult: [
        `Modified counter *${COUNTER_NAME}* to **0** for *${SECOND_COUNTER_WHO}*.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `-- *${COUNTER_WHO}*: 2`,
        `- **${SECOND_COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: -1`,
      ],    
    }),
    new TestCase({
      name: 'testControllerModifyCounter_nonExisting_sets',
      functionToRun: testControllerModifyCounter,
      inputArgs: {
        who: SECOND_COUNTER_WHO ,
        counter_name: COUNTER_NAME,
        amount: 2,
      },
      expectedResult: [
        `Modified counter *${COUNTER_NAME}* to **2** for *${SECOND_COUNTER_WHO}*.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 2`,
      ],    
    }),
    
   
    new TestCase({
      name: 'testControllerRemoveCounter_existingCounter_removed',
      functionToRun: testControllerRemoveCounter,
      inputArgs: {
        who: COUNTER_WHO ,
        counter_name: COUNTER_NAME,
        prepareInput: {},
      },
      expectedResult: [
        `Removed counter *${COUNTER_NAME}* of *${COUNTER_WHO }*.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `- **${SECOND_COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: -1`,
      ],    
    }),
     new TestCase({
      name: 'testControllerRemoveCounter_existingCounterRemovedName_removed',
      functionToRun: testControllerRemoveCounter,
      inputArgs: {
        who: COUNTER_WHO ,
        counter_name: SECOND_COUNTER_NAME,
        prepareInput: {},
      },
      expectedResult: [
        `Removed counter *${SECOND_COUNTER_NAME}* of *${COUNTER_WHO }*.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `-- *${COUNTER_WHO}*: 2`,
      ],    
    }),
     new TestCase({
      name: 'testControllerRemoveCounter_notFound_notFound',
      functionToRun: testControllerRemoveCounter,
      inputArgs: {
        who: COUNTER_WHO ,
        counter_name: COUNTER_NAME,
      },
      expectedResult: [
        `Counter *${COUNTER_NAME}* of *${COUNTER_WHO}* not found.`,
        `No counters.`,
      ],    
    }),
    new TestCase({
      name: 'testControllerRemoveCounter_existingButWhoNotFound_notFound',
      functionToRun: testControllerRemoveCounter,
      inputArgs: {
        who: OTHER_COUNTER_WHO ,
        counter_name: COUNTER_NAME,
        prepareInput: {},
      },
      expectedResult: [
        `Counter *${COUNTER_NAME}* of *${OTHER_COUNTER_WHO}* not found.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `-- *${COUNTER_WHO}*: 2`,
        `- **${SECOND_COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: -1`,        
      ],    
    }),
    new TestCase({
      name: 'testControllerRemoveCounter_existingButNameNotFound_notFound',
      functionToRun: testControllerRemoveCounter,
      inputArgs: {
        who: COUNTER_WHO ,
        counter_name: OTHER_COUNTER_NAME,
        prepareInput: {},
      },
      expectedResult: [
        `Counter *${OTHER_COUNTER_NAME}* of *${COUNTER_WHO}* not found.`,
        `- **${COUNTER_NAME}**:`,
        `-- *${SECOND_COUNTER_WHO}*: 0`,
        `-- *${COUNTER_WHO}*: 2`,
        `- **${SECOND_COUNTER_NAME}**:`,
        `-- *${COUNTER_WHO}*: -1`,        
      ],    
    }),    
      
  ]
}



/*
input: {
  who,
  counter_name,
  value - int,  
  optional channel_id , optional user_id ,
  prepareInput,
}

return: list of messages, including "show".
*/
async function testControllerSetCounter(input) {
  let iocContainer = createIocContainer([])
  
  if (input.prepareInput) {
    await prepareConstantCounters(input.prepareInput, iocContainer)
  }
  
  let channelId = input.channel_id ?? GUILD_ID
  let userId = input.user_id ?? USER_ID
  
  let inputOptions = {
    who: input.who,
    counterName: input.counter_name,
    value: input.value,  
    channelId:  channelId ,
    userId: userId ,
  }
  
  let result = await controller.SetCounter(inputOptions, iocContainer)
  let show = await controller.ShowCounters({
    channelId:  channelId ,
    userId: userId ,
  }, iocContainer)
  
  return result.concat(show)
}

/*
input: {
  who,
  counter_name,
  amount - int,  
  optional channel_id , optional user_id ,
  prepareInput,
}

return: list of messages, including "show".
*/
async function testControllerModifyCounter(input) {
  let iocContainer = createIocContainer([])
  
  if (input.prepareInput) {
    await prepareConstantCounters(input.prepareInput, iocContainer)
  }
  
  let channelId = input.channel_id ?? GUILD_ID
  let userId = input.user_id ?? USER_ID
  
  let inputOptions = {
    who: input.who,
    counterName: input.counter_name,
    amount: input.amount,  
    channelId:  channelId ,
    userId: userId ,
  }
  
  let result = await controller.ModifyCounter(inputOptions, iocContainer)
  let show = await controller.ShowCounters({
    channelId:  channelId ,
    userId: userId ,
  }, iocContainer)
  
  return result.concat(show)
}

/*
input: {
  who,
  counter_name,
  optional channel_id , optional user_id ,
  prepareInput,
}

return: list of messages, including "show".
*/
async function testControllerRemoveCounter(input) {
  let iocContainer = createIocContainer([])
  
  if (input.prepareInput) {
    await prepareConstantCounters(input.prepareInput, iocContainer)
  }
  
  let channelId = input.channel_id ?? GUILD_ID
  let userId = input.user_id ?? USER_ID
  
  let inputOptions = {
    who: input.who,
    counterName: input.counter_name,
    channelId:  channelId ,
    userId: userId ,
  }
  
  let result = await controller.RemoveCounter(inputOptions, iocContainer)
  let show = await controller.ShowCounters({
    channelId:  channelId ,
    userId: userId ,
  }, iocContainer)
  
  return result.concat(show)
}


/*
input: {
  optional channel_id , optional user_id ,
  prepareInput,
}

return: list of messages, including "show".
*/
async function testControllerShowCounters(input) {
  let iocContainer = createIocContainer([])
  
  if (input.prepareInput) {
    await prepareConstantCounters(input.prepareInput, iocContainer)
  }
  
  let channelId = input.channel_id ?? GUILD_ID
  let userId = input.user_id ?? USER_ID
  
  let show = await controller.ShowCounters({
    channelId:  channelId ,
    userId: userId ,
  }, iocContainer)
  
  return show
}
