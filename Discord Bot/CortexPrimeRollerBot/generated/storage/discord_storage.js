"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiscordStorage = exports.DiscordDatabase = exports.createDiscordDatabase = void 0;
const tslib_1 = require("tslib");
const node_fetch_1 = tslib_1.__importDefault(require("node-fetch"));
// https://discord.js.org/docs/packages/discord.js/14.14.1/TextChannel:Class
const discord_js_1 = require("discord.js");
const buffer_1 = require("buffer"); // Import Buffer class
const lz_string_1 = tslib_1.__importDefault(require("lz-string")); // Requires running "npm install lz-string", does render.com support it?
// npx tsc
// This should be 25 Mega.
const MAX_DISCORD_FILE_SIZE_BYTES = 1024 * 1024; // 1 MB
const DISCORD_SINGLE_FETCH_LIMIT = 100;
// TODO(urgent): Is this a single instance or not? check this. If it's not, I have an issue. - Added process ID logs.
// Check in logs how many times do I actually need to load the state. And if I have parallel instances.
// TODO: should I stress test this? Adding lots of things to the "pool" to verify I'm handling multiple messages properly? Can do it after launch.
async function createDiscordDatabase(params) {
    const client = params.discordBotClient;
    const myGuild = await client.guilds.fetch(params.serverId);
    var channel = await getChannelByName(myGuild, params.channelName);
    const discordStorage = new DiscordStorage(params.discordBotClient, channel);
    return new DiscordDatabase(discordStorage);
}
exports.createDiscordDatabase = createDiscordDatabase;
class DiscordDatabase {
    constructor(discordStorage) {
        Object.defineProperty(this, "cacheMap", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "discordStorage", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "currentState", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.cacheMap = new Map();
        this.discordStorage = discordStorage;
        this.currentState = null;
        console.log(`Creating Discord DB. Process = ${process.pid}`);
    }
    async getOrReadState() {
        if (!this.currentState) {
            this.currentState = await this.discordStorage.readExistingState();
        }
        return this.currentState;
    }
    async setItem(key, value) {
        const state = await this.getOrReadState();
        // Since we're storing it in-memory until the end of the program's life-cycle, these would keep the same object instance.
        // This causes "keep" to override previous results unintentionally. Thus, we duplicate the returned value.
        state.entries.set(key, cloneValue(value));
        state.hasChanged = true;
        // await this.discordStorage.writeState(state);
    }
    async getAllItemKeys() {
        const state = await this.getOrReadState();
        return Array.from(state.entries.keys());
    }
    async getItem(key) {
        const state = await this.getOrReadState();
        const item = state.entries.get(key);
        if (!item) {
            return item;
        }
        return cloneValue(item);
    }
    async deleteItem(key) {
        const state = await this.getOrReadState();
        state.entries.delete(key);
        state.hasChanged = true;
        // await this.discordStorage.writeState(state);
    }
    async flush() {
        if (this.currentState?.hasChanged) {
            await this.discordStorage.writeState(this.currentState);
        }
    }
}
exports.DiscordDatabase = DiscordDatabase;
class DiscordStorage {
    constructor(client, channel) {
        Object.defineProperty(this, "client", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "channel", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.client = client;
        this.channel = channel;
    }
    async readExistingState() {
        const channel = this.channel;
        logWithTime(`reading messages`);
        const botMessages = await getUserMessagesInChannel(channel, this.client.user?.id ?? "");
        logWithTime(`read ${botMessages.length} messages`);
        const encryptedMessages = [];
        for (const botMessage of botMessages) {
            const parsed = await parseFileMessage(botMessage);
            if (parsed !== undefined && parsed.fileContent) {
                encryptedMessages.push(parsed);
            }
        }
        if (encryptedMessages.length == 0) {
            return {
                entries: new Map(),
                existingMessageIds: [],
                hasChanged: false,
            };
        }
        const sortedEncryptedMessages = getSortedRelatedMessages(encryptedMessages);
        const buffer = joinUint8Arrays(sortedEncryptedMessages.map((msg) => msg.fileContent));
        const fullContent = lz_string_1.default.decompressFromUint8Array(buffer);
        const pairs = JSON.parse(fullContent);
        const entries = new Map(pairs.map((item) => [item.key, item.value]));
        return {
            entries,
            existingMessageIds: encryptedMessages.map((msg) => msg.messageId),
            hasChanged: false,
        };
    }
    async writeState(storageState) {
        const items = Array.from(storageState.entries.entries()).map((entry) => ({
            key: entry[0],
            value: entry[1],
        }));
        // Consider adding some cache layer over this, up to 10 seconds ttl?
        // will I have efficiency issues at all? will this work in parallel? --> I can change this to per channel later, maybe.
        // I can create an "ioc container" per channel. if not efficient, cache?
        const fullContent = lz_string_1.default.compressToUint8Array(JSON.stringify(items));
        const messagesToSend = splitUint8Array(fullContent, MAX_DISCORD_FILE_SIZE_BYTES);
        // Throttling issues when pinning messages. How many times can I do message "edits"?
        // need to edit messages. assuming won't overwrite due to (can get throttled, BTW? it's possible!!!) lock issues.
        // maybe I'll need a cache layer, writing once each x time.
        const newMessageIds = await this.writeMessagesAsFiles(messagesToSend, fullContent.length);
        // State is the same as written.
        storageState.hasChanged = false;
        await this.deleteFileMessages(storageState);
        // Emptying message IDs so it won't retry indefinitely. E.g., if an ID is missing.
        // Next time we read messages, they won't exist anyway.
        storageState.existingMessageIds = newMessageIds;
    }
    async deleteFileMessages(storageState) {
        try {
            const textChannel = this.channel;
            if (textChannel) {
                logWithTime(`bulk deleting ${storageState.existingMessageIds.length} storage messages`);
                // Consider editting existing messages (and deleting the redundant ones) if it's more efficient. If deleting fails, this should recover the next time it tries to write (as new messages, marked with "isLast", should exist).
                await textChannel.bulkDelete(storageState.existingMessageIds);
            }
            else {
                logWithTime(`deleting ${storageState.existingMessageIds.length} storage messages`);
                for (const msgId of storageState.existingMessageIds) {
                    await this.channel.messages.delete(msgId);
                }
            }
            // WARNING: If one fails in the per-msg deletion, then it would still remove all the existingMessageIds. Note when re-reading them it should resolve itself.
            logWithTime(`deleted storage messages`);
        }
        catch (error) {
            console.error(`Failed deleting existing messages. This should resolve itself out eventually.`, error);
        }
    }
    async writeMessagesAsFiles(messagesToSend, fullContentLength) {
        const newMessageIds = [];
        logWithTime(`sending ${messagesToSend.length} storage messages, total size ${fullContentLength}`);
        // I'm adding timestamp, hopefully this will help resolve issues if writing things at the same time. So no state will override the other. I won't get to a corrupted state, but it's possible we'll lose data.
        let i = 0;
        const timestamp = new Date().getTime();
        for (const binaryMessage of messagesToSend) {
            const isLast = i == messagesToSend.length - 1;
            const message = await this.channel.send({
                // To help sort this, and deal with deletion failures, and write in concurrency.
                content: `${i},${isLast},${timestamp}`,
                files: [buffer_1.Buffer.from(binaryMessage)],
            });
            newMessageIds.push(message.id);
            i++;
        }
        logWithTime(`done sending ${messagesToSend.length} storage messages`);
        return newMessageIds;
    }
}
exports.DiscordStorage = DiscordStorage;
async function parseFileMessage(message, text) {
    const contentText = message?.content ?? text ?? "";
    const content = contentText.split(",");
    const index = Number(content[0]);
    if (isNaN(index)) {
        console.log(`Not a storage message (invalid index): ${contentText}`);
        return undefined;
    }
    const isLast = content[1] === "true";
    const timestamp = Number(content[2]);
    const lastMessageEdited = message?.editedTimestamp ?? message?.createdTimestamp;
    const lastEdited = timestamp ?? lastMessageEdited;
    const firstAttachment = message?.attachments.at(0);
    if (!firstAttachment) {
        console.log(`No attachment: ${contentText}`);
        return undefined;
    }
    const response = await (0, node_fetch_1.default)(firstAttachment.url);
    if (!response.ok) {
        console.log(`Failed fetching attachment URL: ${firstAttachment.url}`);
        return undefined;
    }
    const blob = await response.arrayBuffer();
    return {
        index,
        fileContent: new Uint8Array(blob),
        messageId: message.id,
        timestamp: lastEdited,
        isLast,
    };
}
async function getChannelByName(guild, name) {
    const channels = await guild.channels.fetch();
    return channels.find((channel) => channel?.name === name && channel?.type === discord_js_1.ChannelType.GuildText);
    // Create if doesn't exist? Better tell them to create it themselves? To be clear. And so it won't use existing channels.
    // return await this.guild.channels.create({name, type: ChannelType.GuildText});
}
async function getUserMessagesInChannel(channel, userId) {
    const messages = [];
    let lastMessagesCount;
    let lastMessageId;
    do {
        const fetchedMessages = Array.from((await channel.messages.fetch({
            limit: DISCORD_SINGLE_FETCH_LIMIT,
            before: lastMessageId,
        })).values());
        messages.push(...fetchedMessages.filter((message) => message.author.id === userId));
        lastMessagesCount = fetchedMessages.length;
        lastMessageId = fetchedMessages.pop()?.id;
    } while (lastMessagesCount === DISCORD_SINGLE_FETCH_LIMIT);
    return messages;
}
// Returns the sorted messages, with the most updated message per index, while stopping when such an updated message is marked as last.
function getSortedRelatedMessages(encryptedMessages) {
    const indexToMessage = new Map();
    encryptedMessages.forEach((msg) => {
        if (indexToMessage.has(msg.index) &&
            msg.timestamp < indexToMessage.get(msg.index).timestamp) {
            return; // skip
        }
        indexToMessage.set(msg.index, msg);
    });
    const takenUntilLast = [];
    for (const msg of indexToMessage.values()) {
        takenUntilLast.push(msg);
        if (msg.isLast) {
            break;
        }
    }
    // a,b => a-b, ascending order
    return Array.from(indexToMessage.values()).sort((msgA, msgB) => msgA.index - msgB.index);
}
function joinUint8Arrays(arrays) {
    const totalLength = arrays.reduce((acc, arr) => acc + arr.length, 0);
    const joinedArray = new Uint8Array(totalLength);
    let offset = 0;
    for (const arr of arrays) {
        joinedArray.set(arr, offset);
        offset += arr.length;
    }
    return joinedArray;
}
function splitUint8Array(data, maxByteSize) {
    const chunks = [];
    for (let start = 0; start < data.length; start += maxByteSize) {
        const chunk = data.slice(start, Math.min(start + maxByteSize, data.length));
        chunks.push(chunk);
    }
    return chunks;
}
function logWithTime(msg) {
    const timeWithMilli = new Date().toLocaleTimeString("il", {
        hour12: false,
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        fractionalSecondDigits: 3,
    });
    console.log(`${timeWithMilli} - ${msg}`);
}
function cloneValue(value) {
    if (value == undefined) {
        return value;
    }
    return JSON.parse(JSON.stringify(value));
}
// FYI, admin actions are really slow.
/*
19:01:40.232 - reading messages
19:01:40.684 - read 1 messages

19:01:41.439 - reading messages
19:01:41.849 - read 1 messages
19:01:42.282 - bulk deleting 1 storage messages
19:01:43.090 - deleted storage messages
19:01:43.090 - sending 1 storage messages, total size 310
19:01:43.566 - done sending 1 storage messages
*/
//# sourceMappingURL=discord_storage.js.map