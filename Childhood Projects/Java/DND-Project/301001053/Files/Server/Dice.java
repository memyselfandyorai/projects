
import java.io.*;

public class Dice implements Serializable{
	
	private int n; //number
	private int t; //times
	private int b; //bonus
	
	public Dice(int tt,int nn,int bb)
	{
		n=nn;
		t=tt;
		b=bb;
	}

	public Dice(int tt,int nn)
	{
		n=nn;
		t=tt;
		b=0;
	}


	public Dice(int nn)
	{
		n=nn;
		t=1;
		b=0;
	}
	
	
	/*
	 *returns a random number compatable to this dice
	 */
	public int roll()
	{
	int s=0;
	for(int i=0;i<t;i++)
	s+=(int)(Math.random()*n+1);
	s+=b;
	return s;
	}
	
	/*
	sets the b(bonus) of this object
	 */
	public void bonus(int bb)
	{
		b=bb;
	}
	
	
	
	public String getString(int bonus) //not negative.
	{
	int aa=t+bonus+b;
	int bb=t*n+bonus+b;
	if(aa<0)
	aa=0;
	if(bb<0)
	bb=0;
	
	String s=" ("+(aa)+"-"+(bb)+")"; //for damage
	if(b+bonus>0)
	return t+"d"+n+"+"+(bonus+b)+s;
	if(b+bonus<0)
	return t+"d"+n+(bonus+b)+s;
	return t+"d"+n+s;
	}
	
	/*
	 *returns a random number compatable to the variables given
	 */
	public static int roll(int a,int b,int c)
	{
	int s=0;
	for(int i=0;i<a;i++)
	s+=(int)(Math.random()*b+1);
	s+=c;
	return s;
	}
	
	/*
	 *returns a random number compatable to the variables given
	 */
	public static int roll(int a,int b)
	{
	int s=0;
	for(int i=0;i<a;i++)
	s+=(int)(Math.random()*b+1);
	return s;
	}
	
	/*
	 *returns a random number compatable to the variables given
	 */
	public static int roll(int a)
	{
	return (int)(Math.random()*a+1);
	}
	
	public static int rollWZ(int a)
	{
	if(a>0)
	return (int)(Math.random()*a+1);
	else
	return 0;
	}
	
	
	public String write()
	{
	String s="D*";	
	s+=n+",";
	s+=t+",";	
	s+=b+",";
	s+="*D";
	return s;
	}
	
	
	
	public static Dice read(String s)
	{
		Dice d=new Dice(0);
		
		s=s.substring(2);
		
		int b=s.indexOf(",");
		int a=Integer.parseInt(s.substring(0,b));
		d.n=a;
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		a=Integer.parseInt(s.substring(0,b));
		d.t=a;
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		a=Integer.parseInt(s.substring(0,b));
		d.b=a;
		s=s.substring(b+1);
		
		
		return d;
	}
	
	
	/*
	*returns object as string
	*/
	public String toString()
	{
		
		if(b==0)
		return t+"d"+n;
		if(b>0)
		return t+"d"+n+"+"+b;
		//b<0
		return t+"d"+n+b;
		
	}
	
}
