
import java.awt.*;
import javax.swing.*;
import java.util.*;


//+class quest, and "quster" ("NPC"?)

public class Merchant extends Char{
	
	
	private Vector inv; //inventory
	//private String kind; //blacksmith,brewer(? no.),merchant, forgerer(???!? no.)
	
	//if needed, a different one
	
	public Merchant(String n,int level,Place plc) //1 to 5
	{
		place=plc;
		
		if(Dice.roll(2)==1)
		male=true;
		else
		male=false;
		
		name=n;
		
		//name=createName(male);
		
		inv=new Vector();
		
		if(n.equals("Blacksmith"))
		hp=5*level+2;
		else
		hp=4*level+1;
		
		int r=Dice.roll(10);
		
		if(n.equals("Blacksmith"))
		{
		str=5+level*2+Dice.roll(5);
		dex=7+level+Dice.roll(4);
		con=6+Dice.roll(2,level);
		inl=8+level+Dice.roll(4);
		wis=8+level/2+Dice.roll(3);
		cha=7+level+Dice.roll(2,2);
		
		if(r>=1&&r<=5)
		race="Human";
		if(r>=5&&r<=9)
		race="Dwarf";
		if(r==10)
		race="Elf";
		}
		
		/*if(n.equals("Brewer"))
		{
		str=10+level;
		dex=6+level*2;
		con=8+Dice.roll(1,level);
		inl=8+level*2;
		wis=8+level+Dice.roll(2,3);
		cha=8+level+Dice.roll(1,4);
		
		if(r>=1&&r<=4)
		race="Human";
		if(r>=5&&r<=8)
		race="Elf";
		if(r==9)
		race="Halfling";
		if(r==10)
		race="Dwarf";
		}*/
		
		if(n.equals("Merchant"))
		{
		str=7+level+Dice.roll(1,4);
		dex=7+Dice.roll(1,level,level);
		con=6+Dice.roll(2,level);
		inl=10+level;
		wis=8+level+Dice.roll(1,4);
		cha=10+Dice.roll(2,4);
		
		if(r>=1&&r<=4)
		race="Human";
		if(r>=5&&r<=6)
		race="Dwarf";
		if(r>=7&&r<=10)
		race="Halfling";
		}
		
		gp=(double)Dice.roll(5,level*2,level)/50;
		
		ref=level;
		will=level*2;
		fort=level+3;
		
		if(n.equals("Blacksmith"))
		{
			
			//weapon, shield and armor
			
			if(level>=1&&level<=2)
			{
			r=Dice.roll(100);
				if(r>=1&&r<=20)
				weapon=Weapon.create("Longsword",this);	
				if(r>=21&&r<=35)
				weapon=Weapon.create("Longbow",this);	
				if(r>=36&&r<=50)
				weapon=Weapon.create("Mace",this);	
				if(r>=51&&r<=60)
				weapon=Weapon.create("Short Sword",this);	
				if(r>=61&&r<=75)
				weapon=Weapon.create("Short Bow",this);	
				if(r>=76&&r<=100)
				weapon=Weapon.create("Handaxe",this);	
			
			if(Dice.roll(100)>60)
			shield=Shield.getShield();
			
			if(Dice.roll(100)>80);
			armor=Armor.getArmor();
			}
			
			if(level>=3&&level<=4)
			{
			r=Dice.roll(100);
				if(r>=1&&r<=20)
				weapon=Weapon.create("Morningstar",this);	
				if(r>=21&&r<=35)
				weapon=Weapon.create("Light Crossbow",this);	
				if(r>=36&&r<=50)
				weapon=Weapon.create("Heavy Crossbow",this);	
				if(r>=51&&r<=60)
				weapon=Weapon.create("Club",this);	
				if(r>=61&&r<=75)
				weapon=Weapon.create("Battleaxe",this);	
				if(r>=76&&r<=100)
				weapon=Weapon.create("Longsword",this);	
			
			if(Dice.roll(100)>30)
			shield=Shield.getShield();
			
			if(Dice.roll(100)>50);
			armor=Armor.getArmor();			
			}
			
			if(level==5)
			{
			r=Dice.roll(100);
				if(r>=1&&r<=45)
				weapon=Weapon.create("Greatsword",this);	
				if(r>=46&&r<=80)
				weapon=Weapon.create("Greataxe",this);	
				if(r>=81&&r<=95)
				weapon=Weapon.create("Bastard Sword",this);	
				if(r>=96&&r<=100)
				weapon=Weapon.create("Katana",this);
									
			r=Dice.roll(10);
			if(Dice.roll(100)<30)
			r=0;
			
			if(r>=1&&r<=3)
			armor=Armor.createa("Chainmail",this);
			if(r>=4&&r<=8)
			armor=Armor.createa("Splint Mail",this);
			if(r==9)
			armor=Armor.createa("Half Plate",this);
			if(r==10)
			armor=Armor.createa("Full Plate",this);
			}
		
		
		//merchandise
		if(Dice.roll(100)+level*5>40)
		inv.add(Weapon.create("Longbow",level/2+1));	
		
		if(Dice.roll(100)+level*15>50)
		inv.add(Weapon.create("Longsword",1+level/2));	
		
		if(Dice.roll(100)+level*5>60)
		inv.add(Weapon.create("Battleaxe",1+level/2));
		
		
		if(Dice.roll(100)+level*5>40&&level!=5)
		inv.add(Shield.create("Buckler",level));
		
		inv.add(Item.getItem("Arrows",level*10));
		
		if(Dice.roll(100)+level*10>40&&level!=5)
		inv.add(Item.getItem("Bullets",level*5));
		
		if(Dice.roll(100)+level*10>40&&level!=5)
		inv.add(Item.getItem("Bolts",level*5));
		
		if(level==1||level==2)
		{
		if(Dice.roll(100)+level*10>35)
		inv.add(Weapon.create("Short Sword",level));	
		if(Dice.roll(100)+level*10>40)
		inv.add(Weapon.create("Short Bow",level));
		if(Dice.roll(100)+level*10>55)
		inv.add(Weapon.create("Club",level));
		if(Dice.roll(100)+level*2>30)
		inv.add(Weapon.create("Dagger",3));
		if(Dice.roll(100)+level*2>35)
		inv.add(Weapon.create("Sling",3));
		if(Dice.roll(100)+level*10>45)
		inv.add(Weapon.create("Handaxe",level));
		
		if(Dice.roll(100)+level*2>35)
		inv.add(Shield.create("Wooden Shield",level));
		if(Dice.roll(100)+level*5>40)
		inv.add(Shield.create("Steel Shield",level));
		
		if(Dice.roll(100)+level*5>38)
		inv.add(Armor.createa("Leather Armor",2));
		if(Dice.roll(100)+level*10>45)
		inv.add(Armor.createa("Scale Mail",2));
		if(Dice.roll(100)+level*9>60)
		inv.add(Armor.createa("Chain Shirt",1));
		
	
		}
		
		if(level==3||level==4)
		{
		if(Dice.roll(100)+level*10>35)
		inv.add(Weapon.create("Longsword",level));	
		if(Dice.roll(100)+level*10>40)
		inv.add(Weapon.create("Longbow",level));
		if(Dice.roll(100)+level*10>55)
		inv.add(Weapon.create("Morningstar",level));
		if(Dice.roll(100)+level*2>30)
		inv.add(Weapon.create("Light Crossbow",3));
		if(Dice.roll(100)+level*2>35)
		inv.add(Weapon.create("Heavy Crossbow",3));
		if(Dice.roll(100)+level*10>45)
		inv.add(Weapon.create("Battleaxe",level));
		
		if(Dice.roll(100)+level*2>30)
		inv.add(Shield.create("Steel Shield",level));
		if(Dice.roll(100)+level*5>60)
		inv.add(Shield.create("Tower Shield",1));
		
		if(Dice.roll(100)+level*5>40)
		inv.add(Armor.createa("Chainmail",2));
		if(Dice.roll(100)+level*10>50)
		inv.add(Armor.createa("Splint Male",2));
		if(Dice.roll(100)+level*9>70)
		inv.add(Armor.createa("Half Plate",1));
		
		}
		
		if(level==5)
		{
		inv.add(Weapon.create("Longsword",Dice.roll(2,level)));	
		inv.add(Weapon.create("Longbow",Dice.roll(2,level)));
		inv.add(Weapon.create("Heavy Crossbow",Dice.roll(2,level)));
		if(Dice.roll(100)>55)
		inv.add(Weapon.create("Greataxe",level));
		if(Dice.roll(100)>45)
		inv.add(Weapon.create("Greatsword",level));
		
		inv.add(Shield.create("Steel Shield",level*2));
		inv.add(Shield.create("Buckler",level*2));
		inv.add(Shield.create("Tower Shield",level));
		
		inv.add(Armor.createa("Chainmail",2));
		if(Dice.roll(100)+level*9>70)
		inv.add(Armor.createa("Full Plate",2));
		if(Dice.roll(100)+level*10>60)
		inv.add(Armor.createa("Half Plate",1));
		
		}
		
		
		} //blacksmith
		
		if(n.equals("Merchant"))
		{
			//items
			
			if(level>=1&&level<=2)
			{
			r=Dice.roll(100);
				if(r>=1&&r<=10)
				weapon=Weapon.create("Longsword",this);	
				if(r>=11&&r<=20)
				weapon=Weapon.create("Longbow",this);	
				if(r>=21&&r<=40)
				weapon=Weapon.create("Mace",this);	
				if(r>=41&&r<=60)
				weapon=Weapon.create("Short Sword",this);	
				if(r>=61&&r<=75)
				weapon=Weapon.create("Short Bow",this);	
				if(r>=76&&r<=100)
				weapon=Weapon.create("Handaxe",this);	
			
			if(Dice.roll(100)>80)
			shield=Shield.getShield();
			
			}
			
			if(level>=3&&level<=4)
			{
			r=Dice.roll(100);
				if(r>=1&&r<=20)
				weapon=Weapon.create("Morningstar",this);	
				if(r>=21&&r<=40)
				weapon=Weapon.create("Light Crossbow",this);	
				if(r>=41&&r<=70)
				weapon=Weapon.create("Club",this);	
				if(r>=71&&r<=100)
				weapon=Weapon.create("Longsword",this);	
			
			if(Dice.roll(100)>50)
			shield=Shield.getShield();
			}
			
			if(level==5)
			{
			r=Dice.roll(100);
				if(r>=1&&r<=45)
				weapon=Weapon.create("Longsword",this);	
				if(r>=46&&r<=80)
				weapon=Weapon.create("Battleaxe",this);	
				if(r>=81&&r<=100)
				weapon=Weapon.create("Heavy Crossbow",this);	
			
			shield=Shield.getShield();
				
			}
		
		
		//merchandise
		if(Dice.roll(100)+level*5>50)
		inv.add(Item.getItem("Arrows",level*10));
		
		if(Dice.roll(100)+level*5>50)
		inv.add(Item.getItem("Bullets",level*5));
		
		if(Dice.roll(100)+level*5>50)
		inv.add(Item.getItem("Bolts",level*5));
		
		inv.add(Item.getItem("Meals",level*5));
		
		if(level==1||level==2)
		{
		if(Dice.roll(100)+level*5>50)
		inv.add(Item.getItem("Wood",level));
		if(Dice.roll(100)+level*5>60)
		inv.add(Item.getItem("Copper",level));
		if(Dice.roll(100)+level*5>70)
		inv.add(Item.getItem("Iron",level));
		if(Dice.roll(100)+level*10>65)
		inv.add(Item.getItem("Bow String",level));
		if(Dice.roll(100)+level*9>55)
		inv.add(Item.getItem("Torch",level));
		}
		
		if(level==3||level==4)
		{
		inv.add(Item.getItem("Tent",2));
		if(Dice.roll(100)+level*5>50)
		inv.add(Item.getItem("Lumber",3));
		if(Dice.roll(100)+level*5>60)
		inv.add(Item.getItem("Healer's Kit",2));
		if(Dice.roll(100)+level*5>65)
		inv.add(Item.getItem("Climber's Kit",2));
		if(Dice.roll(100)+level*10>75)
		inv.add(Item.getItem("Artisian Tools",2));
		if(Dice.roll(100)+level*9>55)
		inv.add(Item.getItem("Flint and Steel",level));
		if(Dice.roll(100)+level*9>50)
		inv.add(Item.getItem("Iron",level*2));
		if(Dice.roll(100)+level*9>60)
		inv.add(Item.getItem("Silver",level*2));
		if(Dice.roll(100)+level*9>75)
		inv.add(Item.getItem("Gold",level*2));
		if(Dice.roll(100)+level*5>70)
		inv.add(Item.getItem("Silk",level));
		if(Dice.roll(100)+level*7>70)
		inv.add(Item.getItem("Pebbles",level*10));
		if(Dice.roll(100)+level*10>55)
		inv.add(Item.getItem("Bow String",level));
		if(Dice.roll(100)+level*5>80)
		inv.add(Item.getRing());
		}
		
		if(level==5)
		{
		inv.add(Item.getItem("Wood",10));
		inv.add(Item.getItem("Healer's Kit",2));
		inv.add(Item.getItem("Climber's Kit",2));
		inv.add(Item.getItem("Artisian Tools",2));
		inv.add(Item.getItem("Silver",level*2));
		if(Dice.roll(100)+level*5>55)
		inv.add(Item.getItem("Gold",level*2));
		if(Dice.roll(100)+level*5>50)
		inv.add(Item.getItem("Silk",level*2));
		if(Dice.roll(100)+level*7>60)
		inv.add(Item.getItem("Pebbles",level*10));
		if(Dice.roll(100)+level*6>55)
		inv.add(Item.getItem("Bow String",level));
		int rg=Dice.roll(2,4);
		if(Dice.roll(100)>40)
		for(int i=0;i<rg;i++)
		inv.add(Item.getRing());
		
		}
		
		}//merchant
		
		
		//the others.
		
		if(weapon!=null)
		{
		items.add(weapon);
		weapon.setOwner(this);
		}
		else
		weapon=Weapon.create("Unarmed Strike",this);
		
		if(shield!=null)
		if(weapon.isRanged()&&!shield.getName().equals("Buckler"))
		shield=null;
		
		if(shield!=null)
		{
		shield.setOwner(this);
		items.add(shield);
		}
		
		if(armor!=null)
		{
		armor.setOwner(this);
		items.add(armor);
		}
		
		if(weapon.isRanged())
		items.add(new Item(weapon.kind()));		
		//items. + inv.
		
		
		//System.out.println("items: "+items);
			
	}//end constructor
	
	
	public Merchant(Place plc)
	{
		place=plc;
		
		inv=new Vector();
		
	}
	
	
	public Vector getInv()
	{
		return inv;
	}
	
	public void removeIfInv(String n,int m) 
	{
	for(int i=0;i<inv.size();i++)
	{
		Item t=(Item)(inv.elementAt(i));
		if(n.equals(t.getName()))	
		{
			t.setAmount(t.getAmount()-m);
			if(t.getAmount()<=0)
			inv.remove(t);
			
			i=inv.size();
		}
	}
	
	}
	
	
	public String write()
	{
		String s=super.write();	
		s="H"+s.substring(1,s.length()-3)+",";
		s+=";";
		for(int i=0;i<inv.size();i++)
		{
			Item t=(Item)(inv.elementAt(i));
			s+=t.write();
		}
		s+=";";
		
		//System.out.println("\n\n\n\nmerchant: "+s);
		
		return s+"*H";
	}
	
	
	
	public static Merchant readM(String s,Place plc,Vector seens) //careful of "C*C"
	{
	Merchant d=new Merchant(plc);
	//d.place=plc;
	s=s.substring(2);
	
	int b=s.indexOf(",");
	d.dam=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);	
	
	b=s.indexOf(",");
	d.name=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hp=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.str=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.dex=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.race=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	
	if((s.substring(0,b)).equals("true"))
	d.male=true;
	else
	d.male=false;
	
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.con=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.wis=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.inl=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.cha=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.damr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.nata=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.sr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.gp=Double.parseDouble(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*W"); //have to be.
	d.weapon=Weapon.readW(s.substring(0,b+1));
	s=s.substring(b+3);
	
	if(!d.weapon.getName().equals("Unarmed Strike"))
	d.items.add(d.weapon);
	
	d.weapon.setOwner(d);
	
	String th=s.substring(0,s.indexOf(";"));
	s=s.substring(s.indexOf(";")+1);
	
	//System.out.println("th: "+th);
	b=th.lastIndexOf("*A");
	if(b!=-1)
	{
	d.armor=Armor.readA(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.armor);
	d.armor.setOwner(d);
	}
	
	b=th.lastIndexOf("*S");
	if(b!=-1)
	{
	d.shield=Shield.readS(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.shield);
	d.shield.setOwner(d);
	}
	
	
	
	b=s.indexOf(";");
	//System.out.println("BI: "+b+" , "+s);
	String i=s.substring(0,b);
	//items:
	while(i.indexOf("*")!=-1)
	{
	Item t=null;	
	int c=0;
	
	if(i.indexOf("I")==0)
	{
		c=i.indexOf("*I");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*I")+2;	
		}
		
		t=Item.read(i.substring(0,c+1));
	}
	
	if(i.indexOf("W")==0)
	{	
		c=i.indexOf("*W");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*W")+2;	
		}
		t=Weapon.readW(i.substring(0,c+1));
		((Weapon)t).setOwner(d);
	}
	
	if(i.indexOf("A")==0)
	{
		c=i.indexOf("*A");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*A")+2;	
		}
		t=Armor.readA(i.substring(0,c+1));	
		((Armor)t).setOwner(d);
	}
	
	if(i.indexOf("S")==0)
	{
		c=i.indexOf("*S");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*S")+2;	
		}
		t=Shield.readS(i.substring(0,c+1));
		
		((Shield)t).setOwner(d);
	}
	
	
	d.items.add(t);
	i=i.substring(c+2);
	}
	//end items
	
	s=s.substring(b+1);
	
	
	b=s.indexOf(",");
	d.ref=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.will=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.fort=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*L");
	d.spells=Spells.read(s.substring(0,b));
	s=s.substring(b+3);
	d.spells.setOwner(d);
	
	b=s.indexOf(",");
	//d.attacked=Boolean.getBoolean(s.substring(0,b));
	if((s.substring(0,b)).equals("true"))
	d.attacked=true;
	else
	d.attacked=false;
	s=s.substring(b+1);
	
	
	//seen!
	b=s.indexOf(",");
	int size=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	Vector sen=new Vector();
	for(int j=0;j<size;j++)
	{
	b=s.indexOf(",");
	int index=Integer.parseInt(s.substring(0,b));
	sen.add(index+"");
	//Char c=d.bySerial(index);
	//d.seen.add(c);
	s=s.substring(b+1);
	}
	seens.add(sen);
	//end seen
	
	
	//Merchant:
	
	s=s.substring(1);
	//inv:
	b=s.indexOf(";");
	i=s.substring(0,b);
	
	//System.out.println("inv?: "+s+"\n\n");
	
	//System.out.println("i: "+i);
	
	while(i.indexOf("*")!=-1)
	{
	Item t=null;	
	int c=0;
	
	if(i.indexOf("I")==0)
	{
		c=i.indexOf("*I");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*I")+2;	
		}
		
		t=Item.read(i.substring(0,c+1));
	}
	
	if(i.indexOf("W")==0)
	{	
		c=i.indexOf("*W");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*W")+2;	
		}
		t=Weapon.readW(i.substring(0,c+1));
	}
	
	if(i.indexOf("A")==0)
	{
		c=i.indexOf("*A");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*A")+2;	
		}
		t=Armor.readA(i.substring(0,c+1));	
		
	}
	
	if(i.indexOf("S")==0)
	{
		c=i.indexOf("*S");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*S")+2;	
		}
		t=Shield.readS(i.substring(0,c+1));
	}
	
	
	//System.out.println("first i: "+()i.toCharArray())[0]);
	//System.out.println("first i: "+i);
	
	d.inv.add(t);
	
	//System.out.println("inv: "+d.inv);
	
	i=i.substring(c+2);
	}
	
	s=s.substring(b+1);
	
	
	//end inv
	
	return d;
	}
	
	
	
	
	/* quests.
	public static String createName(boolean male)
	{
	int n=Dice.roll(8);
	String s="";
	if(male)
	switch(n)
	{
	case 1:		
	s="Dwarn";
	break;
	case 2:		
	s="Elnot";
	break;
	case 3:		
	s="Andar";
	break;
	
	case 4:		
	s="Warnebi";
	break;
	case 5:		
	s="Sllitar";
	break;
	case 6:		
	s="Numram";
	break;
	case 7:		
	s="Mortar";
	break;
	case 8:		
	s="Golrad";
	break;
			
	}	
	else
	switch(n)
	{
	case 1:		
	s="Dannila";
	break;
	case 2:		
	s="Elinol";
	break;
	case 3:		
	s="Ambil";
	break;
	
	case 4:		
	s="Winole";
	break;
	case 5:		
	s="Sanra";
	break;
	case 6:		
	s="Nimra";
	break;
	case 7:		
	s="Monfairy";
	break;
	case 8:		
	s="Gallian";
	break;
	}	
		
	
	return s;
	}
	
	
	*/
	
	
}
