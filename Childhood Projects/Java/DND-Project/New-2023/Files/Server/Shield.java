
import java.io.*;

public class Shield extends Item implements Serializable{// extends Item{
	
	
	
	protected int mab; //magic armor bonus
	protected int ab; //armor bonus
	protected int pen; //check (dex only?) penalty
	protected int sf; //spell failure- percentage
	protected Char owner;
	
	public Shield(String n,double lb,double val,int abb,int penn,int sff)
	{
		super(n,lb,val);
		ab=abb;
		pen=penn;		
		sf=sff;
	//	owner=null;	
	}
	
	public Shield(String n,double lb,double val,int abb,int penn,int sff,Char o)
	{
		super(n,lb,val);
		ab=abb;
		pen=penn;
		sf=sff;
		owner=o;	
	}
	
	
	
	public Shield(String n)
	{
	super(n);	
	mab=0;
		
		if(n.equals("Tower Shield")) //?
		{
			value=35;
			lbs=45;
			ab=3;
			pen=-3;
			sf=30;
		}
		
		if(n.equals("Buckler")) //bow and crossbow can be used
		{
			value=15;
			lbs=5;
			ab=1;
			pen=-1;
			sf=5;
		}
		
		if(n.equals("Wooden Shield"))
		{
			value=5;
			lbs=5;
			ab=1;
			pen=-1;
			sf=5;
		}
		
		
		if(n.equals("Steel Shield"))
		{
			value=20;
			lbs=15;
			ab=2;
			pen=-2;
			sf=15;
		}
		
		if(n.equals("Leather Shield"))
		{
			value=25;
			lbs=10;
			ab=2;
			pen=-1;
			sf=15;
		}
		
	}
	
	
	
	public static Shield create(String n,Char o)
	{
	Shield s=new Shield(n);
	s.setOwner(o);
	return s;	
	}

	
	public static Shield create(String n,int num)
	{
	Shield s=new Shield(n);
	s.amount*=num;
	return s;	
	}
		
	public static Shield createImproved(String n,int arBo) //armor bonus
	{
	Shield s=new Shield(n);
	
	s.name=s.name+" +"+arBo;
	
	s.ab+=arBo;
					
	return s;	
	}
	
	
	public static Shield getShield()
	{
		int p=Dice.roll(100);
		
		if(p>=1&&p<=25)
		return new Shield("Buckler");
		if(p>=26&&p<=50)
		return new Shield("Wooden Shield");
		if(p>=51&&p<=70)
		return new Shield("Leather Shield");
		if(p>=71&&p<=90)
		return new Shield("Steel Shield");
		
		return new Shield("Tower Shield");
	}
	
	
	public void magic(int a)
	{
		mab=a;
	}
	
	
	public int getProtection()
	{
		return (ab+mab);
	}
	
		
	public boolean humanBonus()
	{
		String s="Leather";
		if(name.lastIndexOf(s)!=-1)
		return true;
		
		return false;
	}
	
	
	
	public boolean isMagic()
	{
	if(mab!=0)
	return true;
	
	return false;	
	}
	
	public void setOwner(Char a)
	{
		owner=a;
	}
	
	
	
	public int getPen()
	{
	return pen;	
	}
	
	public int getSf()
	{
	return sf;	
	}
	
	public static boolean existsInList(String n)
	{
		Shield s=new Shield(n);
		if(s.ab==0)
		return false;
		
		return true;
	}
	
	
	
	
	public String write() //set owner, when in C* etc. ? (or ask alla)
	{
	String s=super.write();	
	s="S"+s.substring(1,s.length()-3)+",";
	
	s+=mab+",";
	//s+=	 owner?
	s+=ab+",";
	s+=pen+",";
	s+=sf+",";
	s+="*S";
	return s;
	}
	
	
	
	//S*Steel Shield, or ;;?
	
	public static Shield readS(String s) //careful of "S*S"
	{
		Shield d=new Shield("");
				
		s=s.substring(2);
		
		////System.out.println("S: "+s);
		
		int b=s.indexOf(",");
		int a;
		d.lbs=Double.parseDouble(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.name=s.substring(0,b);
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.value=Double.parseDouble(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		a=Integer.parseInt(s.substring(0,b));
		d.amount=a;
		s=s.substring(b+1);
		
		//shield
	
		b=s.indexOf(",");
		d.mab=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.ab=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.pen=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.sf=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
				
		return d;
	}
	
	
	
	
	/*
	protected int mab; //magic armor bonus
	protected int ab; //armor bonus
	protected int pen; //check (dex only?) penalty
	protected int sf; //spell failure- percentage
	*/
		
	public String toString()
	{
		String s=name;
		s+=" - bonus: "+ab+", ";
		
		if(mab!=0)
		s=s+"magic: "+mab+", ";
		
		s+="penalty: "+pen+", ";
		s+="failure: "+sf+"%";
		
		/*
		s+=", armor bonus: "+ab+", ";
		 if(mab!=0)
		s=s+"magic bonus: "+mab+", ";
		
		s+="check penalty: "+pen+", ";
		s+="spell failure: "+sf+"%";
		*/
		
		return s;
	}
	
}
