
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;



/*
�� 3 ����� ������:
1.���� �������� ������� �-tutorials �� Java
2. ����� LookAndFeel
3. ����� ���� ������ �� Java
��� ���� ����� �� �� �����. 
*/

//tooltip!! (\n) (change color if not possible?)

public class Create extends JPanel implements ActionListener{
	
	//private JTextField text;
	/**
	 *Text that is displayed
	 */
	private JLabel text;
	/**
	 *Option buttons
	 */
	private JButton op1,op2,op3,op4,op5,op6;
	/**
	 *The current options
	 */
	private int op;
	/**
	 *Main menu
	 */
	private Main main;
	/**
	 *The character
	 */
	private Character chara;
	/**
	 *Combo boxes for several uses
	 */
	private JComboBox box;
	/**
	 *Radio buttons for several uses
	 */
	private JRadioButton radio[];
	/**
	 *Texts for several uses
	 */
	private JTextField toadd,points;
	/**
	 *Texts to be displayed
	 */
	private JLabel mes,at[];
	
	/**
	 *Attributes
	 */
	private int str,inl,cha,wis,con,dex;
	/**
	 *Text to be displayed
	 */
	private JTextField text2;
	/**
	 *Check box for male or not question
	 */
	private JCheckBox sex;
	/**
	 *Class, race and name chosen
	 */
	private String clas,race,name;
	/**
	 *If male or not
	 */
	private boolean male;
	/**
	 *Panel for further use
	 */
	private JPanel panel;
	
	public Create(JLabel m,Character c,Main main)
	{
	super();	
	
	this.main=main;
	
	at=new JLabel[6];
	at[0]=new JLabel("Strength:");
	at[0].setToolTipText("Modifies the melee attack and the damage");
	at[1]=new JLabel("Dexterety:");
	at[1].setToolTipText("Modifies the range attack, initiation, AC and reflex save");
	at[2]=new JLabel("Constitution:");
	at[2].setToolTipText("Modifies the HP and fortitude save");
	at[3]=new JLabel("Intelligence:");
	at[3].setToolTipText("Modifies the skill points");
	at[4]=new JLabel("Wisdom:");
	at[4].setToolTipText("Modifies the will save");
	at[5]=new JLabel("Charisma:");
	at[5].setToolTipText("Modifies some skills and spells");
	
	
	
	c=chara;
	mes=m;
	
	points=new JTextField((abilities()-4*6)+"");
	toadd=new JTextField("6");
	points.setVisible(false);
	toadd.setVisible(false);
	points.setEnabled(false);
	
	//setBackground(Color.GREEN);
	setBackground(new Color(128,0,0));
	
	panel=new JPanel();
//	panel.setBackground(Color.GREEN);
	panel.setBackground(new Color(128,0,0));
	

	panel.setLayout(new GridLayout(0,3));
	
	text=new JLabel("Choose your class:");
	text.setBackground(Color.BLUE);
	
	op=1;
	op1=new JButton("Barbarian");
	op1.setToolTipText("HP:12.\nSkill points:4+Intelligence modifier\nSkills:Climb (doubled).");
	op2=new JButton("Paladin");
	op2.setToolTipText("HP:10.\nSkill points:2+Intelligence modifier\nSkills:Heal and Spot."
	+"Spellcaster.");
	op3=new JButton("Rogue");
	op3.setToolTipText("HP:6.\nSkill points:8+Intelligence modifier\nSkills:Pick Pocket and Hide.");
	op4=new JButton("Sorcerer");
	op4.setToolTipText("HP:4.\nSkill points:2+Intelligence modifier\nSkills:Perform and Search."
	+"Spellcaster.");
	op5=new JButton("Ranger");
	op5.setToolTipText("HP:10.\nSkill points:4+Intelligence modifier\nSkills:Craft and Intuit Direction."
	+"Spellcaster from level 4."); //? from 4?
	op6=new JButton("Monk");
	op6.setToolTipText("HP:8.\nSkill points:4+Intelligence modifier\nSkills:Swim (doubled).");
	
	op1.addActionListener(this);
	op2.addActionListener(this);
	op3.addActionListener(this);
	op4.addActionListener(this);
	op5.addActionListener(this);
	op6.addActionListener(this);
	//setLayout(new FlowLayout());
	
	add(text);
	
	text2=new JTextField(24);

	sex=new JCheckBox(" Male",true);
	panel.add(sex);
	text2.setVisible(false);
	sex.setVisible(false);
	panel.add(text2);
	
	box=new JComboBox(abilitiesv());
	//box.addActionListener(this);
	box.setVisible(false);
	box.setToolTipText("Score to give");
	//box.setSize(20);
	
	radio=new JRadioButton[6];
	ButtonGroup group=new ButtonGroup();
	
	for(int i=0;i<6;i++)
	{
	radio[i]=new JRadioButton(at[i].getText());
	radio[i].setVisible(false);
	group.add(radio[i]);
	}
	radio[0].setSelected(true);
	
	panel.add(radio[0]);
	panel.add(at[0]);
	panel.add(op1);
	panel.add(radio[1]);
	panel.add(at[1]);
	panel.add(op2);
	panel.add(radio[2]);
	panel.add(at[2]);
	panel.add(op3);
	panel.add(radio[3]);
	panel.add(at[3]);
	panel.add(op4);
	panel.add(radio[4]);
	panel.add(at[4]);
	panel.add(op5);
	panel.add(radio[5]);
	panel.add(at[5]);
	panel.add(op6);
	panel.add(box);
	
	panel.add(points);
	panel.add(toadd);
	
	add(panel);
	
	for(int i=0;i<6;i++)
	at[i].setVisible(false);


//	//System.out.println(""+panel.getComponentCount());
	//op=3;	
	}
	
	public void paintComponent(Graphics g)
	{
	super.paintComponent(g);
	
	
	/*
	if(panel.getComponentCount()==0)
	{
		g.drawString("Character Created.",50,50);
	}
	*/
	//g.drawRect(0,0,500,500);
	}
	
	public Dimension getPreferredSize()
	{
	return new Dimension(200,200);
	}
	
	
	public void actionPerformed(ActionEvent e)
	{
	
	if(e.getSource() instanceof JButton)
	{
	
		if(((JButton)(e.getSource())).getText().equals("Create Character"))
		{
			op++;
			
			/*
			str=Integer.parseInt(op1.getText());
			dex=Integer.parseInt(op2.getText());
			con=Integer.parseInt(op3.getText());
			inl=Integer.parseInt(op4.getText());
			wis=Integer.parseInt(op5.getText());
			cha=Integer.parseInt(op6.getText());
			*/
		
			//creation of character
			
			Component[] com=panel.getComponents();
			
			
			int l[]=new int[10];
			for(int i=0;i<10;i++)
			l[i]=Integer.parseInt(((JButton)(com[i*2+1])).getText());
			

			
			
			int hp=0;
			//int skp=0; //skill points
					
			if(clas.equals("Barbarian"))
			hp=12;
			if(clas.equals("Paladin"))
			hp=10;
			if(clas.equals("Rogue"))
			hp=6;
			if(clas.equals("Sorcerer"))
			hp=4;
			if(clas.equals("Ranger"))
			hp=10;
			if(clas.equals("Monk"))
			hp=8;
			
			
//	public Character(String name,int hp,int str,int dex,String race,boolean m/**/,int co,int in,int wi,int ch,String c)

			//Skills s=new Skills(chara,l[0],l[1],l[2],l[3],l[4],l[5],l[6],l[7],l[8],l[9]);	
			chara=new Character(name,hp+Char.mod(con),str,dex,race,male,con,inl,wis,cha,clas,new Skills(chara,l[0],l[1],l[2],l[3],l[4],l[5],l[6],l[7],l[8],l[9])); //s
			
			panel.removeAll();
			text.setText("Character Created.");
			
			chara.setPlace(Dice.roll(Map.dim)-1,Dice.roll(Map.dim)-1);
			main.setCh(chara);
			main.enabled();
			//repaint();
			
			
			//things when creating: //del
			
			//Enemy en=new Enemy("Skeleton",null);
			////System.out.println(chara.getSpells().useSpell(0,en));
			////System.out.println(chara.attack()+","+chara.NoA());
			/*
			chara.getSpells().addDisease("dsfs",1,5);
			chara.getSpells().addDisease("fdf",3,14);
			
			String s=chara.getSpells().write();
			//System.out.println(s+"\n");
			Spells sp=Spells.read(s);
			String s2=sp.write();
			//System.out.println(s2+"\n");
			if(s2.equals(s))
			//System.out.println("v");
			else
			//System.out.println("x");
			*/
		}
		
		
		
		if(op==5)
		{
			String c=toadd.getText();
			if(!c.equals(""))
			{
			char[] s=c.toCharArray();
			if(s.length>1)
			{
				if(s[0]=='-'&&s[1]>'0'&&s[1]<='9'&&s.length==2)
				{
					JButton t=(JButton)(e.getSource());
					int a=Integer.parseInt(c);
					int min=Integer.parseInt(t.getText());
					
					if(min+a<0)
					{
					a=-min;
					mes.setText("You can't have a skill score lower than 0.");
					}
					
					t.setText((Integer.parseInt(t.getText())+a)+"");
					points.setText((Integer.parseInt(points.getText())-a)+"");
				}
				else
				mes.setText("Must be a number between 1 to 9 (or negative).");
			}
			else
				if(s[0]>'0'&&s[0]<='9')
				{
					int a=Integer.parseInt(c);
					int max=Integer.parseInt(points.getText());
					
					if(a>max)
					{
					a=max;
					mes.setText("You can't divide more points that you have left.");
					}

					JButton t=(JButton)(e.getSource());
					int sl=Integer.parseInt(t.getText());
					
					if(a+sl>5)
					{
					a=5-sl;	
					mes.setText("You can't have s skill score greater than 5 (your level+4).");
					}
					
					t.setText((sl+a)+"");
					points.setText((Integer.parseInt(points.getText())-a)+"");
					
									
										
					
				}
				else
				mes.setText("Must be a number between 1 to 9 (or negative).");
			}
			
			
			
			int nof=0; //number of full
			Component[] com=panel.getComponents();	
			for(int i=0;i<10;i++)
			{
			////System.out.println(com[i*2+1].getClass());
			int aa=Integer.parseInt(((JButton)(com[i*2+1])).getText());
			if(aa==5)//lvl+4
			nof++;
			}
			
			if(Integer.parseInt(points.getText())==0||nof==10)
			{
			//	Component[] com=panel.getComponents();
				//(com[panel.getComponentCount()-1])
				if(!((com[panel.getComponentCount()-1]) instanceof JButton))
				{
				JButton j=new JButton("Create Character");
				j.setToolTipText("Character Finished");
				j.addActionListener(this);
				panel.add(j);
				}
				//if(!(((JButton)(com[panel.getComponentCount()-1])).getText().equals("Create Character")))
				
			}
			else
				{
//				Component[] com=panel.getComponents();
				if(((com[panel.getComponentCount()-1]) instanceof JButton))
				if(((JButton)(com[panel.getComponentCount()-1])).getText().equals("Create Character"))
				panel.remove(panel.getComponentCount()-1); //17?
				}
			//what function?
			
						//	private int str,inl,cha,wis,con,dex;
			
		}
	
		
		if(((JButton)(e.getSource())).getText().equals("Divide Skills"))
		{
		op++;	

		
		panel.setLayout(new GridLayout(0,2));
		
		str=Integer.parseInt(op1.getText());
		dex=Integer.parseInt(op2.getText());
		con=Integer.parseInt(op3.getText());
		inl=Integer.parseInt(op4.getText());
		wis=Integer.parseInt(op5.getText());
		cha=Integer.parseInt(op6.getText());
		
		
		points.setVisible(true);
		toadd.setVisible(true);
			
		
		panel.removeAll();
		//remove(panel);
		
		//text.setText("Divide your skills.");
		text.setText("");
		//text.setVisible(false);
			
		/*
		panel=new JPanel();
		panel.setBackground(Color.GREEN);
		panel.setLayout(new GridLayout(0,2));
		add(panel);
		*/
		
		////System.out.println(text.getText());
		
		for(int i=0;i<10;i++)
		{
		JLabel lab=new JLabel();
		switch(i)
		{
		case 0:
		lab.setText("Swim");
		lab.setToolTipText("Swims at water [modified by Strength]");
		break;	
		case 1:
		lab.setText("Climb");
		lab.setToolTipText("Climbs at rocks [modified by Strength,affected by armor penalty]");
		break;	
		case 2:
		lab.setText("Perform");
		lab.setToolTipText("Performs in front of an audience- a way to gain money legally [modified by Charisma]");
		break;	
		case 3:
		lab.setText("Pick Pocket");
		lab.setToolTipText("Steals money or items [modified by Dexterety,affected by armor penalty]");
		break;	
		case 4:
		lab.setText("Search");
		lab.setToolTipText("Searches for items [modified by Intelligence]");
		break;	
		case 5:
		lab.setText("Spot");
		lab.setToolTipText("Sees if there are characters in the area [modified by Wisdom]");
		break;	
		case 6:
		lab.setText("Hide");
		lab.setToolTipText("Hides from people (for sneaking)- a skill against Spot [modified by Dexterety,affected by armor penalty]");
		break;	
		case 7:
		lab.setText("Intuit Direction");
		lab.setToolTipText("Senses where the right way is [modified by Wisdom]");
		break;	
		case 8:
		lab.setText("Heal");
		lab.setToolTipText("Heals yourself [modified by Wisdom]");//or someone else ?
		break;	
		case 9:
		//lab.setText("Concentrate");
		//lab.setToolTipText("Needed for casting spells and performing some skills [modified by Constitution]"); //skills also?
		lab.setText("Craft");
		lab.setToolTipText("Builds all sort of items [modified by Intelligent]"); //skills also?
		break;	
		}
				
		JButton but=new JButton("0");
		but.setToolTipText("Changes the ability by the Modifier");
		but.addActionListener(this);
		panel.add(lab);
		panel.add(but);
		}
		
		//int sm=0;
		int sk=0;
		int m=Char.mod(inl);
		if(m<0)
		m=0;
		
		if(clas.equals("Barbarian"))
		sk=2+m;
		if(clas.equals("Paladin"))
		sk=2+m;
		if(clas.equals("Rogue"))
		sk=8+m;
		if(clas.equals("Sorcerer"))
		sk=2+m;
		if(clas.equals("Ranger"))
		sk=4+m;
		if(clas.equals("Monk"))
		sk=4+m;
		
		
		if(race.equals("Human"))
		sk++; //+2
		
		sk*=2;
		
		//if(sk<0)
		//sk=0;
		
		points.setText(""+sk);
		toadd.setText("1");
		panel.add(points);
		panel.add(toadd);
		
		//maximum for skill=level+3		
		
		}
		
		
		if(op==4)
		{
			
			if(box.getItemCount()>0)
			{
				
				String s=(String)(box.getSelectedItem());
				//int n=Integer.parseInt(s);
				
				for(int i=0;i<6;i++)
				if(radio[i].isSelected())
				{	
					JButton o=new JButton();
					String s2="";
					switch(i)
					{
					case 0:
					o=op1;
					s2="Strength: ";
					break;	
					case 1:
					o=op2;
					s2="Dexterety: ";
					break;	
					case 2:
					o=op3;
					s2="Constitution: ";
					break;	
					case 3:
					o=op4;
					s2="Intelligence: ";
					break;	
					case 4:
					o=op5;
					s2="Wisdom: ";
					break;	
					case 5:
					o=op6;
					s2="Charisma: ";
					break;	
					}
					
					int n=0;
					
					if(!o.getText().equals(""))
					{
						box.addItem(o.getText());
						n=box.getItemCount()-2;
					}
					
					o.setText(""+s);
					radio[i].setText(s2+o.getText());
					box.removeItem(s);
					
					if(box.getItemCount()>0)
					box.setSelectedIndex(n);
					
					int n2=i+1;
					if(n2>5)
					n2=0;
					
					radio[n2].setSelected(true);
					
					i=6;
					//change button
					
					
				}
				
				
			}

			if(box.getItemCount()==0)
			{
			
			JButton j;
			Component[] com=panel.getComponents();
			for(int i=0;i<com.length;i++)
			if(com[i] instanceof JButton)
			if(((JButton)(com[i])).getText().equals("Choose"))
			{
			JButton jj=(JButton)(com[i]);
			jj.setText("Divide Skills");
			jj.setToolTipText("Finished dividing");
			}
									
			}
			/*
			String c=toadd.getText();
			if(!c.equals(""))
			{
			char[] s=c.toCharArray();
			if(s.length>1)
			{
				if(s[0]=='-'&&s[1]>'0'&&s[1]<='9'&&s.length==2)
				{
					JButton t=(JButton)(e.getSource());
					int a=Integer.parseInt(c);
					int min=Integer.parseInt(t.getText());
					
					if(min+a<4)
					{
					a=-min+4;
					mes.setText("You can't have an ability score lower than 4.");
					}
					
					t.setText((Integer.parseInt(t.getText())+a)+"");
					points.setText((Integer.parseInt(points.getText())-a)+"");
				}
				else
				mes.setText("Must be a number between 1 to 9 (or negative).");
			}
			else
				if(s[0]>'0'&&s[0]<='9')
				{
					int a=Integer.parseInt(c);
					int max=Integer.parseInt(points.getText());
					
					if(a>max)
					{
					a=max;
					mes.setText("You can't divide more points that you have left.");
					}
					JButton t=(JButton)(e.getSource());
					t.setText((Integer.parseInt(t.getText())+a)+"");
					points.setText((Integer.parseInt(points.getText())-a)+"");
					
					
					
					
					
										
					
				}
				else
				mes.setText("Must be a number between 1 to 9 (or negative).");
			}
			
			
			
			//JTextField
			if(Integer.parseInt(points.getText())==0)
			{
				Component[] com=panel.getComponents();
				//(com[panel.getComponentCount()-1])
				if(!((com[panel.getComponentCount()-1]) instanceof JButton))
				{
				JButton j=new JButton("Divide Skills");
				j.setToolTipText("Finished dividing");
				j.addActionListener(this);
				panel.add(j);
				}
				//if(!(((JButton)(com[panel.getComponentCount()-1])).getText().equals("Create Character")))
				
			}
			else
				{
				Component[] com=panel.getComponents();
				if(((com[panel.getComponentCount()-1]) instanceof JButton))
				if(((JButton)(com[panel.getComponentCount()-1])).getText().equals("Divide Skills"))
				panel.remove(panel.getComponentCount()-1); //17?
				}
			//what function?
			
						//	private int str,inl,cha,wis,con,dex;
			*/
			
		}
	
	
		if(op==3)
		{
			String s=text2.getText();
			if(!s.equals("")&&s.indexOf('/')==-1&&s.indexOf("\\")==-1&&s.indexOf(';')==-1&&s.indexOf(',')==-1&&s.indexOf('*')==-1&&s.indexOf('.')==-1&&s.indexOf('<')==-1&&s.indexOf('>')==-1&&s.indexOf('|')==-1&&s.indexOf('?')==-1&&s.indexOf(':')==-1&&s.indexOf('"')==-1&&s.indexOf(' ')==-1)
			{
			text.setText("Divide your points to your attributes.");
			
			name=s;	
			male=sex.isSelected();
			text2.setVisible(false);
			sex.setVisible(false);
			op1.setVisible(false);
			/*				
			op1.setVisible(true);
			op2.setVisible(true);
			op3.setVisible(true);
			op4.setVisible(true);
			op5.setVisible(true);
			op6.setVisible(true);
			//panel.add(new JLabel("Strength"));
			op1.setText("-");
			op1.setToolTipText("Changes the ability by the Modifier");
			op2.setText("-");
			op2.setToolTipText("Changes the ability by the Modifier");
			op3.setText("-");
			op3.setToolTipText("Changes the ability by the Modifier");
			op4.setText("-");
			op4.setToolTipText("Changes the ability by the Modifier");
			op5.setText("-");
			op5.setToolTipText("Changes the ability by the Modifier");
			op6.setText("-");
			op6.setToolTipText("Changes the ability by the Modifier");

				
			//points.setVisible(true);
			//points.setToolTipText("Points left");
			//toadd.setVisible(true);
			//toadd.setToolTipText("The Modifier");
			*/
			
			box.setVisible(true);
			
			//JButton j=new JButton("Divide Skills");
			//j.setToolTipText("Finished dividing");
			JButton j=new JButton("Choose");
			j.setToolTipText("Gives the score to the chosen ability");
			j.addActionListener(this);
			panel.add(j);
			
			op1.setText("");
			op2.setText("");
			op3.setText("");
			op4.setText("");
			op5.setText("");
			op6.setText("");
			
			for(int i=0;i<6;i++)
			{
				//at[i].setVisible(true);
				radio[i].setVisible(true);
			}
												
			op++;
			}
			else
				if(s.equals(""))
				mes.setText("You have to have a name.");
				else
				{
				char g='"';
				mes.setText("You can't have / \\ ? | < > * "+g+" . , : ; or space in the name.");
				}
			
		}
		
		
		if(op==2)
		{
			race=((JButton)(e.getSource())).getText();
			
			op++;
			text.setText("Choose name and sex:");
			
			text2.setVisible(true);
			sex.setVisible(true);
				
			op2.setVisible(false);
			op3.setVisible(false);
			op4.setVisible(false);
			op5.setVisible(false);
			op6.setVisible(false);
			op1.setText("OK");
			op1.setToolTipText("Chosen");
		}
		
		if(op==1)
		{
			clas=((JButton)(e.getSource())).getText();
			
			op++;
			text.setText("Choose your race:");
			op1.setText("Human");
			op1.setToolTipText("Speed=6.\nSkill Points bonuses.\n"
			+"Bonus to strength and constitution.\nBonus with Shields.");
			op2.setText("Elf");
			op2.setToolTipText("Speed=6.\nBonus to Dexterety, penalty to Constitution.\n"
			+"Bonus to Spot and Search.\nBonus to Will save.\nBonus with Bows.");
			op3.setText("Dwarf");
			op3.setToolTipText("Bonus to AC.\nSpeed=3.\nBonus to Constitution, penalty to Charisma.\n"
			+"Bonus to Will save.\nBonus against Orcs and Goblins.\nBonus with Axes.");
			op4.setText("Halfling");
			op4.setToolTipText("Bonus to AC.\nSpeed=3.\nBonus to Dexterety, penalty to Strength.\n"
			+"Bonus to Hide and Climb.\nBonus to all saves.\nBonus with Short weapons.");
			op5.setVisible(false);
			op6.setVisible(false);
			
		}
	}
	
	
	/*if(e.getSource() instanceof JComboBox)
	{
		
	
	
		
		
	}
	*/
	
	} //end actions

	
	public int abilities()
	{
		int s=0;
		for(int i=0;i<6;i++)
		{
			
			int c=0;
			int min=6;
			
			for(int j=0;j<4;j++)
			{
				int a=(int)(Math.random()*6+1);
				
				////System.out.println(a+"");
				
				if(a<min)
				min=a;
				c+=a;
			}
			c-=min;
			
		////System.out.println(c+"");
		s+=c;
		}
		
	////System.out.println(s+"");
	return s;
	}
	
	
	public Vector abilitiesv()
	{
	
	Vector v=new Vector();
			
		for(int i=0;i<6;i++)
		{
			
			int c=0;
			int min=6;
			
			for(int j=0;j<4;j++)
			{
				int a=(int)(Math.random()*6+1);
				
				////System.out.println(a+"");
				
				if(a<min)
				min=a;
				c+=a;
			}
			c-=min;
			
		////System.out.println(c+"");
		v.add(""+c);
		}
		
	////System.out.println(s+"");
	return v;
	}
	
	
}

