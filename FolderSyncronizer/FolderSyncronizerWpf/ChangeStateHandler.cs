﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FolderSyncronizerWpf
{
    class ChangeStateHandler
    {
        private readonly int _maxItems;
        private int _itemsHandled;

        public ChangeStateHandler(int maxItems)
        {
            _maxItems = maxItems;
            _itemsHandled = 0;
        }

        public void AddHandledItems(int count = 1)
        {
            _itemsHandled+= count;
        }

        public bool IsDone => _itemsHandled >= _maxItems;
    }
}
