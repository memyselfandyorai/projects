import java.util.*;
import java.awt.*;
import javax.swing.*;

public class Pack {
	
	
	private Vector cards;
		
	
	
	public Pack(boolean deck)
	{
		cards=new Vector();
		
		
		if(!deck)
		while(cards.size()<52)
		{
		int n=(int)(Math.random()*13)+2;
		int s=(int)(Math.random()*4);
					
			if(!hasCard(n,s))
			{
				Card c=new Card(n,s);
				cards.add(c);
			}
		
		}
		
	}
	
	
	public Pack(Pack copy)
	{
		cards=new Vector(copy.cards);
	}
	
	
	
	public boolean hasCard(int n,int s)
	{
		for(int i=0;i<cards.size();i++)
		{
		Card c=(Card)cards.elementAt(i);
		if(c.same(n,s))	
		return true;
		}
		
	return false;	
	}
	
	
	public Vector cards()
	{
	return cards;	
	}
	
	
	
	public Card draw()
	{
	Card c=(Card)cards.elementAt(0);
	cards.remove(0);
	return c;		
	}
	
	
	public void addCard(Card d)
	{
	cards.add(0,d);
	}
	
	
	public String inst()
	{
	if(cards.size()==0)
	return "Put any card.";
	
	Card d=(Card)cards.elementAt(0);
	if(d.num()!=3&&d.num()!=7&&d.num()!=14)
	return "Put a card higher or equals to "+d.getNum()+"."; //and special? add?
	
	if(d.num()==14)
	return "Put an ace or a special card.";
	
	if(d.num()==7)
	return "Put a card lower or equals to 7.";
	
	//==3:
	
	Pack p=new Pack(this);
	p.cards.remove(0);
	return p.inst();
	}
	
	
	
	
	
	public int getNum()
	{
	if(cards.size()==0)
	return 2;
	
	int n=((Card)cards.elementAt(0)).num();
	
	if(n==3)
	{
	Pack p=new Pack(this);	
	p.cards.remove(0);	
	return p.getNum();	
	}
	
	
	return n;
	}
	
	
	public int burn(JPanel m,Player player,int order,int size)
	{
	
	boolean b=false;	
	String mes="";
	
	if(cards.size()>0)
		if(((Card)cards.elementAt(0)).num()==10)
		{
			b=true;
			mes="10 burns";
		}
	
	if(!b)	
	if(cards.size()>=4)
	{
	b=true;
	int n=((Card)cards.elementAt(0)).num();
		for(int i=1;i<4;i++)
		if(n!=((Card)cards.elementAt(i)).num())
		b=false;
	b=b||row()==4;
	mes="same card in a row 4 times ["+Card.getNum(n)+"]";
	}
		
		
	if(b)
	{
		if(!player.finished())
		{
			order--;
			if(order<0)
			order=size-1;
		}
	
	cards.clear();
	JOptionPane.showMessageDialog(m,"Burn! ("+mes+")","Stack emptied",JOptionPane.INFORMATION_MESSAGE);	
	}
	
	
	return order;
	}
		
	
	
	
	public int row() //with 3
	{
	if(cards.size()==0)
	return 0;
	
	Card c=(Card)cards.elementAt(0);
	
	int n=c.num();
	int s=1;
	
	if(n==3)
	{
	n=1;
	s=0;	
	}
	
	for(int i=1;i<cards.size();i++)
	{
		Card d=(Card)cards.elementAt(i);
		int m=d.num();
		
		if(n!=1)		
		if(m==n)
		s++;
		else
			if(m!=3)
			break;		
			
			
		if(n==1&&m!=3)
		{
		n=m;	
		s++;	
		}
		
				
	}
	
		
	return s;
	}
	
	
	
	public String rowf()
	{
		
	if(cards.size()<4)
	return "";	
		
	int s=1;	
	
	int n=((Card)cards.elementAt(0)).num();
	
	if(n!=3)
	return "";	
		
	for(int i=1;i<4;i++)	
	{
		if(((Card)cards.elementAt(i)).num()==n)
		s++;
		else
		break;
	}
		
		
		
		
		
	return " and "+s+" [card '3'] in a row";	
	}
	
	
	
}
