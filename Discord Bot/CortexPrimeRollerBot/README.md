# Cortex Prime roller bot

This is a bot meant to ease on playing [Cortex Prime](https://www.cortexrpg.com/) (RPG game) using Discord. 
Specifically, it supports a lot of useful features for rolling dice and managing dice-pools and counters.

<br/><br/>
<a href="./readme/gallery/Original-bot.jpeg">
    <img class="bot" src="./readme/gallery/bot-small.jpeg" alt="Bot's Avatar" />
</a>

<a name="install"></a>

## Installation

Use the [invite link](https://discord.com/api/oauth2/authorize?client_id=947923931615088682&scope=identify+bot+applications.commands&permissions=2214669313) for Discord.

The permissions are required for the bot's functionality. E.g., for managing pinned messages and for successfully storing data.

## Table of contents

* [Installation](#install)
* [Commands](#commands)
    * [Roll Commands](#roll_commands)
        * [/r](#roll)
        * [/pool-roll](#pool_roll)
    * [Follow-up Commands](#follow_commands)
        * [/append](#append)
        * [/keep](#keep)
        * [/reroll](#reroll)
    * [Dice Pool Commands](#pool_commands)
        * [/pool-add](#pool_add)
        * [/pool-remove](#pool_remove)
        * [/pool-move](#pool_move)
        * [/pool-clear](#pool_clear)
        * [/pool-roll](#pool_roll)
        * [/pool-step](#pool_step)
        * [/pool-show](#pool_show)
    * [Counter Commands](#counter_commands)
        * [/counter-set](#counter_set)
        * [/counter-modify](#counter_modify)
        * [/counter-remove](#counter_remove)
        * [/counter-show](#counter_show)
    * [Saved-dice Commands](#save_commands)
        * [/save](#save)
        * [/save-delete](#save_delete)
        * [/save-show](#save_show)
    * [General Commands](#general)
        * [/pin-summary](#pin_summary)
        * [/help](#help)
 * [Known limitations](#limitations)
 * [Privacy agreement](#privacy)
 * [Legal](#legal)
 * [For Developers](#for_dev)
 * [Support](#support)
 * [Image Gallery](#gallery)
        
        

<a name="commands"></a>

## Commands

Note: All commands execute on the channel level (and not guild-level, which is the server containing the channels).

Glossary:

* `Dice format` (alternatively: **dice formatted**) - is a space-separated list of numbers, representing die types. The `3d10` annotation is supported as well. Example: `4 10 4 2d6 1D10`.
* `Supports wildcard` - `*` is a wildcard that means "all". Note that the "all" wildcard is ignored for saved dice created with `wildcard-mode`=`OPT_OUT`.
* `Saved dice` - a mechanism to store specific dice for usage by other (non Game-Master) players. You can instead use dice pools, but players often forget bonuses others created for them.

<a name="roll_commands"></a>

### Roll commands

<a name="roll"></a>
#### /r

Rolls dice using the Cortex Prime method:

* Rolls dice.
* Each `1` counts as a "hitch".
* The score is using 2 non-hitched die results.
* The effect is using one of the remainder non-hitched die (its type).
* A result is built from both a numerical score and an effect which is a die type.
* There could be multiple results of interest: e.g., a lesser scope but higher effect.

Notes:

* Some dice can be of a special "pool" that cannot be hitched nor used for effect (makred with `[...]` in results).
* The command defaultly adds all saved dice created with `wildcard-mode`=`OPT_IN`. Using explicit saved names in `save-opt-out` or `save-opt-in` will change this behavior.

*Parameters:*

* `dice` (*required*) - **dice format**.
* `keep` (*default*: 2) - how many dice to use for score (these will be unused for effect).
* `effect-mod` (*default*: 0) - modifies the final effect by a number of steps. Examples: `1` - increases the effect by 1 (e.g., D6 -> D8), `-2` - decreases the effect by 2 (e.g., D8 -> D4).
* `hitch` (*default*: 1) - the number from which a roll counts as a hitch and is ignored from the result pool (for the score and effect).
* `die-mod` (*default*: 0) - modifies the input dice pool: Runs its absolute-value times. 
When positive - steps up the minimal die. When negative - steps down the max die. Example: for `2` die-mod and `2D4` it will step up D4 to D6 and then the other D4 to D6.
* `mod` (*default*: none) - automatically parameterizes special cases.
    * `SHOCK` - adds a D6, increases the effect, but steps down maximal die.
    * `OBLIVIOUS` - hitch is 2.
* `extra-effect-dice` (**dice formatted**) - additional dice to pool, while each one adds an additional effect-die to the result (e.g., effect used for multiple targets).
* `focus` (**dice formatted**) - die types to merge (i.e., removes one and steps up the other). This runs after adding save/pool results. Each die type merges one pair. Example: using focus `6 6` on dice `5D6` will result in `1D6 2D8`.
* `save-opt-out` (*default*: empty) - comma-separated saved dice names to exclude from roll. **Supports wildcard**.
* `save-opt-in` (*default*: `*`) - comma-separated saved dice names to include in roll. **Supports wildcard**.
* `pools` (*default*: empty) - comma-separated dice pool names to include in this roll.

<a name="pool_roll"></a>

#### /pool-roll

Rolls a specific pool. See [/r command](#r) for more details.

Note: The command does not add all saved dice created with `wildcard-mode`=`OPT_IN` by default. You can use saved names explicitly in `save-opt-out` or `save-opt-in` to change this behavior.

*Parameters:*

* `names` (*required*) - comma separated names of the pools to roll. Use `*` to roll all.
* `dice` (**dice formatted**) - extra dice to add to the roll.
* `keep` (*default*: 2) - how many dice to use for score (these will be unused for effect).
* `save-opt-out` (*default*: empty) - comma-separated saved dice names to disclude from roll. **Supports wildcard**.
* `save-opt-in` (*default*: empty) - comma-separated saved dice names to include in roll. **Supports wildcard**.
* `extra-effects` (*default*: 0) - how many extra dice (beyond the default `1`) to use as effect.

<a name="follow_commands"></a>

### Follow up commands

These commands are ran on the last roll a specific user performed in a specific channel, usually creating a new roll.

The last roll results are saved for up to ~10 minutes.

<a name="append"></a>

#### /append

When you already rolled, but need to add additional dice (e.g., forgot to add a die to the pool). This keeps the last rolled dice pool **and** its results, and adds additional die rolls.

This replaces the last saved roll.

*Parameters:*

* `dice` (*required*) - **dice formatted**.

<a name="keep"></a>

#### /keep

When you want to check if adding additional dice to the score of your last roll will help you (e.g., to figure out if you want to use a plot-point to increase the score).

This does not replace the last saved roll.

*Parameters:*

* `keep` (*default*: 3) - how many dice to take for the score.

<a name="reroll"></a>

#### /reroll

Rerolls the last roll (e.g., using a special ability), reusing that roll's accumulated dice-pool in addition to the `hitch`, `effect-mod` and `keep` settings. 

This replaces the last saved roll.

*Parameters:*

* `dice` (**dice formatted**) - extra dice to add to roll.
* `step-up` (*default*: none) - a die type to step up/down.
* `step-up-mod` (*default*: 1) - the number of steps to use for step up/down. If negative, it's a step down.


<a name="pool_commands"></a>

### Dice Pool Commands

Stores pools of dice by name. Can later be used to roll the pools, or augment another roll.

The main usage is the `doom pool`, but it can be utilized to store other attributes which consist of dice (such as *complications* or *crisis pools*).

<a name="pool_add"></a>

#### /pool-add

Adds dice to an existing pool, or creates one with these dice if it doesn't exist. Pool can later be used to automatically roll the dice in the pool.

Note pool rolls are defaultly opted out from saved dice, but can be opted in manually or by using the wildcard `*` to include all `OPT_IN` dice. 

This command should be mainly used by the Game-Master. Most common use case is the "doom pool". 

*Parameters:*

* `name` (*required*) - unique name of the pool.
* `dice` (*required*, **dice formatted**) - the dice to add to the pool.

<a name="pool_remove"></a>

#### /pool-remove

Removes dice from an existing pool.

*Parameters:*

* `name` (*required*) - name of the pool. 
* `dice` (*required*, **dice formatted**) - the dice to remove from the pool.

<a name="pool_move"></a>

#### /pool-move

Moves dice from one pool to another. Will fail if source pool does not exist or does not contain the expected dice. Can create a new dice pool.

*Parameters:*

* `from` (*required*) - the source pool to move the dice from.
* `to` (*required*) - the target pool to move the dice to. 
* `dice` (*required*, **dice formatted**) - the dice to move.
    
<a name="pool_clear"></a>

#### /pool-clear

Removes a pool from the pool list.

*Parameters:*

* `names` (*required*) - comma separated list of pool names. Use `*` to delete all pools from this channel.

<a name="pool_step"></a>

#### /pool-step

Steps up or down a single die in a specific pool. 

Note this does not remove a stepped down D6, since the pool can be used to represent complications.

*Parameters:*

* `name` (*required*) - name of an existing pool.
* `die` (*required*) - the die to change.
* `mod` (*required*) - direction of the step.
    * `UP` - steps **up**.
    * `DOWN` - steps **down**.
* `steps` (*default*: 1) - number of steps.

<a name="pool_show"></a>

#### /pool-show

Shows all existing pools in this channel.

#### /pool-roll

See [/pool-roll](#pool_roll).

<a name="counter_commands"></a>

### Counter Commands

Counters are useful for storing a per-player integer values. Common examples can be experience points and plot-points.

Counters are not limited to XP and PP, though they're its main use-case, and thus are supported as built-in types.

Counters are stored per-player and per-channel. The default player name is the nickname of the one initiating the command.

<a name="counter_set"></a>

#### /counter-set

Sets a number for a player that can be easily increased or decreased (a.k.a. `counter`). Overrides value if the counter for the player in that channel already exists.

Main use cases: plot-points and experience points.

*Parameters:*

* `who` (*default*: the initiating user's nickname, or username if it doesn't exist) - the name of the player.
* `name` (*default*: `pp`) - the name of the counter.
* `value` (*default*: 2) - the value to set the counter to.
* `type` (*default*: none) - a built in counter name. If used, replaces the `name` parameter.
    * `pp` - plot points.
    * `xp` - experience points.

<a name="counter_modify"></a>

#### /counter-modify

Modifies a counter for a player in this channel. If the counter doesn't exit, creates it with the `amount` as its value.

*Parameters:*

* `who` (*default*: the initiating user's nickname, or username if doesn't exist) - the name of the player.
* `name` (*default*: `pp`) - the name of the counter.
* `amount` (*default*: 1) - a number to modify the existing counter by (can be negative). Used to increase or decrease the value.
* `type` (*default*: none) - a built in counter name. If used, replaces the `name` parameter.
    * `pp` - plot points.
    * `xp` - experience points.


<a name="counter_remove"></a>

#### /counter-remove

Removes a counter from the list for a player in this channel. 

*Parameters:*

* `who` (*default*: the initiating user's nickname, or username if doesn't exist) - the name of the player.
* `name` (*default*: `pp`) - the name of the counter.
* `type` (*default*: none) - a built in counter name. If used, replaces the `name` parameter.
    * `pp` - plot points.
    * `xp` - experience points.

<a name="counter_show"></a>

#### /counter-show

Shows all existing counters for all players in this channel.

<a name="save_commands"></a>

### Saved dice

<a name="save"></a>

#### /save

Saves extra dice to be used automatically by others in this channel. Overrides existing saved name if exists.

The bonus is saved for up to ~4 hours.

*Parameters:*

* `dice` (*required*, **dice formatted**) - extra dice that will be added to other rolls. By default, these will be added automatically to all `/r` rolls, but will not be added to `/pool-roll`.
* `name` (*required*) - unique name for this special bonus. Can be use to explicitly opt in or out from using it.
* `lifespan` (*required*) - for how long the bonus lasts.
    * `SCENE` - saves dice for the whole session. You must delete it manually at the end of the scene.
    * `ROUND` - saves dice for a single use per user. Recommendation: delete it manually when effect is done, especially when someone didn't get a chance to use it.
* `wildcard-mode` (*default*: OPT_IN) - mode of how to defaulty treat wildcards for these saved dice. Is this opted in by default, or not?
    * `OPT_IN` - defaulty added to all `/r` rolls. Can be explicitly opted out. Not added to `/pool-roll`, but can be explicitly opted in. Using `*` for opt-in or opt-out includes this.
    * `OPT_OUT` - not added defaulty to `/r` nor `/pool-roll`. Should be explicitly opted in. Using `*` for opt-in or opt-out would ignore this.

<a name="save_delete"></a>

#### /save-delete

Deletes saved names. Should be used when they are no longer needed.

*Parameters:*

* `name` (*required*) - comma-separated list of names to delete. Use `*` to delete all saved names in this channel.

<a name="save_show"></a>

#### /save-show

Shows all saved names in this channel. Mentions explicitly if the user initiating the command already used up that save when its lifespan is `ROUND`.

<a name="general"></a>

### General Commands

<a name="pin_summary"></a>

#### /pin-summary

Pins a new summary message to the channel. The message updates dynamically each time a relevant command is executed (even `-show` commands).

The summary message includes the `-show` data from pools, counters and saved dice.

It is recommended to recreate the message or run `/pool-show` at the start of each session to keep it updated. Reason: Some data expires (like saved dice), but only commands update the pinned message.

**Important**: `Manage Messages` bot permission is mandatory for this action.

<a name="help"></a>

#### /help

Adds a helpful description per specific subject.

*Parameters:*

* `subject` (*required*) - the subject to need help with.
    * `general` - general info, linking to documentations.
    * `/r mods` - details about the built-in mods for rolling dice.
    * `save lifespan` - details about how saved-dice lifespan works.

<a name="limitations"></a>

## Known limitations

### Scale and storage
This is a community contribution project. It assumes small-scale needs.
If the bot becomes slow or faulty, feel free to contact the developer.

Both the server and the storage are best-effort:

* It's possible the hosting server will be down, and it could take a minute until it's back on.
* The storage solution is a workaround that might fail if used in parallel by lots of players.

Some error messages say "contact developer". If they repeat, feel free to do so.

If I see this bot will be commonly used, I might consider other solution tiers. But I can't guarantee I'll have the resources to spend on other solutions.


### Rate limiting in Discord API
Sometimes there is a `You are being rate limited` error from Discord API. This can cause errors such as not updating the pinned message. You can try again later.

<a name="privacy"></a>

## Privacy Agreement

See [Privacy Agreement](https://sites.google.com/view/youreye-communityprojects/cortex-prime-discord-bot#h.jhkjxyd40jqi) on my Google Sites.

<a name="legal"></a>

## Legal

All rights reserved for Yorai Geffen for writing the bot's code. Thanks for Neria Khavkin for drawing the bot's avatar.

I give permission to reuse the code and enhance it for your own non-commercial reasons.

*Disclaimer*: I have no rights over Render.com (the infrastructure), Discord nor Cortex Prime rollplaying game.

<a name="for_dev"></a>

## For developers

If you're using the open source to modify this, note the following prefix-commands:

* `!admin` - using the admin user ID, helps to manage to storage.
* `!invite` - creates an invite link.
* `!rollc` - rolls the most basic version of dice rolling.
* `!test` - runs tests, making sure everything is in order.

The `index.js` file shows the required environment variables.

Depending on your server and storage infrastructues, it's possible you'll need to re-implement `api.ts` and/or a storage solution like `replit_storage.ts` (it's possible you'll need additional code changes, such as to `chooser.ts`).

### Old infrastructure

In the past I used "autocode". It stopped working due to the free tier. Also, from a message I got, the bot might shut down on 26-04-2024.

If it doesn't shut down, and you want to install your own bot, it might be as easy as "Insall app" on the [publish page](https://autocode.com/app/youreye/discord-cortex-prime/) - I haven't tried it.

1. Find a way to deploy a bot. Example: use Autocode (you'll have a bot running within 15 minutes).
2. Upload the code there. Note you'll need to attach handlers to each bot command.
3. Each command has a script to register the command in Discord. If you're using Autocode, they have a "command builder" tool that can register the commands. Unfortunately, it doesn't support using the commend text. But you can copy-paste all texts there (manual work).

### Links

* Google Site page: https://sites.google.com/view/youreye-communityprojects/cortex-prime-discord-bot
* Personal Git: https://bitbucket.org/memyselfandyorai/projects/src/master/Discord%20Bot/CortexPrimeRollerBot/
* Published on Autocode (might be shut down): https://autocode.com/app/youreye/discord-cortex-prime/

<a name="support"></a>

## Support

This was created by yorai.geffen@gmail.com.

I cannot guarantee I'll have the resources to fix issues or handle new feature requests. You can always reuse the code (**not** for commercial uses) and try to upload your own bot.

<a name="gallery"></a>

## Image Gallery

### Screenshot: Roll, append and keep

<img src="./readme/gallery/roll%26append%26keep.png" alt="Screenshot: Roll, append and keep">

### Screenshot: Counters, pools, pin

<img src="./readme/gallery/counter%26pool%26pin.png" alt="Screenshot: Counters, pools, pin">

### Screenshot: Save, roll and reroll

<img src="./readme/gallery/save-roll-reroll.png" alt="Screenshot: Save, roll and reroll">