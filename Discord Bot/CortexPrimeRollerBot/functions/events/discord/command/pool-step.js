const { StepPool } = require("../../../../common/poolsController.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  let input = {};
  await StepPool(input, commandInteraction);
}
/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "pool-step",
  "description": "Steps up or down a single die in the pool.",
  "options": [
    {
      "type": 3,
      "name": "name",
      "description": "Name of the pool.",
      "required": true
    },
    {
      "type": 4,
      "name": "die",
      "description": "The die to change.",
      "required": true
    },
    {
      "type": 4,
      "name": "mod",
      "description": "Which was to step.",
      "choices": [
        {
          "name": "UP",
          "value": 1
        },
        {
          "name": "DOWN",
          "value": -1
        }
      ],
      "required": true
    },
    {
      "type": 4,
      "name": "steps",
      "description": "How many steps. Default 1."
    }
  ]
});
*/
