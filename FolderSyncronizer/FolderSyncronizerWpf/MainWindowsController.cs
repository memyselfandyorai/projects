﻿using FolderSyncronizer.Core;
using FolderSyncronizer.Core.Data;
using FolderSyncronizer.Core.State;
using FolderSyncronizer.Core.Watchers;
using FolderSyncronizer.Debugging;
using Infra.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace FolderSyncronizerWpf
{
    class MainWindowsController : IDisposable, INotifyPropertyChanged
    {
        private static readonly HashSet<string> VideoExtensions = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase) {
            // Images:
            //".PNG", ".JPG", ".JPEG", ".BMP", ".GIF", //etc
            // Sound:
            ".WAV", ".MID", ".MIDI", ".WMA", ".MP3", ".OGG", ".RMA", //etc
            // Videos:
            ".MOV", ".AVI", ".MP4", ".DIVX", ".WMV", ".FLV" , ".MPG", ".MPEG", //etc
        };

        private static readonly HashSet<string> ImageExtensions = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase) {
            // Images:
            "PNG", "JPG", "JPEG", "BMP", "GIF", //etc
        };

        // Ends with $ to specify this is an extension. Otherise ".mov" as part of the name would be matched as well.
        private static readonly string _imageRegexPattern = $".*\\.({ImageExtensions.JoinToString("|")})$"; 


        private FileWatcherCollection _fileWatcherCollection;
        private CancellationTokenSource _runningCancellationSource;

        private readonly ModificationsContainer _modificationsContainer;
        private readonly SynchronizerStateHandler _stateHandler;
        private readonly Action<Action> _uiThreadAction;
        private readonly Button _startSyncButton;
        private readonly Button _stopSyncButton;
        private readonly TextBox _sourcePathTextBox;
        private readonly TextBox _targetPathTextBox;
        private readonly Button _addButton;
        private readonly Button _removeButton;
        private readonly Button _loadButton;

        private ChangeStateHandler _changeStateHandler = new ChangeStateHandler(0);
        private bool _disposedValue;


        public MainWindowsController(Action<Action> uiThreadAction, Button startSyncButton, Button stopSyncButton, TextBox sourcePathText, TextBox targetPathText,
            Button addButton, Button removeButton, Button loadButton)
        {
            _uiThreadAction = uiThreadAction;
            _startSyncButton = startSyncButton;
            _stopSyncButton = stopSyncButton;
            _sourcePathTextBox = sourcePathText;
            _targetPathTextBox = targetPathText;
            _addButton = addButton;
            _removeButton = removeButton;
            _loadButton = loadButton;
            ModificationsList = new ObservableCollection<IFileAction>();
            Folders = new ObservableCollection<FolderSource>();

            _modificationsContainer = new ModificationsContainer();
            _stateHandler = new SynchronizerStateHandler();

            _runningCancellationSource = new CancellationTokenSource();

            ModificationsList.CollectionChanged += (x, eventArgs) =>
            {
                if(eventArgs.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    _changeStateHandler.AddHandledItems(eventArgs.NewItems.Count);
                }
                if (_changeStateHandler.IsDone)
                {
                    OnPropertyChanged(nameof(AllSourceFileExtensions));
                    OnPropertyChanged(nameof(ChangeCount));
                }
            };
            ActiveModificationsList.Refresh();
        }

        private void NewFileAction(object sender, FileActionChangedEventArgs e)
        {
            _uiThreadAction.Invoke(() =>
            {
                ModificationsList.Add(e.Action);
                ActiveModificationsList.Refresh();
            });
        }

        public ObservableCollection<IFileAction> ModificationsList { get; private set; }


        internal bool ContainsVideos()
        {
            return FilteredSourceFileExtensions.Any(VideoExtensions.Contains);
        }

        private Regex _sourceFileFilterRegex = new Regex(_imageRegexPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant);
        public string SourceFileFilterRegex
        {
            get { return _sourceFileFilterRegex.ToString(); }
            set
            {
                _sourceFileFilterRegex = new Regex(value, RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant);
                ActiveModificationsList.Refresh();
                OnActiveModificationsListChanged();
            }
        }


        private bool _filterFilesByRegex;
        public bool FilterFilesByRegex
        {
            get { return _filterFilesByRegex; }
            set
            {
                _filterFilesByRegex = value;
                ActiveModificationsList.Refresh();
                OnActiveModificationsListChanged();
            }
        }


        // TODO: This is really not efficient. Should limit to < 1000 files (true for when loading files as well). Or alternatively, not update the UI for every change. Also: filter using a more efficient algorithm?
        private void OnActiveModificationsListChanged()
        {
            OnPropertyChanged(nameof(SourceFileFilterRegex));
            OnPropertyChanged(nameof(FilterFilesByRegex));
            OnPropertyChanged(nameof(AllSourceFileExtensions));
            OnPropertyChanged(nameof(ChangeCount));
        }

        private ICollectionView ActiveModificationsList
        {
            get
            {
                if (Singleton.IsDebugMode)
                {
                    Singleton.Logger.LogDebug($"Converting modifications: {Environment.NewLine + string.Join(Environment.NewLine, ModificationsList.Select(t => t.Description))}");
                }

                var source = CollectionViewSource.GetDefaultView(ModificationsList);
                source.Filter = action => ShouldShowAction((IFileAction)action);
                return source;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string AllSourceFileExtensions =>
            SourceFileExtensions.OrderBy(x => x).JoinToString(" , ") + " (" + GetLowerExtensions(FilteredModifications).OrderBy(x => x).JoinToString(" , ") + ")";

        private IEnumerable<IFileAction> FilteredModifications => ModificationsList.Where(f => !IsFilteredByRegex(f));
        

        private HashSet<string> SourceFileExtensions => GetLowerExtensions(ModificationsList);
        private HashSet<string> FilteredSourceFileExtensions => GetLowerExtensions(FilteredModifications);

        public int ChangeCount => ModificationsList.Where(ShouldShowAction).Count();

        public ObservableCollection<FolderSource> Folders { get; private set; }
        public string ImageRegex => _imageRegexPattern;

        private HashSet<string> GetLowerExtensions(IEnumerable<IFileAction> actionList)
        {
            return new HashSet<string>(actionList.SelectMany(action => new[]
            {
                Path.GetExtension(action.SourceFile)?.ToLower(),
                // To support extension renames as well:
                Path.GetExtension(action.TargetFile)?.ToLower(),
            }));
        }

        private bool ShouldShowAction(IFileAction action)
        {
            if (action.IsCancelled)
            {
                return false;
            }

            if (IsFilteredByRegex(action))
            {
                if (Singleton.IsDebugMode)
                {
                    Singleton.Logger.LogDebug($"Skipped file (by regex) '{action.SourceFile}': {action.Description}");
                }
                return false;
            }

            return true;
        }

        private bool IsFilteredByRegex(IFileAction action)
        {
            if (FilterFilesByRegex && !string.IsNullOrWhiteSpace(SourceFileFilterRegex))
            {
                if (!_sourceFileFilterRegex.IsMatch(action.SourceFile))
                {
                    return true;
                }
            }

            return false;
        }

        public void Start()
        {
            // TODO: popup if not empty to clear? ModificationsList.Clear();
            _fileWatcherCollection = new FileWatcherCollection(_modificationsContainer, Folders);
            _fileWatcherCollection.NewFileAction += NewFileAction;
            ChangeControlStage(canStartSync: false);
        }

        internal void AddSourceFolders(string sourcePath, string targetPath)
        {
            if (!Directory.Exists(sourcePath))
            {
                throw new DirectoryNotFoundException($"Folder '{sourcePath}' does not exist");
            }
            if (!Directory.Exists(targetPath))
            {
                throw new DirectoryNotFoundException($"Folder '{targetPath}' does not exist");
            }

            Folders.Add(new FolderSource { Source = sourcePath, Target = targetPath });
        }

        internal void RemoveSourceFolders(int selectedIndex)
        {
            Folders.RemoveAt(selectedIndex);
        }

        private void ChangeControlStage(bool canStartSync)
        {
            _sourcePathTextBox.IsEnabled = canStartSync;
            _targetPathTextBox.IsEnabled = canStartSync;
            _startSyncButton.IsEnabled = canStartSync;
            _stopSyncButton.IsEnabled = !canStartSync;
            _addButton.IsEnabled = canStartSync;
            _removeButton.IsEnabled = canStartSync;
            _loadButton.IsEnabled = canStartSync;
        }

        public void Stop()
        {
            _fileWatcherCollection?.Dispose();
            _fileWatcherCollection = null;
            ChangeControlStage(canStartSync: true);
        }

        internal void Save(string fileName)
        {
            SynchronizerSnapshot snapshot = new SynchronizerSnapshot()
            {
                FolderSources = Folders.ToList(),
                Actions = ModificationsList.ToList(),
            };
            _stateHandler.Save(fileName, snapshot);
            
            // For debugging:
            // var targetFiles = ActiveModificationsList.Cast<IFileAction>().Select(a =>  Path.GetFileName(a.TargetFile));
            // var text = targetFiles.OrderBy(x => x).JoinToString(Environment.NewLine);
            // File.WriteAllText(fileName + ".txt", text);
        }


        internal void Load(string fileName)
        {
            var snapshot = _stateHandler.Load(fileName);
            Folders.Clear();
            if (snapshot.FolderSources != null)
            {
                foreach (var folder in snapshot.FolderSources)
                {
                    Folders.Add(folder);
                }
            }

            ModificationsList.Clear();
            _modificationsContainer.Clear();
            if (snapshot.Actions != null)
            {
                int maxActions = snapshot.Actions.Count(a => !a.IsCancelled);
                _changeStateHandler = new ChangeStateHandler(maxActions);
                foreach (var action in snapshot.Actions)
                {
                    if(!action.IsCancelled)
                    {
                        ModificationsList.Add(action);
                        _modificationsContainer.Add(action);
                    }
                }
                _changeStateHandler = new ChangeStateHandler(0);
            }
        }

        internal Task PerformActions()
        {
            return Task.Factory.StartNew(() =>
            {
                RunActionOptions actionOptions = new RunActionOptions { ShouldSkipAction = IsFilteredByRegex };
                while (_modificationsContainer.RunNextAction(actionOptions))
                {
                    _uiThreadAction.Invoke(() =>
                    {
                        ModificationsList.RemoveAt(0);
                    });
                    if (_runningCancellationSource.IsCancellationRequested)
                    {
                        break;
                    }
                }
            }, _runningCancellationSource.Token);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    Stop();
                    _runningCancellationSource?.Cancel();
                    _runningCancellationSource?.Dispose();
                    _runningCancellationSource = null;
                }
                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
