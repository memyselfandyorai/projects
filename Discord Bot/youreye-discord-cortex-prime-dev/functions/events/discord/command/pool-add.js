const {POOL_ADD}= require('../../../../common/pools.js');
const {ModifyPools}= require('../../../../common/poolsController.js');

let input = {
  action: POOL_ADD,
}
await ModifyPools(input, context.params.event)

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "pool-add",
  "description": "Add dice to a pool.",
  "options": [
    {
      "type": 3,
      "name": "name",
      "description": "Name of the pool.",
      "required": true
    },
    {
      "type": 3,
      "name": "dice",
      "description": "Dice to add.",
      "required": true
    }
  ]
});
*/