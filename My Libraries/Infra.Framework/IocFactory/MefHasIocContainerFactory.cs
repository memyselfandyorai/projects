﻿using Infra.Framework.Extensions;
using Infra.Framework.Ioc;
using Infra.Framework.Mef;
using System;
using System.Linq;
using System.Reflection;

namespace Infra.Framework.IocFactory
{
    public class MefHasIocContainerFactory : MefFactory<IHasIocContainer, Type, IHasIocContainer>, IHasIocContainerFactory
    {
        private const BindingFlags AllBindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;

        private readonly IIocContainer _iocContainer;

        public MefHasIocContainerFactory(IIocContainer iocContainer)
        {
            _iocContainer = iocContainer;
        }

        public T CreateInstance<T>(params object[] args) where T : IHasIocContainer
        {
            return CreateInstanceNotSafe<T>(args);
        }

        public T CreateInstanceNotSafe<T>(params object[] args)
        {
            T instance = (T)Create(typeof(T));
            var argsWithIoc = _iocContainer.ToSingleton().Union(args).ToArray();
            return (T)CreateNewImport((IHasIocContainer)instance, argsWithIoc);
        }

        protected override IHasIocContainer CreateNewImport(IHasIocContainer import, params object[] args)
        {
            // Supports also non-public ctors.
            return (IHasIocContainer)Activator.CreateInstance(import.GetType(), AllBindingFlags, null, args, null);
        }

        protected override IHasIocContainer ConvertImport(IHasIocContainer import)
        {
            return import;
        }

        protected override Type GetKey(IHasIocContainer import)
        {
            return import.GetType();
        }
    }
}
