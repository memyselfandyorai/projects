import type { DataStorage } from "./storage";
export declare class IocContainer {
    storage: DataStorage;
    consturctor(): void;
    setStorage(storage: DataStorage): void;
    getIocContainer(): any;
}
//# sourceMappingURL=ioc_container.d.ts.map