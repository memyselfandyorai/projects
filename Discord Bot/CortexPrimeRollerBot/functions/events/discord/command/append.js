const controller = require("../../../../common/controller.js");
const {
  ReplyToCommand,
  ReceivedCommand,
} = require("../../../../common/commands.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let inputOptions = parseInput(commandInteraction);
  console.log("input dice: " + inputOptions.dice);

  // Note: the append shouldn't be used on a roll with die-modifier (the step-up/step-down and its results might not be valid anymore),
  //  unless you know what you're doing, and this append is "safe" - i.e., shouldn't change the affect dice.
  let messageContent = await controller.RunSafe(() =>
    controller.RollCortexPrimeAppend(
      inputOptions,
      commandInteraction.getIocContainer(),
    ),
  );
  await ReplyToCommand(commandInteraction, messageContent);
}

// Returns input: {dice, cache_key}
function parseInput(commandInteraction) {
  var inputMap = commandInteraction.getInputOptionsMap();

  return {
    dice: inputMap.get("dice"),
    cache_key: controller.FormatChannelKey(commandInteraction),
  };
}

/*

Create command:

const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "append",
  "description": "Appends dice to the previous roll",
  "options": [
    {
      "type": 3,
      "name": "dice",
      "description": "Space-separated list of dice. Support 2D10 format.",
      "required": true
    }
  ]
});
*/
