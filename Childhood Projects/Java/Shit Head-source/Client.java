import java.io.*;
import java.net.*;
import java.awt.*;
import javax.swing.*;

import java.util.*;
import java.awt.event.*;

//((InetSocketAddress)socket.getRemoteSocketAddress()).getHostName()

//InetAddress.getLocalHost().getHostName();

class Reading implements Runnable,ActionListener,KeyListener{
  Socket socket;
  Game game;
  String name;
  BufferedWriter out;	
  static JFrame frame;
  JFrame gframe;
  JPanel players;
  JPanel main;
  JTextField text; 
  JButton send;
  JButton priv;
  JTextArea conv;
  JList list;

  
 public Reading(Socket socket,Game g,String n,JFrame f){
   this.socket=socket;
   game=g;
   name=n;
   frame=f;
   players=new JPanel();
   
   priv=new JButton("Send private message");
   priv.addActionListener(this);
   send=new JButton("Send message");
   send.addActionListener(this);
   
   frame.setSize(500,500);
   frame.setVisible(true);
   main=new JPanel();
   
   main.setLayout(new BorderLayout());
   main.add(BorderLayout.WEST,players);
   
   //text=new JTextField("Enter message here");
   text=new JTextField();
   text.setToolTipText("Enter message here");
   text.addKeyListener(this);
   
   players.setLayout(new GridLayout(1,1)); 
   //players layout, text-actionlsenter? (enter)
   
   	JPanel p=new JPanel();
 	p.add(send);
 	p.add(priv);
   
    JPanel txt=new JPanel();
    txt.setLayout(new GridLayout(2,1)); 
    
    txt.add(text);
    txt.add(p);
    
    main.add(BorderLayout.SOUTH,txt);
       
   frame.getContentPane().add(main);
   //frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
   //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   
   frame.addWindowListener(new WindowListener(){
				
				
				public void windowClosing(WindowEvent e)
				{
				if(game==null)
				{
					sendMessage("exit"); //better than quit?
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				}
				else
					if(game.canExit())
					{
					sendMessage("exit");
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					}
					else
			   		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				}
				
				public void windowClosed(WindowEvent e)
				{
					
				}
				
				public void windowIconified(WindowEvent e)
				{
					
				}
				
				public void windowDeiconified(WindowEvent e)
				{
					
				}
				
				public void windowDeactivated(WindowEvent e)
				{
					
				}
				
				public void windowActivated(WindowEvent e)
				{
					
				}
				
				public void windowOpened(WindowEvent e)
				{
					
				}
					
				});	
   
   
   
   
   players.setLayout(new GridLayout(1,1));
    
   conv=new JTextArea("Chat:",20,20);
   
   JScrollPane scr=new JScrollPane(conv);
   
   //scr.setPreferredSize(new Dimension(500,300));
   
   scr.setWheelScrollingEnabled(true); //check
         
   conv.setToolTipText("Chat");
   conv.setEnabled(false);
   
   main.add(BorderLayout.CENTER,scr);
   
 }
 
 
 	public void keyPressed(KeyEvent e)
	{
		
	}
	public void keyReleased(KeyEvent e)
	{
		
	}
	
	public void keyTyped(KeyEvent e)
	{
		//if(e.getKeyCode()==KeyEvent.VK_ENTER) would work in released
		if(e.getKeyChar()=='\n')
		{
			if(text.hasFocus()&&!text.equals(""))
			{
				sendMessage("CHAT*"+name+": "+text.getText());
				text.setText(""); 
			}
		}
		
	}
 
 
 public void run(){
   try{
   
    out=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
   	
    BufferedReader in= new BufferedReader(new InputStreamReader(socket.getInputStream()));
    String line;

  
  sendMessage("NAME*"+name);
  
   while ((line=in.readLine())!=null)
   {
	   if(line.equals("exit")||line.equals("Disconnected."))
	   {
		   System.out.println("exiting...");
		   sendMessage("quit");
		   break;
	   }
	   
   	   	int b;
   	   	
   	  //System.out.println(line);	
   	   	
   	   if(line.indexOf("NAMES*")==0)
   	   {
   	   	String s=line.substring(6);
   	   	
   	   	//System.out.println(s);
   	   	
   	   	Vector<String> n=new Vector<String>();
   	   	
   	   	while((b=s.indexOf(","))!=-1)
   	   	{
   	   	String c=s.substring(0,b);
   	   	n.add(c);
  	   	s=s.substring(b+1);
   	   	}
   	   	
   	   	if(n.size()>4&&name.equals((String)n.elementAt(4)))
   	   	{
   	   	sendMessage("quit");
   	   	JOptionPane.showMessageDialog(frame,"Too many players.","Kicked",JOptionPane.PLAIN_MESSAGE);
   	   	}
   	   	else
   	   	update(n);
   	   	   	   	
   	   }
   	   	
   	   	
   	   if(line.equals("*KICK*"))
   	   {
   	   	sendMessage("quit");
   	   	JOptionPane.showMessageDialog(frame,"Kicked by host.","Kicked",JOptionPane.PLAIN_MESSAGE);
   	   }
   	   
   	   if(line.equals("*KICKEDN*"))
   	   {
   	   	sendMessage("quit");
   	   	JOptionPane.showMessageDialog(frame,"Nickname taken.","Kicked",JOptionPane.PLAIN_MESSAGE);
   	   }
   	   	
   	
   	   if(line.equals("*ST*"))
   	   {
   	   	JOptionPane.showMessageDialog(frame,"Game starts.","",JOptionPane.PLAIN_MESSAGE);
   	   	if(gframe!=null)
   	   	gframe.setVisible(true);
   	   }
   		
   	   if(line.indexOf("CHAT*")==0)
   	   {
   	   	String s=line.substring(5);
   	   	conv.append("\n"+s);
        conv.setCaretPosition(conv.getDocument().getLength());
       }
   		
   		
   	   if(line.indexOf("GAME*")==0)
   	   {
   	   	String s=line.substring(5);
   	   	if(game==null)
   	   	{
   	   	gframe=new JFrame("Shit Head - "+name);
   	   	game=new Game(gframe,this);
   	   	game.read(s);
   	   	game.game();
   	   	}
   	   	else
   	   	game.read(s);
   	   	
   	   	game.act(name);   	   	
       }
   
   		
   		if(line.indexOf("MOVE*")==0)
   		{
   		String s=line.substring(5);
   	   	Vector<String> nums=new Vector<String>();
	   	   	
   	   	while((b=s.indexOf(","))!=-1)
   	   	{
   	   	nums.add(s.substring(0,b));
   	   	s=s.substring(b+1);
   	   	}
	   	   	
   	   	String name=s;
	   	   
	   	//System.out.println(line+"\n"+nums+"\n");
	   	
	   	game.showMiddle(game.player(name),nums);
	   	
   	   	//game.move(name,nums,this);
  		}
  		
  		if(line.indexOf("MOVET*")==0)
   		{
   		String s=line.substring(6);
   	   	Vector<String> nums=new Vector<String>();
	   	   	
   	   	while((b=s.indexOf(","))!=-1)
   	   	{
   	   	nums.add(s.substring(0,b));
   	   	s=s.substring(b+1);
   	   	}
	   	   	
		b=s.indexOf(";");
	   	String name=s.substring(0,b);
	   	s=s.substring(b+1);
	   	   	
	   	boolean sen=s.equals("true");
	   	   
	   	//System.out.println(line+"\n"+nums+"\n");
	   	
	   	game.showMiddle(game.player(name),nums,sen);
	   	
   	   	//game.move(name,nums,this);
  		}
  		
  		
  		
  		if(line.indexOf("TAKE*")==0)
	   	{
	    String s=line.substring(5);
	   	   	
	   	String name=s;
	   	   	
	   	game.stake(name);
	   	   	
	    } 
  		
  		if(line.indexOf("TAKEN*")==0)
	   	{
	    String s=line.substring(6);
	   	   	
		b=s.indexOf(",");
	   	int ind=Integer.parseInt(s.substring(0,b));
	   	s=s.substring(b+1);
	   	   	
	   	String name=s;
	   	   	
	   	game.staken(name);
	   	   	
	    } 
  		
  		
  		if(line.indexOf("BURN*")==0)
	   	{
	    String s=line.substring(5);
	   	   	
	   	String mes=s;
	   	
	   	//System.out.println("burn: "+mes);
	   	game.burn(mes,name);
	   	   	
	    } 
  		
  		   		
   		if(line.indexOf("MISSED*")==0)
	   	{
	    String s=line.substring(7);
	   	   	
	   	String n=s;
	   	   	
	   	game.missed(n,name.equals(n));
	   	   	
	    } 
   		
   		if(line.indexOf("FINISHED*")==0)
	   	{
	    String s=line.substring(9);
	   	   	
	   	String n=s;
	   	   	
	   	game.finished(n,name.equals(n));
	   	   	
	    } 
   		
   		if(line.indexOf("FINISH*")==0)
	   	{
	    String s=line.substring(7);
	   	   	
	   	String lost=s;
	   	   	
	   	game.finish(lost);
	   	   	
	    } 
   		
   		
   		
   } //end while (loop)
   
   
  }catch(IOException e){
	  e.printStackTrace();
  }
  System.out.println("Disconnected from server");
  System.exit(1);
 }
 
 
 
 
 
 
	public void sendMessage(final String messer){
	      Thread send =new Thread(new Runnable(){
	        public void run(){
				try{
			   out.write(messer+"\n");
	           out.flush();
	           }catch(IOException e){}
	        }
	      });
	      send.start();
	}
 
 
 
 
 
 
 
 public void update(Vector n)
 {
 players.setVisible(false);	
 players.removeAll(); 
 
 list=new JList(n);
 
 list.setPreferredSize(new Dimension(200,200));
 
 list.setToolTipText("Players");
 
 list.setBackground(Color.gray);
 
 players.add(list);  
   
 players.setVisible(true);	
 	
 }
 
 
 public void sendGame()
 {
 sendMessage("GAME*"+game.write());
 }
 
 
 public void sendMove(Vector nums)
 {
 	 String s="MOVE*";	
 	 for(int i=0;i<nums.size();i++)
 	 {
 	 s+=nums.elementAt(i)+",";
 	 }
 	
 	s+=name;
 	sendMessage(s);
 }
 
 public void sendMoveT(Vector nums,boolean sen)
 {
 	 String s="MOVET*";	
 	 for(int i=0;i<nums.size();i++)
 	 {
 	 s+=nums.elementAt(i)+",";
 	 }
 	
 	s+=name+";";
 	s+=sen;
 	sendMessage(s);
 }
 
 
 public void sendTake()
 {
 	 String s="TAKE*";	
 	 s+=name;
 	sendMessage(s);
 }
 
 
 public void sendTaken(int index)
 {
 	 String s="TAKEN*";	
 	 s+=index+",";
 	 s+=name;
 	sendMessage(s);
 }
 
 
 public void actionPerformed(ActionEvent e)
 {
 	
 	JButton b=(JButton)e.getSource();

	if(b==send)
	{
		sendMessage("CHAT*"+name+": "+text.getText());
		text.setText(""); 	
 	}
 	
 	if(b==priv)
	{
		int sel=list.getSelectedIndex();
			if(sel!=-1)
			{
			sendMessage("PRIV*"+sel+",(private) "+name+": "+text.getText());
			text.setText(""); 	
			}
 	}

 	
 }
 
  
}


//when disconnects- remove from map.


class Client implements Runnable{


Socket socket;
BufferedReader in;
BufferedWriter out;

static Game game;

boolean connected;

JFrame frame;


public Client(){
	connected=false;
	
	
	frame=new JFrame();
	
	//final JFrame f=new JFrame();
	//f.getContentPane().add(game);
	//f.setSize(Main.dimx,Main.dimy);
	//f.setVisible(true);
	
	//f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	
	/*	
	f.addWindowListener(new WindowListener(){
	
	
	public void windowClosing(WindowEvent e)
	{
	if(connected&&!main.getPlayer().attacked())
	sendMessage("exit"); //better than quit?
	
	if(!connected)	
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void windowClosed(WindowEvent e)
	{
		
	}
	
	public void windowIconified(WindowEvent e)
	{
		
	}
	
	public void windowDeiconified(WindowEvent e)
	{
		
	}
	
	public void windowDeactivated(WindowEvent e)
	{
		
	}
	
	public void windowActivated(WindowEvent e)
	{
		
	}
	
	public void windowOpened(WindowEvent e)
	{
		
	}
		
	});	*/
	
}


public boolean isConnected()
{
return connected;	
}

	public static boolean isNatural(String s)
	{
	if(s==null||s.equals(""))
	return false;
		
	char[] c=s.toCharArray();	
		
		for(int i=0;i<c.length;i++)
		if(c[i]<'0'||c[i]>'9')
		return false;
		
	return true;	
	}



public void start(){
      Thread logRead=new Thread(this,"client");
      logRead.start();
}

public void run(){
  
 String s=JOptionPane.showInputDialog(frame,"Enter host name or IP: (press enter for current computer)");
 
 if(s==null||s.equals(""))
 {
   try{
   s=InetAddress.getLocalHost().getHostName();
   }catch(UnknownHostException e){}
 
 JOptionPane.showMessageDialog(frame,"Default: Local host");

 }
 
 String p=JOptionPane.showInputDialog(frame,"Enter port:","4000");
 
 if(!isNatural(p))
 {
 p="4000";
 JOptionPane.showMessageDialog(frame,"Default Port: 4000");
 }
  
 try{
 socket=new Socket(s,Integer.parseInt(p));
 //socket=new Socket(s,Integer.parseInt(p),InetAddress.getLocalHost(),4000);
 
 out=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
 in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
 connected=true;
  
 }catch(IOException e){
 	 System.err.println("Couldn't get I/O for the connection to server.");
 	JOptionPane.showMessageDialog(Reading.frame,"Couldn't get I/O for the connection to server.");
     System.exit(1);
 	}
   
  
  String name=JOptionPane.showInputDialog(frame,"Enter your nickname:","Nickname",JOptionPane.PLAIN_MESSAGE);
  
  if(name==null)
  name="";
  
  if(!name.equals("")&&name.indexOf('/')==-1&&name.indexOf("\\")==-1&&name.indexOf(';')==-1&&name.indexOf(',')==-1&&name.indexOf('*')==-1&&name.indexOf('.')==-1&&name.indexOf('<')==-1&&name.indexOf('>')==-1&&name.indexOf('|')==-1&&name.indexOf('?')==-1&&name.indexOf(':')==-1&&name.indexOf('"')==-1&&name.indexOf(' ')==-1)
  name=name;
  else
  name="Player";
  
  frame.setTitle(name);
  
  	Reading readThread=new Reading(socket,game,name,frame);
  	Thread read=new Thread(readThread,"read");
  	read.start();
	  
}

 

public static void main(String [] arg){
 Client t=new Client();
     t.start();
  }
}