// authenticates you with the API standard library

const roller = require("../../../../../../common/roller.js");
const {
  ReplyToPrefixCommand,
} = require("../../../../../../common/commands.js");
const controller = require("../../../../../../common/controller.js");

module.exports = {
  async execute(prefixCommandInteraction, msgContent) {
    await handleCommand(prefixCommandInteraction, msgContent);
  },
};

async function handleCommand(prefixCommandInteraction, msgContent) {
  let diceRoll = msgContent
    .split(" ")
    .slice(1)
    .map((x) => parseInt(x))
    .filter((x) => !isNaN(x));
  let input = { diceRoll: diceRoll };
  let messageContent = roller.RollCortexPrime(
    input,
    prefixCommandInteraction.getIocContainer(),
  ).messageContent;

  // console.log(`Permissions: ` + context.params.event.member.permissions);
  // console.log(`Permissions: ` + JSON.stringify(context.params.event));

  // Bad - "permissions": "1071698665025",
  // Good -"permissions": "1071698660929",

  let options = {
    replyToMessage: true,
  };
  await ReplyToPrefixCommand(prefixCommandInteraction, messageContent, options);
}
