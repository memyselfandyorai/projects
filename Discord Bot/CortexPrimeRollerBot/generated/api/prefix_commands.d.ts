import type { MessageSender } from "./messages";
export interface PrefixCommandInteraction extends MessageSender {
    getIocContainer(): any;
    getAdminId(): string;
    botId(): string;
    senderId(): string;
    getMessageId(): string;
    generateInvite(): string;
}
//# sourceMappingURL=prefix_commands.d.ts.map