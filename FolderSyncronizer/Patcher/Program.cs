﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FolderSyncronizer.Debugging;
using Infra.Framework.Extensions;

namespace Patcher
{
    public class Program
    {

        private const bool MonitorOnly = true;

        private const Mode WorkMode = Mode.ShowDiff;

        private enum Mode
        {
            /// <summary>
            /// Only monitors files to compare. Writes everything.
            /// </summary>
            MonitorOnly,
            /// <summary>
            /// Copies missing files
            /// </summary>
            CopyMissing,
            /// <summary>
            /// Shows the (relative) diff between source and target. Note this is single directional.
            /// </summary>
            ShowDiff,
        }

        private static readonly TimeSpan? LimitTimeBack = null; // TimeSpan.FromDays(365);

        private static readonly HashSet<string> validExtensions = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase) { ".png", ".jpg", "jpeg", ".bmp", ".gif", ".mov", ".mp4" };
        private static readonly HashSet<string> subPathsToIgnore = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase) { "Itai-NewZealand", "Records-Not4iPhone" };

        // try to create a script that outputs all jpgs from all subdirectories, ordered by name(without folder) + including count
        // compare iphone cats to memories cats
        // think on how to fix?
        // while fixing, also fix "if modified < create, switch them" (code in other project)

        static void Main(string[] args)
        {
            //string source = @"D:\Documents\Memories\דירה פתח תקווה 2020\חתולים\2021-06";
            //string target = @"D:\iPhone Pictures\דירה פתח תקווה 2020\חתולים\2021-06";

            //string source = @"D:\Documents\Memories\דירה פתח תקווה 2020\חתולים";
            //string target = @"D:\iPhone Pictures\דירה פתח תקווה 2020\חתולים";
            
            string target= @"C:\Users\Yorai\Pictures\Pictures\First import";
            string source = @"C:\Users\Yorai\Pictures\Pictures\Last import";

            //string source = @"D:\Documents\Memories\";
            //string target = @"D:\iPhone Pictures\";

            Singleton.Logger.LogDebug("Getting");
            Console.WriteLine("Getting");
            var sourceResult = GetAllFileNames(source);
            var targetResult = GetAllFileNames(target);

            switch (WorkMode)
            {
                case Mode.MonitorOnly:
                    MonitorFilesAndFolders(sourceResult, targetResult);
                    break;
                case Mode.CopyMissing:
                    CopyMissingFiles(target, sourceResult, targetResult);
                    break;
                case Mode.ShowDiff:
                    ShowDiff(sourceResult, targetResult);
                    break;
            }
            Console.WriteLine("Done");
        }

        private static void ShowDiff(FolderSyncResult sourceResult, FolderSyncResult targetResult)
        {
            var sourceFiles = new HashSet<string>(sourceResult.FileNamesToPathList.Keys);
            var targetFiles = new HashSet<string>(targetResult.FileNamesToPathList.Keys);
            sourceFiles.ExceptWith(targetFiles);

            string path = @"C:\Users\Yorai\Pictures\Pictures\fileDiff.txt";
            Console.WriteLine("Writing to " + path);
            WriteToFile(sourceFiles, path);
            
            sourceFiles = new HashSet<string>(sourceResult.FileNamesToPathList.Keys);
            targetFiles = new HashSet<string>(targetResult.FileNamesToPathList.Keys);
            targetFiles.ExceptWith(sourceFiles);

            path = @"C:\Users\Yorai\Pictures\Pictures\fileDiff-rev.txt";
            Console.WriteLine("Writing to " + path);
            WriteToFile(targetFiles, path);
        }

        private static void CopyMissingFiles(string target, FolderSyncResult sourceResult, FolderSyncResult targetResult)
        {
            Singleton.Logger.LogDebug("Copying");
            SyncPatcher.CopyMissedFiles(sourceResult, targetResult);
            Singleton.Logger.LogDebug("Patching");
            Console.WriteLine("Patching");
            SyncPatcher.PatchFileCreation(target);
            Singleton.Logger.LogDebug("Done");
        }

        private static void MonitorFilesAndFolders(FolderSyncResult sourceResult, FolderSyncResult targetResult)
        {
            Console.WriteLine("Writing");
            WriteToFile(sourceResult, @"C:\Users\Yorai\Pictures\Pictures\sourceFiles.txt");
            WriteToFile(targetResult, @"C:\Users\Yorai\Pictures\Pictures\targetFiles.txt");

            WriteToFile(GetAllFolders(sourceResult), @"C:\Users\Yorai\Pictures\Pictures\sourceFolders.txt");
            WriteToFile(GetAllFolders(targetResult), @"C:\Users\Yorai\Pictures\Pictures\targetFolders.txt");
        }

        private static void WriteToFile(HashSet<string> hashSets, string outputPath)
        {
            StringBuilder result = new StringBuilder();
            foreach (var item in hashSets.OrderBy(x => x))
            {
                result.AppendLine($"{item}");
            }

            File.WriteAllText(outputPath, result.ToString());
        }

        private static void WriteToFile(FolderSyncResult folderResult, string outputPath)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine(folderResult.FileExtensions.OrderBy(x => x).JoinToString(","));
            foreach (var item in folderResult.FileNamesToPathList.OrderBy(x => x.Key))
            {
                result.AppendLine($"{item.Key} ; {item.Value.Count}");
            }


            File.WriteAllText(outputPath, result.ToString());
        }

        private static FolderSyncResult GetAllFileNames(string folder)
        {
            var result = new FolderSyncResult
            {
                RootPath = folder,
                FileExtensions = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase),
                FileNamesToPathList = new Dictionary<string, List<string>>(StringComparer.InvariantCultureIgnoreCase),
            };

            foreach (var file in Directory.EnumerateFiles(folder, "*", SearchOption.AllDirectories).Where(IsImage))
            {
                result.FileExtensions.Add(Path.GetExtension(file));
                result.FileNamesToPathList.AddToList(Path.GetFileName(file), file);
            }

            return result;
        }
        
        public static bool IsImage(string file)
        {
            bool containsPathToIgnore = subPathsToIgnore.Any(file.Contains);
            bool withinTimeLimits = LimitTimeBack == null || (DateTime.Now - LimitTimeBack) < File.GetCreationTime(file);
            bool validExtension = validExtensions.Contains(Path.GetExtension(file));

            return !containsPathToIgnore && withinTimeLimits && validExtension;
        }


        private static HashSet<string> GetAllFolders(FolderSyncResult result)
        {
            return new HashSet<string>(result.FileNamesToPathList.Values.SelectMany(l => l.Select(f => Path.GetFileName(Path.GetDirectoryName(f)))));
        }
    }

    public class FolderSyncResult
    {
        public Dictionary<string, List<string>> FileNamesToPathList { get; set; }
        public HashSet<string> FileExtensions { get; set; }
        public string RootPath { get; internal set; }
    }

}
