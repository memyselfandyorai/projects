﻿using System;

namespace Infra.Framework.Exceptions
{
    public class InvalidActionException : SystemException
    {

        public InvalidActionException(string msgFormat, params object[] args) :
            base(args.Length == 0 ? msgFormat : string.Format(msgFormat, args))
        {

        }
    }
}