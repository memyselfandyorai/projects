
// https://autocode.com/guides/how-to-build-a-discord-bot/#testing-commands

const messages = require('./messages.js');

const CONSOLE_LOG = false

/*

// 
Inputs: 
  IocContainer = {
    getNextNumber: func(min, max) -> int, 
    options { 
      cacheTimeSecs - time for caching last roll,
      saveDiceCacheTimeSecs - time for caching saved dice, 
      extraEffectDieModeOn - when true, shows increased effects as additional dice ; when false, shows as D12+,
      extraRollDieIncreaseModeOn - when true, enables more die values when stepping up ; when false, non pool die is limited to 1,
    }
    cache { 
      async cacheValue: func(key, value, optional ttlSeconds) -> void,
      async getCacheValue: func(key, optional defaultValue) -> value,     
      async getCacheEntries: func() -> list of {key, value},     
      async clearCacheKey: func(key) -> void,   
     }
    storage {
      async storeValue: func(keyList, value) -> void,
      async getStoredValues: func(keyList, optional options) -> list of {keyListSuffix, value},
      async getStoredValue: func(keyList, defaultValue) -> value,
      async getAllStoredEntries: (optional options) -> list of {keyList, value},     
      async clearKeys: func(keyList) -> list of keyListSuffix,
    },
  }
  
  RollResult (getResultsBasedOnRolls result) = {
    messageContent, 
    diceRoll, 
    rolls, 
    nonPoolDice, 
    nonPoolRolls, 
    scores, 
    maxScores, 
    options 
  } - note it is not cached out-of-the-box, but rather used to convert in cacheResult(...)
  
*/


class InvalidDiceFormatError extends Error {
  // Error should include the problem and the specific issue.
  constructor(...params) {
      super(...params)
      if (Error.captureStackTrace) {
          Error.captureStackTrace(this, this.constructor);
      }
      this.name = this.constructor.name
  }
}


module.exports = {
  InvalidDiceFormatError: InvalidDiceFormatError,
  ParseDiceInput: parseDiceInput,
  RollCortexPrime: rollCortexPrime,
  RollCortexPrimeKeep: rollCortexPrimeKeep,
  RollCortexPrimeAppend: rollCortexPrimeAppend,
  StepUp: stepUp,
  MergeDice: mergeDice,
};

/*
Guidelines:
- Dices don't go away (e.g., decreasing D4 stays D4)
- Effect has minimum of D4
- Stepping up D12 from dice pool, gives D4 that cannot be hitched and cannot be used for effect. There is no double step-up from D12.
- Effect with increased D12 should be shown as "D12+" regardless of value. But I should still log it if it'll be relevant. Put a const.
*/

/*
Future:
- Performance: Consider. Try not awaiting on cache. Maybe quicker. Maybe eeror. (Extra work?) - if there's an error it'll be uncaught, should at least catch and log if so
*/

const MIN_EFFECT = 4
const MAX_EFFECT = 12
const MIN_DIE = 4
const MAX_DIE = 12
const EXTRA_NON_POOL_DIE = 4

// Input: text in format "10 5 4D10"
// Output: list of numbers (diceRoll)
function parseDiceInput (dice) {
  let diceRoll = []
  if(dice.length == 0) {
    return diceRoll
  }
  
  let pool =  dice.split(' ').map(s => s.trim()).filter(x => !!x)
  pool.forEach(dieRoll => {
    // Case of just a number:
    if(!isNaN(dieRoll)) {
      let value = parseInt(dieRoll)
      if(!isNaN(value)) {
        diceRoll.push(value)
      } else {
        console.log(`Unexpected dieRoll, NaN: ${value}`);
        throw new InvalidDiceFormatError(`Not a number: '${dieRoll}'`)
      }
    // Case of xDy:
    } else {
      let rollFormat = dieRoll.toUpperCase().split('D')
      let validNumbers = rollFormat.filter(v => !isNaN(v)).map(x => parseInt(x))
      if (rollFormat.length == 2 && validNumbers.length == 2) {
        let dieCount = validNumbers[0]
        let validDie = validNumbers[1]
        
        if (isNaN(validDie) || isNaN(dieCount )) {
          console.log(`Unexpected xDy format, NaN: ${dieRoll}`);
          throw new InvalidDiceFormatError(`Wrong format: '${dieRoll}'`)
        }
        
        for (let i = 0; i < dieCount; i++) {
          diceRoll.push(validDie)
        }
      } else {
        console.log(`Unexpected xDy format, too much Ds? ${dieRoll}`);
        throw new InvalidDiceFormatError(`Wrong format: '${dieRoll}'`)
      }      
    }
  })
  
  return diceRoll 
}

/*
input: {diceRoll - list of numbers, 
        nonPoolDice - list of numbers that cannot be hitched or part of the effect,
        keep (optional) - positive number of dice to keep for score, 
        effectKeep (optional) - optional number (min: 1) to keep for effect, (if increased effect: choice by user)
        effectModifier (optional) - modifies the effect die/dice,
        dieModifier (optional) - by level: when positive, increases minimal dice, when negative, decreases maximal dice,
        hitch (optional) - max number that counts as hitch
        }, 
    iocContainer - IocContainer
output: RollResult
*/
function rollCortexPrime(input, iocContainer){
  console.log('input: ' + JSON.stringify(input))

  let diceRoll = input.diceRoll
  var options = {
    keep: Math.max(input.keep ?? 2, 1), // Minimum 1
    effectKeep: Math.max(input.effectKeep ?? 1, 1), // // Minimum 1
    effectModifier: input.effectModifier ?? 0,
    hitch: Math.max(input.hitch ?? 1, 0), // Minimum 0    
    extraEffectDieModeOn: iocContainer.options.extraEffectDieModeOn ?? false,
    extraRollDieIncreaseModeOn: iocContainer.options.extraRollDieIncreaseModeOn ?? false,
  }
  
  let dieModifier = input.dieModifier ?? 0
  let additionalMessageContent = []
  let nonPoolDice = input.nonPoolDice ?? []
  if(dieModifier != 0) {
    modifyResult = modifyDice(diceRoll, dieModifier, options)
    diceRoll = modifyResult.modifiedDice
    nonPoolDice.push(...modifyResult.nonPoolDice)
    additionalMessageContent = modifyResult.messageContent
    console.log('modified dice: ' + diceRoll.join(','))
  }
  console.log('non pool dice: ' + nonPoolDice.join(','))

  var rolls = rollDice(diceRoll, iocContainer)
  var nonPoolRolls= rollDice(nonPoolDice, iocContainer)
  let result = getResultsBasedOnRolls(diceRoll, rolls, nonPoolDice, nonPoolRolls, options)
  result.messageContent = additionalMessageContent.concat(result.messageContent)  
  return result
}

/*
cachedRolls: {diceRoll, rolls, ...} of previous roll
iocContainer - IocContainer

output: RollResult
*/
function rollCortexPrimeKeep (cachedRolls, keep, iocContainer) {
  console.log('input cachedRolls: ' + JSON.stringify(cachedRolls))
  console.log('input keep: ' + keep)
  
  let options = cachedRolls.options
  options.keep = Math.max(keep ?? 2, 0) // Minimum 0
  
  return getResultsBasedOnRolls(cachedRolls.diceRoll, cachedRolls.rolls, cachedRolls.nonPoolDice, cachedRolls.nonPoolRolls, options)
}

/*
cachedRolls: {diceRoll, rolls, ...} of previous roll
diceRoll: new rolls (parsed)
iocContainer - IocContainer

output: RollResult
*/
function rollCortexPrimeAppend(cachedRolls, diceRoll, iocContainer ) {
  console.log('input cachedRolls: ' + JSON.stringify(cachedRolls))
  console.log('input diceRoll: ' + diceRoll.join(','))
  
  var newRolls = rollDice(diceRoll, iocContainer)
  
  let concatDiceRolls = cachedRolls.diceRoll.concat(diceRoll)
  let concatRolls = cachedRolls.rolls.concat(newRolls)
  // TODO: dieModifier (e.g., step up) might not choose the right numbers!
  return getResultsBasedOnRolls(concatDiceRolls, concatRolls, cachedRolls.nonPoolDice, cachedRolls.nonPoolRolls, cachedRolls.options)
}

/*
input: 
  diceRoll - previous dice rolled
  stepUp - die type to step up (if 0, no step up)
  stepUpMod - by how much to step up
  iocContainer - IocContainer,
return: {
  newDiceRoll, 
  nonPoolDice - additional dice not part of effect pool or hitch
  messageContent - empty if no step up}
*/
function stepUp(diceRoll, stepUp, stepUpMod, iocContainer) {
  if (stepUp <= 0) {
    return {
      newDiceRoll: diceRoll,
      nonPoolDice: [],
      messageContent: [],
    }
  }
  
  let index = diceRoll.indexOf(stepUp)
  if(index < 0) {
    return {
      newDiceRoll: diceRoll,
      nonPoolDice: [],
      messageContent: [`No step up/down found for D${stepUp}: did not step up/down`],
    }
  }
  
  let newDiceRoll = diceRoll.concat([])
  let dieModification = modifyDie(stepUp, stepUpMod, iocContainer.options)
  newDiceRoll[index] = dieModification.modifiedDie
  
  return {
    newDiceRoll: newDiceRoll,
    nonPoolDice: dieModification.nonPoolDice,
    messageContent: messages.FormatDieModification(dieModification),
  }
}

/*
Merges each input die type to a single stepped up die.

input: {
  dice - list of input dice
  mergeDice - list of die types to merge  
}
iocContainer - IocContainer,
return: {
  newDiceRoll, 
  nonPoolDice - additional dice not part of effect pool or hitch
  messageContent - empty if no step up}
*/
function mergeDice (input, iocContainer) {  
  console.log('merge input: ' + JSON.stringify(input))
  if (input.dice.length <= 0 || input.mergeDice.length <= 0) {
    return {
      newDiceRoll: input.dice,
      nonPoolDice: [],
      messageContent: [`Did not merge: empty input`],
    }
  }
  
  let newDiceRoll = input.dice.concat([]) // clone
  let nonPoolDice= []
  let messageContent = []
  
  input.mergeDice.forEach(dieType => {
    let firstIndex = newDiceRoll.indexOf(dieType)
    let secondIndex = newDiceRoll.indexOf(dieType, firstIndex + 1)
    if(firstIndex == secondIndex || secondIndex < 0) {
      messageContent.push(`Did not find two input dice of type D${dieType}`)     
    } else {
      // Remove original die:
      newDiceRoll.splice(secondIndex, 1) // start with second index so first one doesn't change      
      
      let dieModification = modifyDie(dieType, 1, iocContainer.options)
      // replace first index
      newDiceRoll[firstIndex] = dieModification.modifiedDie 
      messageContent = messageContent.concat([`Removed one D${dieType}`, ...messages.FormatDieModification(dieModification)])     
      // set new die if needed
      nonPoolDice.push(...dieModification.nonPoolDice)      
    }    
  })  
  
  return {
    newDiceRoll: newDiceRoll,
    nonPoolDice: nonPoolDice,
    messageContent: messageContent ,
  }
  
}

/*
 By level: when positive, increases minimal dice, when negative, decreases maximal dice,
 Return: {
  modifiedDice: list of new dice to roll,
  nonPoolDice: list of dice to roll that cannot be hitched or used as effect,
  messageContent: list of strings,
}
*/
function modifyDice(diceRoll, dieModifier, options) {
  let aggregatedResult = {
    modifiedDice: diceRoll.concat([]), // cloned list
    nonPoolDice: [],
    messageContent: [],
  }
  if (diceRoll.length == 0) {
    return aggregatedResult 
  }
  
  // NOTE / WARNING: This won't work well with D12 that's already increased. Behavior is undefined. Currently it'll add another D4.
  
  // Do this thing "dieModifier" times.
  for (let i = 0; i < Math.abs(dieModifier); i++) {    
    if (dieModifier < 0) {
      // Decrease max die.
      let maxValue = Math.max(...aggregatedResult.modifiedDice)
      let index = aggregatedResult.modifiedDice.indexOf(maxValue)
      let modificationResult = modifyDie(maxValue, -1, options)
      aggregatedResult.modifiedDice[index] = modificationResult.modifiedDie 
      aggregatedResult.nonPoolDice.push(...modificationResult.nonPoolDice) 
      aggregatedResult.messageContent.push(...messages.FormatDieModification(modificationResult))
    } else { // dieModifier > 0
      // Increase min die.
      let minValue = Math.min(...aggregatedResult.modifiedDice)
      let index = aggregatedResult.modifiedDice.indexOf(minValue)
      let modificationResult = modifyDie(minValue, +1, options)
      aggregatedResult.modifiedDice[index] = modificationResult.modifiedDie 
      aggregatedResult.nonPoolDice.push(...modificationResult.nonPoolDice) 
      aggregatedResult.messageContent.push(...messages.FormatDieModification(modificationResult))
    }    
  }
  return aggregatedResult
}


/*
input: 
       diceRoll - list of input numbers, 
       rolls - list of roll results of rollDice(...), 
      nonPoolDice - list of input numbers that cannot be hitched or used for effect,
      nonPoolRolls - list of nonPoolDice roll results,
       options - {
            keep - positive number of dice to keep for score,
            effectKeep - positive number of dice to keep for effect,
            effectModifier (optional)  - modifies the effect die/dice,
            hitch (optional) - max number that counts as hitch,
            extraEffectDieModeOn - when true, shows increased effects as additional dice ; when false, shows as D12+},
            extraRollDieIncreaseModeOn - see IocConainer
          }
output: RollResult
*/
function getResultsBasedOnRolls(diceRoll, rolls, nonPoolDice, nonPoolRolls, options) {
  let messageContent = [ ];

  logMessage('options: ' + JSON.stringify(options))
  logMessage('rolls: ' + rolls.map(JSON.stringify).join(' ; '))
  logMessage('nonPoolRolls: ' + nonPoolRolls.map(JSON.stringify).join(' ; '))
  var scores = getAllScoreOptions(rolls, nonPoolRolls, options)
  logMessage('scoreEffects: ' + scores.map(JSON.stringify).join(' ; '))
  var modifiedScores = modifyEffects(scores, options)
  logMessage('Modified scoreEffects: ' + modifiedScores.map(JSON.stringify).join(' ; '))
  let maxScores = getMaximumScoreEffects(modifiedScores, options)
  logMessage('Max scores: ' + maxScores.map(JSON.stringify).join(' ; '))

  let formatInput = {
    dicePool: diceRoll, 
    rolls: rolls, 
    nonPoolDice: nonPoolDice,
    nonPoolRolls: nonPoolRolls,
    maxScores: maxScores,
    modifyEffectFunc: modifyEffect,
    options: options,
  }

  messageContent = messages.FormatResults(formatInput)
  logMessage('Formatted results: ')
  messageContent.forEach(t => console.log(t))

  return {
    messageContent: messageContent,
    diceRoll: diceRoll,
    rolls : rolls,
    nonPoolDice: nonPoolDice,
    nonPoolRolls: nonPoolRolls,
    scores : scores,
    maxScores : maxScores,
    options : options,
  }
}

// Input: scores - list of {score, effect, sourceRolls}, options - same as above.
// Returns: list of {score, list of effects, sourceRolls} with a modified score by options.
function modifyEffects(scores, options) {
  // We do not modify effects if there are more than one.
  if(options.effectKeep > 1) {
    return scores
  }
  
  effectModifier = options.effectModifier
  // returns a duplicated copy
  return scores.map(tuple => {
    let dup = {...tuple}; 
    dup.effect = new Effect(modifyEffect(dup.effect.getMaxEffect(), effectModifier), true); 
    return dup;
  })
}

// Input: an effect, and its modifier
// Returns: an effect modified. List of effects if increased.
function modifyEffect(effect, effectModifier) {
  let modifiedEffect = effect + effectModifier * 2
  if (modifiedEffect < MIN_EFFECT) {
    return [MIN_EFFECT]
  }
  if(modifiedEffect <= MAX_EFFECT) {
    return [modifiedEffect]
  }
  
  // Next effect is minimum MIN_EFFECT. But we need to remove 2 as it "costs" 2 to get there.
  let nextEffect = modifiedEffect - MAX_EFFECT + MIN_EFFECT - 2
  return [MAX_EFFECT, ...modifyEffect(nextEffect, 0)]
}

/*
Input: a die and a modifier level.
Returns: {
  originalDie - the original die,
  modifiedDie - a modified die, up to restrictions,
  nonPoolDice - a list of dice added to roll, that can't be hitched, and can't be used for effect.
}
*/
function modifyDie(die, modifier, options) {    
  let modifiedDie = die + modifier*2
  if (modifiedDie < MIN_DIE) {
    return {
      originalDie: die,
      modifiedDie: MIN_DIE,
      nonPoolDice: [],
    }
  }
  if(modifiedDie > MAX_DIE) {
    let extraDie = EXTRA_NON_POOL_DIE
    if(options.extraRollDieIncreaseModeOn) {
      // Step up as additionally can, considering max value.
      extraDie = Math.min(EXTRA_NON_POOL_DIE + (modifiedDie - MAX_DIE) - 2, MAX_DIE)
    }
    
    return {
      originalDie: die,
      modifiedDie: MAX_DIE,
      nonPoolDice: [extraDie],
    }
  }
  return {
    originalDie: die,
    modifiedDie: modifiedDie,
    nonPoolDice: [],
  }}





// Input: list of {score, Effect, sourceRolls}, options
// Returns list of {score, Effect, sourceRolls}, filtered by scores who make sense
function getMaximumScoreEffects(scoreEffectResults, options) {
    if (scoreEffectResults.length <= 1){
      return scoreEffectResults
    }
  
    let effectValueFunc = scoreEffect => getEffectValue(scoreEffect.effect, options)    
  
    // Key: effect. Value: ScoreEffect of a maximum scores.
    effectToScoreMap = new Map()
    scoreEffectResults.forEach(scoreEffect => {
      let effectValue = effectValueFunc(scoreEffect)      
      let currentMaxScore = effectToScoreMap.get(effectValue)?.score || 0
      if (scoreEffect.score > currentMaxScore ){
        effectToScoreMap.set(effectValue, scoreEffect)
      }  
    })   
    
    logMessage('effectToScoreMap: ' + JSON.stringify(Object.fromEntries(effectToScoreMap)))
    let scoreEffectList = Array.from(effectToScoreMap.values())
    logMessage('scoreEffectList: ' + JSON.stringify(scoreEffectList))
    if (scoreEffectList.length <= 1){
      return scoreEffectList 
    }
    
    let sortedByEffectDesc = scoreEffectList.sort(
      (scoreEffect1, scoreEffect2) => (effectValueFunc(scoreEffect1) > effectValueFunc(scoreEffect2) ) ? -1 : 1)
    
    // Highest effect is always an option
    maxScoreEffectResults = [sortedByEffectDesc[0]]
    sortedByEffectDesc.slice(1).forEach(scoreEffect => {
      // We know the effect is strictly lower than anything in maxScoreEffectResults.
      // If we have the highest score, we add ourselves.
      if(scoreEffect.score > Math.max(...maxScoreEffectResults.map(se => se.score))){
        maxScoreEffectResults.push(scoreEffect)
      }
    })
    
    logMessage('maxScoreEffectResults: ' + JSON.stringify(maxScoreEffectResults))
    
    return maxScoreEffectResults
  }

// Input: effect - Effect, options
// Output: value of an effect, that can be used as key or for sort
function getEffectValue(effect, options) {
  let effectList = effect.getEffectList()
  
  if (effectList.length == 0) {
    return MIN_EFFECT;
  }
  
  if (effect.isSingleEffect()) {
    let value = effectList.reduce((x,y)=> x+y, 0)
    if (!options.extraEffectDieModeOn) {
      if (effectList.length == 1) {
        value = effectList[0]
      } else {
        // All D12+ are treated the same:
        value = MAX_EFFECT + 1
      }
    }
    return value
  }
  
  // Compare this differently. If both are bigger - yes. Otherwise - non comparable...? Compare first, then next, then....
  // D6 D4 - see tests for more examples.
  // D6 D6
  // D8 D4
  // IDEA - convert integers to character (can't be negative), and treat them as string. then compare between strings!

  return String.fromCharCode(...effectList)
}

/* 
   Input: 
    dieResults - list of { die: / result: }, 
    nonPoolResults - list of { die: / result: } not part of pool/hitch.
    options - {
      keep -  number of how many score dice,
      effectKeep - keep for effect,
      effectModifier (optional)  - modifies the effect die/dice,
      hitch (optional) - max number that counts as hitch,
    }
   Returns list of {score, effect - Effect, sourceRolls}
   sourceRolls - list of source rolls which got this score
*/
function getAllScoreOptions(dieResults, nonPoolResults, options) {
  const keep = options.keep
  const effectKeep = options.effectKeep 
  const hitch = options.hitch  
  let validForEffect = dieResults
    .map((roll, index) => { roll.originalIndex = index; return roll;})
    .filter(d => d.result > hitch)
  let validForScore = validForEffect.concat(nonPoolResults
    .map((roll, index) => { roll.originalIndex = dieResults.length + index; return roll;}))
  let length = validForScore.length 
  if (length == 0){
    // No valid score, means there's no valid effect!
    return [{score: 0, effect: new Effect([MIN_EFFECT]), sourceRolls: []}]
  } else if (keep == 0) {
    let maxEffect = getMaxEffect(validForEffect, new Set(), effectKeep)
    return [{score: 0, effect: maxEffect, sourceRolls: []}]
  }
  
  let allKeepIndexSets = getAllIndexSets(keep, 0, length)
  if (allKeepIndexSets.length == 0 || allKeepIndexSets[0].size == 0){
    throw 'Unexpected behavior: ' + messages.PrintIndexSet(allKeepIndexSets) + ' : ' + keep + ',' + length;
  }
    
  var results = []
  allKeepIndexSets.forEach(indexSet => {
    // Each indexSet is a list of (distinct) indexes that can form a score.
    let allValidResults = Array.from(indexSet).map(i => validForScore[i])
    let originalIndexSet = new Set(allValidResults.map(result => result.originalIndex))
    
    let totalScore = allValidResults.map(r => r.result).reduce((x,y) => x+y, 0)
    
    let maxEffect = getMaxEffect(validForEffect, originalIndexSet, effectKeep)
    results.push({score: totalScore  , effect: maxEffect , sourceRolls: allValidResults.sort(byDieAscending) })
  })

  logMessage(`allKeepIndexSets: ` + messages.PrintIndexSet(allKeepIndexSets))
  logMessage(`results: ` + JSON.stringify(results))
  
  return results
}

/*
validForEffect: dice valid for effect, including dice that were used for score
scoreDiceOriginalIndexSet: Set of indexes in the orginal dice pool that were used for score.
effectKeep: effect dice to kee[]

Returns: Effect
*/
function getMaxEffect(validForEffect, scoreDiceOriginalIndexSet, effectKeep) {
  // Take an effect die which was not chosen for score and is not a hitch.
  let effectDicePool = validForEffect.filter(validRoll => !scoreDiceOriginalIndexSet.has(validRoll.originalIndex)).map(d => d.die)       
  
  let paddedEffectDicePool = effectDicePool.concat(Array(Math.max(0, effectKeep - effectDicePool.length)).fill(MIN_EFFECT))
  let effectDicePoolByMax = paddedEffectDicePool.sort(intSort).reverse()
  let maxEffects = effectDicePoolByMax.slice(0, effectKeep)
  // maxEffect = Math.max(...effectDicePool, MIN_EFFECT)
  return new Effect(maxEffects)
}

// Function for sorting by di then by roll.
function byDieAscending(validResult1, validResult2) {
  if(validResult1.die > validResult2.die) {
    return 1
  } else if (validResult1.die < validResult2.die) {
    return -1
  }
  
  if(validResult1.result > validResult2.result) {
    return 1
  } else if (validResult1.result < validResult2.result) {
    return -1
  }

  return 0;
}


function intSort(a, b) {
  return a - b;
}


// Returns a list of Set of indexes. Size of each hashset is min(keep, length).
// Algorithm to get all "k out of n" indexes -  list of set / hashset
// length > 0.
// startIndex - when calling this non-recursively, should be 0.
function getAllIndexSets(keep, startIndex, length) {
  if (keep == 0 || length == 0 || startIndex == length) {
    return [new Set()];
  }
  
  countLeft = length - startIndex
  if (keep >= countLeft ) {
    let hash = new Set(range(startIndex, length - 1))
    return [hash]
  }
  
  var resultSets = []
  // Can't take too much now.
  for (var i = startIndex; i < length - keep + 1 ; i++) { 
    let allChildSets = getAllIndexSets(keep - 1, i + 1, length)
    let resultsForIndex = allChildSets.map(hashset => hashset.add(i))
    resultSets = resultSets.concat(resultsForIndex)
  }
  
  return resultSets 
}

function range(startAt, endAt) {  
    return [...Array(endAt-startAt+1).keys()].map(i => i + startAt);
}


// Input: list of dice max values
// Returns { die: / result: }
function rollDice(dice, iocContainer) {
  rollDie = d => iocContainer.getNextNumber(1, d)
  return dice.map( d => ({ result: rollDie(d) , die:d }))
}

function logMessage(msg) {
  if(CONSOLE_LOG) {
    console.log(msg);
  }
}

class Effect {
  constructor(effectsDesc, isIncreased = false) {
    this.effects = effectsDesc
    this.maxEffect = effectsDesc[0]
    this.isIncreased = isIncreased
  }
  
  getEffectList() {
    return this.effects
  }
  
  getMaxEffect() {
    return this.maxEffect
  }
  
  isIncreased() {
    return this.isIncreased
  }
  
  // In case there's only one effect, or it is considered as an increase - this is a single effect.
  isSingleEffect() {
    return this.effects.length == 1 || this.isIncreased
  }
}