﻿using FolderSyncronizer.Debugging;
using Infra.Framework.Extensions;
using System;
using System.IO;

namespace FolderSyncronizer.Core.Watchers.Actions
{
    public class RenameAction : ActionBase
    {
        // Do not use - for serialization only! All properties should not be set externally.
        public RenameAction() { }

        public RenameAction(string sourceFile, string sourceNewFile, string targetFile, string targetNewFile)
        {
            SourceFile = sourceFile;
            SourceNewFile = sourceNewFile;
            TargetFile = targetFile;
            TargetNewFile = targetNewFile;
        }

        public string SourceNewFile { get; set; }
        public string TargetNewFile { get; set; }

        public override bool IsCancelled { get => base.IsManuallyCancelled || GetRenameType() == RenameType.Cancelled; set => base.IsCancelled = value; }
        protected override string ActionDescription => $"Rename: {(GetRenameType())} '{TargetFile}' => '{TargetNewFile}' (source: '{SourceFile}')";

        protected override void ApplyAction()
        {
            switch (GetRenameType())
            {
                case RenameType.File:
                    File.Move(TargetFile, TargetNewFile);
                    break;
                case RenameType.Directory:
                    Directory.Move(TargetFile, TargetNewFile);
                    break;
            }
        }

        public override bool ShouldAffectOlderAction(ActionBase olderAction)
        {
            return GetOlderActionEffectType(olderAction) != OlderActionEffectType.DoNothing;
        }


        protected override void ChangeOlderAction(ActionBase olderAction)
        {
            OlderActionEffectType actionEffectType = GetOlderActionEffectType(olderAction, logWarnings: false);

            if (olderAction is DeleteAction)
            {
                // File was deleted, not need to address it.
                return;
            }

            switch (actionEffectType)
            {
                case OlderActionEffectType.FileRename:
                case OlderActionEffectType.DirectoryRename:
                    ChangeOlderActionDueToSourceFileOrFolder(olderAction);
                    break;
                case OlderActionEffectType.NewFileOrFolderRename:
                    ChangeOlderActionDueToTransitiveRename(olderAction);
                    break;
            }
        }

        private void ChangeOlderActionDueToTransitiveRename(ActionBase olderAction)
        {
            if (olderAction is RenameAction olderRenameAction)
            {
                // Renaming something like (x -> y) then (y -> z), should affect:
                olderRenameAction.SourceNewFile = this.SourceNewFile;
                olderRenameAction.TargetNewFile = this.TargetNewFile;

                // The "if" might not be relevant, since if it's so, it means the same file was renamed somewhere else and "this" will be cancelled later anyway.
                if (!olderAction.IsCancelled)
                {
                    Singleton.Logger.LogDebug($"Cancelling this rename \"{this.Description}\" due to merging with older rename \"{olderRenameAction.Description}\"");
                    this.IsCancelled = true;

                    if (olderRenameAction.TargetFile.EqualsIgnoreCase(olderRenameAction.TargetNewFile))
                    {
                        Singleton.Logger.LogDebug($"Cancelling old rename \"{olderRenameAction.Description}\" since Target and TargetNew are the same");
                        olderRenameAction.IsCancelled = true;
                    }
                }
            }
        }

        private void ChangeOlderActionDueToSourceFileOrFolder(ActionBase olderAction)
        {
            // Relevant to both copy and rename: Change the folder prefix.
            // Note this doesn't affect the actual action of rename.
            olderAction.SourceFile = olderAction.SourceFile.Replace(this.SourceFile, this.SourceNewFile, StringComparison.InvariantCultureIgnoreCase);

            if (olderAction is CopyAction olderCopyAction)
            {
                // The "if" might not be relevant, since if it's so, it means the same file was copied somewhere else and "this" will be cancelled later anyway.
                if(!olderAction.IsCancelled)
                {
                    switch (GetRenameType())
                    {
                        case RenameType.File:
                            // Renamed source of copy? Can cancel rename, and just copy to target.
                            olderCopyAction.TargetFile = olderCopyAction.TargetFile.Replace(this.TargetFile, this.TargetNewFile, StringComparison.InvariantCultureIgnoreCase);
                            Singleton.Logger.LogDebug($"Cancelling this rename \"{this.Description}\" due to merging with older copy \"{olderCopyAction.Description}\"");
                            this.IsCancelled = true;
                            break;
                        case RenameType.Directory:
                            // Directory rename - we do not cancel and no need to affect the target. It's not merged into anything older. The rename will fix the final state.
                            break;
                    }
                }
            }
        }

        protected OlderActionEffectType GetOlderActionEffectType(ActionBase olderAction, bool logWarnings = true)
        {
            // Note: Don't look at olderAction.IsCancelled, since the rename could cause it to be cancelled. We should check the IsCancelled only before cancelling *this*.
            
            bool sameSourceFiles = base.ShouldAffectOlderAction(olderAction);

            if (olderAction is DeleteAction)
            {
                // The target of the older action is still the same and needs to be deleted. Nothing to do.
                // Example: Deleted dir/x, then renamed dir.
                
                if (sameSourceFiles)
                {
                    // We should still do nothing if the file was recreated.
                    Warn(logWarnings, $"Older Delete \"{olderAction.Description}\" and new Rename \"{this.Description}\" has the same SourceFile. This shouldn't happen unless the file was recreated.");
                }

                return OlderActionEffectType.DoNothing;
            }

            if (olderAction is RenameAction olderRenameAction)
            {
                // Renaming something like (x -> y) then (y -> z), should affect:
                bool transitiveRename = this.SourceFile.EqualsIgnoreCase(olderRenameAction.SourceNewFile);

                if(transitiveRename)
                {
                    if (sameSourceFiles)
                    {
                        Warn(logWarnings, $"Older Rename \"{olderAction.Description}\" and new \"{this.Description}\" has the same SourceFile. This shouldn't happen since is means the rename from from X to X.");
                    }
                    return OlderActionEffectType.NewFileOrFolderRename;
                }
            }

            if (sameSourceFiles)
            {
                return OlderActionEffectType.FileRename;
            }

            if(olderAction.SourceFile.Contains(this.SourceFile + Path.DirectorySeparatorChar, StringComparison.InvariantCultureIgnoreCase))
            {
                // this.SourceFile is a Directory:
                if(GetRenameType() != RenameType.Directory)
                {
                    Warn(logWarnings, $"Older action \"{olderAction.Description}\" contains source of new rename \"{this.Description}\" but the type is not Directory, instead: {GetRenameType()}.");
                }
                return OlderActionEffectType.DirectoryRename;
            }

            // No relationship between the source files.
            return OlderActionEffectType.DoNothing;
        }

        private void Warn(bool logWarnings, string msg)
        {
            if(logWarnings)
            {
                Singleton.Logger.LogWarning(msg);
            }
        }

        protected enum OlderActionEffectType
        {
            DoNothing,
            // (probably not rename) something that uses x then (x -> y).
            FileRename,
            // Renaming (or another action) something like (dir\x -> dir\y) then (dir -> renamedDir). If older is rename, should it affect more then just the source file? No, the target at the time is the same, just don't cancel this.
            DirectoryRename,
            // Renaming something like (x -> y) then (y -> z) - "transitive rename", relevant to both files and folders.
            NewFileOrFolderRename,
        }

        protected RenameType GetRenameType()
        {
            // After this occured, the SourceFile shouldn't exist anymore.
            if (File.Exists(SourceNewFile) || File.Exists(TargetFile))
            {
                return RenameType.File;
            }
            if (Directory.Exists(SourceNewFile) || Directory.Exists(TargetFile))
            {
                return RenameType.Directory;
            }

            return RenameType.Cancelled;
        }

        protected enum RenameType
        {
            Cancelled,
            File,
            Directory,
        }
    }
}
