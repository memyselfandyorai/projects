const DEFAULT_ACCOUNT = "Yorai Otsar";

const DEFAULTS_SHEET_NAME = 'Defaults'
const DEFAULTS_PROPERTY_NAME = 'DefaultsObj'


// 'Expenses Sheets'
// Assumption: sheets are "sorted". i.e., distinct date ranges (inclusive)
const DEFAULTS_EXPENSE_SHEETS_ROW = 2
// 'Non-Expenses Sheets'
const DEFAULTS_NON_EXPENSE_SHEETS_ROW = 3
// 'Default Account'
const DEFAULTS_DEFAULT_ACCOUNT_ROW = 4

// 'Order of the first categories'
const CATEGORY_ORDER_ROW = 5
// 'Descending order of the last categories (first here is last one shown)'
const CATEGORY_DESC_ORDER_ROW = 6
// 'All accounts'
const ALL_ACCOUNTS_ROW = 7


const DEFAULTS_VALUES_START_COL = 2



const DEFAULTS_COLUMN_PAYEE = 1;
const DEFAULTS_COLUMN_ACCOUNT = 2;
const DEFAULTS_COLUMN_CATEGORY = 3;
const DEFAULTS_COLUMN_AMOUNT = 4;
const DEFAULTS_COLUMN_NOTE = 5;

const DEFAULTS_ROW_PAYEE_START = 5;


function reloadDefaults() {
  let valueDefaults = createDefaults()
  setProperty(DEFAULTS_PROPERTY_NAME, valueDefaults)
}

function getDefaults() {
  return getProperty(DEFAULTS_PROPERTY_NAME)
}


/**
 * Returns: {
 *  defaultAccount,
    payeeAccountMap,
    payeeCategoryMap,
    payeeAmountMap,
    payeeDescriptionMap,
    expenseSheetNameList,
    nonExpenseSheetNameList,
    firstCategoriesAsc,
    lastCategoriesDesc,
    allAcountNames,
 * } 
 */
function createDefaults() {
  let spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  let defaultsSheet = spreadsheet.getSheetByName(DEFAULTS_SHEET_NAME)
  if (!defaultsSheet) {
    return undefined
  }

  let accountName = defaultsSheet.getRange(DEFAULTS_DEFAULT_ACCOUNT_ROW, DEFAULTS_VALUES_START_COL).getValue()

  let payeeAccountMap = {}
  let payeeCategoryMap = {}
  let payeeAmountMap = {}
  let payeeDescriptionMap = {}

  let defaultsRange = defaultsSheet.getRange(DEFAULTS_ROW_PAYEE_START, DEFAULTS_COLUMN_PAYEE, defaultsSheet.getLastRow() - 1, defaultsSheet.getLastColumn()).getValues()
  let indexOffset = -1
  let columnToDefaultMapMap = new Map()
    .set(DEFAULTS_COLUMN_ACCOUNT, payeeAccountMap)
    .set(DEFAULTS_COLUMN_CATEGORY, payeeCategoryMap)
    .set(DEFAULTS_COLUMN_AMOUNT, payeeAmountMap)
    .set(DEFAULTS_COLUMN_NOTE, payeeDescriptionMap)

  for(var row = 0; row < defaultsRange.length; row++) {
    let rangeRow = defaultsRange[row]
    let payee = rangeRow[DEFAULTS_COLUMN_PAYEE + indexOffset]
    if (!payee) {
      continue
    }

    for(let column of columnToDefaultMapMap.keys()) {
      let columnValue = rangeRow[column + indexOffset]
      if (columnValue) {
        let map = columnToDefaultMapMap.get(column)
        map[payee] = columnValue
      } 
    }
  }

  let expenseSheetNames = getRowValues(defaultsSheet, DEFAULTS_EXPENSE_SHEETS_ROW)
  let nonExpenseSheetNames = getRowValues(defaultsSheet, DEFAULTS_NON_EXPENSE_SHEETS_ROW)
  let firstCategoriesAsc = getRowValues(defaultsSheet, CATEGORY_ORDER_ROW)
  let lastCategoriesDesc = getRowValues(defaultsSheet, CATEGORY_DESC_ORDER_ROW)
  let allAcountNames = getRowValues(defaultsSheet, ALL_ACCOUNTS_ROW)

  return {
    defaultAccount: accountName ?? DEFAULT_ACCOUNT,
    payeeAccountMap: payeeAccountMap,
    payeeCategoryMap: payeeCategoryMap,
    payeeAmountMap: payeeAmountMap,
    payeeDescriptionMap: payeeDescriptionMap,
    expenseSheetNameList: expenseSheetNames,
    nonExpenseSheetNameList: nonExpenseSheetNames,
    firstCategoriesAsc: firstCategoriesAsc,
    lastCategoriesDesc: lastCategoriesDesc,
    allAcountNames: allAcountNames,
  }
}

function getRowIndexByName(sheet, colName) {
    var data = sheet.getDataRange().getValues();
    return data[0].indexOf(colName);
}

function getRowValues(sheet, row) {
  let singleRow = sheet.getRange(row, DEFAULTS_VALUES_START_COL, 1, sheet.getDataRange().getNumColumns() - 1).getValues()[0]
  return singleRow.filter(value => value && value != '')
}

function setProperty(key, value) {
    PropertiesService.getScriptProperties().setProperty(key, JSON.stringify(value))
}

function getProperty(key) {
    return JSON.parse(PropertiesService.getScriptProperties().getProperty(key))
}