import java.util.*;

public class Enemy extends Char{
	
	//don't forget saves in spells.
		
	private double lvl; //lvl of enemy (?)
	private int hide; //skill
	private int spot; //skill
	private int pick; //skill - useless?
	
	
	//poison,drain (+vampire?),sleep,paralysis,breath acid, breath fire
	
	//skeleton(0.3),zombie(1),snake(1),wraith(5),ogre(2),ogre mage(8),orc(0.5),orc archer(0.5),goblin(0.2),dretch(2),quasit(3),lemure(1),imp(2),osyluth(6),gnome(0.5),gnoll(1),ghoul(1)						
	//http://www.google.co.il/imghp?hl=iw&tab=wi&q=
	
	//thief, murderer,
	
	//sex gives bonuses\penalties?
	
	public Enemy(String n,int h,int st,int de,String ra,boolean mm,int nat/**/,int co,int in,int wi,int ch,double l,int hi,int sp,int pi,int r,int w,int f)
	{
	super(n,h,st,de,ra,mm,nat);	
	con=co;
	wis=wi;
	inl=in;
	cha=ch;
	lvl=l;
	hide=hi;
	spot=sp;
	pick=pi;
	ref=f;
	will=w;
	fort=f;
	}
	
	
	public Enemy(Place plc)
	{
	super(plc);
	}
	

	public Enemy(String n,Place plc)
	{
	super(n,plc); //nata,damr, items, w\s\a, gp, race, hp
	//race- orc, goblin, dragon? no reason to not- no more work.
	if(Dice.roll(2)==1)
	male=true;
	else
	male=false;
	
		if(n.equals("Skeleton"))
		{
		hp=Dice.roll(12);
		nata=2;
		str=10;
		dex=12;
		con=10; //none
		inl=10; //none
		wis=10;
		cha=11;
		race="Undead"; //no poison, stun\paralize (?)
		fort=0;
		ref=1;
		will=2;
		weapon=new Weapon("Claw","",0,0,1,4,2,this); //not in items.
		lvl=0.33; //easier than 1/3
		//gp=0;
		// Weapon(String n,String k,double w,double v,int ds,int de,int c,Char o)
		hide=0;
		spot=0;
		pick=0;
		}			
		
		if(n.equals("Zombie")) //large
		{
		hp=Dice.roll(4,12,3);
		nata=2;
		str=17; //3
		dex=8; //-1
		con=10; //none
		inl=10; //none
		wis=10;
		cha=1; //-4
		race="Undead"; //no poison, stun\paralize (?)
		
		fort=1;
		ref=1;
		will=4;
		weapon=new Weapon("Slam","",0,0,1,1,8,1,this,3); //not in items.
		lvl=1;
		//gp=0;
		// Weapon(String n,String k,double w,double v,int ds,int de,int c,Char o)
		hide=0;
		spot=0;
		pick=0;
		}			
		
		if(n.equals("Snake")) //large
		{
		hp=Dice.roll(3,8);
		nata=2;
		str=10; //0
		dex=17; //3
		con=11; //0
		inl=1; //-4
		wis=12; //1
		cha=2; //-4
		race="Animal";
		
		fort=3;
		ref=3;
		will=1;
		weapon=new Weapon("Bite","",0,0,4,1,4,0,this,1); //not in items.
		lvl=1;

		spells.add("Poison",4,4,4,2); //1d6 damage.
		//name,level,uses,turns,save
		
		hide=5;
		spot=8;
		pick=0;
		}		
		
		if(n.equals("Wraith"))
		{
		hp=Dice.roll(3,8);
		nata=2;
		str=10; //0
		dex=16; //3
		con=10; //0
		inl=14; //2
		wis=14; //2
		cha=15; //2
		race="Undead";
		
		fort=1;
		ref=1;
		will=-1;
		weapon=new Weapon("Touch","",0,0,4,1,4,0,this,2); //not in items.
		lvl=5;

		spells.add("Drain",2,-1,0,0); //1d6 drains damage
		//name,level,uses,turns,save
		damr=2;
		
		hide=8; //dex
		spot=8; //wis
		pick=0;
		}
		
		if(n.equals("Wyvern"))
		{
		hp=Dice.roll(7,12,14);
		nata=6;
		str=19; //4
		dex=12; //1
		con=15; //2
		inl=6; //-2
		wis=12; //1
		cha=9; //-1
		race="Dragon";
		
		fort=5;
		ref=5;
		will=5;
			if(Dice.roll(2)==1)
			weapon=new Weapon("Bite","",0,0,0,2,8,-2,this,2); //not in items.
			else
			weapon=new Weapon("Wing","",0,0,0,1,8,-2,this,2); //not in items.
		lvl=6;

		spells.add("Poison",5,-1,Dice.roll(2,4),2); //fort (dc 17?)
		//name,level,uses,turns,save
		damr=2;
		
		hide=4; //dex
		spot=14; //wis
		pick=0;
		}
		
		if(n.equals("Vampire"))
		{
		hp=Dice.roll(4,12);
		nata=3;
		str=16; //3
		dex=14; //2
		con=10; //0
		inl=13; //1
		wis=13; //1
		cha=14; //2
		race="Undead";
		
		fort=1;
		ref=3;
		will=4;
		weapon=new Weapon("Slam","",0,0,2,1,6,1,this,2); //not in items.
					
		lvl=4;

		spells.add("Drain",4,-1,0,0); //1d4(?) drains damage
		//name,level,uses,turns,save
		damr=3;
		
		hide=8; //dex
		spot=10; //wis
		pick=0;
		}
		
		
		if(n.equals("Ogre"))
		{
		hp=Dice.roll(4,8,8);
		nata=7;
		str=21;
		dex=8;
		con=15;
		inl=6;
		wis=10;
		cha=7;
		race="Ogre";
		fort=1; //basic or all?
		ref=1;
		will=1;
		weapon=new Weapon("Greatclub","",15,60,2,6,2,this);
		weapon.magic(3,2); //+3 attack,+2 damage
		items.add(weapon);
		
		lvl=2; 

		hide=0;
		spot=2;
		pick=0;
		}
	
		
		if(n.equals("Ogre Mage"))
		{
		hp=Dice.roll(5,8,15);
		nata=5;
		str=21;
		dex=10;
		con=17;
		inl=14;
		wis=14;
		cha=17;
		race="Ogre";
		fort=2;
		ref=1;
		will=1;
		
			
		armor=Armor.createa("Chain Shirt",this);
		items.add(armor);
		
		
		if(Dice.roll(3)>1)
		{
		weapon=new Weapon("Huge greatsword","",20,80,2,8,2,this);
		weapon.magic(3,2); //+3 attack,+2 damage
		items.add(weapon);
		}
		else
		{
		weapon=new Weapon("Huge longbow","Arrows",15,90,2,6,2,this); 
		weapon.magic(3,0); //+3 attack,+2 damage
		items.add(weapon);
		items.add(new Item("Arrows"));
		items.add(new Weapon("Huge greatsword","",20,80,3,2,8,2,this,2));
		}
		lvl=8; 
				
		hide=0;
		spot=5;
		pick=0;
		
		spells.add("Sleep",5,5,8);
		//name,level,uses,turns
		
		}
	
				
		if(n.equals("Orc"))
		{
		hp=Dice.roll(1,8);
		nata=0;
		str=15;
		dex=10;
		con=11;
		inl=9;
		wis=8;
		cha=8;
		race="Orc"; //suffers -1 on attack rolls in the sun.
		fort=0;
		ref=0;
		will=0;
		
		weapon=Weapon.create("Greataxe",this);
		weapon.magic(1,1);
		items.add(weapon);
		
		armor=Armor.createa("Scale Mail",this);
		items.add(armor);
		
		lvl=0.5; 
				
		hide=0;
		spot=3;
		pick=0;
		}

		
		if(n.equals("Orc Archer"))
		{
		hp=Dice.roll(1,8);
		nata=0;
		str=10;
		dex=15;
		con=11;
		inl=9;
		wis=8;
		cha=8;
		race="Orc"; //suffers -1 on attack rolls in the sun.
		fort=0;
		ref=0;
		will=0;
		
		weapon=Weapon.create("Longbow",this);
		weapon.magic(1,0);
		items.add(weapon);
		items.add(new Item("Arrows"));
		
		armor=Armor.createa("Scale Mail",this);
		items.add(armor);
		
		lvl=0.5; 
				
		hide=0;
		spot=3;
		pick=0;
		}
				
	
		if(n.equals("Goblin")) //another one?
		{
		hp=Dice.roll(1,8);
		nata=0;
		str=8; //-1
		dex=13; //+1
		con=11;
		inl=10;
		wis=11;
		cha=8;
		race="Goblin";
		fort=3;
		ref=0;
		will=0;
		
		weapon=new Weapon("Morningstar","",6,14,1,1,8,0,this,2);
		items.add(weapon);
		
		lvl=0.25; 
				
		hide=5;
		spot=3;
		pick=1;
		}
		
	
		if(n.equals("Dretch"))
		{
		hp=Dice.roll(2,8);
		nata=6;
		str=10;
		dex=10;
		con=10;
		inl=5; //-3
		wis=11;
		cha=11;
		race="Demon";
		fort=3;
		ref=3;
		will=3;
		
		weapon=new Weapon("Claw","",0,0,3,1,4,0,this,2); //not item
				
		//(String n,String k,double w,double v,int ab,int ds,int de,int db,Char o,int c)
	
		lvl=2; 
		
		damr=1;
		sr=5;
		hide=0;
		spot=0;
		pick=0;
		}
		

		if(n.equals("Quasit"))
		{
		hp=Dice.roll(3,8);
		nata=5;
		str=8; //-1
		dex=17; //+3
		con=10;
		inl=10;
		wis=12; //+1
		cha=10;
		race="Demon";
		fort=4;
		ref=3;
		will=3;
		
		weapon=new Weapon("Bite","",0,0,4,1,4,0,this,2); //not item
				
		lvl=3; 
		
		damr=1;
		sr=5;
		hide=8;
		spot=6;
		pick=0;
		}
		
		
		if(n.equals("Lemure"))
		{
		hp=Dice.roll(2,8);
		nata=3;
		str=10;
		dex=10;
		con=10;
		inl=0;
		wis=11;
		cha=5; 
		race="Devil";
		fort=3;
		ref=3;
		will=3;
		
		weapon=new Weapon("Claw","",0,0,2,1,3,0,this,2); //not item
				
		lvl=1; 
		damr=1;
		sr=5;
		hide=0;
		spot=0;
		pick=0;
		}
		
		
		if(n.equals("Imp"))
		{
		hp=Dice.roll(3,8);
		nata=5;
		str=10;
		dex=17;
		con=10;
		inl=10;
		wis=12;
		cha=10; 
		race="Devil";
		fort=3;
		ref=3;
		will=3;
		
		weapon=new Weapon("Sting","",0,0,8,1,4,0,this,2); //not item
				
		//(String n,String k,double w,double v,int ab,int ds,int de,int db,Char o,int c)
	
		lvl=2; 
		damr=1;
		sr=5;
		hide=8;
		spot=5;
		pick=0;
		}
		
				
		if(n.equals("Osyluth"))
		{
		hp=Dice.roll(5,8,10);
		nata=7;
		str=21; //5
		dex=10; //0
		con=15; //2
		inl=14; //2
		wis=14; //2
		cha=14; //2
		race="Devil";
		fort=4;
		ref=4;
		will=4;
		
		weapon=new Weapon("Bite","",0,0,4,1,8,0,this,2); //not item
		
		spells.add("Poison",4,6,4,2); //1d6 damage.
		//name,level,uses,turns,save
		
		lvl=6; 
		damr=4;
		sr=22;
		
		hide=6;
		spot=10;
		pick=0;
		}
		
		
		
		if(n.equals("Gnome"))
		{
		hp=Dice.roll(1,8,1);
		nata=1;
		str=8; //-1
		dex=10; 
		con=12; //1
		inl=11;
		wis=11;
		cha=11;
		race="Gnome";
		fort=2;
		ref=0;
		will=0;
		
		if(Dice.roll(2)==1)
		{
			weapon=Weapon.create("Short Sword",this); 
			weapon.magic(3,0);
		}
		else
		{
			weapon=Weapon.create("Light Crossbow",this); 
			weapon.magic(2,0);
		}


				
		shield=Shield.create("Small Shield",this);
		armor=Armor.createa("Chain Shirt",this);
		
		items.add(new Weapon(weapon.getName()));
		items.add(armor);
		items.add(shield);

		
		lvl=0.5; 
		damr=0;
		sr=0;
		
		hide=0; //dex
		spot=2; //wis
		pick=0; //dex
		}
		

		if(n.equals("Gnoll"))
		{
		hp=Dice.roll(2,8,2);
		nata=0;
		str=15; //2
		dex=10; 
		con=13; //1
		inl=8; //-12
		wis=11;
		cha=8; //-1
		race="Gnoll";
		fort=3;
		ref=0;
		will=0;
		
		weapon=Weapon.create("Battleaxe",this); 
		weapon.magic(1,0);
		
		shield=Shield.create("Steel Shield",this);
		armor=Armor.createa("Scale Mail",this);
		
		items.add(weapon);
		items.add(armor);
		items.add(shield);
		
		//power attack?
				
		//(String n,String k,double w,double v,int ab,int ds,int de,int db,Char o,int c)
		//name,level,uses,turns,save
		
		lvl=1; 
		damr=0;
		sr=0;
		
		hide=0; //dex
		spot=3; //wis
		pick=0; //dex
		}
		
		
		
		if(n.equals("Ghoul"))
		{
		hp=Dice.roll(2,12);
		nata=2;
		str=13; //1
		dex=15; //2 
		con=10; //none
		inl=13; //1
		wis=14; //2
		cha=16; //3
		race="Undead";
		fort=5; 
		ref=0;
		will=3;
		
		weapon=new Weapon("Bite","",0,0,2,1,6,0,this,2); 
				
		spells.add("Paralysis",4,4,Dice.roll(6)+1,2); //paralized. can't act+no dex bonus 
		
		//(String n,String k,double w,double v,int ab,int ds,int de,int db,Char o,int c)
		//name,level,uses,turns,save
		
		lvl=1; 
		damr=0;
		sr=0;
		
		hide=5; //dex
		spot=5; //wis
		pick=0; //dex
		}
		
		if(n.equals("Wolf"))
		{
		hp=Dice.roll(2,8,4);
		nata=2;
		str=13; //1
		dex=15; //2 
		con=15; //2
		inl=2; //-4
		wis=12; //1
		cha=6; //-2
		race="Animal";
		fort=3; 
		ref=3;
		will=0;
		
		weapon=new Weapon("Bite","",0,0,2,1,6,0,this,2); 
		
		lvl=1; 
		damr=0;
		sr=0;
		
		hide=1; //dex
		spot=3; //wis
		pick=0; //dex
		}		
		
		
		if(n.equals("Black Dragon")) //young adult
		{
		hp=Dice.roll(16,12,48);
		nata=14;
		str=19; //4
		dex=10; //0 
		con=17; //3
		inl=12; //1
		wis=13; //1
		cha=12; //1
		race="Dragon";
		fort=10; 
		ref=10;
		will=10;
		
		weapon=new Weapon("Bite","",0,0,15,2,6,0,this,2); 
		
		spells.add("Breath Acid",0,-1,0,0); //10d4 damage.
		
		//(String n,String k,double w,double v,int ab,int ds,int de,int db,Char o,int c)
		//name,level,uses,turns,save
		
		lvl=8; 
		damr=3;
		sr=17;
		
		hide=0; //dex
		spot=0; //wis
		pick=0; //dex
		}	
		
		if(n.equals("Red Dragon")) //juvenile
		{
		hp=Dice.roll(16,12,64);
		nata=14;
		str=29; //9
		dex=10; //0 
		con=19; //4
		inl=14; //2
		wis=15; //2
		cha=14; //2
		race="Dragon";
		fort=14; 
		ref=10;
		will=12;
		
		weapon=new Weapon("Bite","",0,0,15,2,6,0,this,2); 
		
		spells.add("Breath Fire",0,-1,0,0); //8d10 damage. (+burning?)
		
		//+24,8d10
		//(String n,String k,double w,double v,int ab,int ds,int de,int db,Char o,int c)
		//name,level,uses,turns,save
		
		lvl=9; 
		damr=0;
		sr=0;
		
		hide=0; //dex
		spot=0; //wis
		pick=0; //dex
		}
		
		
		//dragons,genie (and efreet?),giant (the first 2), more undead,kobold,elemental. enough?
		
		
		//some don't get treasure?
		
		//treasure. gp+items.
		
		int p=Dice.roll(100);
		if(p<=15)
		gp=0;
		if(p>=16&&gp<=29)
		gp=(double)Dice.roll((int)(lvl),6)/100;
		if(p>=30&&gp<=52)
		gp=(double)Dice.roll((int)(lvl),8)/50;
		if(p>=53&&gp<=95)
		gp=Dice.roll((int)(lvl),10)*10;
		if(p>=96&&gp<=100)
		gp=Dice.roll((int)(lvl),12)*50;
		
		if(p>=53)
		gp/=10;
		
		if(lvl<1&&p>15)
		gp=(double)Dice.roll(1,10)*lvl/100;
		
		
			for(int i=1;i<=lvl;i++)
			{
		p=Dice.roll(100);
		if(p>=30&&p<=50)
		items.add(new Item("Gem",(double)(1),(double)(Dice.roll(100))));
		if(p>=45&&p<=60)
		items.add(Item.getItem("Iron",Dice.roll(5)));
		if(p>=60&&p<=70)
		items.add(Item.getItem("Silver",Dice.roll(4)));
		
		
		p=Dice.roll(100);
		if(p>=20&&p<=50)
		items.add(Item.getItem("Copper",Dice.roll(10)));
		if(p>=40&&p<=50)
		items.add(Item.getItem("Gold",Dice.roll(2)));
			}
		
		
		p=Dice.roll(100);
		if(p<80)
		for(int i=0;i<lvl;i++)
		{
			p=Dice.roll(100);
			if(p>=80)
			i=(int)(lvl);
		}
		
		if(p>=80&&p<=90)
		{
			if(lvl<1)
			items.add(new Weapon("Dagger"));
			if(lvl==1)
			items.add(new Weapon("Club"));
			if(lvl>1&&lvl<=4)
			items.add(new Weapon("Mace"));
			if(lvl>4)
			items.add(new Weapon("Morningstar"));
		}
		if(p>=90&&p<=98)
		{
			if(lvl==1)
			items.add(new Weapon("Sling"));
			if(lvl>1&&lvl<=4)
			items.add(new Weapon("Light Crossbow"));
			if(lvl>4)
			items.add(new Weapon("Heavy Crossbow"));
		}
		
		
		
		p=Dice.roll(100);
		if(p<80)
		for(int i=0;i<lvl;i++)
		{
			p=Dice.roll(100);
			if(p>=80)
			i=(int)(lvl);
		}
				
		if(p>=80&&p<=84)
		items.add(new Shield("Buckler"));
		if(p>=85&&p<=90)
		items.add(new Shield("Wooden Shield"));
		if(p>=88&&p<=92)
		items.add(new Armor("Leather Armor"));
		if(p>=93&&p<=97)
		items.add(new Shield("Scale Mail"));


		//	public Item(String n,double l,double value,int a)
		
		
		
		if(weapon!=null)
		{
		//items.add(weapon);
		weapon.setOwner(this);
		}
		else
		weapon=Weapon.create("Unarmed Strike",this);
		
		if(shield!=null)
		if(weapon.isRanged()&&!shield.getName().equals("Buckler"))
		shield=null;
		
		if(shield!=null)
		{
		shield.setOwner(this);
		items.add(shield);
		}
		
		if(armor!=null)
		{
		armor.setOwner(this);
		items.add(armor);
		}
			
	}
	
	
	
	//functions for spot,pick and hide.
	
	
	public int hide()
	{
	int s=hide;
	s+=dex();
	
	if(getArmor()!=null)
	s+=getArmor().getPen();
	if(getShield()!=null)
	s+=getShield().getPen();
	
	int a=Item.howMany(getItems(),"Ring of Stealth");
	a+=Item.howMany(getItems(),"Ring of Darkness");
	if(a>5)
	a=5;
	s+=a;
		
	return s;	
	}
		
	public int getHiding()
	{
	if(hide>0)
	return Dice.roll(20)+hide();
	return Dice.roll(10)+Dice.roll(4); //+hide??
	}
	
	
	public int getSpot() //if torch+(fire making) - cave? etc
	{
	int s=spot;
	s+=wis();
	
	int a=Item.howMany(getItems(),"Ring of Vision");
	if(a>4)
	a=4;
	s+=a;
		
	return s;	
	}



	public boolean spot(Char c)
	{
	int d=Dice.roll(20)+getSpot(),dc=0;
		
		if(c instanceof Enemy)
		dc=((Enemy)(c)).getHiding();
		else
			if(c instanceof Character)
			dc=((Character)(c)).getHiding();
			
	if(dc==0)
	dc=Dice.roll(10)+Dice.roll(5);
	
	//if(c instanceof Character)
	//System.out.println("this: "+this+"    ,d: "+d+" dc: "+dc);
	
	if(d>=dc)
	return true;		
			
	return false;	
	}
		
	public double getLvl()
	{
	return lvl;	
	}
	
	
	public String write()
	{
		String s=super.write();	
		s="E"+s.substring(1,s.length()-3)+",";
		s+=lvl+",";
		s+=hide+",";
		s+=spot+",";
		s+=pick+",";
		
		return s+"*E";
	}
	
	
	public static Enemy readE(String s,Place plc,Vector seens) //careful of "C*C"
	{
	Enemy d=new Enemy(plc);
	//d.place=plc;
	s=s.substring(2);
	
	int b=s.indexOf(",");
	d.dam=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);	
	
	b=s.indexOf(",");
	d.name=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hp=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.str=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.dex=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.race=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	
	if((s.substring(0,b)).equals("true"))
	d.male=true;
	else
	d.male=false;
	
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.con=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.wis=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.inl=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.cha=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.damr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.nata=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.sr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.gp=Double.parseDouble(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*W"); //have to be.
	d.weapon=Weapon.readW(s.substring(0,b+1));
	s=s.substring(b+3);
	
	String wname=d.weapon.getName();
	//claw,bite,sting
	if(!wname.equals("Unarmed Strike")&&!wname.equals("Claw")&&!wname.equals("Bite")&&!wname.equals("Sting"))
	{
	Vector www=d.getWeapons(); //look for it.
	boolean bl=true;
	for(int i=0;i<www.size();i++)
	if(d.weapon.getName().equals(((Weapon)(www.elementAt(i))).getName()))
	bl=false;
	
	if(bl)
	d.items.add(d.weapon);
	}
	
	d.weapon.setOwner(d);
	
	String th=s.substring(0,s.indexOf(";"));
	s=s.substring(s.indexOf(";")+1);
	
	//System.out.println("th: "+th);
	b=th.lastIndexOf("*A");
	if(b!=-1)
	{
	d.armor=Armor.readA(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.armor);
	d.armor.setOwner(d);
	}
	
	b=th.lastIndexOf("*S");
	if(b!=-1)
	{
	d.shield=Shield.readS(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.shield);
	d.shield.setOwner(d);
	}
	
	
	
	b=s.indexOf(";");
	//System.out.println("BI: "+b+" , "+s);
	String i=s.substring(0,b);
	//items:
	
	//System.out.println("\nitems: "+i+"\n");
	
	while(i.indexOf("*")!=-1)
	{
	//System.out.println("\nentered.\n");
	
	Item t=null;	
	int c=0;
	
	if(i.indexOf("I")==0)
	{
		c=i.indexOf("*I");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*I")+2;	
		}
		
		t=Item.read(i.substring(0,c+1));
	}
	
	if(i.indexOf("W")==0)
	{	
		c=i.indexOf("*W");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*W")+2;	
		}
		t=Weapon.readW(i.substring(0,c+1));
		((Weapon)t).setOwner(d);
	}
	
	if(i.indexOf("A")==0)
	{
		c=i.indexOf("*A");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*A")+2;	
		}
		t=Armor.readA(i.substring(0,c+1));	
		((Armor)t).setOwner(d);
	}
	
	if(i.indexOf("S")==0)
	{
		c=i.indexOf("*S");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*S")+2;	
		}
		t=Shield.readS(i.substring(0,c+1));
		
		((Shield)t).setOwner(d);
	}
	
	
	d.items.add(t);
	i=i.substring(c+2);
	}
	//end items
	
	s=s.substring(b+1);
	
	
	b=s.indexOf(",");
	d.ref=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.will=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.fort=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*L");
	d.spells=Spells.read(s.substring(0,b));
	s=s.substring(b+3);
	d.spells.setOwner(d);
	
	b=s.indexOf(",");
	//d.attacked=Boolean.getBoolean(s.substring(0,b));
	if((s.substring(0,b)).equals("true"))
	d.attacked=true;
	else
	d.attacked=false;
	s=s.substring(b+1);
	
	
	//seen!
	b=s.indexOf(",");
	int size=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	Vector sen=new Vector();
	for(int j=0;j<size;j++)
	{
	b=s.indexOf(",");
	int index=Integer.parseInt(s.substring(0,b));
	sen.add(index+"");
	//Char c=d.bySerial(index);
	//d.seen.add(c);
	s=s.substring(b+1);
	}
	seens.add(sen);
	//end seen
	
	
	//Enemy:
	
	b=s.indexOf(",");
	d.lvl=Double.parseDouble(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hide=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.spot=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.pick=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	return d;
	}
	
	
}