﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FolderSyncronizer.Core.Data
{
    public interface IPubsub<T> : IDisposable
    {
        // TODO: Add an option to publish to a single subscriber?

        /// <summary>
        /// Publishes to all subsribers.
        /// </summary>
        /// <param name="item"></param>
        void Publish(string publisherId, T item);

        /// <summary>
        /// This is called for each item
        /// </summary>
        /// <param name="itemReceived"></param>
        void Subscribe(string subscriberId, Action<T> itemReceived);
    }

    public delegate void ItemReceived<T>(T item);
}
