import type { OutputMessage } from "./messages.ts";

export interface PinnedMessage {
  id: string;
  content: OutputMessage;
  timestamp: Date;
}

// Things here can throw NoPermissionsError.
export interface PinManager {
  fetchAllPinnedMessages(channelId: string): Promise<PinnedMessage[]>;
  deletePinnedMessage(channelId: string, messageId: string): Promise<void>;
  pinMessage(channelId: string, messageId: string): Promise<void>;
  updatePinnedMessage(
    channelId: string,
    messageId: string,
    message: OutputMessage,
  ): Promise<void>;
}
