"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NoPermissionsError = void 0;
class NoPermissionsError extends Error {
    constructor(...params) {
        super(...params);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, this.constructor);
        }
        this.name = this.constructor.name;
        const permissions = BigInt(0x1) |
            BigInt(0x400) |
            BigInt(0x800) |
            BigInt(0x10000) |
            BigInt(0x4000000) |
            BigInt(0x400) |
            BigInt(0x80000000) |
            BigInt(0x2000);
        let link = `https://discord.com/oauth2/authorize?client_id=947923931615088682&scope=identify%20bot%20applications.commands&permissions=` +
            permissions;
        console.log("No permission: " + link);
    }
}
exports.NoPermissionsError = NoPermissionsError;
//# sourceMappingURL=messages.js.map