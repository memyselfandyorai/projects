// authenticates you with the API standard library
 const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});
 const GUILD_TEXT_CHANNEL_TYPE = 0;
 
 let channels = context.params.event.channels
 
 let generalChannel = channels.find((channel) => {
   return channel.name.toLowerCase() === 'general' && channel.type == GUILD_TEXT_CHANNEL_TYPE;
 });
 
 if(!generalChannel) {
   generalChannel = channels.find((channel) => {
     return channel.type == GUILD_TEXT_CHANNEL_TYPE;
   });
 }
 
 let deployedMessageContact = [
   `Thanks for installing this Autocode-powered Discord bot.`,
   `Use the slash-commands to see which commands are available.`,
   `The most basic command is **/r**.`,
   `You can also use **/help** or read more on https://autocode.com/app/youreye/discord-cortex-prime#cortex-prime-roller-bot.`,
 ]
 
 if (generalChannel) {
   console.log('Sending deploy message');
   // let botInfo = await lib.discord.users['@0.0.6'].me.list();
   await lib.discord.channels['@0.0.6'].messages.create({
     channel_id: generalChannel.id,
     content: deployedMessageContact.join('\n')
   });
 } else {
   console.log('No text channel found');
 }
 
 
 // This would require permissions... Too much power.
 /*
 
 let guildId = context.params.event.id
 let botMember = context.params.event.members[0]
 let userId = botMember.user.id
 
 console.log(`Updating nickname for user ${userId } in guild ${guildId }`);
 
 await lib.discord.guilds['@0.1.0'].members.update({
   user_id: `${userId }`, 
   guild_id: `${guildId }`, 
   nick: `CortexPrime Roller Bot`
 });
 */