"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CacheLimitationError = void 0;
// Note: CacheLimitationError is obsolete, as we're no longer using Autocode's storage.
class CacheLimitationError extends Error {
    constructor(...params) {
        super(...params);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, this.constructor);
        }
        this.name = this.constructor.name;
    }
}
exports.CacheLimitationError = CacheLimitationError;
//# sourceMappingURL=storage.js.map