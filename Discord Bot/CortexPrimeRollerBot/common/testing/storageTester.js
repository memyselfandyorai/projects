const storage = require('../storage.js');
const {TestCase, createIocContainer, }= require('./common.js');


module.exports = {
  TestCases: createTestCases(),
};

function createTestCases() {
return [
  new TestCase({
    name: 'testSingleStoreValue_3keys_getStoreGetGetAllClearGet ',
    functionToRun: testSingleStoreValue,
    inputArgs: {},
    expectedResult: [
      'defaultValue',
      'value',
      [{keyList:  ['type', 'id', 'name'], value: 'value'}],
      [[]],
      'defaultValue',
    ],
  }),
  new TestCase({
    name: 'testHierarchyStoreValue_3keysWithParents_store3GetAllGetParentGetChildGetOtherClearParent ',
    functionToRun: testHierarchyStoreValue,
    inputArgs: {},
   expectedResult: [
      [
        {keyList:  ['type', 'id', 'name'], value: 'value'},
        {keyList:  ['type', 'id', 'name2'], value: 'value2'},        
        {keyList:  ['type2', 'id', 'name'], value: 'other'},
        {keyList:  ['type'], value: 'parent'},
      ],
      [
        {keyListSuffix:  ['id', 'name'], value: 'value'},
        {keyListSuffix:  ['id', 'name2'], value: 'value2'},
        {keyListSuffix:  [], value: 'parent'},
      ],
      [
        {keyListSuffix:  [], value: 'value'},
      ],
      'other',
      [['id', 'name'], ['id', 'name2'], []], 
       [
        {keyList:  ['type2', 'id', 'name'], value: 'other'},
      ],
    ],
  }),
  new TestCase({
    name: 'testOtherCacheValuesNotAffecting',
    functionToRun: testOtherCacheValuesNotAffecting,
    inputArgs: {},
    expectedResult: [
      [
        {keyList:  ['type', 'id', 'name'], value: 'value'},
      ],
      [
        {keyListSuffix:  ['id', 'name'], value: 'value'},
      ],
      [
        {keyListSuffix:  [], value: 'value'},
      ],
      [['id', 'name']], 
      [],
      [
        {key:  'type.id.name', value: 'value2'},
        {key:  'type', value: 'value3'},
        {key:  ['type'], value: 'value4'},
      ],
    ],
  }),
  
  new TestCase({
    name: 'testErrors',
    functionToRun: testErrors,
    inputArgs: {},
    expectedError: {
      message: `Test error for method 'getCacheEntries' and index 1 -- expected`,
      type: Error.prototype,
    },  
  }),
  
  
  
  ]
}

 /* async storeValue: func(keyList, value) -> void,
async getStoredValues: func(keyList) -> list of {keyListSuffix, value},
async getStoredValue: func(keyList, defaultValue) -> value,
async getAllStoredEntries: () -> list of {keyList, value},     
async clearKeys: func(keyList) -> void,*/


async function testSingleStoreValue(input) {
  let iocContainer = createIocContainer([])
  let key1 = ['type', 'id', 'name']
  
  let results = []  
  results.push(await iocContainer.storage.getStoredValue(key1, 'defaultValue'))
  
  await iocContainer.storage.storeValue(key1, 'value')
  
  results.push(await iocContainer.storage.getStoredValue(key1, 'defaultValue'))
  
  results.push(await iocContainer.storage.getAllStoredEntries())
  
  results.push(await iocContainer.storage.clearKeys(key1))
  
  results.push(await iocContainer.storage.getStoredValue(key1, 'defaultValue'))
  
  return results
  
 
}

async function testHierarchyStoreValue(input) {
  let iocContainer = createIocContainer([])
  let key1 = ['type', 'id', 'name']
  let key2 = ['type', 'id', 'name2']
  let otherKey = ['type2', 'id', 'name']
  let parentKey = ['type']
  
  let results = []  
  await iocContainer.storage.storeValue(key1, 'value')
  await iocContainer.storage.storeValue(key2, 'value2')
  await iocContainer.storage.storeValue(otherKey, 'other')
  await iocContainer.storage.storeValue(parentKey, 'parent') // This might not work with other implementations
  
  results.push(await iocContainer.storage.getAllStoredEntries())
  results.push(await iocContainer.storage.getStoredValues(parentKey))
  results.push(await iocContainer.storage.getStoredValues(key1))
  results.push(await iocContainer.storage.getStoredValue(otherKey, 'defaultValue'))
  
  results.push(await iocContainer.storage.clearKeys(parentKey))
  
  results.push(await iocContainer.storage.getAllStoredEntries())
  
  return results
}


async function testOtherCacheValuesNotAffecting(input) {
  let iocContainer = createIocContainer([])
  let key1 = ['type', 'id', 'name']
  let parentKey = ['type']
  
  let results = []  
  await iocContainer.storage.storeValue(key1, 'value')
  await iocContainer.cache.cacheValue(key1.join('.'), 'value2')
  await iocContainer.cache.cacheValue(parentKey.join('.'), 'value3')
  await iocContainer.cache.cacheValue(parentKey, 'value4')
  
  results.push(await iocContainer.storage.getAllStoredEntries())
  results.push(await iocContainer.storage.getStoredValues(parentKey))
  results.push(await iocContainer.storage.getStoredValues(key1))
  
  results.push(await iocContainer.storage.clearKeys(parentKey))
  
  results.push(await iocContainer.storage.getAllStoredEntries())
  results.push(await iocContainer.cache.getCacheEntries())
  
  return results
}

async function testErrors(input) {
  let options = {
    failMap: {
      'getCacheEntries': new Set([1]),
    }
  }
  let iocContainer = createIocContainer([], options )
  let key1 = ['type', 'id', 'name']
  let parentKey = ['type']
  
  let results = []  
  await iocContainer.storage.storeValue(key1, 'value')
  results.push(await iocContainer.storage.getStoredValues(parentKey))
  let expectedError = null
  try {
    results.push(await iocContainer.storage.getStoredValues(key1))
  } catch(e) {
    expectedError = e
  }
  results.push(await iocContainer.storage.clearKeys(parentKey))
  
  if(expectedError?.message) {
    throw new Error(`${expectedError.message} -- expected`)
  } else {
    throw new Error(`no error. Results:\n` + results.join('\n'))
  }
  
  return results
}
