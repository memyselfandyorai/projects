const controller = require('../controller.js');
const {compareObjects, throwIfNotEqual }= require('./common.js');
const saver = require('../saver.js');
const {POOL_ADD } = require('../pools.js');    


const GUILD_ID = '123'
const USER_ID = 5
const SAVE_NAME = 'battle_strategy'
const SINGLE_SAVE_NAME = 'grip'
const OPTED_OUT_SAVE_NAME = 'out'
const OTHER_SAVE_NAME = 'none'
const OTHER_GUILD_ID = '111'
const OTHER_USER_ID = 1

const POOL_NAME = 'doom'
const SECOND_POOL_NAME = 'doom2'
const NON_POOL_NAME = 'missing'

const COUNTER_WHO = 'me'
const SECOND_COUNTER_WHO = 'him'
const COUNTER_NAME = 'pp'
const SECOND_COUNTER_NAME = 'xp'
const OTHER_COUNTER_WHO = 'menot'
const OTHER_COUNTER_NAME = 'notpp'

const CONSTANT_ROLL_CACHE_KEY = 'user,channel'
const CONSTANT_ROLL_RESULTS = [4, 1, 2, 3, 5]
const CONSTANT_MERGE_ROLL_RESULTS = [1, 1, 8, 2, 3]
const CONSTANT_SAVER_ROLL_RESULTS = [4, 1, 2, 3, 5, 3, 3, 3]
const CONSTANT_NON_POOL_ROLL_RESULTS = [1, 3, 1, 4]
const CONSTANT_REROLL_STEPUP_ROLL_RESULTS = [1, 10, 1, 1, 6, 4, 1]
const CONSTANT_POOL_ROLL_RESULTS = [3, 2, 1, 1, 6]
const CONSTANT_POOL_ROLL_ADVANCED_RESULTS = [2, 3, 5, 5, 4, 6]


const PREPARE_ACTION_GET_FOR_USER = 1 // gets data for user
const PREPARE_ACTION_DELETE_ALL = 2 // deletes all
const PREPARE_ACTION_DELETE_SCENE = 3 // deletes SAVE_NAME 
const PREPARE_ACTION_DELETE_SINGLE = 4 // deletes SINGLE_SAVE_NAME 
const PREPARE_ACTION_DELETE_OTHER = 5 // deletes OTHER_SAVE_NAME 
const PREPARE_ACTION_WILDCARD_OPTED_OUT = 6 // creates another OPTED_OUT_SAVE_NAME D10 that is opted out.


// Reminder: adding something new? add to exports!
const CONSTANT_ROLL_DEFAULT = 1 // prepareKeepEffectModHitchConstantRoll
const CONSTANT_ROLL_ADVANCED = 2 // prepareExtraEffectDiceAndMergeConstantRoll
const CONSTANT_ROLL_SAVE = 3 // prepareConstantRollWithSave
const CONSTANT_ROLL_NON_POOL_SAVE = 4 // prepareNonPoolDiceConstantRoll
const CONSTANT_ROLL_REROLL_STEPUP = 5 // prepareStepUpRerollConstantRoll
const CONSTANT_POOL_ROLL = 6 // prepareConstantPoolRoll
const CONSTANT_POOL_ROLL_ADVANCED = 7 // prepareConstantPoolRollWithSave

/*
type: one of CONSTANT_ROLL_.* .

void
*/
async function prepareConstantRoll(type, iocContainer) {
  
  let preparationInput = {
    cache_key: CONSTANT_ROLL_CACHE_KEY,
  }
  
   switch(type ?? CONSTANT_ROLL_DEFAULT) {
      case CONSTANT_ROLL_DEFAULT : 
        await prepareKeepEffectModHitchConstantRoll(preparationInput, iocContainer)
        break  
      case CONSTANT_ROLL_ADVANCED : 
        await prepareExtraEffectDiceAndMergeConstantRoll(preparationInput, iocContainer)
        break  
      case CONSTANT_ROLL_SAVE : 
        await prepareConstantRollWithSave(preparationInput, iocContainer)
        break  
      case CONSTANT_ROLL_NON_POOL_SAVE:
        await prepareNonPoolDiceConstantRoll(preparationInput, iocContainer)
        break 
      case CONSTANT_ROLL_REROLL_STEPUP :
        await prepareStepUpRerollConstantRoll(preparationInput, iocContainer)
        break 
      case CONSTANT_POOL_ROLL :
        await prepareConstantPoolRoll(preparationInput, iocContainer)
        break          
      case CONSTANT_POOL_ROLL_ADVANCED :
        await prepareConstantPoolRollWithSave(preparationInput, iocContainer)
        break            
      default:
        throw new Error(`Unimplemented constant roll type: ${type}`);
  }
}

/*
returns a list of additional required mock rolls.
*/
function getRequiredRollResults(type) {
   switch(type ?? CONSTANT_ROLL_DEFAULT) {
      case CONSTANT_ROLL_DEFAULT : 
        return CONSTANT_ROLL_RESULTS
      case CONSTANT_ROLL_ADVANCED : 
        return CONSTANT_MERGE_ROLL_RESULTS 
      case CONSTANT_ROLL_SAVE : 
        return CONSTANT_SAVER_ROLL_RESULTS 
      case CONSTANT_ROLL_NON_POOL_SAVE:
       return CONSTANT_NON_POOL_ROLL_RESULTS         
      case CONSTANT_ROLL_REROLL_STEPUP :
       return CONSTANT_REROLL_STEPUP_ROLL_RESULTS 
      case CONSTANT_POOL_ROLL:
        return CONSTANT_POOL_ROLL_RESULTS
      case CONSTANT_POOL_ROLL_ADVANCED :
        return CONSTANT_POOL_ROLL_ADVANCED_RESULTS  
      default:
        throw new Error(`Unimplemented constant roll type: ${type}`);
  }
  return []
}


/*
input: {
  cache_key,  
}

Uses: dice, keep, effect modifier, die modifier, hitch, and save-input.
*/
async function prepareKeepEffectModHitchConstantRoll(input, iocContainer) {
  const rollInput = {
    dice: '4 6 2D8 12',
    keep: 1,
    effect_mod: 1,
    die_mod: -1,
    hitch: 2,
    cache_key: input.cache_key,
    save_input: {
      userId:  USER_ID, 
      guildId: GUILD_ID,
    },
  } 
  
  let expectedResult = [
    'Modified roll die: D12 -> D10',
    'Rolling: 1D4 1D6 2D8 1D10',
    'D4 : 4',
    'D6 : **(1)**',
    'D8 : **(2)** 3',
    'D10 : 5',
    'Results ordered by best total:',
    'Score: 5 (5) with Increased-Effect: D10',
    'Score: 4 (4) with Increased-Effect: D12',
  ]
  
  let result = await controller.RollCortexPrime(rollInput, iocContainer)
  throwIfNotEqual(result, expectedResult, 'roll')    
}

/*
input: {
  cache_key,  
}
Uses: dice, extra effect dice and merge
*/
async function prepareExtraEffectDiceAndMergeConstantRoll(input, iocContainer) {
  const rollInput = {
    dice: '6 6 6 8',
    extra_effect_dice: '6 12',
    merge: '6',
    cache_key: input.cache_key,
    save_input: {
      userId:  USER_ID, 
      guildId: GUILD_ID,
    },
  } 
  
  let expectedResult = [
    'Removed one D6',
    'Modified roll die: D6 -> D8',
    'Rolling: 2D6 2D8 1D12',    
    'D6 : **(1)** 2',
    'D8 : **(1)** 8',
    'D12 : 3',
    'Results ordered by best total:',
    'Score: 11 (8+3) with Effects: D6,D4,D4',
    'Score: 10 (2+8) with Effects: D12,D4,D4',
  ]
  
  let result = await controller.RollCortexPrime(rollInput, iocContainer)
  throwIfNotEqual(result, expectedResult, 'roll')    
}

/*
input: {
  cache_key,  
}

Uses: dice, keep, effect modifier, die modifier, hitch, and save-input.
*/
async function prepareConstantRollWithSave(input, iocContainer) {
  const rollInput = {
    dice: '4 6 2D8 12',
    keep: 1,
    effect_mod: 1,
    die_mod: -1,
    hitch: 2,
    cache_key: input.cache_key,
    save_input: {
      userId:  USER_ID, 
      guildId: GUILD_ID,
    },
  } 
    
  await prepareConstantSaveDices({}, iocContainer)    
    
  let expectedResult = [
    `Bonus dice due to '${SAVE_NAME}': 1D6`,
    `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,    
    'Modified roll die: D12 -> D10',
    'Rolling: 1D4 2D6 4D8 1D10',
    'D4 : 4',
    'D6 : **(1)** 3',
    'D8 : **(2)** 3 3 3',
    'D10 : 5',
    'Results ordered by best total:',
    'Score: 5 (5) with Increased-Effect: D10',
    'Score: 4 (4) with Increased-Effect: D12',
  ]
  
  let result = await controller.RollCortexPrime(rollInput, iocContainer)
  throwIfNotEqual(result, expectedResult, 'rollAfterSave')    
}

/*
input: {
  cache_key,  
}

Uses: dice, die modifier, merge.
*/
async function prepareNonPoolDiceConstantRoll(input, iocContainer) {  
  const rollInput = {
    dice: '1D10 2D12',
    merge: '12',
    die_mod: 2,
    cache_key: input.cache_key,
    save_input: {
      userId:  USER_ID, 
      guildId: GUILD_ID,
    },
  } 
    
  let expectedResult = [
    'Removed one D12',
    `Modified roll die: D12 -> D12+D4`,
    `Modified roll die: D10 -> D12`,    
    `Modified roll die: D12 -> D12+D4`,    
    'Rolling: 2D12',
    'Rolling (dice pool unusable for hitch or effect): 2D4',
    'D4 : [1] [4]',    
    'D12 : **(1)** 3',
    'Results ordered by best total:',
    'Score: 7 (4+3) with Effect: D4',
    'Score: 5 (1+4) with Effect: D12',
  ]
  
  let result = await controller.RollCortexPrime(rollInput, iocContainer)
  throwIfNotEqual(result, expectedResult, 'rollNonPool')    
}

/*
input: {
  cache_key,  
  defaultly sets extraRollDieIncreaseModeOn to true.
}

Uses: dice, merge, reroll stepuo
*/
async function prepareStepUpRerollConstantRoll(input, iocContainer) {  
  iocContainer.options.extraRollDieIncreaseModeOn = true

  const rollInput = {
    dice: '1D10 2D12',
    merge: '12',
    cache_key: input.cache_key,
    save_input: {
      userId:  USER_ID, 
      guildId: GUILD_ID,
    },
  } 
    
  let expectedResult = [
    'Removed one D12',
    `Modified roll die: D12 -> D12+D4`,
    'Rolling: 1D10 1D12',
    'Rolling (dice pool unusable for hitch or effect): 1D4',
    'D4 : [1]',    
    'D10 : **(1)**',
    'D12 : 10',
    'Results ordered by best total:',
    'Score: 11 (1+10) with Effect: D4',
  ] 
  
  let result = await controller.RollCortexPrime(rollInput, iocContainer)
  throwIfNotEqual(result, expectedResult, 'rollMerge')    
  
  let rerollInput = {
    step_up: 10,
    step_up_mod: 3,
    cache_key: input.cache_key,
  }
  
  let rerollExpectedResult = [
    `Modified roll die: D10 -> D12+D6`,
    'Rolling: 2D12',
    'Rolling (dice pool unusable for hitch or effect): 1D4 1D6',
    'D4 : [4]',    
    'D6 : [1]',
    'D12 : **(1)** 6',
    'Results ordered by best total:',
    'Score: 10 (4+6) with Effect: D4',
    'Score: 5 (4+1) with Effect: D12',
  ]  
  
  let messageContentResult = await controller.RerollCortexPrime(rerollInput, iocContainer)
  throwIfNotEqual(messageContentResult, rerollExpectedResult, 'rerollNonPool')    
  
}


/*
input: {
  cache_key,  
  constant_pools_input,
}

Uses: dice, keep, CONSTANT_POOL_ROLL_RESULTS 
*/
async function prepareConstantPoolRoll(input, iocContainer) {
  
  /*TBD:
  saveOptOut: input.save_opt_out ?? '',
  saveOptIn: input.save_opt_in ?? '',
  extraEffects: input.extra_effects ?? 0,
  */
  
  
  await prepareConstantPools(input.constant_pools_input, iocContainer)
  
  const rollInput = {
    names: POOL_NAME,
    dice: '12',
    keep: 1,    
    channelId: GUILD_ID ,
    userId: USER_ID,
    cacheKey: input.cache_key ?? CONSTANT_ROLL_CACHE_KEY,    
  }   
    
  let expectedResult = [
    `Dice pool '${POOL_NAME}': 1D6 1D8 2D10`,
    'Rolling: 1D6 1D8 2D10 1D12',
    'D6 : 3',
    'D8 : 2',
    'D10 : **(1)** **(1)**',
    'D12 : 6',
    'Results ordered by best total:',
    'Score: 6 (6) with Effect: D8',
    'Score: 3 (3) with Effect: D12',
  ]
  
  
  let result = await controller.RollDicePool(rollInput, iocContainer)
  throwIfNotEqual(result, expectedResult, 'poolRoll')    
}

/*
input: {
  prepareConstantPoolRollWithSave_pools_input,
}

Uses: saveOptOut, saveOptIn, extraEffects, CONSTANT_POOL_ROLL_ADVANCED_RESULTS 
*/
async function prepareConstantPoolRollWithSave(input, iocContainer) {
  await prepareConstantPools(input.constant_pools_input, iocContainer)
  await prepareConstantSaveDices({}, iocContainer) 
  
  const rollInput = {
    names: POOL_NAME,
    saveOptOut: SAVE_NAME,
    saveOptIn: controller.SAVE_NAME_ALL_WILCARD,
    extraEffects: 2,
    channelId: GUILD_ID ,
    userId: USER_ID,
    cacheKey: input.cache_key ?? CONSTANT_ROLL_CACHE_KEY,    
  }       
    
  let expectedResult = [
    `Dice pool '${POOL_NAME}': 1D6 1D8 2D10`,
    `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,   
    'Rolling: 1D6 3D8 2D10',
    'D6 : 2',
    'D8 : 3 4 6',
    'D10 : 5 5',
    'Results ordered by best total:',
    'Score: 11 (6+5) with Effects: D10,D8,D8',
    'Score: 10 (4+6) with Effects: D10,D10,D8',
    ]
    
  let result= await controller.RollDicePool(rollInput, iocContainer)
  throwIfNotEqual(result, expectedResult, 'advancedPoolRoll')    
}

/*
input: {}
Uses: POOL_ADD.
*/
async function prepareConstantPools(input, iocContainer) {  
  let inputOptions = {
    name: POOL_NAME,
    dice: '6 8 2d10',
    action: POOL_ADD,
    channelId: GUILD_ID ,
    userId: USER_ID,
  }
    
  let expectedResult = [
    `Created new pool '${POOL_NAME}': 1D6 1D8 2D10.`
  ]
  
  let result= await controller.ModifyDicePool(inputOptions, iocContainer)
  throwIfNotEqual(result, expectedResult, 'pool')    
  
  let inputOptions2 = {
    name: SECOND_POOL_NAME + "   ", // with space, tests "trim"
    dice: '12',
    action: POOL_ADD,
    channelId: GUILD_ID ,
    userId: USER_ID,
  }
    
  let expectedResult2 = [
    `Created new pool '${SECOND_POOL_NAME}': 1D12.`
  ]
  
  let result2 = await controller.ModifyDicePool(inputOptions2, iocContainer)
  throwIfNotEqual(result2, expectedResult2, 'pool2')    
}

/*
  input: {}

  output: list of messages
*/
async function prepareConstantCounters(input, iocContainer) {
  let channelId = input.channel_id ?? GUILD_ID
  let userId = input.user_id ?? USER_ID
  
  let inputOptions = {
    who: COUNTER_WHO,
    counterName: COUNTER_NAME,
    value: 2,  
    channelId: GUILD_ID,
    userId: USER_ID,
  }
  let expectedResult = [
    `Set counter *${COUNTER_NAME}* to **2** for *${COUNTER_WHO}*.`,
  ]
  let result = await controller.SetCounter(inputOptions, iocContainer)
  throwIfNotEqual(result, expectedResult, 'counter1')    
  
  let inputOptions2 = {
    who: SECOND_COUNTER_WHO,
    counterName: COUNTER_NAME  + "   ", // with space, tests "trim"
    value: 0,  
    channelId: GUILD_ID,
    userId: USER_ID,
  }
  let expectedResult2 = [
    `Set counter *${COUNTER_NAME}* to **0** for *${SECOND_COUNTER_WHO}*.`,
  ]
  let result2 = await controller.SetCounter(inputOptions2, iocContainer)
  throwIfNotEqual(result2, expectedResult2, 'counter2')    
  
  let inputOptions3 = {
    who: COUNTER_WHO,
    counterName: SECOND_COUNTER_NAME,
    value: -1,  
    channelId: GUILD_ID,
    userId: USER_ID,
  }
  let expectedResult3 = [
    `Set counter *${SECOND_COUNTER_NAME}* to **-1** for *${COUNTER_WHO}*.`,
  ]
  let result3 = await controller.SetCounter(inputOptions3, iocContainer)
  throwIfNotEqual(result3, expectedResult3, 'counter3')    
}


/*
  input: actions - list of prepare actions

  output: list of messages
*/
async function prepareConstantSaveDices(input, iocContainer) {
    const saverInput = {
      dice: '6',
      action: saver.SCENE,
      name: SAVE_NAME,
      guild_id: GUILD_ID,
      user_id: USER_ID,
      // wildcard_mode: default.
    }        
    
    let expectedResult = [
      `Saved '${SAVE_NAME}' successfully as '1D6'.`
    ]
    
    let result = await controller.SaveDice(saverInput, iocContainer)
    throwIfNotEqual(result, expectedResult, 'save') 
    
    const singleSaverInput = {
      dice: '2d8',
      action: saver.SINGLE,
      name: SINGLE_SAVE_NAME + "   ", // with space, tests "trim"
      guild_id: GUILD_ID,
      user_id: USER_ID,
      wildcard_mode: saver.WILDCARD_MODE_OPT_IN,
    }        
    expectedResult = [
      `Saved '${SINGLE_SAVE_NAME}' successfully as '2D8'.`
    ]
    result = await controller.SaveDice(singleSaverInput, iocContainer)
    throwIfNotEqual(result, expectedResult, 'save') 
        
    input.actions = input.actions ?? []
    
    console.log(`Preparing actions: ${input.actions.join(',')}`)
    
    for(action of input.actions){
      switch(action) {
        case PREPARE_ACTION_GET_FOR_USER: 
          let getInput = {
            userId: USER_ID,
            guildId: GUILD_ID,
            saveOptOuts: [],
            saveOptIns: saver.ALL_NAMES,
          }
          
          let expectedGet = [
            `Bonus dice due to '${SAVE_NAME}': 1D6`,
            `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
          ]
          let getResult = await saver.GetSaveForUser(getInput, iocContainer)
          throwIfNotEqual(getResult.additionalPool, [6, 8, 8], 'get') 
          throwIfNotEqual(getResult.messageContent , expectedGet , 'get') 
        
        break  
      case PREPARE_ACTION_DELETE_ALL:
        let deleteInput = {
          guildId: GUILD_ID,
          saveNames: saver.ALL_NAMES,
        }
        
        let expectedDelete = [
          `Deleted saved result for all.`
          ]
        
        let deleteResult = await saver.DeleteSave(deleteInput, iocContainer)
        throwIfNotEqual(deleteResult , expectedDelete, 'delete all') 
        
        break
      case PREPARE_ACTION_DELETE_SCENE:
      
        let deleteInput2 = {
          guildId: GUILD_ID,
          saveNames: [SAVE_NAME],
        }

        let expectedDelete2 = [
          `Deleted saved result for '${SAVE_NAME}'.`
          ]

        let deleteResult2 = await saver.DeleteSave(deleteInput2, iocContainer)
        throwIfNotEqual(deleteResult2 , expectedDelete2, 'delete scene') 
      
        break
      case PREPARE_ACTION_DELETE_SINGLE:
        let deleteInput3 = {
          guildId: GUILD_ID,
          saveNames: [SINGLE_SAVE_NAME],
        }

        let expectedDelete3 = [
          `Deleted saved result for '${SINGLE_SAVE_NAME}'.`
          ]

        let deleteResult3 = await saver.DeleteSave(deleteInput3, iocContainer)
        throwIfNotEqual(deleteResult3 , expectedDelete3, 'delete single') 

        break
        
      case PREPARE_ACTION_DELETE_OTHER :
        let deleteInput4 = {
          guildId: GUILD_ID,
          saveNames: [OTHER_SAVE_NAME],
        }

        let expectedDelete4 = [
          `Deleted saved result for '${OTHER_SAVE_NAME}'.`
        ]         
        
        let deleteResult4 = await saver.DeleteSave(deleteInput4, iocContainer)
        throwIfNotEqual(deleteResult4 , expectedDelete4, 'delete other') 
      
        break
      case PREPARE_ACTION_WILDCARD_OPTED_OUT :
        const optedOutSceneSaverInput = {
          dice: '10',
          action: saver.SCENE,
          name: OPTED_OUT_SAVE_NAME,
          guild_id: GUILD_ID,
          user_id: USER_ID,
          wildcard_mode: saver.WILDCARD_MODE_OPT_OUT,
        }        
        let expectedResultOut = [
          `Saved '${OPTED_OUT_SAVE_NAME}' successfully as '1D10'.`
        ]
        let resultOut = await controller.SaveDice(optedOutSceneSaverInput , iocContainer)
        throwIfNotEqual(resultOut, expectedResultOut, 'opted-out-save') 
        break
      
      }        
    }
  }
    

module.exports = {
  prepareConstantRoll: prepareConstantRoll,  
  getRequiredRollResults: getRequiredRollResults,  
  prepareConstantSaveDices: prepareConstantSaveDices,  
  prepareConstantPools: prepareConstantPools,
  prepareConstantCounters: prepareConstantCounters,
  CONSTANT_ROLL_CACHE_KEY : CONSTANT_ROLL_CACHE_KEY,
  CONSTANT_ROLL_DEFAULT: CONSTANT_ROLL_DEFAULT ,
  CONSTANT_ROLL_ADVANCED : CONSTANT_ROLL_ADVANCED ,
  CONSTANT_ROLL_SAVE : CONSTANT_ROLL_SAVE ,  
  CONSTANT_ROLL_NON_POOL_SAVE : CONSTANT_ROLL_NON_POOL_SAVE ,
  CONSTANT_ROLL_REROLL_STEPUP: CONSTANT_ROLL_REROLL_STEPUP ,  
  CONSTANT_POOL_ROLL: CONSTANT_POOL_ROLL ,
  CONSTANT_POOL_ROLL_ADVANCED: CONSTANT_POOL_ROLL_ADVANCED ,
  
  GUILD_ID: GUILD_ID,
  USER_ID: USER_ID,
  SAVE_NAME: SAVE_NAME,  
  SINGLE_SAVE_NAME: SINGLE_SAVE_NAME,
  OPTED_OUT_SAVE_NAME: OPTED_OUT_SAVE_NAME,
  OTHER_SAVE_NAME: OTHER_SAVE_NAME,
  OTHER_GUILD_ID: OTHER_GUILD_ID,
  OTHER_USER_ID: OTHER_USER_ID,
  PREPARE_ACTION_GET_FOR_USER: PREPARE_ACTION_GET_FOR_USER,
  PREPARE_ACTION_DELETE_SCENE: PREPARE_ACTION_DELETE_SCENE,
  PREPARE_ACTION_DELETE_ALL: PREPARE_ACTION_DELETE_ALL ,
  PREPARE_ACTION_DELETE_SINGLE : PREPARE_ACTION_DELETE_SINGLE ,
  PREPARE_ACTION_DELETE_OTHER : PREPARE_ACTION_DELETE_OTHER ,
  PREPARE_ACTION_WILDCARD_OPTED_OUT: PREPARE_ACTION_WILDCARD_OPTED_OUT ,

  POOL_NAME: POOL_NAME,
  SECOND_POOL_NAME: SECOND_POOL_NAME,
  NON_POOL_NAME: NON_POOL_NAME,
  
  COUNTER_WHO: COUNTER_WHO,
  COUNTER_NAME: COUNTER_NAME, 
  SECOND_COUNTER_WHO: SECOND_COUNTER_WHO,
  SECOND_COUNTER_NAME: SECOND_COUNTER_NAME, 
  OTHER_COUNTER_WHO: OTHER_COUNTER_WHO,
  OTHER_COUNTER_NAME: OTHER_COUNTER_NAME,
  
};