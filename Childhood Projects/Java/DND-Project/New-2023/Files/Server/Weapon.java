import java.io.*;

public class Weapon extends Item implements Serializable{
	
	
	private Dice damage;
	private Char owner;
	private String kind; //if ""-melee, else kind is the type of ammunition
	
	private int ab; //attack bonus
	private int db; //damage bonus
	
	private int crit; //critical: 1=19\20 X2,2=20 X2,3=20 X3
	
	
	
	public Weapon(String n,String k,double w,double v,int ds,int de,int c)
	{
	super(n,w,v);
	damage=new Dice(ds,de);
	kind=k;
	crit=c;
	
	ab=0;
	db=0;
	}
	
	public Weapon(String n,String k,double w,double v,int ds,int de,int c,Char o)
	{
	super(n,w,v);
	damage=new Dice(ds,de);
	owner=o;
	kind=k;
	crit=c;
	
	ab=0;
	db=0;
	}
	
	
	
	public Weapon(String n,String k,double w,double v,int ab,int ds,int de,int db,Char o,int c)
	{
	super(n,w,v);
	damage=new Dice(ds,de);
	owner=o;
	kind=k;
	crit=c;
	
	this.ab=ab;
	this.db=db;
	}

	
	public Weapon(String n) //"bow"\"great"- can't use shield
	{
	super(n);
	ab=0;
	db=0;
	//my value
		if(n.equals("Longsword"))
		{
		value=15;
		lbs=4;
		damage=new Dice(1,8);
		kind="";
		crit=1;//19,20
		}	
		
		if(n.equals("Short Sword"))
		{
		value=10;
		lbs=3;
		damage=new Dice(1,6);
		kind="";
		crit=1;//19,20
		}	
		
		if(n.equals("Short Bow")) //bows: no AoO? (can sneak attack\get a free attack)
		{
		value=20;
		lbs=2;
		damage=new Dice(1,6);
		kind="Arrows"; //lbs:3 for 20.
		crit=3;//*3
		}	
			
		if(n.equals("Longbow"))
		{
		value=40;
		lbs=2;
		damage=new Dice(1,8);
		kind="Arrows"; //lbs:3 for 20.
		crit=3;//*3
		}	
		
		if(n.equals("Light Crossbow"))
		{
		value=30;
		lbs=6;
		damage=new Dice(1,8);
		kind="Bolts"; //lbs:1 for 10.
		crit=1;
		}	
						
		if(n.equals("Heavy Crossbow"))
		{
		value=40;
		lbs=9;
		damage=new Dice(1,10);
		kind="Bolts"; //lbs:1 for 10.
		crit=1;
		}	
		
		
		if(n.equals("Dagger"))
		{
		value=2;
		lbs=1;
		damage=new Dice(1,4);
		kind=""; 
		crit=1;
		}	
		
		if(n.equals("Mace"))
		{
		value=12;
		lbs=12;
		damage=new Dice(1,8);
		kind=""; 
		crit=2; //*2
		}	
		
			
		if(n.equals("Club"))
		{
		value=5;
		lbs=3;
		damage=new Dice(1,6);
		kind=""; 
		crit=2;
		}	
		
					
		if(n.equals("Morningstar"))
		{
		value=16;
		lbs=8;
		damage=new Dice(1,10);
		kind=""; 
		crit=2;
		}	
		
		if(n.equals("Handaxe"))
		{
		value=6;
		lbs=5;
		damage=new Dice(1,6);
		kind=""; 
		crit=3;
		}	
		
		
		if(n.equals("Battleaxe"))
		{
		value=10;
		lbs=7;
		damage=new Dice(1,8);
		kind=""; 
		crit=3;
		}	
		
		if(n.equals("Greatsword"))
		{
		value=45; //not like
		lbs=15;
		damage=new Dice(2,6);
		kind=""; 
		crit=1;
		}	
			
		if(n.equals("Greataxe"))
		{
		value=35;
		lbs=20;
		damage=new Dice(1,12);
		kind="";
		crit=3;
		}		
		
		if(n.equals("Sling"))
		{
		value=0.5;
		lbs=0.1;
		damage=new Dice(1,4);
		kind="Bullets"; //lbs:1 for 10.
		crit=2;
		}	
		
		if(n.equals("Bastard Sword"))
		{
		value=25;
		lbs=10;
		damage=new Dice(1,10);
		kind="";
		crit=1;
		}	
		
						
		if(n.equals("Katana"))
		{
		value=60;
		lbs=6;
		damage=new Dice(1,10);
		kind="";
		crit=1;
		ab=1;
		}		
	
		
	}
	
	
	
	public static Weapon create(String n,Char o)
	{
	Weapon w=new Weapon(n);
		
	if(n.equals("Unarmed Strike"))
	{
	w.value=0;
	w.lbs=0;
	int b=0;
	if(o.race().equals("Dwarf")||o.race().equals("Halfling"))
	{
	w.damage=new Dice(1,2);
	b=0;
	}
	else
	{
	w.damage=new Dice(1,3);
	b=-2;
	}		
	
	if(o instanceof Character)
	{
	Character c=(Character)o;	
		if(c.getClas().equals("Monk"))
		{
			int l=c.lvl();
			if(l>=1&&l<=3)
			w.damage=new Dice(1,6+b);
			if(l>=4&&l<=7)
			w.damage=new Dice(1,8+b);
			if(l>=8&&l<=11)
			w.damage=new Dice(1,10+b);
			if(l>=12&&l<=15)
			w.damage=new Dice(1,12+b);
			if(l>=16)
				if(b==0)
				w.damage=new Dice(1,20);
				else
				w.damage=new Dice(2,6);
		}
	}
		
	
	w.kind="";
	w.crit=2;
	w.ab=0;
	}
		
	w.setOwner(o);
	
	return w;	
	}
	


	public static Weapon create(String n,int num)
	{
	Weapon w=new Weapon(n);
		
	w.amount*=num;	
		
	return w;	
	}
	
	public static Weapon createImproved(String n,int atDam) //attack, damage
	{
	Weapon w=new Weapon(n);
	
	w.name=w.name+" +"+atDam;
	
	w.ab+=atDam;
	w.db+=atDam;
					
	return w;	
	}
	
	
	public void magic(int a,int d)
	{
		ab=a;
		db=d;
	}
	
	public int getAB()
	{
		return ab;
	}
	
	public int getDB()
	{
		return db;	
	}
	
	public String kind()
	{
		return kind;
	}
	
	public boolean isRanged()
	{
	if(kind.equals(""))
	return false;
	
	return true;	
	}
	
	
	public int damBonus()
	{
		int s;
		if(!kind.equals("")) //if ranged- no bonus
		s=0;
		else
		s=Char.mod(owner.getSTR());
		
		s+=db;
		
		int a=Item.howMany(owner.getItems(),"Ring of Damage");
		if(a>3)
		a=3;
				
		s+=a;
		
		
		if(owner.race().equals("Elf")&&(this.getName().equals("Short Bow")||this.getName().equals("Longbow")))
		s++;
		if(owner.race().equals("Dwarf")&&(this.getName().equals("Handaxe")||this.getName().equals("Battleaxe")))
		s++;
		if(owner.race().equals("Halfling")&&(this.getName().equals("Short Bow")||this.getName().equals("Short Sword"))) //not dagger
		s++;
		
		
		return s;
	}
	
	public int damage() //to change //no-(it's also an update)
	{
	
		int r=damage.roll()+damBonus();
		
		return r;
	}
	
	/*public int attack()
	{
		damage.bonus(owner.attackSum());
		
	}
	*/
	
	
	public void setOwner(Char a)
	{
		owner=a;
	}
	
	
	public int getMagic()
	{
	int a=ab;
	int d=db;
	int m=0;
	
	while(a>0&&d>0)
	{
	m++;
	a--;
	d--;	
	}
	
	return m;		
	}
	
	
	public static Weapon getWeaponS()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=15)
	return new Weapon("Short Sword");
	if(r>=16&&r<=25)
	return new Weapon("Longsword");
	if(r>=26&&r<=40)
	return new Weapon("Short Bow");
	if(r>=41&&r<=50)
	return new Weapon("Longbow");
	if(r>=51&&r<=65)
	return new Weapon("Handaxe");
	if(r>=66&&r<=75)
	return new Weapon("Battleaxe");
	if(r>=76&&r<=90)
	return new Weapon("Light Crossbow");
	//91-100
	return new Weapon("Heavy Crossbow");
	}
	
	
	public static Weapon getWeaponM()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=15)
	return new Weapon("Longsword");
	if(r>=16&&r<=30)
	return new Weapon("Longbow");
	if(r>=31&&r<=45)
	return new Weapon("Battleaxe");
	if(r>=46&&r<=60)
	return new Weapon("Heavy Crossbow");
	if(r>=61&&r<=75)
	return new Weapon("Greatsword");
	if(r>=76&&r<=90)
	return new Weapon("Greataxe");
		
	//91-100
	return new Weapon("Bastard Sword");
	}
	
	
	public static boolean existsInList(String n)
	{
		Weapon w=new Weapon(n);
		if(w.damage==null)
		return false;
		
		return true;
	}
	
	
	public int critDam(int a,int ac)
	{
	int s=0;
	if(crit==1)
		if(a==19||a==20)
		{
		int d=Dice.roll(20);
			if(d+owner.attack()>=ac||d==20)
			s=owner.damage();
		}
	if(crit==2)
		if(a==20)
		{
		int d=Dice.roll(20);
			if(d+owner.attack()>=ac||d==20)
			s=owner.damage();
		}
	if(crit==3)
		if(a==20)
		{
		int d=Dice.roll(20);
			if(d+owner.attack()>=ac||d==20)
			s=owner.damage()+owner.damage();
		}
	
	
	
	return s;
	}
	
	
	public Dice getDamage()
	{
	return damage;		
	}	
	
	
	public String write() //set owner, when in C* etc. ? (or ask alla)
	{
	String s=super.write();	
	s="W"+s.substring(1,s.length()-3)+",";
	
	s+=damage.write()+",";
	//s+=	 owner?
	s+=kind+",";
	s+=ab+",";
	s+=db+",";
	s+=crit+",";
	s+="*W";
	return s;
	}
	
	
	
	public static Weapon readW(String s) //careful of "W*W"
	{
		Weapon d=new Weapon("");
				
		s=s.substring(2);
		
		int b=s.indexOf(",");
		int a;
		d.lbs=Double.parseDouble(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.name=s.substring(0,b);
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.value=Double.parseDouble(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		a=Integer.parseInt(s.substring(0,b));
		d.amount=a;
		s=s.substring(b+1);
		
		//weapon
		b=s.indexOf("*D");
		d.damage=Dice.read(s.substring(0,b+1));
		s=s.substring(b+3);
		
		b=s.indexOf(",");
		d.kind=s.substring(0,b);
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.ab=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.db=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.crit=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);
				
		return d;
	}
	
	
	
	
	/*
	*returns object as string
	*/
	
	
	public String toString()
	{
		String s,type="";
		if(!kind.equals(""))
		{
			s="range";
			if(owner!=null)
			type=", ammo: "+kind+" ("+Item.howMany(owner.getItems(),kind)+")";
			else
			type=", ammo: "+kind;
			//ammunition
		}
		else
		s="melee";
		
	//	if(owner!=null)
	//	damage();
		
		int nim=0;
		if(owner!=null)
		nim=damBonus();
		
		String dam=damage.getString(nim);
			
		if(owner!=null)
		//return super.toString()+", kind: "+s+", attack: "+owner.attack()+", damage: "+damage+type;
		return name+" - kind: "+s+", attack: "+owner.attack(this)+", damage: "+dam+type;
		else
		//return super.toString()+", kind: "+s+", attack: "+ab+", damage: "+damage+type;
		return name+" - kind: "+s+", attack: "+ab+", damage: "+dam+type;
	}
	
	
}
