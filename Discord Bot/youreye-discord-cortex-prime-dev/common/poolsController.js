const controller = require('./controller.js');
const {ReplyToCommand, ReceivedCommand}= require('./commands.js');

/*
input: {
  action (POOL_...),
}
*/
async function modifyPools(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let inputOptions = {
    name: parsedInput.pool_name,
    names: parsedInput.pool_names,
    dice: parsedInput.dice ??  '',
    action: input.action,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  }
  
  let messageContent = await controller.RunSafe(() => controller.ModifyDicePool(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Modify pool")
}

/*
input: {
  
}
*/
async function moveBetweenPools(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let dice = parsedInput.dice ??  '',
  source_pool, target_pool
  
  let inputOptions = {
    sourceName: parsedInput.source_pool,
    targetName: parsedInput.target_pool,
    dice: parsedInput.dice ??  '',
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  }
  
  let messageContent = await controller.RunSafe(() => controller.MoveDicePool(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Move pool")
}

/*
input: {
}
*/
async function stepPool(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let inputOptions = {
    name: parsedInput.pool_name,
    die: parsedInput.die ?? 0,
    steps: parsedInput.steps ?? 1,
    mod: parsedInput.mod ?? 0,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  }
  
  let messageContent = await controller.RunSafe(() => controller.StepDicePool(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Step pool")
}

/*
input: {
}
*/
async function rollPool(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let inputOptions = {
    names: parsedInput.pool_names,
    dice: parsedInput.dice ?? '',
    keep: parsedInput.keep ?? 2,
    saveOptOut: parsedInput.save_opt_out ?? '',
    saveOptIn: parsedInput.save_opt_in ?? '',
    extraEffects: parsedInput.extra_effects ?? 0,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
    cacheKey: controller.FormatChannelKey(event),
  }  
  
  let messageContent = await controller.RunSafe(() => controller.RollDicePool(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Roll pool")
}

/*
input: {
}
*/
async function showPools(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let inputOptions = {    
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  }
  
  let messageContent = await controller.RunSafe(() => controller.ShowDicePools(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Show pool")
}

async function replyToCommand(event, messageContent, commandName) {
  console.log(commandName + ' result: ' + messageContent.join('\n'))
  await ReplyToCommand(event, messageContent)  
  await updateSummaryMessage(event)
}

/* 
Returns input: {
  pool_name, pool_names, source_pool, target_pool
  die (number), dice,
  mod (number), steps (number),
  keep, save_opt_out, save_opt_in, extra_effects
  channel_id, user_id (number - defaults to 0)
  }
  
  Strings default to '', numbers default to undefined.
*/
function parseInput(event) {
  let dataOptions = event.data.options
  
  var inputMap = new Map()
  dataOptions.forEach(option => {
    inputMap.set(option.name, option.value)
  }) 
  
  return  {
    pool_name: inputMap.get('name') ?? '',
    source_pool: inputMap.get('from') ?? '',
    target_pool: inputMap.get('to') ?? '',
    dice: inputMap.get('dice') ?? '',
    pool_names: inputMap.get('names') ?? '',
    die: inputMap.get('die') ?? undefined,
    mod: inputMap.get('mod') ?? undefined,
    steps: inputMap.get('steps') ?? undefined,
    keep: inputMap.get('keep') ?? undefined,
    save_opt_out: inputMap.get('save-opt-out') ?? '',    
    save_opt_in: inputMap.get('save-opt-in') ?? '',
    extra_effects: inputMap.get('extra-effects') ?? undefined,        
    channel_id : event.channel_id ?? '', 
    user_id: event.member.user.id ?? 0,
  }
}

async function updateSummaryMessage(event) {
  let updateInput = {
    event: event,
  }
  await controller.UpdatePinnedSummaryMessage(updateInput, controller.DefaultIocContainer)  
}

module.exports = {
  ShowPools: showPools,
  ModifyPools: modifyPools,
  MoveBetweenPools: moveBetweenPools,
  StepPool: stepPool,
  RollPool: rollPool,
};
