const controller = require('../controller.js');
const {Storage}= require('../storage.js');

const CONSOLE_LOG = false


function throwIfNotEqual(result, expectedResult, name) {
   if(!compareObjects(result, expectedResult)) {
    logMessage(`Got: ` + JSON.stringify(result));
    logMessage(`Expected: ` + JSON.stringify(expectedResult));
    throw new Error(`Unexpected result when preparing constant ${name}. Got: ` + JSON.stringify(result));
  }
}


function compareObjects(array1, array2) {
  return JSON.stringify(array1) == JSON.stringify(array2)
  //return array1.length === array2.length && array1.every(function(value, index) { return value === array2[index]})
}


/*
Array: array of randomizer results
options: {
  delayMap - method name to list of MS delays in MemCache,
  failMap - method name to a set of call-indexes to throw an error to,
}

*/
function createIocContainer(array, options){
  var random = new Randomizer(array);
  var cache = new MemCache(options?.delayMap, options?.failMap);
  var storage = new Storage(cache);
  return {
    getNextNumber: (n, m) => random.getNextNumber(n,m), 
    options: { 
      cacheTimeSecs: 0,
    },
    cache: {
      cacheValue: (key, value, ttlSeconds = 600) => cache.cacheValue(key, value, ttlSeconds),
      getCacheValue: (key, defaultValue = undefined) => cache.getCacheValue(key, defaultValue),   
      getCacheEntries: () => cache.getCacheEntries(),
      clearCacheKey: (key) => cache.clearCacheKey(key),
    },
    storage: storage,
  }
}

// async function.
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


class TestCase{
  /*
   input: {
      name: string,
      functionToRun: async func(input) => result,
      inputArgs: any,
      expectedResult: any,
      expectedError: {
        message: string,
        type: Error type
      },
  }
  */
  constructor(input) {
    this.input = input
  }
  
  name() {
    return this.input.name
  }
  
  async run() {    
    return await this.input.functionToRun(this.input.inputArgs)
  }
  
  expectedError() {
    return this.input.expectedError
  }
  
  expectedResult() {
    return this.input.expectedResult
  }
  
}

  
class MemCache {
   constructor(delayMap = undefined, failMap = undefined){
    this.cacheMap = new Map()
    // Map from method name to a list of delays (in ms) to be performed by order.
    this.delayMap = delayMap ?? {}
    // Map from method name to a set of indexes (e.g., counter) to throw an error to.
    this.failMap = failMap ?? {}
    // Map from method name to a counter of how many times it was called.
    this.runCounter = {}
  }
  
  async cacheValue(key, value, ttlSeconds) {
    await this.waitIfNeeded(this.cacheValue.name)
    logMessage(`Test caching key: ` + key);
    logMessage(`Test caching value: ` + JSON.stringify(value));
    this.cacheMap.set(key, value)
  }
  
  async getCacheValue(key, defaultValue) {
    await this.waitIfNeeded(this.getCacheValue.name)
    let value = this.cacheMap.get(key) ?? defaultValue
    logMessage(`Test getting key: ` + key);
    logMessage(`Test cached value: ` + JSON.stringify(value));
    return cloneMemValue(value);
  }
  
  async getCacheEntries() {
    await this.waitIfNeeded(this.getCacheEntries.name)
    let result = Array.from(this.cacheMap.entries()).map(kv => ({key: kv[0], value: cloneMemValue(kv[1])}))
    logMessage(`Test get all cache entries: ` + JSON.stringify(result));
    return result
  }
  
  async clearCacheKey(key) {
    await this.waitIfNeeded(this.clearCacheKey.name)
    logMessage(`Test deleting key: ` + key);
    this.cacheMap.delete(key)
  }
  
  async waitIfNeeded(methodName) {    
    if(!this.runCounter[methodName]) {
      this.runCounter[methodName] = 0
    }
    
    let currentIndex = this.runCounter[methodName]++
    let delayArray = this.delayMap[methodName] ?? []
    let currentDelay = delayArray[currentIndex]
    if(currentDelay) {
      logMessage(`[start] Sleeping for ${methodName}: ${currentDelay}ms`)
      await sleep(currentDelay)
      logMessage(`[end] Sleeping for ${methodName}: ${currentDelay}ms`)
    }
    
    let errorIndexSet = this.failMap[methodName] ?? new Set()
    if (errorIndexSet.has(currentIndex)) {
      logMessage(`Throwing error for method '${methodName}' and index ${currentIndex}`)
      throw new Error(`Test error for method '${methodName}' and index ${currentIndex}`);
    } else {
      logMessage(`Not throwing error for method '${methodName}' and index ${currentIndex}: ${Array.from(errorIndexSet).join(',')}`)
    }
  }
}

// Meant for cases where the undrlying object might change without actually storing the value (e.g., memory cache).
function cloneMemValue(value) {
  if (value == undefined) {
    return value
  }
  
  return JSON.parse(JSON.stringify(value)) 
}
  
  
class Randomizer {
  constructor(results){
    this.results = results
    this.index = -1
    logMessage(`Randomizer: ` + results.join(','));
  }
  
  getNextNumber(min, max) {
    this.index = this.index + 1
    let r = this.results[this.index]
    logMessage(`Randomizer r: ` +r);
    if (r < min || r > max) {
      throw new Error(`r (${r}) is out of bounds [${min}, ${max}]`);
    }
    return r
    
  }
  
}


function logMessage(msg) {
  if(CONSOLE_LOG) {
    console.log(`[${new Date()}] - ` + msg);
  }
}


module.exports = {
  compareObjects: compareObjects,
  throwIfNotEqual: throwIfNotEqual,
  TestCase: TestCase,
  createIocContainer: createIocContainer,
};
