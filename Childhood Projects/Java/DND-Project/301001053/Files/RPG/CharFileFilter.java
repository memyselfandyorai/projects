
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

public class CharFileFilter extends javax.swing.filechooser.FileFilter{
	
	/**
	 *The extension of the filter
	 */
    private String extension = "char";

	
	/**
	*Constructs a new CharFileFilter
	*/
	public CharFileFilter()
	{
		super();
	}

	
	/**
	*Checks if the file has the wanted extension
	*@param f : the file
	*/
    public boolean accept(File f)
	{
		if(f!=null)
		{
			if(f.isDirectory())
			return true;
		
		
		if(getExtension(f)!=null&&getExtension(f).equals(extension))
		return true;
		}
		
		return false;
	}

  
	/**
	*Returns the extension of a file
	*@param f : the file
	*/
   public String getExtension(File f) {                        
	if(f != null) {
	    String filename = f.getName();
	    int i = filename.lastIndexOf('.');
	    if(i>0 && i<filename.length()-1) {
		return filename.substring(i+1).toLowerCase();
	    };
	}
	return null;
    }

     
    public String getDescription()  {
	return "*."+extension;
    }
    
    }