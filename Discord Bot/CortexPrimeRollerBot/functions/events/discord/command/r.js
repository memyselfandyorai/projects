const controller = require("../../../../common/controller.js");
// const lib = require("lib")({ token: process.env.STDLIB_SECRET_TOKEN });
const {
  ReplyToCommand,
  ReceivedCommand,
} = require("../../../../common/commands.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  await ReceivedCommand(commandInteraction);

  // Adding D6, decreases largest die, increases effect
  const SHOCK_MOD = 1;
  // result of 2 counts as 1
  const OBLIVIOUS_MOD = 2;

  // console.log("raw input data: " + JSON.stringify(context.params.event.data));
  let inputOptions = parseInput(commandInteraction);
  console.log("parsed input: " + JSON.stringify(inputOptions));

  let messageContent = [];

  // Special mod.
  switch (inputOptions.mod ?? 0) {
    case 0:
      break;
    case SHOCK_MOD:
      // Adding D6, decreases largest die, increases effect
      inputOptions.dice = inputOptions.dice + " 6";
      inputOptions.effect_mod = (inputOptions.effect_mod ?? 0) + 1;
      inputOptions.die_mod = (inputOptions.die_mod ?? 0) - 1;
      messageContent.push(["SHOCK: Added D6, increased effect"]);
      break;

    case OBLIVIOUS_MOD:
      // result of 2 counts as 1
      inputOptions.hitch = 2;
      messageContent.push(["OBLIVIOUS: 2 counts as 1"]);
      break;
  }

  let resultMessageContent = await controller.RunSafe(() =>
    controller.RollCortexPrime(
      inputOptions,
      commandInteraction.getIocContainer(),
    ),
  );
  messageContent = messageContent.concat(resultMessageContent);

  await ReplyToCommand(commandInteraction, messageContent);
}

/*
 Returns input: {
  dice, keep, effect_mod, die_mod, hitch, mod, extra_effect_dice, merge
  save_opt_out, save_opt_in
  cache_key}
*/
function parseInput(commandInteraction) {
  var inputMap = commandInteraction.getInputOptionsMap();

  let input = {
    dice: inputMap.get("dice"),
    keep: inputMap.get("keep"),
    hitch: inputMap.get("hitch"),
    effect_mod: inputMap.get("effect-mod"),
    die_mod: inputMap.get("die-mod"),
    mod: inputMap.get("mod"),
    extra_effect_dice: inputMap.get("extra-effect-dice"),
    merge: inputMap.get("focus"),
    save_opt_out: inputMap.get("save-opt-out"),
    save_opt_in: inputMap.get("save-opt-in"),
    pools: inputMap.get("pools"),

    cache_key: controller.FormatChannelKey(commandInteraction),
    save_input: controller.GetSaveInput(commandInteraction),
  };

  return input;
}

/*
"data": {
  "type": 1,
  "options": [
    {
      "value": "10 4d6",
      "type": 3,
      "name": "dice"
    }
  ],
  "name": "rollp",
  "id": "951595708358750239",
  "guild_id": "947924911391911997"
},
*/
// "guild_id": "947924911391911997",
// channel_id": "947924911836524545
// "application_id": "947923931615088682",

/*
Create command:

const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "r",
  "description": "Rolls a cortex-prime roll",
  "options": [
    {
      "type": 3,
      "name": "dice",
      "description": "Space-separated list of dice. Support 2D10 format.",
      "required": true
    },
    {
      "type": 4,
      "name": "keep",
      "description": "Optional (default 2) value indicating how many dice to keep for the score"
    },
    {
      "type": 4,
      "name": "effect-mod",
      "description": "Effect modifier. Can be negative or positive. Defaults to 0."
    },
    {
      "type": 4,
      "name": "hitch",
      "description": "The roll score which counts as a hitch (i.e., ignored from result pool). Defaults to 1."
    },
    {
      "type": 4,
      "name": "die-mod",
      "description": "Modifies die levels (default: 0). When positive- increases min, when negative- decreases max."
    },
    {
      "type": 4,
      "name": "mod",
      "description": "A mode that relates to a special roll. See \"/help subject: /r mods\".",
      "choices": [
        {
          "name": "SHOCK",
          "value": 1
        },
        {
          "name": "OBLIVIOUS",
          "value": 2
        }
      ]
    },
    {
      "type": 3,
      "name": "extra-effect-dice",
      "description": "Dice to add to the roll (same format as \"dice\"), each one adds an additional effect die."
    },
    {
      "type": 3,
      "name": "focus",
      "description": "A list of die types to merge. For each, merges a pair to their step-up."
    },
    {
      "type": 3,
      "name": "save-opt-out",
      "description": "A list of comma-separated saved names to disclude from roll. Use * for all. Default: empty."
    },
    {
      "type": 3,
      "name": "save-opt-in",
      "description": "A list of comma-separated saved names to include in roll. Use * for all. Default: *."
    },
    {
      "type": 3,
      "name": "pools",
      "description": "A list of comma-separated pool names to include in roll. Default: empty."
    }
  ]
});
*/
