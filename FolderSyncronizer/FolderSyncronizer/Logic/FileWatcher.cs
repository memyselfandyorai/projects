﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderSyncronizer.Logic
{
    class FileWatcher
    {
        private FileSystemWatcher _fileSystemWatcher;

        private string _sourceDirectory;

        private string _targetDirectory;

        public FileWatcher(string sourceDirectory, string targetDirectory)
        {
            _sourceDirectory = sourceDirectory;
            _targetDirectory = targetDirectory;

            _fileSystemWatcher = new FileSystemWatcher(sourceDirectory, "*.*");
            _fileSystemWatcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName;

            _fileSystemWatcher.Created += ChangeOccured;
            _fileSystemWatcher.Deleted += ChangeOccured;
            _fileSystemWatcher.Renamed += ChangeOccured;
            _fileSystemWatcher.Changed += ChangeOccured;

            _fileSystemWatcher.IncludeSubdirectories = true;
            _fileSystemWatcher.EnableRaisingEvents = true;
        }

        private void ChangeOccured(object sender, FileSystemEventArgs e)
        {
            //e.
            throw new NotImplementedException();
        }
    }
}
