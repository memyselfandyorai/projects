﻿using Infra.Framework.Ioc;
using System.Threading;

namespace Infra.FrameworkTests.Examples
{
    class Main
    {

        public void RunProgram(string[] args)
        {
            using (IIocContainer iocContainer = BootstrapperImplementations())
            {
                AppFlavor flavor = (args?.Length > 0 && args[0].Equals("SLAVE")) ? AppFlavor.SLAVE : AppFlavor.MASTER;
                IAppManager appManager = iocContainer.Resolve<IAppManagerFlavorFactory>().GetAppManager(flavor);
                appManager.Start(args: null);
                Thread.Sleep(10);
                appManager.Stop();
            } // appManager.Dispose() - automatically called.

        }

        private IIocContainer BootstrapperImplementations()
        {
            IIocContainer container = SimpleInjectorIocContainer.Create();

            container.Register<ILogger, Log4netLogger>(Lifecycle.Singleton);
            container.Register<IAppManagerFlavorFactory, AppManagerFlavorFactory>(Lifecycle.Singleton);

            return container;
        }
    }
}
