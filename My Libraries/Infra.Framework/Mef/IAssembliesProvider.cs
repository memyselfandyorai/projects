﻿using System.Collections.Generic;
using System.Reflection;

namespace Infra.Framework.Mef
{
    public interface IAssembliesProvider
    {
        /// <summary>
        /// Returns all assemblies that the using project references.
        /// These are the projects that the MefFactory supports loading from.
        /// </summary>
        IEnumerable<Assembly> GetAllProjectAssemblies();
    }
}
