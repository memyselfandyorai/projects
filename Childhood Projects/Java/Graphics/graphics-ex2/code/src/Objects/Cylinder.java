package Objects;

import General.Intersection;
import General.NormalVector;
import General.Plane;
import General.Point;
import General.Point2D;
import General.Ray;
import General.Scene;
import General.Vector;
import Main.Utilities;

/**
 * 
 * represents a cylinder
 *
 */
public class Cylinder extends Surface {
	
	private Point start;
	private NormalVector direction;
	private double length;
	private double radius;
	
	/**
	 * default constructor
	 */
	public Cylinder() {
	}


	public Cylinder(Point start, NormalVector direction, double length, double radius) {
		this.start = start;
		this.direction = direction;
		this.length = length;
		this.radius = radius;
	}
	
	
	public Point getStart() {
		return start;
	}

	public void setStart(Point start) {
		this.start = start;
	}

	public Vector getDirection() {
		return direction;
	}

	public void setDirection(NormalVector direction) {
		this.direction = direction;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}


	public Intersection intersect(Scene scene,Ray ray) {
		NormalVector rayVector = ray.getVector();
		Point rayPoint = ray.getPoint();
		
		//a = |VXD|^2
		Vector VXD = rayVector.crossProductRegular(direction);
		double a = VXD.dotProduct(VXD);
		
		//b= 2* ( (P0-O)XD) * (VXD)
		Vector P0_O = Vector.subPoints(rayPoint, start);
		Vector P0_OxD = P0_O.crossProduct(direction);
		double b = 2*(P0_OxD.dotProduct(VXD));
		
		//c= | (P0-O)XD |^2 - r^2
		double c = P0_OxD.dotProduct(P0_OxD) - Math.pow(radius, 2);
		
		double[] result = Utilities.solveQuadraticEquation(a, b, c);
		
		if (result==Utilities.NO_SOLUTION)
			return Intersection.MISS;
				
		double t=-1; //init
		
		Point point1 = ray.getPoint(result[0]);
		Point point2 = ray.getPoint(result[1]);
		
		double dist1 = rayPoint.distance(point1);
		double dist2 = rayPoint.distance(point2);
		
		boolean onCylinder1=isOnCylinder(point1);
		boolean onCylinder2=isOnCylinder(point2);
		
		if(!onCylinder1&&!onCylinder2)
			return Intersection.MISS;
		
		
		if(!onCylinder1)
			t=result[1];
		if(!onCylinder2)
			t=result[0];
		
		if(onCylinder1&&onCylinder2)
		{
			if (dist1<dist2)
				t=result[0];
			else
				t=result[1];
		}
		
		
		if(t<0) //both are negative
			return Intersection.MISS;
		
		//P = P0 + t*V
		Point P = ray.getPoint(t);
		
		double dist = rayPoint.distance(P);
		
		
		NormalVector normal=getNormal(ray,P);
						
		return new Intersection(dist, this, P,ray,normal);
			
	}
	
	
	public boolean hasAllValues() {
		if (start==null || direction==null)
			return false;
		if (length<=0 || radius<=0)
			return false;
		return true;
	}

	
	
	
	
	private boolean isOnCylinder(Point P)
	{
		//U = O + length*D
		Point U = start.add(direction.product(length).toPoint());
		
		double distPO = P.distance(start);
		double distPU = P.distance(U);
		
		Vector vec;
		Point point;
		
		if (distPO < distPU) {
			vec = direction;
			point = start;
		}
		else{
			vec = direction.negative();
			point = U;
		}
		
		if (vec.dotProduct(Vector.subPoints(P,point))<0)
			return false;
		
		return true;
	}
	
	
	private NormalVector getNormal(Ray ray,Point P)
	{
		//distance from start point to the projection of (hit-start)
		double startDirectionHitDistance=direction.dotProduct(Vector.subPoints(P,start));
		//the point from start point to the projection of (hit-start)
		Point startDirectionHit=Ray.getPoint(start, direction, startDirectionHitDistance);
		//the normal to the cylinder at hit point
		NormalVector normal=NormalVector.subPoints(P, startDirectionHit);
		//==real normal.
		
		//check if inner side is hit:
				
		
		return normal;
	}
	
	

	public Point2D parametrization(Point point) {
		Vector vec1 = Vector.subPoints(point, start);
		double angle = (vec1.dotProduct(direction))/(vec1.length()*direction.length());
		double length_angle = vec1.length()*angle;
		Vector vec2 = direction.product(length_angle);
		Point project = start.add(new Point(vec2.getX(),vec2.getY(),vec2.getZ()));
		NormalVector spanVector = NormalVector.subPoints(project, point);
		Point p1 = new Point (start.getX(),start.getY()-1,start.getZ());
		Vector yAxis = Vector.subPoints(start, p1);
		Vector relativeVec = direction.crossProduct(yAxis);
		if (relativeVec.isZero()){
			Point p2 = new Point (start.getX()-1,start.getY(),start.getZ());
			Vector xAxis = Vector.subPoints(start, p2);
			relativeVec = direction.crossProduct(xAxis);
		}
		relativeVec.normalize();
		double cosTheta = relativeVec.dotProduct(spanVector);
		double theta = Math.acos(cosTheta);
		Vector helpVector = direction.crossProduct(relativeVec);
		if (helpVector.dotProduct(spanVector)<0)
			theta = 2*Math.PI - theta;
		double v = theta/(2*Math.PI);
		double u = (length - length_angle)/length;
		
		return new Point2D(u,v);
	}
	

	public boolean onRight(Plane plane)
	{
		return plane.onRight(start);
	}
	
}
