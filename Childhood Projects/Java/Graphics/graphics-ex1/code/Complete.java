
/**
 * The class represents the completion method
 */
public class Complete extends AbstractSolver{
	
	
	private ImageMatrix input;
	private ImageMatrix mask;
		
	/**
	 * constructor of 'Complete' class
	 * @param in - input image matrix
	 * @param m - mask image matrix
	 */
	public Complete(ImageMatrix in,ImageMatrix m)
	{
		super(in.getRows(),in.getCols());
		input=in;
		mask=m;
	}
	
	/**
	 * returns true iff coordinate (i,j) is a hole 
	 */
	protected boolean isMissing(int i,int j)
	{
		return ImageMatrix.isMissing(i, j, mask);
	}
	
	/**
	 *  sets a value in vector b to be the known value (when pixel is not a hole)
	 */
	protected void setBNotMissing(double[] b,int index,ImageMatrix.RGB rgb)
	{
		int i=iIndex(index);
		int j=jIndex(index);
		b[index]=input.getColor(i, j, rgb);
	}
	
	/**
	 *  sets a value in vector b to be 0 (when pixel is a hole)
	 */
	protected void setBMissing(double[] b,int index,ImageMatrix.RGB rgb)
	{
		b[index]=0;
	}
	
}
