const { RollPool } = require("../../../../common/poolsController.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  let input = {};
  await RollPool(input, commandInteraction);
}

/*

const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "pool-roll",
  "description": "Rolls a pool.",
  "options": [
    {
      "type": 3,
      "name": "names",
      "description": "Comma separates list of pool names. Use * to roll all.",
      "required": true
    },
    {
      "type": 3,
      "name": "dice",
      "description": "Extra dice to add. Default: none."
    },
    {
      "type": 4,
      "name": "keep",
      "description": "Value indicating how many dice to keep for the score. Default: 2."
    },
    {
      "type": 3,
      "name": "save-opt-out",
      "description": "A list of comma-separated saved names to disclude from roll. Use * for all. Default: empty."
    },
    {
      "type": 3,
      "name": "save-opt-in",
      "description": "A list of comma-separated saved names to include in roll. Use * for all. Default: empty."
    },
    {
      "type": 4,
      "name": "extra-effects",
      "description": "A number indicating extra dice to use as effect. Default: 0."
    }
  ]
});
*/
