echo off

echo Prerequisites: Java Runtime (supporting javac command)

set /p path=Installation folder? 

set folderPath=%path%\DND3rd


echo cd /D %folderPath%\rpg > Play.bat
echo javac *.java >> Play.bat
echo java Client >> Play.bat


echo cd /D %folderPath%\server > "Start Server.bat"
echo javac *.java >> "Start Server.bat"
echo java Server >> "Start Server.bat"

echo on

md %folderPath%\server
md %folderPath%\rpg
cd..
cd files
copy server\*.* %folderPath%\server
copy rpg\*.* %folderPath%\rpg

pause