const _ = require("lodash");
const { ShowSaves } = require("./saver.js");
const { ShowPools } = require("./pools.js");
const { ShowCounters } = require("./counters.js");
const { SendMessage } = require("./commands.js");

const SUMMARY_INTRO = "Game Information:";

class RateLimitError extends Error {
  constructor(...params) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
    }
    this.name = this.constructor.name;
  }
}

// Pins a new summary message, and cleans up old summary messages (from storage and pin).
// Returns list of messages to respond
// input: commandInteraction
async function pinNewSummaryMessage(input, iocContainer) {
  let interaction = input.event;
  let channelId = interaction.getChannelId();
  const pinManager = interaction.getPinManager();

  let pinnedMessage = await getPinnedMessage(pinManager, channelId);
  if (pinnedMessage) {
    console.log(`Destroying pinned message ${pinnedMessage.id}`);
    await pinManager.deletePinnedMessage(channelId, pinnedMessage.id);
  }

  let messageContent = await getSummaryContent(input, iocContainer);
  let content = messageContent.join("\n");
  console.log(`Summary content: ` + content);

  let createdMessages = await SendMessage(interaction, messageContent);
  let msgId = createdMessages[0].id;
  console.log(`New message ID: ` + msgId);

  await pinManager.pinMessage(channelId, msgId);

  return ["Pinned info message"];
}

// input: {event (commandInteraction)}
// iocContainer: IocContainer
// void
async function updateSummaryMessage(input, iocContainer) {
  let commandInteraction = input.event;
  let channelId = commandInteraction.getChannelId();
  const pinManager = commandInteraction.getPinManager();

  let pinnedMessage = await getPinnedMessage(pinManager, channelId);
  if (pinnedMessage) {
    console.log(`Found pinned message ${pinnedMessage.id}`);
    let messageContent = await getSummaryContent(input, iocContainer);
    let content = messageContent.join("\n");

    console.log(`Summary content: ` + content);

    await pinManager.updatePinnedMessage(channelId, pinnedMessage.id, {
      text: content,
    });
  }

  return [];
}

// returns a the newest summary pinned message.
async function getPinnedMessage(pinManager, channelId) {
  try {
    let messages = await pinManager.fetchAllPinnedMessages(channelId);

    return _.maxBy(
      messages.filter((msg) => msg.content.text.startsWith(SUMMARY_INTRO)),
      "timestamp",
    );
  } catch (e) {
    if (e.message.includes("You are being rate limited")) {
      // "message": "You are being rate limited. (discord/channels@0.3.1/pins/list)"
      // "stack": "Error: You are being rate limited. (discord/channels@0.3.1/pins/list)\n at IncomingMessage.<anonymous> (/opt/node_modules/lib/lib/request.js:116:21)\n at IncomingMessage.emit (events.js:412:35)\n at endReadableNT (internal/streams/readable.js:1334:12)\n at processTicksAndRejections (internal/process/task_queues.js:82:21)"
      // Print this since stacktrace disappears:
      console.warn(e);
      throw new RateLimitError("Too many frequent requests to discord API", {
        cause: e,
      });
    } else {
      throw e;
    }
  }
}

// Same input as above
// Returns a list.
async function getSummaryContent(input, iocContainer) {
  let messageContent = [SUMMARY_INTRO];
  let commandInteraction = input.event;
  let channelId = commandInteraction.getChannelId();

  let showPoolsInput = {
    userId: undefined,
    channelId: channelId,
  };
  let poolsResult = await ShowPools(showPoolsInput, iocContainer);
  if (poolsResult.hasPools) {
    let pools = poolsResult.messageContent;
    messageContent.push("");
    messageContent.push("**Pools -**");
    messageContent.push(...pools);
  }

  let showCountersInput = {
    userId: undefined,
    channelId: channelId,
  };
  let countersResult = await ShowCounters(showCountersInput, iocContainer);
  if (countersResult.hasCounters) {
    let counters = countersResult.messageContent;
    messageContent.push("");
    messageContent.push("**Counters -**");
    messageContent.push(...counters);
  }

  let showSaveInput = {
    userId: undefined,
    guildId: channelId,
  };
  let saveResult = await ShowSaves(showSaveInput, iocContainer);
  if (saveResult.hasSaves) {
    let saves = saveResult.messageContent;
    messageContent.push("");
    messageContent.push("**Saved results -**");
    messageContent.push(...saves);
  }

  return messageContent;
}

module.exports = {
  RateLimitError: RateLimitError,
  UpdateSummaryMessage: updateSummaryMessage,
  PinNewSummaryMessage: pinNewSummaryMessage,
};
