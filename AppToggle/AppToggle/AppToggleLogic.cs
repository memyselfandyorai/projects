﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppToggle
{
    class AppToggleLogic
    {
        internal void Run()
        {
            var applications = AppToggleSettings.Default.ApplicationPaths;
            int nextIndex = AppToggleSettings.Default.NextIndex;

            if(nextIndex >= 0 && nextIndex < applications.Count)
            {
                RunApplication(applications[nextIndex]);
            }
            else
            {
                Console.WriteLine("No suitable collection for index " + nextIndex);
            }

            int advance = nextIndex + 1;
            if(advance >= applications.Count)
            {
                advance = 0;
            }
            AppToggleSettings.Default.NextIndex = advance;
            AppToggleSettings.Default.Save();
        }

        private void RunApplication(string path)
        {
            ProcessStartInfo info = new ProcessStartInfo
            {
                FileName = "cmd",
                Arguments = $"/c \"{path}\"",
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                RedirectStandardInput = true
            };
            Console.WriteLine("Start " + path);
            Process process = Process.Start(info);
            Console.WriteLine("Waiting");
            Bprocess.WaitForExit();
            Console.WriteLine("End");
        }
    }
}
