﻿using Infra.Framework.Ioc;
using Infra.Framework.Mef;
using System;
using System.Collections.Generic;

namespace Infra.FrameworkTests.TestObjects
{
    public class SimpleTypeFactory : MefFactory<IKeyedObject, MyType, IKeyedObject>, ITypeFactory
    {
        private readonly Lifecycle _lifecycle;

        public bool DisposedCalled { get; private set; }
        public ISimpleInterface OptionalImplementation { get; private set; }

        public const string ASSEMBLY_PREFIX = "Infra.Framework";

        public SimpleTypeFactory(IIocContainer optionalContainer) : base(convertSingleton: import => CreateNew(import, optionalContainer), ASSEMBLY_PREFIX)
        {
            this._lifecycle = Lifecycle.Singleton;
            this.OptionalImplementation = optionalContainer.Resolve<ISimpleInterface>();
        }

        internal SimpleTypeFactory(Lifecycle lifecycle) : base(convertSingleton: null, ASSEMBLY_PREFIX)
        {
            this._lifecycle = lifecycle;
        }

        internal SimpleTypeFactory(IAssembliesProvider provider) : base(provider)
        {
            this._lifecycle = Lifecycle.Singleton;
        }

        public override void Dispose()
        {
            base.Dispose();
            this.DisposedCalled = true;
        }

        public IEnumerable<IKeyedObject> GetAll()
        {
            return CreateAll();
        }

        public IKeyedObject GetByType(MyType type)
        {
            return Create(type);
        }

        protected override IKeyedObject ConvertImport(IKeyedObject import)
        {
            switch (_lifecycle)
            {
                case Lifecycle.Singleton:
                    // Reuse object
                    return import;
                case Lifecycle.Transient:
                    // Create new instance each time
                    return CreateNewImport(import);
                default:
                    throw new NotImplementedException();
            }
        }

        protected override MyType GetKey(IKeyedObject import)
        {
            return import.Type;
        }
    }
}
