﻿using System;
using System.ComponentModel.Composition;

namespace Infra.Framework.Mef
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class ExportForMefAttribute : ExportAttribute
    {
        /// <summary>
        /// Exports the class as a TImport for usage by a MefFactory implementation.
        /// </summary>
        /// <param name="type">Type that is the corresponding TImport from MefFactory</param>
        public ExportForMefAttribute(Type type) : base(type)
        {

        }
    }
}
