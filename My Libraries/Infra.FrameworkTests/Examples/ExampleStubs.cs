﻿using Infra.Framework.Ioc;
using Infra.Framework.Mef;
using System;

namespace Infra.FrameworkTests.Examples
{
    #region Logger
    class Log4netLogger : ILogger
    {
        public void LogDebug(string msg, Exception exception = null)
        {
        }
    }
    #endregion

    #region AppManger

    // Implementations specific to Slave in the ApplicationEngine abstraction level. ExportForMef is needed.
    [ExportForMef(typeof(IAppManager))]
    class SlaveAppManager : IAppManager
    {
        public AppFlavor SupportedFlavor => AppFlavor.SLAVE;

        public SlaveAppManager()
        {
            // Default constructor is needed for Mef.
        }

        public SlaveAppManager(ILogger unused)
        {
        }

        public void Dispose()
        {
        }

        public void Start(AppManagerArgs args)
        {
        }

        public void Stop()
        {
        }
    }

    // Implementations specific to Master in the ApplicationEngine abstraction level. ExportForMef in needed.
    [ExportForMef(typeof(IAppManager))]
    class MasterAppManager : IAppManager
    {
        private readonly ILogger _logger;

        public AppFlavor SupportedFlavor => AppFlavor.MASTER;

        public MasterAppManager()
        {
            // Default constructor is needed for Mef.
        }

        public MasterAppManager(ILogger logger)
        {
            this._logger = logger;
        }

        public void Dispose()
        {
            Stop();
        }

        public void Start(AppManagerArgs args)
        {
            this._logger.LogDebug($"Starting with {args}");
        }

        public void Stop()
        {
            this._logger?.LogDebug("Stopping"); // Could be null since Dispose will be called for created instances with default ctors.
        }
    }


    // Mapping between AppFlavor to its AppManager
    class AppManagerFlavorFactory : MefFactory<IAppManager, AppFlavor, IAppManager>, IAppManagerFlavorFactory
    {
        private readonly IIocContainer _iocContainer;

        // When IocContainer creates IAppManagerFlavorFactory, it injects (Resolves) recursively every registered interface. In example IIocContainer implementation, it registered itself.
        // First parameter of "base" is optional. If we wanted to create a singleton with parameters, this was the way: Replacing between ConvertStoredSingleton and ConvertImport implementations.
        public AppManagerFlavorFactory(IIocContainer iocContainer)
        {
            this._iocContainer = iocContainer;
        }
                

        public IAppManager GetAppManager(AppFlavor flavor)
        {
            var manager = Create(flavor);

            if(manager == null)
            {
                throw new NotImplementedException("Use [ExportForMef(typeof(...))] and remember to call base() in factory implementation with your Projects' prefixes");
            }

            return manager;
        }

        protected override IAppManager ConvertImport(IAppManager import)
        {
            // This will create the relevant implementation of IAppManager, adding logger to constructor.
            return CreateNewImport(import, this._iocContainer.Resolve<ILogger>()); 
        }

        protected override AppFlavor GetKey(IAppManager import)
        {
            return import.SupportedFlavor;
        }
    }

    #endregion
}
