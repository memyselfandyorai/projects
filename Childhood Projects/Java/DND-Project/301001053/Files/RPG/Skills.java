
import java.io.*;
import java.util.*;
import javax.swing.*;

//not allowed to copy\paste!

//add weight considerations, in climb and swim!.

public class Skills implements Serializable{
	
	/**
 	*Swim skill score
 	*/
	private int swim; //str
	/**
 	*Climb skill score
 	*/
	private int climb; //str -arm pen
	/**
 	*Perform skill score
 	*/
	private int perf; //perform (cha)
	/**
 	*Pick Pocket skill score
 	*/
	private int pick; //pickp pocket. dex -ar penalty. check against what.   all gp, partly gp, item.
	/**
 	*Search skill score
 	*/
	private int search; //int
	/**
 	*Spot skill score
 	*/
	private int spot; //wis
	/**
 	*Hide skill score
 	*/
	private int hide; //dex -ar penalty. [move silently]
	/**
 	*Intuit Direction skill score
 	*/
	private int dir; // intuit direction. wis. dc 15 at forests (and more?)
	/**
 	*Heal skill score
 	*/
	private int heal; //wis
	/**
 	*Craft skill score
 	*/
	private int craft; //make items\weapons etc. int. invent weapon?
	
	//concentration- maybe later. only for magicians. d20+lvl+mod (against 10+lvl?)
	
	//swim: -1 for every 5 (more?) pound inv.
	
	
	//after "talk": persuade

	/**
 	*"Owner" of the skills
 	*/
	private Character owner;
	
	
	/**
	*Constructs a new Skills
	*@param o : the owner
	*@param s : swim
	*@param c : climb
	*@param p : perform
	*@param pp : pick pocket
	*@param sc : search
	*@param sp : spot
	*@param h : hide
	*@param r : intuit direction
	*@param hl : heal
	*@param cr : craft
	*/
	public Skills(Character o,int s,int c,int p,int pp,int sc,int sp,int h,int r,int hl,int cr)
	{
		swim=s;
		climb=c;
		perf=p;
		pick=pp;
		search=sc;
		spot=sp;
		hide=h;
		dir=r;
		heal=hl;
		craft=cr;
		owner=o;
	
	}
	
	public Skills()
	{
		
	}
	
	public void setOwner(Character c)
	{
	owner=c;		
	}
	
	public void addSpot(int a)
	{
		spot+=a;
	}
	
	public void addSearch(int a)
	{
		search+=a;
	}
	
	public void addHide(int a)
	{
		hide+=a;
	}
	
	public void addClimb(int a)
	{
		climb+=a;
	}
	
	
	public void addPerf(int a)
	{
		perf+=a;
	}
	
	
	public void addCraft(int a)
	{
		craft+=a;
	}

	
	public void addDir(int a)
	{
		dir+=a;
	}	
	
	
	public void addPick(int a)
	{
		pick+=a;
	}	
		
	public void addSwim(int a)
	{
		swim+=a;
	}	

	public void addHeal(int a)
	{
		heal+=a;
	}	
	
	
	public int craft()
	{
	int s=craft;
	s+=owner.inl();
		
	int a=Item.howMany(owner.getItems(),"Ring of Crafting");
	if(a>4)
	a=4;
	s+=a;
	
	a=Item.howMany(owner.getItems(),"Artisian Tools");
	if(a>0)
	s+=2;
	
	return s;	
	}
	
	
	public int heal()
	{
	int s=heal;
	s+=owner.wis();
		
	int a=Item.howMany(owner.getItems(),"Healer's Kit");
	if(a>0)	
	s+=2;
		
	return s;	
	}
	
	
	
	
	public int intuit()
	{
	int s=dir;
	s+=owner.wis();
		
	int a=Item.howMany(owner.getItems(),"Intuition");
	if(a>4)
	a=4;
	s+=a;
		
	return s;	
	}
	

	
	public int hide()
	{
	int s=hide;
	s+=owner.dex();
	
	if(owner.getArmor()!=null)
	s+=owner.getArmor().getPen();
	if(owner.getShield()!=null)
	s+=owner.getShield().getPen();
	
	int a=Item.howMany(owner.getItems(),"Ring of Stealth");
	a+=Item.howMany(owner.getItems(),"Ring of Darkness");
	if(a>5)
	a=5;
	s+=a;
		
	return s;	
	}
	
	
	public int spot() //if torch+(fire making) - cave? etc
	{
	int s=spot;
	s+=owner.wis();
	
		
	int a=Item.howMany(owner.getItems(),"Ring of Vision");
	if(a>4)
	a=4;
	s+=a;
		
	return s;	
	}
	
		
	public int search()
	{
	int s=search;
	s+=owner.inl();
	
	int a=Item.howMany(owner.getItems(),"Ring of Vision");
	a+=Item.howMany(owner.getItems(),"Ring of Intuition");
	if(a>5)
	a=5;
	s+=a;
		
	return s;	
	}
	
	
	public int pick()
	{
	int s=pick;
	s+=owner.dex();
	
	s+=getPen();
		
	int a=Item.howMany(owner.getItems(),"Ring of Stealth");
	if(a>4)
	a=4;
	s+=a;
		
	return s;	
	}
	
		
	public int perform()
	{
	int s=perf;
	s+=owner.cha();
		
	int a=Item.howMany(owner.getItems(),"Ring of Charm");
	if(a>4)
	a=4;
	s+=a;
		
	return s;	
	}
	
		
	
	public int swim() //change: -1 for every 5 pound? (something like that) + drowning (5 below needed)
	{
	int s=swim;
	s+=owner.str();
		
	int a=Item.howMany(owner.getItems(),"Lumber"); //ring of swimming?
	if(a>0)
	s+=2;
	
	return s;	
	}
	
	
	public int climb()
	{
	int s=climb;
	s+=owner.str();
	
	s+=getPen();
		
	int a=Item.howMany(owner.getItems(),"Climber's Kit"); //ring of ...?
	if(a>0)
	s+=2;
		
	return s;	
	}
	
	
	
	public Character owner()
	{
	return owner;	
	}
	
	//+3,-3
	//+1,-6
	/**
	*Returns the penalty a character has from the weight he is carrying
	*/
	public int getPen() //weight penalty and more
	{
	double w=0;
	Vector it=owner.getItems();
	for(int i=0;i<it.size();i++)
	{
	Item item=(Item)(it.elementAt(i));
	w+=item.getWeight();
	}
	
	int s=owner.getSTR()+1;
	int m=s*3;
	int h=s*6;
		
	int pen=0;
	
	if(w>m&&w<=h)
	pen=-3;
	if(w>h)
	pen=-6;
	
	
	int p=0;
	if(owner.getArmor()!=null)
	p+=owner.getArmor().getPen();
	if(owner.getShield()!=null)
	p+=owner.getShield().getPen();
	
	if(p>pen)
	pen=p;
		
	return pen;
	}
	
	
	/**
	*Returns a string vector that specifies the skills
	*/
	public Vector getVector()
	{
	Vector v=new Vector();	
	
	v.add("Swim: "+swim());
	v.add("Climb: "+climb());
	v.add("Perform: "+perform());
	v.add("Pick Pocket: "+pick());
	v.add("Search: "+search());
	v.add("Spot: "+spot());
	v.add("Hide (sneak): "+hide());
	v.add("Intuit Direction: "+intuit());
	v.add("Heal: "+heal());
	v.add("Craft: "+craft());	
		
	return v;
	}
	
	
	
	public String getSkill(int d)
	{
	if(d==1)
	return "Climb";
	if(d==2)
	return "Perform";
	if(d==3)
	return "Pick Pocket";
	if(d==4)
	return "Search";
	if(d==5)
	return "Spot";
	if(d==6)
	return "Hide";
	if(d==7)
	return "Intuit Direction";
	if(d==8)
	return "Heal";
	if(d==9)
	return "Craft";
	
	return "Swim"; //d=0
	}
	
	
	/**
	*Returns the result of a skill check
	*@param n : the skill
	*@param c : the character the skill affects
	*/
	public boolean check(String n,Char c)
	{
	int d=Dice.roll(20),dc=0;;
	int dd=d;
	
	if(!n.equals("Spot"))
	if(owner.getClas().equals("Rogue"))
	if(owner.lvl()>=10)
	{
	int bon=10+owner.inl();
	int op=JOptionPane.showConfirmDialog(Main.main,"You have the Skill Mastery ability.\nIt enables you to get in your die roll 10+intelligence modifier ("+bon+") instead of the normal roll (1 to 20).\nDo you want to use it?","Skill Mastery?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);		
		if(op==JOptionPane.YES_OPTION)	
		d=bon;
	}
	
	
	if(n.equals("Spot"))
	{
	dd+=spot();
	
	if(owner instanceof Character)
	dd=dd+((Character)(owner)).rangerBon(c);
	
	if(c.male()!=owner.male())
		if(c.getCON()>owner.getCON())
		dd++;
	
		if(c instanceof Enemy)
		dc=((Enemy)(c)).getHiding();
		else
			if(c instanceof Character)
			{
			dc=((Character)(c)).getHiding();
			////System.out.println("Character!!!1!!1!! zoidberg.");
			}
			
	if(dc==0)
	dc=Dice.roll(10)+Dice.roll(5);
	
	}
	
	
	if(n.equals("Search"))
	{
	dd+=search();
	
	Place p=owner.getPlace();
	
	dc=p.dc();	
	}
	
	
	if(n.equals("Hide"))
	{
	dd+=hide();
	if(dd<1)
	dd=1;
	owner.setHiding(dd);
	}
	
	
	if(n.equals("Heal"))
	{
	dd+=heal();
	if(c.male()!=owner.male())
		if(c.getCON()>owner.getCON())
		dd++;
	dc=15;
	//if it's poison? then use the Char (c).
	}
	
	
	if((dd>=dc&&d!=1)||d==20)
	return true;
		
	return false;	
		
	}
	
	
	/**
	*Returns the result of a skill check
	*@param n : the skill
	*@param dc : the difficulty class to beat
	*/
	public boolean check(String n,int dc)
	{
	int d=Dice.roll(20);
	int dd=d;
	
	
	if(owner.getClas().equals("Rogue"))
	if(owner.lvl()>=10)
	{
	int bon=10+owner.inl();
	int op=JOptionPane.showConfirmDialog(Main.main,"You have the Skill Mastery ability.\nIt enables you to get in your die roll 10+intelligence modifier ("+bon+") instead of the normal roll (1 to 20).\nDo you want to use it?","Skill Mastery?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);		
		if(op==JOptionPane.YES_OPTION)	
		d=bon;
	}
	
	
	if(n.equals("Craft"))
	{
	dd+=craft();
	if(dd<1)
	dd=1;
	}
	
	if(n.equals("Pick Pocket"))
	{
	dd+=pick();
	if(dd<1)
	dd=1;
	}
	
	if(n.equals("Swim"))
	{
	dd+=swim();
	if(dd<1)
	dd=1;	
	}
	
	if(n.equals("Climb"))
	{
	dd+=climb();
	if(dd<1)
	dd=1;	
	}
	
	if(n.equals("Intuit Direction"))
	{
	dd+=intuit();
	if(dd<1)
	dd=1;	
	}
	
	if(n.equals("Perform"))
	{
	dd+=perform();
	if(dd<1)
	dd=1;	
	}
	
	
	if((dd>=dc&&d!=1)||d==20)
	return true;
		
	return false;	
	}
	
	/**
	*Adds a number to a skill
	*@param n : the skill
	*@param a : the number
	*/
	public void add(String s,int a)
	{
		if(s.equals("Spot"))
		addSpot(a);
		
		if(s.equals("Climb"))
		addClimb(a);
		
		if(s.equals("Swim"))
		addSwim(a);
		
		if(s.equals("Intuit Direction"))
		addDir(a);
		
		if(s.equals("Hide"))
		addHide(a);
		
		if(s.equals("Search"))
		addSearch(a);
				
		if(s.equals("Craft"))
		addCraft(a);
		
		if(s.equals("Pick Pocket"))
		addPick(a);
		
		if(s.equals("Heal"))
		addHeal(a);
		
		if(s.equals("Perform"))
		addPerf(a);

	}
	
	
	
	/**
	*Connverts the skills to a String (coded)
	*/
	public String write() //owner
	{
	String s="K*";
	s+=swim+",";	
	s+=climb+",";
	s+=perf+",";
	s+=pick+",";
	s+=search+",";
	s+=spot+",";
	s+=hide+",";
	s+=dir+",";
	s+=heal+",";
	s+=craft+",";
	
	return s+"*K";
	}
	
	/**
	*Returns skills by a coded String
	*@param s : the coded String
	*/
	public static Skills read(String s)
	{
	Skills d=new Skills();
	
	////System.out.println("Skills: "+s);
	
	s=s.substring(2);
		
	int b=s.indexOf(",");
	d.swim=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.climb=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.perf=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.pick=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.search=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.spot=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hide=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.dir=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.heal=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.craft=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
				
	
	return d;
	}
	
	
}
//not allowed to copy\paste!