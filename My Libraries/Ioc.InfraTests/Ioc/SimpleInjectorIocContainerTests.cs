﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ioc.Infra.Ioc;
using Ioc.InfraTests.TestObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ioc.Infra.Ioc.Tests
{
    [TestClass()]
    public class MefFactoryTests
    {
        [TestMethod()]
        public void RegisterResolve_RegisterInterfaceWithImplementationType_ResolveReturnsImplementation()
        {
            var iocContainer = CreateBasicContainer();

            iocContainer.Register<ISimpleInterface, SimpleImpl>();
            var impl = iocContainer.Resolve<ISimpleInterface>();

            Assert.AreEqual(impl.GetType(), typeof(SimpleImpl));
            Assert.AreEqual("1".GetHashCode(), impl.GetNumber("1"));
        }

        [TestMethod()]
        public void RegisterResolve_RegisterInterfaceWithImplementationTypeAsSingleton_ResolveReturnsSameInstance()
        {
            var iocContainer = CreateBasicContainer();

            iocContainer.Register<ISimpleInterface, SimpleImpl>(Lifecycle.Singleton);
            var impl = iocContainer.Resolve<ISimpleInterface>();
            var impl2 = iocContainer.Resolve<ISimpleInterface>();

            Assert.AreEqual(impl.ID, impl2.ID);
        }

        [TestMethod()]
        public void RegisterResolve_RegisterInterfaceWithImplementationTypeAsTransient_ResolveReturnsDifferentInstance()
        {
            var iocContainer = CreateBasicContainer();

            iocContainer.Register<ISimpleInterface, SimpleImpl>(Lifecycle.Transient);
            var impl = iocContainer.Resolve<ISimpleInterface>();
            var impl2 = iocContainer.Resolve<ISimpleInterface>();

            Assert.AreNotEqual(impl.ID, impl2.ID);
        }

        [TestMethod()]
        public void RegisterResolve_RegisterInstance_ResolveSameInstance()
        {
            var iocContainer = CreateBasicContainer();
            var impl = new SimpleImpl();

            iocContainer.Register<ISimpleInterface>(impl);
            
            var result = iocContainer.Resolve<ISimpleInterface>();
            Assert.AreEqual(impl, result);
        }

        [TestMethod()]
        public void RegisterResolve_RegisterFunc_ResolveSameInstance()
        {
            var iocContainer = CreateBasicContainer();
            SimpleImpl impl = new SimpleImpl();

            iocContainer.Register<ISimpleInterface>(() => (impl = new SimpleImpl()));

            var result = iocContainer.Resolve<ISimpleInterface>();
            Assert.AreEqual(impl, result);
        }

        [TestMethod()]
        public void Dispose_RegisterInstance_DisposeCalled()
        {
            ISimpleInterface impl;
            using (var iocContainer = CreateBasicContainer())
            {
                iocContainer.Register<ISimpleInterface, SimpleImpl>(Lifecycle.Singleton);
                impl = iocContainer.Resolve<ISimpleInterface>();
            }

            Assert.IsTrue(impl.DisposeCalled);
        }

        [TestMethod()]
        [ExpectedException(typeof(SimpleInjector.ActivationException))]
        public void Resolve_TypeNotRegistered_ThrowError()
        {
            ISimpleInterface impl;
            var iocContainer = CreateBasicContainer();
            iocContainer.Resolve<ISimpleInterface>();
        }

        private IIocContainer CreateBasicContainer()
        {
            var ioc = new SimpleInjectorIocContainer();
            return ioc;
        }
    }
}
