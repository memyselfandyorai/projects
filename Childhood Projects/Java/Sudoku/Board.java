
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;




//�� �� ����� ����, ������ ���� ��� �����, ��� �� ��� ������. ���'.

public class Board extends JPanel implements ActionListener,MouseListener{
	
	
	static final int size=700;
	static final int sq=9;
	//static boolean minimum=true;
	//check texts
	private JTextField bor[][];
	
	private JButton com,res,fil,list;
	
	private JTextField ri,le;
	
	private JCheckBox box;
	
	private JTextField sc;
	private Num num[][];
	
	public Board()
	{
	super();
	
	bor=new JTextField[sq][sq];
	num=new Num[sq][sq];
	

	
	setLayout(new GridLayout(0,sq,5,5));
	
	for(int i=0;i<sq;i++)
	{
	for(int j=0;j<sq;j++)
	{
	bor[i][j]=new JTextField("");
	bor[i][j].setFont(new Font("",0,30));
	
	//bor[i][j].addActionListener(this);
	
	//bor[i][j].setDragEnabled(true);
	//if((j+1)%3==0)
	//bor[i][j].setText("fg");
	
	//bor[i][j].setSize(1,1);
	bor[i][j].addMouseListener(this);
	add(bor[i][j]);
	
	num[i][j]=new Num();
	
	}
	}
	
	
	

	
	
	com=new JButton("Start");
	com.addActionListener(this);
	add(com);
	
	res=new JButton("Reset");
	res.addActionListener(this);
	add(res);
	
	fil=new JButton("Fill");
	fil.addActionListener(this);
	add(fil);
	
	ri=new JTextField();
	add(ri);
	le=new JTextField();
	add(le);
	
	list=new JButton("List");
	list.addActionListener(this);
	list.setEnabled(false);
	add(list);
	
	
	sc=new JTextField();
	sc.setColumns(4);
	add(sc);
	
	box=new JCheckBox("Min 18",true);
	//box.addMouseListener(this);
	add(box);
	//sc.setText("dsfgsd \n fgfdfgdfgdg");
	//System.out.println(sc.getText());
	/*Num n=new Num();
	n.add(3);
	n.add(5);
	n.add(6);
	n.print();
	n.remove(5);
	n.print();
	*/
	
	
		
	}
	
	
	public void paintComponent(Graphics g)
	{
	super.paintComponent(g);
	
	g.fillRect(226,-3,5,600);
	g.fillRect(457,-3,5,600);
	
	g.fillRect(-3,195,695,5);
	g.fillRect(-3,396,695,5);
	
	
	//g.drawOval(40,40,40,40);
	
	}
	
	
	public Dimension getPreferredSize()
	{
	return new Dimension(size,size);	
	}
	
	
	
	public void mouseClicked(MouseEvent e)
	{
		//JCheckBox b=(JCheckBox)(e.getSource());
		
	}
	
	public void mousePressed(MouseEvent e)
	{
		JTextField t=(JTextField)(e.getSource());
		
		for(int i=0;i<sq;i++)
		for(int j=0;j<sq;j++)
		if(bor[i][j]==t)
		{
		Ma.options(num[i][j],sc);		
		}
	}
	
	public void mouseReleased(MouseEvent e)
	{
		
	}
	
	
	public void mouseEntered(MouseEvent e)
	{
		
	}
	
	
	public void mouseExited(MouseEvent e)
	{
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
	//System.out.println("Action:"+e.getActionCommand());
	//bor[0][0].postActionEvent();
	
	
	if(((JButton)(e.getSource())).getText().equals("Start"))
	{
		//starting to "play".
		int mar[][]=new int[sq][sq];
		
			for(int i=0;i<sq;i++)
			for(int j=0;j<sq;j++)
			{
			if(!(bor[i][j].getText().equals("")))
			mar[i][j]=Ma.in(bor[i][j]);
			//��� �� ���� ���� �-0?
			}
		
		boolean valid=true;
		
		for(int i=0;i<sq;i++)
		for(int j=0;j<sq;j++)
		if(mar[i][j]<0||mar[i][j]>sq)
		{
		System.out.println("Number not valid (should be between 1 to "+sq+" or 0 for empty): row "+(i+1)+" col "+(j+1));
		valid=false;
		}
		
		if(box.isSelected())
		{
		if(Ma.checkAll(mar)&&(81-Ma.left(mar))>=18)
		{
			
			if(valid)		
			mar=Ma.play(mar,num);
			//System.out.println(""+Ma.left(mar));
		}
		else
			if(!Ma.checkAll(mar))
			System.out.println("Numbers not correct.");
			else
			System.out.println("Not enough numbers ("+(81-Ma.left(mar))+"). At least 18 are needed.");
		}
		else
		if(Ma.checkAll(mar))
		{
			if(valid)
			mar=Ma.play(mar,num);
		}
		else
		System.out.println("Numbers not correct.");
		
		
			
		
		/*
		for(int i=0;i<sq;i++)
		{
			for(int j=0;j<sq;j++)
			System.out.print(mar[i][j]+" ");//del
		System.out.println("");//del
		}
		*/
	
		if(Ma.full(mar)&&Ma.checkAll(mar))
		{
		System.out.println("finished:");
		Ma.print(mar);
		}
		else
		System.out.println("impossible to solve.");
	
		for(int i=0;i<sq;i++)
		for(int j=0;j<sq;j++)
		{
		if(mar[i][j]!=0)
		bor[i][j].setText(""+mar[i][j]);
		}
	}
	
	if(((JButton)(e.getSource())).getText().equals("Reset"))
	{
		for(int i=0;i<sq;i++)
		for(int j=0;j<sq;j++)
		bor[i][j].setText("");
	}
		
		
	if(((JButton)(e.getSource())).getText().equals("Fill"))	
	{
	//11 in sodukos- easy.
	bor[0][2].setText("1");	
	bor[0][4].setText("5");
	bor[0][6].setText("2");
	bor[1][1].setText("9");
	bor[1][3].setText("6");
	bor[1][5].setText("2");	
	bor[1][7].setText("7");
	bor[2][1].setText("4");
	bor[2][3].setText("7");
	bor[2][5].setText("8");
	bor[2][7].setText("5");
	bor[3][0].setText("9");
	bor[3][3].setText("1");
	bor[3][5].setText("4");
	bor[3][8].setText("6");
	bor[4][2].setText("6");
	bor[4][4].setText("8");
	bor[5][0].setText("5");
	bor[5][3].setText("9");
	bor[5][5].setText("7");
	bor[5][8].setText("2");
	bor[6][1].setText("5");
	bor[6][3].setText("2");
	bor[6][5].setText("1");	
	bor[6][7].setText("6");
	bor[7][1].setText("6");
	bor[7][3].setText("8");
	bor[7][5].setText("9");	
	bor[7][7].setText("3");
	bor[8][2].setText("9");	
	bor[8][4].setText("7");
	bor[8][6].setText("1");
	

	//8. -hard
	/*bor[0][0].setText("3");
	bor[0][3].setText("1");
	bor[0][4].setText("4");
	bor[0][5].setText("5");
	bor[1][2].setText("1");
	bor[2][0].setText("4");
	bor[2][3].setText("7");
	bor[2][5].setText("8");
	bor[2][7].setText("6");
	bor[3][2].setText("4");
	bor[3][5].setText("9");
	bor[4][0].setText("5");
	bor[4][2].setText("2");
	bor[4][6].setText("6");
	bor[4][8].setText("7");
	bor[5][3].setText("8");
	bor[5][6].setText("4");
	bor[6][1].setText("3");
	bor[6][3].setText("9");
	bor[6][5].setText("7");
	bor[6][8].setText("6");
	bor[7][6].setText("1");
	bor[8][3].setText("3");
	bor[8][4].setText("8");
	bor[8][5].setText("2");
	bor[8][8].setText("9");
	*/
	
	//35. -hard
	/*bor[0][1].setText("8");
	bor[0][5].setText("9");
	bor[0][6].setText("1");
	bor[0][7].setText("5");
	bor[1][0].setText("5");
	bor[1][3].setText("4");
	bor[2][2].setText("7");
	bor[2][4].setText("2");
	bor[2][8].setText("4");
	bor[3][0].setText("7");
	bor[3][6].setText("5");
	bor[4][0].setText("4");
	bor[4][3].setText("2");
	bor[4][5].setText("7");
	bor[4][8].setText("3");
	bor[5][2].setText("1");
	bor[5][8].setText("6");
	bor[6][0].setText("2");
	bor[6][4].setText("3");
	bor[6][6].setText("6");
	bor[7][5].setText("2");
	bor[7][8].setText("5");
	bor[8][1].setText("4");
	bor[8][2].setText("5");
	bor[8][3].setText("9");
	bor[8][7].setText("8");
	*/

	//very hard
	/*
	bor[0][0].setText("1");
	bor[0][5].setText("4");
	bor[0][8].setText("8");
	bor[1][0].setText("9");
	bor[1][3].setText("6");
	bor[1][6].setText("7");
	bor[2][1].setText("5");
	bor[2][5].setText("3");
	bor[2][6].setText("2");
	bor[3][7].setText("7");
	bor[4][2].setText("3");
	bor[4][3].setText("2");
	bor[4][5].setText("5");
	bor[4][6].setText("8");
	bor[5][1].setText("8");
	bor[6][2].setText("6");
	bor[6][3].setText("5");
	bor[6][7].setText("8");
	bor[7][2].setText("7");
	bor[7][5].setText("2");
	bor[7][8].setText("1");
	bor[8][0].setText("4");
	bor[8][3].setText("9");
	bor[8][8].setText("2");
	
	*/
	//my own- impossible?
	/*
	bor[0][0].setText("1");
	bor[0][2].setText("2");
	bor[0][4].setText("7");
	bor[0][6].setText("3");
	bor[0][7].setText("5");
	bor[2][1].setText("3");
	bor[3][3].setText("4");
	bor[3][4].setText("5");
	bor[3][6].setText("6");
	bor[3][7].setText("7");
	bor[6][0].setText("8");
	bor[6][1].setText("9");
	bor[6][3].setText("2");
	bor[6][8].setText("1");
	bor[7][5].setText("3");
	bor[7][7].setText("8");
	bor[8][4].setText("9");
	bor[8][8].setText("6");
	*/
	
	//my own, gets stuck
	/*
	for(int i=0;i<sq;i++)
	bor[4][i].setText((i+1)+"");
	bor[3][3].setText("1");
	bor[3][4].setText("2");
	bor[3][5].setText("3");
	bor[5][3].setText("7");
	bor[5][4].setText("8");
	bor[5][5].setText("9");
	bor[0][0].setText("7");
	bor[1][0].setText("8");
	bor[2][0].setText("9");
	bor[6][0].setText("4");
	bor[7][0].setText("5");
	bor[8][0].setText("6");
	*/
	
	}
//
//Object
	
	//e.getSource()!! action doesn't work!
	
	if(((JButton)(e.getSource())).getText().equals("List"))
	{
	int i=Ma.in(ri),j=Ma.in(le);
	Ma.options(num[i][j],sc);	
	//System.out.println(i+" "+j);		
	}
	
	/*if(((JTextField)(e.getSource())).getText().equals(""))
	{
	for(int i=0;i<sq;i++)
	for(int j=0;j<sq;j++)
	if(((JTextField)(e.getSource()))==bor[i][j])
	Ma.options(num[i][j],sc);
	}

	System.out.println("fghfg");
	}
	*/
	
	}
	
	public static void main(String[] args) {
		// TODO: Add your code here
		
		JFrame f=new JFrame("Sudoku");
		Board b=new Board();
		
		f.getContentPane().add(b);
		
		
		f.setSize(Board.size,Board.size);
		f.setVisible(true);
		
		
		long a=(long)(255*Math.pow(256,0)+255*Math.pow(256,1)+255*Math.pow(256,2)+255*Math.pow(256,3));
		System.out.println(a);
		
	}	
}
