/*
event - context.params.event
messageContent - list of messages
options - {
  splitMessages - bool
  replyToMessage - bool (takes event.id)
  embed - embed object ({title, type, color, ... })
}
returns a list of messages that contain IDs
*/
async function sendMessage(messageSender, messageContent, options = undefined) {
  let content = messageContent.join("\n");
  let messages = [content];
  console.log(`Sent message: ` + content);
  if (options?.splitMessages) {
    messages = splitToSeparateMessages(messageContent);
  }

  let discordMessages = [];

  let shouldWait = false;
  for (let index = 0; index < messages.length; index++) {
    let msg = messages[index];
    let lastMessageEmbedSummary =
      index < messages.length - 1 ? undefined : options?.embed;
    console.log(
      `Send Message - size: ${msg.length}, summary: ${lastMessageEmbedSummary?.length ?? 0}`,
    );

    let msgObj = { error: "undefined" };

    try {
      if (shouldWait) {
        await sleep(1000);
        shouldWait = false;
      }

      msgObj = { text: msg };
      // MessageOptions
      const sendOptions = {
        reply_to_message_id: options?.replyToMessage
          ? messageSender.getMessageId()
          : undefined,
        embed: lastMessageEmbedSummary,
      };

      const messageId = await messageSender.send(msgObj, sendOptions);
      discordMessages.push({ id: messageId });
    } catch (error) {
      console.error(
        `Failed sending message normally: '${JSON.stringify(msgObj)}'`,
        error,
      );
      if (error.message.includes("You are being rate limited")) {
        shouldWait = true;
        console.log(`trying again`);
        index = index - 1;
        continue;
      }
    }
  }

  return discordMessages;
}

// async function.
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

// Returns lists of strings, each one with max characters
// Reason: do avoid errors of "Must be 2000 or fewer in length."
function splitToSeparateMessages(
  messageContentList,
  joinChar = "\n",
  maxChars = 2000,
) {
  if (messageContentList.length == 0) {
    return [""];
  }
  let result = [];

  let currentMessageContent = messageContentList[0];
  messageContentList.slice(1).forEach((msg) => {
    let newMsg = currentMessageContent + joinChar + msg;
    if (newMsg.length > maxChars) {
      result = result.concat([currentMessageContent]);
      currentMessageContent = msg;
    } else {
      currentMessageContent = newMsg;
    }
  });

  if (currentMessageContent.length > 0) {
    result = result.concat([currentMessageContent]);
  }

  return result;
}

// Should run at the start of every command handling, since token is valid for only 2 seconds:
// https://autocode.com/lib/discord/interactions/#responses-create
async function receivedCommand(commandInteraction) {
  console.log(`Received command: ${commandInteraction}`);
  try {
    await commandInteraction.prepareCommandReply();
  } catch (error) {
    console.error(`Failed preparing command`, error);
    await sendMessage(commandInteraction, [
      "Unexpected error when starting to handle command. Contact developer for more info.",
    ]);
  }
}

/*
event - context.params.event
messageContent - list of messages
void
*/
async function replyToCommand(commandInteraction, messageContent) {
  let content = messageContent.join("\n");
  console.log(`Reply to command: ` + content);

  /*
  if (event.token == "A_UNIQUE_TOKEN") {
    await lib.discord.channels["@0.0.6"].messages.create({
      channel_id: event.channel_id,
      content: "DEBUG MSG: " + content,
    });
    return;
  }
  */

  try {
    await commandInteraction.reply({
      text: content,
    });
  } catch (error) {
    console.error(`Failed sending message to '${commandInteraction}'`, error);
    await sendMessage(commandInteraction, [
      "Unexpected error when replying to command. Contact developer for more info.",
    ]);
  }

  await flushStorage(commandInteraction);
}

async function replyToPrefixCommand(
  prefixCommandInteraction,
  messageContent,
  options = undefined,
) {
  await sendMessage(prefixCommandInteraction, messageContent, options);
  await flushStorage(prefixCommandInteraction);
}

// interactionWithIocContainer or prefixCommandInteraction.
async function flushStorage(interactionWithIocContainer) {
  try {
    await interactionWithIocContainer.getIocContainer().cache.flushState();
  } catch (error) {
    console.error(
      `Failed flushing storage from command '${interactionWithIocContainer}'`,
      error,
    );
    await sendMessage(interactionWithIocContainer, [
      "Unexpected error when trying to save storage state. Some data might have been lost. Contact developer for more info.",
    ]);
  }
}

module.exports = {
  ReceivedCommand: receivedCommand,
  ReplyToCommand: replyToCommand,
  SendMessage: sendMessage,
  ReplyToPrefixCommand: replyToPrefixCommand,
};
