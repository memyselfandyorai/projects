package General;

/**
 * The class represents a normalized vector
 * */
public class NormalVector extends Vector{
	
	
	//problem: normalizing... if negative turns to positive. change normalization?
	
	public NormalVector(double x,double y,double z)
	{
		super(x,y,z);
		normalize();
	}
	
	public NormalVector(Vector vector)
	{
		super(vector);
		normalize();
	}
	
	/**
	 * returns the cross product between normalized vectors 'this' and 'd'
	 * @param d - normalized vector
	 * @return
	 */
	public NormalVector crossProduct(NormalVector d)  
	{
		return new NormalVector(super.crossProduct(d));
	}
	
	
	public Vector crossProductRegular(Vector d)  
	{
		return super.crossProduct(d);
	}
		

	/**
	 * returns a vector that runs between points 'p1' and 'p2'
	 * @param p1 - point 
	 * @param p2 - point
	 * @return
	 */
	public static NormalVector subPoints(Point p1,Point p2) 
	{
		return new NormalVector(p1.getX()-p2.getX(),p1.getY()-p2.getY(),p1.getZ()-p2.getZ());
	}

	/**
	 * returns the opposite vector of 'this'
	 */
	public NormalVector negative() //still normalized
	{
		return new NormalVector(-x,-y,-z);
	}
	
}
