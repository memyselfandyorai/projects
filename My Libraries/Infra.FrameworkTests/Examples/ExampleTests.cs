﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Infra.FrameworkTests.Examples
{
    [TestClass()]
    public class ExampleTests
    {
        [TestMethod()]
        public void Example_RunSlave_NoError()
        {
            new Main().RunProgram(new[] { "SLAVE" });
        }

        [TestMethod()]
        public void Example_RunMaster_NoError()
        {
            new Main().RunProgram(args:null);
        }
    }
}
