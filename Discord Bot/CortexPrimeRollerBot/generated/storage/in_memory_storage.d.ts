import type { BasicDataStorage } from "./base_storage";
import type { StorageItem } from "../api/storage";
export declare class InMemoryDatabase implements BasicDataStorage {
    cacheMap: Map<string, StorageItem>;
    constructor();
    setItem(key: string, value: StorageItem): Promise<void>;
    getAllItemKeys(): Promise<string[]>;
    getItem(key: string): Promise<StorageItem | null | undefined>;
    deleteItem(key: string): Promise<void>;
}
//# sourceMappingURL=in_memory_storage.d.ts.map