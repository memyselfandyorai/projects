
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

module.exports = {
  CacheValue: cacheValue,
  GetCacheValue: getCacheValue,
};

async function cacheValue(key, value, ttlSeconds = 600) {
  await lib.utils.kv['@0.1.16'].set({
    key: key,
    value: value,
    ttl: ttlSeconds 
  });
  
  console.log(`Caching key: ` + key);
  console.log(`Caching value: ` + JSON.stringify(value));
}

async function getCacheValue(key, defaultValue = undefined) {
  let value = await lib.utils.kv['@0.1.16'].get({
    key: key,
    defaultValue: defaultValue 
  });
  console.log(`Getting key: ` + key);
  console.log(`Got value: ` + JSON.stringify(value));
  
  return value
}