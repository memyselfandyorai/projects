const saver = require("../saver.js");
const controller = require("../controller.js");
const { TestCase, createIocContainer } = require("./common.js");
const {
  GUILD_ID,
  USER_ID,
  SAVE_NAME,
  SINGLE_SAVE_NAME,
  PREPARE_ACTION_GET_FOR_USER,
  PREPARE_ACTION_DELETE_SCENE,
  PREPARE_ACTION_DELETE_ALL,
  PREPARE_ACTION_DELETE_SINGLE,
  OTHER_GUILD_ID,
  OTHER_USER_ID,
  OPTED_OUT_SAVE_NAME,
  PREPARE_ACTION_WILDCARD_OPTED_OUT,
  prepareConstantSaveDices,
} = require("./constantRolls.js");

module.exports = {
  TestCases: createTestCases(),
};

function createTestCases() {
  return [
    new TestCase({
      name: "testControllerSaveDice_missingName_badMessage",
      functionToRun: testSaveDice,
      inputArgs: {
        name: "",
        action: saver.SCENE,
        dice: "6 6",
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [`No input name. Did not save anything.`],
    }),
    new TestCase({
      name: "testControllerSaveDice_invalidNameSeparator_badMessage",
      functionToRun: testSaveDice,
      inputArgs: {
        name: `fdsfs${controller.SAVE_NAME_SEPARATOR}gfdgd`,
        action: saver.SCENE,
        dice: "6 6",
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `Invalid name 'fdsfs${controller.SAVE_NAME_SEPARATOR}gfdgd': must not contain '${controller.SAVE_NAME_SEPARATOR}' or '\\${controller.SAVE_NAME_ALL_WILCARD}'.`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_invalidNameWildcard_badMessage",
      functionToRun: testSaveDice,
      inputArgs: {
        name: `${controller.SAVE_NAME_ALL_WILCARD} avdfv`,
        action: saver.SCENE,
        dice: "6 6",
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `Invalid name '\\${controller.SAVE_NAME_ALL_WILCARD} avdfv': must not contain '${controller.SAVE_NAME_SEPARATOR}' or '\\${controller.SAVE_NAME_ALL_WILCARD}'.`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_missingDice_badMessage",
      functionToRun: testSaveDice,
      inputArgs: {
        name: SAVE_NAME,
        action: saver.SCENE,
        dice: "",
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [`Input dice was empty. Did not save anything.`],
    }),
    new TestCase({
      name: "testControllerSaveDice_scene_good",
      functionToRun: testSaveDice,
      inputArgs: {
        name: SAVE_NAME,
        action: saver.SCENE,
        dice: "6 6",
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [`Saved '${SAVE_NAME}' successfully as '2D6'.`],
    }),
    new TestCase({
      name: "testControllerSaveDice_singleD6Annotation_good",
      functionToRun: testSaveDice,
      inputArgs: {
        name: SAVE_NAME,
        action: saver.SINGLE,
        dice: "2D8",
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [`Saved '${SAVE_NAME}' successfully as '2D8'.`],
    }),
    new TestCase({
      name: "testControllerSaveDice_show_nothing",
      functionToRun: testSaveDice,
      inputArgs: {
        action: saver.SHOW,
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [`No saved results.`],
    }),
    new TestCase({
      name: "testControllerSaveDice_showFresh_all",
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
      },
      expectedResult: [
        `*${SAVE_NAME}* (scene): 1D6`,
        `*${SINGLE_SAVE_NAME}* (single/round): 2D8`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_showAfterDeletion_showNone",
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_DELETE_ALL],
      },
      expectedResult: [`No saved results.`],
    }),
    new TestCase({
      name: "testControllerSaveDice_showAfterUsage_allWithCaveat",
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_GET_FOR_USER],
      },
      expectedResult: [
        `*${SAVE_NAME}* (scene): 1D6`,
        `*${SINGLE_SAVE_NAME}* (single/round): 2D8 - **already used by user**`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_showAfterSceneDeletion_showSome",
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_DELETE_SCENE],
      },
      expectedResult: [`*${SINGLE_SAVE_NAME}* (single/round): 2D8`],
    }),
    new TestCase({
      name: "testControllerSaveDice_showAfterSingleDeletion_showSome",
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_DELETE_SINGLE],
      },
      expectedResult: [`*${SAVE_NAME}* (scene): 1D6`],
    }),
    new TestCase({
      name: "testControllerSaveDice_showAfterUsageDifferentUser_allWithoutCaveat",
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: OTHER_USER_ID,
        actions: [PREPARE_ACTION_GET_FOR_USER],
      },
      expectedResult: [
        `*${SAVE_NAME}* (scene): 1D6`,
        `*${SINGLE_SAVE_NAME}* (single/round): 2D8`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_showOtherGuild_showNone",
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: OTHER_GUILD_ID,
        user_id: USER_ID,
        actions: [],
      },
      expectedResult: [`No saved results.`],
    }),
    new TestCase({
      name: "testControllerSaveDice_showAfterDeletionsOfOtherName_all",
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
      },
      expectedResult: [
        `*${SAVE_NAME}* (scene): 1D6`,
        `*${SINGLE_SAVE_NAME}* (single/round): 2D8`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_deleteEmptyString_invalidInput",
      functionToRun: testDeleteSavedDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
        name: "",
      },
      expectedResult: [
        `No input name. Did not delete anything.`,
        `*${SAVE_NAME}* (scene): 1D6`,
        `*${SINGLE_SAVE_NAME}* (single/round): 2D8`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_deleteMultipleInputs_deleted",
      functionToRun: testDeleteSavedDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
        name: `${SAVE_NAME}  ${controller.SAVE_NAME_SEPARATOR} ${SINGLE_SAVE_NAME}`,
      },
      expectedResult: [
        `Deleted 2 saved results out of '${SAVE_NAME}', '${SINGLE_SAVE_NAME}'.`,
        `No saved results.`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_deleteAll_deleted",
      functionToRun: testDeleteSavedDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
        name: `${controller.SAVE_NAME_ALL_WILCARD}`,
      },
      expectedResult: [
        `Deleted 2 saved results out of all.`,
        `No saved results.`,
      ],
    }),
    new TestCase({
      name: "testControllerSaveDice_deleteAllAndThenShowWithDeleteDelays_deleted",
      functionToRun: testDeleteSavedDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
        name: `${controller.SAVE_NAME_ALL_WILCARD}`,
        delayMap: {
          cacheValue: [10, 10],
          clearCacheKey: [0, 20, 100],
        },
      },
      expectedResult: [
        `Deleted 2 saved results out of all.`,
        `No saved results.`,
      ],
    }),

    new TestCase({
      name: "testGetSaveForUser_otherGuild_nothingFound",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: OTHER_GUILD_ID,
        user_id: USER_ID,
        opt_ins: saver.ALL_NAMES,
        opt_outs: [],
        actions: [],
      },
      expectedResult: {
        additionalPool: [],
        messageContent: [],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_userAlreadyGot_noAdditionalPool",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: saver.ALL_NAMES,
        opt_outs: [],
        actions: [PREPARE_ACTION_GET_FOR_USER],
      },
      expectedResult: {
        additionalPool: [6],
        messageContent: [`Bonus dice due to '${SAVE_NAME}': 1D6`],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_user_getAllBonuses",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: saver.ALL_NAMES,
        opt_outs: [],
        actions: [],
      },
      expectedResult: {
        additionalPool: [6, 8, 8],
        messageContent: [
          `Bonus dice due to '${SAVE_NAME}': 1D6`,
          `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        ],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_optInOnlySingle_getSingleBonus",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: [SINGLE_SAVE_NAME],
        opt_outs: [],
        actions: [],
      },
      expectedResult: {
        additionalPool: [8, 8],
        messageContent: [`Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_optInList_getAllByCacheOrder",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: [SINGLE_SAVE_NAME, SAVE_NAME],
        opt_outs: [],
        actions: [],
      },
      expectedResult: {
        additionalPool: [6, 8, 8],
        messageContent: [
          `Bonus dice due to '${SAVE_NAME}': 1D6`,
          `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        ],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_optOutInList_getOne",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: [SINGLE_SAVE_NAME, SAVE_NAME],
        opt_outs: [SAVE_NAME],
        actions: [],
      },
      expectedResult: {
        additionalPool: [8, 8],
        messageContent: [`Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_optOutAll_getOne",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: saver.ALL_NAMES,
        opt_outs: saver.ALL_NAMES,
        actions: [],
      },
      expectedResult: {
        additionalPool: [],
        messageContent: [],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_withWildcardOptOutMode_getAllBesidesOptOut",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: saver.ALL_NAMES,
        opt_outs: [],
        actions: [PREPARE_ACTION_WILDCARD_OPTED_OUT],
      },
      expectedResult: {
        additionalPool: [6, 8, 8],
        messageContent: [
          `Bonus dice due to '${SAVE_NAME}': 1D6`,
          `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        ],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_withWildcardOptOutModeAskedSpecifically_getOnlyOptedOut",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: [OPTED_OUT_SAVE_NAME],
        opt_outs: [],
        actions: [PREPARE_ACTION_WILDCARD_OPTED_OUT],
      },
      expectedResult: {
        additionalPool: [10],
        messageContent: [`Bonus dice due to '${OPTED_OUT_SAVE_NAME}': 1D10`],
      },
    }),
    new TestCase({
      name: "testGetSaveForUser_withWildcardOptOutModeOptedOut_getNone",
      functionToRun: testGetSaveForUser,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        opt_ins: [OPTED_OUT_SAVE_NAME],
        opt_outs: [OPTED_OUT_SAVE_NAME],
        actions: [PREPARE_ACTION_WILDCARD_OPTED_OUT],
      },
      expectedResult: {
        additionalPool: [],
        messageContent: [],
      },
    }),
  ];
}

/*
  input: {
      name,
      action,
      dice,  - as string
      guild_id, 
      user_id,
    }    
  
  output: list of messages
*/
async function testSaveDice(input) {
  const saverInput = {
    dice: input.dice,
    action: input.action,
    name: input.name,
    guild_id: input.guild_id,
    user_id: input.user_id,
  };

  let iocContainer = createIocContainer([]);

  let result = await controller.SaveDice(saverInput, iocContainer);

  return result;
}

/*
    input: {
        guild_id, 
        user_id,
        actions - list of prepare actions,
      }    
    
    output: list of messages
  */
async function testShowDice(input) {
  let iocContainer = createIocContainer([]);
  let prepareInput = {
    actions: input.actions ?? [],
  };
  await prepareConstantSaveDices(prepareInput, iocContainer);

  const saverInput = {
    action: saver.SHOW,
    guild_id: input.guild_id,
    user_id: input.user_id,
  };

  let showResult = await controller.SaveDice(saverInput, iocContainer);
  return showResult;
}

/*
    input: {
        guild_id, 
        user_id,
        actions - list of prepare actions,
        name - comma separated or string (SAVE_NAME_SEPARATOR, SAVE_NAME_ALL_WILCARD)
        delayMap - map of cache delays in ms
      }    
    
    output: list of messages
  */
async function testDeleteSavedDice(input) {
  let options = {
    delayMap: input.delayMap,
  };
  let iocContainer = createIocContainer([], options);
  let prepareInput = {
    actions: input.actions ?? [],
  };
  await prepareConstantSaveDices(prepareInput, iocContainer);

  const deleteInput = {
    action: saver.DELETE,
    name: input.name,
    guild_id: input.guild_id,
    user_id: input.user_id,
  };

  let messageContent = await controller.SaveDice(deleteInput, iocContainer);

  const showInput = {
    action: saver.SHOW,
    guild_id: input.guild_id,
    user_id: input.user_id,
  };

  messageContent = messageContent.concat(
    await controller.SaveDice(showInput, iocContainer),
  );

  return messageContent;
}

/*
input: {
  guild_id,
  user_id,
  opt_outs: list of save names to opt out or ALL_NAMES for all,
  opt_ins: list of save names to opt in to or ALL_NAMES for all,
  actions - list of prepare actions,
}
output: {
  additionalPool,
  messageContent,
}
*/
async function testGetSaveForUser(input) {
  let iocContainer = createIocContainer([]);
  let prepareInput = {
    actions: input.actions ?? [],
  };
  await prepareConstantSaveDices(prepareInput, iocContainer);

  const getInput = {
    saveOptIns: input.opt_ins,
    saveOptOuts: input.opt_outs,
    guildId: input.guild_id,
    userId: input.user_id,
  };

  let result = await saver.GetSaveForUser(getInput, iocContainer);
  return result;
}
