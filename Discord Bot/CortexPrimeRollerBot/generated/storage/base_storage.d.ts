import type { StorageItem, DataStorage } from "../api/storage";
import type { Client } from "discord.js";
export interface BasicDataStorage {
    setItem(key: string, value: StorageItem): Promise<void>;
    getItem(key: string): Promise<StorageItem | null | undefined>;
    getAllItemKeys(): Promise<string[]>;
    deleteItem(key: string): Promise<void>;
}
export interface FlushableDataStorage extends BasicDataStorage {
    flush(): Promise<void>;
}
export interface StorageInitParams {
    channelName: string;
    serverId: string;
    discordBotClient: Client;
}
export declare class BaseDatabase implements DataStorage {
    undelyingStorage: BasicDataStorage;
    name: string;
    constructor(name: string, undelyingStorage: BasicDataStorage);
    cacheValue(key: string, value: any, ttlSeconds?: number): Promise<void>;
    getCacheValue(key: string, defaultValue?: any): Promise<any>;
    getCacheEntries(): Promise<StorageItem[]>;
    clearCacheKey(key: string): Promise<any>;
    /** Returns the storage item or undefined if non existent or expired. If item is expired, deletes it. */
    getCachedItem(key: string): Promise<StorageItem | undefined>;
    clearCachedItem(item: StorageItem): Promise<any>;
    flushState(): Promise<void>;
    isExpired(item: StorageItem): boolean;
}
//# sourceMappingURL=base_storage.d.ts.map