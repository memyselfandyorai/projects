const rollerTester= require('../../../../../../common/tester.js');
const {SendMessage}= require('../../../../../../common/commands.js');


let params = context.params.event.content.split(' ')
console.log(`params: ` +params.join(", "));

let inputOptions = {}
if(params[1]) {
  inputOptions.namePrefix = params[1]
}

let testResults = await rollerTester.RunTests(inputOptions)

let messageContent = testResults.messages
let totalSuccess = testResults.totalSuccess
let totalFail= testResults.totalFail

let summaryMessage = `${totalSuccess}/${totalSuccess + totalFail} succeeded`

let color = totalFail == 0? 0x00AA00 : 0xFF0000	 // Green / red color
let summary = {
  title: summaryMessage,
  type: 'rich',
  color: color, 
}

let options = {
  splitMessages: true,
  replyToMessage: true,
  embed: summary,
}
await SendMessage(context.params.event, messageContent, options)

console.log(`Summary: ` + summaryMessage);