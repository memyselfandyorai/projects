const roller = require('../../../../common/roller.js');
const cache = require('../../../../common/cache.js');
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

// TBD:
// 2. support the "keep" comment
// 3. Try to "store" things temporarily (or find previous message results, maybe add readable metadata), and perform "keep" on actual rolls

//console.log(JSON.stringify(context.params.event, null, 2))
/*
"data": {
  "type": 1,
  "options": [
    {
      "value": "10",
      "type": 3,
      "name": "dice"
    }
  ],
  "name": "rollp",
  "id": "951595708358750239",
  "guild_id": "947924911391911997"
},
*/
// "guild_id": "947924911391911997",
// channel_id": "947924911836524545
// "application_id": "947923931615088682",

//const NodeCache = require( "node-cache" );
//const myCache = new NodeCache({stdTTL: 600});





let userId = context.params.event.member.user.id
let dice = context.params.event.data.options[0].value
let keep = context.params.event.data.options[1]?.value
let diceRoll = dice.split(' ').map(x => parseInt(x)).filter(x => !isNaN(x))


let input = { diceRoll: diceRoll, keep: keep }
let result = roller.RollCortexPrime(input)

await cache.CacheValue(userId, { rolls: result.rolls, diceRoll: diceRoll})

let messageContent = result.messageContent


if (context.params.event.token == 'A_UNIQUE_TOKEN') {
  await lib.discord.channels['@0.0.6'].messages.create({
    channel_id: context.params.event.channel_id,
    content: 'DEBUG: ' + messageContent.join('\n') ,
  });
} else {
  await lib.discord.interactions['@1.0.0'].responses.create({
    token: `${context.params.event.token}`,
    response_type: 'DEFERRED_CHANNEL_MESSAGE_WITH_SOURCE',
  });
  
  await lib.discord.interactions['@1.0.0'].followups.create({
    token: `${context.params.event.token}`,
    content: messageContent.join('\n') ,
  });
}
