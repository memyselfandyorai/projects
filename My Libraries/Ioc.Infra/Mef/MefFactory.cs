﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;

namespace Ioc.Infra.Mef
{
    /// <summary>
    /// A base class for factories that return objects by a key.
    /// Implementations should be accessed by interface that is registered to IocContainer as a singleton.
    /// 
    /// Mef - "Managed Extensibility Framework".
    /// </summary>
    /// <typeparam name="TImport">Type that is registered via adding [ExportForMef(...)] to the class definition</typeparam>
    /// <typeparam name="TKey">The key to return each object by</typeparam>
    /// <typeparam name="TOut">The type the factory returns when requested, converted by registrations - ususally the same as TImport</typeparam>
    public abstract class MefFactory<TImport, TKey, TOut> : IDisposable
    {
        [ImportMany()]
        private IEnumerable<TImport> _imports;

        private Dictionary<TKey, TImport> _dictionary;

        /// <summary>
        /// Loads all instances marked by [ExportForMef] or [Export] attributes and separates them by key.
        /// </summary>
        /// <param name="convertSingleton">Function that changes stored singleton. If null, keeps instance as-is</param>
        /// <param name="assembliesPrefixes">All assembly prefixes that are relevant to load from</param>
        protected MefFactory(ConvertStoredSingleton<TImport> convertSingleton = null, params string[] assembliesPrefixes)
        {
            if(assembliesPrefixes == null || assembliesPrefixes.Length == 0)
            {
                assembliesPrefixes = new[] { Assembly.GetCallingAssembly().GetName().Name };
            }

            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                            .Where(assembly => assembliesPrefixes.Any(prefix => assembly.GetName().Name.StartsWith(prefix)));
            var container = new CompositionContainer(new AggregateCatalog(assemblies.Select(assembly => new AssemblyCatalog(assembly))));
            container.ComposeParts(this);
            BuildDictionary(convertSingleton ?? new ConvertStoredSingleton<TImport>(import => import));
        }

        public virtual void Dispose()
        {
            if (typeof(IDisposable).IsAssignableFrom(typeof(TImport)))
            {
                foreach (TImport import in _dictionary.Values)
                {
                    ((IDisposable)import).Dispose();
                }
            }
        }

        private void BuildDictionary(ConvertStoredSingleton<TImport> convertSingleton)
        {
            _dictionary = new Dictionary<TKey, TImport>();
            foreach (TImport import in _imports)
            {
                _dictionary.Add(GetKey(import), convertSingleton(import));
            }
        }

        /// <summary>
        /// Maps an import to its key: Given an instance of TImport, returns the key that defines it.
        ///
        /// When implementing, this defines how to get a key for each object. No need to invoke it.
        /// </summary>
        protected abstract TKey GetKey(TImport import);

        /// <summary>
        /// Maps an import to an out: Converts an instance to its return value.
        /// 
        /// When implementing, this defines what's the return value of each object. This is usually the same as TImport. No need to invoke it.
        /// This could be used to recreate a new object instead of returning a singleton.
        /// 
        /// </summary>
        protected abstract TOut ConvertImport(TImport import);

        /// <summary>
        /// Creates a converted instance value by a given key.
        /// Returns null if missing.
        /// 
        /// When implementing, this defines what's the return value of each object. This is usually the same as TImport. No need to invoke it.
        /// </summary>
        protected TOut Create(TKey key)
        {
            if (_dictionary.ContainsKey(key))
            {
                TImport import = _dictionary[key];
                return ConvertImport(import);
            }
            return default(TOut);
        }

        /// <summary>
        /// Creates and returns all TOut objects.
        /// 
        /// When implementing, this should be used if "all object types" are interesting, regardless of their key.
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<TOut> CreateAll()
        {
            return _dictionary.Values.Select(ConvertImport);
        }

        
        /// <summary>
        /// Helper method that creates a new instance from import's concrete type with the given args.
        /// 
        /// Notice that all concrete types of import should have a constructor with the given amount (and type) of parameters of args.
        /// </summary>
        /// <param name="import">Instance of a specific import</param>
        /// <param name="args">Arguments for imports creation</param>
        /// <returns></returns>
        protected static TImport CreateNewImport(TImport import, params object[] args)
        {
            return (TImport)Activator.CreateInstance(import.GetType(), args);
        }
    }

    /// <summary>
    /// Converts the imported instance to a different instance which will be stored.
    /// If ConvertImport will return the parameter as-is, it means the new instance will be a singleton.
    /// </summary>
    /// <param name="import">The imported instance</param>
    /// <returns>An instance that we be stored for the import's key</returns>
    public delegate TImport ConvertStoredSingleton<TImport>(TImport import);
}