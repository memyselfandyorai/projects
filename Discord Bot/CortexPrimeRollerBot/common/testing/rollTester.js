const roller = require('../roller.js');
const controller = require('../controller.js');
const {TestCase, createIocContainer, throwIfNotEqual}= require('./common.js');
const {
  prepareConstantSaveDices, 
  CONSTANT_ROLL_CACHE_KEY, 
  SAVE_NAME, SINGLE_SAVE_NAME, PREPARE_ACTION_GET_FOR_USER, PREPARE_ACTION_DELETE_SCENE,
  OTHER_GUILD_ID, OTHER_USER_ID,
  OPTED_OUT_SAVE_NAME, PREPARE_ACTION_WILDCARD_OPTED_OUT,
  prepareConstantPools,
  POOL_NAME, SECOND_POOL_NAME, NON_POOL_NAME,  
  GUILD_ID, USER_ID } = require('./constantRolls.js');

    
module.exports = {
  TestCases: createTestCases(),
};


function createTestCases() {
  return [
    new TestCase({
      // Special input and rolls that resulted in a missing score-effect option.
      name: 'testSpecialInput_with3ScoreResults',
      functionToRun: testRollerRollCortexPrime,
      inputArgs: {
        dice: '4D4 2D6 1D8 1D12',
        keep: 3,
        effectModifier: 1,
        hitch: 2,
        actualRollResults: [1, 1, 1, 2, 4, 5, 7, 11],        
      },
      expectedResult: [
        'Rolling: 4D4 2D6 1D8 1D12',
        'D4 : **(1)** **(1)** **(1)** **(2)**',
        'D6 : 4 5',
        'D8 : 7',
        'D12 : 11',
        'Results ordered by best total:',
        'Score: 23 (5+7+11) with Increased-Effect: D8',
        'Score: 20 (4+5+11) with Increased-Effect: D10',
        'Score: 16 (4+5+7) with Increased-Effect: D12+',
      ],    
    }),  
    new TestCase({
      name: 'testRollerRollCortexPrime_nonPoolDice_notChosenForEffect',
      functionToRun: testRollerRollCortexPrime,
      inputArgs: {
        dice: '3D6',
        nonPoolDice: '12',
        actualRollResults: [5, 5, 5, 7],        
      },
      expectedResult: [
        'Rolling: 3D6',
        'Rolling (dice pool unusable for hitch or effect): 1D12',
        'D6 : 5 5 5',
        'D12 : [7]',
        'Results ordered by best total:',
        'Score: 12 (5+7) with Effect: D6',        
      ],    
    }),          
    new TestCase({
      name: 'testRollerRollCortexPrime_nonPoolDice_notChosenForHitch',
      functionToRun: testRollerRollCortexPrime,
      inputArgs: {
        dice: '2D6',
        nonPoolDice: '4',
        actualRollResults: [5, 5, 1],        
      },
      expectedResult: [
        'Rolling: 2D6',
        'Rolling (dice pool unusable for hitch or effect): 1D4',
        'D4 : [1]',
        'D6 : 5 5',        
        'Results ordered by best total:',
        'Score: 10 (5+5) with Effect: D4',    
        'Score: 6 (1+5) with Effect: D6',        
      ],    
    }),     
    
    new TestCase({
      name: 'testControllerRollCortexPrime_onlyDice_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        /*
        keep: 2,
        effectModifier: 0,
        dieModifier: 0,
        hitch,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 9 (4+5) with Effect: D8',
        'Score: 8 (4+4) with Effect: D10',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_badFormat_failMessage',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2Dx',
        cache_key: '',
        actualRollResults: [1, 1, 3],
      },
      expectedError: {
        message: `Wrong format: '2Dx'`,
        type: roller.InvalidDiceFormatError.prototype,
      },      
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_badFormatSecond_failMessage',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 -',
        cache_key: '',
        actualRollResults: [1, 1],
      },
      expectedError: {
        message: `Wrong format: '-'`,
        type: roller.InvalidDiceFormatError.prototype,
      },      
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep4_goodMessagesMinimalEffect',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 4,
        /*
        effectModifier: 0,
        dieModifier: 0,
        hitch,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 16 (4+3+4+5) with Effect: D4',
      ],
    }),
    new TestCase({
    name: 'testControllerRollCortexPrime_noValidScores_goodMessagesMinimalScoreEffect',
    functionToRun: testControllerRollCortexPrime,
    inputArgs: {
      dice: '3',
      cache_key: '',
      actualRollResults: [1],
    },
    expectedResult: [
        'Rolling: 1D3',
        'D3 : **(1)**',
        'Results ordered by best total:',
        'Score: 0 () with Effect: D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_hitch4_oneValidScore',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        hitch: 4,
        keep: 2,
        /*
        effectModifier: 0,
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** **(4)**',
        'D6 : **(1)** **(3)**',
        'D8 : **(4)**',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Effect: D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_hitch0_allScoresValue',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        hitch: 0,
        keep: 2,
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : 1 4',
        'D6 : 1 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 9 (4+5) with Effect: D8',
        'Score: 8 (4+4) with Effect: D10',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep1EffectModifier_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 1,
        effectModifier: 2,
        extraDie: false,        
        /*
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D12',               
        'Score: 4 (4) with Increased-Effect: D12+',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep1EffectModifierExtraDie_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 1,
        effectModifier: 2,
        extraDie: true,
        /*
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D12',        
        'Score: 4 (4) with Increased-Effect: D12,D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep1EffectModifierExtra2Dice_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 1,
        effectModifier: 7,
        extraDie: true,
        /*
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D12,D12',        
        'Score: 4 (4) with Increased-Effect: D12,D12,D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep1NegativeEffectModifier_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 1,
        effectModifier: -3,
        /*
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Decreased-Effect: D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep3DieModifierPositive_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 3,
        dieModifier: 2,
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Modified roll die: D4 -> D6',
        'Modified roll die: D4 -> D6',
        'Rolling: 4D6 1D8 1D10',
        'D6 : **(1)** **(1)** 3 4',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 13 (4+4+5) with Effect: D6',
        'Score: 12 (3+4+5) with Effect: D8',
        'Score: 11 (3+4+4) with Effect: D10',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep3DieModifierNegative_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 2,
        dieModifier: -1,
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Modified roll die: D10 -> D8',
        'Rolling: 2D4 2D6 2D8',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4 5',
        'Results ordered by best total:',
        'Score: 9 (4+5) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceDieModifierPositiveMoreThan12_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '3D12',
        dieModifier: 2,
        cache_key: '',
        actualRollResults: [1, 1, 4, 1, 3],
      },
      expectedResult: [
        'Modified roll die: D12 -> D12+D4',
        'Modified roll die: D12 -> D12+D4',
        'Rolling: 3D12',
        'Rolling (dice pool unusable for hitch or effect): 2D4',
        'D4 : [1] [3]',
        'D12 : **(1)** **(1)** 4',
        'Results ordered by best total:',
        'Score: 7 (3+4) with Effect: D4',
        'Score: 4 (1+3) with Effect: D12',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_mergeMissing_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        merge: '2',
        cache_key: '',
        actualRollResults: [2, 2, 4,],
      },
      expectedResult: [
        'Did not find two input dice of type D2',
        'Rolling: 1D4 1D6 1D8',
        'D4 : 2',
        'D6 : 2',
        'D8 : 4',        
        'Results ordered by best total:',
        'Score: 6 (2+4) with Effect: D6',
        'Score: 4 (2+2) with Effect: D8',
      ],
    }),
     new TestCase({
      name: 'testControllerRollCortexPrime_mergeOnlyOne_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        merge: '6',
        cache_key: '',
        actualRollResults: [2, 2, 4,],
      },
      expectedResult: [
        'Did not find two input dice of type D6',
        'Rolling: 1D4 1D6 1D8',
        'D4 : 2',
        'D6 : 2',
        'D8 : 4',        
        'Results ordered by best total:',
        'Score: 6 (2+4) with Effect: D6',
        'Score: 4 (2+2) with Effect: D8',
      ],
    }),
     new TestCase({
      name: 'testControllerRollCortexPrime_mergeValid_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 6 8',
        merge: '6',
        cache_key: '',
        actualRollResults: [2, 7, 4,],
      },
      expectedResult: [
        'Removed one D6',
        'Modified roll die: D6 -> D8',
        'Rolling: 1D4 2D8',
        'D4 : 2',
        'D8 : 4 7',        
        'Results ordered by best total:',
        'Score: 11 (4+7) with Effect: D4',
        'Score: 9 (2+7) with Effect: D8',
      ],
    }),    
    new TestCase({
      name: 'testControllerRollCortexPrime_3mergesOneD12_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6D6 8 2D12',
        merge: '6 6 12',
        cache_key: '',
        actualRollResults: [2, 8, 7, 4, 4, 6, 12, 3],
      },
      expectedResult: [
        'Removed one D6',
        'Modified roll die: D6 -> D8',
        'Removed one D6',
        'Modified roll die: D6 -> D8',
        'Removed one D12',
        'Modified roll die: D12 -> D12+D4',
        'Rolling: 1D4 2D6 3D8 1D12',
        'Rolling (dice pool unusable for hitch or effect): 1D4',
        'D4 : 2 [3]',
        'D6 : 4 4',
        'D8 : 6 7 8',        
        'D12 : 12', 
        'Results ordered by best total:',
        'Score: 20 (8+12) with Effect: D8',
        'Score: 15 (7+8) with Effect: D12',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_extraRollEffect_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        extraEffectDice: '6',
        cache_key: '',
        actualRollResults: [2, 4, 3, 6],
      },
      expectedResult: [
        'Rolling: 1D4 2D6 1D8',
        'D4 : 2',
        'D6 : 4 6',
        'D8 : 3',        
        'Results ordered by best total:',
        'Score: 10 (4+6) with Effects: D8,D4',
        'Score: 8 (2+6) with Effects: D8,D6',
      ],
    }),
     new TestCase({
      name: 'testControllerRollCortexPrime_twoExtraRollEffect_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        extraEffectDice: '10 12',
        cache_key: '',
        actualRollResults: [2, 4, 3, 9, 10],
      },
      expectedResult: [
        'Rolling: 1D4 1D6 1D8 1D10 1D12',
        'D4 : 2',
        'D6 : 4',
        'D8 : 3',        
        'D10 : 9',        
        'D12 : 10',        
        'Results ordered by best total:',
        'Score: 19 (9+10) with Effects: D8,D6,D4',
        'Score: 14 (4+10) with Effects: D10,D8,D4',
        'Score: 13 (4+9) with Effects: D12,D8,D4',
        'Score: 11 (2+9) with Effects: D12,D8,D6',
        'Score: 7 (4+3) with Effects: D12,D10,D4',
        'Score: 6 (2+4) with Effects: D12,D10,D8',
      ],
    }),        
    new TestCase({
      name: 'testControllerRollCortexPrime_extraRollEffectWithIncreased_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '8 8',
        keep: 1,
        effectModifier: 1,
        extraDie: false,  
        extraEffectDice: '6 12',
        cache_key: '',
        actualRollResults: [3, 4, 2, 2],
      },
      expectedResult: [
        'Rolling: 1D6 2D8 1D12',
        'D6 : 2',
        'D8 : 3 4',        
        'D12 : 2',        
        'Results ordered by best total:',
        'Score: 4 (4) with Effects: D12,D8,D6. Choose the Increased-die - one of: D12+ | D10 | D8',
        'Score: 2 (2) with Effects: D12,D8,D8. Choose the Increased-die - one of: D12+ | D10 | D10',      
      ],
    }),    
    new TestCase({
      name: 'testControllerRollCortexPrime_extraRollEffectWith2IncreasedExtraDie_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '8 8',
        keep: 1,
        effectModifier: 2,
        extraDie: true,  
        extraEffectDice: '6 12',
        cache_key: '',
        actualRollResults: [3, 4, 2, 2],
      },
      expectedResult: [
        'Rolling: 1D6 2D8 1D12',
        'D6 : 2',
        'D8 : 3 4',        
        'D12 : 2',        
        'Results ordered by best total:',
        'Score: 4 (4) with Effects: D12,D8,D6. Choose the Increased-die - one of: D12,D6 | D12 | D10',
        'Score: 2 (2) with Effects: D12,D8,D8. Choose the Increased-die - one of: D12,D6 | D12 | D12',      
      ],
    }),    
    
    // Save tests
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSave_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [],
          // guild_id
          // user_id
          // save_opt_out: ,
          // save_opt_in - string,
        },

        cache_key: '',
        actualRollResults: [2, 3, 4, 5, 6],
      },
      expectedResult: [
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        'Rolling: 2D6 3D8',
        'D6 : 2 4',
        'D8 : 3 5 6',
        'Results ordered by best total:',
        'Score: 11 (5+6) with Effect: D8',
      ],
    }),   
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveOptInAll_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [],
          save_opt_in: controller.SAVE_NAME_ALL_WILCARD,
        },    
        cache_key: '',
        actualRollResults: [2, 3, 4, 5, 6],
      },
      expectedResult: [
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        'Rolling: 2D6 3D8',
        'D6 : 2 4',
        'D8 : 3 5 6',
        'Results ordered by best total:',
        'Score: 11 (5+6) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveOptInManualAll_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [],
          save_opt_in: `${SAVE_NAME}${controller.SAVE_NAME_SEPARATOR} ${SINGLE_SAVE_NAME} `,
        },    
        cache_key: '',
        actualRollResults: [2, 3, 4, 5, 6],
      },
      expectedResult: [
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        'Rolling: 2D6 3D8',
        'D6 : 2 4',
        'D8 : 3 5 6',
        'Results ordered by best total:',
        'Score: 11 (5+6) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveOptInManualOne_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [],
          save_opt_in: `${SAVE_NAME}    `,
        },    
        cache_key: '',
        actualRollResults: [2, 3, 4],
      },
      expectedResult: [
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        'Rolling: 2D6 1D8',
        'D6 : 2 4',
        'D8 : 3',
        'Results ordered by best total:',
        'Score: 7 (4+3) with Effect: D6',
        'Score: 6 (2+4) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveOptOutAll_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [],
          save_opt_out: controller.SAVE_NAME_ALL_WILCARD,
        },    
        cache_key: '',
        actualRollResults: [2, 3],
      },
      expectedResult: [
        'Rolling: 1D6 1D8',
        'D6 : 2',
        'D8 : 3',
        'Results ordered by best total:',
        'Score: 5 (2+3) with Effect: D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveOptOutManualAll_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [],
          save_opt_out: `${SAVE_NAME}   ${controller.SAVE_NAME_SEPARATOR}${SINGLE_SAVE_NAME} `,
        },    
        cache_key: '',
        actualRollResults: [2, 3],
      },
      expectedResult: [
        'Rolling: 1D6 1D8',
        'D6 : 2',
        'D8 : 3',
        'Results ordered by best total:',
        'Score: 5 (2+3) with Effect: D4',
      ],
    }),
     new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveOptOutManualSingle_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [],
          save_opt_out: `${SINGLE_SAVE_NAME} `,
        },    
        cache_key: '',
        actualRollResults: [2, 3, 4],
      },
      expectedResult: [
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        'Rolling: 2D6 1D8',
        'D6 : 2 4',
        'D8 : 3',
        'Results ordered by best total:',
        'Score: 7 (4+3) with Effect: D6',
        'Score: 6 (2+4) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveOtherGuild_goodMessagesNoSave',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [],
          guild_id: OTHER_GUILD_ID,
        },
        cache_key: '',
        actualRollResults: [2, 3],
        },
        expectedResult: [
          'Rolling: 1D6 1D8',
          'D6 : 2',
          'D8 : 3',
          'Results ordered by best total:',
          'Score: 5 (2+3) with Effect: D4',
        ],
    }),   
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveSecondTimeForSingle_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [PREPARE_ACTION_GET_FOR_USER],
        },    
        cache_key: '',
        actualRollResults: [2, 3, 4],
      },
      expectedResult: [
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        'Rolling: 2D6 1D8',
        'D6 : 2 4',
        'D8 : 3',
        'Results ordered by best total:',
        'Score: 7 (4+3) with Effect: D6',
        'Score: 6 (2+4) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveSecondTimeForOtherUser_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [PREPARE_ACTION_GET_FOR_USER],
          user_id: OTHER_USER_ID,
        },    
        cache_key: '',
        actualRollResults: [2, 3, 4, 5, 6],
      },
      expectedResult: [
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        'Rolling: 2D6 3D8',
        'D6 : 2 4',
        'D8 : 3 5 6',
        'Results ordered by best total:',
        'Score: 11 (5+6) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveDeleteScene_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [PREPARE_ACTION_DELETE_SCENE],
        },    
        cache_key: '',
        actualRollResults: [2, 3, 5, 6],
      },
      expectedResult: [
        `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        'Rolling: 1D6 3D8',
        'D6 : 2',
        'D8 : 3 5 6',
        'Results ordered by best total:',
        'Score: 11 (5+6) with Effect: D8',
      ],
    }),
    
    
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveWithWildcardOptedOut_noExtraDice',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [PREPARE_ACTION_WILDCARD_OPTED_OUT],
          // guild_id
          // user_id
          // save_opt_out: ,
          // save_opt_in - string,
        },
        cache_key: '',
        actualRollResults: [2, 3, 4, 5, 6],
      },
      expectedResult: [
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        'Rolling: 2D6 3D8',
        'D6 : 2 4',
        'D8 : 3 5 6',
        'Results ordered by best total:',
        'Score: 11 (5+6) with Effect: D8',
      ],
    }),   
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveWithWildcardOptedOutAsked_onlyOptedOut',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [PREPARE_ACTION_WILDCARD_OPTED_OUT],
          save_opt_in: `${OPTED_OUT_SAVE_NAME}`,
        },
        cache_key: '',
        actualRollResults: [4, 3, 5],
      },
      expectedResult: [
        `Bonus dice due to '${OPTED_OUT_SAVE_NAME}': 1D10`,
        'Rolling: 1D6 1D8 1D10',
        'D6 : 4',
        'D8 : 3',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 9 (4+5) with Effect: D8',
        'Score: 7 (4+3) with Effect: D10',
      ],
    }),  
    new TestCase({
      name: 'testControllerRollCortexPrime_constantSaveWithWildcardOptedOutAskedBothInAndOut_noExtra',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 8',
        saverInput : {
          preparation_actions: [PREPARE_ACTION_WILDCARD_OPTED_OUT],
          save_opt_in: `${OPTED_OUT_SAVE_NAME}`,
          save_opt_out: `${OPTED_OUT_SAVE_NAME}`,
        },
        cache_key: '',
        actualRollResults: [2, 3],
      },
      expectedResult: [
        'Rolling: 1D6 1D8',
        'D6 : 2',
        'D8 : 3',
        'Results ordered by best total:',
        'Score: 5 (2+3) with Effect: D4',
      ],
    }),  
          
   
    new TestCase({
      name: 'testControllerRollCortexPrime_singlePool_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        cache_key: '',
        actualRollResults: [2, 2, 4, 6 ,8 , 1, 10],
        preparePool: true,
        poolsList: [POOL_NAME],
      },
      expectedResult: [
        `Dice pool '${POOL_NAME}': 1D6 1D8 2D10`,
        'Rolling: 1D4 2D6 2D8 2D10',
        'D4 : 2',
        'D6 : 2 6',
        'D8 : 4 8',        
        'D10 : **(1)** 10',        
        'Results ordered by best total:',
        'Score: 18 (8+10) with Effect: D8',
        'Score: 14 (6+8) with Effect: D10',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_twoPools_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        cache_key: '',
        actualRollResults: [2, 2, 4, 6 ,8 , 1, 10, 2],
        preparePool: true,
        poolsList: [POOL_NAME, SECOND_POOL_NAME],
      },
      expectedResult: [
        `Dice pool '${POOL_NAME}': 1D6 1D8 2D10`,
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        'Rolling: 1D4 2D6 2D8 2D10 1D12',
        'D4 : 2',
        'D6 : 2 6',
        'D8 : 4 8',        
        'D10 : **(1)** 10',        
        'D12 : 2',      
        'Results ordered by best total:',
        'Score: 18 (8+10) with Effect: D12',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_noPools_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        cache_key: '',
        actualRollResults: [2, 2, 4],
        preparePool: true,
        poolsList: [],
      },
      expectedResult: [
        'Rolling: 1D4 1D6 1D8',
        'D4 : 2',
        'D6 : 2',
        'D8 : 4',        
        'Results ordered by best total:',
        'Score: 6 (2+4) with Effect: D6',
        'Score: 4 (2+2) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_noPoolPrepared_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        cache_key: '',
        actualRollResults: [2, 2, 4],
        preparePool: false,
        poolsList: [POOL_NAME],
      },
      expectedResult: [
        `Pool not found: '${POOL_NAME}'`,
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_noPoolFound_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        cache_key: '',
        actualRollResults: [2, 2, 4],
        preparePool: true,
        poolsList: [NON_POOL_NAME, POOL_NAME],
      },
      expectedResult: [
        `Pool not found: '${NON_POOL_NAME}'`,
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_onePoolSaverDice_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        cache_key: '',
        actualRollResults: [2, 2, 4, 6, 6],
        saverInput : {
          preparation_actions: [],
          save_opt_in : `${SAVE_NAME}`,
        },  
        preparePool: true,
        poolsList: [SECOND_POOL_NAME],
      },
      expectedResult: [
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        'Rolling: 1D4 2D6 1D8 1D12',
        'D4 : 2',
        'D6 : 2 6',
        'D8 : 4',        
        'D12 : 6',      
        'Results ordered by best total:',
        'Score: 12 (6+6) with Effect: D8',
        'Score: 10 (6+4) with Effect: D12',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_onePoolSaverDiceMergeBoth_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '6 12',
        cache_key: '',
        actualRollResults: [2, 4, 3],
        merge: '6 12',
        saverInput : {
          preparation_actions: [],
          save_opt_in : `${SAVE_NAME}`,
        },  
        preparePool: true,
        poolsList: [SECOND_POOL_NAME],
      },
      expectedResult: [
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        'Removed one D6',
        'Modified roll die: D6 -> D8',
        'Removed one D12',
        'Modified roll die: D12 -> D12+D4',
        'Rolling: 1D8 1D12',
        'Rolling (dice pool unusable for hitch or effect): 1D4',
        'D4 : [3]',
        'D8 : 2',
        'D12 : 4',      
        'Results ordered by best total:',
        'Score: 7 (3+4) with Effect: D8',
        'Score: 5 (3+2) with Effect: D12',
      ],
    }),
  ]
}


/*
input: {
dice - string,
nonPoolDice - string,
keep,
effectModifier,
dieModifier,
hitch,
actualRollResults - list of random results,
  }
*/
async function testRollerRollCortexPrime(input) {
  const rollInput = {
    diceRoll: roller.ParseDiceInput(input.dice),
    nonPoolDice: roller.ParseDiceInput(input.nonPoolDice ?? ''),
    keep: input.keep,
    effectModifier: input.effectModifier,
    dieModifier: input.dieModifier,
    hitch: input.hitch,
  } 

  let iocContainer = createIocContainer(input.actualRollResults)

  let result = await roller.RollCortexPrime(rollInput, iocContainer)

  return result.messageContent     
}


  
  
/*
input: {
  dice - string,
  keep,
  effectModifier,
  extraDie - if true effect adds extra dice (as opposed to "12+" mark),
  dieModifier,
  hitch,
  merge,
  extraEffectDice,  

  saverInput (triggers preparation) : {
    preparation_actions
    guild_id, (defaults to const)
    user_id, (defaults to const)
    save_opt_out - string,
    save_opt_in - string,
  },
  
  preparePool - bool,
  poolList - list of strings,

  cache_key,
  actualRollResults - list of random results,
}
*/
async function testControllerRollCortexPrime(input) {
  let iocContainer = createIocContainer(input.actualRollResults)
  iocContainer.options.extraEffectDieModeOn = input.extraDie ?? false

  if(input.saverInput) {
    let saverInput = input.saverInput
    let prepareInput = {
      actions: saverInput.preparation_actions
    }    
    await prepareConstantSaveDices(prepareInput, iocContainer)    
  }

  if(input.preparePool) {
    await prepareConstantPools(input.preparePool, iocContainer)
  }

  const rollInput = {
    dice: input.dice,
    keep: input.keep,
    effect_mod: input.effectModifier,
    die_mod: input.dieModifier,
    hitch: input.hitch,
    merge: input.merge,
    save_opt_out: input.saverInput?.save_opt_out,
    save_opt_in: input.saverInput?.save_opt_in,
    extra_effect_dice: input.extraEffectDice,
    pools: input.poolsList?.join(","),
    save_input: {
      userId:  input.saverInput?.user_id ?? USER_ID, 
      guildId: input.saverInput?.guild_id ?? GUILD_ID,
    },
    cache_key: input.cache_key
  } 

  let result = await controller.RollCortexPrime(rollInput, iocContainer)

  return result
}

