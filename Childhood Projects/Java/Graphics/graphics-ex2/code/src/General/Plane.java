package General;

/**
 * 
 * The class represents a plane
 *
 */
public class Plane {
	
	private NormalVector normal;
	private Point point;
	public static final Point MISS = null;
	
	public Plane (NormalVector normal,Point point)
	{
		this.normal=normal;
		this.point=point;
	}
	
	public Point hit(Ray ray) {
		Vector rayVector = ray.getVector();
		Point rayPoint = ray.getPoint();
		double constant = -normal.dotProduct(point.toVector()); // c=-N dot P0
		double result = -(rayPoint.toVector().dotProduct(normal)+constant)/(rayVector.dotProduct(normal));
		if (result<0)
			return MISS;
		Point pointIntersection = ray.getPoint(result);
				
		return pointIntersection;
	}

	
	/**
	 * returns the normal for this plane to the opposite direction of ray
	 * */
	public NormalVector getNormal(Ray ray)
	{
		NormalVector normalDirection=ray.getVector().negative();
		if(normal.dotProduct(normalDirection)>=0) //angel less (or equals) 90 degrees
			return normal;
		
		return normal.negative();
	}
	
	
	
	/**
	 * returns true if a the origin point (of a ray) is on the right ("up") side of the plane
	 * */
	public boolean onRight(Point origin)
	{
		NormalVector vec=NormalVector.subPoints(origin, point);
		
		return normal.dotProduct(vec)>=0; //if point on plane: right
	}
	
	
}
