package Objects;

import General.Intersection;
import General.NormalVector;
import General.Plane;
import General.Point;
import General.Point2D;
import General.Ray;
import General.Scene;
import General.Vector;
import Main.Utilities;


/**
 * 
 * represents a sphere
 *
 */
public class Sphere extends Surface {
	private Point center;
	private double radius;
	
	/**
	 * default constructor
	 */
	public Sphere() {
	}
	
	
	public Sphere(Point center, double radius) {
		this.center = center;
		this.radius = radius;
	}

	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Intersection intersect(Scene scene,Ray ray)
	{
		NormalVector rayVector = ray.getVector();
		Point rayPoint = ray.getPoint();
		double[] result;
		double a = 1;
		Vector vec = rayVector.product(2); //2*v 
		double b = vec.dotProduct(Vector.subPoints(rayPoint,center)); //2*v dot (P0-O) 
		//double c = 0; // point.distance(center) - Math.pow(radius,2)
		Vector sub=Vector.subPoints(rayPoint,center);
		double c = sub.dotProduct(sub) - Math.pow(radius,2); //sub*sub= |sub|^2
		result= Utilities.solveQuadraticEquation(a,b,c);
		
		if (result==Utilities.NO_SOLUTION)
			return Intersection.MISS;
		
		//remove the 0 ?
		if (result[0]<=0 && result[1]<=0) //also if zero, then it's a wrong value
			return Intersection.MISS;
		
				
		Point point1 = ray.getPoint(result[0]);
		Point point2 = ray.getPoint(result[1]);
		
		double dist1 = rayPoint.distance(point1);
		double dist2 = rayPoint.distance(point2);
				
		if (result[0]<=0)
			return new Intersection(dist2,this,point2,ray,getNormal(point2));
		if (result[1]<=0)
			return new Intersection(dist1, this, point1,ray,getNormal(point2));
		
		if (dist1<dist2)
			return new Intersection(dist1, this, point1,ray,getNormal(point2));
		else
			return new Intersection(dist2,this,point2,ray,getNormal(point2));
	}

	
	public boolean hasAllValues() {
		if (center==null || radius<=0)
			return false;
		return true;
	}

	/**
	 * returns the normal at a given point
	 * */
	private NormalVector getNormal(Point hit)
	{
		return NormalVector.subPoints(hit, center); // (hit-center) the normal at that point
	}



	public Point2D parametrization(Point point) {
		Vector vec = Vector.subPoints(point, center);
		double v = Math.acos(vec.getZ()/radius);
		double u = Math.acos(vec.getX()/(radius*Math.sin(v)))/2;
		if(vec.getY() < 0){
			u = Math.PI - u;
		}
		u = u/Math.PI;
		v = v/Math.PI;
		return new Point2D(u,v);
	}
	
	
	public boolean onRight(Plane plane)
	{
		return plane.onRight(center);
	}
}
