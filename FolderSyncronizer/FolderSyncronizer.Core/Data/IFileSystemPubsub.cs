﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace FolderSyncronizer.Core.Data
{
    public record FileWatcherArgs
    {
        public string SourceDirectory { get; set; }
        public string TargetDirectory { get; set; }
        public FileSystemEventArgs FileEventsArgs { get; set; }
    }
}
