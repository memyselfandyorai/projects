const {POOL_REMOVE}= require('../../../../common/pools.js');
const {ModifyPools}= require('../../../../common/poolsController.js');

let input = {
  action: POOL_REMOVE,
}
await ModifyPools(input, context.params.event)

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "pool-remove",
  "description": "Remove dice from a pool.",
  "options": [
    {
      "type": 3,
      "name": "name",
      "description": "Name of the pool.",
      "required": true
    },
    {
      "type": 3,
      "name": "dice",
      "description": "Dice to remove.",
      "required": true
    }
  ]
});
*/