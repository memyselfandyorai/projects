import type { BasicDataStorage } from "./base_storage";
import type { StorageItem } from "../api/storage";

export class InMemoryDatabase implements BasicDataStorage {
  cacheMap: Map<string, StorageItem>;
  constructor() {
    this.cacheMap = new Map();
  }

  async setItem(key: string, value: StorageItem): Promise<void> {
    this.cacheMap.set(key, value);
  }

  async getAllItemKeys(): Promise<string[]> {
    return Array.from(this.cacheMap.keys());
  }

  async getItem(key: string): Promise<StorageItem | null | undefined> {
    const item = this.cacheMap.get(key);
    if (!item) {
      return item;
    }
    return cloneMemValue(item);
  }

  async deleteItem(key: string): Promise<void> {
    this.cacheMap.delete(key);
  }
}

function cloneMemValue(value: StorageItem): StorageItem | undefined {
  if (value == undefined) {
    return value;
  }

  return JSON.parse(JSON.stringify(value)) as StorageItem;
}
