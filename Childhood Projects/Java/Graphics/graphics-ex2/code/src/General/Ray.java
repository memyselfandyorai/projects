package General;

/**
 * The class represents a ray
 *
 */
public class Ray {
	
	//ray = (x0,y0,z0) + t*(x,y,z)   ( point+ t*direction)
	private Point point; //start point
	private NormalVector vector; 
	
	private static final double EPSILON=0.0001;
	
	public Ray(Point p,NormalVector v)
	{
		point=p;
		vector=v;
	}
	
	
	public Point getPoint() {
		return point;
	}

	public NormalVector getVector() {
		return vector;
	}
	
	
	public Point getPoint(double t)
	{
		Point p= point.add(vector.product(t).toPoint());
		return p;
	}
	
	
	/**
	 * adds a small value to the start point, so the ray won't intersect its origin surface
	 * */
	public void addEpsilon()
	{
		point=getPoint(EPSILON);		
	}
	
		
	public static Point getPoint(Point p0,Vector dir,double dist)
	{
		Point p= p0.add(dir.product(dist).toPoint());
		//Point p=(new Ray(p0,dir)).getPoint(dist);  - normilizes!
		return p;
	}
	
	public String toString()
	{
		return point+"+t*"+vector;
	}
	
	
}
