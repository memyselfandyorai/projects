const {RemoveCounter}= require('../../../../common/countersController.js');

await RemoveCounter({}, context.params.event)

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "counter-remove",
  "description": "Removes a counter for a player.",
  "options": [
    {
      "type": 3,
      "name": "who",
      "description": "The name of a player. Defaults to nickname or user name."
    },
    {
      "type": 3,
      "name": "name",
      "description": "Counter name. Defaults to \"pp\"."
    },
    {
      "type": 4,
      "name": "type",
      "description": "A built-in type to act as a counter-name.",
      "choices": [
        {
          "name": "pp",
          "value": 1
        },
        {
          "name": "xp",
          "value": 2
        }
      ]
    }
  ]
});
*/