﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Ioc.InfraTests.TestObjects
{
    class SimpleImpl : ISimpleInterface, IDisposable
    {
        private static int _nextId = 0;

        public int ID { get; private set; }
        public bool DisposeCalled { get; private set; }

        public SimpleImpl()
        {
            this.ID = Interlocked.Increment(ref _nextId);
        }

        public int GetNumber(string name)
        {
            return name.GetHashCode();
        }

        public void Dispose()
        {
            DisposeCalled = true;
        }
    }
}
