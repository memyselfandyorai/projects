const controller = require('./controller.js');
const {ReplyToCommand, ReceivedCommand}= require('./commands.js');

module.exports = {
  RunSaver: runSaver,
};


/*
event - context.params.event,
actionResolver - func(input of parseInput) -> saver action.
*/
async function runSaver(event, actionResolver) {
  
  await ReceivedCommand(event)  
  
  console.log('raw input data: ' + JSON.stringify(event.data))
  let inputOptions = parseInput(event) 
  console.log('parse input: ' + JSON.stringify(inputOptions))
  
  inputOptions.action = actionResolver(inputOptions)
  
  let messageContent = await controller.RunSafe(() => controller.SaveDice(inputOptions, controller.DefaultIocContainer))
  console.log('Saver result: ' + messageContent )
  await ReplyToCommand(event, messageContent)  
  
  let updateInput = {
    event: event,
  }
  
  await controller.UpdatePinnedSummaryMessage(updateInput, controller.DefaultIocContainer)
}

// Returns input: {name, lifespan, optional wildcard_mode, dice, guild_id, user_id}
function parseInput(event) {
  let dataOptions = event.data.options
  
  var inputMap = new Map()
  dataOptions.forEach(option => {
    inputMap.set(option.name, option.value)
  }) 
  
  let userId = event.member.user.id
    
  return  {
    name: inputMap.get('name') ?? '',
    dice: inputMap.get('dice') ?? '',
    lifespan: inputMap.get('lifespan') ?? '',
    wildcard_mode: inputMap.get('wildcard-mode') ?? 0,
    guild_id: event.channel_id ?? '', // Note: using channel ID instead of guild (which is the whole server).
    user_id: userId ?? 0,
  }
}