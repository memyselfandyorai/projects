﻿using FolderSyncronizer.Core.Watchers;
using Infra.Framework.Mef;
using System.IO;

namespace FolderSyncronizer.Core
{
    class WatcherHandlerFactory : MefFactory<IWatcherHandler, WatcherChangeTypes, IWatcherHandler>
    {
        public WatcherHandlerFactory() : base(assembliesPrefixes: "FolderSyncronizer")
        {

        }

        public IWatcherHandler GetHandler(WatcherChangeTypes type)
        {
            return Create(type);
        }

        protected override IWatcherHandler ConvertImport(IWatcherHandler import)
        {
            return import;
        }

        protected override WatcherChangeTypes GetKey(IWatcherHandler import)
        {
            return import.SupportedType;
        }
    }
}
