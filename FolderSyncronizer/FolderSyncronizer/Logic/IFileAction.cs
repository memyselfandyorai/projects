﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderSyncronizer.Logic
{
    interface IFileAction
    {
        string SourceFile { get; }
        string TargetFile { get; }
        string Description { get; }

        void Apply();
    }
}
