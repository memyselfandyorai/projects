const controller= require('../../../../common/controller.js');
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});
const {ReplyToCommand, ReceivedCommand}= require('../../../../common/commands.js');

await ReceivedCommand(context.params.event)  
console.log('input data: ' + JSON.stringify(context.params.event.data))

let inputOptions = parseInput(context.params.event) 

let messageContent = await controller.RunSafe(() => controller.RollCortexPrimeKeep(inputOptions, controller.DefaultIocContainer))

await ReplyToCommand(context.params.event, messageContent)  

// Returns input: {keep, cache_key}
function parseInput(event) {
  let dataOptions = event.data.options
  
  var inputMap = new Map()
  dataOptions.forEach(option => {
    inputMap.set(option.name, option.value)
  })
  
  return  {
    keep: inputMap.get('keep') ?? 3, // defaults to 3
    cache_key: controller.FormatChannelKey(context.params.event),
  }
}

/*
Command:

const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "keep",
  "description": "Keeps another number of dice from the previous roll",
  "options": [
    {
      "type": 4,
      "name": "keep",
      "description": "Number of dice to keep",
      "required": true
    }
  ]
});

*/