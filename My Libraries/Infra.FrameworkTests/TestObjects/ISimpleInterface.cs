﻿namespace Infra.FrameworkTests.TestObjects
{
    public interface ISimpleInterface
    {
        int GetNumber(string name);

        int ID { get; }
        bool DisposeCalled { get; }
    }
}
