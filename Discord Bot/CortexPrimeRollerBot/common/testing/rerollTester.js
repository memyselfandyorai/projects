const controller = require('../controller.js');
const {TestCase, createIocContainer}= require('./common.js');
const {
  prepareConstantRollWithSave,
  CONSTANT_ROLL_CACHE_KEY, 
  getRequiredRollResults, 
  prepareConstantRoll, 
  CONSTANT_ROLL_DEFAULT, CONSTANT_ROLL_ADVANCED ,CONSTANT_ROLL_SAVE ,  CONSTANT_ROLL_NON_POOL_SAVE , CONSTANT_ROLL_REROLL_STEPUP, CONSTANT_POOL_ROLL, CONSTANT_POOL_ROLL_ADVANCED,
  GUILD_ID, USER_ID
 } = require('./constantRolls.js');
const saverTester = require('./saverTester.js');


module.exports = {
  TestCases: createTestCases(),
};


function createTestCases() {
  return [
    new TestCase({
      name: 'testControllerRerollCortexPrime_noPreviousRoll_noCachedMessage',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        dice: '6',
        cache_key: 'empty',
        actualRollResults :[1, 2, 3, 4],
      },
      expectedResult: [
        'No last roll found',
      ],    
    }),
    
     new TestCase({
      name: 'testControllerRerollCortexPrime_justReroll_usedDie',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        dice: '',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        actualRollResults :[2, 3, 3, 4, 5],
      },
      expectedResult: [        
        'Rolling: 1D4 1D6 2D8 1D10',
        'D4 : **(2)**',
        'D6 : 3',
        'D8 : 3 4',
        'D10 : 5',
        'Results ordered by best total:',              
        'Score: 5 (5) with Increased-Effect: D10',        
        'Score: 4 (4) with Increased-Effect: D12',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_withAdditionalDice_usedDie',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        dice: '12',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        actualRollResults :[6, 2, 3, 3, 4, 5],
      },
      expectedResult: [        
        'Rolling: 1D4 1D6 2D8 1D10 1D12',
        'D4 : **(2)**',
        'D6 : 3',
        'D8 : 3 4',
        'D10 : 5',
        'D12 : 6',
        'Results ordered by best total:',      
        'Score: 6 (6) with Increased-Effect: D12',
        'Score: 5 (5) with Increased-Effect: D12+',        
      ],    
    }),
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_withStepUp_steppedUp',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        step_up: 4, 
        step_up_mod: 1, 
        actualRollResults :[2, 3, 3, 4, 5],
      },
      expectedResult: [
        'Modified roll die: D4 -> D6',
        'Rolling: 2D6 2D8 1D10',
        'D6 : **(2)** 3',
        'D8 : 3 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D10',
        'Score: 4 (4) with Increased-Effect: D12',
      ],    
    }),
    new TestCase({
      name: 'testControllerRerollCortexPrime_withNegativeStepUp_steppedDown',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        step_up: 6, 
        step_up_mod: -1, 
        actualRollResults :[2, 3, 3, 4, 5],
      },
      expectedResult: [
        'Modified roll die: D6 -> D4',
        'Rolling: 2D4 2D8 1D10',
        'D4 : **(2)** 3',
        'D8 : 3 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D10',
        'Score: 4 (4) with Increased-Effect: D12',
      ],    
    }),
     new TestCase({
      name: 'testControllerRerollCortexPrime_withStepDownOfMinimum_noStepDown',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        step_up: 4, 
        step_up_mod: -1, 
        actualRollResults :[2, 3, 3, 4, 5],
      },
      expectedResult: [
        'Rolling: 1D4 1D6 2D8 1D10',
        'D4 : **(2)**',
        'D6 : 3',
        'D8 : 3 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D10',
        'Score: 4 (4) with Increased-Effect: D12',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_withStepUpBeyond12_steppedUp',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        step_up: 10, 
        step_up_mod: 3, 
        actualRollResults :[2, 3, 3, 3, 5, 4],
      },
      expectedResult: [
        'Modified roll die: D10 -> D12+D4',
        'Rolling: 1D4 1D6 2D8 1D12',
        'Rolling (dice pool unusable for hitch or effect): 1D4',
        'D4 : **(2)** [4]',
        'D6 : 3',
        'D8 : 3 3',
        'D12 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D10',
        'Score: 4 (4) with Increased-Effect: D12+',
      ],    
    }),
    new TestCase({
      name: 'testControllerRerollCortexPrime_stepUpNotFound_notFound',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        step_up: 12, 
        step_up_mod: 1, 
        actualRollResults :[2, 3, 3, 3, 5],
      },
      expectedResult: [
        'No step up/down found for D12: did not step up/down',
        'Rolling: 1D4 1D6 2D8 1D10',
        'D4 : **(2)**',
        'D6 : 3',
        'D8 : 3 3',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D10',
        'Score: 3 (3) with Increased-Effect: D12',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_rerollAfterSaver_useSavedDie',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        dice: '',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_ROLL_SAVE,
        actualRollResults :[2, 3, 3, 4, 5, 6, 7, 8],
      },
      expectedResult: [        
        'Rolling: 1D4 2D6 4D8 1D10',
        'D4 : **(2)**',
        'D6 : 3 6',
        'D8 : 3 4 7 8',
        'D10 : 5',
        'Results ordered by best total:',              
        'Score: 8 (8) with Increased-Effect: D12',        
      ],    
    }),     
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_rerollAfterNonPoolDice_expectedMessages',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        dice: '',
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_ROLL_NON_POOL_SAVE,
        actualRollResults :[2, 3, 1, 1],
      },
      expectedResult: [        
        'Rolling: 2D12',
        'Rolling (dice pool unusable for hitch or effect): 2D4',
        'D4 : [1] [1]',    
        'D12 : 2 3',
        'Results ordered by best total:',
        'Score: 5 (2+3) with Effect: D4',
        'Score: 4 (1+3) with Effect: D12',
      ],    
    }),   
     new TestCase({
      name: 'testControllerRerollCortexPrime_rerollAfterStepupReroll_goodMessages',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_ROLL_REROLL_STEPUP,
        actualRollResults :[6, 6, 1, 4],
      },      
      expectedResult: [
        'Rolling: 2D12',
        'Rolling (dice pool unusable for hitch or effect): 1D4 1D6',
        'D4 : [1]',    
        'D6 : [4]',
        'D12 : 6 6',
        'Results ordered by best total:',
        'Score: 12 (6+6) with Effect: D4',
        'Score: 10 (4+6) with Effect: D12',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_rerollDicePool_goodMessages',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_POOL_ROLL ,
        actualRollResults :[1, 6, 1, 4, 5],
      },      
      expectedResult: [
        'Rolling: 1D6 1D8 2D10 1D12',
        'D6 : **(1)**',
        'D8 : 6',
        'D10 : **(1)** 4',
        'D12 : 5',
        'Results ordered by best total:',
        'Score: 6 (6) with Effect: D12',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_rerollAdvancedDicePool_goodMessages',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        prepare_mod: CONSTANT_POOL_ROLL_ADVANCED,
        actualRollResults :[2, 6, 5, 4, 1, 1 ],
      },      
      expectedResult: [
        'Rolling: 1D6 3D8 2D10',
        'D6 : 2',
        'D8 : **(1)** **(1)** 6',
        'D10 : 4 5',
        'Results ordered by best total:',
        'Score: 11 (6+5) with Effects: D10,D6,D4',
        'Score: 8 (2+6) with Effects: D10,D10,D4',
      ],    
    }),
    
    
  ]
}


/*
input: {
  dice - string,
  step_up, 
  step_up_mod, 
  cache_key,
  actualRollResults - list of random results,
  
  prepare_mod - one of CONSTANT_ROLL_....
}
*/
async function testControllerRerollCortexPrime(input) {  
  let constantRolls = getRequiredRollResults(input.prepare_mod)  
  let actualRollResults = [...constantRolls, ...input.actualRollResults]
  let iocContainer = createIocContainer(actualRollResults)
  
  await prepareConstantRoll(input.prepare_mod, iocContainer)  
  
  let rerollInput = {
    dice: input.dice,
    step_up: input.step_up,
    step_up_mod: input.step_up_mod,
    cache_key: input.cache_key,
  }
  let messageContentResult = await controller.RerollCortexPrime(rerollInput, iocContainer)
  
  return messageContentResult
}
