﻿using FolderSyncronizer.Core.Watchers.Actions;
using Infra.Framework.Extensions;
using Infra.Framework.Mef;
using System.Collections.Generic;
using System.IO;

namespace FolderSyncronizer.Core.Watchers.Handlers
{
    [ExportForMef(typeof(IWatcherHandler))]
    class FileRenamedHandler : IWatcherHandler
    {
        public WatcherChangeTypes SupportedType => WatcherChangeTypes.Renamed;

        public IEnumerable<IFileAction> HandleChange(string sourceFolder, string targetFolder, FileSystemEventArgs eventArgs)
        {
            var renamedArgs = eventArgs.AssertType<RenamedEventArgs>();
            string fileInSource = renamedArgs.FullPath;
            string relativeNew = Path.GetRelativePath(sourceFolder, fileInSource);

            string oldInSource = renamedArgs.OldFullPath;
            string relativeOld = Path.GetRelativePath(sourceFolder, oldInSource);

            return new RenameAction(oldInSource, fileInSource, Path.Combine(targetFolder, relativeOld), Path.Combine(targetFolder, relativeNew)).ToSingleton();
        }
    }
}
