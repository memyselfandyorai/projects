﻿using System.IO;

namespace FolderSyncronizer.Core.Watchers.Actions
{
    public class CopyAction : ActionBase
    {
        // Do not use - for serialization only! All properties should not be set externally.
        public CopyAction() { }

        public CopyAction(string sourceFile, string targetFile, bool overwrite)
        {
            SourceFile = sourceFile;
            TargetFile = targetFile;
            Overwrite = overwrite;
        }

        public bool Overwrite { get; set; }

        protected override string ActionDescription => $"Copy ({ (Overwrite ? "changed" : "new") }): '{SourceFile}' => '{TargetFile}'";

        protected override void ApplyAction()
        {
            Directory.CreateDirectory(Path.GetDirectoryName(TargetFile)); // Create subfolders if needed.
            File.Copy(SourceFile, TargetFile, Overwrite);

            var targetCreationTime = File.GetCreationTime(TargetFile);
            var targetModifiedTime = File.GetLastWriteTime(TargetFile);
            if(targetCreationTime > targetModifiedTime)
            {
                // Fix issue with iPhone time: when moving files the creation time is "now", and then iPhone shows all photos at the same time.
                File.SetCreationTime(TargetFile, targetModifiedTime);
                File.SetLastWriteTime(TargetFile, targetCreationTime);
            }
        }

        protected override void ChangeOlderAction(ActionBase olderAction)
        {
            if (!olderAction.IsCancelled && olderAction is CopyAction)
            {
                // Cancel new copy, there's no need for another one.
                this.IsCancelled = true;
            }
        }
    }
}
