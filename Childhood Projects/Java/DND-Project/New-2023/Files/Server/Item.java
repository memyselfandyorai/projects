
//wieght need to affect sometimes. (climb\swim?)

//change value of meals? or make it rare?

//potions,

//animal.?  or do class Mount? (riding dog etc)

//rope to climb, raft(?)to swim(?)- something else.
import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Item implements Serializable{
	
	
	protected String name;
	protected double lbs; //weight
	protected double value; 
	protected int amount;
	
	public Item(String n,double l,double v,int a)
	{
		name=n;
		lbs=l;
		value=v;
		amount=a;
	}
	
	public Item(String n,double l,double v)
	{
		name=n;
		lbs=l;
		value=v;
		amount=1;
	}
	
	
	public Item(String n)
	{
	name=n;
	amount=1;
		if(n.equals("Tent")) //keeps at night.
		{
		//at night- cold. can get damage.-this lowers chances.fire makes no chance.
		lbs=20;
		value=10;
		}
		
		if(n.equals("Climber's Kit")) //+2 climb
		{
		lbs=5;
		value=5; //one time. 50: per 10 times.
		}
		
		if(n.equals("Healer's Kit")) //+2 healing
		{
		lbs=1;
		value=3; //one time. 30: per 10 times.
		}
		
		if(n.equals("Torch")) //(if lighted) +2 Spot in caves. *caves:-2 to Spot.
		{
		lbs=1;
		value=0.5;
		}
		
		if(n.equals("Flint and Steel")) //to burn. to start a fire.
		{
		lbs=0.5;
		value=1;
		}
		
		if(n.equals("Wood")) //to start a fireplace. keeps you warm. but penalties when at night.
		{
		//can't buy. can collect at forests. (search)
		lbs=2;
		value=0.5;
		}
		
		if(n.equals("Lumber")) //+2 to swim
		{
		lbs=5;
		value=4;
		}
		
		if(n.equals("Arrows")) //20 for 0.05
		{
		lbs=0.15;
		value=0.05; //  1/20
		amount=20;
		}
		
		if(n.equals("Bullets")) //10 for 0.02
		{
		lbs=0.5;
		value=0.02;
		amount=10;
		}
		
		if(n.equals("Bolts")) //10 for 0.1
		{
		lbs=0.1;
		value=0.1;
		amount=10;
		}
		
		if(n.equals("Artisian Tools")) //+2 craft
		{
		lbs=0.1;
		value=0.1;
		amount=10;
		}
		
		if(n.equals("Iron"))
		{
		lbs=1;
		value=0.1;
		amount=1;
		}
		
		if(n.equals("Copper"))
		{
		lbs=1;
		value=0.5;
		amount=1;
		}
				
		if(n.equals("Gold"))
		{
		lbs=1;
		value=50;
		amount=1;
		}
	
		if(n.equals("Silver"))
		{
		lbs=1;
		value=5;
		amount=1;
		}
		
		if(n.equals("Silk"))
		{
		lbs=1;
		value=20;
		amount=1;
		}
	
		if(n.equals("Bow String"))
		{
		lbs=0.1;
		value=2;
		amount=1;
		}
		
		if(n.equals("Meals"))
		{
		lbs=0.1;
		value=0.1;
		amount=1;
		}
		
		if(n.equals("Pebbles"))
		{
		lbs=5;
		value=0.01;
		amount=10;
		}
		
		
		//map? (for dir?- will help? compass?)
				
	}
	
	public Item(String n,int v,int m) //rings- "of "+n,value,amount
	{
	name="Ring of "+n;	
	value=v;
	amount=m; //amount of rings equals size of bonus. 2 rings=+2.  no bonus larger than 3(attack,damage,spell),4(skill),5(ability)
	lbs=0;
	}
	
	
	public static Item getRing()
	{
	int r=Dice.roll(100);
	
	if(r>=1&&r<=10)
	return new Item("Ring of Stealth",100,1); //pickp + hide
	if(r>=11&&r<=25)
	return new Item("Ring of Damage",50,1);
	if(r>=26&&r<=40)
	return new Item("Ring of Charm",40,2); //persuade
	if(r>=41&&r<=50)
	return new Item("Ring of Crafting",30,2);
	if(r>=51&&r<=55)
	return new Item("Ring of Damage Reduction",50,1);
	if(r>=56&&r<=60)
	return new Item("Ring of Health",70,2); //hp+con(?)
	if(r>=61&&r<=70)
	return new Item("Ring of Darkness",50,1); //hide
	if(r>=71&&r<=80)
	return new Item("Ring of Attack",50,1);
	if(r>=81&&r<=90)
	return new Item("Ring of Vision",70,1); //spot+search
	//91-100
	return new Item("Ring of Intuition",90,1); //search+dir
	
	
	
	//ring of damage reduction
	//rings of magics? "Ring of Fireball" etc?
	
	}
	
	
	public static Item getItem(String s,int m)
	{
	Item i=new Item(s);
	i.setAmount(m);
	return i;
	}
	
	public static Item createItem(String s,String clas,int m)
	{
	Item it=null;
	if(clas.equals("class Weapon"))
	it=Weapon.create(s,m);
	if(clas.equals("class Shield"))
	it=Shield.create(s,m);
	if(clas.equals("class Armor"))
	it=Armor.createa(s,m);
	if(clas.equals("class Item"))
	it=Item.getItem(s,m);

	return it;
	}
	
	
	public String getName()
	{
	return name;	
	}
	
	
	public int getAmount()
	{
	return amount;	
	}
	
	public double getWeight()
	{
	return lbs;	
	}
	
	public void setAmount(int n)
	{
	amount=n;	
	}
		
	public double getValue()
	{
	return (float)value*amount;	
	}
	
	public void setValue(double a) //sum of all.
	{
	value=a/amount;
	}
	
	public static int howMany(Vector a,String s)
	{
	int m=0;
	for(int i=0;i<a.size();i++)
	{
		Item k=(Item)(a.elementAt(i));
		if(k.getName().equals(s))
		m+=k.getAmount();
	}
	
	return m;
	}
	
	
	public static Vector getAllWeapons(Vector it)
	{
	Vector w=new Vector();
	for(int i=0;i<it.size();i++)
	if(it.elementAt(i) instanceof Weapon)
	w.add(it.elementAt(i));
		
	return w;
	}
	
	
	public static Vector getAllShields(Vector sh)
	{
	Vector s=new Vector();
	for(int i=0;i<sh.size();i++)
	if(sh.elementAt(i) instanceof Shield&&!(sh.elementAt(i) instanceof Armor))
	s.add(sh.elementAt(i));
		
	return s;
	}
	
	public static Vector getAllArmors(Vector ar)
	{
	Vector a=new Vector();
	for(int i=0;i<ar.size();i++)
	if(ar.elementAt(i) instanceof Armor)
	a.add(ar.elementAt(i));
		
	return a;
	}
	
	
	public static Item getItemT()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=10)
	return new Item("Healer's Kit");
	if(r>=11&&r<=15)
	return new Item("Artisian Tools");
	if(r>=16&&r<=25)
	return new Item("Climber's Kit");
	if(r>=26&&r<=40)
	return new Item("Wood");
	if(r>=41&&r<=50)
	return new Item("Iron");
	if(r>=51&&r<=65)
	return new Item("Copper");
	if(r>=66&&r<=75)
	return new Item("Silver");
	if(r>=76&&r<=90)
	return new Item("Light Crossbow");
	//91-100
	return new Item("Gold");
	}
	
	
	public static Item getItemV()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=15)
	return new Item("Healer's Kit");
	if(r>=16&&r<=25)
	return new Item("Climber's Kit");
	if(r>=26&&r<=45)
	return new Item("Wood");
	if(r>=46&&r<=60)
	return new Item("Lumber");
	if(r>=61&&r<=70)
	return new Item("Copper");
	if(r>=71&&r<=77)
	return new Item("Iron");
	if(r>=78&&r<=85)
	return new Item("Bow String");
	if(r>=86&&r<=95)
	return new Item("Silk");
	//91-100
	return new Item("Silver");
	}
	
	
	public static Item getItemM()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=13)
	return new Item("Healer's Kit");
	if(r>=14&&r<=27)
	return new Item("Climber's Kit");
	if(r>=28&&r<=38)
	return new Item("Wood");
	if(r>=39&&r<=49)
	return new Item("Lumber");
	if(r>=50&&r<=60)
	return new Item("Copper");
	if(r>=61&&r<=70)
	return new Item("Iron");
	if(r>=71&&r<=80)
	return new Item("Silver");
	if(r>=81&&r<=90)
	return new Item("Gold");
	//91-100
	return new Item("Torch");
	}
	
	public static Item getItemC()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=7)
	return new Item("Healer's Kit");
	if(r>=8&&r<=15)
	return new Item("Climber's Kit");
	if(r>=16&&r<=28)
	return new Item("Tent");
	if(r>=29&&r<=49)
	return new Item("Meals");
	if(r>=54&&r<=63)
	return new Item("Copper");
	if(r>=64&&r<=69)
	return new Item("Silver");
	if(r>=70&&r<=74)
	return new Item("Gold");
	if(r>=75&&r<=82)
	return new Item("Iron");
	//91-100
	return new Item("Torch");
	}
	
	
	public static Item getItemR()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=9)
	return new Item("Lumber");
	if(r>=10&&r<=18)
	return new Item("Wood");
	if(r>=19&&r<=30)
	return new Item("Meals");
	if(r>=31&&r<=40)
	return new Item("Copper");
	if(r>=41&&r<=53)
	return new Item("Iron");
	if(r>=53&&r<=58)
	return new Item("Silver");
	if(r>=59&&r<=62)
	return new Item("Gold");
	if(r>=63&&r<=70)
	return new Item("Flint and Steel");
	//91-100
	return new Item("Pebbles");
	}
	
	public static Item getItemN()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=20)
	return new Item("Copper");
	if(r>=21&&r<=40)
	return new Item("Iron");
	if(r>=41&&r<=45)
	return new Item("Silver");
	if(r>=46&&r<=50)
	return new Item("Gold");
	if(r>=51&&r<=60)
	return new Item("Flint and Steel");
	if(r>=61&&r<=70)
	return new Item("Climber's Kit");
	if(r>=71&&r<=80)
	return new Item("Healer's Kit");
	if(r>=81&&r<=87)
	return new Item("Artisian Tools");
	//91-100
	return new Item("Torch");
	}
	
	
	public static Item getItemF()
	{
	int r=Dice.roll(100);	
	if(r>=1&&r<=10)
	return new Item("Torch");
	if(r>=11&&r<=20)
	return new Item("Tent");
	if(r>=21&&r<=30)
	return new Item("Flint and Steel");
	if(r>=31&&r<=45)
	return new Item("Healer's Kit");
	if(r>=46&&r<=60)
	return new Item("Artisian Tools");
	if(r>=61&&r<=70)
	return new Item("Iron");
	if(r>=71&&r<=80)
	return new Item("Silver");
	if(r>=81&&r<=90)
	return new Item("Silk");
	//91-100
	return new Item("Bow String");
	}
	
	
	public String write()
	{
	String s="I*";	
	s+=lbs+",";	
	s+=name+",";
	s+=value+",";
	s+=amount+",";
	s+="*I";
	return s;
	}
		
	
	
	
	public static Item read(String s) //careful of "I*I"
	{
		Item d=new Item("");
		
		s=s.substring(2);
		
		int b=s.indexOf(",");
		int a;
		d.lbs=Double.parseDouble(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.name=s.substring(0,b);
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		d.value=Double.parseDouble(s.substring(0,b));
		s=s.substring(b+1);
		
		b=s.indexOf(",");
		a=Integer.parseInt(s.substring(0,b));
		d.amount=a;
		s=s.substring(b+1);
		
		
		return d;
	}
	
	
	
	public String info()
	{
		//return name+" - weight: "+lbs+" lbs, value: "+value+" gp";
		//return name+" - weight: "+lbs+" lbs, value: "+value+" gp"+", amount: "+amount;
		
		return name+" ("+amount+") - weight: "+lbs+" lbs, value: "+getValue()+" gp";
	}
	
	/*
	*returns object as string
	*/
	public String toString()
	{
		//return name+" - weight: "+lbs+" lbs, value: "+value+" gp";
		//return name+" - weight: "+lbs+" lbs, value: "+value+" gp"+", amount: "+amount;
		
		return name+" ("+amount+") - weight: "+lbs+" lbs, value: "+getValue()+" gp";
	}
}
