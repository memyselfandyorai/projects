﻿using Ioc.Infra.Extensions;
using System;

namespace Ioc.Infra.Exceptions
{
    public class ProgrammersException : SystemException
    {
        public ProgrammersException(string msgFormat, params object[] args) :
            this(null, msgFormat, args)
        {

        }

        private ProgrammersException(Exception inner, string msgFormat, params object[] args) :
            base(msgFormat.Format(args), inner)
        {

        }


        public static ProgrammersException NotImplemented(string msg, params object[] args)
        {
            string formatted = msg.Format(args);
            return new ProgrammersException(new NotImplementedException(formatted), formatted);
        }
    }
}