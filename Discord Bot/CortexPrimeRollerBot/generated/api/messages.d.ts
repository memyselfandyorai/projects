export interface OutputMessage {
    text: string;
}
export interface MessageOptions {
    /**it's possible this only works for actual-real messages, and not commands. */
    reply_to_message_id?: string;
    /**
     * embed object ({title, type, color, ... })
     * probably like this: https://discord-api-types.dev/api/discord-api-types-v10/interface/APIEmbed.
     * passes as-is to the API.
     */
    embed?: any;
    extra_data?: any;
}
export interface MessageSender {
    send(messages: OutputMessage, options: ?MessageOptions): Promise<string>;
}
export declare class NoPermissionsError extends Error {
    constructor(...params: any[]);
}
//# sourceMappingURL=messages.d.ts.map