const controller = require("../../../../../../common/controller.js");
const {
    ReplyToPrefixCommand,
} = require("../../../../../../common/commands.js");

module.exports = {
    async execute(prefixCommandInteraction, msgContent) {
        await handleCommand(prefixCommandInteraction, msgContent);
    },
};

async function handleCommand(prefixCommandInteraction, msgContent) {
    const adminId = prefixCommandInteraction.getAdminId();
    let userId = prefixCommandInteraction.senderId();

    let messageContent = [];

    console.log(`!admin prefix by user: ` + userId);

    // params options:
    // clearStorage (deletes all)
    // deleteStorageKeys (a specific prefix)

    if (userId != adminId) {
        messageContent.push("User is not an admin.");
    } else {
        let options = {
            rawValue: true,
        };
        const iocContainer = prefixCommandInteraction.getIocContainer();
        let entries = await iocContainer.storage.getAllStoredEntries(options);
        messageContent.push(`Storage size: ${entries.length}.`);
        entries.forEach((entry) => {
            messageContent.push(`Storage entry: ${JSON.stringify(entry)}.`);
        });

        let params = msgContent.split(" ");
        console.log(`params: ` + params.join(", "));
        if (params.includes("cache")) {
            options.readCache = true;
            let cacheEntries =
                await iocContainer.storage.getAllStoredEntries(options);
            messageContent.push(`Cache size: ${cacheEntries.length}.`);
            cacheEntries.forEach((entry) => {
                messageContent.push(`Cache entry: ${JSON.stringify(entry)}.`);
            });
        }
        if (params.includes("clearStorage")) {
            await iocContainer.storage.clearKeys([]);
            messageContent.push(`Cleared storage`);
        }
        if (params[1] == "deleteStorageKeys") {
            let keys = params[2]?.split(".");
            await iocContainer.storage.clearKeys(keys);
            messageContent.push(`Deleting keys: ` + keys.join("."));
        }
    }

    let options = {
        splitMessages: true,
        replyToMessage: true,
    };
    await ReplyToPrefixCommand(prefixCommandInteraction, messageContent, options);
}
