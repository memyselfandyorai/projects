
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.io.*;


//spell failure!! (sf, in shield and armor), check if stack

//if dead from disease- then what? - check if i did.-  hope it will cause no bugs in the order of battle.

//damage reduction- to consider it on spells? (more fair)


public class Spells implements Serializable{
	
	//spells and abilities. lvl -1 means ability.
	
	//spell resistance: 1d20+char level >= spell resistance. put it on armors? and on enemies
	//enemies also cast spells. random.
	//concentrate- damage taken last round. (?) [number of "attacked"? - enemies]
	
	/**
 	*Owner of the spells
 	*/
	private Char owner;
	/**
 	*Spells name
 	*/
	private String spells[];
	/**
 	*Spells which affect you, their number of turns, turns passed, and their difficulty class
 	*/
	private Vector affected,NoT,tp,sdc; //strings. number of turns, turns passed, spell dc
	/**
 	*Level of spells, their uses, how many times this day they were used, how long do they last, and which saving throw rejects them
 	*/
	private int lvl[],uses[],used[],turns[],save[]; //save: 0-none,1-ref,2-fort,3-will
	/**
 	*Number of spells
 	*/
	private int num;	
	
	/**
	 *Constructs a new Spells
	 */
	public Spells()
	{
		affected=new Vector();
		NoT=new Vector();
		tp=new Vector();
		sdc=new Vector();
		num=0;
	}
	
	/**
	 *Constructs a new Spells
	 *@param o : their owner
	 */
	public Spells(Char o)
	{
		owner=o;
		affected=new Vector();
		NoT=new Vector();
		tp=new Vector();
		sdc=new Vector();
		
		if(owner instanceof Character&&((Character)(owner)).getClas()!=null)
		{
		String clas=((Character)(owner)).getClas();
		
	
		
		if(clas.equals("Barbarian"))
		{
			num=1;
			spells=new String[1];
			lvl=new int[1];
			uses=new int[1];
			used=new int[1];
			turns=new int[num];
			save=new int[num];
			
			spells[0]="Rage"; //+4 str,+2*lvl hp,+2 will,-2 ac; from lvl 12: +6.+3*lvl,+3,-2
			lvl[0]=-1;
			uses[0]=1; //lvl/4+1
			turns[0]=3+((Character)(owner)).con()+2;//3+cons (the new), may end any time?	
			save[0]=0;
		}
		
		
		if(clas.equals("Rogue"))
		{
			num=1;
			spells=new String[1];
			lvl=new int[1];
			uses=new int[1];
			used=new int[1];
			turns=new int[num];
			save=new int[num];
			
			spells[0]="Sneak Attack"; //[(lvl+1)/2]d6 bonus damage (if not seen or sleep\paralized etc.),not multiplied
			lvl[0]=-1;
			uses[0]=-1; //-1 - meaning all the time
			turns[0]=0;
			save[0]=0;
		}
		
		
		
		
		if(clas.equals("Ranger"))
		{
		//ranger- based on wisdom
		 

		//every 5 levels: 5,10,15
		//animal, dragon,(orc, goblin),elemental,undead, (giant?)
		//animal- no... ranger... anyway- "animal horse"?
		//the "race" in enemy. (?)
		
		
			num=0; //favoured enemy: orc\goblin (bonus:1+lvl/5)
			//+1 Spot,+1 dam (even with bow) against
			
			/*
			spells=new String[1];
			lvl=new int[1];
			uses=new int[1];
			used=new int[1];
			turns=new int[num];
			
			spells[0]="Favoured Enemy - ";
			lvl[0]=2
			uses[0]=0;
			turns[0]=0;
			*/
		}
		
		
		
		
		if(clas.equals("Paladin"))
		{
			//based on wisdom
			//charisma added to saving throws
			num=1;
			spells=new String[1];
			lvl=new int[1];
			uses=new int[1];
			used=new int[1];
			turns=new int[num];
			save=new int[num];
			
			spells[0]="Lay On Hands"; //cha+level hp heals. heals +1
			lvl[0]=-1; //not wasting a turn
			
			int temp=((Character)(owner)).cha();
			if(temp<0)
			temp=0;
			uses[0]=((Character)(owner)).lvl()*temp; //ch*level and heals 1
			
			turns[0]=0;
			save[0]=0;
			//make an option to choose the number that heals?
		}

		if(clas.equals("Sorcerer"))
		{
			//based on charisma
						
			num=4;
			spells=new String[num];
			lvl=new int[num];
			uses=new int[num];
			used=new int[num];
			turns=new int[num];
			save=new int[num];
			
			spells[0]="Distrup Undead"; //1d6 damage to undead.
			lvl[0]=0;
			uses[0]=1;
			turns[0]=0;
			save[0]=0;
			
			spells[1]="Ray Of Frost"; //1d3 damage.
			lvl[1]=0;
			uses[1]=2;
			turns[1]=0;
			save[0]=2;
			
			spells[2]="Resistance"; //+2 all saves. (not just will)
			lvl[2]=0;
			uses[2]=1;
			turns[2]=10;
			save[0]=0;
			
			spells[3]="Magic Missile"; //1d4+1 damage. [(lvl+1)\2]d4+(lvl+1)\2 :max of 5
			lvl[3]=1;
			uses[3]=2;
			turns[3]=0;
			save[0]=1;
			//remember- level 1 only!
			//protection from... , mage armor,true strike
			//spells, turns.
		}
		
		
		
		if(clas.equals("Monk"))
		{
			//+wis+lvl/5 (if positive) to AC (when not wearing armor)
			
			//unarmed:
			//1-3: d6, 4-7: d8, 8-11: d10, 12-15: d12, 16: d20
			//small:1-3: d4, 4-7: d6, 8-11: d8, 12-15: d10, 16: 2d6
			
			//unarmed attack bonus: same, only every 4 another /+1 and not every 6 (not -5, -3).
			
			num=1;
			spells=new String[num];
			lvl=new int[num];
			uses=new int[num];
			used=new int[num];
			turns=new int[num];
			save=new int[num];
			
			spells[0]="Stunning Attack"; //stuns him for 1 round if hit and fail fort save of 10+wis+lvl\2
			lvl[0]=-1;
			uses[0]=1;//lvl
			turns[0]=1;
			save[0]=0;
			
		}
	
				
		for(int i=0;i<num;i++)
		used[i]=0;
		
		}
			
			
			
	}		
	
	
	
	/**
	 *Adds a spell\ability
	 *@param n : name of ability
	 *@param l : level of ability
	 *@param u : uses of ability
	 *@param t : turns of ability
	 *@param s : the save (and if) of the ability
	 */
	public void add(String n,int l,int u,int t,int s) //name,level,uses,turns,save
	{
		String sp[]=new String[num+1];
		int[] lv=new int[num+1];
		int[] us=new int[num+1];
		int[] ud=new int[num+1];
		int[] tr=new int[num+1];
		int[] sav=new int[num+1];
		
		for(int i=0;i<num;i++)
		{
		sp[i]=spells[i];
		lv[i]=lvl[i];
		us[i]=uses[i];
		ud[i]=0;
		tr[i]=turns[i];
		sav[i]=save[i];
		}
		
		sp[num]=n;
		lv[num]=l;
		us[num]=u;
		ud[num]=0;
		tr[num]=t;
		sav[num]=s;
		
		spells=sp;
		lvl=lv;
		uses=us;
		used=ud;
		turns=tr;
		save=sav;
		
		num++;
				
	}
			
	/**
	 *Adds a spell\ability
	 *@param n : name of ability
	 *@param l : level of ability
	 *@param u : uses of ability
	 *@param t : turns of ability
	 */
	public void add(String n,int l,int u,int t) //name,level,uses,turns
	{
		String sp[]=new String[num+1];
		int[] lv=new int[num+1];
		int[] us=new int[num+1];
		int[] ud=new int[num+1];
		int[] tr=new int[num+1];
		int[] sav=new int[num+1];
		
		for(int i=0;i<num;i++)
		{
		sp[i]=spells[i];
		lv[i]=lvl[i];
		us[i]=uses[i];
		ud[i]=0;
		tr[i]=turns[i];
		sav[i]=save[i];
		}
		
		sp[num]=n;
		lv[num]=l;
		us[num]=u;
		ud[num]=0;
		tr[num]=t;
		sav[num]=0;
		
		spells=sp;
		lvl=lv;
		uses=us;
		used=ud;
		turns=tr;
		save=sav;
				
		num++;
				
	}
	
	
	public Vector getVector()
	{
	Vector v=new Vector();	
	
	for(int i=0;i<num;i++)
	{
	String s=""+lvl[i];
	if(lvl[i]<0)
	s="ability";
	String us=""+uses[i];
	if(uses[i]<0)
	us="unlimited";
	String tu=""+turns[i];
	if(turns[i]==0)
	tu="instant";
	v.add(spells[i]+" - "+" level: "+s+", uses per day: "+us+", used this day: "+used[i]+", turns lasts: "+tu);
	}
	
	return v;
	}
	
	
	/**
	 *Update the uses of a spell
	 *@param s : name of ability
	 *@param n : number to add to the ability
	 */
	public void updateUses(String s,int n) //adds n
	{
		for(int i=0;i<num;i++)
		if(spells[i].equals(s))
		uses[i]+=n;
		
	}
	
	
	
	public boolean exists(String s)
	{
	for(int i=0;i<num;i++)
	if(spells[i].equals(s))
	return true;
	
	return false;		
	}
	

	 /**
	 *Adds a disease to the "affected"
	 *@param s : name of disease
	 *@param not : number of turns it lasts
	 *@param dc : its difficulty class
	 */
	public void addDisease(String s,int not,int dc) //change all of this function? (we'll see in battle)
	{
	affected.add(s);
	NoT.add(not+"");
	tp.add(""+0);
	sdc.add(""+dc);
	}
		
	 /**
	 *Checks if a character has a disease
	 *@param s : name of the disease
	 */
	public boolean hasDisease(String s)
	{
	for(int i=0;i<affected.size();i++)
	{
	String n=(String)(affected.elementAt(i));
	if(s.equals(n))
	return true;			
	}
	return false;
	}
	
	public int NoD() //number of diseases
	{
	int s=0;
	for(int i=0;i<affected.size();i++)
	{
	String n=(String)(affected.elementAt(i));
	if(n.equals("Sleep")||n.equals("Paralysis")||n.equals("Poison")||n.equals("Acid Arrow"))
	s++;
	}
	
	return s;
	}
	
	public String[] getDiseases(int nu)
	{
	String[] dis=new String[nu];
	int num=0;
	for(int i=0;i<affected.size();i++)
	{
	String n=(String)(affected.elementAt(i));
	if(n.equals("Sleep")||n.equals("Paralysis")||n.equals("Poison")||n.equals("Acid Arrow"))
	{
	dis[num]=n;
	num++;
	}
	}
	
	return dis;
	}
	
	
	public String heal(Char c,String dis)
	{
	String s="";	
		
	Character player=(Character)(owner);	
	
	int ind=0;
	
	for(int i=0;i<c.getSpells().affected.size();i++)
	{
	String n=(String)(c.getSpells().affected.elementAt(i));
	if(n.equals(dis))
	ind=0;
	i=affected.size();
	}
	
	int dc=Integer.parseInt((String)(c.getSpells().sdc.elementAt(ind)));
	int d=Dice.roll(20);
	int dd=d+player.getSkills().heal();
	if(c.male()!=player.male())
		if(c.getCON()>player.getCON())
		dd++;
		
	
	boolean suc=(dd>=dc&&d!=1)||d==20;
	
	
		if(suc)
		{
			c.getSpells().removeDisease(ind);
			s="You succeeded to remove "+dis+" from "+c.name()+".";
		}
		else
		s="You failed to remove "+dis+" from "+c.name()+".";
		
	return s;	
	}
	
	
	public String chooseDisease()
	{
	for(int i=0;i<affected.size();i++)
	{
	String n=(String)(affected.elementAt(i));
	if(n.equals("Sleep")||n.equals("Paralysis")||n.equals("Poison")||n.equals("Acid Arrow"))
	return n;
	}
	
	return "";
	}
	
	
	
	public Vector getAffected()
	{
	return affected;	
	}
	
	public Vector getTp()
	{
	return tp;	
	}
	
	public Vector getNot()
	{
	return NoT;	
	}
	
	public Vector getSdc()
	{
	return sdc;	
	}
	
	public int getNum()
	{
	return num;	
	}
	
	
	public String getSpell(int i)
	{
	if(i<num&&i>=0)
	return spells[i];
	
	return "";
	}
	
	public int getUsed(int i)
	{
	if(i<num&&i>=0)
	return used[i];
	
	return -1;
	}
	
	public int getUses(int i)
	{
	if(i<num&&i>=0)
	return uses[i];
	
	return -1;
	}
	
	public void setOwner(Char c)
	{
	owner=c;	
	}
	
	public void removeDisease(int i)
	{
	tp.remove(i);	
	affected.remove(i);
	NoT.remove(i);
	sdc.remove(i);
	}
	
	
	public void sleep()
	{
	for(int i=0;i<num;i++)
	used[i]=0;	
	}
	
	 /**
	 *Updates the diseases after another turn
	 */
	public String addTurn()
	{
	int dam=0;
	String dis="";
	for(int i=0;i<tp.size();i++)
	{
	int t=Integer.parseInt((String)tp.elementAt(i));
	int n=Integer.parseInt((String)NoT.elementAt(i));
	t++;
	
		if(t>n) //> and not >=, because then it would end before it started.
		{
		removeDisease(i);
		}
		else
		{
		//tp.remove(i)
		//tp.add(i,t+"");
		tp.setElementAt(t+"",i);
		}
	}
	
	if(hasDisease("Acid Arrow"))
	{
		if(dam==0)
		dis="Acid Arrow";
		else
		dis=dis+" and Acid Arrow";
		
		dam=dam+Dice.roll(2,4);
	}
	
	
	if(hasDisease("Poison"))
	{
		if(dam==0)
		dis="Poison";
		else
		dis=dis+" and Poison";
		
		dam=dam+Dice.roll(1,6);
	}
	
	
	owner.dealDam(dam);
	
	if(dam==0)
	return "";
	else
	{	
		if(owner.isDead())
		return dam+" damage was dealt to "+owner.name()+" due to "+dis+" and he died.";
		else
			if(owner instanceof Character)
			{
				if(((Character)(owner)).isDying())
				return dam+" damage was dealt to "+owner.name()+" due to "+dis+" and he is dying.";
				else
				return " {"+dam+" damage was dealt to "+owner.name()+" due to "+dis+"}";
			}
			else
			return " {"+dam+" damage was dealt to "+owner.name()+" due to "+dis+"}";
	}
	
	
	}
	
	
	public String canPlay() //if concious: "", else the reason ; in progress
	{
	if(hasDisease("Sleep")) //exm
	return "Sleep";	
	
	if(hasDisease("Paralysis")) 
	return "Paralysis";	
	
	
	return "";	
	} //can play
	
	
	
	
	 /**
	 *Update the spells by a character's ability scores
	 */
	public void updateSpells() //only for characters; in progress
	{
	Character player=(Character)(owner);
	int level=player.lvl();
	
	//do seperated classes? -more efficient.
	
	for(int i=0;i<num;i++)
	{
	if(spells[i].equals("Rage"))
	{
		uses[i]=level/4+1;
		turns[i]=5+player.con();
		if(hasDisease("Rage"))
		turns[i]=turns[i]-2;
	}
	
	//paladin
	
	if(player.getClas().equals("Paladin"))
	{
			
		if(spells[i].equals("Remove Disease"))
		{
			uses[i]=level/3;
		}
		
		if(spells[i].equals("Turn Undead"))
		{
			uses[i]=player.cha()+3;
			if(level>4)
			uses[i]=uses[i]+(level/4-1)*4;
		}
		
		if(spells[i].equals("Lay On Hands"))
		{
			uses[i]=player.cha()+level;
		}
		
		if(spells[i].equals("Cure Light Wounds"))
		{
			int wis=player.wis();
			if(wis<1)
			wis=1;
			uses[i]=wis;
			uses[i]=uses[i]+level/6;
		}
		
		if(spells[i].equals("Protection"))
		{
			int wis=player.wis();
			if(wis<1)
			wis=1;
			uses[i]=wis;
			if(level>=10)
			uses[i]=uses[i]+(level/5-1);
		}
		
		if(spells[i].equals("Cure Moderate Wounds"))
		{
			int wis=player.wis();
			if(wis<1)
			wis=1;
			uses[i]=wis;
			if(level>=12)
			uses[i]=uses[i]+(level/4-2);
		}
		
		if(spells[i].equals("Holy Sword"))
		{
			int wis=player.wis();
			if(wis<1)
			wis=1;
			uses[i]=wis;
			if(level>=15)
			uses[i]=uses[i]+(level/3-4);
			
			turns[i]=level;
		}
	}
	//-paladin
	
	
	//sorcerer
	/*
	if(spells[i].equals("Distrup Undead"))
	{
		uses[i]=1;
		if(level>=2)
		uses[i]++;		
	}
	
	if(spells[i].equals("Magic Missile"))
	{
		uses[i]=2;
		if(level>=2)
		uses[i]++;		
	}
	
	if(spells[i].equals("Resistance"))
	{
		uses[i]=1;
		if(level>=3)
		uses[i]++;		
	}
	*/ //not needed?
	
	//-sorcerer
	
	if(spells[i].equals("Stunning Attack"))
	{
		uses[i]=level;
	}
	
	if(spells[i].equals("Acid Arrow"))
	{
		int b=level/3;
		if(b>6)
		b=6;
		turns[i]=1+b;
	}
	
	if(spells[i].equals("Haste"))
	{
		turns[i]=level;
	}
	
	//ranger
	if(player.getClas().equals("Ranger"))
	{		
		if(spells[i].equals("Cure Poison"))
		{
			uses[i]=player.wis();
		}
		
		if(spells[i].equals("Sleep"))
		uses[i]=player.wis();
		
		if(spells[i].equals("Magic Fang"))
		uses[i]=player.wis();
		
		if(spells[i].equals("Wind"))
		uses[i]=player.wis();
	}	
	
	
	if(spells[i].equals("Wholeness of Body"))
	{
			uses[i]=level*2;
	}
	
	
	}//for
	} //update spells
	
	
	public int serial(String sp)
	{
	for(int i=0;i<spells.length;i++)
	if(spells[i].equals(sp))
	return i;
	
	return -1;	
	}
	
	
	public int affectedSerial(String aff)
	{
	for(int i=0;i<affected.size();i++)
	if(((String)(affected.elementAt(i))).equals(aff))
	return i;
	
	return -1;	
	}
	
	
	public int getSpellDc(int i)
	{
	int bon=owner.inl();
		if(owner instanceof Character)
		{
		Character player=(Character)(owner);
		if(player.getClas().equals("Ranger"))
		bon=player.wis();
		if(player.getClas().equals("Paladin"))
		bon=player.wis();
		if(player.getClas().equals("Sorcerer"))
		bon=player.cha();
		}
	return 10+lvl[i]+bon;		
	}
	
	
	
	//lay on hands, like wholeness of body
/*
	public static boolean target(String s) //need target
	{
	if(s.equals("Resistance"))
	return false;
	if(s.equals("Rage"))
	return false;
	if(s.equals("Uncanny Dodge"))
	return false;
	if(s.equals("Protection"))
	return false;
	if(s.equals("Holy Sword"))
	return false;	
	if(s.equals("Skill Mastery")) //no "defensive roll"
	return false;	
	if(s.equals("Bull's Strength"))
	return false;	
	if(s.equals("Haste"))
	return false;	
	if(s.equals("Resist"))
	return false;	
	if(s.equals("Wind"))
	return false;
	if(s.equals("Wholeness of Body"))
	return false;
	if(s.equals("Cure Poison"))
	return false;	
	if(s.equals("Cure Light Wounds"))
	return false;
	if(s.equals("Cure Serious Wounds"))
	return false;
	if(s.equals("Cure Moderate Wounds"))
	return false;
	
	return true;	
	}
	
	public static boolean fight(String s) //not in fight
	{
	if(s.equals("Lay On Hands"))
	return false;
	//if(s.equals("Rage"))
	//return false;
	if(s.equals("Uncanny Dodge")) //or permanent?
	return false;
	if(s.equals("Skill Mastery"))
	return false;
	if(s.equals("Wholeness of Body"))
	return false;
	
	return true;
	}
	*/
	
	
	public static boolean target(String s) //need target
	{
	if(self(s))
	return false;
	
	if(s.equals("Cure Poison"))
	return false;	
	if(s.equals("Cure Light Wounds"))
	return false;
	if(s.equals("Cure Serious Wounds"))
	return false;
	if(s.equals("Cure Moderate Wounds"))
	return false;
	
	return true;	
	}
	
	
	public static boolean self(String s) //not healing
	{
		if(s.equals("Rage"))
		return true;
		if(s.equals("Resistance"))
		return true;
		if(s.equals("Protection"))
		return true;
		if(s.equals("Bull's Strength"))
		return true;
		if(s.equals("Haste"))
		return true;
		if(s.equals("Resist"))
		return true;
		if(s.equals("Magic Fang"))
		return true;
		if(s.equals("Wind"))
		return true;
		if(s.equals("Holy Sword"))
		return true;
		
		//rage,resistance.protection,bull's strength,haste,resist,magic fang,wind,holy sword
		
	return false;	
	}
	
	
	 /**
	 *Uses a spell on an enemy
	 *@param sps : index of spell
	 *@param enm : the enemy
	 */
	public String useSpell(int sps,Char enm) //in progress
	{
	//attacks++ before\after this (when i write this.) . or here? (instance)
	
	//String s="";
	
	//lvls! enemy\chars!
	
	String sp=getSpell(sps);
	//int lv=lvl[sps];
	//int us=uses[sps];
	//int ud=used[sps];
	//int tu=turns[sps];
	//int sa=save[sps];
	int dam=0;
	boolean attack=false;
	boolean abil=false; //on self.
	boolean spat=false; //spell attack
	boolean off=false; //offensive disease
	
	int sdcc=0; //spell dc class.
	
	//affected,NoT,tp; //name,number of turns, turns passed
	int level=0;
	
	if(owner instanceof Character)
	level=((Character)owner).lvl();
	
	if(owner instanceof Enemy)
	level=(int)((Enemy)owner).getLvl();
	
	if(owner.getClass().toString().equals("class Char"))
	level=(owner.wis()+owner.inl())/2;
	
	//char
	
		
	
	if(sp.equals("Rage"))
	{
		used[sps]++;
		abil=true;
	}
	
	if(sp.equals("Sneak Attack")) //for characters
	{
		spat=true;
		/*
		 Character player=(Character)(owner);
		int bonDam=Dice.roll(((player.lvl()+1)/2),6);
		String at=player.attack(enm);
		
		int sb=at.indexOf("attacks");
		at=at.substring(0,sb)+"uses "+sp+at.substring(sb+7);
		
		int d=at.indexOf("deals");
		
		if(d!=-1)
		{
		int m=at.indexOf(" damage");
		String da=at.substring(d+6,m);
		da=(Integer.parseInt(da)+bonDam)+"";
		enm.dealDam(bonDam); //no damr
		at=at.substring(0,d+6)+da+at.substring(m);
		}
		
		
		
		return player.attack(enm,sp);
		*/
	}
	
	if(sp.equals("Lay On Hands")) //can heal himself? (can, but will i do it?)
	{
		//can choose how much to heal?
		
		used[sps]++;
		
		if(enm.race().equals("Undead"))
		{
		enm.dealDam(1);
		return owner.name()+" uses Lay On Hands deals 1 damage to "+enm.name()+".";
		}
		else
		{
		enm.heal(1);
		return owner.name()+" uses Lay On Hands and heals 1 damage from "+enm.name()+".";
		}
	}
		
	if(sp.equals("Distrup Undead"))
	{
		used[sps]++;
		
		if(enm.race().equals("Undead"))
		{
			dam=Dice.roll(6);
			attack=true;
		}
		else
		return owner.name()+" fails to use "+sp+" on "+enm.name()+" because he is not an Undead.";	
	}
	
	
	if(sp.equals("Ray Of Frost"))
	{
		used[sps]++;
		
		dam=Dice.roll(3);
		attack=true;
	}
	
	if(sp.equals("Resistance"))
	{
		used[sps]++;
		abil=true;	
	}
	
	if(sp.equals("Magic Missile"))
	{
		used[sps]++;
		//Character player=(Character)(owner);
		int l=(level+1)/2;
		if(l>5)
		l=5;
		
		dam=Dice.roll(l,4,l);
		
		attack=true;
	}
	
	
	if(sp.equals("Stunning Attack")) //for characters
	{
		used[sps]++;
		spat=true;
		/*
		Character player=(Character)(owner);
		
		String at=player.attack(enm);
		
		int sb=at.indexOf("attacks");
		at=at.substring(0,sb)+"uses "+sp+at.substring(sb+7);
		
		int de=at.indexOf("deals");
		
		if(de!=-1) //hit
		{
			int dc=10+player.wis()+player.lvl()/2;
			int d=Dice.roll(20);
			int dd=d+enm.getFort();
			
			if(!((dd>=dc&&d!=1)||d==20)) //stun
			{
				enm.getSpells().affected.add("Paralysis");
				enm.getSpells().tp.add("0");
				enm.getSpells().NoT.add("1");
				at=at.substring(0,at.length()-1)+" and stuns him ["+dd+"<"+dc+" or 1 on dice]. ";
			}
			else
			at=at.substring(0,at.length()-1)+" and fails to stun him ["+dd+">="+dc+" or 20 on dice]. ";
		}
		
		return at;
		*/
	}
	
		
	if(sp.equals("Smite Evil"))
	{
		used[sps]++;
		
		if(enm instanceof Enemy)
		{
			spat=true;
		}
		else
		return owner.name()+" fails to use "+sp+" on "+enm.name()+" because he is not evil (an Enemy).";	
	}
	
	
	if(sp.equals("Remove Disease")) //in progres
	{
		used[sps]++;
		
		
		for(int i=0;i<affected.size();i++)
		{
			String s=(String)affected.elementAt(i);
			boolean remove=false;
			
			if(s.equals("Sleep"))
			remove=true;
			if(s.equals("Paralysis"))
			remove=true;
			if(s.equals("Poison"))
			remove=true;
			if(s.equals("Acid Arrow"))
			remove=true;
			
				if(remove)
				{
				removeDisease(i);			
				}
			
		}
		
		String n=enm.name();
		if(enm==owner)
		{
			if(owner.male())
			n="himself";
			else
			n="herself";
		}
		
		
		return owner.name()+" uses "+sp+" and remove all bad effects from "+n+".";//affects? 		
	}
	
	
	if(sp.equals("Turn Undead"))
	{
		used[sps]++;
		
		Character player=(Character)(owner);
		
		if(enm.race().equals("Undead"))
		{
			dam=Dice.roll(6)+(player.lvl()-2)+owner.cha();
			attack=true;
		}
		else
		return owner.name()+" fails to use "+sp+" on "+enm.name()+" because he is not an Undead.";	
	}
	
	if(sp.indexOf("Wounds")!=-1) //because cure poison
	{
		used[sps]++;
		
		Character player=(Character)(owner);
		int h=0;
		int ll=0;
		int f=0;
		
		if(sp.indexOf("Light")!=-1)
		{
			ll=5;
			f=1;
		}
		
		if(sp.indexOf("Moderate")!=-1)
		{
			ll=10;
			f=2;
		}
		
		if(sp.indexOf("Serious")!=-1)
		{
			ll=15;
			f=3;
		}
		
		int l=player.lvl();
		if(l>ll)
		l=ll;
		h=Dice.roll(f,8)+l;						
		
		String n=enm.name();
		if(enm==owner)
		{
			if(owner.male())
			n="himself";
			else
			n="herself";
		}
		
		enm.heal(h);
		
		return owner.name()+" uses "+sp+" on "+enm.name()+" and heals "+h+" damage.";
	}
	
	
	if(sp.equals("Protection"))
	{
		used[sps]++;
		abil=true;
	}
	
	if(sp.equals("Holy Sword"))
	{
		used[sps]++;
		abil=true;
	}
	
	if(sp.equals("Sleep"))
	{
		used[sps]++;
		off=true;
		/*
		int bon=0;
		if(player.getClas().equals("Ranger"))
		bon=player.wis();
		if(player.getClas().equals("Paladin"))
		bon=player.wis();
		if(player.getClas().equals("Sorcerer"))
		bon=player.cha();
		*///sorcerer
		//sdcc=10+lvl[sps]+owner.cha();
		sdcc=getSpellDc(sps);
	}
	
	
	if(sp.equals("Acid Arrow"))
	{
		used[sps]++;
		off=true;
		
		sdcc=10+lvl[sps]+owner.cha();
	}
	
	if(sp.equals("Bull's Strength"))
	{
		used[sps]++;
		abil=true;
		
		sdcc=Dice.roll(1,4,1);
	}
	
	
	if(sp.equals("Lightning Bolt"))
	{
		used[sps]++;
		//Character player=(Character)(owner);
		int l=level;
		if(l>10)
		l=10;
		
		dam=Dice.roll(l,6);
		
		attack=true;
	}
	
	
	if(sp.equals("Haste"))
	{
		used[sps]++;
		abil=true;
	}
	
	if(sp.equals("Ice Storm"))
	{
		used[sps]++;
		
		dam=Dice.roll(5,6)*2;
		
		attack=true;
	}
	
	
	if(sp.equals("Cure Poison")) //only poison?
	{
		used[sps]++;
		for(int i=0;i<affected.size();i++)
		{
		String n=(String)(affected.elementAt(i));
		if(n.equals("Poison"))	//more?
		removeDisease(i);
		}
		
		
		String n="";
		if(owner.male())
		n="himself";
		else
		n="herself";
					
		return owner.name()+" uses "+sp+" on "+n+".";
	}
	
	
	if(sp.equals("Resist"))
	{
		used[sps]++;
		abil=true;
	}
	
	
	if(sp.equals("Magic Fang"))
	{
		used[sps]++;
		abil=true;
	}
	
	if(sp.equals("Wind"))
	{
		used[sps]++;
		abil=true;
	}
	
	
	if(sp.equals("Wholeness of Body"))
	{
		//can choose how much to heal?
		used[sps]++;
		
		enm.heal(1);
		
		String n="herself";
		if(owner.male())
		n="himself";
					
		return owner.name()+" uses Wholeness of Body and heals 1 damage from "+n+".";
	}
	
	
	if(sp.equals("Quivering Palm"))
	{
		used[sps]++;
		spat=true;
	}
			
	
	if(sp.equals("Poison"))
	{
		used[sps]++;
		off=true;
		sdcc=getSpellDc(sps);
	}
	
	if(sp.equals("Paralysis"))
	{
		used[sps]++;
		off=true;
		sdcc=getSpellDc(sps);
	}
	
	
	if(sp.equals("Drain"))
	{
		used[sps]++;
		
		int d=Dice.roll(6);
		owner.heal(d);
		enm.dealDam(d);
		
		String h="herself";
		if(owner.male())
		h="himself";
						
		return owner.name()+" uses "+sp+" and drains "+d+" damage from "+enm.name()+" to "+h+".";
	}
	
	
	int fire=0;
	
	
	if(sp.equals("Breath Acid"))
	{
		used[sps]++;
		fire=Dice.roll(10,4);			
	}
	
	if(sp.equals("Breath Fire"))
	{
		used[sps]++;
		fire=Dice.roll(8,10);			
	}
	
	
	//general:
	
	
	if(fire!=0)
	{
	int dc=getSpellDc(sps);
	
	if(enm.save("Reflex",dc))
	return owner.name()+" uses "+sp+" but "+enm.name()+" succeeds his saving throw.";		
	else
	{
	fire-=enm.getDamr();
		if(fire<0)
		fire=0;
	enm.dealDam(fire);	
	
	return owner.name()+" uses "+sp+" and deals "+fire+" damage to "+enm.name()+".";			
	}
	
	}
	
	
	if(spat) //characters
	{
		Character player=(Character)(owner);
		return player.attack(enm,sp);
	}
	
	if(attack) //only for characters now, need to change.
	{
	//save: 0-none,1-ref,2-fort,3-will
		//Character player=(Character)(owner); //(instance, int lvl)
		Char player=owner;
		
		int con=(level+3)+player.con(); //concentration
				
		int d=Dice.roll(20);
		int dd=d+con;
				
		int m=player.getDam();
		int h=player.getHp();
		int p=m*100/h; //per cent of m from h;		
			
		int dc=10+lvl[sps]+p/10;   //damage adds 1 to 10
		
		if(lvl[sps]<=0) //shouldn't concentrate one a level 1 spell...
		d=20;
		
		boolean conc=((dd>=dc&&d!=1)||d==20);
			
		if(conc)
		{
			
		if(!owner.spellFailure())
		{
			//spell resistance
			boolean hit=false;
			
			if(enm.getSr()==0)
			hit=true;
			else
			{
			int dc3=enm.getSr();
			int d3=Dice.roll(20);
			int dd3=d3+level;
			
			if((dd3>=dc3&&d3!=1)||d3==20)
			hit=true;
			else
			return owner.name()+" uses "+sp+" and "+enm.name()+" resisted the spell ["+dd3+"<"+dc3+" or 1 on dice].";		
					
			}
			
			if(hit)
			{
				if(save[sps]==0)
				{
				enm.dealDam(dam);
				return owner.name()+" uses "+sp+" and deals "+dam+" damage to "+enm.name()+" ["+dd+">="+dc+" or 20 on dice].";	
				}
				else //saving throw
				{
					int sav=0;
					if(save[sps]==1)
					sav=enm.getRef();
					if(save[sps]==2)
					sav=enm.getFort();
					if(save[sps]==3)
					sav=enm.getWill();
									
					int d2=Dice.roll(20);
					int dd2=d2+sav;
					int dc2=getSpellDc(sps);
					
					boolean saving=((dd2>=dc2&&d2!=1)||d2==20);
						
						if(!saving)
						{
						enm.dealDam(dam);
						return owner.name()+" uses "+sp+" and deals "+dam+" damage to "+enm.name()+" ["+dd+">="+dc+" or 20 on dice].";		
						}
						else
						return owner.name()+" uses "+sp+" and "+enm.name()+" succeeds his saving throw ["+dd2+">="+dc2+" or 20 on dice].";		
				}
			}
		}
		else
		return owner.name()+" fails to use "+sp+" on "+enm.name()+" because his shield or armor interfered.";	
		}
		else
		return owner.name()+" fails to use "+sp+" on "+enm.name()+" due to lack of concentration ["+dd+"<"+dc+" or 1 on dice].";	
				
	}
	
	if(abil)
	{
	//Character player=(Character)(owner); //(instance, int lvl)
	Char player=owner;
	
		boolean conc;
	
		int con=(level+2)+player.con(); //concentration
				
		int d=Dice.roll(20);
		int dd=d+con;
				
		int m=player.getDam();
		int h=player.getHp();
		int p=m*100/h; //per cent of m from h;		
		p/=10;
		p-=2;
		if(p<0)
		p=0;
			
		int dc=10+lvl[sps]+p;   //damage adds 0 to 8
		
		if(lvl[sps]<=0) //shouldn't concentrate one a level 1 spell...
		d=20;
		
		conc=((dd>=dc&&d!=1)||d==20);
				
		if(conc)
		{
		
		if(!owner.spellFailure())
		{
		affected.add(sp);
		NoT.add(turns[sps]+"");
		tp.add("0");
			
		sdc.add(""+sdcc);
						
		return owner.name()+" uses "+sp+".";
		}
		else
		return owner.name()+" fails to use "+sp+" because his shield or armor interfered.";
		
		}
		else
		return owner.name()+" fails to use "+sp+" due to lack of concentration.";
	}
	
	
	if(off)
	{
	//Character player=(Character)(owner); //(instance, int lvl)
	Char player=owner;
	
	
	boolean conc;
	
		int con=(level+3)+player.con(); //concentration
				
		int d=Dice.roll(20);
		int dd=d+con;
				
		int m=player.getDam();
		int h=player.getHp();
		int p=m*100/h; //per cent of m from h;		
		p/=10;
		p--;
		if(p<0)
		p=0;
			
		int dc=10+lvl[sps]+p;   //damage adds 0 to 9
		
		//if(lvl[sps]<=0) //shouldn't concentrate one a level 1 spell...?
		//d=20;
		
		conc=((dd>=dc&&d!=1)||d==20);
			
		if(conc)
		{
		
		if(!owner.spellFailure())
		{
		
			int sav=0;
			if(save[sps]==1)
			sav=enm.getRef();
			if(save[sps]==2)
			sav=enm.getFort();
			if(save[sps]==3)
			sav=enm.getWill();
					
			int d2=Dice.roll(20);
			int dd2=d2+sav;
			int dc2=sdcc;
					
			boolean saving=(((dd2>=dc2&&d2!=1)||d2==20))&&sav!=0;
			
			if(!saving)
			{
			boolean im=false;
			
				if(enm instanceof Character)
				{
				Character enmy=(Character)(enm);
					if(enmy.getClas().equals("Monk"))
						if(enmy.lvl()>=5)
						im=true;	
				}
			
				if(!im)
				{
				enm.getSpells().affected.add(sp);
				enm.getSpells().NoT.add(turns[sps]+"");
				enm.getSpells().tp.add("0");
			
				enm.getSpells().sdc.add(sdcc+""); //not yet used.
			
				//sdcc in "disease".
			
				return owner.name()+" uses "+sp+" on "+enm.name()+".";
				}
				else
				return owner.name()+" uses "+sp+" but "+enm.name()+" is immune to diseases.";
			}
			else
			return owner.name()+" uses "+sp+" and "+enm.name()+" succeeds his saving throw.";
		}
		else
		return owner.name()+" fails to use "+sp+" on "+enm.name()+" because his shield or armor interfered.";
		
		}
		else
		return owner.name()+" fails to use "+sp+" on "+enm.name()+" due to lack of concentration.";
	}
	
	
	
	
	
	
	
	
	return "";	//s
	}
	
	
	
	/**
	*Connverts the spells to a String (coded)
	*/
	public String write() //char?
	{
	String s="L*";
	
	s+=num+",";
	
	for(int i=0;i<num;i++)
	{
	s+=spells[i]+",";
	s+=lvl[i]+",";
	s+=uses[i]+",";
	s+=used[i]+",";
	s+=turns[i]+",";
	s+=save[i]+",";
	
	//s+=";";
	}
	
	for(int i=0;i<affected.size();i++)
	{
	s+=affected.elementAt(i)+",";
	s+=NoT.elementAt(i)+",";
	s+=tp.elementAt(i)+",";
	s+=sdc.elementAt(i)+",";
	s+=";";
	}
		
	
	return s+"*L";
	}
	
	
	/**
	*Returns spells by a coded String
	*@param s : the coded String
	*/
	public static Spells read(String s)
	{
	Spells d=new Spells();
	
	s=s.substring(2);
	
	int b=s.indexOf(",");
	d.num=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	if(d.num!=0)
	{	
	d.spells=new String[d.num];
	d.lvl=new int[d.num];
	d.uses=new int[d.num];
	d.used=new int[d.num];
	d.turns=new int[d.num];
	d.save=new int[d.num];
	}
		
	for(int i=0;i<d.num;i++)
	{
	b=s.indexOf(",");
	d.spells[i]=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.lvl[i]=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.uses[i]=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.used[i]=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.turns[i]=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.save[i]=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	}
	
	while(s.indexOf(";")!=-1)
	{
	b=s.indexOf(",");
	d.affected.add(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.NoT.add(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.tp.add(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.sdc.add(s.substring(0,b));
	s=s.substring(b+2);
	}
	
		
	return d;
	}
	
	/*
	private Vector affected,NoT,tp; //strings. number of turns, turns passed
	*/
	
	
	
	
	/*public String getSpell(String n)
	{
	String s="";
	for(int i=0;i<num;i++)
	if(n.equals(spells[i]))
	s=spells[i];
			
	return s;
	}*/
	
	/*	
	private int lvl[],uses[],used[],turns[];
	private int num;	
	*/
	
	public String string() //disease
	{
	String s="Affected by: ";	
	
	int size=affected.size();
	
	if(size==0)
	s+="Nothing.";		
	else
	{
		for(int i=0;i<size;i++)
		{
		String dis=(String)(affected.elementAt(i));	
		s+=dis+", ";		
		}
	s=s.substring(0,s.length()-2)+".";		
	}		
		
	return s;
	}
	
}