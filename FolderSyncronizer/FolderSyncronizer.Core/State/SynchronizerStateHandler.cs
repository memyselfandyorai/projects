﻿using FolderSyncronizer.Core.Watchers;
using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;

namespace FolderSyncronizer.Core.State
{
    public class SynchronizerStateHandler
    {
        private readonly DataContractSerializer _serializer;

        public SynchronizerStateHandler()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                            .Where(assembly => assembly.GetName().Name.StartsWith("FolderSyncronizer.Core"))
                            .ToList();
            var actionTypes = assemblies.SelectMany(a => a.GetTypes().Where(t => !t.IsInterface).Where(typeof(IFileAction).IsAssignableFrom)).ToList();
            _serializer = new DataContractSerializer(typeof(SynchronizerSnapshot), actionTypes);
        }

        public void Save(string fileName, SynchronizerSnapshot snapshot)
        {
            using (StringWriter sw = new StringWriter())
            using (XmlTextWriter xw = new XmlTextWriter(sw))
            {
                _serializer.WriteObject(xw, snapshot);
                File.WriteAllText(fileName, XElement.Parse(sw.ToString()).ToString());
            }
        }

        public SynchronizerSnapshot Load(string fileName)
        {
            using (var writer = File.OpenRead(fileName))
            {
                return (SynchronizerSnapshot)_serializer.ReadObject(writer);
            }
        }
    }
}
