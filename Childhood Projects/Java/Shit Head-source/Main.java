import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Main extends JPanel implements ActionListener{
	
	private JList list;
	private Vector<Socket> players;
	private Server server;
	private JButton kick;
	private JButton start;
	
	public Main(Vector<String> names,Vector<Socket> p)
	{
	super();
	
	players=p;
	//server=s;
	
	list=new JList(names);
	list.setToolTipText("Players");
	setLayout(new BorderLayout());
	
	add(BorderLayout.CENTER,list);
	
	JPanel b=new JPanel();
	
	
	kick=new JButton("Kick");
	start=new JButton("Start Game");
	
	kick.addActionListener(this);
	start.addActionListener(this);
	
	b.add(start);
	b.add(kick);
	
	add(BorderLayout.SOUTH,b);
	//setlistdata, but vector could be enough (same vector)	
	}
	
	
	public void setButtons(boolean b)
	{
		start.setEnabled(b);
		kick.setEnabled(b);
	}
	
	public void setS(Server s)
	{
	server=s;	
	}
	
	public void set(Vector names)
	{
	list.setListData(names);	
	}
	
	public void kick(int ind)
	{
		Socket socket=(Socket)players.elementAt(ind);
		
		try{		
		BufferedWriter out=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		out.write("*KICK*\n");			
		out.flush();
		}catch(IOException ie){}
	}
	
	
	public void actionPerformed(ActionEvent e)
	{
		//in start game- how many computer players
		JButton b=(JButton)e.getSource();
		
		if(b.getText().equals("Kick")&&!Server.quiting)
		{
			int ind=list.getSelectedIndex();
			
			if(ind!=-1)
			{	
			kick(ind);				
			}
			
			
		}
		
		if(b.getText().equals("Start Game")&&!Server.quiting)
		{
			int p=players.size();
			int pc=4-p;
			int cp=0;
			
			if(p>0)
			{
				if(pc!=0)
				{
				String[] o;
				
				if(p!=1)
				{
				    o=new String[pc+1];
					for(int i=0;i<=pc;i++)
					o[i]=""+i;
				}
				else
				{
					o=new String[pc];
					for(int i=0;i<pc;i++)
					o[i]=""+(i+1);
				}
				
			
				
				cp=JOptionPane.showOptionDialog(this,"How many computer players?","Choose:",0,JOptionPane.QUESTION_MESSAGE,null,o,o[0]);
				
				if(cp==-1)
				cp=0;
								
				if(p==1)
				cp++;
				
	
					
				}
				
				setButtons(false);
				server.stg(cp);
			}
			
		}
		
		
	}
	
	
	
	
	
}