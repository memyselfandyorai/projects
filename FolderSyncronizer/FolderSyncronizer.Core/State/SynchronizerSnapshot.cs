﻿using FolderSyncronizer.Core.Data;
using FolderSyncronizer.Core.Watchers;
using System.Collections.Generic;

namespace FolderSyncronizer.Core
{
    public record SynchronizerSnapshot
    {
        public List<FolderSource> FolderSources { get; set; }
        
        public List<IFileAction> Actions { get; set; }
    }
}
