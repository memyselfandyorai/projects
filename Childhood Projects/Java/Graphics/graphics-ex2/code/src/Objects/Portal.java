package Objects;

import General.Color;
import General.Intersection;
import General.NormalVector;
import General.Plane;
import General.Point;
import General.Point2D;
import General.Ray;
import General.Scene;

/**
 * 
 * represents a portal (bonus)
 *
 */
public class Portal extends Surface{

	private Disc discIn,discOut;
	
	private boolean findingIntersection; 
	
	public Portal()
	{
		discIn=new Disc();
		discOut=new Disc();
		
		findingIntersection=false;
	}
	
	
	public void setCenter1(Point center) {
		discIn.setCenter(center);
	}
	
	
	public void setNormal1(NormalVector normal) {
		discIn.setNormal(normal);
	}

	public void setRadius1(double radius) {
		discIn.setRadius(radius);
	}
	
	
	public void setCenter2(Point center) {
		discOut.setCenter(center);
	}
	
	
	public void setNormal2(NormalVector normal) {
		discOut.setNormal(normal);
	}

	public void setRadius2(double radius) {
		discOut.setRadius(radius);
	}

	public boolean hasAllValues() {
		if(!discIn.hasAllValues())
			return false;
		if(!discOut.hasAllValues())
			return false;
		
		return true;
	}

	
	public Intersection intersect(Scene scene, Ray ray) {

		if(findingIntersection) //meaning hit itself from discOut: to avoid infinite loops
			return Intersection.MISS; 
		
		Intersection inter=discIn.intersect(scene, ray);
		if(inter==Intersection.MISS)
			return Intersection.MISS;
						
		Point hit=inter.getHit();
		
		double distance=inter.getDistance(); //distance between eye and disc
		
		//r: raduius, c: center
		double rIn=discIn.getRadius();
		Point cIn=discIn.getCenter();
		double rOut=discOut.getRadius();
		
		NormalVector inNormal=discIn.getNormal();
		NormalVector outNormal=discOut.getNormal();
		
		NormalVector position=NormalVector.subPoints(hit, cIn);
		
		//computes relative point:
		NormalVector relativePosition=getRelativeVector(inNormal,position,outNormal);
		double distanceIn=cIn.distance(hit);
		
		double distanceOut=(distanceIn/rIn)*rOut;
		
		//relative point:
		Point outHit=Ray.getPoint(discOut.getCenter(),relativePosition,distanceOut);
		
		NormalVector outVector=getRelativeVector(inNormal,ray.getVector(),outNormal);
			
		
		//System.out.println(position+"   VSVEC   "+relativePosition);
		//System.out.println(hit+"   VS   "+outHit);
		
		//System.out.println(ray.getVector()+"  MMOOOOO   "+outVector);
		
		//outVector=new NormalVector(0,-1,0);
		
		Ray newRay=new Ray(outHit,outVector);
		
		findingIntersection=true;
		//System.out.println("moo");
		Intersection newInter=scene.findIntersection(newRay);
				
		findingIntersection=false;
		if(newInter!=Intersection.MISS)
			newInter.setDistance(distance);
		
		return newInter;
	}

	
	/**
	 * returns the vector relative to N2 as dir is to N
	 * 
	 * 
	 * the method is wrong, i couldn't find anywhere how to do it 
	 * 
	 * */
	private static NormalVector getRelativeVector(NormalVector N,NormalVector dir,NormalVector N2)
	{
		NormalVector result;
		
		NormalVector NXdir=N.crossProduct(dir);
		
		result=NXdir.crossProduct(N2);
		
		//return result.negative();
		return result.negative();
	}
	
	
	
	
	/**
	 * returns the color of the hit's object
	 * */
	public Color getColor(Scene scene,Intersection intersection){
		
		return intersection.getObject().getColor(scene,intersection);
	}
	
	
	public boolean onRight(Plane plane)
	{
		return discIn.onRight(plane);
	}
	
	
	/**
	 * shouldn't get here!!
	 * */
	public Point2D parametrization(Point point) {
		
		return null;
	}
	

}
