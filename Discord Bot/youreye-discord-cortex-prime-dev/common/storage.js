const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});
const {CacheObject} = require('./cache.js');

const KEY_SEPARATOR = "."
const STORAGE_PREFIX = "STORAGE:"

const CURRENT_STORE_VERSION = "v1"

// Assumption: keyList values do not contain KEY_SEPARATOR.
function getKeyFromList(keyList) {
  return (keyList ?? []).join(KEY_SEPARATOR)
}

function getFullKeyFromList(keyList) {
  return STORAGE_PREFIX + getKeyFromList(keyList)
}


// Returns a key list
function getKeyListFromKey(key) {
  if (!key) {
    return []
  }
  return key.split(KEY_SEPARATOR)
}

const ROOT = "root"
const END = "end"

function wrapKey(key) {
  // Add separator to avoid something like "type" finding "type2".
  return wrapKeyPrefix(key) + END
}

function wrapKeyPrefix(keyPrefix) {
  let suffix = keyPrefix ? `${keyPrefix}${KEY_SEPARATOR}` : ''
  return `${ROOT}${KEY_SEPARATOR}${suffix}`
}

function getKeyFromWrapped(wrappedKeyPrefix, storageKey) {
  let suffix = wrappedKeyPrefix ? `${wrappedKeyPrefix}${KEY_SEPARATOR}` : ''
  let suffixKey = wrapKey(storageKey).slice(wrappedKeyPrefix.length).slice(0, -END.length)
  return suffixKey.endsWith(KEY_SEPARATOR) ? suffixKey.slice(0, -KEY_SEPARATOR.length) : suffixKey
}


// Returns value with metadata.
function wrapValue (value) {
  return {
    obj: value,
    timestampUtc: new Date().toUTCString(),
    version: CURRENT_STORE_VERSION ,
  }
}

function unwrapValue(valueWithMetadata) {
  if(valueWithMetadata?.version) {
    switch(valueWithMetadata.version) {
      case CURRENT_STORE_VERSION:
        return valueWithMetadata.obj
    }
  }
  // v0
  return valueWithMetadata
}

class Storage {
  // cacheObject has the same API as IocContainer.cache suggests.
  constructor(cacheObject) {
    this.cacheObject = cacheObject
  }
  
  // keyList - list of keys
  // void
  // throws CacheLimitationError
  async storeValue(keyList, value) {
    let key = getFullKeyFromList(keyList)
    let valueWithMetadata = wrapValue(value)
    await this.cacheObject.cacheValue(key, valueWithMetadata)
  }
  
  // keyList - list of keys to return all their subkeys (including self).
  // options: { rawValue - true to return raw stored values}
  // returns a list of all values under keyList: {keyListSuffix - the remainder keyList , value}
  async getStoredValues(keyList, options = undefined) {
    let key = getKeyFromList(keyList)    
    let keyPrefix = wrapKeyPrefix(key)
    
    let unwrapValueFunc = v => unwrapValue(v)
    if(options?.rawValue) {
      unwrapValueFunc = v => v
    }
    
    let entries = await this.cacheObject.getCacheEntries()
    let values = entries
      .filter(kv => (typeof kv.key === 'string') && kv.key.startsWith(STORAGE_PREFIX))
      .map(kv => ({key: kv.key.slice(STORAGE_PREFIX.length), value: kv.value}))
      .filter(kv => wrapKey(kv.key).startsWith(keyPrefix))
      .map(kv => ({keyListSuffix: getKeyListFromKey(getKeyFromWrapped(keyPrefix, kv.key)) , value: unwrapValueFunc(kv.value)}))
    
    return values
  }
  
  // returns a single value or defaultValue
  async getStoredValue(keyList, defaultValue = undefined) {
    let key = getFullKeyFromList(keyList)
    let result = await this.cacheObject.getCacheValue(key, defaultValue)  
    return result != defaultValue ? unwrapValue(result) : defaultValue 
  }
  
  // options: { rawValue - true to return raw stored values}
  // Returns list of {keyList, value} objects.
  async getAllStoredEntries(options = undefined) {
    let entries = await this.getStoredValues([], options)
    return entries.map(kv => ({keyList: kv.keyListSuffix, value: kv.value}))
  }
  
  // Clears key and all its sub children (i.e., keyList of [1,2] deletes everything starting with [1,2])
  // Returns all cleared suffixes (list of string list).
  async clearKeys(keyList) {
    let entries = await this.getStoredValues(keyList)
    let allSuffixes = entries.map(kv => kv.keyListSuffix)
    let allKeys = allSuffixes.map(suffixList => keyList.concat(suffixList)).map(list => getFullKeyFromList(list))
    for(let key of allKeys) {      
      await this.cacheObject.clearCacheKey(key)
    }
    
    return allSuffixes
  }
  
}



module.exports = {
  KEY_SEPARATOR: KEY_SEPARATOR,
  Storage: Storage,
  /*StorageObject: {
    storeValue: storeValue,
    getStoredValues: getStoredValues,  
    getStoredValue: getStoredValue,   
    getAllStoredEntries: getAllStoredEntries,     
    clearKeys: clearKeys,     
  },*/
};