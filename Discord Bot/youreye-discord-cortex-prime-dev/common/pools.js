const {FormatInputDicePool}= require('./messages.js');
const {StepUp}= require('./roller.js');
const _ = require('lodash')

const STEP_MOD_UP = 1
const STEP_MOD_DOWN = -1

const POOL_ADD = 1
const POOL_REMOVE = -1
const POOL_CLEAR = -100

const ALL_NAMES = "all" // instead of a list

// Stored value: tuple of { dice - list of dice for pool }
const STORAGE_NAME = "POOL"

/*
input: {
  channelId, userId,
}
iocContainer: IocContainer
Output:{
  messageContent - list of messages,
  hasPools - boolean,
} 
*/
async function showPools(input, iocContainer){
  let parentKey = getAllPoolsKey(input.channelId)
  // Each entry: {keyListSuffix - the remainder keyList , value}
  let entries = await iocContainer.storage.getStoredValues(parentKey)
  let messageContent = []
  for(let entry of entries) {
    let poolName = entry.keyListSuffix[0]
    let dice = entry.value.dice
    messageContent.push(`*${poolName}*: ${FormatInputDicePool(dice)}`)
  }  
  
  if (messageContent.length <= 0) {
    messageContent.push(`No pools.`)
    return {
      messageContent: messageContent,
      hasPools: false,
    }
  }
  
  return {
    messageContent: messageContent,
    hasPools: true,
  }
}


/*
input: {
  name, action- add or remove, dice - list,
  channelId, userId,
}
iocContainer: IocContainer

Returns {
    success - boolean,
    messageContent - list of messages,
  }
*/
async function modifyPool(input, iocContainer) {
  let key = getStorageKey(input.name, input.channelId)
  let existingPool = await iocContainer.storage.getStoredValue(key, { })
  
  let newPool = existingPool.dice ?? []
  let messageContent = []
  let dice = input.dice
  let diceFormat = FormatInputDicePool(dice)
  switch (input.action) {
    case POOL_ADD:
      newPool = newPool.concat(dice)
      if (existingPool.dice) {
        messageContent.push(`Added to pool '${input.name}': ${diceFormat}. New pool: ${FormatInputDicePool(newPool)}.`)      
      } else {
        messageContent.push(`Created new pool '${input.name}': ${diceFormat}.`)      
      }
      
      break
    case POOL_REMOVE:
      if(!existingPool.dice) {
        return {
          success: false,
          messageContent: [`Pool not found: '${input.name}'.`],
        }
      }      
      
      let removeResult = removeFromDice(newPool, dice)
      if (removeResult.notFound.length > 0) {
        return {
          success: false,
          messageContent: [`Dice '${FormatInputDicePool(removeResult.notFound)}' are missing from pool '${input.name}'.`],
        }
      }
      
      newPool = removeResult.newPool
      let removedFormat = removeResult.actuallyRemoved.length > 0 ? FormatInputDicePool(removeResult.actuallyRemoved) : 'none'
      messageContent.push(`Removed from pool '${input.name}': ${removedFormat}. New pool: ${FormatInputDicePool(newPool)}.`)      
      break
    default:
      return {
        success: false,
        messageContent: [`No such modify-pool command: ${input.action}.`],
      }
  }
  
  existingPool.dice = newPool
  await iocContainer.storage.storeValue(key, existingPool)
  return {
    success: true,
    messageContent: messageContent,
  }
}

/*
return: {
  newPool - list of dice left in the pool,
  actuallyRemoved - list of dice found and removed,
  notFound - list of dice not found,
}
*/
function removeFromDice(dicePool, toRemove) {
  let newPool = [...dicePool]
  let actuallyRemoved = []
  let notFound = []
  for(let die of toRemove) {
    let index = newPool.indexOf(die)
    if (index >= 0) {
      newPool.splice(index, 1);
      actuallyRemoved.push(die)        
    } else {
      notFound.push(die)
    }
  }
  
  return {
    newPool: newPool ,
    actuallyRemoved: actuallyRemoved ,
    notFound: notFound,
  }
}

/*
input: {
  poolNames - list or ALL_NAMES,
  channelId, userId,
}
iocContainer: IocContainer

Returns list of messages.
*/
async function clearPools(input, iocContainer) {
  let poolNames = input.poolNames 
  let deletedNames = []
  if(poolNames == ALL_NAMES) {
    // Remove all.
    let parentKey = getAllPoolsKey(input.channelId)
    let listSuffixes = await iocContainer.storage.clearKeys(parentKey)
    deletedNames = deletedNames.concat(listSuffixes.map(suffixList => suffixList[0]))
    return [`Cleared all pools: ${formatNameList(deletedNames)}.`]
  }
  
  for(let name of poolNames) {
    let key = getStorageKey(name, input.channelId)
    let clearResult = await iocContainer.storage.clearKeys(key)    
    if(clearResult.length > 0) {
      deletedNames.push(name)
    } else {
      console.log(`Name missing: ` + name)
    }
  }
  
  return [`Cleared pools: ${formatNameList(deletedNames)}.`]
}

function formatNameList(names) {
  return names.length == 0 ? 'none' : names.map(n => `'${n}'`).join(', ')
}

/*
input: {
  poolName,
  stepUp - the die,
  stepUpMod - steps and direction,
  channelId, 
  userId,
}
iocContainer: IocContainer

Returns list of messages.
*/
async function stepUpPool(input, iocContainer) {
  let poolName = input.poolName
  let key = getStorageKey(poolName, input.channelId)
  let existingPool = await iocContainer.storage.getStoredValue(key, { })
  if(!existingPool.dice) {
    return [`Pool not found: ${poolName}`]
  }      
  
  let diceRoll = existingPool.dice
  let stepUp = input.stepUp
  let stepUpMod = input.stepUpMod
  
  let stepUpResult = await StepUp(diceRoll, stepUp, stepUpMod, iocContainer)
  let messageContent = []
  messageContent.push(...stepUpResult.messageContent)
  
  // Ignoring nonPoolDice since it's not relevant.
  let newPool = stepUpResult.newDiceRoll    
    
  if(!_.isEqual(existingPool.dice, newPool)) {
    let type = stepUpMod < 0 ? 'down' : 'up'
    existingPool.dice = newPool
    messageContent.push(`Stepped ${type} pool '${poolName}': ${FormatInputDicePool(newPool)}`)      
    await iocContainer.storage.storeValue(key, existingPool)
  } else {
    messageContent.push(`Pool '${poolName}' has not changed: ${FormatInputDicePool(newPool)}`)      
  }
  
  return messageContent
}


/*
input: {
  poolNames - list or ALL_NAMES,
  channelId, 
  userId,
}
iocContainer: IocContainer

Returns a {
  messageContent: list of messages,
  dicePool: list of dice, undefined when not found or error,
}
*/
async function getDicePool(input, iocContainer) {
  let poolNames = input.poolNames
  
  let dicePool = []
  let messageContent = []
  
  if(poolNames == ALL_NAMES) {
    let parentKey = getAllPoolsKey(input.channelId)
    // Each entry: {keyListSuffix - the remainder keyList , value}
    let entries = await iocContainer.storage.getStoredValues(parentKey)
    
    for(let entry of entries) {
      let poolName = entry.keyListSuffix[0]
      let dice = entry.value.dice
      
      messageContent.push(`Dice pool '${poolName}': ${FormatInputDicePool(dice)}`)
      dicePool.push(...dice)
    }  
  } else {
    for(let poolName of poolNames) {
      let key = getStorageKey(poolName, input.channelId)
      let existingPool = await iocContainer.storage.getStoredValue(key, { })
      if(!existingPool.dice) {
        return {
          messageContent: [`Pool not found: '${poolName}'`],
          dicePool: undefined,
        }
      }
      
      messageContent.push(`Dice pool '${poolName}': ${FormatInputDicePool(existingPool.dice)}`)
      dicePool.push(...existingPool.dice)
    }
  }    
  
 return {
    messageContent: messageContent,
    dicePool: dicePool,
  }
}


function getAllPoolsKey(channelId) {
  return [
    STORAGE_NAME,
    channelId,
  ]
}

function getStorageKey(name, channelId) {
  return [
    STORAGE_NAME,
    channelId,
    name,
  ]
}


module.exports = {
  ShowPools: showPools,
  ModifyPool: modifyPool,
  StepUpPool: stepUpPool,
  GetDicePool: getDicePool,
  ClearPools: clearPools,
  ALL_NAMES : ALL_NAMES ,
  POOL_ADD: POOL_ADD ,
  POOL_REMOVE :POOL_REMOVE ,
  POOL_CLEAR: POOL_CLEAR ,
  STEP_MOD_UP: STEP_MOD_UP,
  STEP_MOD_DOWN: STEP_MOD_DOWN,
};
