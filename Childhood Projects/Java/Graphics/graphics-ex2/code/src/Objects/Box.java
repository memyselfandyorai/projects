package Objects;




import General.Intersection;
import General.Plane;
import General.Point;
import General.Point2D;
import General.Ray;
import General.Scene;
import General.Vector;

/**
 * 
 * represents a box
 *
 */
public class Box extends Surface{
	
	private Point p0,p1,p2,p3;
	
	/**
	 * default constructor
	 */
	public Box() {
	}

	public Box(Point p0, Point p1, Point p2, Point p3) {
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}


	public Point getP0() {
		return p0;
	}


	public void setP0(Point p0) {
		this.p0 = p0;
	}


	public Point getP1() {
		return p1;
	}


	public void setP1(Point p1) {
		this.p1 = p1;
	}


	public Point getP2() {
		return p2;
	}


	public void setP2(Point p2) {
		this.p2 = p2;
	}


	public Point getP3() {
		return p3;
	}


	public void setP3(Point p3) {
		this.p3 = p3;
	}


	public Intersection intersect(Scene scene,Ray ray) {
	
		
		Rectangle[] recs = getRectangles();
		Intersection[] inters = new Intersection[6];
		for (int i=0;i<6;i++)
			inters[i] = recs[i].intersect(scene,ray);		
		
		double minDist = Double.POSITIVE_INFINITY;
		int index = 0;
		int falses = 0;
		
		for (int i=0;i<6;i++){
			Intersection inter = inters[i];
			if (inter!=Intersection.MISS){
				inter.setObject(this); //up to now it was rectangle
				if (inter.getDistance()<minDist){
					index=i;
					minDist = inter.getDistance();
				}
			}
			else {
				falses++;
			}
		}
		if (falses==6)
			return Intersection.MISS;
		else
			return inters[index];
	}
		
		

	public boolean hasAllValues() {
		if (p0==null || p1==null || p2==null || p3==null)
			return false;
		return true;
	}

	/**
	 * return true iff there is a subset of 3 points that are co-linear
	 * @return
	 */
	public boolean coLinear(){
		return (Point.coLinear(p0, p1, p2) || Point.coLinear(p0, p1, p3) || Point.coLinear(p0, p2, p3)
				||Point.coLinear(p1, p2, p3));
	}
	
	
	
	/**
	 * returns all (rectangle) sides of the box
	 * */
	private Rectangle[] getRectangles()
	{
	
		Point p0tag = p1.add(Vector.subPoints(p3, p0).toPoint()).add(Vector.subPoints(p2, p0).toPoint());
		Point p1tag = p2.add(Vector.subPoints(p3, p0).toPoint());
		Point p2tag = p1.add(Vector.subPoints(p3, p0).toPoint());
		Point p3tag = p2.add(Vector.subPoints(p1, p0).toPoint());
		
		
		Rectangle[] recs = new Rectangle[6];
	
		recs[0] = new Rectangle(p0,p1,p2);
		recs[1] = new Rectangle(p0,p1,p3);
		recs[2] = new Rectangle(p0,p2,p3);
		recs[3] = new Rectangle(p0tag,p1tag,p2tag);
		recs[4] = new Rectangle(p0tag,p1tag,p3tag);
		recs[5] = new Rectangle(p0tag,p2tag,p3tag);

		
		return recs;
		
	}

	public Point2D parametrization(Point point) {
		Rectangle[] rectangles = getRectangles();
		if(rectangles[0].isOnPlane(point))
			return rectangles[0].parametrization(point);
		else if(rectangles[1].isOnPlane(point))
			return rectangles[1].parametrization(point);
		else if(rectangles[2].isOnPlane(point))
			return rectangles[2].parametrization(point);
		else if(rectangles[3].isOnPlane(point))
			return rectangles[3].parametrization(point);
		else if(rectangles[4].isOnPlane(point))
			return rectangles[4].parametrization(point);
		else
			return rectangles[5].parametrization(point);
	}

	
	public boolean onRight(Plane plane)
	{
		return plane.onRight(p0);
	}
	
	
}
