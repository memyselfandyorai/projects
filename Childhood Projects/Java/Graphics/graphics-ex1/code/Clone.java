
/**
 * The class represents the cloning method
 */
public class Clone extends AbstractSolver {
	
	private ImageMatrix background;
	private ImageMatrix cloned;
	private ImageMatrix mask;
	private int x0,y0;
	
	/**
	 * constructor of 'Clone' class
	 * @param back - background image matrix
	 * @param cl - cloned image matrix
	 * @param ma - mask image matrix
	 * @param x - coordinate x0
	 * @param y - coordinate y0
	 */
	public Clone(ImageMatrix back,ImageMatrix cl,ImageMatrix ma,int x,int y)
	{
		super(back.getRows(),back.getCols());
		mask=ma;
		cloned=cl;
		background=back;
		y0=y;
		x0=x;
	}
	
	/**
	 * returns true iff coordinate (i,j) is a hole 
	 */
	protected boolean isMissing(int i,int j)
	{
		return ImageMatrix.isMissing(i-y0, j-x0, mask);
	}
	
	/**
	 *  sets a value in vector b to be the known value (when pixel is not a hole)
	 */
	protected void setBNotMissing(double[] b,int index,ImageMatrix.RGB rgb)
	{
		int i=iIndex(index);
		int j=jIndex(index);
		b[index]=background.getColor(i, j, rgb);
	}
	
	/**
	 *  sets a value in vector b to be as the Laplacian of the cloned image (when pixel is a hole)
	 */
	protected void setBMissing(double[] b,int index,ImageMatrix.RGB rgb)
	{
		int i=iIndex(index)-y0;
		int j=jIndex(index)-x0;
		
		assert(i>=0&&j>=0&&i<cloned.getRows()&&j<cloned.getCols());
			
		double sum=0;
		
		int weight=0;
		
		
		if(i>0)
		{
			sum+=cloned.getColor(i-1, j, rgb);
			weight++;
		}
		if(i<cloned.getRows()-1)
		{
			sum+=cloned.getColor(i+1, j, rgb);
			weight++;
		}
		
		if(j<cloned.getCols()-1)
		{
			sum+=cloned.getColor(i, j+1, rgb);
			weight++;
		}
		
		if(j>0)
		{
			sum+=cloned.getColor(i, j-1, rgb);
			weight++;
		}
		
		sum+= (-weight)*cloned.getColor(i, j, rgb);
		
		b[index]=sum;
	}
	

}
