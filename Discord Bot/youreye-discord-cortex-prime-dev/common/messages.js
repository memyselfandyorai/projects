
module.exports = {
  FormatResults: formatResults,
  FormatInputDicePool: formatInputDicePool,
  FormatDieModification: formatDieModification,
  PrintIndexSet: printIndexSet,
};



/*
Input: {
  originalDie - the original die,
  modifiedDie - a modified die, up to restrictions,
  nonPoolDice - a list of dice added to roll, that can't be hitched, and can't be used for effect.
}
*/
// Resturns a list of messages describing the change
function formatDieModification(modificationResult){
  let oldDie = modificationResult.originalDie
  let newDie = modificationResult.modifiedDie 
  let nonPoolDice = modificationResult.nonPoolDice
  
  if (oldDie == newDie && nonPoolDice.length == 0) {
    return []
  }
  
  let nonPoolDiceAddition = ''
  if(nonPoolDice.length > 0) {
    nonPoolDiceAddition = '+'+nonPoolDice.map(d => `D${d}`).join('+')
  }
  
  return [`Modified roll die: D${oldDie} -> D${newDie}${nonPoolDiceAddition}`]
}


/*
Input: {
  dicePool
  rolls
  nonPoolDice
  nonPoolRolls
  maxScores
  modifyEffectFunc : (effect value, effectModifier) -> list of effects
  options
}


Output:

Expected format:
Rolling: D8 2D10 
D4: [1] [2]
D8 : (1) 
D10 : 4 4 
Best Total: 8 (4 + 4) with Effect: D4
*/
function formatResults(input){
  var messageContent = [
    'Rolling: ' + formatInputDicePool(input.dicePool),
  ]
  if(input.nonPoolDice?.length > 0) {
    messageContent.push('Rolling (dice pool unusable for hitch or effect): ' + formatInputDicePool(input.nonPoolDice))
  }
  
  // Show results for each die.
  let rollResults = new Map()
  input.rolls.forEach(roll => {
    let resultArray = rollResults.get(roll.die) || []
    resultArray.push({
      result: roll.result,
      canBeHitched: true,
      })
    rollResults.set(roll.die, resultArray)
  })
  input.nonPoolRolls.forEach(roll => {
    let resultArray = rollResults.get(roll.die) || []
    resultArray.push({
      result: roll.result,
      canBeHitched: false,
    })
    rollResults.set(roll.die, resultArray)
  })
  
  let options = input.options
  let rollDiesAsc = Array.from(rollResults.keys()).sort(intSort)
  messageContent = messageContent.concat(rollDiesAsc.map(die => formatDieRoll(die, rollResults, options)))
   
  
  messageContent = messageContent.concat(['Results ordered by best total:'])
  messageContent = messageContent.concat(
    input.maxScores
      .sort((scoreEffect1, scoreEffect2) => (scoreEffect1.score > scoreEffect2.score) ? -1 : 1)
      .map(se => formatScoreEffect(se, input.modifyEffectFunc, options))
  )
  return messageContent
}


// dicePool: array
function formatInputDicePool(dicePool) {
   let rolling = new Map()
  // Count dice of each type for "Rolling" section.
  dicePool.forEach(die => {
    rolling.set(die, 1 + (rolling.get(die) || 0))
  })
  return Array.from(rolling.keys()).sort(intSort).map(d => (rolling.get(d) + 'D' + d)).join(' ')
}


/*
rollResults: map between die type to {
  result - value,
  canBeHitched: boolean,
  }
*/
function formatDieRoll(die, rollResults, options) {

  let byResultAsc = (r1, r2) => (r1.result > r2.result) ? 1 : -1
  let formatRollResult = r => 
    !r.canBeHitched 
      ? `[${r.result}]`
      : ((r.result > options.hitch) 
        ? `${r.result}` 
        : `**(${r.result})**`)
  
  // D10 : **(1)** 4 4 [6]
  let results = rollResults.get(die).sort(byResultAsc).map(formatRollResult).join(' ') 
  return `D${die} : ${results}`
}

// modifyEffectFunc: (effect value, effectModifier) -> list of effect values
function formatScoreEffect(scoreEffect, modifyEffectFunc, options){
  // 8 (4 + 4) with Effect: D4
  let reason = scoreEffect.sourceRolls.map(r => r.result).join('+')
  let effect = scoreEffect.effect  
  
  let effectMod = ''
  if(options.effectModifier > 0) {
    effectMod = 'Increased-'
  } else if(options.effectModifier < 0) {
    effectMod = 'Decreased-'
  }
  
  if(effect.isSingleEffect()) {
    let effectString = formatSingleEffect(effect.getEffectList(), options)
    return `Score: ${scoreEffect.score } (${reason }) with ${effectMod}Effect: ${effectString}`   
  }  
  
  // Else: multiple effect.
  if(effectMod.length > 0) {
    let getModifiedListFromEffectValue = effect => modifyEffectFunc(effect, options.effectModifier)
    let effectList = effect.getEffectList()
      .map(getModifiedListFromEffectValue)
      .map(modifiedEffectValues => formatSingleEffect(modifiedEffectValues, options))
    effectMod = `. Choose the ${effectMod}die - one of: ${effectList.join(' | ')}`
  }
  
  let effectString = effect.getEffectList().map(e => `D${e}`).join(',')
  return `Score: ${scoreEffect.score } (${reason }) with Effects: ${effectString}${effectMod}`     
}

// Input: effect - list of effect values
function formatSingleEffect(effectValues, options) {
  let effectString = effectValues.map(e => `D${e}`).join(',')
  
  if (effectValues.length > 1 && !options.extraEffectDieModeOn) {
    effectString = `D${effectValues[0]}+`
  } 
            
  return effectString
}


function intSort(a, b) {
  return a - b;
}

function printIndexSet(listOfSet) {
  return '[' + listOfSet.map(s => '{' +  Array.from(s).join(',')+ '}').join(',') + ']'
}


// internal test:
function debugTest () {

  console.log(`getAllIndexSets A: ` + printIndexSet(getAllIndexSets(1, 0, 5)));
  console.log(`getAllIndexSets B: ` + printIndexSet(getAllIndexSets(1, 3, 5)));
  console.log(`getAllIndexSets C: ` + printIndexSet(getAllIndexSets(2, 0, 3)));
  console.log(`getAllIndexSets D: ` + printIndexSet(getAllIndexSets(3, 0, 3)));
  console.log(`getAllIndexSets E: ` + printIndexSet(getAllIndexSets(3, 0, 4)));
  console.log(`getAllIndexSets F: ` + printIndexSet(getAllIndexSets(4, 0, 4)));
  console.log(`getAllIndexSets G: ` + printIndexSet(getAllIndexSets(5, 0, 4)));
  console.log(`getAllIndexSets H: ` + printIndexSet(getAllIndexSets(1, 0, 1)));
  console.log(`getAllIndexSets I: ` + printIndexSet(getAllIndexSets(0, 0, 1)));
  console.log(`getAllIndexSets J: ` + printIndexSet(getAllIndexSets(1, 0, 0)));
  /*
  getAllIndexSets A: [{0},{1},{2},{3},{4}]
  getAllIndexSets B: [{3},{4}]
  getAllIndexSets C: [{1,0},{2,0},{2,1}]
  getAllIndexSets D: [{0,1,2}]
  getAllIndexSets E: [{2,1,0},{3,1,0},{3,2,0},{2,3,1}]
  getAllIndexSets F: [{0,1,2,3}]
  getAllIndexSets G: [{0,1,2,3}]
  getAllIndexSets H: [{0}]
  getAllIndexSets I: [{}]
  getAllIndexSets J: [{}]
  
  */
  
}
