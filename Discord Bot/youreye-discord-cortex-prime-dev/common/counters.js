const _ = require('lodash')

// Stored value: tuple of { value - current value }
const STORAGE_NAME = "COUNTER"

/*
input: {
  channelId, 
  userId,
}
iocContainer: IocContainer
Output:{
  messageContent - list of messages,
  hasCounters - boolean,
} 
*/
async function showCounters(input, iocContainer){
  let parentKey = getAllCountersKey(input.channelId)
  // Each entry: {keyListSuffix - the remainder keyList , value}
  let entries = await iocContainer.storage.getStoredValues(parentKey)
  let counterToSortedNames = getCountersByName(entries)
  let messageContent = []
  let keyValuePairs = Array.from(counterToSortedNames.entries())
  for(let entry of _.sortBy(keyValuePairs, e => e[0])) {
    let counterName = entry[0]
    let list = entry[1]
    
    messageContent.push(`- **${counterName}**:`)
    for(let entry of list) {
      messageContent.push(`-- *${entry.userName}*: ${entry.counterValue}`)
    }
  }  
  
  if (messageContent.length <= 0) {
    messageContent.push(`No counters.`)
    return {
      messageContent: messageContent,
      hasCounters: false,
    }
  }
  
  return {
    messageContent: messageContent,
    hasCounters: true,
  }
}

/*
entries: list of {keyListSuffix - the remainder keyList (counter name, user name) , value}

returns: Map between counter name to a list ordered by user names of {counterName, userName, counterValue}
*/
function getCountersByName(entries) {
  let entryObjects = entries.map(kv => ({
    counterName: kv.keyListSuffix[0],
    userName: kv.keyListSuffix[1],
    counterValue: kv.value.value,
  }))
  
  let groupedByCounter = _.groupBy(entryObjects, entry => entry.counterName)
  
  let counterMap = new Map()
  Object.entries(groupedByCounter).forEach(kv => {
    let sortedList = _.sortBy(kv[1], entry => entry.userName)
    counterMap.set(kv[0], sortedList)
  })
  return counterMap
}

/*
input: {
  who,
  counterName,
  amount,  
  channelId, 
  userId,
}    
iocContainer: IocContainer

Returns list of messages.
*/
async function modifyCounter(input, iocContainer) {

  let key = getStorageKey(input.channelId, input.counterName, input.who)
  let existingCounter = await iocContainer.storage.getStoredValue(key, { value: 0 })
  
  let counterValue = {...existingCounter}
  counterValue.value += input.amount
  await iocContainer.storage.storeValue(key, counterValue)
  
  let messageContent = [`Modified counter *${input.counterName}* to **${counterValue.value}** for *${input.who}*.`]
  return messageContent
}





/*
input: {
  who,
  counterName,
  value,  
  channelId, 
  userId,
}    
iocContainer: IocContainer

Returns list of messages.
*/
async function setCounter(input, iocContainer) {
  let key = getStorageKey(input.channelId, input.counterName, input.who)
  
  let counterValue = {value: input.value}
  await iocContainer.storage.storeValue(key, counterValue )
  
  let messageContent = [`Set counter *${input.counterName}* to **${input.value}** for *${input.who}*.`]
  return messageContent
}

/*
input: {
  who,
  counterName,
  channelId, 
  userId,
}    
iocContainer: IocContainer

Returns list of messages.
*/
async function removeCounter(input, iocContainer) {
  let key = getStorageKey(input.channelId, input.counterName, input.who)    
  let clearedKeys = await iocContainer.storage.clearKeys(key)    
  if(clearedKeys.length > 0) {
    return [`Removed counter *${input.counterName}* of *${input.who}*.`]
  } else {
    return [`Counter *${input.counterName}* of *${input.who}* not found.`]
  }
  
}


function getAllCountersKey(channelId) {
  return [
    STORAGE_NAME,
    channelId,
  ]
}

function getStorageKey(channelId, counterName, userName) {
  return [
    STORAGE_NAME,
    channelId,
    counterName,
    userName,
  ]
}


module.exports = {
  SetCounter: setCounter,
  ShowCounters: showCounters,
  ModifyCounter: modifyCounter,
  RemoveCounter: removeCounter,
};
