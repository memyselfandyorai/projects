﻿using FolderSyncronizer.Debugging;
using Infra.Framework.Extensions;
using System;
using System.Runtime.Serialization;

namespace FolderSyncronizer.Core.Watchers.Actions
{
    public abstract class ActionBase : IFileAction
    {
        public string SourceFile { get; set; }
        public string TargetFile { get; set; }

        // Since IsCancelled contains logic that could change after performing actions, a separate value should be stored.
        public bool IsManuallyCancelled { get; set; } = false;

        [IgnoreDataMember]
        public virtual bool IsCancelled 
        { 
            get { return IsManuallyCancelled; }
            set { IsManuallyCancelled = value; }
        } 

        [IgnoreDataMember]
        public string Description 
        { 
            get
            {
                return !IsCancelled ? ActionDescription : $"{ActionDescription} (cancelled)";
            }
        }

        public void OnNewAction(IFileAction action)
        {
            var actionBase = ((ActionBase)action);

            // Note: Runs this on cancelled events as well. Do not cancel self if the older is cancelled.
            if (!actionBase.ShouldAffectOlderAction((ActionBase)this))
            {
                return;
            }

            Singleton.Logger.LogDebug($"Updating action \"{this.Description}\" by new action \"{action.Description}\"");
            actionBase.ChangeOlderAction(this);
        }

        public virtual bool ShouldAffectOlderAction(ActionBase action)
        {
            return this.SourceFile.EqualsIgnoreCase(action.SourceFile);
        }


        public void Apply()
        {
            if(!IsCancelled)
            {
                ApplyAction();
            }
            else
            {
                Singleton.Logger.LogDebug($"Action {GetType()} for '{SourceFile}' is cancelled. Description: {Description}");
            }
        }

        protected abstract void ApplyAction();
        protected abstract void ChangeOlderAction(ActionBase olderAction);

        protected abstract string ActionDescription { get; }
    }
}
