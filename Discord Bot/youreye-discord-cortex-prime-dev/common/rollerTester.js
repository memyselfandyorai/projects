const roller = require('./roller.js');
const saver = require('./saver.js');
const controller = require('./controller.js');


module.exports = {
  RunTests: runTests,
};

// TODO: test controller functions --- 
// TODO: create function with a "baseline roll", then test keep / append / reroll. (done, but not used for append/keep, and not covering reroll completely)
// TODO: test all options for "roll" method (separately and together?) - already done
// TODO: test ideas at the bottom of the test cases.
// TODO: test all options for the other commands.

// TODO: test like testControllerRollCortexPrime_diceKeep1EffectModifierExtraDie_goodMessages and the one before, 
// with two possible increased effects beyond D12: D12,D4 + D12,D6 (take an existing score with 2 effects as results)

function createTestCases() {
  return [
    new TestCase({
      // Special input and rolls that resulted in a missing score-effect option.
      name: 'testSpecialInput',
      functionToRun: testRollerRollCortexPrime,
      inputArgs: {
        dice: '4D4 2D6 1D8 1D12',
        keep: 3,
        effectModifier: 1,
        hitch: 2,
        actualRollResults: [1, 1, 1, 2, 4, 5, 7, 11],        
      },
      expectedResult: [
        'Rolling: 4D4 2D6 1D8 1D12',
        'D4 : **(1)** **(1)** **(1)** **(2)**',
        'D6 : 4 5',
        'D8 : 7',
        'D12 : 11',
        'Results ordered by best total:',
        'Score: 23 (5+7+11) with Increased-Effect: D8',
        'Score: 20 (4+5+11) with Increased-Effect: D10',
        'Score: 16 (4+5+7) with Increased-Effect: D12+',
      ],    
    }),    
        
    new TestCase({
      // Special input and rolls that resulted in a missing score-effect option.
      name: 'testControllerRerollCortexPrime_noPreviousRoll_noCachedMessage',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        dice: [6],
        cache_key: 'empty',
        actualRollResults :[1, 2, 3, 4],
      },
      expectedResult: [
        'No last roll found',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerParseDiceInput_dice_diceListGood',
      functionToRun: testControllerParseDiceInput ,
      inputArgs: {
        dice: '4 6 2D6 3D8'
      },
      expectedResult: [4, 6, 6, 6, 8, 8, 8],
    }),
    
    new TestCase({
      name: 'testControllerStepUp_existingNumberOnce_resultOk ',
      functionToRun: testControllerStepUp,
      inputArgs: {
        dice: [6, 8, 8],
        stepUp: 6, 
        stepUpMod: 1, 
      },
      expectedResult: {newDiceRoll: [8, 8, 8], nonPoolDice: [], messageContent : ['Modified roll die: D6 -> D8']},
    }),
    new TestCase({
      name: 'testControllerStepUp_numberDoesntExist_missingMessage ',
      functionToRun: testControllerStepUp,
      inputArgs: {
        dice: [6, 8, 8],
        stepUp: 4, 
        stepUpMod: 1, 
      },
      expectedResult: {newDiceRoll: [6, 8, 8], nonPoolDice: [], messageContent : ['No step up found for D4: did not step up']},
    }),
    new TestCase({
      name: 'testControllerStepUp_existingNumber3Times_resultOk ',
      functionToRun: testControllerStepUp,
      inputArgs: {
        dice: [6, 8, 8],
        stepUp: 6, 
        stepUpMod: 3, 
      },
      expectedResult: {newDiceRoll: [12, 8, 8], nonPoolDice: [], messageContent : ['Modified roll die: D6 -> D12']},
    }),
    new TestCase({
      name: 'testControllerStepUp_existingNumberDecreaseTwice_resultOk ',
      functionToRun: testControllerStepUp,
      inputArgs: {
        dice: [6, 8, 8],
        stepUp: 8, 
        stepUpMod: -2, 
      },
      expectedResult: {newDiceRoll: [6, 4, 8], nonPoolDice: [], messageContent : ['Modified roll die: D8 -> D4']},
    }),
    new TestCase({
      name: 'testControllerStepUp_existing4DecreaseOnce_staysSameWithMessage ',
      functionToRun: testControllerStepUp,
      inputArgs: {
        dice: [4, 8, 8],
        stepUp: 4, 
        stepUpMod: -1, 
      },
      expectedResult: {newDiceRoll: [4, 8, 8], nonPoolDice: [], messageContent : []},
    }),
    new TestCase({
      name: 'testControllerStepUp_existing12IncreaseOnce_staysSameWithMessage',
      functionToRun: testControllerStepUp,
      inputArgs: {
        dice: [4, 8, 12],
        stepUp: 12, 
        stepUpMod: 2, 
      },
      expectedResult: {newDiceRoll: [4, 8, 12], nonPoolDice: [4], messageContent : ['Modified roll die: D12 -> D12+D4']},
    }),
    
    new TestCase({
      name: 'testControllerRollCortexPrime_onlyDice_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        /*
        keep: 2,
        effectModifier: 0,
        dieModifier: 0,
        hitch,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 9 (4+5) with Effect: D8',
        'Score: 8 (4+4) with Effect: D10',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep4_goodMessagesMinimalEffect',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 4,
        /*
        effectModifier: 0,
        dieModifier: 0,
        hitch,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 16 (4+3+4+5) with Effect: D4',
      ],
    }),
    new TestCase({
    name: 'testControllerRollCortexPrime_diceKeep4_goodMessagesMinimalEffect',
    functionToRun: testControllerRollCortexPrime,
    inputArgs: {
      dice: '3',
      cache_key: '',
      actualRollResults: [1],
    },
    expectedResult: [
        'Rolling: 1D3',
        'D3 : **(1)**',
        'Results ordered by best total:',
        'Score: 0 () with Effect: D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep4_goodMessagesMinimalEffect',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        hitch: 4,
        keep: 2,
        /*
        effectModifier: 0,
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** **(4)**',
        'D6 : **(1)** **(3)**',
        'D8 : **(4)**',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Effect: D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep1EffectModifier_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 1,
        effectModifier: 2,
        extraDie: false,        
        /*
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D12',               
        'Score: 4 (4) with Increased-Effect: D12+',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep1EffectModifierExtraDie_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 1,
        effectModifier: 2,
        extraDie: true,
        /*
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D12',        
        'Score: 4 (4) with Increased-Effect: D12,D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep1EffectModifierExtra2Dice_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 1,
        effectModifier: 7,
        extraDie: true,
        /*
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D12,D12',        
        'Score: 4 (4) with Increased-Effect: D12,D12,D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep1NegativeEffectModifier_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 1,
        effectModifier: -3,
        /*
        dieModifier: 0,
        */
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Rolling: 2D4 2D6 1D8 1D10',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Decreased-Effect: D4',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep3DieModifierPositive_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 3,
        dieModifier: 2,
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Modified roll die: D4 -> D6',
        'Modified roll die: D4 -> D6',
        'Rolling: 4D6 1D8 1D10',
        'D6 : **(1)** **(1)** 3 4',
        'D8 : 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 13 (4+4+5) with Effect: D6',
        'Score: 12 (3+4+5) with Effect: D8',
        'Score: 11 (3+4+4) with Effect: D10',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceKeep3DieModifierNegative_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 2D6 1D8 4 10',
        keep: 2,
        dieModifier: -1,
        cache_key: '',
        actualRollResults: [1, 1, 3, 4, 4, 5],
      },
      expectedResult: [
        'Modified roll die: D10 -> D8',
        'Rolling: 2D4 2D6 2D8',
        'D4 : **(1)** 4',
        'D6 : **(1)** 3',
        'D8 : 4 5',
        'Results ordered by best total:',
        'Score: 9 (4+5) with Effect: D8',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_diceDieModifierPositiveMoreThan12_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '3D12',
        dieModifier: 2,
        cache_key: '',
        actualRollResults: [1, 1, 4, 1, 3],
      },
      expectedResult: [
        'Modified roll die: D12 -> D12+D4',
        'Modified roll die: D12 -> D12+D4',
        'Rolling: 2D4 3D12',
        'D4 : [1] [3]',
        'D12 : **(1)** **(1)** 4',
        'Results ordered by best total:',
        'Score: 7 (3+4) with Effect: D4',
        'Score: 4 (1+3) with Effect: D12',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_mergeMissing_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        merge: '2',
        cache_key: '',
        actualRollResults: [2, 2, 4,],
      },
      expectedResult: [
        'Did not find two input dice of type D2',
        'Rolling: 1D4 1D6 1D8',
        'D4 : 2',
        'D6 : 2',
        'D8 : 4',        
        'Results ordered by best total:',
        'Score: 6 (2+4) with Effect: D6',
        'Score: 4 (2+2) with Effect: D8',
      ],
    }),
     new TestCase({
      name: 'testControllerRollCortexPrime_mergeOnlyOne_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        merge: '6',
        cache_key: '',
        actualRollResults: [2, 2, 4,],
      },
      expectedResult: [
        'Did not find two input dice of type D6',
        'Rolling: 1D4 1D6 1D8',
        'D4 : 2',
        'D6 : 2',
        'D8 : 4',        
        'Results ordered by best total:',
        'Score: 6 (2+4) with Effect: D6',
        'Score: 4 (2+2) with Effect: D8',
      ],
    }),
     new TestCase({
      name: 'testControllerRollCortexPrime_mergeValid_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 6 8',
        merge: '6',
        cache_key: '',
        actualRollResults: [2, 7, 4,],
      },
      expectedResult: [
        'Removed one D6',
        'Modified roll die: D6 -> D8',
        'Rolling: 1D4 2D8',
        'D4 : 2',
        'D8 : 4 7',        
        'Results ordered by best total:',
        'Score: 11 (4+7) with Effect: D4',
        'Score: 9 (2+7) with Effect: D8',
      ],
    }),    
    new TestCase({
      name: 'testControllerRollCortexPrime_3mergesOneD12_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6D6 8 2D12',
        merge: '6 6 12',
        cache_key: '',
        actualRollResults: [2, 8, 7, 4, 4, 6, 12, 3],
      },
      expectedResult: [
        'Removed one D6',
        'Modified roll die: D6 -> D8',
        'Removed one D6',
        'Modified roll die: D6 -> D8',
        'Removed one D12',
        'Modified roll die: D12 -> D12+D4',
        'Rolling: 2D4 2D6 3D8 1D12',
        'D4 : 2 [3]',
        'D6 : 4 4',
        'D8 : 6 7 8',        
        'D12 : 12', 
        'Results ordered by best total:',
        'Score: 20 (8+12) with Effect: D8',
        'Score: 15 (7+8) with Effect: D12',
      ],
    }),
    new TestCase({
      name: 'testControllerRollCortexPrime_extraRollEffect_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        extraEffectDice: '6',
        cache_key: '',
        actualRollResults: [2, 4, 3, 6],
      },
      expectedResult: [
        'Rolling: 1D4 2D6 1D8',
        'D4 : 2',
        'D6 : 4 6',
        'D8 : 3',        
        'Results ordered by best total:',
        'Score: 10 (4+6) with Effects: D8,D4',
        'Score: 8 (2+6) with Effects: D8,D6',
      ],
    }),
     new TestCase({
      name: 'testControllerRollCortexPrime_twoExtraRollEffect_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '4 6 8',
        extraEffectDice: '10 12',
        cache_key: '',
        actualRollResults: [2, 4, 3, 9, 10],
      },
      expectedResult: [
        'Rolling: 1D4 1D6 1D8 1D10 1D12',
        'D4 : 2',
        'D6 : 4',
        'D8 : 3',        
        'D10 : 9',        
        'D12 : 10',        
        'Results ordered by best total:',
        'Score: 19 (9+10) with Effects: D8,D6,D4',
        'Score: 14 (4+10) with Effects: D10,D8,D4',
        'Score: 13 (4+9) with Effects: D12,D8,D4',
        'Score: 11 (2+9) with Effects: D12,D8,D6',
        'Score: 7 (4+3) with Effects: D12,D10,D4',
        'Score: 6 (2+4) with Effects: D12,D10,D8',
      ],
    }),        
    new TestCase({
      name: 'testControllerRollCortexPrime_extraRollEffectWithIncreased_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '8 8',
        keep: 1,
        effectModifier: 1,
        extraDie: false,  
        extraEffectDice: '6 12',
        cache_key: '',
        actualRollResults: [3, 4, 2, 2],
      },
      expectedResult: [
        'Rolling: 1D6 2D8 1D12',
        'D6 : 2',
        'D8 : 3 4',        
        'D12 : 2',        
        'Results ordered by best total:',
        'Score: 4 (4) with Effects: D12,D8,D6. Choose the Increased-die - one of: D12+ | D10 | D8',
        'Score: 2 (2) with Effects: D12,D8,D8. Choose the Increased-die - one of: D12+ | D10 | D10',      
      ],
    }),    
    new TestCase({
      name: 'testControllerRollCortexPrime_extraRollEffectWith2IncreasedExtraDie_goodMessages',
      functionToRun: testControllerRollCortexPrime,
      inputArgs: {
        dice: '8 8',
        keep: 1,
        effectModifier: 2,
        extraDie: true,  
        extraEffectDice: '6 12',
        cache_key: '',
        actualRollResults: [3, 4, 2, 2],
      },
      expectedResult: [
        'Rolling: 1D6 2D8 1D12',
        'D6 : 2',
        'D8 : 3 4',        
        'D12 : 2',        
        'Results ordered by best total:',
        'Score: 4 (4) with Effects: D12,D8,D6. Choose the Increased-die - one of: D12,D6 | D12 | D10',
        'Score: 2 (2) with Effects: D12,D8,D8. Choose the Increased-die - one of: D12,D6 | D12 | D12',      
      ],
    }),    
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_withStepUp_steppedUp',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        // dice: [6],
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        step_up: 4, 
        step_up_mod: 1, 
        actualRollResults :[2, 3, 3, 4, 5],
      },
      expectedResult: [
        'Modified roll die: D4 -> D6',
        'Rolling: 2D6 2D8 1D10',
        'D6 : **(2)** 3',
        'D8 : 3 4',
        'D10 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D10',
        'Score: 4 (4) with Increased-Effect: D12',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerRerollCortexPrime_withStepUpBeyond12_steppedUp',
      functionToRun: testControllerRerollCortexPrime,
      inputArgs: {
        // dice: [6],
        cache_key: CONSTANT_ROLL_CACHE_KEY,
        step_up: 10, 
        step_up_mod: 3, 
        actualRollResults :[2, 3, 3, 3, 5, 4],
      },
      expectedResult: [
        'Modified roll die: D10 -> D12+D4',
        'Rolling: 2D4 1D6 2D8 1D12',
        'D4 : **(2)** [4]',
        'D6 : 3',
        'D8 : 3 3',
        'D12 : 5',
        'Results ordered by best total:',
        'Score: 5 (5) with Increased-Effect: D10',
        'Score: 4 (4) with Increased-Effect: D12+',
      ],    
    }),
    
    new TestCase({
      name: 'testControllerSaveDice_missingName_badMessage',
      functionToRun: testSaveDice,
      inputArgs: {
        name: '',
        action: saver.SCENE,
        dice: '6 6',
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `No input name. Did not save anything.`
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_invalidNameSeparator_badMessage',
      functionToRun: testSaveDice,
      inputArgs: {
        name: `fdsfs${controller.SAVE_NAME_SEPARATOR}gfdgd`,
        action: saver.SCENE,
        dice: '6 6',
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `Invalid name 'fdsfs${controller.SAVE_NAME_SEPARATOR}gfdgd': must not contain '${controller.SAVE_NAME_SEPARATOR}' or '${controller.SAVE_NAME_ALL_WILCARD}'.`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_invalidNameSeparator_badMessage',
      functionToRun: testSaveDice,
      inputArgs: {
        name: `${controller.SAVE_NAME_ALL_WILCARD} avdfv`,
        action: saver.SCENE,
        dice: '6 6',
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `Invalid name '${controller.SAVE_NAME_ALL_WILCARD} avdfv': must not contain '${controller.SAVE_NAME_SEPARATOR}' or '${controller.SAVE_NAME_ALL_WILCARD}'.`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_missingDice_badMessage',
      functionToRun: testSaveDice,
      inputArgs: {
        name: SAVE_NAME,
        action: saver.SCENE,
        dice: '',
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `Input dice was empty. Did not save anything.`
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_scene_good',
      functionToRun: testSaveDice,
      inputArgs: {
        name: SAVE_NAME,
        action: saver.SCENE,
        dice: '6 6',
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `Saved '${SAVE_NAME}' successfully as '2D6'.`
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_singleD6Annotation_good',
      functionToRun: testSaveDice,
      inputArgs: {
        name: SAVE_NAME,
        action: saver.SINGLE,
        dice: '2D8',
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `Saved '${SAVE_NAME}' successfully as '2D8'.`
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_show_nothing',
      functionToRun: testSaveDice,
      inputArgs: {
        action: saver.SHOW,
        guild_id: GUILD_ID,
        user_id: USER_ID,
      },
      expectedResult: [
        `No saved results.`
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_showFresh_all',
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
      },
      expectedResult: [
        `${SAVE_NAME} (scene): 1D6`,
        `${SINGLE_SAVE_NAME} (single/round): 2D8`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_showAfterDeletion_showNone',
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_DELETE_ALL],
      },
      expectedResult: [
        `No saved results.`
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_showAfterUsage_allWithCaveat',
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_GET_FOR_USER],
      },
      expectedResult: [
        `${SAVE_NAME} (scene): 1D6`,
        `${SINGLE_SAVE_NAME} (single/round): 2D8 - **already used by user**`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_showAfterSceneDeletion_showSome',
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_DELETE_SCENE],
      },
      expectedResult: [
        `${SINGLE_SAVE_NAME} (single/round): 2D8`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_showAfterSingleDeletion_showSome',
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_DELETE_SINGLE],
      },
      expectedResult: [
        `${SAVE_NAME} (scene): 1D6`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_showAfterUsageDifferentUser_allWithoutCaveat',
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: OTHER_USER_ID,
        actions: [PREPARE_ACTION_GET_FOR_USER],
      },
      expectedResult: [
        `${SAVE_NAME} (scene): 1D6`,
        `${SINGLE_SAVE_NAME} (single/round): 2D8`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_showOtherGuild_showNone',
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: OTHER_GUILD_ID,
        user_id: USER_ID,
        actions: [],
      },
      expectedResult: [
        `No saved results.`
      ],    
    }),
     new TestCase({
      name: 'testControllerSaveDice_showAfterDeletionsOfOtherName_all',
      functionToRun: testShowDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [PREPARE_ACTION_DELETE_OTHER],
      },
      expectedResult: [
        `${SAVE_NAME} (scene): 1D6`,
        `${SINGLE_SAVE_NAME} (single/round): 2D8`,
      ],    
    }),                
    new TestCase({
      name: 'testControllerSaveDice_deleteEmptyString_invalidInput',
      functionToRun: testDeleteSavedDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
        name: '',
      },
      expectedResult: [
        `No input name. Did not delete anything.`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_deleteMultipleInputs_deleted',
      functionToRun: testDeleteSavedDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
        name: `${SAVE_NAME}  ${controller.SAVE_NAME_SEPARATOR} ${SINGLE_SAVE_NAME}`,
      },
      expectedResult: [
        `Deleted saved result for '${SAVE_NAME}', '${SINGLE_SAVE_NAME}'.`,
      ],    
    }),
    new TestCase({
      name: 'testControllerSaveDice_deleteAll_deleted',
      functionToRun: testDeleteSavedDice,
      inputArgs: {
        guild_id: GUILD_ID,
        user_id: USER_ID,
        actions: [],
        name: `${controller.SAVE_NAME_ALL_WILCARD}`,
      },
      expectedResult: [
        `Deleted saved result for all.`,
      ],    
    }),
    
    // TODO: tests for opt ins and opt outs.
    
    
    // TODO: test on previous roll with a die modifier, then step up 
    // TODO: test roller with nonPoolDice of D6 - see if it works - not hitched and not chosen for effect
    // TODO: more tests on reroll with dice (and without params) and negative stepup mod
    
    
  ]
}

/*
output: {
  
  messages - list of strings,
  totalSuccess - int count,
  totalFail - int count,
}
*/
async function runTests() {
  const TEST_CASES = createTestCases()
  let successCount = 0
  let failCount = 0
  let results = []
  for (const testCase of TEST_CASES) {    
    let expectedResult= testCase.expectedResult()
    let name = testCase.name()
    console.log(`Running test '${name}'`);
    
    let testOutput = undefined
    
    try {
      testOutput = await testCase.run()
    }
    catch(err) {
      console.log(`Error: ${err}`);
      console.log(`Stacktrace: ${err.stack}`);
      results = results.concat([
      `${name}: FAILED`,
      // JSON.stringify(..., null, 2)
      `    Error: ` + err,
      ])     
      failCount = failCount + 1
      continue
    }
    
    if(compareObjects(testOutput, expectedResult)) {
      results = results.concat([`${name }: SUCCESS`])
      successCount = successCount + 1
    } else {
      results = results.concat([
        `${name}: FAILED`,
        // JSON.stringify(..., null, 2)
        `    Expected: ` + JSON.stringify(expectedResult),
        `    Actual     : ` + JSON.stringify(testOutput),
        ])
      failCount = failCount + 1     
    }
  }
  
  return {
    messages: results,
    totalSuccess: successCount,
    totalFail: failCount ,
  }
}

function compareObjects(array1, array2) {
  return JSON.stringify(array1) == JSON.stringify(array2)
  //return array1.length === array2.length && array1.every(function(value, index) { return value === array2[index]})
}

/*
input: {dice - string}
*/
async function testControllerParseDiceInput (input) {
  return await roller.ParseDiceInput(input.dice)
}

/*
input: {
  dice - list of numbers,
  stepUp , 
  stepUpMod 
}

Output: newDiceRoll, messageContent - empty if no step up}
*/
async function testControllerStepUp (input) {  
  return await roller.StepUp(input.dice, input.stepUp, input.stepUpMod )
}

/*
input: {
  dice - array list,
  step_up, 
  step_up_mod, 
  cache_key,
  actualRollResults - list of random results,
}
*/
async function testControllerRerollCortexPrime(input) {  
  let actualRollResults = [...CONSTANT_ROLL_RESULTS, ...input.actualRollResults]
  let iocContainer = createIocContainer(actualRollResults )
  
  let preparationInput = {
    cache_key: CONSTANT_ROLL_CACHE_KEY,
  }
  await prepareConstantRoll(preparationInput, iocContainer)
  
  let rerollInput = {
    dice: input.dice,
    step_up: input.step_up,
    step_up_mod: input.step_up_mod,
    cache_key: input.cache_key,
  }
  let messageContentResult = await controller.RerollCortexPrime(rerollInput, iocContainer)
  
  return messageContentResult
}


const CONSTANT_ROLL_CACHE_KEY = 'user,channel'
const CONSTANT_ROLL_RESULTS = [4, 1, 2, 3, 5]

/*
input: {
  cache_key,
}
*/
async function prepareConstantRoll(input, iocContainer) {
  const rollInput = {
    dice: '4 6 2D8 12',
    keep: 1,
    effect_mod: 1,
    die_mod: -1,
    hitch: 2,
    cache_key: input.cache_key,
  } 
  
  let expectedResult = [
    'Modified roll die: D12 -> D10',
    'Rolling: 1D4 1D6 2D8 1D10',
    'D4 : 4',
    'D6 : **(1)**',
    'D8 : **(2)** 3',
    'D10 : 5',
    'Results ordered by best total:',
    'Score: 5 (5) with Increased-Effect: D10',
    'Score: 4 (4) with Increased-Effect: D12',
  ]
  
  let result = await controller.RollCortexPrime(rollInput, iocContainer)
  throwIfNotEqual(result, expectedResult, 'roll')    
}

/*
input: {
  dice - string,
  keep,
  effectModifier,
  extraDie - if true effect adds extra die,
  dieModifier,
  hitch,
  merge,
  extraEffectDice,  
  
  saverInput (triggers preparation) : {
    preparation_actions
    guild_id, (defaults to const)
    user_id, (defaults to const)
    save_opt_out - string,
    save_opt_in - string,
  },
  
  cache_key,
  actualRollResults - list of random results,
}
*/
async function testControllerRollCortexPrime(input) {
  let iocContainer = createIocContainer(input.actualRollResults)
  iocContainer.options.extraEffectDieModeOn = input.extraDie ?? false
  
  if(input.saverInput) {
    let saverInput = input.saverInput
    let prepareInput = {
      actions: saverInput.preparation_actions
    }    
    await prepareConstantSaveDices(prepareInput, iocContainer)    
  }
  
  const rollInput = {
    dice: input.dice,
    keep: input.keep,
    effect_mod: input.effectModifier,
    die_mod: input.dieModifier,
    hitch: input.hitch,
    merge: input.merge,
    save_opt_out: input.saverInput?.save_opt_out,
    save_opt_in: input.saverInput?.save_opt_in,
    extra_effect_dice: input.extraEffectDice,
    save_input: {
      userId:  input.saverInput?.user_id ?? USER_ID, 
      guildId: input.saverInput?.guild_id ?? GUILD_ID,
    },
    cache_key: input.cache_key
  } 
  
  let result = await controller.RollCortexPrime(rollInput, iocContainer)

  return result
}


/*
input: {
  dice - string,
  keep,
  effectModifier,
  dieModifier,
  hitch,
  actualRollResults - list of random results,
   }
*/
async function testRollerRollCortexPrime(input) {
    const rollInput = {
      diceRoll: roller.ParseDiceInput(input.dice),
      keep: input.keep,
      effectModifier: input.effectModifier,
      dieModifier: input.dieModifier,
      hitch: input.hitch,
    } 
                
    let iocContainer = createIocContainer(input.actualRollResults)
    
    let result = await roller.RollCortexPrime(rollInput, iocContainer)
  
    return result.messageContent     
  }
  

const GUILD_ID = '123'
const OTHER_GUILD_ID = '111'
const USER_ID = 5
const OTHER_USER_ID = 1
const SAVE_NAME = 'battle_strategy'
const SINGLE_SAVE_NAME = 'grip'
const OTHER_SAVE_NAME = 'none'

const PREPARE_ACTION_GET_FOR_USER = 1 // gets data for user
const PREPARE_ACTION_DELETE_ALL = 2 // deletes all
const PREPARE_ACTION_DELETE_SCENE = 3 // deletes SAVE_NAME 
const PREPARE_ACTION_DELETE_SINGLE = 4 // deletes SINGLE_SAVE_NAME 
const PREPARE_ACTION_DELETE_OTHER = 5 // deletes OTHER_SAVE_NAME 


/*
  input: actions - list of prepare actions

  output: list of messages
*/
async function prepareConstantSaveDices(input, iocContainer) {
    const saverInput = {
      dice: '6',
      action: saver.SCENE,
      name: SAVE_NAME,
      guild_id: GUILD_ID,
      user_id: USER_ID,
    }        
    
    let expectedResult = [
      `Saved '${SAVE_NAME}' successfully as '1D6'.`
    ]
    
    let result = await controller.SaveDice(saverInput, iocContainer)
    throwIfNotEqual(result, expectedResult, 'save') 
    
    const singleSaverInput = {
      dice: '2d8',
      action: saver.SINGLE,
      name: SINGLE_SAVE_NAME,
      guild_id: GUILD_ID,
      user_id: USER_ID,
    }        
    expectedResult = [
      `Saved '${SINGLE_SAVE_NAME}' successfully as '2D8'.`
    ]
    result = await controller.SaveDice(singleSaverInput, iocContainer)
    throwIfNotEqual(result, expectedResult, 'save') 
    
    console.log(`Preparing actions: ${input.actions.join(',')}`)
    
    for(action of input.actions){
      switch(action) {
        case PREPARE_ACTION_GET_FOR_USER: 
          let getInput = {
            userId: USER_ID,
            guildId: GUILD_ID,
            saveOptOuts: [],
            saveOptIns: saver.ALL_NAMES,
          }
          
          let expectedGet = [
            `Bonus dice due to '${SAVE_NAME}': 1D6`,
            `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
          ]
          let getResult = await saver.GetSaveForUser(getInput, iocContainer)
          throwIfNotEqual(getResult.additionalPool, [6, 8, 8], 'get') 
          throwIfNotEqual(getResult.messageContent , expectedGet , 'get') 
        
        break  
      case PREPARE_ACTION_DELETE_ALL:
        let deleteInput = {
          guildId: GUILD_ID,
          saveNames: saver.ALL_NAMES,
        }
        
        let expectedDelete = [
          `Deleted saved result for all.`
          ]
        
        let deleteResult = await saver.DeleteSave(deleteInput, iocContainer)
        throwIfNotEqual(deleteResult , expectedDelete, 'delete all') 
        
        break
      case PREPARE_ACTION_DELETE_SCENE:
      
        let deleteInput2 = {
          guildId: GUILD_ID,
          saveNames: [SAVE_NAME],
        }

        let expectedDelete2 = [
          `Deleted saved result for '${SAVE_NAME}'.`
          ]

        let deleteResult2 = await saver.DeleteSave(deleteInput2, iocContainer)
        throwIfNotEqual(deleteResult2 , expectedDelete2, 'delete scene') 
      
        break
      case PREPARE_ACTION_DELETE_SINGLE:
        let deleteInput3 = {
          guildId: GUILD_ID,
          saveNames: [SINGLE_SAVE_NAME],
        }

        let expectedDelete3 = [
          `Deleted saved result for '${SINGLE_SAVE_NAME}'.`
          ]

        let deleteResult3 = await saver.DeleteSave(deleteInput3, iocContainer)
        throwIfNotEqual(deleteResult3 , expectedDelete3, 'delete single') 

        break
        
      case PREPARE_ACTION_DELETE_OTHER :
        let deleteInput4 = {
          guildId: GUILD_ID,
          saveNames: [OTHER_SAVE_NAME],
        }

        let expectedDelete4 = [
          `Deleted saved result for '${OTHER_SAVE_NAME}'.`
          ]

        let deleteResult4 = await saver.DeleteSave(deleteInput4, iocContainer)
        throwIfNotEqual(deleteResult4 , expectedDelete4, 'delete other') 
      
      break
      }        
      
      
    }
  }
    
function throwIfNotEqual(result, expectedResult, name) {
   if(!compareObjects(result, expectedResult)) {
    console.log(`Got: ` + JSON.stringify(result));
    console.log(`Expected: ` + JSON.stringify(expectedResult));
    throw new Error(`Unexpected result when preparing constant ${name}. Got: ` + JSON.stringify(result));
  }
}


/*
  input: {
      name,
      action,
      dice,  - as string
      guild_id, 
      user_id,
    }    
  
  output: list of messages
*/
async function testSaveDice(input) {
    const saverInput = {
      dice: input.dice,
      action: input.action,
      name: input.name,
      guild_id: input.guild_id,
      user_id: input.user_id,
    } 
                
    let iocContainer = createIocContainer([])
    
    let result = await controller.SaveDice(saverInput, iocContainer)
  
    return result
  }
  
  
/*
    input: {
        guild_id, 
        user_id,
        actions - list of prepare actions,
      }    
    
    output: list of messages
  */
async function testShowDice(input) {
  let iocContainer = createIocContainer([])
  let prepareInput = {
    actions: input.actions ?? [],
  }
  await prepareConstantSaveDices(prepareInput, iocContainer)

  const saverInput = {
    action: saver.SHOW,
    guild_id: input.guild_id,
    user_id: input.user_id,
  } 

  return await controller.SaveDice(saverInput, iocContainer)
}  


/*
    input: {
        guild_id, 
        user_id,
        actions - list of prepare actions,
        name - comma separated or string (SAVE_NAME_SEPARATOR, SAVE_NAME_ALL_WILCARD)
      }    
    
    output: list of messages
  */
async function testDeleteSavedDice(input) {
  let iocContainer = createIocContainer([])
  let prepareInput = {
    actions: input.actions ?? [],
  }
  await prepareConstantSaveDices(prepareInput, iocContainer)

  const deleteInput = {
    action: saver.DELETE,
    name: input.name,
    guild_id: input.guild_id,
    user_id: input.user_id,
  } 
  return await controller.SaveDice(deleteInput, iocContainer)
}  

function createIocContainer(array){
  var random = new Randomizer(array);
  var cache = new MemCache();
  return {
    getNextNumber: (n, m) => random.getNextNumber(n,m), 
    options: { 
      cacheTimeSecs: 0,
    },
    cache: {
      cacheValue: (key, value, ttlSeconds = 600) => cache.cacheValue(key, value, ttlSeconds),
      getCacheValue: (key, defaultValue = undefined) => cache.getCacheValue(key, defaultValue),   
      getCacheEntries: () => cache.getCacheEntries(),
      clearCacheKey: (key) => cache.clearCacheKey(key),
    },
  }
}


class TestCase{
  /*
   input: {
      name: string,
      functionToRun: async func(input) => result,
      inputArgs: any,
      expectedResult: any,
  }
  */
  constructor(input) {
    this.input = input
  }
  
  name() {
    return this.input.name
  }
  
  async run() {
    return await this.input.functionToRun(this.input.inputArgs)
  }
  
  expectedResult() {
    return this.input.expectedResult
  }
  
}

  
class MemCache {
   constructor(cacheMap){
    this.cacheMap = new Map()
  }
  
  async cacheValue(key, value, ttlSeconds) {
    console.log(`Test caching key: ` + key);
    console.log(`Test caching value: ` + JSON.stringify(value));
    this.cacheMap.set(key, value)
  }
  
  async getCacheValue(key, defaultValue) {
    let value = this.cacheMap.get(key) ?? defaultValue
    console.log(`Test getting key: ` + key);
    console.log(`Test cached value: ` + JSON.stringify(value));
    return value;
  }
  
  async getCacheEntries() {
    return Array.from(this.cacheMap.entries()).map(kv => ({key: kv[0], value: kv[1]}))
  }
  
  async clearCacheKey(key) {
    this.cacheMap.delete(key)
  }
}
  
  
  
class Randomizer {
  constructor(results){
    this.results = results
    this.index = -1
  }
  
  getNextNumber(min, max) {
    this.index = this.index + 1
    let r = this.results[this.index]
    if (r < min || r > max) {
      throw new Error(`r (${r}) is out of bounds [${min}, ${max}]`);
    }
    return r
    
  }
  
  
}