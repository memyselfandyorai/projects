const messages = require('./messages.js');

const ACTION_DELETE = -1
const ACTION_SHOW = 100
const ACTION_SCENE = 1
const ACTION_SINGLE = 2

const WILDCARD_MODE_OPT_IN = 1
const WILDCARD_MODE_OPT_OUT = -1

const ALL_NAMES = 'all'


module.exports = {
  SaveDice: saveDice,
  DeleteSave: deleteSave,
  ShowSaves: showSaves,
  GetSaveForUser: getSaveForUser, 
  ALL_NAMES: ALL_NAMES,
  DELETE: ACTION_DELETE,
  SHOW: ACTION_SHOW,
  SCENE : ACTION_SCENE,
  SINGLE : ACTION_SINGLE,
  WILDCARD_MODE_OPT_IN: WILDCARD_MODE_OPT_IN,
  WILDCARD_MODE_OPT_OUT: WILDCARD_MODE_OPT_OUT,
};


/*
input: {
  guildId,
  saveName,
  userId,
  action,
  dicePool,
  wildcardMode,
}
iocContainer: IocContainer

Output: list of messages
*/
async function saveDice(input, iocContainer) {
  let messageContent = []
  
  let saveName = input.saveName
  if (!saveName) {
    messageContent.push(`No input name. Did not save anything.`)    
    return messageContent
  }
  
  
  let saveValue = {
    saveName: saveName ,
    dicePool: input.dicePool,
    action: input.action,
    wildcardMode: input.wildcardMode ?? 0,
  }
  
  if(input.dicePool.length <= 0) {
    messageContent.push(`Input dice was empty. Did not save anything.`)    
    return messageContent
  }
   
  let key = getGlobalCacheKey(input.guildId, saveName)
  
  // First delete key if exists. Removing all its "usages".
  let deleteInput = {
    guildId: input.guildId,
    saveNames: [saveName],
  }  
  await deleteSave(deleteInput, iocContainer)
  
  await cacheValue(key , saveValue, iocContainer)
  
  messageContent.push(`Saved '${saveName}' successfully as '${messages.FormatInputDicePool(saveValue.dicePool)}'.`)    
  return messageContent
}




/*
input: {
    userId,
    guildId,
    saveOptOuts: list of save names to opt out or ALL_NAMES for all,
    saveOptIns: list of save names to opt in to or ALL_NAMES for all,
    
  }
iocContainer: IocContainer  
  
  output: {
    additionalPool,
    messageContent,
  }
*/
async function getSaveForUser(input, iocContainer) {   
  let userId = input.userId
  let guildId = input.guildId
  let saveOptIns = input.saveOptIns
  let saveOptOuts = input.saveOptOuts
  
  let messageContent = []
  let additionalPool = []
  
  // Array size two: first key, second save value
  let result = await iocContainer.cache.getCacheEntries()
  
  let saveValues = result
    .filter(kv => isGlobalCacheKey(kv.key, guildId))
    .filter(kv => isInNameList(kv.key, guildId, saveOptIns, kv.value))
    .filter(kv => !isInNameList(kv.key, guildId, saveOptOuts, kv.value))
    .map(kv => kv.value)
  let userUsed = new Set(result.filter(kv => isUserCacheKey(kv.key, userId)).map(kv => kv.key))
  
  for (const saveValue of saveValues) {
    let name = saveValue.saveName
    if(saveValue.action == ACTION_SINGLE){
      let userCacheKey = getUserCacheKey(userId, getGlobalCacheKey(guildId, name))
      if(userUsed.has(userCacheKey)) {
        console.log(`User ${userId} already received saved dice '${name}'`)
        continue
      }
      
      await cacheValue(userCacheKey, {}, iocContainer)
    }
    
    messageContent.push(`Bonus dice due to '${name}': ${messages.FormatInputDicePool(saveValue.dicePool)}`)
    additionalPool.push(...saveValue.dicePool)
  }
  
  
  return {
    additionalPool: additionalPool,
    messageContent: messageContent,
  }
}

/*
input: {
    userId,
    guildId,
  }
iocContainer: IocContainer  
  
output: { 
  messageContent - list
  hasSaves - bool
}
*/
async function showSaves(input, iocContainer) { 

  let userId = input.userId
  let guildId = input.guildId

  let messageContent = []
  
  let result = await iocContainer.cache.getCacheEntries()

  let saveValues = result.filter(kv => isGlobalCacheKey(kv.key, guildId)).map(kv => kv.value)
  let userUsed = new Set(result.filter(kv => isUserCacheKey(kv.key, userId)).map(kv => kv.key))

  for (const saveValue of saveValues) {
    let name = saveValue.saveName

    let isUsableByUser = true
    let type = 'scene'

    if(saveValue.action == ACTION_SINGLE){
      type = 'single/round'
      let userCacheKey = getUserCacheKey(userId, getGlobalCacheKey(guildId, name))    
      if(userUsed.has(userCacheKey)) {
        isUsableByUser = false
      }
    }
    
    let wildcardMode = getWildcardMode(saveValue)
    let modeFormat = wildcardMode == WILDCARD_MODE_OPT_OUT? ', defaultly opted out' : ''
    
    let extra = isUsableByUser ? '' : ' - **already used by user**'
    messageContent.push(`*${name}* (${type}${modeFormat}): ${messages.FormatInputDicePool(saveValue.dicePool)}${extra}`)
  }

  if (messageContent.length <= 0) {
    messageContent.push(`No saved results.`)
    return {
      messageContent: messageContent,
      hasSaves: false,
    }
  }

  return {
    messageContent: messageContent,
    hasSaves: true,
  }
}


// Default value: opt in.
function getWildcardMode (saveValue) {
  let value = saveValue.wildcardMode ?? 0
  return value == 0 ? WILDCARD_MODE_OPT_IN : value
}

/*
input: {
  guildId,
  saveNames,
  }
iocContainer: IocContainer  

output: messageContent list
*/
async function deleteSave(input, iocContainer) {
  let guildId = input.guildId
  let saveNames = input.saveNames
  
  if (!saveNames || saveNames?.length == 0) {
    return [`No input name. Did not delete anything.`]
  }
  
   let keysToDelete = []
   let allEntries = await iocContainer.cache.getCacheEntries()
  
   let deletedNames = "all"
  
   if (saveNames == ALL_NAMES) {
      keysToDelete.push(...allEntries.map(kv => kv.key).filter(isSaverCacheKey))
    } else {
      var globalKeys = saveNames.map(saveName => getGlobalCacheKey(guildId, saveName))
      keysToDelete.push(...globalKeys)
      
      deletedNames = `${saveNames.map(n => "'"+n+"'").join(", ")}`
      
      // All per-user key for this key
      let userKeys = allEntries
          .filter(kv =>  globalKeys.some(globalKey => isUserCacheKeyOfGlobal(kv.key, globalKey)))
          .map(kv => kv.key)
      console.log(`Deleting ${userKeys.length} user keys of key ${deletedNames}`);      
      keysToDelete.push(...userKeys)
    }
  
  console.log(`Deleting total ${keysToDelete.length} keys`);
  for (let key of keysToDelete) {
    await iocContainer.cache.clearCacheKey(key)
  }
  
  return [`Deleted saved result for ${deletedNames}.`]
}


const GLOBAL_PREFIX = 'SAVE'
const USER_PREFIX = 'USER'
const SEPARATOR = ':'

function isInNameList(key, guildId, saveOptIns, saveValue) {
  let wildcardMode =  getWildcardMode(saveValue)
  
  if (saveOptIns == ALL_NAMES) {
    return wildcardMode == WILDCARD_MODE_OPT_IN
  }
  let nameSet = new Set(saveOptIns.map(name => getGlobalCacheKey(guildId, name)))
   
  return nameSet.has(key)
}


function isSaverCacheKey (key) {
  return key.startsWith(`${GLOBAL_PREFIX}`) || key.startsWith(`${USER_PREFIX}`)
}


function isGlobalCacheKey (key, guildId) {
  return key.startsWith(`${GLOBAL_PREFIX}${SEPARATOR}${guildId}${SEPARATOR}`)
}

function isUserCacheKey (key, userId) {
  return key.startsWith(`${USER_PREFIX}${SEPARATOR}${userId}${SEPARATOR}`)
}

function isUserCacheKeyOfGlobal(key, globalKey) {
  return key.startsWith(`${USER_PREFIX}${SEPARATOR}`) && key.endsWith(`${SEPARATOR}${globalKey}`)  
}


function getGlobalCacheKey (guildId, name) {
  return `${GLOBAL_PREFIX}${SEPARATOR}${guildId}${SEPARATOR}${name}`;
}


function getUserCacheKey (userId, globalCacheKey ){
  return `${USER_PREFIX}${SEPARATOR}${userId}${SEPARATOR}${globalCacheKey}`
}


/*
saveValue = {
  saveName
  dicePool
  action
}
*/
async function cacheValue(key, value, iocContainer){
  return await iocContainer.cache.cacheValue(
    key, 
    value, 
    iocContainer.options.saveDiceCacheTimeSecs)
}

/*
// Given guide ID, returns user IDs in the channel.
async function getChannelUserIds(guildId) {
  let members = await lib.discord.guilds['@0.2.3'].members.list({
    guild_id: guild_id,
    limit: 100
  });
  
  console.log('members list result: ' + JSON.stringify(members))
  return members.map(member => member.user.id)
} */
  