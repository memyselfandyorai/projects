import type {
  Client,
  Guild,
  ChatInputCommandInteraction,
  GuildMember,
  Message,
  MessageCreateOptions,
  DiscordAPIError,
  PartialTextBasedChannelFields,
  TextBasedChannel,
} from "discord.js";

import { ChannelType, PermissionFlagsBits, OAuth2Scopes } from "discord.js";
import type { CommandOption, CommandInteraction } from "./api/commands";
import type { PrefixCommandInteraction } from "./api/prefix_commands";
import type {
  OutputMessage,
  MessageSender,
  MessageOptions,
} from "./api/messages";
import { NoPermissionsError } from "./api/messages";
import type { IocContainer } from "./api/ioc_container";
import type { PinnedMessage, PinManager } from "./api/pin_manager";
import type { DiscordClientInteraction } from "./api/client_manager";

// REMINDER: manually generate using "npx tsc" in Replit. 97 errors is standard.

// Docs: https://discord.js.org/docs/packages/discord.js/14.14.1/ChatInputCommandInteraction:Class#channel

export class NodeJsCommand implements CommandInteraction {
  interaction: ChatInputCommandInteraction;
  messageSender: NodeJsMessageSender;
  iocContainer: IocContainer;
  constructor(
    interaction: ChatInputCommandInteraction,
    iocContainer: IocContainer,
  ) {
    this.interaction = interaction;
    this.messageSender = new NodeJsMessageSender(interaction.channel);
    this.iocContainer = iocContainer;
  }

  getIocContainer(): any {
    return this.iocContainer.getIocContainer();
  }

  getAllInputOptions(): string[] {
    return this.interaction.options.data.map((option) => option.name);
  }

  inputOptions(...names: string[]): CommandOption[] {
    return names.map(
      (name) =>
        <CommandOption>{
          name,
          value: this.interaction.options.get(name)?.value,
        },
    );
  }

  getInputOptionsMap(): Map<string, string | number | boolean> {
    var inputMap = new Map<string, string | number | boolean>();
    this.getAllInputOptions().forEach((optionName) => {
      inputMap.set(optionName, this.inputOptions(optionName)[0].value);
    });
    return inputMap;
  }

  user(): string {
    return (
      (this.interaction.member as GuildMember)?.nickname ??
      this.interaction.user.globalName ??
      this.interaction.user.username
    );
  }

  getUserId(): string {
    return this.interaction.user.id;
  }

  getChannelId(): string {
    return this.interaction.channelId;
  }

  getMessageId(): string {
    return this.interaction.id;
  }

  getPinManager(): PinManager {
    return new NodeJsPinManager(this.interaction);
  }

  async send(
    message: OutputMessage,
    options: MessageOptions?,
  ): Promise<string> {
    return await this.messageSender.send(message, options);
  }

  async prepareCommandReply(): Promise<void> {
    await this.interaction.deferReply();
  }

  async reply(message: OutputMessage): Promise<string> {
    const msg = await this.interaction.editReply(message.text);
    return msg.id;
  }

  public toString(): string {
    return `${this.interaction.toString()}, token ${this.interaction.token}`;
  }
}

export class NodeJsPrefixCommandInteraction
  implements PrefixCommandInteraction
{
  message: Message;
  messageSender: MessageSender;
  adminId: string;
  iocContainer: IocContainer;
  constructor(message: Message, adminId: string, iocContainer: IocContainer) {
    this.message = message;
    this.messageSender = new NodeJsMessageSender(message.channel);
    this.adminId = adminId;
    this.iocContainer = iocContainer;
  }

  getIocContainer(): any {
    return this.iocContainer.getIocContainer();
  }

  botId(): string {
    return this.message.client.user.id;
  }

  getAdminId(): string {
    return this.adminId;
  }

  senderId(): string {
    return this.message.author.id;
  }

  getMessageId(): string {
    return this.message.id;
  }

  generateInvite(): string {
    const link = this.message.client.generateInvite({
      permissions: [
        PermissionFlagsBits.ViewChannel,
        PermissionFlagsBits.CreateInstantInvite,
        PermissionFlagsBits.ChangeNickname,
        PermissionFlagsBits.SendMessages,
        PermissionFlagsBits.ManageMessages,
        PermissionFlagsBits.UseApplicationCommands,
        PermissionFlagsBits.ReadMessageHistory,
      ],
      scopes: [
        OAuth2Scopes.Identify,
        OAuth2Scopes.Bot,
        OAuth2Scopes.ApplicationsCommands,
      ],
    });

    return link;
  }

  async send(
    message: OutputMessage,
    options: MessageOptions?,
  ): Promise<string> {
    return await this.messageSender.send(message, options);
  }
}

class NodeJsMessageSender implements MessageSender {
  channel: PartialTextBasedChannelFields | null;
  constructor(channel: PartialTextBasedChannelFields | null) {
    this.channel = channel;
  }

  async send(
    message: OutputMessage,
    options: MessageOptions?,
  ): Promise<string> {
    const reply = options?.reply_to_message_id
      ? {
          messageReference: options?.reply_to_message_id,
          // failIfNotExists: false,
        }
      : undefined;
    const embeds = options?.embed ? [options.embed] : [];
    const createOptions: MessageCreateOptions = {
      content: message.text,
      reply: reply,
      embeds: embeds,
    };

    if (options.extra_data) {
      Object.assign(createOptions, options.extra_data);
    }
    const createdMessage = await this.channel?.send(createOptions);
    return createdMessage?.id ?? "";
  }
}

export class NodeJsPinManager implements PinManager {
  interaction: ChatInputCommandInteraction;
  constructor(interaction: ChatInputCommandInteraction) {
    this.interaction = interaction;
  }

  async fetchAllPinnedMessages(channelId: string): Promise<PinnedMessage[]> {
    const messages =
      (await this.interaction.channel?.messages.fetchPinned()) ?? [];

    const pinnedMessages: Message[] = [];
    messages.forEach((message: Message) => pinnedMessages.push(message));

    return pinnedMessages.map(
      (msg: Message) =>
        <PinnedMessage>{
          id: msg.id,
          timestamp: msg.createdAt,
          content: { text: msg.content },
        },
    );
  }
  async deletePinnedMessage(
    channelId: string,
    messageId: string,
  ): Promise<void> {
    try {
      await this.interaction.channel?.messages.unpin(messageId);
    } catch (error: any) {
      const discordError = error as DiscordAPIError;
      if (discordError) {
        if (discordError.code == 50_013) {
          throw new NoPermissionsError("No UNPIN permissions for bot", {
            cause: error,
          });
        }
      }
    }
  }

  async pinMessage(channelId: string, messageId: string): Promise<void> {
    await this.interaction.channel?.messages.pin(messageId);
  }

  async updatePinnedMessage(
    channelId: string,
    messageId: string,
    message: OutputMessage,
  ): Promise<void> {
    await this.interaction.channel?.messages.edit(messageId, message.text);
  }
}

export class NodeJsDiscordClientInteraction
  implements DiscordClientInteraction
{
  client: Client;
  guild: Guild;
  constructor(client: Client, guild: Guild) {
    this.client = client;
    this.guild = guild;
  }

  async sendJoinMessage(): Promise<void> {
    const channels = await this.guild.channels.fetch();
    let generalChannel = channels.find(
      (channel) =>
        channel.name.toLowerCase() === "general" &&
        channel.type === ChannelType.GuildText,
    );

    if (!generalChannel) {
      generalChannel = channels.find((channel) => {
        return channel.type === ChannelType.GuildText;
      });
    }

    let deployedMessageContact = [
      `Thanks for installing this Discord bot.`,
      `Use the slash-commands to see which commands are available.`,
      `The most basic command is **/r**.`,
      `You can also use **/help** or read more on https://sites.google.com/view/youreye-communityprojects/cortex-prime-discord-bot.`,
      `By using this bot, you're agreeing to its Privacy Agreement (https://sites.google.com/view/youreye-communityprojects/cortex-prime-discord-bot#h.jhkjxyd40jqi).`,
    ];

    if (generalChannel) {
      console.log("Sending deploy message");
      await generalChannel.send(deployedMessageContact.join("\n"));
    } else {
      console.log("No text channel found");
    }
  }
}

/*
// iterating existing channels:
const joinDeltaMiliseconds = 10 * 60 * 1000; // 10 minutes

// TODO: client.generateInvite?
this.client.guilds.cache.forEach((guild) => {
  console.log(`Guild Name: ${guild.name} (ID: ${guild.id})`);

  // You can loop through channels here as well
  guild.channels.cache.forEach((channel) => {
    console.log(`  - Channel Name: ${channel.name} (ID: ${channel.id})`);
    const now = new Date().getTime();
    const joined = guild.joinedAt.getTime();
    if (
      channel.type == ChannelType.GuildText &&
      now < joined + joinDeltaMiliseconds
    ) {
      console.log(`text: ` + guild.joinedAt);

      // TODO: need to copy the "joined" logic, checking for "general" or similar text (per guild).
      // TODO: how to catch it when server in ON ?

      // channel.send("Hello from your new bot!"); ??
    }
  });
});
*/
