"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IocContainer = void 0;
const storage_1 = require("../../common/storage");
require("../storage/in_memory_storage");
const chooser_1 = require("../storage/chooser");
class IocContainer {
    constructor() {
        Object.defineProperty(this, "storage", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
    }
    consturctor() {
        this.storage = (0, chooser_1.getDefaultStorage)();
    }
    setStorage(storage) {
        this.storage = storage;
    }
    getIocContainer() {
        return {
            getNextNumber: function (min, max) {
                return Math.floor(Math.random() * max) + min;
            },
            options: {
                cacheTimeSecs: 600 /*10 mins*/,
                saveDiceCacheTimeSecs: 14400 /*4 hours*/,
                extraEffectDieModeOn: false /* show D12+*/,
                extraRollDieIncreaseModeOn: false /* D12 step ups is limited to D4 */,
            },
            cache: this.storage,
            storage: new storage_1.Storage(this.storage),
        };
    }
}
exports.IocContainer = IocContainer;
//# sourceMappingURL=ioc_container.js.map