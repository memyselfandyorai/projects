// authenticates you with the API standard library

const roller = require('../../../../../../common/roller.js');
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

const MIN_EFFECT = 4

let diceRoll = context.params.event.content.split(' ').slice(1).map(x => parseInt(x)).filter(x => !isNaN(x))
let input = { diceRoll: diceRoll }
let messageContent = roller.RollCortexPrime(input).messageContent

await lib.discord.channels['@0.0.6'].messages.create({
  channel_id: context.params.event.channel_id,
  // 947924911836524545
  //context.params.event.channel_id,
  content: messageContent.join('\n') ,
  message_reference: {
    message_id: context.params.event.id
  }
});

