import type { Client, TextBasedChannel } from "discord.js";
import type { FlushableDataStorage, StorageInitParams } from "./base_storage";
import type { StorageItem } from "../api/storage";
export declare function createDiscordDatabase(params: StorageInitParams): Promise<DiscordDatabase>;
export declare class DiscordDatabase implements FlushableDataStorage {
    cacheMap: Map<string, StorageItem>;
    discordStorage: DiscordStorage;
    currentState?: StorageState | null;
    constructor(discordStorage: DiscordStorage);
    getOrReadState(): Promise<StorageState>;
    setItem(key: string, value: StorageItem): Promise<void>;
    getAllItemKeys(): Promise<string[]>;
    getItem(key: string): Promise<StorageItem | null | undefined>;
    deleteItem(key: string): Promise<void>;
    flush(): Promise<void>;
}
interface StorageState {
    entries: Map<string, any>;
    existingMessageIds: string[];
    hasChanged: boolean;
}
export declare class DiscordStorage {
    client: Client;
    channel: TextBasedChannel;
    constructor(client: Client, channel: TextBasedChannel);
    readExistingState(): Promise<StorageState>;
    writeState(storageState: StorageState): Promise<void>;
    deleteFileMessages(storageState: StorageState): Promise<void>;
    writeMessagesAsFiles(messagesToSend: Uint8Array[], fullContentLength: number): Promise<string[]>;
}
export {};
//# sourceMappingURL=discord_storage.d.ts.map