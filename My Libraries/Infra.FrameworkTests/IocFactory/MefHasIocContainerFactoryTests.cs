﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Infra.Framework.IocFactory;
using System;
using System.Collections.Generic;
using System.Text;
using Infra.Framework.Ioc;
using Moq;
using Infra.Framework.Mef;

namespace Infra.Framework.IocFactory.Tests
{
    [TestClass()]
    public class MefHasIocContainerFactoryTests
    {
        [TestMethod()]
        public void CreateInstanceTest()
        {
            Mock<IIocContainer> mockContainer = new Mock<IIocContainer>();
            IHasIocContainerFactory factory = new MefHasIocContainerFactory(mockContainer.Object);

            TestHasIoc result = factory.CreateInstance<TestHasIoc>(5);

            Assert.AreEqual(5, result.Prop);
            Assert.AreEqual(mockContainer.Object, result.Container);
        }
    }

    [ExportForMef(typeof(IHasIocContainer))]
    class TestHasIoc : IHasIocContainer
    {
        public TestHasIoc()
        {

        }

        public TestHasIoc(IIocContainer container, int prop)
        {
            Container = container;
            Prop = prop;
        }

        public IIocContainer Container { get; }
        public int Prop { get; }
    }
}