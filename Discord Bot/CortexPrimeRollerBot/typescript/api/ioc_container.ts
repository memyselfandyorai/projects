import { Storage } from "../../common/storage";
import type { DataStorage } from "./storage";
import { InMemoryDatabase } from "../storage/in_memory_storage";
import { getDefaultStorage } from "../storage/chooser";

export class IocContainer {
  storage: DataStorage;
  consturctor() {
    this.storage = getDefaultStorage();
  }

  setStorage(storage: DataStorage): void {
    this.storage = storage;
  }

  getIocContainer(): any {
    return {
      getNextNumber: function (min: number, max: number) {
        return Math.floor(Math.random() * max) + min;
      },
      options: {
        cacheTimeSecs: 600 /*10 mins*/,
        saveDiceCacheTimeSecs: 14400 /*4 hours*/,
        extraEffectDieModeOn: false /* show D12+*/,
        extraRollDieIncreaseModeOn: false /* D12 step ups is limited to D4 */,
      },
      cache: this.storage,
      storage: new Storage(this.storage),
    };
  }
}
