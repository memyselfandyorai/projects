const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});
const controller= require('../../../../common/controller.js');
const {ReplyToCommand, ReceivedCommand}= require('../../../../common/commands.js');

await ReceivedCommand(context.params.event)  

const MOD = 1
const SAVE_ACTIONS = 2
const GENERAL = 3

const DOC_URL = 'https://autocode.com/app/youreye/discord-cortex-prime#cortex-prime-roller-bot'

let inputOptions = parseInput(context.params.event)
let messageContent = []

// Special mod.
switch(inputOptions.subject?? 0) {
  case 0:
  case GENERAL:
     messageContent = [      
      `See more info on ${DOC_URL}.`,
    ]
    break;
  case MOD :   
    messageContent = [
      '**SHOCK**: Adding D6 to dice pool, increasing effect, stepping down largest die.',
      '**OBLIVIOUS**: Result of 2 counts as 1.',
      `See more info on ${DOC_URL}.`,
      ]
    break;
  case SAVE_ACTIONS: 
     messageContent = [
      `**SCENE**: Saves dice for the whole session by a specific name. You must delete it manually at the end of the scene.`,
      `**ROUND**: Saves dice for a single use per user by a specific name. Recommend deleting it manually when effect is done, especially when someone didn't get a chance to use it.`,
      `See more info on ${DOC_URL}.`,
    ]
    break;
}

await ReplyToCommand(context.params.event, messageContent)  


// Returns input: {subject}
function parseInput(event) {
  let dataOptions = event.data.options
  
  var inputMap = new Map()
  dataOptions.forEach(option => {
    inputMap.set(option.name, option.value)
  })
  
  return  {
    subject: inputMap.get('subject'),
  }
}


// Creation command:
/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "help",
  "description": "Adds a helpful description per needs",
  "options": [
    {
      "type": 4,
      "name": "subject",
      "description": "The subject to need help with.",
      "choices": [
        {
          "name": "general",
          "value": 0
        },
        {
          "name": "/r mods",
          "value": 1
        },
        {
          "name": "save lifespan",
          "value": 2
        }
      ],
      "required": true
    }
  ]
});

*/