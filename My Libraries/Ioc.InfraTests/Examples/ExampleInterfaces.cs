﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioc.InfraTests.Examples
{
    interface ILogger
    {
        void LogDebug(string msg, Exception exception = null);
    }


    #region AppMangager

    enum AppFlavor
    {
        MASTER,
        SLAVE,
    }

    class AppManagerArgs
    {
        public int Arg1 { get; private set; }
    }

    interface IAppManager : IDisposable
    {
        // Notice: each implementation should have both an empty ctor and one that receives ILogger

        AppFlavor SupportedFlavor { get; }

        void Start(AppManagerArgs args);

        void Stop();
    }


    interface IAppManagerFlavorFactory
    {
        IAppManager GetAppManager(AppFlavor flavor);
    }
    #endregion


}
