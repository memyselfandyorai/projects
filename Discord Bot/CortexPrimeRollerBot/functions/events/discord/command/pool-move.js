const { MoveBetweenPools } = require("../../../../common/poolsController.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  let input = {};
  await MoveBetweenPools(input, commandInteraction);
}

// Command:
/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "pool-move",
  "description": "Moves dice between pools.",
  "options": [
    {
      "type": 3,
      "name": "from",
      "description": "The source pool to move the dice from.",
      "required": true
    },
    {
      "type": 3,
      "name": "to",
      "description": "The target pool to move the dice to.",
      "required": true
    },
    {
      "type": 3,
      "name": "dice",
      "description": "Dice to move.",
      "required": true
    }
  ]
});
*/
