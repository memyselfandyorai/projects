﻿using FolderSyncronizer.Core.Watchers.Actions;
using FolderSyncronizer.Debugging;
using Infra.Framework.Extensions;
using Infra.Framework.Mef;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FolderSyncronizer.Core.Watchers.Handlers
{
    [ExportForMef(typeof(IWatcherHandler))]
    class ChangedFileHandler : IWatcherHandler
    {
        public WatcherChangeTypes SupportedType => WatcherChangeTypes.Changed;

        public IEnumerable<IFileAction> HandleChange(string sourceFolder, string targetFolder, FileSystemEventArgs eventArgs)
        {
            if (Directory.Exists(eventArgs.FullPath))
            {
                Singleton.Logger.LogError($"Modify is unsupported for directory: '{eventArgs.FullPath}'");
                // Does not support directories.
                return Enumerable.Empty<IFileAction>();
            }

            string fileInSource = eventArgs.FullPath;
            string relative = Path.GetRelativePath(sourceFolder, fileInSource);
            return new CopyAction(fileInSource, Path.Combine(targetFolder, relative), overwrite: true).ToSingleton();
        }
    }
}
