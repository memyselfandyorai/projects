const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});
const controller = require('../../../../../../common/controller.js');
const {SendMessage}= require('../../../../../../common/commands.js');

const ADMIN_ID = 305439864592138240

let userId = context.params.event.author.id

let messageContent = []

console.log(`Admin prefix by: ` + userId );

if (userId != ADMIN_ID) {
  messageContent.push('User is not an admin.')
} else {
  let options = {
    rawValue: true,
  }
  let entries = await controller.DefaultIocContainer.storage.getAllStoredEntries(options)
  messageContent.push(`Storage size: ${entries.length}.`)
  entries.forEach(entry => {
    messageContent.push(`Storage entry: ${JSON.stringify(entry)}.`)
  })  
  
  let params = context.params.event.content.split(' ')
  console.log(`params: ` +params.join(", ") );
  if(params.includes('clearStorage')) {    
    await controller.DefaultIocContainer.storage.clearKeys([])
    messageContent.push(`Cleared storage`)
  } 
  if(params[1]  == 'deleteStorageKeys') {
    let keys = params[2]?.split('.')
    await controller.DefaultIocContainer.storage.clearKeys(keys)
    messageContent.push(`Deleting keys: ` + keys.join("."))
  }
}

let options = {
  splitMessages: true,
  replyToMessage: true,
}
await SendMessage(context.params.event, messageContent, options)

