﻿namespace FolderSyncronizer.Core.Watchers
{
    /// <summary>
    /// Filter meant for multiple folders handling the same files.
    /// </summary>
    public interface IFolderFilter
    {
        void Clear();

        void AddSourceFolder(string folder);

        bool ShouldHandleFile(string sourceFolderPath, string filePath);
    }
}
