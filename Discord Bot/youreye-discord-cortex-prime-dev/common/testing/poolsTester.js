const controller = require('../controller.js');
const {TestCase, createIocContainer, throwIfNotEqual}= require('./common.js');
const {
  POOL_ADD, POOL_REMOVE, POOL_CLEAR,
  STEP_MOD_UP, STEP_MOD_DOWN ,
 } = require('../pools.js');    


const {
  prepareConstantPools,
  POOL_NAME, SECOND_POOL_NAME, NON_POOL_NAME,
  GUILD_ID, USER_ID, OTHER_GUILD_ID,
  prepareConstantSaveDices,
  CONSTANT_ROLL_CACHE_KEY,
  SAVE_NAME, SINGLE_SAVE_NAME, OTHER_SAVE_NAME,
} = require('./constantRolls.js');



module.exports = {
  TestCases: createTestCases(),
};



function createTestCases() {
  return [
    new TestCase({
      name: 'testModifyDicePool_addedDice_diceAdded',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '6 2D8',
        names: POOL_NAME, 
        action: POOL_ADD,
      },
      expectedResult: [
        `Created new pool '${POOL_NAME}': 1D6 2D8.`
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_invalidNameComma_invalid',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '6 2D8',
        names: 'with,comma', 
        action: POOL_ADD,
      },
      expectedResult: [
        `Invalid name 'with,comma': must not contain ',', '\\*' or '.'.`,
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_invalidNameWildcard_invalid',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '6 2D8',
        names: '*wild', 
        action: POOL_ADD,
      },
      expectedResult: [
        `Invalid name '\\*wild': must not contain ',', '\\*' or '.'.`,
      ],    
    }),  
     new TestCase({
      name: 'testModifyDicePool_invalidNameDot_invalid',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '6 2D8',
        names: 'dot.', 
        action: POOL_ADD,
      },
      expectedResult: [
        `Invalid name 'dot.': must not contain ',', '\\*' or '.'.`,
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_addedDiceToExisting_diceAdded',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '4 8',
        names: POOL_NAME, 
        action: POOL_ADD,
        prepareInput: {},
        appendShow: true,
      },
      expectedResult: [
        `Added to pool '${POOL_NAME}': 1D4 1D8. New pool: 1D4 1D6 2D8 2D10.`,
        `*${POOL_NAME}*: 1D4 1D6 2D8 2D10`,
        `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_removeDiceExisting_diceRemoved',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '8 10',
        names: POOL_NAME, 
        action: POOL_REMOVE,
        prepareInput: {},
        appendShow: true,
      },
      expectedResult: [
        `Removed from pool '${POOL_NAME}': 1D8 1D10. New pool: 1D6 1D10.`,
        `*${POOL_NAME}*: 1D6 1D10`,
        `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_emptyDice_error',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '',
        names: POOL_NAME, 
        action: POOL_REMOVE,
        prepareInput: {},
        appendShow: true,
      },
      expectedResult: [
        `No dice to modify: ''`,
        `*${POOL_NAME}*: 1D6 1D8 2D10`,
        `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }), 
    new TestCase({
      name: 'testModifyDicePool_zeroDice_error',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '0d8',
        names: POOL_NAME, 
        action: POOL_REMOVE,
        prepareInput: {},
        appendShow: true,
      },
      expectedResult: [
        `No dice to modify: '0d8'`,
        `*${POOL_NAME}*: 1D6 1D8 2D10`,
        `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),     
    new TestCase({
      name: 'testModifyDicePool_removeDicePartiallyExisting_error',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '4 8',
        names: POOL_NAME, 
        action: POOL_REMOVE,
        prepareInput: {},
        appendShow: true,
      },
      expectedResult: [
        `Dice '1D4' are missing from pool '${POOL_NAME}'.`,
        `*${POOL_NAME}*: 1D6 1D8 2D10`,
        `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_removeDiceFromNonExisting_notFound',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '4 8',
        names: NON_POOL_NAME, 
        action: POOL_REMOVE,
        prepareInput: {},
        appendShow: true,
      },
      expectedResult: [
        `Pool not found: '${NON_POOL_NAME}'.`,
        `*${POOL_NAME}*: 1D6 1D8 2D10`,
        `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_wrongCommand_noMessages',
      functionToRun: testModifyDicePool,
      inputArgs: {
        dice: '4 8',
        names: POOL_NAME, 
        action: 1234,
        prepareInput: {},
      },
      expectedResult: [        
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_clearFromExisting_cleared',
      functionToRun: testModifyDicePool,
      inputArgs: {
        names: POOL_NAME, 
        action: POOL_CLEAR,
        prepareInput: {},
        appendShow: true,
      },
      expectedResult: [
        `Cleared pools: '${POOL_NAME}'.`,
        `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_clear2FromExisting_cleared',
      functionToRun: testModifyDicePool,
      inputArgs: {
        names: POOL_NAME + controller.POOL_NAME_SEPARATOR+ SECOND_POOL_NAME + controller.POOL_NAME_SEPARATOR + NON_POOL_NAME, 
        action: POOL_CLEAR,
        prepareInput: {},
      },
      expectedResult: [
        `Cleared pools: '${POOL_NAME}', '${SECOND_POOL_NAME}'.`
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_clearAllFromExisting_cleared',
      functionToRun: testModifyDicePool,
      inputArgs: {
        names: controller.POOL_NAME_ALL_WILCARD, 
        action: POOL_CLEAR,
        prepareInput: {},
      },
      expectedResult: [
        `Cleared all pools: '${POOL_NAME}', '${SECOND_POOL_NAME}'.`
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_clearAllFromNonExisting_none',
      functionToRun: testModifyDicePool,
      inputArgs: {
        names: controller.POOL_NAME_ALL_WILCARD, 
        action: POOL_CLEAR,
      },
      expectedResult: [
        `Cleared all pools: none.`
      ],    
    }),  
    new TestCase({
      name: 'testModifyDicePool_clearNonExisting_none',
      functionToRun: testModifyDicePool,
      inputArgs: {
        names: NON_POOL_NAME, 
        action: POOL_CLEAR,
        prepareInput: {},
      },
      expectedResult: [
        `Cleared pools: none.`
      ],    
    }),  
    
    new TestCase({
      name: 'testStepPool_nonexistingDie_noStepUp',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: 4,
        steps: 1,
        mod : STEP_MOD_UP,
        prepareInput: {},
      },
      expectedResult: [
        `No step up/down found for D4: did not step up/down`,
        `Pool '${POOL_NAME}' has not changed: 1D6 1D8 2D10`,
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_nonexistingPool_notFound',
      functionToRun: testStepPool,
      inputArgs: {
        name: NON_POOL_NAME, 
        die: 4,
        steps: 1,
        mod : STEP_MOD_UP,
        prepareInput: {},
      },
      expectedResult: [
        `Pool not found: ${NON_POOL_NAME}`
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_existingDie_stepUp',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: 6,
        steps: 1,
        mod : STEP_MOD_UP,
        prepareInput: {},
      },
      expectedResult: [
        `Modified roll die: D6 -> D8`,
        `Stepped up pool '${POOL_NAME}': 2D8 2D10`
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_stepDownTwo_stepDown',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: 10,
        steps: 2,
        mod : STEP_MOD_DOWN,
        prepareInput: {},
      },
      expectedResult: [
        `Modified roll die: D10 -> D6`,
        `Stepped down pool '${POOL_NAME}': 2D6 1D8 1D10`
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_stepDownMin_keepsInMin',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: 10,
        steps: 4,
        mod : STEP_MOD_DOWN,
        prepareInput: {},
      },
      expectedResult: [
        `Modified roll die: D10 -> D4`,
        `Stepped down pool '${POOL_NAME}': 1D4 1D6 1D8 1D10`
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_stepUpMax_keepsInMax',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: 10,
        steps: 2,
        mod : STEP_MOD_UP,
        prepareInput: {},
      },
      expectedResult: [
        `Modified roll die: D10 -> D12+D4`,
        `Stepped up pool '${POOL_NAME}': 1D6 1D8 1D10 1D12`
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_stepUp0_error',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: 10,
        steps: 0,
        mod : STEP_MOD_UP,
        prepareInput: {},
      },
      expectedResult: [
        `Number of steps must be positive. Got: 0`,
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_stepUpNegative_error',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: 10,
        steps: -1,
        mod : STEP_MOD_UP,
        prepareInput: {},
      },
      expectedResult: [
        `Number of steps must be positive. Got: -1`,
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_stepDieNegative_error',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: -1,
        steps: 1,
        mod : STEP_MOD_UP,
        prepareInput: {},
      },
      expectedResult: [
        `Die must be positive. Got: -1`,
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_unknownStepDie_error',
      functionToRun: testStepPool,
      inputArgs: {
        name: POOL_NAME, 
        die: 10,
        steps: 1,
        mod : 1234,
        prepareInput: {},
      },
      expectedResult: [
        `Unknown pool stepup mode: 1234`,
      ],    
    }),  
    new TestCase({
      name: 'testStepPool_stepUpMaxFromMax_nothingChanged',
      functionToRun: testStepPool,
      inputArgs: {
        name: SECOND_POOL_NAME, 
        die: 12,
        steps: 2,
        mod : STEP_MOD_UP,
        prepareInput: {},
      },
      expectedResult: [
        `Modified roll die: D12 -> D12+D4`,
        `Pool '${SECOND_POOL_NAME}' has not changed: 1D12`,
      ],    
    }),  
       
    new TestCase({
      name: 'testRollPool_rollSimple_works',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: POOL_NAME, 
        prepareInput: {},
        actualRollResults: [1, 2, 7, 8]
      },
      expectedResult: [
        `Dice pool '${POOL_NAME}': 1D6 1D8 2D10`,
        'Rolling: 1D6 1D8 2D10',
        'D6 : **(1)**',
        'D8 : 2',
        'D10 : 7 8',
        'Results ordered by best total:',
        'Score: 15 (7+8) with Effect: D8',
        'Score: 10 (2+8) with Effect: D10',
      ],    
    }),  
    new TestCase({
      name: 'testRollPool_multiplePools_works',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: `${SECOND_POOL_NAME} , ${POOL_NAME}`, 
        prepareInput: {},
        actualRollResults: [1, 1, 2, 7, 8]
      },
      expectedResult: [
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        `Dice pool '${POOL_NAME}': 1D6 1D8 2D10`,
        'Rolling: 1D6 1D8 2D10 1D12',
        'D6 : **(1)**',
        'D8 : 2',
        'D10 : 7 8',
        'D12 : **(1)**',
        'Results ordered by best total:',
        'Score: 15 (7+8) with Effect: D8',
        'Score: 10 (2+8) with Effect: D10',
      ],    
    }),  
    new TestCase({
      name: 'testRollPool_allPoolsWildcard_works',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: `*`, 
        prepareInput: {},
        actualRollResults: [1, 2, 7, 8, 1]
      },
      expectedResult: [
        `Dice pool '${POOL_NAME}': 1D6 1D8 2D10`,
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        'Rolling: 1D6 1D8 2D10 1D12',
        'D6 : **(1)**',
        'D8 : 2',
        'D10 : 7 8',
        'D12 : **(1)**',
        'Results ordered by best total:',
        'Score: 15 (7+8) with Effect: D8',
        'Score: 10 (2+8) with Effect: D10',
      ],    
    }),  
    new TestCase({
      name: 'testRollPool_notFoundInList_notFound',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: `${SECOND_POOL_NAME} , ${POOL_NAME}, ${NON_POOL_NAME}`, 
        prepareInput: {},
        actualRollResults: []
      },
      expectedResult: [
        `Pool not found: '${NON_POOL_NAME}'`
      ],  
    }),  
    
    new TestCase({
      name: 'testRollPool_rollWithKeepAndEffects_works',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: SECOND_POOL_NAME, 
        dice: '3d6',
        keep: 1,
        extra_effects: 2,
        prepareInput: {},
        actualRollResults: [6, 1, 3, 5]
      },
      expectedResult: [
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        'Rolling: 3D6 1D12',
        'D6 : **(1)** 3 5',
        'D12 : 6',
        'Results ordered by best total:',
        'Score: 6 (6) with Effects: D6,D6,D4',
        'Score: 5 (5) with Effects: D12,D6,D4',
      ],    
    }), 
    new TestCase({
      name: 'testRollPool_rollWithSave_ignoresSaveByDefault',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: SECOND_POOL_NAME, 
        dice: '3d6',
        keep: 1,
        prepareInput: {},
        actualRollResults: [6, 1, 3, 5],
        saverInputPreparationActions: [],
      },
      expectedResult: [
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        'Rolling: 3D6 1D12',
        'D6 : **(1)** 3 5',
        'D12 : 6',
        'Results ordered by best total:',
        'Score: 6 (6) with Effect: D6',
        'Score: 5 (5) with Effect: D12',
      ],    
    }), 
    new TestCase({
      name: 'testRollPool_rollWithOptedInSave_considersIn',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: SECOND_POOL_NAME, 
        dice: '1d6',
        keep: 1,
        prepareInput: {},
        actualRollResults: [6, 1, 2],
        save_opt_in: SAVE_NAME,
        saverInputPreparationActions: [],
      },
      expectedResult: [
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        `Bonus dice due to '${SAVE_NAME}': 1D6`,
        'Rolling: 2D6 1D12',
        'D6 : **(1)** 2',
        'D12 : 6',
        'Results ordered by best total:',
        'Score: 6 (6) with Effect: D6',
        'Score: 2 (2) with Effect: D12',
      ],    
    }),   
    new TestCase({
        name: 'testRollPool_rollWithOptedInWildcard_considersIn',
        functionToRun: testRollPool,
        inputArgs: {
          pool_name: SECOND_POOL_NAME, 
          dice: '1d6',
          keep: 1,
          prepareInput: {},
          actualRollResults: [6, 1, 2, 1, 1],
          save_opt_in: controller.SAVE_NAME_ALL_WILCARD,
          saverInputPreparationActions: [],
        },
        expectedResult: [
          `Dice pool '${SECOND_POOL_NAME}': 1D12`,
          `Bonus dice due to '${SAVE_NAME}': 1D6`,
          `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,          
          'Rolling: 2D6 2D8 1D12',
          'D6 : **(1)** 2',
          'D8 : **(1)** **(1)**',
          'D12 : 6',
          'Results ordered by best total:',
          'Score: 6 (6) with Effect: D6',
          'Score: 2 (2) with Effect: D12',
        ],    
      }),    
    new TestCase({
      name: 'testRollPool_rollWithMissingOptIn_ignoresOptIn',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: SECOND_POOL_NAME, 
        dice: '1d6',
        keep: 1,
        prepareInput: {},
        actualRollResults: [6, 1],
        save_opt_in: OTHER_SAVE_NAME,
        saverInputPreparationActions: [],
      },
      expectedResult: [
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        'Rolling: 1D6 1D12',
        'D6 : **(1)**',
        'D12 : 6',
        'Results ordered by best total:',
        'Score: 6 (6) with Effect: D4',
      ],    
    }),    
    new TestCase({
      name: 'testRollPool_rollWithOptedInWildcardAndOptedOut_considersOnlyIn',
      functionToRun: testRollPool,
      inputArgs: {
        pool_name: SECOND_POOL_NAME, 
        dice: '1d6',
        keep: 1,
        prepareInput: {},
        actualRollResults: [6, 2, 1, 1],
        save_opt_in: controller.SAVE_NAME_ALL_WILCARD,
        save_opt_out: SAVE_NAME,
        saverInputPreparationActions: [],
      },
      expectedResult: [        
        `Dice pool '${SECOND_POOL_NAME}': 1D12`,
        `Bonus dice due to '${SINGLE_SAVE_NAME}': 2D8`,
        'Rolling: 1D6 2D8 1D12',
        'D6 : 2',
        'D8 : **(1)** **(1)**',
        'D12 : 6',
        'Results ordered by best total:',
        'Score: 6 (6) with Effect: D6',
        'Score: 2 (2) with Effect: D12',
      ],    
    }),    
    
    new TestCase({
      name: 'testShowPools_noPools_nothing',
      functionToRun: testShowPools,
      inputArgs: {
        // prepareInput: {},        
      },
      expectedResult: [        
        `No pools.`,
      ],    
    }),    
    new TestCase({
      name: 'testShowPools_constantPools_showBoth',
      functionToRun: testShowPools,
      inputArgs: {
        prepareInput: {},        
      },
      expectedResult: [        
       `*${POOL_NAME}*: 1D6 1D8 2D10`,
       `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),    
    new TestCase({
      name: 'testShowPools_constantPoolsDifferentChannel_none',
      functionToRun: testShowPools,
      inputArgs: {
        prepareInput: {},  
        channel_id: OTHER_GUILD_ID,      
      },
      expectedResult: [        
       `No pools.`,
      ],    
    }),    
        
        
    new TestCase({
      name: 'testMoveDicePool_moveOneDie_success',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: POOL_NAME,
        target: SECOND_POOL_NAME,
        dice: '6',
        prepareInput: {},  
        appendShow: true,
      },
      expectedResult: [ 
      `Removed from pool '${POOL_NAME}': 1D6. New pool: 1D8 2D10.`,       
      `Added to pool '${SECOND_POOL_NAME}': 1D6. New pool: 1D6 1D12.`,
      `*${POOL_NAME}*: 1D8 2D10`,
      `*${SECOND_POOL_NAME}*: 1D6 1D12`,
      ],    
    }),    
    new TestCase({
      name: 'testMoveDicePool_moveTwoDice_success',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: POOL_NAME,
        target: SECOND_POOL_NAME,
        dice: '6 1d10',
        prepareInput: {},  
        appendShow: true,
      },
      expectedResult: [ 
      `Removed from pool '${POOL_NAME}': 1D6 1D10. New pool: 1D8 1D10.`,       
      `Added to pool '${SECOND_POOL_NAME}': 1D6 1D10. New pool: 1D6 1D10 1D12.`,
      `*${POOL_NAME}*: 1D8 1D10`,
      `*${SECOND_POOL_NAME}*: 1D6 1D10 1D12`,
      ],    
    }),    
    new TestCase({
      name: 'testMoveDicePool_leavePoolEmpty_success',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: SECOND_POOL_NAME,
        target: POOL_NAME,
        dice: '12',
        prepareInput: {},  
        appendShow: true,
      },
      expectedResult: [ 
      `Removed from pool '${SECOND_POOL_NAME}': 1D12. New pool: .`,       
      `Added to pool '${POOL_NAME}': 1D12. New pool: 1D6 1D8 2D10 1D12.`,
      `*${POOL_NAME}*: 1D6 1D8 2D10 1D12`,
      `*${SECOND_POOL_NAME}*: `,
      ],    
    }),    
    new TestCase({
      name: 'testMoveDicePool_moveNonExistentPool_success',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: SECOND_POOL_NAME,
        target: NON_POOL_NAME,
        dice: '12',
        prepareInput: {},  
        appendShow: true,
      },
      expectedResult: [ 
      `Removed from pool '${SECOND_POOL_NAME}': 1D12. New pool: .`,       
      `Created new pool '${NON_POOL_NAME}': 1D12.`,
      `*${POOL_NAME}*: 1D6 1D8 2D10`,
      `*${SECOND_POOL_NAME}*: `,
      `*${NON_POOL_NAME}*: 1D12`,
      ],    
    }),    
    new TestCase({
      name: 'testMoveDicePool_noDice_error',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: SECOND_POOL_NAME,
        target: POOL_NAME,
        dice: '0d8',
        prepareInput: {},  
        appendShow: true,
      },
      expectedResult: [ 
      `No dice to modify: '0d8'`,       
      `*${POOL_NAME}*: 1D6 1D8 2D10`,
      `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),    
    new TestCase({
      name: 'testMoveDicePool_invalidSource_error',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: 'fds.dfd',
        target: POOL_NAME,
        dice: '1d8',
        prepareInput: {},  
        appendShow: true,
      },
      expectedResult: [ 
      `Invalid name 'fds.dfd': must not contain ',', '\\*' or '.'.`,       
      `*${POOL_NAME}*: 1D6 1D8 2D10`,
      `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),    
    new TestCase({
      name: 'testMoveDicePool_invalidTarget_error',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: POOL_NAME,
        target: 'fds.dfd',
        dice: '1d8',
        prepareInput: {},  
        appendShow: true,
      },
      expectedResult: [ 
      `Invalid name 'fds.dfd': must not contain ',', '\\*' or '.'.`,       
      `*${POOL_NAME}*: 1D6 1D8 2D10`,
      `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),  
    new TestCase({
      name: 'testMoveDicePool_missingDice_error',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: POOL_NAME,
        target: SECOND_POOL_NAME,
        dice: '12',
        prepareInput: {},  
        appendShow: true,
      },
      expectedResult: [ 
      `Dice '1D12' are missing from pool '${POOL_NAME}'.`,       
      `*${POOL_NAME}*: 1D6 1D8 2D10`,
      `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),  
    new TestCase({
      name: 'testMoveDicePool_errorStoring_error',
      functionToRun: testMoveDicePool,
      inputArgs: {
        source: POOL_NAME,
        target: SECOND_POOL_NAME,
        dice: '6 1d10',
        prepareInput: {},  
        appendShow: true,
        options: {
          failMap: {            
            'cacheValue': new Set([3]), // the 4th cache, after constant values and after one remove
          },
        }
      },
      expectedResult: [ 
      `Removed from pool '${POOL_NAME}': 1D6 1D10. New pool: 1D8 1D10.`,       
      `Failed adding '1D6 1D10' to pool '${SECOND_POOL_NAME}': Manually try again or add the dice back to '${POOL_NAME}'. Unexpected error.`,
      `*${POOL_NAME}*: 1D8 1D10`,
      `*${SECOND_POOL_NAME}*: 1D12`,
      ],    
    }),  
        
  ]
}

/*
  input: {
      names - comma separate list, (for pool_name as well)
      dice - string
      action - POOL_...,
      optional channel_id,  optional user_id,
      prepareInput, 
      appendShow - bool,
    }    
  
  output: list of messages
*/
async function testModifyDicePool(input) {
  let iocContainer = createIocContainer([])

  if (input.prepareInput) {
    await prepareConstantPools(input.prepareInput, iocContainer)
  }

  let inputOptions = {
    name: input.names,
    names: input.names,
    dice: input.dice,
    action: input.action,
    channelId: input.channel_id ?? GUILD_ID ,
    userId: input.user_id ?? USER_ID,
  }
  
  let result = await controller.ModifyDicePool(inputOptions, iocContainer)
  
  if (input.appendShow ) {
    let showOptions = {
      channelId: input.channel_id ?? GUILD_ID ,
      userId: input.user_id ?? USER_ID,
    }
    
    let showResult = await controller.ShowDicePools(showOptions, iocContainer)
    result.push(...showResult)
  }
  
  return result
}


/*
  input: {
      name
      die - int,
      steps - int,
      mod - STEP_MOD_UP / STEP_MOD_DOWN
      optional channel_id,  optional user_id,
      prepareInput, 
    }    
  
  output: list of messages
*/
async function testStepPool(input) {
  let iocContainer = createIocContainer([])

  if (input.prepareInput) {
    await prepareConstantPools(input.prepareInput, iocContainer)
  }

  let inputOptions = {
    name: input.name,
    die: input.die,
    steps: input.steps,
    mod: input.mod,
    channelId: input.channel_id ?? GUILD_ID ,
    userId: input.user_id ?? USER_ID,
  }

  let result= await controller.StepDicePool(inputOptions, iocContainer)
  return result
}


/*
  input: {
      pool_name - comma separated list,
      dice - optional string,
      keep - optional int,
      save_opt_out- optional string,
      save_opt_in - optionalstring,
      extra_effects - optionalint,
      optional channel_id,  optional user_id, optional cache_key
      prepareInput, 
      actualRollResults - list of ints,
      saverInputPreparationActions - none or []
    }    
  
  output: list of messages
*/
async function testRollPool(input) {
  let iocContainer = createIocContainer(input.actualRollResults)

  if (input.prepareInput) {
    await prepareConstantPools(input.prepareInput, iocContainer)
  }
  
  if(input.saverInputPreparationActions) {
    let prepareInput = {
      actions: input.saverInputPreparationActions
    }    
    await prepareConstantSaveDices(prepareInput, iocContainer)    
  }

  let inputOptions = {
    names: input.pool_name,
    dice: input.dice ?? '',
    keep: input.keep ?? 2,
    saveOptOut: input.save_opt_out ?? '',
    saveOptIn: input.save_opt_in ?? '',
    extraEffects: input.extra_effects ?? 0,
    channelId: input.channel_id ?? GUILD_ID ,
    userId: input.user_id ?? USER_ID,
    cacheKey: input.cache_key?? CONSTANT_ROLL_CACHE_KEY,
  }

  let result= await controller.RollDicePool(inputOptions, iocContainer)
  return result
}

/*
  input: {
      source,
      target,
      dice - string
      optional channel_id,  optional user_id,
      prepareInput, 
      appendShow - bool,
      options - mock ioc container options
    }    
  
  output: list of messages
*/
async function testMoveDicePool(input) {
  let iocContainer = createIocContainer([], input.options)

  if (input.prepareInput) {
    await prepareConstantPools(input.prepareInput, iocContainer)
  }

  let inputOptions = {
    sourceName: input.source,
    targetName: input.target,
    dice: input.dice,
    channelId: input.channel_id ?? GUILD_ID ,
    userId: input.user_id ?? USER_ID,
  }
  
  let result = await controller.MoveDicePool(inputOptions, iocContainer)
  
  if (input.appendShow ) {
    let showOptions = {
      channelId: input.channel_id ?? GUILD_ID ,
      userId: input.user_id ?? USER_ID,
    }
    
    let showResult = await controller.ShowDicePools(showOptions, iocContainer)
    result.push(...showResult)
  }
  
  return result
}


/*
  input: {
      optional channel_id,  optional user_id, optional cache_key
      prepareInput, 
    }    
  
  output: list of messages
*/
async function testShowPools(input) {
  let iocContainer = createIocContainer([])

  if (input.prepareInput) {
    await prepareConstantPools(input.prepareInput, iocContainer)
  }

  let inputOptions = {
    channelId: input.channel_id ?? GUILD_ID ,
    userId: input.user_id ?? USER_ID,
  }

  let result= await controller.ShowDicePools(inputOptions, iocContainer)
  return result
}