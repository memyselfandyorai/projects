﻿using System;

namespace FolderSyncronizer.Core
{
    public interface IFileWatcherCollection
    {
        event EventHandler<FileActionChangedEventArgs> NewFileAction;

        void Dispose();
    }
}