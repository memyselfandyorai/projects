const EXPENSE_DATE_COLUMN = 1;
const EXPENSE_ACCOUNT_COLUMN = 2;
const EXPENSE_CATEGORY_COLUMN = 3;
const EXPENSE_PAYEE_COLUMN = 4;
const EXPENSE_AMOUNT_COLUMN = 5;
// const EXPENSE_NOTE_COLUMN = 6;
const EXPENSE_START_ROW = 2;

const SUMMARY_SHEET = 'Summary'

const INVESTMENT_NAME = 'Investment'


// First categories (default).
const CATEGORY_ORDER = [
  'Groceries',
  'Dining',
  'Auto: Gas',
  'Auto: Parking',
  'Auto: Service',
  'Auto: Registration',
  'Household',
  'Insurance',
  'Rent',
  'Utilities: Gas& Electric',
  'Utilities: Internet',
  'Utilities: Water',
  'Pets',
  'Health & Fitness',
  'Entertainment',
  'Gifts',
  'Clothing',
  'Charity',
  'Travel: Vacation',
  'Utilities: Telephone',
]

function getInvestmentCol(options) {
  return getCategoryOrder(options).indexOf(INVESTMENT_NAME) + SUMMARY_CATEGORY_START_COL
}

// Last categories, first is last (default).
const CATEGORY_ORDER_DESC = [
  'Salary',
  'Bonus',
  'Auto',
  'Utilities',
  'Others',
  'חבר',
]


const SUMMARY_LINK_COL = 1;
const SUMMARY_YEAR_COL = 2;
const SUMMARY_MONTH_COL = 3;
const SUMMARY_BALANCE_COL = 4;
const SUMMARY_SALARY_COL = 5;
const SUMMARY_EXPENSE_COL = 6;
const SUMMARY_CATEGORY_START_COL = 7;

const SUMMARY_FIRST_AMOUNT_COL = SUMMARY_BALANCE_COL;

const SUMMARY_COLUMN_ROW = 1;
const SUMMARY_START_ROW = SUMMARY_COLUMN_ROW + 1;

const MONTH_FORMATTER = new Intl.NumberFormat('en-US', { 
    minimumIntegerDigits: 2, 
});

const GOOD_COLOR = 'green'
const EXPENSE_COLOR = 'orange'
const LARGE_EXPENSE_COLOR = 'red'
const GREAT_EXPENSE_COLOR = 'darkred'
const HUGE_EXPENSE_COLOR = 'purple'

const DARK_BACKGROUND_FOREGROUND_COLOR = 'red'

const VERY_GOOD_COLOR = 'darkgreen'
const LESS_GOOD_COLOR = 'lightgreen'

const SUMMARY_OVERALL_CATEGORIES = [SUMMARY_BALANCE_COL, SUMMARY_SALARY_COL, SUMMARY_EXPENSE_COL]
const SUMMARY_OVERALL_FIRST_COL = Math.min(...SUMMARY_OVERALL_CATEGORIES)

const TRANSPOSED_SUMMARY_ROW = 1
const TRANSPOSED_VALUES_COL = 2
const TRANSPOSED_CATEGORIES_COL = 1
const TRANSPOSED_INIITAL_CATEGORIES = [SUMMARY_YEAR_COL, SUMMARY_MONTH_COL, ...SUMMARY_OVERALL_CATEGORIES]
const TRANSPOSED_INIITAL_BOLD_VALUES = [SUMMARY_YEAR_COL, SUMMARY_MONTH_COL]
const TRANSPOSED_INIITAL_AMOUNT_VALUES = SUMMARY_OVERALL_CATEGORIES
const TRANSPOSED_INITIAL_CATEGORIES_FIRST_COL = Math.min(...TRANSPOSED_INIITAL_CATEGORIES)
const TRANSPOSED_INITIAL_CATEGORIES_TOTAL_COLS = TRANSPOSED_INIITAL_CATEGORIES.length
const TRANSPOSED_START_ROW = TRANSPOSED_INIITAL_CATEGORIES.length + 1

const ACCOUNT_SEPARATOR = ';'

const CHART_SHEET = 'Chart'
const CHART_START_COL = 1
const CHART_CATEGORY_ROW = 1
const CHART_SUM_ROW = 2

const TRANSPOSED_ALLOWED_SHEET_SOURCE_NAME_SUBSTRINGS = [SUMMARY_SHEET, CHART_SHEET]


/**
* @OnlyCurrentDoc
*/

/**
 * Adds a custom menu item to run the script.
 */
function onOpenSummary() {
  let ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.addMenu("Expense Tools", [
      { name: "Create Shortcuts Only", functionName: 'createShortcuts' },
      { name: "Create Summary", functionName: 'createSummary' },
      { name: "Create Summary for account(s)", functionName: 'createSummaryForAccounts' },
      { name: "Copy Transpose (for chosen rows and columns)", functionName: 'copyTransposed' },
      { name: "Create pie chart (for chosen dates)", functionName: 'createPieChart' },
      { name: "Reload defaults", functionName: 'reloadDefaults' },
  ]);

  reloadDefaults()
}


function createSummaryForAccounts() {
  let ui = SpreadsheetApp.getUi();
  let valueDefaults = getDefaults()
  
  let response = ui.prompt(`Which accounts? (separated by '${ACCOUNT_SEPARATOR}')\nSuggested Default: ${valueDefaults.allAcountNames.join(ACCOUNT_SEPARATOR)}`, ui.ButtonSet.OK_CANCEL)
  if (response.getSelectedButton() != ui.Button.OK) {
    return
  }
  let rawAccounts = response.getResponseText()
  let accounts = rawAccounts.split(ACCOUNT_SEPARATOR).map(a => a.trim())
  let options = {
    accounts: accounts,
    sheetName: `${SUMMARY_SHEET}(${rawAccounts})`,
  }
  createSummary(options)
}


function createShortcuts() {
  let options = {
    onlyShortcuts: true,
  }
  createSummary(options)
}

/**
 * Summary ignores the "account", unless "options" provide specific accounts.
 * 
 * options: {
 *  accounts - list of strings,
 *  sheetName - alternative name of sheet,
 *  onlyShortcuts - generates only the shortcuts, without the actual summary, 
 * }
 */
// Note: . Optional future feature: add "per account" if needed.
function createSummary(options = undefined) {
  let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  let sheetName = options?.sheetName ?? SUMMARY_SHEET
  let outputSheet = spreadsheet.getSheetByName(sheetName)
  
  if (outputSheet == null) {
      outputSheet = spreadsheet.insertSheet()
      outputSheet.setName(sheetName)
  } else {   
    // Not sure I want this. It's OK to recreate from scratch.
    /*
    let ui = SpreadsheetApp.getUi();
    let response = ui.alert('Are you sure you want to override values in existing sheet?', ui.ButtonSet.YES_NO);

    // Process the user's response.
    if (response != ui.Button.YES) {
        return;
    }
    */
    outputSheet.getRange(1, 1, outputSheet.getMaxRows(), outputSheet.getMaxColumns()).clear()
  }

  let valueDefaults = getDefaults()
  options = options ?? {}
  options.firstCategoriesAsc = valueDefaults.firstCategoriesAsc
  options.lastCategoriesDesc = valueDefaults.lastCategoriesDesc

  let sheetsData = valueDefaults.expenseSheetNameList.map(name => getSheetData(spreadsheet, name))
  // Descending order.
  sheetsData.sort((s1, s2) => s2.dateRange.to - s1.dateRange.to)
   
  createSummaryInSheet(outputSheet, sheetsData, options)
}


/** Returns:
 *           {
    allDates: list of dates,
    sheetName: string,
    sheetId: string,
    dateRange: {
      from: date,
      to: date,
    }
    categories: Set of strings,
  }
 * 
 */
function getSheetData(spreadsheet, sheetName) {
  let sheet = spreadsheet.getSheetByName(sheetName)
  if(!sheet) {
    throw new Error(`No sheet named '${sheetName}'`)
  }
  let textDates = sheet.getRange(EXPENSE_START_ROW, EXPENSE_DATE_COLUMN, sheet.getLastRow() - 1, 1).getValues().flatMap(a => a).filter(t => !!t && t != '')
  let allDates = textDates.map(text => new Date(text)).filter(date => Object.prototype.toString.call(date) === '[object Date]')

  let allCategories = sheet.getRange(EXPENSE_START_ROW, EXPENSE_CATEGORY_COLUMN, sheet.getLastRow() - 1, 1).getValues().flatMap(a => a).map(v => v.trim()).filter(t => !!t && t != '')
  let distinctCategories = new Set(allCategories)

  return {
    // allDates: allDates,
    sheetName: sheetName,
    sheetId: sheet.getSheetId(),
    dateRange: {
      from: getMinDate(allDates),
      to: getMaxDate(allDates),
    }, 
    categories: distinctCategories,
  }
}


function createSummaryInSheet(summarySheet, sheetsData, options) {
  let firstDate = getMinDate(sheetsData.map(sheet => sheet.dateRange.from))
  // Take the first day of the first month
  firstDate = new Date(Date.UTC(firstDate.getFullYear(), firstDate.getMonth(), 1))
  let lastDate = getMaxDate(sheetsData.map(sheet => sheet.dateRange.to))

  let descendingDate = new Date(lastDate)
  let currentRow = SUMMARY_START_ROW
  while(descendingDate >= firstDate) {
    let month = descendingDate.getMonth()
    // Adding "+1" since it's a month "index".
    createSummaryRow(summarySheet, sheetsData, currentRow, descendingDate.getFullYear(), month)
    currentRow++
    descendingDate.setMonth(month - 1)
  }

  if (!options?.onlyShortcuts) {
    let totalColumns = createColumns(summarySheet, sheetsData, options)

    createSums(summarySheet, sheetsData, totalColumns, currentRow, options)
    formatCells(summarySheet, currentRow, totalColumns)
  }
}

function formatCells(summarySheet, totalRows, totalColumns) {
  summarySheet.getRange(1, SUMMARY_MONTH_COL, totalRows)
    .setNumberFormat('0#')
    .setBorder(null, null, null, true, null, null, null, SpreadsheetApp.BorderStyle.SOLID_THICK)

  // Maybe I should limit categories per each range? But all categories should be columns...
  summarySheet.getRange(1, 1, 1, totalColumns).setFontWeight('bold')
  summarySheet.setFrozenColumns(SUMMARY_OVERALL_FIRST_COL + SUMMARY_OVERALL_CATEGORIES.length - 1)
  summarySheet.setFrozenRows(SUMMARY_START_ROW - 1)
  let numberCells = summarySheet.getRange(SUMMARY_START_ROW , SUMMARY_FIRST_AMOUNT_COL, totalRows, totalColumns)
  numberCells.setNumberFormat(`"₪"#,##0.00`)

  let sumCells = [summarySheet.getRange(SUMMARY_START_ROW , SUMMARY_CATEGORY_START_COL, totalRows, totalColumns - 2)]
  let balanceCells = [summarySheet.getRange(SUMMARY_START_ROW , SUMMARY_BALANCE_COL, totalRows, 1)]
  let formattingRules = summarySheet.getConditionalFormatRules();
  formattingRules.push(
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberLessThan(0)
      .setBackground(GOOD_COLOR)
      .setRanges(sumCells)
      .build(),
      SpreadsheetApp.newConditionalFormatRule()
      .whenNumberEqualTo(0)
      .setRanges(sumCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberLessThanOrEqualTo(500)
      .setBackground(EXPENSE_COLOR)
      .setRanges(sumCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberLessThanOrEqualTo(2000)
      .setBackground(LARGE_EXPENSE_COLOR)
      .setRanges(sumCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberLessThanOrEqualTo(10000)
      .setBackground(GREAT_EXPENSE_COLOR)
      .setFontColor(DARK_BACKGROUND_FOREGROUND_COLOR)
      .setRanges(sumCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberGreaterThan(10000)
      .setBackground(HUGE_EXPENSE_COLOR)
      .setFontColor(DARK_BACKGROUND_FOREGROUND_COLOR)
      .setRanges(sumCells)
      .build()
    )

  formattingRules.push(
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberGreaterThan(13000)
      .setBackground(VERY_GOOD_COLOR)
      .setRanges(balanceCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberGreaterThan(8000)
      .setBackground(GOOD_COLOR)
      .setRanges(balanceCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberGreaterThan(0)
      .setBackground(LESS_GOOD_COLOR)
      .setRanges(balanceCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberEqualTo(0)
      .setRanges(balanceCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberGreaterThanOrEqualTo(-500)
      .setBackground(EXPENSE_COLOR)
      .setRanges(balanceCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberGreaterThanOrEqualTo(-2000)
      .setBackground(LARGE_EXPENSE_COLOR)
      .setRanges(balanceCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberGreaterThanOrEqualTo(-10000)
      .setBackground(GREAT_EXPENSE_COLOR)
      .setFontColor(DARK_BACKGROUND_FOREGROUND_COLOR)
      .setRanges(balanceCells)
      .build(),
    SpreadsheetApp.newConditionalFormatRule()
      .whenNumberLessThan(-10000)
      .setBackground(HUGE_EXPENSE_COLOR)
      .setFontColor(DARK_BACKGROUND_FOREGROUND_COLOR)
      .setRanges(balanceCells)
      .build()
    )

    
  summarySheet.setConditionalFormatRules(formattingRules)          

  summarySheet.autoResizeColumns(1, totalColumns)
  
  // Note: there's a bug and the resize doesn't work...
  /*
  Logger.log(`Resize: ${totalColumns}, ${columnToLetter(totalColumns)}`)
  summarySheet.autoResizeColumn(43)
  Logger.log(`Resize: ${columnToLetter(43)}`)
  */
}

function createSummaryRow(summarySheet, sheetsData, currentRow, year, monthIndex) {
  let monthNumber = monthIndex + 1

  summarySheet.getRange(currentRow, SUMMARY_YEAR_COL).setValue(year)
  // Format should be: "0#""
  summarySheet.getRange(currentRow, SUMMARY_MONTH_COL).setValue(MONTH_FORMATTER.format(monthNumber))
  let sheetData = findSheetByDate(sheetsData, new Date(Date.UTC(year, monthIndex, 1)))

  let baseUrl = `${SpreadsheetApp.getActiveSpreadsheet().getUrl()}#gid=${sheetData.sheetId}`
  let yearColumn = convertIndexToColumnLetter(SUMMARY_YEAR_COL)
  let monthColumn = convertIndexToColumnLetter(SUMMARY_MONTH_COL)
  let dateFormula = `DATE(${yearColumn}${currentRow},${monthColumn}${currentRow},1)`
  let cellTextFormula = `CONCAT("Jump to ",text(${dateFormula},"MM/yyyy"))`

  let dateColumn = convertIndexToColumnLetter(EXPENSE_DATE_COLUMN)
  let matchDateRange = `'${sheetData.sheetName}'!${dateColumn}$${EXPENSE_START_ROW}:${dateColumn}` // SHEET!A2$A
  let findFirstDateFormula = `MATCH(${dateFormula},${matchDateRange},1)+1`
  let findIndexFormula = `IFNA(${findFirstDateFormula},${EXPENSE_START_ROW})`

  let linkFormula = `=HYPERLINK(CONCAT("${baseUrl}&range=${dateColumn}", ${findIndexFormula}), ${cellTextFormula})`
  summarySheet.getRange(currentRow, SUMMARY_LINK_COL).setFormula(linkFormula)
}

// Returns number of columns.
function createColumns(summarySheet, sheetsData, options) {
  summarySheet.getRange(SUMMARY_COLUMN_ROW, SUMMARY_LINK_COL).setValue("Shortcuts")
  summarySheet.getRange(SUMMARY_COLUMN_ROW, SUMMARY_YEAR_COL).setValue("Year")
  summarySheet.getRange(SUMMARY_COLUMN_ROW, SUMMARY_MONTH_COL).setValue("Month")
  summarySheet.getRange(SUMMARY_COLUMN_ROW, SUMMARY_BALANCE_COL).setValue("Balance")
  summarySheet.getRange(SUMMARY_COLUMN_ROW, SUMMARY_SALARY_COL).setValue("Income")
  summarySheet.getRange(SUMMARY_COLUMN_ROW, SUMMARY_EXPENSE_COL).setValue("Total Expenses")

  let categoryColumn = SUMMARY_CATEGORY_START_COL
  let allCategories = new Set(sheetsData.flatMap(sheet => Array.from(sheet.categories)))
  let sortedCategories = Array.from(allCategories)
  sortedCategories.sort((c1, c2) => compareCategories(c1, c2, options))
  for(let category of sortedCategories) {
    summarySheet.getRange(SUMMARY_COLUMN_ROW, categoryColumn).setValue(category)
    categoryColumn++
  }

  return categoryColumn
}


/*
1. Copy to start of next month (notice column J is titles) - note all should be adjacent, no extra lines. If I add new categories, need to update indexes in salary/expense formulas				
2. Change Month/Year				
3. Copy generated forumla to Groceries:			=-SUMIFS(E:E, C:C, J979, A:A,">="&(date($K$977, $K$978, 1)),A:A,"<="&(date($K$977, $K$978+1, 1)-1))	
4. Drag till last category				
5. Copy generated formula to both salary and expense (previously: suffix from first "A:A...." till the end of formula)				
				
Salary/Income:			=+SUMIFS(E:E, E:E,">0", A:A,">="&(date($K$977, $K$978, 1)),A:A,"<="&(date($K$977, $K$978+1, 1)-1))	
Expenses:			=-SUMIFS(E:E, E:E,"<0", A:A,">="&(date($K$977, $K$978, 1)),A:A,"<="&(date($K$977, $K$978+1, 1)-1))	
 */
function createSums(summarySheet, sheetsData, columnsNumber, rowsNumber, options) {
  let firstRow = SUMMARY_START_ROW
  let firstColumn = SUMMARY_CATEGORY_START_COL
  let firstCell = summarySheet.getRange(firstRow, firstColumn)
 
  let orCriterionCartesianProduct = cartesian(sheetsData, [options?.accounts].flat())
  let perSheetCategoryFormulas = orCriterionCartesianProduct.map(orCriterionList => {
    let sheet = orCriterionList[0]
    let account = orCriterionList[1] // can be null
    return createSumByCategoryFormula(sheet, firstRow, firstColumn, account)
  })
  let sumFormula = `=-SUM(${perSheetCategoryFormulas.join(',')})`
  firstCell.setFormula(sumFormula)

  let perSheetSalaryExpenseFormulas = [">0", "<0"].map(amountFilter => 
    orCriterionCartesianProduct.map(orCriterionList => {
      let sheet = orCriterionList[0]
      let account = orCriterionList[1] // can be null
      return createSumByAccountCriterionFormula(sheet, firstRow, amountFilter, account)
    })
  )

  let firstIncomeCell = summarySheet.getRange(firstRow, SUMMARY_SALARY_COL)
  firstIncomeCell.setFormula(`=SUM(${perSheetSalaryExpenseFormulas[0].join(',')})`)
  let firstExpenseCell = summarySheet.getRange(firstRow, SUMMARY_EXPENSE_COL)
  firstExpenseCell.setFormula(`=-SUM(${perSheetSalaryExpenseFormulas[1].join(',')})`)
  let firstBalanceCell = summarySheet.getRange(firstRow, SUMMARY_BALANCE_COL)
  let firstInvestmentCell = summarySheet.getRange(firstRow, getInvestmentCol(options))
  firstBalanceCell.setFormula(`=${firstIncomeCell.getA1Notation()}-${firstExpenseCell.getA1Notation()}+${firstInvestmentCell.getA1Notation()}`)

  // Note: can't combine both, as one of them is a single cell for a whole table.
  let otherSummaryCells = summarySheet.getRange(SUMMARY_START_ROW, SUMMARY_CATEGORY_START_COL, rowsNumber - SUMMARY_START_ROW, columnsNumber - SUMMARY_CATEGORY_START_COL)
  firstCell.copyTo(otherSummaryCells)

  let otherIncomeExpenseSummaryCells = summarySheet.getRange(SUMMARY_START_ROW, SUMMARY_OVERALL_FIRST_COL, rowsNumber - SUMMARY_START_ROW, SUMMARY_OVERALL_CATEGORIES.length)
  summarySheet.getRange(firstRow, SUMMARY_OVERALL_FIRST_COL, 1, SUMMARY_OVERALL_CATEGORIES.length).copyTo(otherIncomeExpenseSummaryCells)
}

// Returns a SUMIFS formula string.
function createSumByAccountCriterionFormula(sheet, row, amountCriterion, optionalAccount) {
  let sumAmountRange = getFormulaRange(sheet, `$${convertIndexToColumnLetter(EXPENSE_AMOUNT_COLUMN)}`)
  
  let criteriaFormulaPairList = [createCriteria(sumAmountRange,`"${amountCriterion}"`)]
  criteriaFormulaPairList.push(...createDateRangeCriterias(sheet, row))
  criteriaFormulaPairList.push(...createAccountCriterias(sheet, optionalAccount))

  return createSumIfsFormula(sumAmountRange, criteriaFormulaPairList)
}

// Returns a SUMIFS formula string.
function createSumByCategoryFormula(sheet, row, column, optionalAccount) {
  let sumAmountRange = getFormulaRange(sheet, `$${convertIndexToColumnLetter(EXPENSE_AMOUNT_COLUMN)}`)
  
  let matchCategoryRange = getFormulaRange(sheet, `$${convertIndexToColumnLetter(EXPENSE_CATEGORY_COLUMN)}`)
  let categoryCellA1Notation = `${convertIndexToColumnLetter(column)}$${SUMMARY_COLUMN_ROW}`
  let criteriaFormulaPairList = [createCriteria(matchCategoryRange, categoryCellA1Notation)]
  criteriaFormulaPairList.push(...createDateRangeCriterias(sheet, row))
  criteriaFormulaPairList.push(...createAccountCriterias(sheet, optionalAccount))

  return createSumIfsFormula(sumAmountRange, criteriaFormulaPairList)
}

// Returns a list like criteriaFormulaPairList from createSumIfsFormula.
function createDateRangeCriterias(sheet, row) {
  let dateFormula = getDateFormulaForRow(row)
  let nextMonthDateFormula = getDateFormulaForRow(row, '+1')
  let matchDateFromFormula = `">="&${dateFormula}`
  let matchDateToFormula = `"<"&${nextMonthDateFormula}`

  let matchDateRange = getFormulaRange(sheet, `$${convertIndexToColumnLetter(EXPENSE_DATE_COLUMN)}`)

  return [
    createCriteria(matchDateRange, matchDateFromFormula), 
    createCriteria(matchDateRange, matchDateToFormula), 
  ]
}

function createAccountCriterias(sheet, optionalAccount) {
  if (!optionalAccount) {
    return []
  }

  let matchAccountRange = getFormulaRange(sheet, `$${convertIndexToColumnLetter(EXPENSE_ACCOUNT_COLUMN)}`)
  
  return [createCriteria(matchAccountRange, `"${optionalAccount}"`)]
}



function copyTransposed() {
  let doc = SpreadsheetApp.getActiveSpreadsheet()
  let sheet = doc.getActiveSheet()
  
  let sheetName = sheet.getName()
  if(TRANSPOSED_ALLOWED_SHEET_SOURCE_NAME_SUBSTRINGS.every(substr => !sheetName.includes(substr))) {
    return
  }
  
  let chosenRange = sheet.getActiveRange()
  if(chosenRange.getRow() < SUMMARY_START_ROW || chosenRange.getColumn() < SUMMARY_FIRST_AMOUNT_COL) {
    return
  }

  let outputSheet = doc.insertSheet()

  copyCategories(
    sheet.getRange(chosenRange.getRow(),  Math.min(...TRANSPOSED_INIITAL_BOLD_VALUES), chosenRange.getNumRows(), TRANSPOSED_INIITAL_BOLD_VALUES.length),
    outputSummaryValueRange = outputSheet.getRange(TRANSPOSED_SUMMARY_ROW, TRANSPOSED_VALUES_COL))
  copyAmountValues(
    sheet.getRange(chosenRange.getRow(),  Math.min(...TRANSPOSED_INIITAL_AMOUNT_VALUES), chosenRange.getNumRows(), TRANSPOSED_INIITAL_AMOUNT_VALUES.length),
    outputSummaryValueRange = outputSheet.getRange(TRANSPOSED_SUMMARY_ROW + TRANSPOSED_INIITAL_BOLD_VALUES.length, TRANSPOSED_VALUES_COL))
  let outputChosenRange = outputSheet.getRange(TRANSPOSED_START_ROW, TRANSPOSED_VALUES_COL)
  copyAmountValues(chosenRange, outputChosenRange)
  
  let summaryCategoryRange = sheet.getRange(SUMMARY_COLUMN_ROW, TRANSPOSED_INITIAL_CATEGORIES_FIRST_COL, 1, TRANSPOSED_INITIAL_CATEGORIES_TOTAL_COLS)
  let outputCategoryRange = outputSheet.getRange(TRANSPOSED_SUMMARY_ROW, TRANSPOSED_CATEGORIES_COL)
  copyCategories(summaryCategoryRange, outputCategoryRange)
  let columnRange = sheet.getRange(SUMMARY_COLUMN_ROW, chosenRange.getColumn(), 1, chosenRange.getNumColumns())
  let outputColumns = outputSheet.getRange(TRANSPOSED_START_ROW,TRANSPOSED_CATEGORIES_COL)
  copyCategories(columnRange, outputColumns)  

  let lastColumn = TRANSPOSED_VALUES_COL + chosenRange.getNumRows() - 1
  let lastRow = TRANSPOSED_START_ROW + chosenRange.getNumColumns() - 1
  let sortedByTimeStartColumn =  lastColumn + 2
  let sortedByTimeStartRow = TRANSPOSED_SUMMARY_ROW
  let valueRangeStart = `${convertIndexToColumnLetter(TRANSPOSED_VALUES_COL)}${TRANSPOSED_SUMMARY_ROW}`
  let valueRangeEnd = `${convertIndexToColumnLetter(lastColumn)}${lastRow}`
  let transposeFormula = `TRANSPOSE(${valueRangeStart}:${valueRangeEnd})`
  let sortFormula = `SORT(${transposeFormula}, ${TRANSPOSED_INIITAL_BOLD_VALUES[0]}, true, ${TRANSPOSED_INIITAL_BOLD_VALUES[1]}, true)`
  outputSheet.getRange(sortedByTimeStartRow, sortedByTimeStartColumn).setFormula(`=TRANSPOSE(${sortFormula})`)
}


function createPieChart() {
  let spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  let summarySheet = spreadsheet.getActiveSheet()

  if(!summarySheet.getName().includes(SUMMARY_SHEET)) {
    return
  }

  let inputRange = summarySheet.getActiveRange()
  if(inputRange.getRow() < SUMMARY_START_ROW) {
    return
  }

  let inputRangeStartRow = inputRange.getRow()
  let inputRangeLastRow = inputRange.getRow()  + inputRange.getNumRows() - 1

  let fromInclusive = getDateByRow(summarySheet, inputRangeLastRow);
  let toInclusive = getDateByRow(summarySheet, inputRangeStartRow)

  const formatMonth = date => Utilities.formatDate(date, Session.getScriptTimeZone() , "MM/yyyy")
  
  let sheetName = `${CHART_SHEET}(${formatMonth(fromInclusive)}-${formatMonth(toInclusive)})` // TODO: "(startDate-toDate)"
  let outputSheet = spreadsheet.getSheetByName(sheetName)
  if (outputSheet == null) {
      outputSheet = spreadsheet.insertSheet()
      outputSheet.setName(sheetName)
  } else {
    outputSheet.clear()
    var charts = outputSheet.getCharts();
    for (var i in charts) {
      outputSheet.removeChart(charts[i]);
    }
  }
  
  let inputRangeStartColumn = inputRange.getColumn()

  // Decide on columns: all or just what was chosen
  let categoryColumnRange = undefined
  if (inputRangeStartColumn < SUMMARY_CATEGORY_START_COL) {
    // All columns:
    let lastColumn = summarySheet.getRange(1,1, 1, summarySheet.getLastColumn()).getLastColumn()
    categoryColumnRange= summarySheet.getRange(SUMMARY_COLUMN_ROW, SUMMARY_CATEGORY_START_COL, 1, lastColumn - SUMMARY_CATEGORY_START_COL + 1)
  } else {
    // Chosen columns:
    categoryColumnRange = summarySheet.getRange(SUMMARY_COLUMN_ROW, inputRangeStartColumn, 1, inputRange.getNumColumns())
  }
  
  categoryColumnRange.copyTo(outputSheet.getRange(CHART_CATEGORY_ROW, CHART_START_COL))

  let sheetData = {
    sheetName: summarySheet.getSheetName(),
  }
  
  let sumFromCellA1Notation = `${convertIndexToColumnLetter(inputRangeStartColumn)}$${inputRangeStartRow}`
  let sumToCellA1Notation = `${convertIndexToColumnLetter(inputRangeStartColumn)}$${inputRangeLastRow}`
  let monthsSumFormula = `=SUM(${getFormulaRange(sheetData, sumFromCellA1Notation, sumToCellA1Notation)})`
  let firstSumValueCell = outputSheet.getRange(CHART_SUM_ROW, CHART_START_COL)
  firstSumValueCell.setFormula(monthsSumFormula)
  firstSumValueCell.copyTo(outputSheet.getRange(CHART_SUM_ROW, CHART_START_COL, 1 , inputRange.getNumColumns()))


  let chartRange = outputSheet.getRange(CHART_CATEGORY_ROW, CHART_START_COL, [CHART_CATEGORY_ROW, CHART_SUM_ROW].length , inputRange.getNumColumns())

  let timeDiffMonths = inputRangeLastRow - inputRangeStartRow

  // https://developers.google.com/codelabs/apps-script-fundamentals-5#2
  // https://developers.google.com/apps-script/reference/charts
  let pie = outputSheet.newChart().asPieChart()
    .setTransposeRowsAndColumns(true)
    .addRange(chartRange)
    .setPosition(6,6,0,0)
    .setOption('title', `Sum by category (for ${timeDiffMonths} months)`)
    .setOption('legend', {position: 'right'})
    .build();
    
    
  outputSheet.insertChart(pie);  
}

// ******************************************************
// *********************** UTILITIES ********************
// ******************************************************

function getDateByRow(summarySheet, row, monthOffset = 0) {
  let month = summarySheet.getRange(row, SUMMARY_MONTH_COL).getValue()
  let year = summarySheet.getRange(row, SUMMARY_YEAR_COL).getValue()
  return new Date(Date.UTC(year, month - 1 + monthOffset, 1))
}

function createCriteria(rangeFormula, criteriaFormula) {
  return {
    range: rangeFormula,
    criteria: criteriaFormula,
  }
}

/** 
 * criteriaFormulaPairList - a list of {
 *  range,
 *  criteria,
 * }
 */
function createSumIfsFormula(valueRangeFormula, criteriaFormulaPairList) {
  let allCriteriasExpression = criteriaFormulaPairList.map(pair => `${pair.range},${pair.criteria}`).join(',')
  if (allCriteriasExpression != '') {
    allCriteriasExpression = ',' + allCriteriasExpression
  }
  return `SUMIFS(${valueRangeFormula}${allCriteriasExpression})`

}

function getDateFormulaForRow(row, monthDelta = undefined) {
  let monthDeltaText = monthDelta ?? ''
  let yearCell = `$${convertIndexToColumnLetter(SUMMARY_YEAR_COL)}${row}`
  let monthCell = `$${convertIndexToColumnLetter(SUMMARY_MONTH_COL)}${row}`
  return `DATE(${yearCell},${monthCell}${monthDeltaText},1)`
}

function getFormulaRange(sheet, fromCell, toCell = undefined) { 
  let to = toCell ?? fromCell
  return`'${sheet.sheetName}'!${fromCell}:${to}`
}

function getMinDate(dates) {
  return new Date(Math.min(...dates)) 
}

function getMaxDate(dates) {
  return new Date(Math.max(...dates)) 
}

/**
 * Returns negative if first < second. etc.
 */
function compareCategories(first, second, options) {
  if (first == second) {
    return 0
  }

  let initialOrder = compareByArray(first, second, getCategoryOrder(options))
  if (initialOrder !== undefined) {
    return initialOrder
  }

  let descOrder = compareByArray(first, second, options?.lastCategoriesDesc ?? CATEGORY_ORDER_DESC)
  if (descOrder !== undefined) {
    // Return reverse.
    return -descOrder
  }

  // If both not in CATEGORY_ORDER or CATEGORY_ORDER_DESC, abc-order:
  return first.localeCompare(second)
}

function getCategoryOrder(options) {
  var categories = options?.firstCategoriesAsc ?? CATEGORY_ORDER
  return [INVESTMENT_NAME].concat(categories)
}

/**
 * Returns undefined if both do not exist in array.
 */
function compareByArray(first, second, array) {
  let firstIndex = array.indexOf(first)
  let secondIndex = array.indexOf(second)

  // If at least one of them is in array:
  if (firstIndex + secondIndex != -2) {
    if (firstIndex == -1 || secondIndex == -1) {
      return firstIndex == -1 ? 1 : -1
    }

    return firstIndex - secondIndex
  }

  return undefined
}


function findSheetByDate(sheetsData, date) {
  // // sheetsData: descending order.
  let sheet = sheetsData.find(sheet => sheet.dateRange.from <= date && date <= sheet.dateRange.to)
  let lastSheet = sheetsData[0]
  let firstSheet = sheetsData[sheetsData.length - 1]

  if (sheet) {
    return sheet
  }

  for(let i = 0; i < sheetsData.length - 1 ; i++) {
    let newerSheet = sheetsData[i]
    let olderSheet = sheetsData[i + 1]
    if (date > olderSheet.dateRange.to && date < newerSheet.dateRange.from) {
      return newerSheet
    }
  }

  return date > lastSheet.dateRange.to ? lastSheet : firstSheet
}

function getAllSheetsByDate(sheetsData, date) {
  return sheetsData.filter(sheet => sheet.dateRange.from <= date && date <= sheet.dateRange.to)
}


function convertIndexToColumnLetter(columnNumber) {
  // return String.fromCharCode(A_CHAR_CODE - 1 + rowNumber)
  return columnToLetter(columnNumber)
}

const A_CHAR_CODE = 'A'.charCodeAt(0)
const LETTERS_IN_ABC = 26

function columnToLetter(column) {
  let columnLetters = '';
  while (column > 0)
  {
    let letterIndex = (column - 1) % LETTERS_IN_ABC;
    let letter = String.fromCharCode(letterIndex + A_CHAR_CODE)
    columnLetters = letter + columnLetters;
    column = (column - letterIndex - 1) / LETTERS_IN_ABC;
  }
  return columnLetters;
}

function copyAmountValues(sourceRange, targetRange) {
  sourceRange.copyTo(targetRange, SpreadsheetApp.CopyPasteType.PASTE_VALUES, true)
  sourceRange.copyTo(targetRange, SpreadsheetApp.CopyPasteType.PASTE_CONDITIONAL_FORMATTING, true)
  sourceRange.copyTo(targetRange, SpreadsheetApp.CopyPasteType.PASTE_FORMAT, true)
}

function copyCategories(sourceRange, targetRange) {
  sourceRange.copyTo(targetRange, SpreadsheetApp.CopyPasteType.PASTE_VALUES, true)
}


function cartesian(...lists) {
  return  lists.reduce((cartesianList, singleList) => cartesianList.flatMap(list => singleList.map(item => [list].flat().concat([item]))))
}

