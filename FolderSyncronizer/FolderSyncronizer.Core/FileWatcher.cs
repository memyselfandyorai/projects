﻿using FolderSyncronizer.Core.Data;
using FolderSyncronizer.Core.Watchers;
using FolderSyncronizer.Debugging;
using System;
using System.IO;
using IFileSystemPubsub = FolderSyncronizer.Core.Data.IPubsub<FolderSyncronizer.Core.Data.FileWatcherArgs>;

namespace FolderSyncronizer.Core
{
    public class FileWatcher : IDisposable, IFileWatcher
    {
        private FileSystemWatcher _fileSystemWatcher;

        private readonly IFileSystemPubsub _modificationsPubsub;
        private string _sourceDirectory;
        private string _targetDirectory;

        private bool _disposedValue;

        private string PublisherId => $"{_sourceDirectory};{_targetDirectory}";

        // TODO(03/11/21): write tests for all the older-file handles (ChangeOlderAction). Delete, Rename, Copy, all coverage! Including "cancelled".
        // TODO(25/10/21): Write tests for folder rename and folder delete. How they affect subfiles/folders.

        // TODO(18/12/21): idea (infeasible?) - run a "verifier" - write log, like the patcher, for missing files? This will be difficult, since some do not exist on purpose... I can do this for all "input source" , **but it won't help if watcher didn't catch it...** , even checking file count might not solve the last part.

        public FileWatcher(IFileSystemPubsub modificationsPubsub, string sourceDirectory, string targetDirectory)
        {
            _modificationsPubsub = modificationsPubsub;
            _sourceDirectory = sourceDirectory;
            _targetDirectory = targetDirectory;

            _fileSystemWatcher = new FileSystemWatcher(sourceDirectory, "*.*")
            {
                NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastWrite,
                IncludeSubdirectories = true,
                EnableRaisingEvents = true,
            };

            _fileSystemWatcher.Created += ChangeOccured;
            _fileSystemWatcher.Deleted += ChangeOccured;
            _fileSystemWatcher.Renamed += ChangeOccured;
            _fileSystemWatcher.Changed += ChangeOccured;

            

            Singleton.Logger.LogDebug($"FileWatcher for '{sourceDirectory}' started");
        }

        private void ChangeOccured(object sender, FileSystemEventArgs e)
        {
            _modificationsPubsub.Publish(PublisherId, new FileWatcherArgs()
            {
                FileEventsArgs = e,
                SourceDirectory = _sourceDirectory,
                TargetDirectory = _targetDirectory,
            });
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    Singleton.Logger.LogDebug($"FileWatcher for '{_sourceDirectory}' stopped");
                    _fileSystemWatcher.Dispose();
                }
                _disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    public class FileActionChangedEventArgs : EventArgs
    {
        public FileActionChangedEventArgs(IFileAction action)
        {
            Action = action;
        }

        public IFileAction Action { get; }
    }
}
