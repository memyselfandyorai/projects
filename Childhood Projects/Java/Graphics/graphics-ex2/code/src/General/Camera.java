package General;

import org.eclipse.swt.widgets.Canvas;
/**
 * 
 * The class represents a simple pinhole camera. 
 *
 */
public class Camera {
		
	
	private Point eye;  //p (originate)
	private Point lookAt;
	private NormalVector direction;
	private NormalVector upDirection; //up vector
	private NormalVector rightDirection; //right vector
	private double screenDist; //d
	private double screenWidth; //xfox, default: 2.0
	
	private double screenHeight; 
	
	private int height;
	private int width;
	
	
	public Camera()
	{
		screenWidth=2.0;
	}
	
	
	public Point getEye() {
		return eye;
	}

	public void setEye(Point eye) {
		this.eye = eye;
	}

	public void setDirection(NormalVector direction) {
		this.direction = direction;
	}
	
	
	public void setLookAt(Point lookAt) {
		this.lookAt = lookAt;
	}

	public NormalVector getUpDirection() {
		return upDirection;
	}

	public void setUpDirection(NormalVector upDirection) {
		this.upDirection = upDirection;
	}

	public double getScreenDist() {
		return screenDist;
	}

	public void setScreenDist(double screenDist) {
		this.screenDist = screenDist;
	}

	public double getScreenWidth() {
		return screenWidth;
	}

	public void setScreenWidth(double screenWidth) {
		this.screenWidth = screenWidth;
	}
	
	public NormalVector getRightDirection()
	{
		return rightDirection;
	}
	
	
	public double getScreenHeight()
	{
		return screenHeight;
	}
	
	public NormalVector getDirection() //towards
	{
		return direction;
	}
	
	public Point getLookAt() {
		
		return lookAt;
		
	}

	
		
	public boolean hasAllValues()
	{
		if(eye==null)
			return false;
		
		if(upDirection==null)
			return false;
		
		if(screenDist == 0)
			return false;
		
		if(lookAt == null && direction==null) //setDirection sets the lookAt
			return false;
		
		return true;
	}
	
	
	/**
	 * to be activated after information has all values
	 * */
	public void commit(Canvas canvas)
	{
		
		if(direction==null) //lookAt!=null
			direction=NormalVector.subPoints(lookAt,eye);
		
		rightDirection =direction.crossProduct(upDirection);
		
		//if(upDirection.dotProducet(dircetion) != 0
		upDirection=rightDirection.crossProduct(direction); //if up and direction are not orthogonal
			
		//if(lookAt==null)    -> so if direction AND lookAtPoint specified, they would be consistent 
		lookAt=Ray.getPoint(eye, direction,screenDist);
		
		
		height=canvas.getBounds().height;
		width=canvas.getBounds().width;
		
		double porportion=(double)height/width;
		
		screenHeight=porportion*screenWidth;
		
	}
	
	
	
	
	public Ray rayThroughPixel(double x,double y)
	{
		double d=getScreenDist();
		Point eye=getEye();
		NormalVector dir=getDirection();
		
		Point screenCenter=Ray.getPoint(eye,dir,d);

		double halfWidth=getScreenWidth()/2;
		double halfHeight=getScreenHeight()/2;
		
		NormalVector right=getRightDirection();
		NormalVector left=right.negative();
		NormalVector up=getUpDirection();
		NormalVector down=up.negative();
		
		
		/*
		Point rightCenter=Ray.getPoint(screenCenter,right,halfWidth);
		
		
		Point rightDown=Ray.getPoint(rightCenter, down, halfHeight);
		Point leftDown=Ray.getPoint(rightDown, left, halfHeight*2);
		Point rightUp=Ray.getPoint(rightCenter, up,halfHeight);
		*/
		
		Point leftCenter=Ray.getPoint(screenCenter,left,halfWidth);
		Point leftUp=Ray.getPoint(leftCenter,up,halfHeight);
		//Point leftDown=Ray.getPoint(leftCenter,down,halfHeight);
		
		//double xPor= (2*x+1)/(2*width) * halfWidth*2;
		// = average (x/width,  (x+1)/width) * screenWidth
		
		double xPor= (2*x+1)/width * halfWidth; //between 0 to screenWidth: center of pixel
		double yPor= (2*y+1)/height * halfHeight; //between 0 to screenHeight: center of pixel
		
		//Point tempHit=Ray.getPoint(leftDown,right,xPor);
		Point tempHit=Ray.getPoint(leftUp,right,xPor);
		//Point hit=Ray.getPoint(tempHit,up,yPor);
		Point hit=Ray.getPoint(tempHit,down,yPor);
		
		NormalVector vector=NormalVector.subPoints(hit,eye);
	
		Ray ray=new Ray(eye,vector);
		
		return ray;
	}
	
	
}
