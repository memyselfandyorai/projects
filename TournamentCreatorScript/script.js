/*
Copyright 2022 Yorai Geffen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// To learn how to use this script, refer to the documentation:
// https://sites.google.com/view/youreye-communityprojects/tournament-extensions

/**
* @OnlyCurrentDoc
*/

// Last edit: 2023-12-31
/*
Instructions of updating:
1. Upload to git. Check diff.
2. Deploy -> Manage Deployment. See last version. Take the next one.
3. New Deployment -> Library type. Name it. Remember version index.
4. Go to "API & Services" on cloud console: https://console.cloud.google.com/apis/credentials/consent?project=tournament-generator-script
5. Find "Google Workspace Marketplace SDK": https://console.cloud.google.com/apis/api/appsmarket-component.googleapis.com/credentials?project=tournament-generator-script
6. App Configuration -> Sheets Add-on version number
7. Change it to new number. Save.
8. Verify the extension was updated (see date in Google Marketplace). It takes a few minutes to hours.
*/

const IS_LOGGING_ON = true;

const NEW_STATE = 0
const CANCEL_STATE = -1
const OVERWRITE_STATE = 1

const EXTENSION_MENU = 'Extensions';
const EXTENSION_NAME = 'Tournament Extensions';
const CREATE_OPTIONS_NAME = 'Create default options'
const CREATE_TOURNAMENT_NAME = 'Create tournament';
const RECREATE_TOURNAMENT_NAME = 'Rereate tournament';
const CREATE_MINIMAL_OPTIONS_NAME = 'Create minimal options'


const OPTIONS_SHEET = 'Options';
const PLAYERS_COLUMN_NAME = 'Participants';
const LEGACY_PLAYERS_COLUMN_NAME = 'Players';
const SCORE_TEMPLATE_COLUMN_NAME = 'Score template';
const DOUBLE_KNOCKOUT_COLUMN_NAME = 'Double Knockout';
const DUMMY_PLAYER_COLUMN_NAME = 'Auto win name';
const TITLE_COLUMN_NAME = 'Title / Instructions';
const WINNER_HOUSE_TITLE_COLUMN_NAME = 'Main tournament name';
const WINNER_HOUSE_SUBTITLE_COLUMN_NAME = 'Main tournament instructions';
const CONDOLENCE_HOUSE_TITLE_COLUMN_NAME = 'Condolence tournament name';
const CONDOLENCE_HOUSE_SUBTITLE_COLUMN_NAME = 'Condolence tournament instructions';

const OPTIONS_HEADER_ROW = 1;
const OPTIONS_VALUE_ROW = 2;

const TITLES_COLUMN = 1;

const TOURNAMENT_SHEET_NAME = 'Tournament';
const TOURNAMENT_START_ROW = 6;
const TOURNAMENT_START_COLUMN = 2;

const CONDOLENCE_ROW_DIFFERENCE = 8;

const DUMMY_PLAYER_NAME = 'N/A';
const EMPTY_DUMMY_PLAYER_NAME = ' ';

const MATCH_NOT_READY_COLOR = 'red';
const MATCH_READY_COLOR = 'orange';
const MATCH_DONE_COLOR = 'lightgreen';

const FIRST_DEADLINE_COLUMN_NAME = 'First deadline';
const DEADLINE_DELTA_COLUMN_NAME = 'Deadline delta (days)';
const DEADLINE_TITLE = 'Play by:';
const MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
const SPREADSHEET_DAY_TIMESTAMP = 1;
const DEADLINE_DEFAULT_DAYS_DELTA = 7;

const DEADLINE_FONT_SIZE = 9;
const DEADLINE_SOON_DAYS_QUOTIENT = 4;
const DEADLINE_SOON_COLOR = 'red';
const DEADLINE_LONGER_DAYS_QUOTIENT = 2;
const DEADLINE_LONGER_COLOR = 'orange';
const DEADLINE_DONE_COLOR = MATCH_DONE_COLOR;

const FALSE_CHECKBOX_STRING_VALUE = 'false'

const SCORE_MIN_COLUMN_WIDTH = 50;

class RichTextValueBuilder {
    constructor() {
        this.textStylePairs = []
    }

    appendText(text, style = undefined) {
        this.textStylePairs.push([text, style])
        return this
    }

    build() {
        let stringBuilder = ''
        let styles = []
        let richTextBulder = SpreadsheetApp.newRichTextValue()

        for (let pair of this.textStylePairs) {
            let text = pair[0]
            let startIndex = stringBuilder.length
            stringBuilder = stringBuilder.concat(text)
            let style = pair[1]
            if (style) {
                styles.push([startIndex, startIndex + text.length, style.build()])
            } else {
                styles.push([])
            }
        }

        richTextBulder.setText(stringBuilder)
        for (let style of styles) {
            if (style?.length >= 3) {
                richTextBulder.setTextStyle(style[0], style[1], style[2])
            }
        }

        return richTextBulder.build()
    }
}

function createDefaultInstructionsTitle() {
    return new RichTextValueBuilder()
        .appendText(`A Double Knock out draw:\n`)
        .appendText(` * The winner moves on in the Winners House.\n`)
        .appendText(` * The other players moves to the Condolence House there they have the chance the conquer the 3rd place. A second loss is the end in this tournament. Each player plays a min of 2 games.\n`)
        .appendText(` * "${DUMMY_PLAYER_NAME}" means a free game.\n`)
        .appendText(` * Color legend:\n`)
        .appendText(`   - Match number color: `)
        .appendText(`red`, SpreadsheetApp.newTextStyle().setForegroundColor(MATCH_NOT_READY_COLOR).setBold(true))
        .appendText(` - match not ready (one of the players is unknown), `)
        .appendText(`orange`, SpreadsheetApp.newTextStyle().setForegroundColor(MATCH_READY_COLOR).setBold(true))
        .appendText(` - match ready to begin, `)
        .appendText(`green`, SpreadsheetApp.newTextStyle().setForegroundColor(MATCH_DONE_COLOR).setBold(true))
        .appendText(` - match done.\n`)
        .appendText(`   - Deadline color: `)
        .appendText(`orange`, SpreadsheetApp.newTextStyle().setForegroundColor(DEADLINE_LONGER_COLOR))
        .appendText(` - deadline draws near, `)
        .appendText(`read`, SpreadsheetApp.newTextStyle().setForegroundColor(DEADLINE_SOON_COLOR))
        .appendText(` - deadline nearly reached, `)
        .appendText(`green`, SpreadsheetApp.newTextStyle().setForegroundColor(DEADLINE_DONE_COLOR))
        .appendText(` - match is done.`)
        .build()
}

const columnOptions = [
    createOption({
        name: DUMMY_PLAYER_COLUMN_NAME,
        defaultValue: normalizeName(DUMMY_PLAYER_NAME),
        fieldName: 'dummyPlayerName',
        emptyDefaultValue: EMPTY_DUMMY_PLAYER_NAME,
    }),
    createOption({
        name: 'Title/Instructions',
        defaultValue: () => createDefaultInstructionsTitle(),
        fieldName: 'instructions',
        formatCellAction: cell => cell.setFontFamily('Verdana').setFontSize(12),
    }),
    createOption({
        name: 'Main tournament name',
        defaultValue: `Winner's House`,
        fieldName: 'mainTitle',
        formatCellAction: cell => cell.setFontSize(24),
    }),
    createOption({
        name: 'Main tournament instructions',
        defaultValue: 'Please enter your score (how many matches you won) in the Grey Box.',
        fieldName: 'mainSubtitle',
        formatCellAction: cell => cell.setFontSize(10),
    }),
    createOption({
        name: 'Condolence House',
        defaultValue: 'Please enter your score (how many matches you won) in the Grey Box.',
        fieldName: 'condolenceTitle',
        formatCellAction: cell => cell.setFontSize(24),
    }),
    createOption({
        name: 'Condolence tournament instructions',
        defaultValue: 'Please enter your score in the Grey Box',
        fieldName: 'condolenceSubtitle',
        formatCellAction: cell => cell.setFontSize(10),
    }),
]

// TODO: consider optimizations, e.g., when there's "auto win", delete the previous match and add as-is? Can use a flag since it might no be pretty...


function createOption({ name, defaultValue, fieldName, emptyDefaultValue = undefined, formatCellAction = undefined }) {
    // Note defaultValue can be a function.
    return { column: name, defaultValue: defaultValue, fieldName: fieldName, emptyDefaultValue: emptyDefaultValue, formatCellAction: formatCellAction }
}

/** Logs a message. Parameters */
function logData(message, parameters = {}) {
    let data = { ...parameters }
    data.timestamp = new Date()
    data.user_key = Session.getTemporaryActiveUserKey()
    if (IS_LOGGING_ON) {
        console.log({ message: message, data: data });
    }
}

function onInstall(e) {
    onOpen(e);
}
/**
 * Adds a custom menu item to run the script.
 */
function onOpen(ignored) {
    SpreadsheetApp.getUi().createAddonMenu()
        .addItem(`${CREATE_OPTIONS_NAME} (start here)`, 'createOptions')
        .addItem(CREATE_TOURNAMENT_NAME, 'createTournament')
        .addSeparator()
        .addItem(`${CREATE_MINIMAL_OPTIONS_NAME} (alternative start here)`, 'createMinimalOptions')
        .addItem(`${RECREATE_TOURNAMENT_NAME} (keeping only players, scores and modified deadlines)`, 'recreateTournament')
        .addToUi()
}

/** Creates a options sheet with minimal data. */
function createMinimalOptions() {
    let options = {
        minimal: true,
    }
    createOptions(options)
}

/** 
 * Creates a default options sheet. 
 * options: optional. {
 *   minimal - if true, generates the minimum values.
 * }
 */
function createOptions(options) {
    let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    let outputSheet = spreadsheet.getSheetByName(OPTIONS_SHEET)
    let setDefaultValues = !(options?.minimal)
    let methodName = setDefaultValues ? 'createOptions' : 'createMinimalOptions'

    if (outputSheet == null) {
        outputSheet = spreadsheet.insertSheet()
        outputSheet.setName(OPTIONS_SHEET)
        logData(methodName, { state: NEW_STATE })
    } else {
        var ui = SpreadsheetApp.getUi();
        var response = ui.alert(`Are you sure you want to override values in existing '${OPTIONS_SHEET}' sheet?`, ui.ButtonSet.YES_NO);

        // Process the user's response.
        if (response != ui.Button.YES) {
            logData(methodName, { state: CANCEL_STATE })
            return;
        }

        logData(methodName, { state: OVERWRITE_STATE })

        outputSheet.clearFormats()
        outputSheet.getRange(1, 1, outputSheet.getMaxRows(), outputSheet.getMaxColumns()).setDataValidation(null);
        outputSheet.clearConditionalFormatRules();
    }

    outputSheet.getRange(OPTIONS_HEADER_ROW, 1).setValue(PLAYERS_COLUMN_NAME).setFontWeight('bold')
    if (setDefaultValues) {
        outputSheet.getRange(2, 1).setValue('Player1')
        outputSheet.getRange(3, 1).setValue('Player2')
        outputSheet.getRange(4, 1).setValue('Player3')
    }

    outputSheet.getRange(OPTIONS_HEADER_ROW, 2).setValue(DOUBLE_KNOCKOUT_COLUMN_NAME).setFontWeight('bold')
    let doubleKnockoutCell = outputSheet.getRange(OPTIONS_VALUE_ROW, 2).insertCheckboxes()
    doubleKnockoutCell.setValue(setDefaultValues ? true : false)

    outputSheet.getRange(OPTIONS_HEADER_ROW, 3).setValue(SCORE_TEMPLATE_COLUMN_NAME).setFontWeight('bold')
    let scoreTemplaceCell = outputSheet.getRange(OPTIONS_VALUE_ROW, 3)
    if (setDefaultValues) {
        let dropdown = SpreadsheetApp.newDataValidation().requireValueInList(['0', '1', '2'], true).build()
        scoreTemplaceCell.setDataValidation(dropdown).setBackground('lightgrey').setValue('')
    } else {
        scoreTemplaceCell.insertCheckboxes()
    }


    outputSheet.getRange(OPTIONS_HEADER_ROW, 4).setValue(FIRST_DEADLINE_COLUMN_NAME).setFontWeight('bold')
    let firstDeadlineCell = outputSheet.getRange(OPTIONS_VALUE_ROW, 4)
        .setFontSize(DEADLINE_FONT_SIZE)
        .setFontStyle('italic')
    let defaultDeadlineValue = formatDate(new Date(new Date().getTime() + MILLIS_PER_DAY * DEADLINE_DEFAULT_DAYS_DELTA))
    firstDeadlineCell.setValue(setDefaultValues ? defaultDeadlineValue : '')

    outputSheet.getRange(OPTIONS_HEADER_ROW, 5).setValue(DEADLINE_DELTA_COLUMN_NAME).setFontWeight('bold')
    let deadlineDeltaCell = outputSheet.getRange(OPTIONS_VALUE_ROW, 5)
    deadlineDeltaCell.setValue(setDefaultValues ? `${DEADLINE_DEFAULT_DAYS_DELTA}` : '')

    let minColumn = 6
    // So resize won't affect them:
    let instructionsCell = outputSheet.getRange(OPTIONS_HEADER_ROW + 6, 3).setValue('')
    columnOptions.forEach((option, i) => {
        outputSheet.getRange(OPTIONS_VALUE_ROW, minColumn + i).setValue(EMPTY_DUMMY_PLAYER_NAME)
    })

    outputSheet.autoResizeColumns(1, minColumn + columnOptions.length)

    columnOptions.forEach((option, i) => {
        outputSheet.getRange(OPTIONS_HEADER_ROW, minColumn + i).setValue(option.column).setFontWeight('bold')

        if (!setDefaultValues) {
            // Skip setting the value.
            return;
        }

        let valueCell = outputSheet.getRange(OPTIONS_VALUE_ROW, minColumn + i)
        if (typeof option.defaultValue === 'string' || option.defaultValue instanceof String) {
            valueCell.setValue(option.defaultValue)
        } else {
            let cellValue = option.defaultValue()
            valueCell.setRichTextValue(cellValue)
        }

        if (option.formatCellAction) {
            option.formatCellAction(valueCell)
        }
    })

    let instructionsColumnCell = outputSheet.getRange(OPTIONS_HEADER_ROW + 5, 3).setValue('').setFontWeight('bold')


    instructionsColumnCell.setValue('Instructions / How to use:')
    instructionsCell.setRichTextValue(formatInstructions())
}

function formatDate(date) {
    let timezone = SpreadsheetApp.getActive().getSpreadsheetTimeZone()
    return Utilities.formatDate(date, timezone, "yyyy-MM-dd")
}

function buildOption(label) {
    return `'${EXTENSION_MENU}' -> '${EXTENSION_NAME}' -> '${label}'`
}

// How to use:
function formatInstructions() {
    let instructions = new RichTextValueBuilder()
    instructions.appendText(`How to use:\n`, SpreadsheetApp.newTextStyle().setUnderline(true))
    instructions.appendText(`1. Use ${buildOption(CREATE_OPTIONS_NAME)}`, SpreadsheetApp.newTextStyle().setBold(true))
    instructions.appendText(` to create a sheet with default options.\n`)
    instructions.appendText(`2. Use ${buildOption(CREATE_TOURNAMENT_NAME)}`, SpreadsheetApp.newTextStyle().setBold(true))
    instructions.appendText(` to generate a knockout tournament between X players. First put the players under the "${PLAYERS_COLUMN_NAME}" column, and randomize their order. If you wish to create multiple tournaments, rename the created '${TOURNAMENT_SHEET_NAME}' sheet.\n`)
    instructions.appendText(`Advanced:\n`, SpreadsheetApp.newTextStyle().setUnderline(true))
    instructions.appendText(`3. Use ${buildOption(RECREATE_TOURNAMENT_NAME)}`, SpreadsheetApp.newTextStyle().setBold(true))
    instructions.appendText(` to re-generate an existing tournament from the new options, keeping only the players, the scores and the modified deadlines.\n`)
    instructions.appendText(`4. Easy start: `, SpreadsheetApp.newTextStyle().setBold(true))
    instructions.appendText(`Insert a Checkbox to the '${SCORE_TEMPLATE_COLUMN_NAME}' column, remove the '${FIRST_DEADLINE_COLUMN_NAME}' value, and everything on the right of '${DUMMY_PLAYER_COLUMN_NAME}'. Add players, randomize their order, and create the tournament.\n`)
    instructions.appendText(`5. Modify the values under the options columns in the '${CREATE_OPTIONS_NAME}' sheet:\n`)
    instructions.appendText(`   * '${SCORE_TEMPLATE_COLUMN_NAME}' - `, SpreadsheetApp.newTextStyle().setBold(true))
    instructions.appendText(`This is copied near all players. You can use whicever template you want (checkbox, free text, dropdown menu, etc.) - the winner is automatically chosen by the "higher" value. As long as there's a draw, no winner will be chosen.\n`)
    instructions.appendText(`   * '${DUMMY_PLAYER_COLUMN_NAME}' - `, SpreadsheetApp.newTextStyle().setBold(true))
    instructions.appendText(`Placeholder/filler text for opponents that all players automatically win against. Used in case players count is not a power of 2.\n`)
    instructions.appendText(`   * '${DOUBLE_KNOCKOUT_COLUMN_NAME}' - `, SpreadsheetApp.newTextStyle().setBold(true))
    instructions.appendText(`When checked, creates a "condolence" house. Each player who didn't win (except the last one, the second place) will have at least one more match to play, for a chance to reach the 3rd place.\n`)
    instructions.appendText(`   * '${FIRST_DEADLINE_COLUMN_NAME}' - `, SpreadsheetApp.newTextStyle().setBold(true))
    instructions.appendText(`When a date is set, it's added (including format) as a deadline to all first matches (prefixed by '${DEADLINE_TITLE}'). The other matches will be at least X days from the matches they are based on (X = value of '${DEADLINE_DELTA_COLUMN_NAME}', with a default of ${DEADLINE_DEFAULT_DAYS_DELTA}). Put an empty value if you don't want to assign deadlines.\n`)
    instructions.appendText(`      - You can manually set a match's date (use "ctrl+;" or "ctrl+c" and then "ctrl+shift+v" to easily pick a date), and all dependent dates will be updated accordingly.\n`)
    instructions.appendText(`      - When the time draws near, it'll turn `)
        .appendText('orange', SpreadsheetApp.newTextStyle().setForegroundColor(DEADLINE_LONGER_COLOR))
        .appendText(`. When it's nearly reached, it'll turn `)
        .appendText('red', SpreadsheetApp.newTextStyle().setForegroundColor(DEADLINE_SOON_COLOR))
        .appendText(`. When the match is done, it'll turn `)
        .appendText('green\n', SpreadsheetApp.newTextStyle().setForegroundColor(DEADLINE_DONE_COLOR))
    instructions.appendText(`      - You can use times in addition (or instead) of a date, and use fractions (e.g., '=1/24' for one hour delta) in '${DEADLINE_DELTA_COLUMN_NAME}'.\n`)
    instructions.appendText(`   * The other columns are used as titles/instructions in the generated sheet. Try generating to see how they look.\n`)
    instructions.appendText(`   * Color legend: `, SpreadsheetApp.newTextStyle().setItalic(true).setBold(true))
    instructions.appendText(`Red`, SpreadsheetApp.newTextStyle().setForegroundColor(MATCH_NOT_READY_COLOR))
    instructions.appendText(` - match not ready (one of the players is unknown), `)
    instructions.appendText(`orange`, SpreadsheetApp.newTextStyle().setForegroundColor(MATCH_READY_COLOR))
    instructions.appendText(` - match ready to begin, `)
    instructions.appendText(`green`, SpreadsheetApp.newTextStyle().setForegroundColor(MATCH_DONE_COLOR))
    instructions.appendText(` - match done.\n`)

    return instructions.build()
}

function throwError(message, ...args) {
    throw new Error(Utilities.formatString(message, ...args))
}

/** Creates the tournament sheet. */
function createTournament() {
    createTournamentInner()
}


// https://developers.google.com/apps-script/guides/triggers/events#google_sheets_events
function onEdit(e) {
    let activeSheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet()

    if ((!e.oldValue || e.oldValue == FALSE_CHECKBOX_STRING_VALUE) && e.value) {
        // There was an edit from none to something.
        let input = {
            updatedRange: e.range,
        }
        updateDeadlineToNow(activeSheet, input)
    }
}

function updateDeadlineToNow(activeSheet, input) {
    let state = getProperty(getTournamentStateKey(activeSheet.getSheetId()))
    if (!state) {
        return;
    }

    // Map from annotation to the deadline cell
    let playerScoreCells = new Map(
        state.results.map(result => {
            return [result.player1ScoreCell, result.deadlineCell]
        }).concat(state.results.map(result => {
            return [result.player2ScoreCell, result.deadlineCell]
        }))
    )

    let range = input.updatedRange
    for (let i = 1; i <= range.getNumRows(); i++) {
        for (let j = 1; j <= range.getNumColumns(); j++) {
            let cell = range.getCell(i, j)
            let annotation = cell.getA1Notation()
            if (playerScoreCells.has(annotation)) {
                let deadlineCellNotation = playerScoreCells.get(annotation)
                let deadlineCell = activeSheet.getRange(deadlineCellNotation)
                let newDate = new Date()
                let existingDate = deadlineCell.getValue()
                // Only set to "now" if deadline already passed. The idea is to give "delta" time until next match. No need to make all deadlines earlier if matches are played faster.
                if (newDate > existingDate) {
                    deadlineCell.setValue(formatDate(newDate))
                }
            }
        }
    }
}

function recreateTournament() {
    let activeSheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet()
    let state = getProperty(getTournamentStateKey(activeSheet.getSheetId()))
    if (!state) {
        throwError('Active sheet is not a tournament or there was a storage quota error.')
    }

    let matchMap = state.results.map(result => ({
        key: result.matchNumber,
        value: {
            player1Score: activeSheet.getRange(result.player1ScoreCell).getValue(),
            player2Score: activeSheet.getRange(result.player2ScoreCell).getValue(),
            deadline: getCurrentDeadlineValue(activeSheet, result.deadlineCell, state.firstDeadline),
        },
    })).reduce((map, obj) => {
        map[obj.key] = obj.value;
        return map;
    }, {});

    let recreateOptions = {
        players: state.players,
        matchResults: matchMap,
        sheetName: activeSheet.getName(),
    }

    createTournamentInner(recreateOptions)
}

function getCurrentDeadlineValue(activeSheet, deadlineCell, firstDeadline) {
    if (!deadlineCell || !firstDeadline) {
        return undefined
    }
    let cell = activeSheet.getRange(deadlineCell)
    let cellValue = cell.getValue()
    if (!cell.getFormula() && new Date(cellValue) != firstDeadline && cellValue) {
        return new Date(cellValue)
    }
    return null
}

/**
 * recreateOptions: {
 *  sheetName, 
 *  players - list,
 *  results: map of matchNumber -> {
 *    player1Score,
 *    player2Score, 
 *    nullable deadline, - if exists, should be a value different than template
 *  }
 * }
 */
function createTournamentInner(recreateOptions = undefined) {
    let spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    let optionsSheet = spreadsheet.getSheetByName(OPTIONS_SHEET);
    if (optionsSheet == null) {
        throwError('Please use the %s to create a sheet named "%s".', buildOption(CREATE_OPTIONS_NAME), OPTIONS_SHEET)
    }

    let templateIndex = getColumnIndexByName(optionsSheet, SCORE_TEMPLATE_COLUMN_NAME)
    if (templateIndex == -1) {
        throwError('No column "%s" exists. Consider using the %s to override the sheet with proper columns.', SCORE_TEMPLATE_COLUMN_NAME, buildOption(CREATE_OPTIONS_NAME))
    }
    let scoreTemplate = optionsSheet.getRange(OPTIONS_VALUE_ROW, templateIndex + 1)

    let playersArray = recreateOptions?.players
    if (!playersArray) {
        let playerColumn = getColumnIndexByName(optionsSheet, PLAYERS_COLUMN_NAME)
        if (playerColumn == -1) {
            // For backwards compatibility.
            playerColumn = getColumnIndexByName(optionsSheet, LEGACY_PLAYERS_COLUMN_NAME)
            if (playerColumn == -1) {
                throwError('No column "%s" exists. Consider using the %s to override the sheet with proper columns.', PLAYERS_COLUMN_NAME, buildOption(CREATE_OPTIONS_NAME))
            }
        }
        playersArray = optionsSheet.getRange(OPTIONS_VALUE_ROW, playerColumn + 1, optionsSheet.getDataRange().getNumRows() - 1).getValues().map(row => row[0]).filter(value => value != '')
        if (playersArray.length < 2) {
            throwError('Minimum 2 players. Got %d.', playersArray.length)
        }
    }

    let doubleKnockoutIndex = getColumnIndexByName(optionsSheet, DOUBLE_KNOCKOUT_COLUMN_NAME)
    if (doubleKnockoutIndex == -1) {
        throwError('No column "%s" exists. Consider using the %s to override the sheet with proper columns.', DOUBLE_KNOCKOUT_COLUMN_NAME, buildOption(CREATE_OPTIONS_NAME))
    }
    let doubleKnockoutOn = optionsSheet.getRange(OPTIONS_VALUE_ROW, doubleKnockoutIndex + 1).getValue() == true

    let tournamentSheetName = recreateOptions?.sheetName ?? TOURNAMENT_SHEET_NAME

    let logParameters = {
        is_recreate: !!recreateOptions,
        is_double_knockout: doubleKnockoutOn,
        player_count: playersArray.length,
        score_template_validation_criteria: `${scoreTemplate.getDataValidation()?.getCriteriaType() ?? 'NONE'}`,
    }

    let outputSheet = spreadsheet.getSheetByName(tournamentSheetName)
    if (outputSheet == null) {
        outputSheet = spreadsheet.insertSheet()
        outputSheet.setName(TOURNAMENT_SHEET_NAME)
        logParameters.state = NEW_STATE
    } else {
        var ui = SpreadsheetApp.getUi();
        var textAddition = recreateOptions ? '' : '\nRename the sheet if you want to keep its values while creating a new tournament.'
        var response = ui.alert(`Are you sure you want to override values in the existing '${tournamentSheetName}' sheet?${textAddition}`, ui.ButtonSet.YES_NO);

        // Process the user's response.
        if (response != ui.Button.YES) {
            logParameters.state = CANCEL_STATE
            logData('createTournament', logParameters)
            return;
        }

        logParameters.state = OVERWRITE_STATE

        outputSheet.clear()
        outputSheet.getRange(1, 1, outputSheet.getMaxRows(), outputSheet.getMaxColumns()).setDataValidation(null);
        outputSheet.clearConditionalFormatRules();
    }

    let firstDeadlineCell = undefined
    let firstDeadlineIndex = getColumnIndexByName(optionsSheet, FIRST_DEADLINE_COLUMN_NAME)
    if (firstDeadlineIndex != -1) {
        let cell = optionsSheet.getRange(OPTIONS_VALUE_ROW, firstDeadlineIndex + 1)
        let dateValue = new Date(cell.getValue())
        if (!isNaN(dateValue)) {
            firstDeadlineCell = cell
        }
    }
    let deadlineDelta = DEADLINE_DEFAULT_DAYS_DELTA
    let deadlineDeltaIndex = getColumnIndexByName(optionsSheet, DEADLINE_DELTA_COLUMN_NAME)
    if (deadlineDeltaIndex != -1) {
        let numberValue = Number(optionsSheet.getRange(OPTIONS_VALUE_ROW, deadlineDeltaIndex + 1).getValue())
        if (numberValue && !isNaN(numberValue)) {
            deadlineDelta = numberValue
        }
    }

    let deadlineOptions = firstDeadlineCell ? {
        firstDeadlineCell: firstDeadlineCell,
        deadlineDeltaDays: deadlineDelta,
    } : undefined

    logParameters.has_deadline = !!deadlineOptions
    logData('createTournament', logParameters)

    let options = {
        doubleKnockoutOn: doubleKnockoutOn,
        scoreTemplate: scoreTemplate,
        deadlineOptions: deadlineOptions,
        recreateOptions: recreateOptions,
    }

    columnOptions.forEach(option => {
        let optionValue = ''
        let optionCell = undefined
        let columnIndex = getColumnIndexByName(optionsSheet, option.column)
        if (columnIndex != -1) {
            optionCell = optionsSheet.getRange(OPTIONS_VALUE_ROW, columnIndex + 1)
            let value = optionCell.getValue()
            if (option.emptyDefaultValue && value == '') {
                optionValue = option.emptyDefaultValue
            } else {
                optionValue = value
            }
        }

        options[option.fieldName] = optionValue
        options[`${option.fieldName}Cell`] = optionCell
    })


    let tournamentResult = createTournamentSheet(outputSheet, playersArray, options)
    let condolenceMatches = []
    if (options.doubleKnockoutOn) {
        condolenceMatches = createCondolenceTournament(outputSheet, tournamentResult)
    }

    printWinningResults(tournamentResult, condolenceMatches)


    // Store data for recreating.
    /*
      players: ordered list of name,
      results: list of {
        matchNumber,
        player1ScoreCell,
        player2ScoreCell,
        deadlineCell, -nullable
      },
      firstDeadline: nullable date,
     */
    // Recreate state:
    let allRelevantMatches = tournamentResult.allMatches.concat(condolenceMatches).flatMap(match => match)
    let results = allRelevantMatches.map(match => ({
        matchNumber: match.number,
        player1ScoreCell: match.player1ScoreCell.getA1Notation(),
        player2ScoreCell: match.player2ScoreCell.getA1Notation(),
        deadlineCell: match.deadlineCell?.getA1Notation(),
    }))

    let firstDeadline = firstDeadlineCell ? new Date(firstDeadlineCell.getValue()) : undefined
    let state = {
        players: playersArray,
        results: results,
        firstDeadline: firstDeadline,
    }
    setProperty(getTournamentStateKey(outputSheet.getSheetId()), state)
}

function printWinningResults(tournamentResult, condolenceMatches) {
    let allMatches = tournamentResult.allMatches
    let state = tournamentResult.state
    let outputSheet = state.outputSheet

    // Last list is empty.
    let winningMatch = allMatches[allMatches.length - 2][0]

    let resultRow = winningMatch.winnerCell.getRow()
    let resultColumRow = resultRow - 1
    let startingColumn = winningMatch.winnerCell.getColumn() + 3

    outputSheet.getRange(resultColumRow, startingColumn)
        .setValue('Winner (first place)')
        .setFontWeight('bold')
    outputSheet.getRange(resultColumRow, startingColumn + 1)
        .setValue('Second place')
        .setFontWeight('bold')

    outputSheet.getRange(resultRow, startingColumn)
        .setFormula(`=${winningMatch.winnerCell.getA1Notation()}`)
        .setFontWeight('bold')
        .setFontSize(20)
    let secondPlaceCell = outputSheet.getRange(resultRow, startingColumn + 1).setFontSize(18)
    setCondolencePlayer(secondPlaceCell, winningMatch)

    if (condolenceMatches.length > 0) {
        outputSheet.getRange(resultColumRow, startingColumn + 2)
            .setValue('Third place')
            .setFontWeight('bold')

        // Last list is empty.
        let lastCondolenceMatch = condolenceMatches[condolenceMatches.length - 2][0]
        outputSheet.getRange(resultRow, startingColumn + 2)
            .setFormula(`=${lastCondolenceMatch.winnerCell.getA1Notation()}`)
            .setFontSize(16)
    }

    outputSheet.autoResizeColumns(startingColumn, 3)
    let resultSize = Math.max(...[0, 1, 2].map(i => outputSheet.getColumnWidth(startingColumn + i)), state.playersColumnWidth) * 2
    outputSheet.setColumnWidths(startingColumn, 3, resultSize)

    // Add formatting rules for match numbers.
    let formattingrules = outputSheet.getConditionalFormatRules();
    let allRelevantMatches = allMatches.concat(condolenceMatches).flatMap(match => match)

    let additionalRules = allRelevantMatches
        .map(match => [
            SpreadsheetApp.newConditionalFormatRule()
                .whenFormulaSatisfied(`=${match.winnerCell.getA1Notation()}<>""`)
                .setBackground(MATCH_DONE_COLOR)
                .setRanges([match.matchNumberCell])
                .build(),
            SpreadsheetApp.newConditionalFormatRule()
                .whenFormulaSatisfied(`=or(${match.player1Cell.getA1Notation()}="",${match.player2Cell.getA1Notation()}="")`)
                .setBackground(MATCH_NOT_READY_COLOR)
                .setRanges([match.matchNumberCell])
                .build(),
        ]).flatMap(rules => rules)


    let deadlineFormatRules = []
    if (state.options.deadlineOptions) {
        let allDeadlineCells = allRelevantMatches.map(match => match.deadlineCell)
        let firstMatchDeadlineCell = allDeadlineCells[0]
        let firstMatchWinnerCell = allRelevantMatches[0].winnerCell
        let deadlineDeltaDays = state.options.deadlineOptions.deadlineDeltaDays
        deadlineFormatRules = [
            SpreadsheetApp.newConditionalFormatRule()
                .whenFormulaSatisfied(`=NE(${firstMatchWinnerCell.getA1Notation()}, "")`)
                .setFontColor(DEADLINE_DONE_COLOR)
                .setRanges(allDeadlineCells)
                .build(),
            SpreadsheetApp.newConditionalFormatRule()
                .whenFormulaSatisfied(`=LT(${firstMatchDeadlineCell.getA1Notation()}, now() + ${deadlineDeltaDays / DEADLINE_SOON_DAYS_QUOTIENT})`)
                .setFontColor(DEADLINE_SOON_COLOR)
                .setRanges(allDeadlineCells)
                .build(),
            SpreadsheetApp.newConditionalFormatRule()
                .whenFormulaSatisfied(`=LT(${firstMatchDeadlineCell.getA1Notation()}, now() + ${deadlineDeltaDays / DEADLINE_LONGER_DAYS_QUOTIENT})`)
                .setFontColor(DEADLINE_LONGER_COLOR)
                .setRanges(allDeadlineCells)
                .build(),
        ]
    }

    formattingrules.push(...additionalRules, ...deadlineFormatRules);
    outputSheet.setConditionalFormatRules(formattingrules);

    let matchCountStartingColumn = startingColumn - 2
    let matchCountHeaderRow = TOURNAMENT_START_ROW - 1
    let matchCountValueRow = matchCountHeaderRow + 1

    let players1Range = `{${allRelevantMatches.map(match => match.player1Cell.getA1Notation()).join(',')}}`
    let players2Range = `{${allRelevantMatches.map(match => match.player2Cell.getA1Notation()).join(',')}}`
    let noPlayerIsDummyLambda = `lambda(player1, player2, INT(AND(NE(player1, "${state.options.dummyPlayerName}"), NE(player2, "${state.options.dummyPlayerName}"))))`
    let countRealMatchesFormula = `sum(map(${players1Range},${players2Range}, ${noPlayerIsDummyLambda}))`

    let winnersRange = `{${allRelevantMatches.map(match => match.winnerCell.getA1Notation()).join(',')}}`
    let matchDoneWithoutDummyLambda = `lambda(player1, player2, winner, INT(AND(NE(player1, "${state.options.dummyPlayerName}"), NE(player2, "${state.options.dummyPlayerName}"),NE(winner, ""))))`
    let countMatchesDoneWithoutDummyFormula = `sum(map(${players1Range},${players2Range}, ${winnersRange}, ${matchDoneWithoutDummyLambda}))`

    outputSheet.getRange(matchCountHeaderRow, matchCountStartingColumn)
        .setValue('Matches Done')
        .setFontWeight('bold')
    outputSheet.getRange(matchCountValueRow, matchCountStartingColumn)
        .setFormula(`=${countMatchesDoneWithoutDummyFormula}`)
    outputSheet.getRange(matchCountHeaderRow, matchCountStartingColumn + 1)
        .setValue('Total Matches')
        .setFontWeight('bold')
    outputSheet.getRange(matchCountValueRow, matchCountStartingColumn + 1)
        .setFormula(`=${countRealMatchesFormula}`)
}

function getTournamentStateKey(sheetId) {
    return `TOURNAMENT_STATE_${sheetId}`
}

function setProperty(key, value) {
    // Properties value size	9 KB / val	
    // Consider optimization with `Utilities.newBlob` and `Utilities.gzip` to compress json string value.
    PropertiesService.getDocumentProperties().setProperty(key, JSON.stringify(value))
}

function getProperty(key) {
    return JSON.parse(PropertiesService.getDocumentProperties().getProperty(key))
}

function getColumnIndexByName(sheet, colName) {
    var data = sheet.getDataRange().getValues();
    return data[0].indexOf(colName);
}


/** 
 * Creates the main tournament graph. 
 * 
 * Returns an object {allMatches - list of each level of matches, state}
*/
function createTournamentSheet(outputSheet, playersArray, options) {
    options.instructionsCell?.copyTo(outputSheet.getRange(1, TITLES_COLUMN))
    options.mainTitleCell?.copyTo(outputSheet.getRange(2, TITLES_COLUMN))
    options.mainSubtitleCell?.copyTo(outputSheet.getRange(3, TITLES_COLUMN))

    let matchNumber = 1
    let allMatches = []
    let currentLevelMatches = []

    let players = fillPlayers(playersArray, options).map(v => normalizeName(v))
    for (i = 0; i < players.length; i += 2) {
        let player1 = players[i]
        let player2 = players[i + 1]

        let player1Row = TOURNAMENT_START_ROW + i * 2
        let player2Row = player1Row + 2

        let player1Cell = outputSheet.getRange(player1Row, TOURNAMENT_START_COLUMN).setValue(player1)
        let player2Cell = outputSheet.getRange(player2Row, TOURNAMENT_START_COLUMN).setValue(player2)

        let match = createMatch(outputSheet, player1Cell, player2Cell, options, matchNumber)
        currentLevelMatches.push(match)
        matchNumber++
    }

    let minWidth = outputSheet.getColumnWidth(TOURNAMENT_START_COLUMN)
    outputSheet.autoResizeColumn(TOURNAMENT_START_COLUMN)
    let playersColumnWidth = Math.max(outputSheet.getColumnWidth(TOURNAMENT_START_COLUMN), minWidth)
    outputSheet.setColumnWidth(TOURNAMENT_START_COLUMN, playersColumnWidth)

    outputSheet.autoResizeColumn(TOURNAMENT_START_COLUMN + 1)
    let scoreTemplateWidth = Math.max(outputSheet.getColumnWidth(TOURNAMENT_START_COLUMN + 1), SCORE_MIN_COLUMN_WIDTH)

    allMatches.push(currentLevelMatches)

    let state = {
        matchNumber: matchNumber,
        outputSheet: outputSheet,
        options: options,
        playersColumnWidth: playersColumnWidth,
        scoreTemplateWidth: scoreTemplateWidth,
    }

    while (currentLevelMatches.length > 0) {
        currentLevelMatches = createNextTournamentLevelMatches(state, currentLevelMatches)
        allMatches.push(currentLevelMatches)
    }

    return {
        allMatches: allMatches,
        state: state,
    }
}

function normalizeName(v) {
    name = String(v)
    return name.trim() == '' ? name : name.trim()
}

/**
 * state = {
    matchNumber: matchNumber,
    outputSheet: outputSheet,
    scoreTemplate: scoreTemplate,
    options, 
    playersColumnWidth, 
    scoreTemplateWidth,
  }
 * 
 */
function createNextTournamentLevelMatches(state, previousMatches) {
    let currentLevelMatches = []

    let outputSheet = state.outputSheet

    if (previousMatches.length > 0) {
        outputSheet.setColumnWidth(previousMatches[0].winnerCell.getColumn(), state.playersColumnWidth)
        outputSheet.setColumnWidth(previousMatches[0].player1ScoreCell.getColumn(), state.scoreTemplateWidth)
    }


    if (previousMatches.length == 1) {
        let winnerCell = addMatchWinner(previousMatches[0], state.options)
        winnerCell
            .setBorder(true, true, true, true, null, null, null, SpreadsheetApp.BorderStyle.SOLID_THICK)
            .setBackground('yellow')
            .setFontSize(winnerCell.getFontSize() + 2)
        return []
    }

    for (i = 0; i < previousMatches.length; i += 2) {
        let match1 = previousMatches[i]
        let match2 = previousMatches[i + 1]

        let winner1Cell = addMatchWinner(match1, state.options)
        let winner2Cell = addMatchWinner(match2, state.options)
        let deadlineValue = getDeadlineValue(match1, match2, state.options)

        let match = createMatch(outputSheet, winner1Cell, winner2Cell, state.options, state.matchNumber, deadlineValue)
        currentLevelMatches.push(match)
        state.matchNumber++
    }

    return currentLevelMatches
}

// Returns all condolence matches.
function createCondolenceTournament(outputSheet, tournamentResult) {
    let allMatches = tournamentResult.allMatches
    let state = tournamentResult.state

    let firstLevelMatches = allMatches[0]

    if (firstLevelMatches.length < 2) {
        // If there is originally only one match, no point in doing anything.
        return [];
    }

    let lastElement = getLastElement(firstLevelMatches)
    let startingRow = lastElement.player2Cell.getRow() + CONDOLENCE_ROW_DIFFERENCE
    state.options.condolenceTitleCell?.copyTo(outputSheet.getRange(startingRow - 4, TITLES_COLUMN))
    state.options.condolenceSubtitleCell?.copyTo(outputSheet.getRange(startingRow - 3, TITLES_COLUMN))

    let allCondolenceMatches = []
    let currentLevelMatches = []
    for (let i = 0; i < firstLevelMatches.length; i += 2) {
        let player1Cell = outputSheet.getRange(startingRow + i * 2, TOURNAMENT_START_COLUMN)
        let player2Cell = outputSheet.getRange(startingRow + i * 2 + 2, TOURNAMENT_START_COLUMN)

        let match1 = firstLevelMatches[i]
        let match2 = firstLevelMatches[i + 1]
        setCondolencePlayer(player1Cell, match1)
        setCondolencePlayer(player2Cell, match2)
        let deadlineValue = getDeadlineValue(match1, match2, state.options)

        let match = createMatch(outputSheet, player1Cell, player2Cell, state.options, state.matchNumber, deadlineValue)
        currentLevelMatches.push(match)
        state.matchNumber++
    }

    allCondolenceMatches.push(currentLevelMatches)

    for (let i = 1; i < allMatches.length && currentLevelMatches.length > 0; i++) {
        // "length-2" since last "matchs" is zero-length (only the winner), and the one before that is a single match - 
        // containing the "second place" which does not get another match.
        let appendCondolencePlayers = (i < allMatches.length - 2)
        let currentLevelOriginalMatches = allMatches[i]
        currentLevelMatches = createNextCondolenceTournamentLevelMatches(state, currentLevelMatches, appendCondolencePlayers, currentLevelOriginalMatches)
        allCondolenceMatches.push(currentLevelMatches)
    }

    // for leftover levels created by dummy matches:
    while (currentLevelMatches.length > 0) {
        currentLevelMatches = createNextTournamentLevelMatches(state, currentLevelMatches)
        allCondolenceMatches.push(currentLevelMatches)
    }

    return allCondolenceMatches
}

function createNextCondolenceTournamentLevelMatches(state, previousMatches, appendCondolencePlayers, currentLevelOriginalMatches) {
    let outputSheet = state.outputSheet

    let currentLevelMatches = createNextTournamentLevelMatches(state, previousMatches)

    if (currentLevelMatches.length < 1) {
        return currentLevelMatches
    }

    // append more losing players and/or dummy players:
    let lastMatch = getLastElement(currentLevelMatches)
    let column = lastMatch.player1Cell.getColumn()
    let playerRowDifference = lastMatch.player2Cell.getRow() - lastMatch.player1Cell.getRow()
    let thisLevelStartingRow = lastMatch.player2Cell.getRow() + playerRowDifference

    let nextMatchRow = thisLevelStartingRow

    if (appendCondolencePlayers) {
        for (i = 0; i < currentLevelOriginalMatches.length; i += 2) {
            let player1Cell = outputSheet.getRange(nextMatchRow, column)
            let player2Cell = outputSheet.getRange(nextMatchRow + playerRowDifference, column)

            let match1 = currentLevelOriginalMatches[i]
            let match2 = currentLevelOriginalMatches[i + 1]
            setCondolencePlayer(player1Cell, match1)
            setCondolencePlayer(player2Cell, match2)
            let deadlineValue = getDeadlineValue(match1, match2, state.options)

            let match = createMatch(outputSheet, player1Cell, player2Cell, state.options, state.matchNumber, deadlineValue)
            currentLevelMatches.push(match)
            state.matchNumber++
            nextMatchRow += playerRowDifference + 2
        }
    }

    // Add dummy matches for non-even levels.
    if (currentLevelMatches.length > 1 && currentLevelMatches.length % 2 != 0) {
        let player1Cell = outputSheet.getRange(nextMatchRow, column).setValue(state.options.dummyPlayerName)
        let player2Cell = outputSheet.getRange(nextMatchRow + playerRowDifference, column).setValue(state.options.dummyPlayerName)

        let match = createMatch(outputSheet, player1Cell, player2Cell, state.options, state.matchNumber)
        currentLevelMatches.push(match)
        state.matchNumber++
    }

    return currentLevelMatches
}

function getLastElement(array) {
    return array[array.length - 1]
}

function setCondolencePlayer(playerCell, match) {
    let player1Location = match.player1Cell.getA1Notation()
    let player2Location = match.player2Cell.getA1Notation()
    let winnerLocation = match.winnerCell.getA1Notation()
    let loserPlayerFormula = `=if(or(${player1Location}="",${player2Location}=""), "",if(${winnerLocation}=${player1Location}, ${player2Location}, if(${winnerLocation}=${player2Location}, ${player1Location},"")))`
    playerCell.setFormula(loserPlayerFormula)
}

/**
 * Using two players with value, creates the match format.
 * 
 * Returns "match:" {
      number : matchNumber,
      player1Cell: player1Cell,
      player2Cell: player2Cell,
      player1ScoreCell: player1ScoreCell,
      player2ScoreCell: player2ScoreCell,
      winnerCell: winnerCell,
      matchNumberCell,
      deadlineCell, nullable
    }
 */
function createMatch(outputSheet, player1Cell, player2Cell, options, matchNumber, deadlineValue = undefined) {
    player1Cell.setBorder(true, null, null, null, null, null, null, SpreadsheetApp.BorderStyle.SOLID_THICK)
    player2Cell.setBorder(null, null, true, null, null, null, null, SpreadsheetApp.BorderStyle.SOLID_THICK)

    let player1ScoreCell = outputSheet.getRange(player1Cell.getRow(), player1Cell.getColumn() + 1)
    options.scoreTemplate.copyTo(player1ScoreCell)
    player1ScoreCell.setBorder(true, null, null, true, null, null, null, SpreadsheetApp.BorderStyle.SOLID_THICK)

    let player2ScoreCell = outputSheet.getRange(player2Cell.getRow(), player2Cell.getColumn() + 1)
    options.scoreTemplate.copyTo(player2ScoreCell)
    player2ScoreCell.setBorder(null, null, true, true, null, null, null, SpreadsheetApp.BorderStyle.SOLID_THICK)

    let middleRowIndex = (player1ScoreCell.getRow() + player2ScoreCell.getRow()) / 2
    let matchNumberCell = outputSheet.getRange(middleRowIndex, player1ScoreCell.getColumn())
        .setValue(`#${matchNumber}`)
        .setFontWeight('bold')
        .setBackground(MATCH_READY_COLOR)

    fillRightBorder(outputSheet, player1ScoreCell, player2ScoreCell)

    let deadlineCell = undefined
    if (options.deadlineOptions) {
        deadlineCell = outputSheet.getRange(matchNumberCell.getRow(), matchNumberCell.getColumn() - 1)

        outputSheet.getRange(deadlineCell.getRow(), deadlineCell.getColumn() - 1)
            .setValue(DEADLINE_TITLE)
            .setFontStyle('italic')
            .setFontSize(DEADLINE_FONT_SIZE)

        options.deadlineOptions.firstDeadlineCell.copyTo(deadlineCell)
        if (deadlineValue) {
            if (deadlineValue.startsWith('=')) {
                deadlineCell.setFormula(deadlineValue)
            } else {
                deadlineCell.setValue(deadlineValue)
            }
        }
    }

    let winnerCell = outputSheet.getRange(middleRowIndex, player2ScoreCell.getColumn() + 1)
    if (options.recreateOptions) {
        let matchResult = options.recreateOptions.matchResults[matchNumber]
        if (matchResult) {
            player1ScoreCell.setValue(matchResult.player1Score)
            player2ScoreCell.setValue(matchResult.player2Score)
            if (matchResult.deadline && deadlineCell) {
                deadlineCell.setValue(matchResult.deadline)
            }
        }
    }

    return {
        number: matchNumber,
        player1Cell: player1Cell,
        player2Cell: player2Cell,
        player1ScoreCell: player1ScoreCell,
        player2ScoreCell: player2ScoreCell,
        winnerCell: winnerCell,
        matchNumberCell: matchNumberCell,
        deadlineCell: deadlineCell, // nullable
    }
}

function fillRightBorder(sheet, fromCellDisculing, toCellDiscluding) {
    let column = fromCellDisculing.getColumn()
    for (let row = fromCellDisculing.getRow() + 1; row < toCellDiscluding.getRow(); row++) {
        sheet.getRange(row, column).setBorder(null, null, null, true, null, null, null, SpreadsheetApp.BorderStyle.SOLID_THICK)
    }
}

// Returns a new cell containing the winnin player from the previous match.
// @param previousMatch {player1Cell, player2Cell, player1ScoreCell, player2ScoreCell, winnerCell}
// @param options {dummyPlayerName}
function addMatchWinner(previousMatch, options) {
    let player1Location = previousMatch.player1Cell.getA1Notation()
    let player2Location = previousMatch.player2Cell.getA1Notation()
    let player1ScoreLocation = previousMatch.player1ScoreCell.getA1Notation()
    let player2ScoreLocation = previousMatch.player2ScoreCell.getA1Notation()


    let isPlayer1WinFormula = `${player1ScoreLocation}>${player2ScoreLocation}`
    let isPlayer2WinFormula = `${player2ScoreLocation}>${player1ScoreLocation}`
    let winnerByScoreFormula = `if(${isPlayer1WinFormula}, ${player1Location}, if(${isPlayer2WinFormula}, ${player2Location}, ""))`
    let winnerFormula = `=SWITCH("${options.dummyPlayerName}",${player2Location}, ${player1Location}, ${player1Location}, ${player2Location}, ${winnerByScoreFormula})`

    let winnerCell = previousMatch.winnerCell.setFormula(winnerFormula)
    return winnerCell
}


function getDeadlineValue(match1, match2, options) {
    if (!options.deadlineOptions || !match1.deadlineCell || !match2.deadlineCell) {
        return undefined
    }

    let deadline1Location = match1.deadlineCell.getA1Notation()
    let deadline2Location = match2.deadlineCell.getA1Notation()
    let spreadsheetDelta = options.deadlineOptions.deadlineDeltaDays * SPREADSHEET_DAY_TIMESTAMP
    return `=max(${deadline1Location}, ${deadline2Location}) + ${spreadsheetDelta}`
}


function fillPlayers(playersArray, options) {
    let count = playersArray.length
    let numberOfMatches = Math.ceil(Math.log2(count))
    let numberOfTotalPlayers = Math.pow(2, numberOfMatches)
    let numberOfDummyPlayers = numberOfTotalPlayers - count

    // Put one dummy after each participant. this should even out the scatterting (alternative: one can just jump to the last game).
    let result = []
    for (i = 0; i < count; i++) {
        result.push(playersArray[i])
        if (i < numberOfDummyPlayers) {
            result.push(options.dummyPlayerName)
        }
    }

    return result
}


// copied from https://stackoverflow.com/questions/1573053/javascript-function-to-convert-color-names-to-hex-codes.
function colourNameToHex(colour) {
    var colours = {
        "aliceblue": "#f0f8ff", "antiquewhite": "#faebd7", "aqua": "#00ffff", "aquamarine": "#7fffd4", "azure": "#f0ffff",
        "beige": "#f5f5dc", "bisque": "#ffe4c4", "black": "#000000", "blanchedalmond": "#ffebcd", "blue": "#0000ff", "blueviolet": "#8a2be2", "brown": "#a52a2a", "burlywood": "#deb887",
        "cadetblue": "#5f9ea0", "chartreuse": "#7fff00", "chocolate": "#d2691e", "coral": "#ff7f50", "cornflowerblue": "#6495ed", "cornsilk": "#fff8dc", "crimson": "#dc143c", "cyan": "#00ffff",
        "darkblue": "#00008b", "darkcyan": "#008b8b", "darkgoldenrod": "#b8860b", "darkgray": "#a9a9a9", "darkgreen": "#006400", "darkkhaki": "#bdb76b", "darkmagenta": "#8b008b", "darkolivegreen": "#556b2f",
        "darkorange": "#ff8c00", "darkorchid": "#9932cc", "darkred": "#8b0000", "darksalmon": "#e9967a", "darkseagreen": "#8fbc8f", "darkslateblue": "#483d8b", "darkslategray": "#2f4f4f", "darkturquoise": "#00ced1",
        "darkviolet": "#9400d3", "deeppink": "#ff1493", "deepskyblue": "#00bfff", "dimgray": "#696969", "dodgerblue": "#1e90ff",
        "firebrick": "#b22222", "floralwhite": "#fffaf0", "forestgreen": "#228b22", "fuchsia": "#ff00ff",
        "gainsboro": "#dcdcdc", "ghostwhite": "#f8f8ff", "gold": "#ffd700", "goldenrod": "#daa520", "gray": "#808080", "green": "#008000", "greenyellow": "#adff2f",
        "honeydew": "#f0fff0", "hotpink": "#ff69b4",
        "indianred ": "#cd5c5c", "indigo": "#4b0082", "ivory": "#fffff0", "khaki": "#f0e68c",
        "lavender": "#e6e6fa", "lavenderblush": "#fff0f5", "lawngreen": "#7cfc00", "lemonchiffon": "#fffacd", "lightblue": "#add8e6", "lightcoral": "#f08080", "lightcyan": "#e0ffff", "lightgoldenrodyellow": "#fafad2",
        "lightgrey": "#d3d3d3", "lightgreen": "#90ee90", "lightpink": "#ffb6c1", "lightsalmon": "#ffa07a", "lightseagreen": "#20b2aa", "lightskyblue": "#87cefa", "lightslategray": "#778899", "lightsteelblue": "#b0c4de",
        "lightyellow": "#ffffe0", "lime": "#00ff00", "limegreen": "#32cd32", "linen": "#faf0e6",
        "magenta": "#ff00ff", "maroon": "#800000", "mediumaquamarine": "#66cdaa", "mediumblue": "#0000cd", "mediumorchid": "#ba55d3", "mediumpurple": "#9370d8", "mediumseagreen": "#3cb371", "mediumslateblue": "#7b68ee",
        "mediumspringgreen": "#00fa9a", "mediumturquoise": "#48d1cc", "mediumvioletred": "#c71585", "midnightblue": "#191970", "mintcream": "#f5fffa", "mistyrose": "#ffe4e1", "moccasin": "#ffe4b5",
        "navajowhite": "#ffdead", "navy": "#000080",
        "oldlace": "#fdf5e6", "olive": "#808000", "olivedrab": "#6b8e23", "orange": "#ffa500", "orangered": "#ff4500", "orchid": "#da70d6",
        "palegoldenrod": "#eee8aa", "palegreen": "#98fb98", "paleturquoise": "#afeeee", "palevioletred": "#d87093", "papayawhip": "#ffefd5", "peachpuff": "#ffdab9", "peru": "#cd853f", "pink": "#ffc0cb", "plum": "#dda0dd", "powderblue": "#b0e0e6", "purple": "#800080",
        "rebeccapurple": "#663399", "red": "#ff0000", "rosybrown": "#bc8f8f", "royalblue": "#4169e1",
        "saddlebrown": "#8b4513", "salmon": "#fa8072", "sandybrown": "#f4a460", "seagreen": "#2e8b57", "seashell": "#fff5ee", "sienna": "#a0522d", "silver": "#c0c0c0", "skyblue": "#87ceeb", "slateblue": "#6a5acd", "slategray": "#708090", "snow": "#fffafa", "springgreen": "#00ff7f", "steelblue": "#4682b4",
        "tan": "#d2b48c", "teal": "#008080", "thistle": "#d8bfd8", "tomato": "#ff6347", "turquoise": "#40e0d0",
        "violet": "#ee82ee",
        "wheat": "#f5deb3", "white": "#ffffff", "whitesmoke": "#f5f5f5",
        "yellow": "#ffff00", "yellowgreen": "#9acd32"
    };

    if (typeof colours[colour.toLowerCase()] != 'undefined')
        return colours[colour.toLowerCase()];

    return false;
}
