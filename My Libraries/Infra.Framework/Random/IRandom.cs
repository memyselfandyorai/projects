﻿namespace Infra.Framework.Random
{
    public interface IRandom
    {
        int GetNextInt(int min, int max);
    }
}
