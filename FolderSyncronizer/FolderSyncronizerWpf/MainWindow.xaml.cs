﻿using FolderSyncronizer.Debugging;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace FolderSyncronizerWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowsController _controller;

        public MainWindow()
        {
            InitializeComponent();
            // Next:
            // 1. Add tests (create folder for test or something - is there ready method?) - scenarios of all. Each action separately, then actions which affect each other. Also, for each such things, make sure the previous folder+action lists are the same as loaded after save.
            // Remember: Folders inside folder (2 folders, one inside)
            // 2. Support directory rename and delete (for all actions). If sourceTo is directory, then replace it if contains (ignore case). - Path.Conains? Contains(x+"\")?
        }

        private void PathText_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                if(files.Length > 0 && Directory.Exists(files[0]))
                {
                    ((TextBox)e.Source).Text = files[0];
                }
            }

        }

        private void PathText_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

        private void StartSyncButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _controller.Start();
            }
            catch (Exception exc)
            {
                ShowError("Failed starting: ", exc);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _controller = new MainWindowsController(Dispatcher.Invoke, StartSyncButton, StopSyncButton, SourcePathText, TargetPathText, AddButton, RemoveButton, LoadButton);
            this.DataContext = _controller;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                Singleton.Logger.LogDebug("Disposing controller");
                _controller.Dispose();
                Singleton.Logger.LogDebug("Disposed controller");
            }
            catch (Exception exc)
            {
                Singleton.Logger.LogError("Failed disposing controller", exc);
            }
            
        }

        private void StopSyncButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _controller.Stop();
            }
            catch (Exception exc)
            {
                ShowError("Failed stopping: ", exc);
            }
        }

        private async void PerfomActions_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_controller.ContainsVideos())
                {
                    var result = MessageBox.Show("Are you sure? There are video files present.", "Forgot something?", MessageBoxButton.YesNo);
                    if (result != MessageBoxResult.Yes)
                    {
                        _controller.SourceFileFilterRegex = _controller.ImageRegex;
                        _controller.FilterFilesByRegex = true;
                        return;
                    }
                }
                await _controller.PerformActions();
            }
            catch (Exception exc)
            {
                ShowError("Failed performing actions: ", exc);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _controller.AddSourceFolders(SourcePathText.Text, TargetPathText.Text);
            }
            catch (Exception exc)
            {
                ShowError("Failed adding folders: ", exc);
            }
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (SourceFoldersListBox.SelectedItem != null)
            {
                try
                {
                    _controller.RemoveSourceFolders(SourceFoldersListBox.SelectedIndex);

                }
                catch (Exception exc)
                {
                    ShowError($"Failed removing '{SourceFoldersListBox.SelectedItem}': ", exc);
                }
            }
        }

        private void ShowError(string message, Exception error)
        {
            MessageBox.Show(message + error.Message);
            MessageBox.Show("Details: " + error);
            Singleton.Logger.LogError(message, error);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "sync";
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    _controller.Save(saveFileDialog.FileName);

                }
                catch (Exception exc)
                {
                    ShowError($"Failed saving to '{saveFileDialog.FileName}': ", exc);
                }
            }
        }
        
        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = "sync";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    _controller.Load(openFileDialog.FileName);

                }
                catch (Exception exc)
                {
                    ShowError($"Failed loading '{openFileDialog.FileName}': ", exc);
                }

                var result = MessageBox.Show("Do you want to start sync? Remember: Syncing over 1000 files in the same time takes a lot of time.", "Forgot something?", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    StartSyncButton_Click(sender, e);
                }
            }
        }
    }
}
