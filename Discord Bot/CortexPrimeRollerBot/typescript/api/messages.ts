export interface OutputMessage {
  text: string;
}

export interface MessageOptions {
  /**it's possible this only works for actual-real messages, and not commands. */
  reply_to_message_id?: string;
  /**
   * embed object ({title, type, color, ... })
   * probably like this: https://discord-api-types.dev/api/discord-api-types-v10/interface/APIEmbed.
   * passes as-is to the API.
   */
  embed?: any;

  // Additional data, copied as-is.
  extra_data?: any;
}

export interface MessageSender {
  // Returns message IDs
  send(messages: OutputMessage, options: MessageOptions?): Promise<string>;
}

export class NoPermissionsError extends Error {
  constructor(...params: any[]) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
    }
    this.name = this.constructor.name;

    const permissions =
      BigInt(0x1) |
      BigInt(0x400) |
      BigInt(0x800) |
      BigInt(0x10000) |
      BigInt(0x4000000) |
      BigInt(0x400) |
      BigInt(0x80000000) |
      BigInt(0x2000);

    let link =
      `https://discord.com/oauth2/authorize?client_id=947923931615088682&scope=identify%20bot%20applications.commands&permissions=` +
      permissions;
    console.log("No permission: " + link);
  }
}
