export declare class CacheLimitationError extends Error {
    constructor(...params: any[]);
}
export interface StorageItem {
    key: string;
    value: any;
    ttlSeconds?: number;
    entryTime: number;
}
export interface DataStorage {
    cacheValue(key: string, value: any, ttlSeconds?: number): Promise<void>;
    getCacheValue(key: string, defaultValue?: any): Promise<any>;
    getCacheEntries(): Promise<StorageItem[]>;
    clearCacheKey(key: string): Promise<any>;
    flushState(): Promise<void>;
}
//# sourceMappingURL=storage.d.ts.map