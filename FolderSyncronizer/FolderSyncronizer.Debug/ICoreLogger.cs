﻿using System;

namespace FolderSyncronizer.Debugging
{
    public interface ICoreLogger
    {
        void LogDebug(string message);
        void LogWarning(string message, Exception exception = null);

        void LogError(string message, Exception exception = null);
    }
}
