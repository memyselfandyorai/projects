package General;

/**
 * 
 * The class represents a color (rgb)
 *
 */
public class Color {
	
	private double red,green,blue; // between 0 to 1
	
	private static final int SCALE=255;
	
	public Color(double r,double g,double b)
	{
		red=r;
		green=g;
		blue=b;
		
		clamp();
	}

	public Color()
	{
		//default in java
		//red=0;
		//green=0;
		//blue=0;
	}
	
	
	private double red() {
		return red;
	}
	
	public int getRed() {
		return (int)(red*SCALE);
	}
	

	public double green() {
		return green;
	}

	public int getGreen() {
		return (int)(green*SCALE);
	}
	
	public double blue() {
		return blue;
	}
	
	public int getBlue() {
		return (int)(blue*SCALE);
	}
	
	public static Color average(Color[][] colors,int n)
	{
		double red=0;
		double green=0;
		double blue=0;
		
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
			{
				red+=colors[i][j].red();
				green+=colors[i][j].green();
				blue+=colors[i][j].blue();
			}
		
		int N=n*n;
		return new Color(red/N,green/N,blue/N);
	}
	
	/**
	 * adds color c to this color
	 * */
	public void add(Color c)
	{
		red+=c.red;
		green+=c.green;
		blue+=c.blue;
		clamp();
	}
	
	
	/**
	 * clamps values to be from 0 to 1
	 * */
	private void clamp()
	{
		red=red>1?1 : red;
		red=red<0?0 : red;
		
		green=green>1?1 : green;
		green=green<0?0 : green;
		
		blue=blue>1?1 : blue;
		blue=blue<0?0 : blue;
	}
	
	
	/**
	 * returns a new color multiplier by scalar
	 * */
	public Color mult(double k)
	{
		return new Color(k*red,k*green,k*blue);
	}
	
	/**
	 * returns a new color multiplier by a color (per-term)
	 * */
	public Color mult(Color c)
	{
		return new Color(red*c.red,green*c.green,blue*c.blue);
	}
	
	public String toString()
	{
		return "("+getRed()+","+getGreen()+","+getBlue()+")";
	}
	
}
