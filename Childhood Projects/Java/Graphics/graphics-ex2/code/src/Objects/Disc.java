package Objects;

import General.Intersection;
import General.NormalVector;
import General.Plane;
import General.Point;
import General.Point2D;
import General.Ray;
import General.Scene;
import General.Vector;

/**
 * 
 * represents a disc
 *
 */
public class Disc extends Surface{

	private Point center;
	private NormalVector normal;
	private double radius;
	
	/**
	 * default constructor
	 */
	public Disc() {
	}
	
	public Disc(Point center, NormalVector normal, double radius) {
		this.center = center;
		this.normal = normal;
		this.radius = radius;
	}

	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public NormalVector getNormal() {
		return normal;
	}

	public void setNormal(NormalVector normal) {
		this.normal = normal;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Intersection intersect(Scene scene,Ray ray) 
	{
		Plane plane = new Plane (normal,center);
		Point intersectPoint = plane.hit(ray);
		
		if(intersectPoint==Plane.MISS)
			return Intersection.MISS;
		
		double distFromCenter = intersectPoint.distance(center);
		if (distFromCenter>radius)
			return Intersection.MISS;
		double dist = ray.getPoint().distance(intersectPoint);
				
		return new Intersection(dist, this, intersectPoint,ray,plane.getNormal(ray));
	}
		
	
	public boolean hasAllValues() {
		if (center==null || normal==null || radius<=0)
			return false;
		return true;
	}

	
	public Point2D parametrization(Point point) {
		NormalVector spanVector = NormalVector.subPoints(center, point);
		Point p1 = new Point(center.getX(),center.getY()-1,center.getZ());
		Vector yAxis = Vector.subPoints(center, p1);
		Vector relativeVec = normal.crossProduct(yAxis);
		if (relativeVec.isZero()){
			Point p2 = new Point (center.getX()-1,center.getY(),center.getZ());
			Vector xAxis = Vector.subPoints(center, p2);
			relativeVec = normal.crossProduct(xAxis);
		}
		relativeVec.normalize();
		double cosTheta = relativeVec.dotProduct(spanVector);	
		double theta = Math.acos(cosTheta);
		Vector helpVector = normal.crossProduct(relativeVec);
		if (helpVector.dotProduct(spanVector)<0)
			theta = 2*Math.PI - theta;
		Vector vec = Vector.subPoints(center, point);
		double v = theta/(2*Math.PI);
		double u = vec.length()/radius;
		return new Point2D(u,v);
	}
	
	public boolean onRight(Plane plane)
	{
		return plane.onRight(center);
	}
	
}
