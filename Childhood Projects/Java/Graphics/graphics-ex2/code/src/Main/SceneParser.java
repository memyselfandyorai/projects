package Main;

import org.eclipse.swt.widgets.Canvas;

import General.Camera;
import General.Color;
import General.NormalVector;
import General.Point;
import General.Scene;
import Lights.AreaLight;
import Lights.DirectedLight;
import Lights.Light;
import Lights.PointLight;
import Objects.Box;
import Objects.Cylinder;
import Objects.Disc;
import Objects.Portal;
import Objects.Rectangle;
import Objects.SecurityCamera;
import Objects.Sphere;
import Objects.Surface;

public class SceneParser extends Parser{
	
	private Scene scene;
	private Surface currentObject;
	private String currentName;
	private Camera camera;
	private Canvas canvas;
	private Light currentLight;
	private AreaLight currentAreaLight;
	
	public SceneParser(Scene s,Canvas c)
	{
		scene=s;
		canvas=c;
		camera=new Camera();
		
		scene.setCanvas(canvas);
	}
	

	// start a new object definition
	// return true if recognized
	public boolean addObject(String name) throws ParseException
	{
		System.out.println("OBJECT: " + name);
		
		currentName=name;
		
		//lights
		if(currentName.equals("light-directed"))
		{
			currentLight=new DirectedLight();
			return true;
		}
		if(currentName.equals("light-point"))
		{
			currentLight=new PointLight();
			return true;
		}
		if(currentName.equals("light-area"))
		{
			currentAreaLight=new AreaLight();
			return true;
		}
		
		
		//surfaces
		if(currentName.equals("rectangle"))
		{
			currentObject=new Rectangle();
			return true;
		}
		
		if(currentName.equals("disc"))
		{
			currentObject=new Disc();
			return true;
		}
		
		if(currentName.equals("sphere"))
		{
			currentObject=new Sphere();
			return true;
		}
		
		if(currentName.equals("box"))
		{
			currentObject=new Box();
			return true;
		}
		
		if(currentName.equals("cylinder"))
		{
			currentObject=new Cylinder();
			return true;
		}
		
		if(currentName.equals("portal"))
		{
			currentObject=new Portal();
			return true;
		}
		
		if(currentName.equals("security-camera"))
		{
			currentObject=new SecurityCamera();
			return true;
		}
		
		
		//scene, camera
		if(currentName.equals("camera")||currentName.equals("scene"))
			return true;
		
		return false;
	}
	
	// set a specific parameter for the current object
	// return true if recognized
	public boolean setParameter(String name, String[] args) throws ParseException
	{
		System.out.print("PARAM: " + name);
	    for (String s : args) 
	        System.out.print(", " + s);
	    System.out.println();
		
		
	    //scene
		if(currentName.equals("scene"))
		{
			return setSceneParameter(name,args);
		}
		
		//camera
		if(currentName.equals("camera"))
		{
			return setCameraParameter(name,args);
		}
		
		
		//lights
		if(currentName.equals("light-directed")||currentName.equals("light-point")||currentName.equals("light-area"))
		{
			return setLightParameter(name,args);
		}
		
		//primitives
		if(currentName.equals("rectangle")||currentName.equals("disc")||currentName.equals("sphere")||currentName.equals("box")||currentName.equals("cylinder")||currentName.equals("portal")||currentName.equals("security-camera"))
		{
			return setPrimitiveParameter(name,args); 
		}
		
		
	    return false;
	}
	
	// finish the parsing of the current object
	// here is the place to perform any validations over the parameters and
	// final initializations.
	public void commit() throws ParseException
	{
		if(!currentName.equals("scene")&&!currentName.equals("camera"))	
		{
			if(currentName.indexOf("light")==-1&&currentObject!=null)
			{
				if(!currentObject.hasAllValues())
					throw new ParseException("values missing from "+currentName+" settings");
				
				if(!currentObject.checkTexture())
					throw new ParseException("texture file is missing or wrong");
								
				if(currentName.equals("box"))
				{
					if(((Box)currentObject).coLinear())
						throw new ParseException("there are 3 points co-linear in box settings");
				}
				
				if(currentName.equals("rectangle"))
				{
					if(((Rectangle)currentObject).coLinear())
						throw new ParseException("points are co-linear in rectangle settings");
				}
				
				scene.add(currentObject);
			}
			else
			{
				if(!currentName.equals("light-area")&&currentLight!=null)
				{
					if(!currentLight.hasAllValues())
						throw new ParseException("values missing from "+currentName+" settings");
					scene.add(currentLight);
				}
				else
				{
					if(!currentAreaLight.hasAllValues())
						throw new ParseException("values missing from "+currentName+" settings");
					scene.add(currentAreaLight);
					
				}
			}
		}
		
		if(currentName.equals("camera"))
		{
			
			if(!camera.hasAllValues())
				throw new ParseException("values missing from camera settings");
			camera.commit(canvas);
			scene.setCamera(camera);
		}
		
		if(currentName.equals("scene"))
		{
			if(!scene.checkTexFile())
				throw new ParseException("wrong texture file in scene settings");
		}
		
		
	}
	
	public void startFile()
	{
		System.out.println("-start-");
	}

	public void endFile() throws ParseException
	{
		System.out.println("-end-");
	}
	
	public void reportError(String err)
	{
		System.err.println("parsing error: " + err);
	}
	
	
	
	
	
	
	
	
	/**
	 * sets a scene parameter
	 * */
	public boolean setSceneParameter(String name,String args[])
	{

		if(name.equals("background-col"))
		{

			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("Background-color format wrong in scene");
				return true;
			}

			scene.setBackColor(c);
			return true;
		}

		if(name.equals("background-tex"))
		{
			if(args.length!=1)
			{
				reportError("Background-texture format wrong in scene");
				return true;
			}

			scene.setBackTex(args[0]);
			return true;
		}

		if(name.equals("ambient-light"))
		{

			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("Ambient-light color format wrong in scene");
				return true;
			}

			scene.setAmbientLight(c);
			return true;
		}


		if(name.equals("super-samp-width"))
		{

			Integer it=Utilities.getIntegerTruncated(args);
			if(it==Utilities.WRONG_FORMAT)
			{
				reportError("Super-sample-width format wrong in scene");
				return true;
			}

			scene.setSuperSampleWidth(it);
			return true;
		}

		if(name.equals("use-acceleration"))
		{
			Integer i=Utilities.getInteger(args);
			if(i==Utilities.WRONG_FORMAT)
			{
				reportError("Acceleration format wrong in scene");
				return true;
			}

			if(i!=0&&i!=1)
			{
				reportError("Acceleration value wrong in scene");
				return true;
			}

			int iInt=i;

			scene.setAccelerate(iInt==1);
			return true;
		}


		return false;
	}
	
	
	
	/**
	 * sets a camera parameter
	 * */
	public boolean setCameraParameter(String name,String args[])
	{
		if(name.equals("eye"))
		{
			Point eye=Utilities.getPoint(args);
			if(eye==Utilities.WRONG_FORMAT)
			{
				reportError("Eye format wrong in camera");
				return true;
			}

			camera.setEye(eye);
			return true;
		}

		if(name.equals("direction"))
		{
			NormalVector dir=Utilities.getVector(args);
			if(dir==Utilities.WRONG_FORMAT)
			{
				reportError("Direction format wrong in camera");
				return true;
			}

			camera.setDirection(dir);
			return true;
		}

		if(name.equals("look-at"))
		{
			Point look=Utilities.getPoint(args);
			if(look==Utilities.WRONG_FORMAT)
			{
				reportError("Look-at-point format wrong in camera");
				return true;
			}

			camera.setLookAt(look);
			return true;
		}

		if(name.equals("up-direction"))
		{
			NormalVector up=Utilities.getVector(args);
			if(up==Utilities.WRONG_FORMAT)
			{
				reportError("Up-direction format wrong in camera");
				return true;
			}

			camera.setUpDirection(up);
			return true;
		}


		if(name.equals("screen-dist"))
		{
			Double d=Utilities.getDouble(args);
			if(d==Utilities.WRONG_FORMAT)
			{
				reportError("Screen-distance format wrong in scene");
				return true;
			}

			camera.setScreenDist(d);
			return true;
		}

		if(name.equals("screen-width"))
		{
			Double d=Utilities.getDouble(args);
			if(d==Utilities.WRONG_FORMAT)
			{
				reportError("Screen-width format wrong in scene");
				return true;
			}

			camera.setScreenWidth(d);
			return true;
		}
		
		
		return false;
	}
	

	/**
	 * sets a light parameter
	 * */
	public boolean setLightParameter(String name,String args[])
	{
		//general
		if(name.equals("color"))
		{

			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("Color format wrong in "+currentName);
				return true;
			}

			if(!currentName.equals("light-area"))
				currentLight.setColor(c);
			else
				currentAreaLight.setColor(c);
			return true;
		}
		
		//light-directed
		if(currentName.equals("light-directed"))
		{
			if(name.equals("direction"))
			{
				NormalVector dir=Utilities.getVector(args);
				if(dir==Utilities.WRONG_FORMAT)
				{
					reportError("Direction format wrong in light-directed");
					return true;
				}

				((DirectedLight)currentLight).setDirection(dir);
				
				return true;
			}
		}
		
		//light-point
		if(currentName.equals("light-point"))
		{
			if(name.equals("pos"))
			{
				Point pos=Utilities.getPoint(args);
				if(pos==Utilities.WRONG_FORMAT)
				{
					reportError("Pos format wrong in light-point");
					return true;
				}

				((PointLight)currentLight).setPos(pos);
				return true;
			}
			
			if(name.equals("attenuation"))
			{
				double[] att=Utilities.getNumbers(args);
				if(att==Utilities.WRONG_FORMAT)
				{
					reportError("Attenuation format wrong in light-point");
					return true;
				}
				double kc=att[PointLight.KC];
				double kl=att[PointLight.KL];
				double kq=att[PointLight.KQ];
				((PointLight)currentLight).setAttenuation(kc, kl, kq);
				return true;
			}
			
		}
		
		//light-area
		if(currentName.equals("light-area"))
		{
			
			if(name.equals("p0"))
			{
				Point p0=Utilities.getPoint(args);
				if(p0==Utilities.WRONG_FORMAT)
				{
					reportError("p0 format wrong in light-area");
					return true;
				}

				currentAreaLight.setP0(p0);
				return true;
			}
			
			if(name.equals("p1"))
			{
				Point p1=Utilities.getPoint(args);
				if(p1 ==Utilities.WRONG_FORMAT)
				{
					reportError("p1 format wrong in light-area");
					return true;
				}

				currentAreaLight.setP1(p1);
				return true;
			}
			
			if(name.equals("p2"))
			{
				Point p2=Utilities.getPoint(args);
				if(p2==Utilities.WRONG_FORMAT)
				{
					reportError("p2 format wrong in light-area");
					return true;
				}

				currentAreaLight.setP2(p2);
				return true;
			}
			
			if(name.equals("grid-width"))
			{
				Integer it=Utilities.getIntegerTruncated(args);
				if(it==Utilities.WRONG_FORMAT)
				{
					reportError("Grid-width format wrong in light-area");
					return true;
				}

				currentAreaLight.setWidth(it);
				return true;
			}
		}
		
		
		return false;
	}
	
	/**
	 * sets a primitive parameter
	 * */
	public boolean setPrimitiveParameter(String name,String args[])
	{
		
		//general
		if(name.equals("mtl-type"))
		{
			String type=Utilities.getString(args);
			if(type==Utilities.WRONG_FORMAT)
			{
				reportError("type format wrong in "+currentName);
				return true;
			}
			
			if(!type.equals("texture")&&!type.equals("flat")&&!type.equals("checkers"))
			{
				reportError("type name wrong in "+currentName);
				return true;
			}
			
			currentObject.setType(type);
			return true;
		}
		
		if(name.equals("mtl-diffuse"))
		{
			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("diffuse color format wrong in "+currentName);
				return true;
			}

			currentObject.setDiffuse(c);
			return true;
		}
		
		if(name.equals("mtl-specular"))
		{
			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("specular color format wrong in "+currentName);
				return true;
			}

			currentObject.setSpecular(c);
			return true;
		}
		
		if(name.equals("mtl-ambient"))
		{
			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("ambient color format wrong in "+currentName);
				return true;
			}

			currentObject.setAmbient(c);
			return true;
		}
		
		if(name.equals("mtl-emission"))
		{
			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("emission color format wrong in "+currentName);
				return true;
			}

			currentObject.setEmission(c);
			return true;
		}
		
		
		
		
		if(name.equals("mtl-shininess"))
		{

			Integer i=Utilities.getInteger(args);
			if(i==Utilities.WRONG_FORMAT)
			{
				reportError("shininess format wrong in "+currentName);
				return true;
			}

			currentObject.setShininess(i);
			return true;
		}
		
		
		if(name.equals("checkers-size"))
		{
			Double d=Utilities.getDouble(args);
			if(d==Utilities.WRONG_FORMAT)
			{
				reportError("checkers-size format wrong in "+currentName);
				return true;
			}

			currentObject.setSize(d);
			return true;
		}
		
		
		
		if(name.equals("checkers-diffuse1"))
		{
			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("checkers-diffuse1 color format wrong in "+currentName);
				return true;
			}

			currentObject.setDiffuse1(c);
			return true;
		}
		

		if(name.equals("checkers-diffuse2"))
		{
			Color c=Utilities.getColor(args);
			if(c==Utilities.WRONG_FORMAT)
			{
				reportError("checkers-diffuse2 color format wrong in "+currentName);
				return true;
			}

			currentObject.setDiffuse2(c);
			return true;
		}
		
		
		if(name.equals("texture"))
		{
			String tex=Utilities.getString(args);
			if(tex==Utilities.WRONG_FORMAT)
			{
				reportError("texture format wrong in "+currentName);
				return true;
			}
			
			currentObject.setTexture(tex);
			return true;
		}
		
		
		if(name.equals("reflectance"))
		{
			Double d=Utilities.getDouble(args);
			if(d==Utilities.WRONG_FORMAT)
			{
				reportError("reflectance format wrong in "+currentName);
				return true;
			}

			currentObject.setReflectance(d);
			return true;
		}
		
		
		//rectangle
		if(currentName.equals("rectangle"))
		{
			if(name.equals("p0"))
			{
				Point p0=Utilities.getPoint(args);
				if(p0==Utilities.WRONG_FORMAT)
				{
					reportError("p0 format wrong in rectangle");
					return true;
				}

				((Rectangle)currentObject).setP0(p0);
				return true;
			}
			
			if(name.equals("p1"))
			{
				Point p1=Utilities.getPoint(args);
				if(p1 ==Utilities.WRONG_FORMAT)
				{
					reportError("p1 format wrong in rectangle");
					return true;
				}

				((Rectangle)currentObject).setP1(p1);
				return true;
			}
			
			if(name.equals("p2"))
			{
				Point p2=Utilities.getPoint(args);
				if(p2==Utilities.WRONG_FORMAT)
				{
					reportError("p2 format wrong in rectangle");
					return true;
				}

				((Rectangle)currentObject).setP2(p2);
				return true;
			}
		}
		
		
		
		//disc
		if(currentName.equals("disc"))
		{
			
			if(name.equals("center"))
			{
				Point center=Utilities.getPoint(args);
				if(center==Utilities.WRONG_FORMAT)
				{
					reportError("Center format wrong in disc");
					return true;
				}

				((Disc)currentObject).setCenter(center);
				return true;
			}
			
			
			if(name.equals("normal"))
			{
				NormalVector norm=Utilities.getVector(args);
				if(norm==Utilities.WRONG_FORMAT)
				{
					reportError("Normal format wrong in disc");
					return true;
				}

				((Disc)currentObject).setNormal(norm);
				
				return true;
			}
			
			if(name.equals("radius"))
			{
				Double r=Utilities.getDouble(args);
				if(r==Utilities.WRONG_FORMAT)
				{
					reportError("Radius format wrong in disc");
					return true;
				}

				((Disc)currentObject).setRadius(r);
				return true;
			}
		}
		
		//sphere
		if(currentName.equals("sphere"))
		{
			
			if(name.equals("center"))
			{
				Point center=Utilities.getPoint(args);
				if(center==Utilities.WRONG_FORMAT)
				{
					reportError("Center format wrong in sphere");
					return true;
				}

				((Sphere)currentObject).setCenter(center);
				return true;
			}
			
					
			
			if(name.equals("radius"))
			{
				Double r=Utilities.getDouble(args);
				if(r==Utilities.WRONG_FORMAT)
				{
					reportError("Radius format wrong in sphere");
					return true;
				}

				((Sphere)currentObject).setRadius(r);
				return true;
			}
		}
				
		//box
		if(currentName.equals("box"))
		{
			if(name.equals("p0"))
			{
				Point p0=Utilities.getPoint(args);
				if(p0==Utilities.WRONG_FORMAT)
				{
					reportError("p0 format wrong in box");
					return true;
				}

				((Box)currentObject).setP0(p0);
				return true;
			}
			
			if(name.equals("p1"))
			{
				Point p1=Utilities.getPoint(args);
				if(p1 ==Utilities.WRONG_FORMAT)
				{
					reportError("p1 format wrong in box");
					return true;
				}

				((Box)currentObject).setP1(p1);
				return true;
			}
			
			if(name.equals("p2"))
			{
				Point p2=Utilities.getPoint(args);
				if(p2==Utilities.WRONG_FORMAT)
				{
					reportError("p2 format wrong in box");
					return true;
				}

				((Box)currentObject).setP2(p2);
				return true;
			}
			
			if(name.equals("p3"))
			{
				Point p3=Utilities.getPoint(args);
				if(p3==Utilities.WRONG_FORMAT)
				{
					reportError("p32 format wrong in box");
					return true;
				}

				((Box)currentObject).setP3(p3);
				return true;
			}
		}
		
		
		//cylinder
		if(currentName.equals("cylinder"))
		{
			
			if(name.equals("start"))
			{
				Point start=Utilities.getPoint(args);
				if(start==Utilities.WRONG_FORMAT)
				{
					reportError("Start point format wrong in cylinder");
					return true;
				}

				((Cylinder)currentObject).setStart(start);
				return true;
			}
			
			
			if(name.equals("direction"))
			{
				NormalVector dir=Utilities.getVector(args);
				if(dir==Utilities.WRONG_FORMAT)
				{
					reportError("Direction format wrong in cylinder");
					return true;
				}

				((Cylinder)currentObject).setDirection(dir);
				
				return true;
			}
			
			if(name.equals("length"))
			{
				Double l=Utilities.getDouble(args);
				if(l==Utilities.WRONG_FORMAT)
				{
					reportError("Length format wrong in cylinder");
					return true;
				}

				((Cylinder)currentObject).setLength(l);
				return true;
			}
			
			if(name.equals("radius"))
			{
				Double r=Utilities.getDouble(args);
				if(r==Utilities.WRONG_FORMAT)
				{
					reportError("Radius format wrong in cylinder");
					return true;
				}

				((Cylinder)currentObject).setRadius(r);
				return true;
			}
		}
		
		
		if(currentName.equals("portal"))
		{
			
				if(name.equals("center1"))
				{
					Point center=Utilities.getPoint(args);
					if(center==Utilities.WRONG_FORMAT)
					{
						reportError("Center1 format wrong in portal");
						return true;
					}

					((Portal)currentObject).setCenter1(center);
					return true;
				}
				
				
				if(name.equals("normal1"))
				{
					NormalVector norm=Utilities.getVector(args);
					if(norm==Utilities.WRONG_FORMAT)
					{
						reportError("Normal1 format wrong in portal");
						return true;
					}

					((Portal)currentObject).setNormal1(norm);
					
					return true;
				}
				
				if(name.equals("radius1"))
				{
					Double r=Utilities.getDouble(args);
					if(r==Utilities.WRONG_FORMAT)
					{
						reportError("Radius1 format wrong in portal");
						return true;
					}

					((Portal)currentObject).setRadius1(r);
					return true;
				}
				
				if(name.equals("center2"))
				{
					Point center=Utilities.getPoint(args);
					if(center==Utilities.WRONG_FORMAT)
					{
						reportError("Center2 format wrong in portal");
						return true;
					}

					((Portal)currentObject).setCenter2(center);
					return true;
				}
				
				
				if(name.equals("normal2"))
				{
					NormalVector norm=Utilities.getVector(args);
					if(norm==Utilities.WRONG_FORMAT)
					{
						reportError("Normal2 format wrong in portal");
						return true;
					}

					((Portal)currentObject).setNormal2(norm);
					
					return true;
				}
				
				if(name.equals("radius2"))
				{
					Double r=Utilities.getDouble(args);
					if(r==Utilities.WRONG_FORMAT)
					{
						reportError("Radius2 format wrong in portal");
						return true;
					}

					((Portal)currentObject).setRadius2(r);
					return true;
				}
		}
		
		
		if(currentName.equals("security-camera"))
		{
			
			if(name.equals("p0"))
			{
				Point p0=Utilities.getPoint(args);
				if(p0==Utilities.WRONG_FORMAT)
				{
					reportError("p0 format wrong in security-camera");
					return true;
				}

				((SecurityCamera)currentObject).setP0(p0);
				return true;
			}
			
			if(name.equals("p1"))
			{
				Point p1=Utilities.getPoint(args);
				if(p1 ==Utilities.WRONG_FORMAT)
				{
					reportError("p1 format wrong in security-camera");
					return true;
				}

				((SecurityCamera)currentObject).setP1(p1);
				return true;
			}
			
			if(name.equals("p2"))
			{
				Point p2=Utilities.getPoint(args);
				if(p2==Utilities.WRONG_FORMAT)
				{
					reportError("p2 format wrong in security-camera");
					return true;
				}

				((SecurityCamera)currentObject).setP2(p2);
				return true;
			}
			
			if(name.equals("view"))
			{
				Point view=Utilities.getPoint(args);
				if(view==Utilities.WRONG_FORMAT)
				{
					reportError("view format wrong in security-camera");
					return true;
				}

				((SecurityCamera)currentObject).setViewPoint(view);
				return true;
			}

		}
		
		return false;
	}
	
	
}
