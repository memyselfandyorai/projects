import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.Matrix;
import no.uib.cipr.matrix.Vector;
import no.uib.cipr.matrix.VectorEntry;
import no.uib.cipr.matrix.sparse.CompRowMatrix;
import no.uib.cipr.matrix.sparse.FlexCompRowMatrix;
import no.uib.cipr.matrix.sparse.GMRES;
import no.uib.cipr.matrix.sparse.ILU;
import no.uib.cipr.matrix.sparse.IterativeSolver;
import no.uib.cipr.matrix.sparse.IterativeSolverNotConvergedException;
import no.uib.cipr.matrix.sparse.OutputIterationReporter;
import no.uib.cipr.matrix.sparse.Preconditioner;

/**
 * The class represents a general solution for ex1
 */
public abstract class AbstractSolver {
	
		protected int rows,cols,N;
		
		/**
		 * constructor of 'AbstractSolver' class
		 * @param r - number of rows
		 * @param c - number of columns
		 */
		protected AbstractSolver(int r,int c)
		{
			rows=r;
			cols=c;
			N=rows*cols;
		}
		
		 
		/**
		 * returns the right converted index
		 */
		protected int index(int i,int j)
		{
			return i*cols+j;
		}
		
		/**
		 * returns the row index from 'ind'
		 */
		protected int iIndex(int ind)
		{
			return ind/cols;
		}
		
		/**
		 * returns the column index from 'ind'
		 */
		protected int jIndex(int ind)
		{
			return ind%cols;
		}
		
		/**
		 * returns the right number describing intensity of color, a value between [0..255]
		 */
		protected int clamp(double d)
		{
			int i=(int)d;
			if(i>255)
				return 255;
			if(i<0)
				return 0;
			
			return i;
		}
		
		/**
		 * sets the right value in vector b (when pixel is not a hole) 
		 */
		protected abstract void setBNotMissing(double[] b,int ind,ImageMatrix.RGB rgb);
		
		/**
		 * sets the right value in vector b (when pixel is a hole) 
		 */
		protected abstract void setBMissing(double[] b,int ind,ImageMatrix.RGB rgb);
		
		/**
		 * returns true iff coordinate (i,j) is a hole 
		 */
		protected abstract boolean isMissing(int i,int j);
		
		/**
		 * returns the result matrix
		 */
		public int[][] getResultMatrix()
		{
			Map<ImageMatrix.RGB,Vector> colors=new HashMap<ImageMatrix.RGB,Vector>();
			
			for(ImageMatrix.RGB rgb: ImageMatrix.RGB.values())
			{
				//new int arrays are allocated with a default value of 0
					
				Main.print("\ncolor "+rgb+":");
				FlexCompRowMatrix matrix=new FlexCompRowMatrix(N,N) ;
							
				double[] vec=new double[N]; 
				
				//fills matrix and vector with the right equations via mask and input
				fill(matrix,vec,rgb);
				
				Vector vector=new DenseVector(vec);
						
				//returns the vector of the complete image
				Vector result=solve(matrix,vector);
				
				colors.put(rgb,result);
			}
			
			int[][] result=new int[rows][cols];
			
			Iterator<VectorEntry> itRed=colors.get(ImageMatrix.RGB.RED).iterator();
			Iterator<VectorEntry> itBlue=colors.get(ImageMatrix.RGB.BLUE).iterator();
			Iterator<VectorEntry> itGreen=colors.get(ImageMatrix.RGB.GREEN).iterator();
			
			assert(colors.get(ImageMatrix.RGB.RED).size()==colors.get(ImageMatrix.RGB.BLUE).size());
			assert(colors.get(ImageMatrix.RGB.RED).size()==colors.get(ImageMatrix.RGB.GREEN).size());
			
			while(itRed.hasNext())
			{
				VectorEntry redEntry=itRed.next();
				VectorEntry blueEntry=itBlue.next();
				VectorEntry greenEntry=itGreen.next();
				double red=redEntry.get();
				double blue=blueEntry.get();
				double green=greenEntry.get();
				
				int index=redEntry.index();
				
				assert(index==blueEntry.index());
				assert(index==greenEntry.index());
				
				int r=clamp(red);
				int b=clamp(blue);
				int g=clamp(green);
				
				Color color=new Color(r,g,b);
				int i=iIndex(index);
				int j=jIndex(index);
				
				result[i][j]=color.getRGB();
			}
			
			return result;
		}
		
		
		/**
		 * solves the system Ax=b. returns vector x.
		 * @param A - matrix
		 * @param b - vector
		 */
		protected Vector solve(Matrix A,Vector b)
		{
			Vector x=new DenseVector(N);
			
			// Allocate storage for Conjugate Gradients
			IterativeSolver solver = new GMRES(x);
					
			// Create a Cholesky preconditioner
			Main.print("copying to CompRowMatrix...");
			CompRowMatrix B=new CompRowMatrix(A);
			Preconditioner M = new ILU(B);
			
			// Set up the preconditioner, and attach it
			Main.print("setting preconditioner...");
			M.setMatrix(A);
			solver.setPreconditioner(M);

			// Add a convergence monitor			
			solver.getIterationMonitor().setIterationReporter(new OutputIterationReporter());
			
			// Start the solver, and check for problems
			Main.print("solving...");
			try {
			  solver.solve(A, b, x); 
			} catch (IterativeSolverNotConvergedException e) {
			  Main.error("Iterative solver failed to converge");
			  return null;
			}
			
			Main.print("solved.");
			
			return x;
		}
		
		
		/**
		 * fills matrix 'A' and vector 'b' with the right values.
		 */
		protected void fill(Matrix A,double[] b,ImageMatrix.RGB rgb)
		{
			Main.print("filling matrix and vector for computation...");
			
			for(int i=0;i<rows;i++)
				for(int j=0;j<cols;j++)
				{
					int index=index(i,j);
					
					if(isMissing(i,j))
					{
						//b[index]=0;
						int weight=0;
						
						if(i>0)
						{
							int ind=index(i-1,j);
							A.set(index,ind,1);
							weight++;
						}
						if(i<rows-1)
						{
							int ind=index(i+1,j);
							A.set(index,ind,1);
							weight++;
						}
						
						if(j<cols-1)
						{
							int ind=index(i,j+1);
							A.set(index,ind,1);
							weight++;
						}
						if(j>0)
						{
							int ind=index(i,j-1);
							A.set(index,ind,1);
							weight++;
						}
						
						A.set(index,index,-weight);
						setBMissing(b,index,rgb);		
					}
					else
					{
						A.set(index,index,1);
						setBNotMissing(b,index,rgb);
					}
					
				}
			
		}
		

}
