import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


//if main stays, possible to start another game?

public class Server implements Runnable{


  private Socket socket;
  private BufferedWriter out;
  private BufferedReader in;
  static Vector<Socket> players;
  private Vector<String> names;
  private String line;
  private Main main;
  static JFrame frame;
  static Game game;
  static boolean playing=false;
  static boolean quiting=false;
  
  public Server(Socket socket,Vector<Socket> v,Vector<String> n,Main m,JFrame f){
    this.socket=socket;
  	players=v;
  	names=n;
  	main=m;
  	frame=f;
  	main.setS(this);
  }

  public void run(){
     try{
        
        out =new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        in = new BufferedReader(new InputStreamReader (socket.getInputStream()));
             
        players.add(socket); //instead of when created.
        
        if(playing)
        	main.kick(players.indexOf(socket));
        
        while (( line=in.readLine())!=null)
        {
           if (!line.equals("exit")&&!line.equals("quit")){
                       
            
            //out.write(...);
            //out.flush();
            int b;
   	   	  	   	    
            if(line.indexOf("NAME*")==0)
            {
            String s=line.substring(5);	//name
				boolean bb=true;
				
				for(int i=0;i<names.size();i++)
				if(s.equals((String)names.elementAt(i)))
				bb=false;
				
				names.add(players.indexOf(socket),s); //anyway- it is removed
				
				if(bb)
				{						
				sendNames();			       	
				main.set(names);
				}
				else
				{
				sendToSocket("*KICKEDN*");
				}
			}
            
            
            if(line.indexOf("CHAT*")==0)
            {
            sendMessage(line);
			}
            
            if(line.indexOf("PRIV*")==0)
            {
            String s=line.substring(5);
            
            b=s.indexOf(",");
            int ind=Integer.parseInt(s.substring(0,b));
   	   		s=s.substring(b+1);
            
            
            try{
         	Socket sock=(Socket)(players.elementAt(ind));
         	BufferedWriter out2 =new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
	     	out2.write("CHAT*"+s+"\n");
	     	out2.flush();
	     	}catch(IOException e){System.out.println(e);}
            
            sendToSocket("CHAT*"+s+"\n");
            
                
			}//priv
            
            
            
            
           if(line.indexOf("GAME*")==0)
	   	   {
	   	   	String s=line.substring(5);
	   	   	game.read(s);
	   	   	
	   	   	sendMessage(line);
	       }
            
           
           if(line.indexOf("MOVE*")==0)
	   	   {
	   	   	
	   	   	sendNessage(line);	   	
	   	   	
	   	   	String s=line.substring(5);
	   	   	Vector<String> nums=new Vector<String>();
	   	   	
	   	   	while((b=s.indexOf(","))!=-1)
	   	   	{
	   	   	nums.add(s.substring(0,b));
	   	   	s=s.substring(b+1);
	   	   	}
	   	   	
	   	   	String name=s;
	   	   	
	   	   	
	   	   	game.move(name,nums);
	   	   	
	       } 
           
           if(line.indexOf("MOVET*")==0)
	   	   {
	   	   	
	   	   	sendNessage(line);  	
	   	   	
	   	   	String s=line.substring(6);
	   	   	Vector<String> nums=new Vector<String>();
	   	   	
	   	   	while((b=s.indexOf(","))!=-1)
	   	   	{
	   	   	nums.add(s.substring(0,b));
	   	   	s=s.substring(b+1);
	   	   	}
	   	   	
	   	   	b=s.indexOf(";");
	   	   	String name=s.substring(0,b);
	   	   	s=s.substring(b+1);
	   	   	
	   	   	boolean sen=s.equals("true");
	   	   	
	   	   	game.movet(name,nums,sen);
	   	   	
	       } 
            
            
           if(line.indexOf("TAKE*")==0)
	   	   {
	   	   	
	   	   	sendNessage(line);	   	
	   	   	
	   	   	String s=line.substring(5);
	   	   	
	   	   	String name=s;
	   	   	
	   	   	game.take(name);
	   	   	
	       } 
           
           
           if(line.indexOf("TAKEN*")==0)
	   	   {
	   	   	
	   	   	sendNessage(line);	   	
	   	   	
	   	   	String s=line.substring(6);
	   	   	
	   	   	b=s.indexOf(",");
	   	   	int ind=Integer.parseInt(s.substring(0,b));
	   	   	s=s.substring(b+1);
	   	   	
	   	   	String name=s;
	   	   		   	   	
	   	   	game.taken(name,ind);
	       }  
            
            //out.write(line);
            //out.flush();
            
            
           }
           else{
            int ind=players.indexOf(socket);
           	players.remove(ind);
           	names.remove(ind);
           	main.set(names);
            //out.write("Disconnected.\n"); 
            //out.flush();
             break;
           }
       }
       
       players.remove(socket);
       out.write("Disconnected.\n"); 
       System.out.println("Disconnected.");
       out.flush();
      
        out.close();
        in.close();
        socket.close();
        
        if(quiting&&players.size()==0) //quiting
        {
        	System.out.println("Quiting...");
        	System.exit(0);
        }

    } catch (IOException e){
        e.printStackTrace();
      }
  }   
	
  public void sendTake(String name)
  {
  	 String s="TAKE*";	
  	 s+=name;
  	sendMessage(s);
  }
  
  
  public void sendTaken(int index,String name)
  {
  	 String s="TAKEN*";	
  	 s+=index+",";
  	 s+=name;
  	sendMessage(s);
  }
		
	public static boolean isNatural(String s)
	{
	if(s==null||s.equals(""))
	return false;
		
	char[] c=s.toCharArray();	
		
		for(int i=0;i<c.length;i++)
		if(c[i]<'0'||c[i]>'9')
		return false;
		
	return true;	
	}
	
	
	public static void playing(boolean p)
	{
		playing=p;
	}
	
	public void sendNames()
	{
	String s="NAMES*";
	for(int i=0;i<names.size();i++)	
	s+=(String)names.elementAt(i)+",";
	
	sendMessage(s);	
	}
	
	public void sendGame()
	{
	sendMessage("GAME*"+game.write());
	}
	
	public void sendBurn(String mes)
	{
	sendMessage("BURN*"+mes); //used to be Nessage
	}
	
	
	public void sendMissed(String name)
	{
	sendMessage("MISSED*"+name);
	}
	
	public void sendFinished(String name)
	{
	sendMessage("FINISHED*"+name);
	}
	
	public void finished(String lost)
	{
	sendMessage("FINISH*"+lost);
	main.setButtons(true);
	//frame.setVisible(true);
	}
	
	
	public static void sendMessage(final String line)
	{
		for(int i=0;i<players.size();i++)
        {
         try{
         	Socket sock=(Socket)(players.elementAt(i));
         	BufferedWriter out2 =new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
	     	out2.write(line+"\n");
	     	out2.flush();
	     	}catch(IOException e){System.out.println(e);}
	    }
	}
	
	public void sendNessage(final String line)
	{
		for(int i=0;i<players.size();i++)
        {
         try{
         	Socket sock=(Socket)(players.elementAt(i));
    			if(sock!=socket)
    			{     	
	         	BufferedWriter out2 =new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
		     	out2.write(line+"\n");
		     	out2.flush();
		     	}
	     	}catch(IOException e){System.out.println(e);}
	    }
	}
	
	
	
	public void stg(int cp) //start game
	{
	Vector<Player> pl=new Vector<Player>();
	for(int i=0;i<names.size();i++)
	{
		Player p=new Player((String)names.elementAt(i),true);
		pl.add(p);
	}
	
	for(int i=0;i<cp;i++)
	{
	String n="Computer "+(i+1);
	pl.add(new Player(n,false));
	}
	
	//frame.setVisible(false);
	playing=true;
	
	sendMessage("*ST*");
	
	game=new Game(new JFrame(),pl,this);
	
	//sendGame(); //bring back?
	
	game.start();
	
	} //stg
	
	
	public void sendMove(Vector nums, String name)
	 {
	 	 String s="MOVE*";	
	 	 for(int i=0;i<nums.size();i++)
	 	 {
	 	 s+=nums.elementAt(i)+",";
	 	 }
	 	
	 	s+=name;
	 	sendMessage(s);
	 }
	 
	 public void sendMoveT(Vector nums,boolean sen,String name)
	 {
	 	 String s="MOVET*";	
	 	 for(int i=0;i<nums.size();i++)
	 	 {
	 	 s+=nums.elementAt(i)+",";
	 	 }
	 	
	 	s+=name+";";
	 	s+=sen;
	 	sendMessage(s);
	 }
	
	
	
	
	public synchronized void sendToSocket(final String line)
	{
		try{
      	out.write(line+"\n");
    	out.flush();
     	}catch(IOException e){System.out.println(e);}
	}
	

  public static void main(String [] args){
	 
	  JFrame f=new JFrame("Host");	
	 	
  	  	
    String port=JOptionPane.showInputDialog(f,"Enter port:","4000");
  	
  	
  	if(!isNatural(port))
  	{
  		port="4000"; 
  		JOptionPane.showMessageDialog(f,"Default Port: 4000");
  	}
	
    ServerSocket ssock;
    Vector<Socket> pl=new Vector<Socket>(); //sockvector
    
    Vector<String> na=new Vector<String>();

    try{
      ssock = new ServerSocket(Integer.parseInt(port));

	  System.out.println("Online");
      
      Main main=new Main(na,pl);
      
      f.getContentPane().add(main);
      f.setSize(500,500);
      f.setVisible(true);   
      f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      f.addWindowListener(new WindowAdapter(){
    	  
    	  public void windowClosing(WindowEvent we)
    	  {
    		  //we.getWindow().setVisible(true);
    		  
    		  //return this
    	  if(!quiting)
    	  {
    		  //if(!playing&&game!=null)
    		  if(players!=null&&players.size()>0)
    		  {
    			  sendMessage("exit");
    			  try{
    				//  Thread.sleep(500);
    			  }catch(Exception e){}
    			  quiting=true;
    			  
    			  System.out.println("kicking players...");
    			
    		  }
    		  else
    		  System.exit(0); //bugs? yes. send exits.
    	  }
    	  } //windowsclose
      });
      
      while (true){
        Socket sock = ssock.accept();
        System.out.println("Connection accepted");
        //pl.add(sock); after map.
        Server st=new Server(sock,pl,na,main,f);
		Thread t=new Thread(st); 
        t.start();
        }
    } catch (IOException e){
        System.out.println("Could not connect.");
        JOptionPane.showMessageDialog(Server.frame,"Could not connect.");
        System.exit(1);        
     }

  }
}