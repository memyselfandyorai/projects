﻿using Infra.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FolderSyncronizer.Core.Watchers
{
    public class SubFolderFilter : IFolderFilter
    {
        private readonly Dictionary<string, List<string>> _folderToIgnoredSubfolders;
        public SubFolderFilter()
        {
            _folderToIgnoredSubfolders = new Dictionary<string, List<string>>(StringComparer.InvariantCultureIgnoreCase);
        }

        public void AddSourceFolder(string newFolder)
        {
            if(_folderToIgnoredSubfolders.ContainsKey(newFolder))
            {
                throw new ArgumentException("Can't use the same source folder twice: " + newFolder);
            }

            List<string> foldersToIgnore = new List<string>();
            foreach (var sourceFolder in _folderToIgnoredSubfolders.Keys)
            {
                if (sourceFolder.Contains(newFolder))
                {
                    // sourceFolder is subfolder of newFolder.
                    foldersToIgnore.Add(sourceFolder);
                } else if (newFolder.Contains(sourceFolder))
                {
                    // newFolder is subfolder of sourceFolder.
                    _folderToIgnoredSubfolders[sourceFolder].Add(newFolder);
                }
            }

            _folderToIgnoredSubfolders.Add(newFolder, foldersToIgnore);
        }

        public void Clear()
        {
            _folderToIgnoredSubfolders.Clear();
        }

        public bool ShouldHandleFile(string sourceFolderPath, string filePath)
        {
            if (!_folderToIgnoredSubfolders.ContainsKey(sourceFolderPath))
            {
                throw new ArgumentException($"Folder '{sourceFolderPath}' for file '{filePath} does not exist in dictionary. See keys: '{_folderToIgnoredSubfolders.Keys.JoinToString(", ")}'");
            }
            var excludedSubDirs = _folderToIgnoredSubfolders[sourceFolderPath];
            return !excludedSubDirs.Any(filePath.Contains);
        }
    }
}
