const controller = require("./controller.js");
const { ReplyToCommand, ReceivedCommand } = require("./commands.js");

/*
input: {
  action (POOL_...),
}
*/
async function modifyPools(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let inputOptions = {
    name: parsedInput.pool_name,
    names: parsedInput.pool_names,
    dice: parsedInput.dice ?? "",
    action: input.action,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  };

  let messageContent = await controller.RunSafe(() =>
    controller.ModifyDicePool(
      inputOptions,
      commandInteraction.getIocContainer(),
    ),
  );
  await replyToCommand(commandInteraction, messageContent, "Modify pool");
}

/*
input: {
  
}
*/
async function moveBetweenPools(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let dice = parsedInput.dice ?? "",
    source_pool,
    target_pool;

  let inputOptions = {
    sourceName: parsedInput.source_pool,
    targetName: parsedInput.target_pool,
    dice: parsedInput.dice ?? "",
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  };

  let messageContent = await controller.RunSafe(() =>
    controller.MoveDicePool(inputOptions, commandInteraction.getIocContainer()),
  );
  await replyToCommand(commandInteraction, messageContent, "Move pool");
}

/*
input: {
}
*/
async function stepPool(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let inputOptions = {
    name: parsedInput.pool_name,
    die: parsedInput.die ?? 0,
    steps: parsedInput.steps ?? 1,
    mod: parsedInput.mod ?? 0,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  };

  let messageContent = await controller.RunSafe(() =>
    controller.StepDicePool(inputOptions, commandInteraction.getIocContainer()),
  );
  await replyToCommand(commandInteraction, messageContent, "Step pool");
}

/*
input: {
}
*/
async function rollPool(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let inputOptions = {
    names: parsedInput.pool_names,
    dice: parsedInput.dice ?? "",
    keep: parsedInput.keep ?? 2,
    saveOptOut: parsedInput.save_opt_out ?? "",
    saveOptIn: parsedInput.save_opt_in ?? "",
    extraEffects: parsedInput.extra_effects ?? 0,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
    cacheKey: controller.FormatChannelKey(commandInteraction),
  };

  let messageContent = await controller.RunSafe(() =>
    controller.RollDicePool(inputOptions, commandInteraction.getIocContainer()),
  );
  await replyToCommand(commandInteraction, messageContent, "Roll pool");
}

/*
input: {
}
*/
async function showPools(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let inputOptions = {
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  };

  let messageContent = await controller.RunSafe(() =>
    controller.ShowDicePools(
      inputOptions,
      commandInteraction.getIocContainer(),
    ),
  );
  await replyToCommand(commandInteraction, messageContent, "Show pool");
}

async function replyToCommand(commandInteraction, messageContent, commandName) {
  console.log(commandName + " result: " + messageContent.join("\n"));
  await ReplyToCommand(commandInteraction, messageContent);
  await updateSummaryMessage(commandInteraction);
}

/* 
Returns input: {
  pool_name, pool_names, source_pool, target_pool
  die (number), dice,
  mod (number), steps (number),
  keep, save_opt_out, save_opt_in, extra_effects
  channel_id, user_id (number - defaults to 0)
  }
  
  Strings default to '', numbers default to undefined.
*/
function parseInput(commandInteraction) {
  var inputMap = commandInteraction.getInputOptionsMap();

  return {
    pool_name: inputMap.get("name") ?? "",
    source_pool: inputMap.get("from") ?? "",
    target_pool: inputMap.get("to") ?? "",
    dice: inputMap.get("dice") ?? "",
    pool_names: inputMap.get("names") ?? "",
    die: inputMap.get("die") ?? undefined,
    mod: inputMap.get("mod") ?? undefined,
    steps: inputMap.get("steps") ?? undefined,
    keep: inputMap.get("keep") ?? undefined,
    save_opt_out: inputMap.get("save-opt-out") ?? "",
    save_opt_in: inputMap.get("save-opt-in") ?? "",
    extra_effects: inputMap.get("extra-effects") ?? undefined,
    channel_id: commandInteraction.getChannelId() ?? "",
    user_id: commandInteraction.getUserId() ?? 0,
  };
}

async function updateSummaryMessage(commandInteraction) {
  let updateInput = {
    event: commandInteraction,
  };
  await controller.UpdatePinnedSummaryMessage(
    updateInput,
    commandInteraction.getIocContainer(),
  );
}

module.exports = {
  ShowPools: showPools,
  ModifyPools: modifyPools,
  MoveBetweenPools: moveBetweenPools,
  StepPool: stepPool,
  RollPool: rollPool,
};
