const controller = require("../../../../common/controller.js");
const {
  ReplyToCommand,
  ReceivedCommand,
} = require("../../../../common/commands.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let inputOptions = parseInput(commandInteraction);
  console.log("parse input: " + JSON.stringify(inputOptions));

  let messageContent = await controller.RunSafe(() =>
    controller.RerollCortexPrime(
      inputOptions,
      commandInteraction.getIocContainer(),
    ),
  );

  await ReplyToCommand(commandInteraction, messageContent);
}

// Returns input: {dice,step_up, step_up_mod, cache_key}
function parseInput(commandInteraction) {
  var inputMap = commandInteraction.getInputOptionsMap();

  return {
    dice: inputMap.get("dice"),
    step_up: inputMap.get("step-up"),
    step_up_mod: inputMap.get("step-up-mod"),
    cache_key: controller.FormatChannelKey(commandInteraction),
  };
}

// Create command:

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "reroll",
  "description": "Rerolls previous roll",
  "options": [
    {
      "type": 3,
      "name": "dice",
      "description": "Extra dice to add to reroll"
    },
    {
      "type": 4,
      "name": "step-up",
      "description": "A die type to step up"
    },
    {
      "type": 4,
      "name": "step-up-mod",
      "description": "Step-up modifier. Defaults to 1."
    }
  ]
});

*/
