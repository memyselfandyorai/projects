package General;
/**
 * The class represents a 2D point
 *
 */
public class Point2D {
	private double x0;
	private double y0;
	
	public Point2D(){};
	
	public Point2D(double x0,double y0){
		this.x0=x0;
		this.y0=y0;
		
	}
	
	public double getX() {
		return x0;
	}

	public double getY() {
		return y0;
	}

	public void setX(double x) {
		x0 = x;
	}

	public void setY(double y) {
		y0 = y;
	}
		
	
	public String toString()
	{
		return "("+x0+","+y0+")";
	}

		
	
}
