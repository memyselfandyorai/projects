﻿using System;
using SimpleInjector;
using Infra.Framework.Exceptions;

namespace Infra.Framework.Ioc
{
    public class SimpleInjectorIocContainer : IIocContainer
    {
        public static IIocContainer Create()
        {
            var container = new SimpleInjectorIocContainer();
            container.Register<IIocContainer>(container);
            return container;
        }

        private readonly Container _container;

        private SimpleInjectorIocContainer()
        {
            _container = new Container();
        }

        public void Register<TService, TConcrete>(Lifecycle lifecycle = Lifecycle.Singleton)
            where TService : class
            where TConcrete : class, TService
        {
            _container.Register<TService, TConcrete>(TransformLifestyle(lifecycle));
        }

        public void Register<TService>(TService instance)
            where TService : class
        {
            Register(() => instance, Lifecycle.Singleton);
        }

        public void Register<TService>(Func<TService> creator, Lifecycle lifecycle = Lifecycle.Singleton)
            where TService : class
        {
            _container.Register(creator, TransformLifestyle(lifecycle));
        }

        public TService Resolve<TService>() where TService : class
        {
            return _container.GetInstance<TService>();
        }

        private Lifestyle TransformLifestyle(Lifecycle lifecycle)
        {
            switch (lifecycle)
            {
                case Lifecycle.Singleton:
                    return Lifestyle.Singleton;
                case Lifecycle.Transient:
                    return Lifestyle.Transient;
                default:
                    throw new InvalidActionException("Lifecycle not mapped: " + lifecycle);
            }
        }

        public void Dispose()
        {
            _container.Dispose();
        }
    }

}