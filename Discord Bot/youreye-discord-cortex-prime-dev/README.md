# Cortex Prime roller bot

This is a bot meant to ease on playing Cortex Prime (RPG game) using Discord. Specifically, it supports a lot of useful features for rolling dice pools.


<img src="./readme/gallery/Original-bot.jpeg" alt="Bot's Avatar" height="150" height="20%">


## Installation

Use the [invite link](https://discord.com/oauth2/authorize?client_id=947923931615088682&scope=identify%20bot%20applications.commands&permissions=2214669313) for Discord.

I'm not sure which permissions are absolutely mandatory. I do know `Manage Messages` is necessary for creating a pinned message.

## Commands

Note: All commands execute on the channel level (and not guild-level, which is the server containing the channels).

`Dice format` (i.e., **dice formatted**) is a space-separated list of numbers, representing die types. A `3d10` format is supported as well. Example: `4 10 2d6 1D10`.

### Roll commands

#### /r

Rolls dice using the Cortex Prime method:
* Rolls dice.
* Each `1` counts as a "hitch".
* The score is using 2 non-hitched die results.
* The effect is using one of the remainder non-hitched die (its type).
* A result is built from both a numerical score and an effect which is a die type.
* There could be multiple results of interest: e.g., a lesser scope but higher effect.

Notes:
* Some dice can be of a special "pool" that cannot be hitched nor used for effect (makred with `[x]` in results).
* The command defaultly adds all saved dice created with `wildcard-mode`=`OPT_IN`. Using explicit saved names in `save-opt-out` or `save-opt-in` will change this behavior.

*Parameters:*
* `dice` (*required*) - **dice format**: `space-separated list of dice. Can put each die individually with repeats, or use a `3d10` format.
* `keep` (*default*: 2) - how many dice to use for score (these will be unused for effect).
* `effect-mod` (*default*: 0) - modifies the final effect by a number of steps. Examples: `1` - increases the effect by 1 (e.g., D6 -> D8), `-2` - decreases the effect by 2 (e.g., D8 -> D4).
* `hitch` (*default*: 1) - the number from which a roll counts as a hitch and is ignored from the result pool (for the score and effect).
* `die-mod` (*default*: 0) - modifies the input dice pool: Runs its absolute-value times. 
When positive - steps up the minimal die. When negative - steps down the max die. Example: for `2` die-mod and `2D4` it will step up D4 to D6 and then the other D4 to D6.
* `mod` (*default*: none) - automatically parameterizes special cases.
    * `SHOCK` - adds a D6, increases the effect, but steps down maximal die.
    * `OBLIVIOUS` - hitch is 2.
* `extra-effect-dice` (**dice formatted**) - additional dice to pool, while each one adds an additional effect-die to the result (e.g., effect used for multiple targets).
* `focus` (`**dice formatted**) - die types to merge (i.e., removes one and steps up the other). This runs after adding save/pool results. Example: using `6 6` on `5D6` will result in `1D6 2D8`.
* `save-opt-out` (*default*: empty) - command-separated saved dice names to disclude from roll. `*` is a wildcard for "all". Note wildcard is ignored for saved dice created with `wildcard-mode`=`OPT_OUT`.
* `save-opt-in` (*default*: `*`) - command-separated saved dice names to include in roll. `*` is a wildcard for "all". Note wildcard is ignored for saved dice created with `wildcard-mode`=`OPT_OUT`.
* `pools` (*default*: empty) - command-separated dice pool names to include in roll.


#### <a name="pool_roll">/pool-roll</a>

Rolls a specific pool. See [/r command](#r) for more details.

Note: The command does not add defaultly all saved dice created with `wildcard-mode`=`OPT_IN`. You can use saved names explicitly in `save-opt-out` or `save-opt-in` to change this behavior.


*Parameters:*
* `names` (*required*) - comma separated names of the pools to roll. Use `*` to roll all.
* `dice` (**dice formatted**) - extra dice to add to the roll.
* `keep` (*default*: 2) - how many dice to use for score (these will be unused for effect).
* `save-opt-out` (*default*: empty) - command-separated saved dice names to disclude from roll. `*` is a wildcard for "all". Note wildcard is ignored for saved dice created with `wildcard-mode`=`OPT_OUT`.
* `save-opt-in` (*default*: empty) - command-separated saved dice names to include in roll. `*` is a wildcard for "all". Note wildcard is ignored for saved dice created with `wildcard-mode`=`OPT_OUT`.
* `extra-effects` (*default*: 0) - how many extra dice (beyond the default `1`) to use as effect.


### Follow up commands

These commands are ran on the last roll a user performed, usually creating a new roll.

The last roll results are saved for up to ~10 minutes.

#### /append

When you already rolled, but need to add additional dice. This keeps the last rolled dice pool and its results, and adds additional dice.

This replaces the last saved roll.

*Parameters:*
* `dice` (*required*) - space-separated list of dice. Can put each die individually with repeats, or use a `3d10` format.

#### /keep

When you want to check if adding additional dice to score will help you (e.g., to figure out if you want to use a plot-point to increase the score).

This does not replace the last saved roll.

*Parameters:*
* `keep` (*default*: 3) - how many dice to take for the score.

#### /reroll

Rerolls the last roll (e.g., using some special ability), reusing its final dice-pool, `hitch`, `effect-mod` and `keep` settings. 

This replaces the last saved roll.

*Parameters:*
* `dice` (**dice formatted**) - extra dice to add to roll.
* `step-up` (*default*: none) - a die type to step up/down.
* `step-up-mod` (*default*: 1) - the number of steps to use for step up/down. If negative, it's a step down.


### Saved dice

#### /save

Saves extra dice to be used automatically by others in this channel. Overrides existing saved name if exists.

*Parameters:*
* `dice` (*required*, **dice formatted**) - extra dice that will be added to other rolls. By default, these will be added automatically to all `/r` rolls, but will not be added to `/pool-roll`.
* `name` (*required*) - unique name for this special bonus. Can be use to explicitly opt in or out from using it.
* `lifespan` (*required*) - for how long the bonus lasts. In any case, the bonus is saved for up to ~4 hours.
  * `SCENE` - saves dice for the whole session. You must delete it manually at the end of the scene.
  * `ROUND` - saves dice for a single use per user. Recommend deleting it manually when effect is done, especially when someone didn't get a chance to use it.
* `wildcard-mode` (*default*: OPT_IN) - mode of how to defaulty treat wildcards for these saved dice. Is this opted in by default, or not?
  * `OPT_IN` - defaulty added to `/r` rolls. Can be explicitly opted our. Not added to `/pool-roll`, but can be explicitly opted in. Using `*` for opt in or opt out includes this.
  * `OPT_OUT` - not added defaulty to `/r` nor `/pool-roll`. Should be explicitly opted in. Using `*` for opt in or opt out ignores this.

#### /save-delete

Deletes saved names. Should be used when they are no longer needed.

*Parameters:*
* `name` (*required*) - comma-separated list of names to delete. Use `*` to delete all saved names in this channel.

#### /save-show

Shows all saved names in this channel. Mentions explicitly if the user initiating the command already used up that save when its lifespan is `ROUND`.

### Pools

Stores a pool of dice by name. Can later be used to roll the pool, or augment another roll.

The main usage is the `doom pool`, but it can be utilized to store other attributes which consist of dice (such as *complications*).

#### /pool-add

Adds dice to an existing pool, or creates one with these dice if it doesn't exist. Pool can later be used to automatically roll the dice in the pool.

Note pool rolls are defaultly opted out from saved dice, but can be opted in manually or using `*` to include all.

Command should be mainly used by the Dungeon Master. Most common use case is the "doom pool". 

*Parameters:*
* `name` (*required*) - unique name of the pool.
* `dice` (*required*, **dice formatted**) - the dice to add to the pool.


#### /pool-remove

Removes dice from an existing pool.

*Parameters:*
* `name` (*required*) - name of the pool. 
* `dice` (*required*, **dice formatted**) - the dice to remove from the pool.

#### /pool-move

Moves dice from one pool to another. Will fail if source pool does not exist or does not contain the expected dice.

*Parameters:*
* `from` (*required*) - the source pool to move the dice from.
* `to` (*required*) - the target pool to move the dice to. 
* `dice` (*required*, **dice formatted**) - the dice to move.
    

#### /pool-clear

Removes a pool from the pool list.

*Parameters:*
* `names` (*required*) - comma separates list of pool names. Use `*` to delete all from channel.

#### /pool-step

Steps up or down a single die in a specific pool. 

Note this does not remove a stepped down D6, since the pool can be used to represent complications.

*Parameters:*
* `name` (*required*) - name of an existing pool.
* `die` (*required*) - the die to change.
* `mod` (*required*) - direction of the step.
  * `UP` - steps up.
  * `DOWN` - steps down.
* `steps` (*default*: 1) - number of steps.

#### /pool-show

Shows all existing pools in this channel.

#### /pool-roll

See [/pool-roll](#pool_roll).

### Counters

#### /counter-set

Sets a number for a player that can be easily increased or decreased (a.k.a. `counter`). Overrides value if the counter for the player in that channel already exists.

Main use cases: plot-points and experience points.

*Parameters:*
* `who` (*default*: the initiating user's nickname, or username if doesn't exist) - the name of the player.
* `name` (*default*: `pp`) - the name of the counter.
* `value` (*default*: 2) - the value to set the counter to.
* `type` (*default*: none) - a built in counter name. If used, replaces the `name` parameter.
  * `pp` - plot points.
  * `xp` - experiment points.



#### /counter-modify

Modifies a counter for a player in this channel. If the counter doesn't exit, creates it with the `amount` as its value.

*Parameters:*
* `who` (*default*: the initiating user's nickname, or username if doesn't exist) - the name of the player.
* `name` (*default*: `pp`) - the name of the counter.
* `amount` (*default*: 1) - a number to modify the existing counter by (can be negative). Used to increase or decrease the value.
* `type` (*default*: none) - a built in counter name. If used, replaces the `name` parameter.
  * `pp` - plot points.
  * `xp` - experiment points.


#### /counter-remove

Removes a counter from the list for a player in this channel. 

*Parameters:*
* `who` (*default*: the initiating user's nickname, or username if doesn't exist) - the name of the player.
* `name` (*default*: `pp`) - the name of the counter.
* `type` (*default*: none) - a built in counter name. If used, replaces the `name` parameter.
  * `pp` - plot points.
  * `xp` - experiment points.


#### /counter-show

Shows all existing counters for all players in this channel.

### General

#### /pin-summary

Pins a summary message to the channel. The message updates dynamically each time a relevant command is executed (even `show` commands).

It is recommended to recreate the message or run `/pool-show` at the start of each session to keep it updated. Reason: The saved dice are deleted with a command, and only commands update the message.

**Important**: `Manage Messages` bot permission is mandatory for this action.

#### /help

Adds a helpful description per needs

*Parameters:*
* `subject` (*required*) - the subject to need help with.
  * `general` - general info, linking to documentations.
  * `/r mods` - details about the ready mods for rolling.
  * `save lifespan` - details about how saved-dice lifespan works.

## Known limitations

### Scale and storage
The storage and cache layer supports up to 1024 keys.
If you get an error message `Contact developer for other storage solutions, or use the code to create your own Autocode discord bot`, this means the bot usage has grown beyond the intended scale.
There are multiple solutions:
1. Create your own bot in Autocode, take the code as-is, and use your own deployed bot. Note Each command needs to be registered, and there's a comment with the full script to register it. I used Autocode's "command builder" interface.
2. Contact the developer, and I can consider using other storage solutions. But I can't guarantee I'll have the resources (time/money) to spend on it.


### Rate limiting in Discord API
Sometimes there is a `You are being rate limited` error from Discord API. This can cause errors such as not updating the pinned message.

## Privacy agreement

By using this bot you acknowlede you agree with the privacy policy:
* There is no guarantee on any security mechanisms or privacy enforcements on user data such as user ID, username, user nickname, channel ID, or any other data (intentionally or not intentionally) transmitted to the Discord server.
* The data is stored as-is in server logs and storage.
* The unencrypted data is accessible to all maintainers of any of the used platforms and infras (such as Discord, Autocode, the bot developers, any backend used by Autocode, etc.), and as such is susceptible to data theft.

I can only guarantee that:
* The bot code does not abuse the data and does not have permissions to do anything harmful to the provided user or channel, and will not do anything on behalf of the user.
* Data will be used only for the purposes of the bot’s known functionality (i.e., commands).


## Legal

All rights reserved for Yorai Geffen for writing the bot's code. Thanks for Neria Khavkin for drawing the bot's avatar.

I give permission to reuse the code and enhance it for your own non-commercial reasons.

*Disclaimer*: I have no rights over Autocode (the infrastructure) nor Cortex Prime rollplaying game.

## For developers

If you're using the open source to modify this, note the following prefix-commands:
* `!admin` - using the admin user ID, helps to manage to storage.
* `!invite` - creates an invite link.
* `!rollc` - rolls the most basic version of dice rolling.
* `!test` - runs tests, making sure everything is in order.

### How to use this for your own bot?

Note: It might be as easy as "Insall app" on the [publish page](https://autocode.com/app/youreye/discord-cortex-prime/) - I haven't tried it.

1. Find a way to deploy a bot. Example: use Autocode (you'll have a bot running within 15 minutes).
2. Upload the code there. Note you'll need to attach handlers to each bot command.
3. Each command has a script to register the command in Discord. If you're using Autocode, they have a "command builder" tool that can register the commands. Unfortunately, it doesn't support using the commend text. But you can copy-paste all texts there (manual work).

### Links

* Published on Autocode: https://autocode.com/app/youreye/discord-cortex-prime/
* Google Site page: https://sites.google.com/view/youreye-communityprojects/cortex-prime-discord-bot
* Personal Git: https://bitbucket.org/memyselfandyorai/projects/src/master/Discord%20Bot/youreye-discord-cortex-prime-dev/

## Support

This was created by yorai.geffen@gmail.com.

I cannot guarantee I'll have the resources to fix issues or handle new feature requests. You can always reuse the code (**not** for commercial uses) to upload your own Autocode bot.

## Gallery

### Screenshot: Roll, append and keep

<img src="./readme/gallery/roll&append&keep.png" alt="Screenshot: Roll, append and keep">

### Screenshot: Counters, pools, pin

<img src="./readme/gallery/counter&pool&pin.png" alt="Screenshot: Counters, pools, pin">

### Screenshot: Save, roll and reroll

<img src="./readme/gallery/save-roll-reroll.png" alt="Screenshot: Save, roll and reroll">