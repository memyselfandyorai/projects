const { ShowPools } = require("../../../../common/poolsController.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  let input = {};
  await ShowPools(input, commandInteraction);
}

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "pool-show",
  "description": "Shows existing pools.",
  "options": []
});
*/
