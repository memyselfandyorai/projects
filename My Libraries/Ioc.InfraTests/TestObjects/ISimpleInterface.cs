﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioc.InfraTests.TestObjects
{
    public interface ISimpleInterface
    {
        int GetNumber(string name);

        int ID { get; }
        bool DisposeCalled { get; }
    }
}
