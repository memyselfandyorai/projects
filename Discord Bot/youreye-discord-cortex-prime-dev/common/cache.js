const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});


class CacheLimitationError extends Error {
  constructor(...params) {
      super(...params)
      if (Error.captureStackTrace) {
          Error.captureStackTrace(this, this.constructor);
      }
      this.name = this.constructor.name
  }
}

// void
// throws CacheLimitationError
async function cacheValue(key, value, ttlSeconds = 0) {  

  console.log(`Cache- Caching key: ${key} (ttls: ${ttlSeconds})`);
  console.log(`Cache- Caching value: ` + JSON.stringify(value));

  try {
    let setInput = {
     key: key,
     value: value,
    }
    if(ttlSeconds) {
      setInput.ttl = ttlSeconds
    }    
   await lib.utils.kv['@0.1.16'].set(setInput);  
  } catch (e) {
    if (e.message.includes("Max key-value pairs reached")) {     
      // "message": "Max key-value pairs reached. (utils/kv@0.1.16/set)",
      // "stack": "Error: Max key-value pairs reached. (utils/kv@0.1.16/set)\n at IncomingMessage.<anonymous> (/opt/node_modules/lib/lib/request.js:116:21)\n at IncomingMessage.emit (events.js:412:35)\n at endReadableNT (internal/streams/readable.js:1334:12)\n at processTicksAndRejections (internal/process/task_queues.js:82:21)"      
      // Print this since stacktrace disappears:
      console.warn(e)
      throw new CacheLimitationError("Reached cache storage limit", {cause: e})
    } else {
      throw e
    }
  }
}

async function getCacheValue(key, defaultValue = undefined) {
  let value = await lib.utils.kv['@0.1.16'].get({
    key: key,
    defaultValue: defaultValue,
  });
  console.log(`Cache- Getting key: ` + key);
  console.log(`Cache- Cached value: ` + JSON.stringify(value));
  return value;
}

async function getCacheEntries() {
  // Array size two: first key, second save value
  let entries = await lib.utils.kv['@0.1.16'].entries()
  console.log(`entries: ` + JSON.stringify(entries));
  return entries.map(kv => ({key: kv[0], value: kv[1]}))
}

async function clearCacheKey(key) {
  console.log(`Cache- Clearing key: ` + key);
  let value = await lib.utils.kv['@0.1.16'].clear({
    key: key,
  });

  return value;
}


module.exports = {
  CacheLimitationError: CacheLimitationError,
  CacheObject: {
    cacheValue: cacheValue,
    getCacheValue: getCacheValue,     
    getCacheEntries: getCacheEntries,     
    clearCacheKey: clearCacheKey,     
  },
};