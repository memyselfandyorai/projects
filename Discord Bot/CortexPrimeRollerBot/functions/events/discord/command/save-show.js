const { RunSaver } = require("../../../../common/saverController.js");
const { SHOW } = require("../../../../common/saver.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  await RunSaver(commandInteraction, (_) => SHOW);
}

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "save-show",
  "description": "Shows all saved names. Mentions explicitly if the user already used up that save.",
  "options": []
});
*/
