package General;

/**
 * The class represents a 3D point 
 * 
 *
 */
public class Point {
	
	private double x0,y0,z0;
	
	private static final double precision=0.001;
	
	
	public Point(double x0,double y0,double z0)
	{
		this.x0=x0;
		this.y0=y0;
		this.z0=z0;
	}
	
	
	public double getX()
	{
		return x0;
	}
	
	public double getY()
	{
		return y0;
	}
	
	public double getZ()
	{
		return z0;
	}
	
	public Vector toVector()
	{
		return new Vector(x0,y0,z0);
	}
	
	public Point add(Point p)
	{
		return new Point(x0+p.x0,y0+p.y0,z0+p.z0);
	}
	
	
	public double distance(Point p)
	{
		double x=x0-p.x0;
		double y=y0-p.y0;
		double z=z0-p.z0;
		return Math.sqrt(x*x+y*y+z*z);		
	}
	
	public static boolean coLinear(Point p1,Point p2,Point p3)
	{
		Vector v1=Vector.subPoints(p1, p2);
		Vector v2=Vector.subPoints(p1, p3);
		
		return v1.coLinear(v2);
	}
	
	
	/**
	 * returns true iff points are equal (by constant precision)
	 * */
	public boolean equals(Point p)
	{
		boolean x=Math.abs(x0-p.x0)<precision;
		boolean y=Math.abs(y0-p.y0)<precision;
		boolean z=Math.abs(z0-p.z0)<precision;
		return x&&y&&z;
	}
	
	public String toString()
	{
		return "("+x0+","+y0+","+z0+")";
	}
	
}
