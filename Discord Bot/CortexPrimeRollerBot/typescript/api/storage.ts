// Note: CacheLimitationError is obsolete, as we're no longer using Autocode's storage.
export class CacheLimitationError extends Error {
  constructor(...params: any[]) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
    }
    this.name = this.constructor.name;
  }
}

export interface StorageItem {
  key: string;
  value: any;
  // time to live in seconds.
  ttlSeconds?: number;
  // ticks (milliseconds) since 1970-01-01.
  entryTime: number;
}

export interface DataStorage {
  // Throws CacheLimitationError.
  cacheValue(key: string, value: any, ttlSeconds?: number): Promise<void>;
  getCacheValue(key: string, defaultValue?: any): Promise<any>;
  getCacheEntries(): Promise<StorageItem[]>;
  // returns the previous key.
  clearCacheKey(key: string): Promise<any>;

  // flushes data into storage, in case storing is lazy
  flushState(): Promise<void>;
}
