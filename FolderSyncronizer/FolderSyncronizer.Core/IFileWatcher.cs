﻿using System;

namespace FolderSyncronizer.Core
{
    public interface IFileWatcher
    {
        void Dispose();
    }
}