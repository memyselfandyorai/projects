﻿using FolderSyncronizer.Core.Watchers.Actions;
using PostSharp.Patterns.Model;

namespace FolderSyncronizer.Core.Watchers
{
    [NotifyPropertyChanged]
    public interface IFileAction
    {
        string SourceFile { get; }
        string TargetFile { get; }
        
        string Description { get; }

        void Apply();
        void OnNewAction(IFileAction action);

        bool IsCancelled { get; }
    }
}
