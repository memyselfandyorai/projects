"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDatabase = void 0;
class BaseDatabase {
    constructor(name, undelyingStorage) {
        Object.defineProperty(this, "undelyingStorage", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "name", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.undelyingStorage = undelyingStorage;
        this.name = name;
        // This is an ugly workaround for reasons beyond my understanding (this is no longer needed):
        /*
        this.cacheValue = this.cacheValue.bind(this);
        this.getCacheValue = this.getCacheValue.bind(this);
        this.getCacheEntries = this.getCacheEntries.bind(this);
        this.clearCacheKey = this.clearCacheKey.bind(this);
        this.getCachedItem = this.getCachedItem.bind(this);
        this.isExpired = this.isExpired.bind(this);
        */
    }
    async cacheValue(key, value, ttlSeconds) {
        const item = {
            key,
            value,
            ttlSeconds: ttlSeconds,
            entryTime: new Date().getTime(),
        };
        console.log(`${this.name}- Caching value: ` + JSON.stringify(item));
        await this.undelyingStorage.setItem(key, item);
    }
    async getCacheValue(key, defaultValue) {
        const storageItem = await this.getCachedItem(key);
        if (!storageItem) {
            return defaultValue;
        }
        return storageItem.value;
    }
    async getCacheEntries() {
        const allKeys = await this.undelyingStorage.getAllItemKeys();
        const items = await Promise.all(allKeys.map(async (key) => await this.getCachedItem(key)));
        return items.filter((x) => x !== undefined);
    }
    // returns the previous key.
    async clearCacheKey(key) {
        const item = await this.getCachedItem(key);
        if (item) {
            console.log(`${this.name}- Clearing key from input: ` + key);
            return await this.clearCachedItem(item);
        }
        console.log(`${this.name}- Not clearing key. Not found: ` + key);
        return undefined;
    }
    /** Returns the storage item or undefined if non existent or expired. If item is expired, deletes it. */
    async getCachedItem(key) {
        console.log(`${this.name}- Get cache key: ` + key);
        const item = await this.undelyingStorage.getItem(key);
        if (item) {
            console.log(`${this.name}- Got cache key value: ` + JSON.stringify(item));
            // ""&& item.key" is to make up for bugs when creating items without keys.
            if (!this.isExpired(item) && item.key) {
                return item;
            }
            else {
                await this.clearCachedItem(item);
            }
        }
        return undefined;
    }
    async clearCachedItem(item) {
        console.log(`${this.name}- Clearing key: ` + item.key);
        await this.undelyingStorage.deleteItem(item.key);
        return item.value;
    }
    async flushState() {
        if (this.undelyingStorage && "flush" in this.undelyingStorage) {
            const flushable = this.undelyingStorage;
            await flushable.flush();
        }
    }
    isExpired(item) {
        if (!item.ttlSeconds) {
            return false;
        }
        const expirationTimeMilliseconds = item.entryTime + item.ttlSeconds * 1000;
        const nowMilliseconds = new Date().getTime();
        return nowMilliseconds > expirationTimeMilliseconds;
    }
}
exports.BaseDatabase = BaseDatabase;
//# sourceMappingURL=base_storage.js.map