const {RunSaver}= require('../../../../common/saverController.js');
const {SCENE, SINGLE} = require('../../../../common/saver.js');

await RunSaver(context.params.event, parsedInput => {
  switch(parseInt(parsedInput.lifespan)) {
    case 1:
      return SCENE
    case 2:
      return SINGLE
  }
  
  return 0
})


// Command creation:
/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "save",
  "description": "Saves extra dice for to be used by others in this channel.",
  "options": [
    {
      "type": 3,
      "name": "dice",
      "description": "List of dice (normal format) to add.",
      "required": true
    },
    {
      "type": 3,
      "name": "name",
      "description": "Unique name for this special bonus.",
      "required": true
    },
    {
      "type": 4,
      "name": "lifespan",
      "description": "How long this save last (indefinitly / single usage). See \"/help subject: save lifespan\".",
      "choices": [
        {
          "name": "SCENE",
          "value": 1
        },
        {
          "name": "ROUND",
          "value": 2
        }
      ],
      "required": true
    },
    {
      "type": 4,
      "name": "wildcard-mode",
      "description": "Mode of how to treat wildcards. Defaulty opt in or opt out.",
      "choices": [
        {
          "name": "OPT_IN",
          "value": 1
        },
        {
          "name": "OPT_OUT",
          "value": -1
        }
      ]
    }
  ]
});
*/