"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InMemoryDatabase = void 0;
class InMemoryDatabase {
    constructor() {
        Object.defineProperty(this, "cacheMap", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.cacheMap = new Map();
    }
    async setItem(key, value) {
        this.cacheMap.set(key, value);
    }
    async getAllItemKeys() {
        return Array.from(this.cacheMap.keys());
    }
    async getItem(key) {
        const item = this.cacheMap.get(key);
        if (!item) {
            return item;
        }
        return cloneMemValue(item);
    }
    async deleteItem(key) {
        this.cacheMap.delete(key);
    }
}
exports.InMemoryDatabase = InMemoryDatabase;
function cloneMemValue(value) {
    if (value == undefined) {
        return value;
    }
    return JSON.parse(JSON.stringify(value));
}
//# sourceMappingURL=in_memory_storage.js.map