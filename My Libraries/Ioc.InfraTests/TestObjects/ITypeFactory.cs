﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioc.InfraTests.TestObjects
{
    public enum MyType
    {
        TypeA,
        TypeB,
        TypeC,
    }

    public interface IKeyedObject
    {
        public MyType Type { get; }
        public int Num{ get; }
    }


    public interface ITypeFactory
    {
        IKeyedObject GetByType(MyType type);
        IEnumerable<IKeyedObject> GetAll();

        bool DisposedCalled { get; }
        ISimpleInterface OptionalImplementation { get; }
    }
}
