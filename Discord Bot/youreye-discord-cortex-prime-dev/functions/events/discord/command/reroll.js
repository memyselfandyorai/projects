const controller= require('../../../../common/controller.js');
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});
const {ReplyToCommand, ReceivedCommand}= require('../../../../common/commands.js');

await ReceivedCommand(context.params.event)  

console.log('raw input data: ' + JSON.stringify(context.params.event.data))

let inputOptions = parseInput(context.params.event) 
console.log('parse input: ' + JSON.stringify(inputOptions))

let messageContent = await controller.RunSafe(() => controller.RerollCortexPrime(inputOptions, controller.DefaultIocContainer))

await ReplyToCommand(context.params.event, messageContent)  

// Returns input: {dice,step_up, step_up_mod, cache_key}
function parseInput(event) {
  let dataOptions = event.data.options
  
  var inputMap = new Map()
  dataOptions.forEach(option => {
    inputMap.set(option.name, option.value)
  })
  
  return  {
    dice: inputMap.get('dice'),
    step_up: inputMap.get('step-up'),
    step_up_mod: inputMap.get('step-up-mod'),
    cache_key: controller.FormatChannelKey(context.params.event),
  }
}


// Create command:

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "reroll",
  "description": "Rerolls previous roll",
  "options": [
    {
      "type": 3,
      "name": "dice",
      "description": "Extra dice to add to reroll"
    },
    {
      "type": 4,
      "name": "step-up",
      "description": "A die type to step up"
    },
    {
      "type": 4,
      "name": "step-up-mod",
      "description": "Step-up modifier. Defaults to 1."
    }
  ]
});

*/