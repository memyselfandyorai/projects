const Discord = require("discord.js");
const { Events, GatewayIntentBits, Collection } = require("discord.js");
const {
  NodeJsCommand,
  NodeJsPrefixCommandInteraction,
  NodeJsDiscordClientInteraction,
} = require("./generated/api.js");
const { StorageChooser } = require("./generated/storage/chooser");
const { IocContainer } = require("./generated/api/ioc_container.js");
const { DiscordStorage } = require("./generated/storage/discord_storage.js");
const fs = require("node:fs");
const path = require("node:path");

// Deployed on render.com, kept up with uptime robot.

// "Render spins down a Free web service that goes 15 minutes without receiving inbound traffic. Render spins the service back up whenever it next receives a request to process."

// Warn about "Your free instance will spin down with inactivity, which can delay requests by 50 seconds or more".... -> can happen?

// Future optimization: "Because you haven't used bot in this channel for 300 days, the bot is now discarding the game data in this channel. Here's what your game looked like:" (summary message).

const client = new Discord.Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
  ],
});
const prefixCommands = generatePrefixCommands();
const token = process.env["DISCORD_TOKEN"];
const adminId = process.env["ADMIN_ID"];
const storageType = process.env["STORAGE_TYPE"];
const storageChannelName = process.env["STORAGE_CHANNEL_NAME"];
const storageServerId = process.env["STORAGE_SERVER_ID"];

const iocContainer = new IocContainer();
const storageChooser = new StorageChooser(storageType);

client.on(Events.ClientReady, async (readyClient) => {
  console.log("I'm in");
  console.log(readyClient.user.username);
  console.log(`Logged in as: ${readyClient.user?.tag}`);
  const storage = await storageChooser.getStorage({
    channelName: storageChannelName,
    serverId: storageServerId,
    discordBotClient: readyClient,
  });
  iocContainer.setStorage(storage);
});

client.on(Events.GuildCreate, (guild) => {
  const clientInteration = new NodeJsDiscordClientInteraction(client, guild);
  clientInteration.sendJoinMessage();
});

client.on(Events.MessageCreate, async (msg) => {
  if (msg.author.id == client.user.id) {
    return;
  }

  let prefix = msg.content.split(" ")[0];
  prefix = prefix.startsWith("!") ? prefix.substring(1) : undefined;
  if (!prefix) {
    // Not a command.
    return;
  }
  if (!prefixCommands.has(prefix)) {
    console.warn("Prefix command not supported: " + prefix);
    return;
  }

  let prefixCommandInteraction = new NodeJsPrefixCommandInteraction(
    msg,
    adminId,
    iocContainer,
  );
  let commandModule = prefixCommands.get(prefix);
  await commandModule.execute(prefixCommandInteraction, msg.content);
});

client.on(Events.InteractionCreate, async (interaction) => {
  if (!interaction.isChatInputCommand()) {
    return;
  }
  const commandHandler = interaction.client.commands.get(
    interaction.commandName,
  );
  if (!commandHandler) {
    console.warn("Command not supported yet: " + interaction.commandName);
    return;
  }

  const command = new NodeJsCommand(interaction, iocContainer);
  await commandHandler.execute(command);
  /*
  const testItem = {
    keyList: ["POOL", "947924911836524545", "testP"],
    value: {
      obj: { dice: [6, 6] },
      timestampUtc: "Fri, 19 Apr 2024 20:03:58 GMT",
      version: "v1",
    },
  };
  const s = new DiscordStorage(interaction.client, interaction.guild);
  await s.writeKeyValue("test_key", testItem);
  await s.readAllFiles();
  */
});

client.commands = generateCommands();

function generateCommands() {
  const commands = new Collection();
  const commandMap = generateModules(getAllCommandFiles());
  for (const [name, command] of commandMap.entries()) {
    commands.set(name, command);
  }

  return commands;
}

function generatePrefixCommands() {
  const commandMap = generateModules(getAllPrefixCommandFiles());
  return commandMap;
}

// Returns a map between command name (filepath) to the module itself that contains an "execute" function.
function generateModules(commandPaths) {
  const commands = new Map();

  const commandFiles = commandPaths
    .filter((file) => file.endsWith(".js"))
    .map((path) => "./" + path);
  for (const filePath of commandFiles) {
    try {
      const fileName = path.parse(filePath).name;
      const command = require(filePath);
      // Set a new item in the Collection with the key as the command name and the value as the exported module
      if ("execute" in command) {
        commands.set(fileName, command);
      } else {
        console.log(
          `[WARNING] The command at ${filePath} is missing a required "execute" property.`,
        );
      }
    } catch (error) {
      if (error.code == "MODULE_NOT_FOUND") {
        console.warn(
          `File has a missing module (not migrated yet?): ${filePath}`,
          error,
        );
      } else {
        console.error(`Failed loading path ${filePath}`, error);
      }
    }
  }

  return commands;
}

function getAllCommandFiles() {
  return getAllFilesPaths("./functions/events/discord/command");
}

function getAllPrefixCommandFiles() {
  return getAllFilesPaths("./functions/events/discord/message/create/prefix");
}

function getAllFilesPaths(foldersPath) {
  const paths = [];
  const commandFolders = fs.readdirSync(foldersPath);

  for (const folder of commandFolders) {
    const commandsPath = path.join(foldersPath, folder);

    const fileStat = fs.statSync(commandsPath);
    if (!fileStat.isDirectory()) {
      paths.push(commandsPath);
    } else {
      const commandFiles = fs
        .readdirSync(commandsPath)
        .filter((file) => file.endsWith(".js"));
      for (const file of commandFiles) {
        const filePath = path.join(commandsPath, file);
        paths.push(filePath);
      }
    }
  }

  return paths;
}

client.login(token);

// https://discordjs.guide/creating-your-bot/command-handling.html#executing-commands
// Guide: https://www.codementor.io/@garethdwyer/building-a-discord-bot-with-node-js-and-repl-it-mm46r1u8y
/** NOT WORKING: "set up a third-party (free!) service like Uptime Robot. Uptime Robot pings your site every 5 minutes to make sure it's still working" */

// This is not working (Replit changed things, and it crashes after 15m always):
// This is important for render.com.
const keep_alive = require("./keep_alive.js");
// uptimerobot.com
// https://dashboard.uptimerobot.com/monitors/796708850
// https://fc18cf29-6769-4c39-8031-cb6d8dfdfc26-00-a9e78866pnxl.pike.repl.co/
