﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace FileExtensionFixer
{
    class Program
    {
        private const string DELETE_FLAG = "-d";
        private const string RECURSIVE_FLAG = "-r";
        private static readonly HashSet<string> DELETE_EXTENSIONS = new HashSet<string>() { ".aae" };
        private const double DELETE_MAX_SIZE_MB = 1.5 / 1024;

        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private extern static System.UInt32 FindMimeFromData(
            System.UInt32 pBC,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
            System.UInt32 cbSize,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
            System.UInt32 dwMimeFlags,
            out System.UInt32 ppwzMimeOut,
            System.UInt32 dwReserverd
        );

        public static string GetMimeFromFile(string filename)
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException(filename + " not found");

            byte[] buffer = new byte[256];
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                if (fs.Length >= 256)
                    fs.Read(buffer, 0, 256);
                else
                    fs.Read(buffer, 0, (int)fs.Length);
            }
            try
            {
                System.UInt32 mimetype;
                FindMimeFromData(0, null, buffer, 256, null, 0, out mimetype, 0);
                System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                string mime = Marshal.PtrToStringUni(mimeTypePtr);
                Marshal.FreeCoTaskMem(mimeTypePtr);
                return mime;
            }
            catch (Exception e)
            {
                return "unknown/unknown";
            }
        }


        private static List<Tuple<byte[], string>> _bytePrefixExtensionPairs = new()
        {
            // 255,216,255
            Tuple.Create(StringToByteArray("FFD8FF"), ".jpg"),
            Tuple.Create(new byte[] { 0,0,0,20}, ".mov"),
            Tuple.Create(new byte[] { 0, 0, 0, 24 }, ".mov"),
            Tuple.Create(new byte[] {63, 120,109, 60 }, ".aae"),
            Tuple.Create(new byte[] { 60,63,120,109 }, ".aae"),
            Tuple.Create(new byte[] { 137, 80, 78, 71 ,13 ,10 ,26 ,10 }, ".png"),
        };


        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }


        static void Main(string[] args)
        {
            string dir = args.FirstOrDefault(a => !a.StartsWith("-"));

            if (String.IsNullOrWhiteSpace(dir))
            {
                Console.WriteLine($"No input folder (use '{DELETE_FLAG}' to delete .aae files, and '{RECURSIVE_FLAG}' to run recursive)");
                return;
            }

            Console.WriteLine($"Running on {dir}...");

            var flags = new HashSet<string>(args.Where(a => a.StartsWith("-")));

            bool deleteFiles = flags.Contains(DELETE_FLAG);
            bool recursive = flags.Contains(RECURSIVE_FLAG);
            string flagStr = deleteFiles ? "on" : "off";
            string rFlagStr = recursive ? "on" : "off";
            Console.WriteLine($"Running on {dir}... (del: {flagStr}, rec: {rFlagStr})");

            /*foreach (var path in Directory.EnumerateFiles(dir).OrderBy(Path.GetExtension))
                Console.WriteLine($"{path}: bytes = { String.Join(",", File.ReadAllBytes(path).Take(20))}");*/

            System.IO.SearchOption options = recursive ? System.IO.SearchOption.AllDirectories : System.IO.SearchOption.TopDirectoryOnly;
            foreach (var path in Directory.EnumerateFiles(dir, "*", options))
            {
                string newExtension = null;

                var binary = File.ReadAllBytes(path);
                foreach (var tuple in _bytePrefixExtensionPairs)
                {
                    var prefix = tuple.Item1;
                    // var extension = tuple.Item2;

                    if(binary.Take(prefix.Length).SequenceEqual(prefix))
                    {
                        newExtension = tuple.Item2;
                        break;
                    }
                }

                // double sizeInMb = new FileInfo(path).Length / 1024.0 / 1024.0;
                string oldExtension = Path.GetExtension(path).ToLower();

                Console.WriteLine($"{path}: old = {oldExtension}, new = { newExtension}");

                if (newExtension != null && !oldExtension.Equals(newExtension))
                {
                    string newPath = path + newExtension;
                    Console.WriteLine($"Renamed to {newPath}");
                    File.Move(path, newPath);
                }
                else if (newExtension == null)
                {
                    Console.WriteLine($"{path}: bytes = { String.Join(",", binary.Take(20))}"); 
                }

                if(deleteFiles && DELETE_EXTENSIONS.Contains(oldExtension))
                {
                    double sizeInMb = binary.Length / 1024.0 / 1024.0;
                    if(sizeInMb < DELETE_MAX_SIZE_MB)
                    {
                        Console.WriteLine($"Deleting {path}: size = { sizeInMb }");
                        FileSystem.DeleteFile(path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                    }
                }

            }
            
            Console.WriteLine($"Done.");
        }
    }
}
