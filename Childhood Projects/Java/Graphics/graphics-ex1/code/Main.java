import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


/* run in cmd, for example:
 * java -Xmx1024m -jar graphics-ex1.jar -complete input.jpg mask.png output.png
 * or
 * java -Xmx1024m -jar graphics-ex1.jar -clone input.jpg clone.jpg mask.png 0 0 output.png
 * */

/**
 * main class of ex1
 */
public class Main {

	private static final String COMPLETE="-complete";
	private static final String CLONE="-clone";
	

	public static void print(Object s)
	{
		System.out.println(s);
	}
	

	public static void prin(Object s)
	{
		System.out.print(s);
	}
	

	public static void error(Object s)
	{
		System.err.println(s);
	}
	

	public static void exit()
	{
		System.exit(1);
	}
	
	/**
	 * if 'n' can be translated to a number, it returns it. otherwise returns -1 
	 * */
	public static int parse(String n)
	{
		try{
		int num=Integer.parseInt(n);
		return num;
		}catch(NumberFormatException e){}
		
		return -1;
	}
	
	/**
	 * writes a matrix to a file 
	 * */
	private static void outputImage(int[][] img,File file)
	{
		String n=file.getName();
		String formatName=n.substring(n.lastIndexOf(".")+1, n.length()); //.png or .jpg
		
		
		try{
		
		print("creating output file...");
			
		int rows=img.length;
		assert(rows>0);
		int cols=img[0].length;
		
		BufferedImage buffer=new BufferedImage(cols,rows, BufferedImage.TYPE_INT_ARGB);
		
		Main.print("filling buffer...");
		for(int i=0;i<rows;i++)
			for(int j=0;j<cols;j++)
			{
				buffer.setRGB(j,i, img[i][j]);  //(x,y) = (j,i)  [i:rows, x: cols]
			}
		
				
		ImageIO.write(buffer, formatName,file);
		
		}catch(IOException e){
			e.printStackTrace();
		}
		
		
		Main.print("finished!");
	}
	
	
		
	
	/**
	 * returns true iff file type is okay
	 * */
	private static boolean checkSuffixBoth(String input)
	{
		if (!input.endsWith(".png")&& !input.endsWith(".jpg"))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * returns true iff file type is okay
	 * */
	private static boolean checkSuffix(String input)
	{
		if (!input.endsWith(".png"))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * returns true iff file exists
	 * */
	private static boolean checkFile (File f)
	{
		if (!f.exists()){
			error("file does not exist : "+f.getName());
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * main function
	 * */
	public static void main(String args[])
	{
		
		if(args.length==4&&args[0].equals(COMPLETE))  //part I: completion
		{
			String input,mask,output;
			input=args[1];
			mask=args[2];
			output=args[3];
			
			
			if(!checkSuffixBoth(input))
			{
				error("Input should be .png or .jpg  : " +input);
				return;
			}
			
			if(!checkSuffix(mask))
			{
				error("Mask should be .png : " +mask);
				return;
			}
			
			if(!checkSuffix(output))
			{
				error("Output should be .png : " +output);
				return;
			}
			
			File fin = new File(input);
			if(!checkFile(fin))
				return;
			
			File fmask = new File(mask);
			if(!checkFile(fmask))
				return;
						
			ImageMatrix inMat=new ImageMatrix(fin);
			ImageMatrix maskMat=new ImageMatrix(fmask);
			
			
			if(inMat.getRows()!=maskMat.getRows())
			{
				error("row number of input image and mask image does not agree");
				return;
			}
			
			if(inMat.getCols()!=maskMat.getCols())
			{
				error("column number of input image and mask image does not agree");
				return;
			}
			
			
			
			Complete comp=new Complete(inMat,maskMat);
			
			int[][] completeMatrix=comp.getResultMatrix();
			
			File fout = new File(output);
			
			outputImage(completeMatrix,fout);
			
			
			return;
		}
		
		
		if(args.length==7&&args[0].equals(CLONE))  //part II: cloning 
		{
			String background,cloned,mask,output;
			int x0,y0;
			background=args[1];
			cloned=args[2];
			mask=args[3];
			output=args[6];
			x0=parse(args[4]);
			y0=parse(args[5]);
			if(x0==-1||y0==-1)
			{
				Main.error("wrong input armunets: not a number");
				return;
			}
			
			if(!checkSuffixBoth(background))
			{
				error("Background should be .png or .jpg  : " +background);
				return;
			}
			
			if(!checkSuffixBoth(cloned))
			{
				error("Cloned should be .png or .jpg  : " +cloned);
				return;
			}
			
			if(!checkSuffix(mask))
			{
				error("Mask should be .png : " +mask);
				return;
			}
			
			if(!checkSuffix(output))
			{
				error("Output should be .png : " +output);
				return;
			}
			
			
			
			File fback = new File(background);
			if(!checkFile(fback))
				return;
			
			File fclone = new File(cloned);
			if(!checkFile(fclone))
				return;
			
			File fmask = new File(mask);
			if(!checkFile(fmask))
				return;
						
			
			
			ImageMatrix backMat=new ImageMatrix(fback);
			ImageMatrix cloneMat=new ImageMatrix(fclone);
			ImageMatrix maskMat=new ImageMatrix(fmask);
			
			if(cloneMat.getRows()!=maskMat.getRows())
			{
				error("row number of cloned image and mask image does not agree");
				return;
			}
			
			if(cloneMat.getCols()!=maskMat.getCols())
			{
				error("column number of cloned image and mask image does not agree");
				return;
			}
			
			
			if(x0<0||y0<0||x0>=backMat.getCols()||y0>=backMat.getRows())
			{
				error("invalid point "+x0+","+y0);
			}
			
			
			Clone comp=new Clone(backMat,cloneMat,maskMat,x0,y0);
			
			int[][] cloneMatrix=comp.getResultMatrix();
			
			File fout = new File(output);
			
			outputImage(cloneMatrix,fout);
			
			
			return;
		}
		
		
		
		error("Wrong arguments");
		return;
		
	}
	
}
