package Lights;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import General.Color;
import General.Point;
import General.Ray;
import General.Vector;
/**
 * 
 * The class represents an area light
 *
 */
public class AreaLight implements Iterable<PointLight>{

	private Point p0,p1,p2;
	private int width;
	private Color color; // I L
	
	public AreaLight()
	{
		color=new Color(1,1,1);
	}
	
	public void setP0(Point p0) {
		this.p0 = p0;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}

	
	public void setWidth(int width) {
		this.width = width;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public boolean hasAllValues() {

		if(p0==null)
			return false;
		if(p1==null)
			return false;
		if(p2==null)
			return false;
		if(width==0)
			return false;
		
		return true;
	}
	
	
		
	public Iterator<PointLight> iterator()
	{
		Set<PointLight> set=new HashSet<PointLight>();
		
		int N=width;
		int N2=N*N; // N square
				
		for(int i=0;i<N;i++)
			for(int j=0;j<N;j++)
			{
				PointLight point=new PointLight();
				point.setAttenuation(1, 0, 0);
				point.setColor(color.mult(1.0/N2));
				point.setPos(getPoint(i,j));
				
				set.add(point);
			}
		
		//N^2 == set.size()
		
		return set.iterator();
	}
	
	
	/**
	 * 
	 * 
	 * 
	 * 
	 *  *can't commit if it will be (i,j) -> (x,y) or -> (y,x),
	 *  but it doesn't matter
	 * 
	 * */
	private Point getPoint(int i,int j)
	{
		int N=width;	
		Vector v1=Vector.subPoints(p1, p0);
		Vector v2=Vector.subPoints(p2, p0);
		
		//middle point of cell in grid
		double x=(2*i+1.0)/(2.0*N);
		double y=(2*j+1.0)/(2.0*N);

		
		//double x=(double)i/N;
		//double y=(double)j/N;
		
		
		Point px=Ray.getPoint(p0, v1,x);
		
		Point pxy=Ray.getPoint(px,v2,y);
		
		return pxy;
	}
	
}
