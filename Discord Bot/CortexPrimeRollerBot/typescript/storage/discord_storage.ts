import type {
    Client,
    Guild,
    ChatInputCommandInteraction,
    GuildMember,
    Message,
    MessageCreateOptions,
    DiscordAPIError,
    PartialTextBasedChannelFields,
    TextChannel,
    TextBasedChannel,
} from "discord.js";
import fetch from "node-fetch";
// https://discord.js.org/docs/packages/discord.js/14.14.1/TextChannel:Class
import { ChannelType, PermissionFlagsBits, OAuth2Scopes } from "discord.js";

import type { FlushableDataStorage, StorageInitParams } from "./base_storage";
import type { StorageItem } from "../api/storage";
import { Buffer } from "buffer"; // Import Buffer class
import LZString from "lz-string"; // Requires running "npm install lz-string", does render.com support it?

// npx tsc

// This should be 25 Mega.
const MAX_DISCORD_FILE_SIZE_BYTES = 1024 * 1024; // 1 MB
const DISCORD_SINGLE_FETCH_LIMIT = 100;

// TODO(urgent): Is this a single instance or not? check this. If it's not, I have an issue. - Added process ID logs.
// Check in logs how many times do I actually need to load the state. And if I have parallel instances.

// TODO: should I stress test this? Adding lots of things to the "pool" to verify I'm handling multiple messages properly? Can do it after launch.

export async function createDiscordDatabase(
    params: StorageInitParams,
): Promise<DiscordDatabase> {
    const client = params.discordBotClient;
    const myGuild = await client.guilds.fetch(params.serverId);
    var channel = await getChannelByName(myGuild, params.channelName);

    const discordStorage = new DiscordStorage(params.discordBotClient, channel);
    return new DiscordDatabase(discordStorage);
}

export class DiscordDatabase implements FlushableDataStorage {
    cacheMap: Map<string, StorageItem>;
    discordStorage: DiscordStorage;
    currentState?: StorageState | null;
    constructor(discordStorage: DiscordStorage) {
        this.cacheMap = new Map();
        this.discordStorage = discordStorage;
        this.currentState = null;
        console.log(`Creating Discord DB. Process = ${process.pid}`);
    }

    async getOrReadState(): Promise<StorageState> {
        if (!this.currentState) {
            this.currentState = await this.discordStorage.readExistingState();
        }

        return this.currentState;
    }

    async setItem(key: string, value: StorageItem): Promise<void> {
        const state = await this.getOrReadState();
        // Since we're storing it in-memory until the end of the program's life-cycle, these would keep the same object instance.
        // This causes "keep" to override previous results unintentionally. Thus, we duplicate the returned value.
        state.entries.set(key, cloneValue(value));
        state.hasChanged = true;
        // await this.discordStorage.writeState(state);
    }

    async getAllItemKeys(): Promise<string[]> {
        const state = await this.getOrReadState();
        return Array.from(state.entries.keys());
    }

    async getItem(key: string): Promise<StorageItem | null | undefined> {
        const state = await this.getOrReadState();
        const item = state.entries.get(key);
        if (!item) {
            return item;
        }
        return cloneValue(item);
    }

    async deleteItem(key: string): Promise<void> {
        const state = await this.getOrReadState();
        state.entries.delete(key);
        state.hasChanged = true;
        // await this.discordStorage.writeState(state);
    }

    async flush(): Promise<void> {
        if (this.currentState?.hasChanged) {
            await this.discordStorage.writeState(this.currentState);
        }
    }
}

interface KeyValuePair {
    key: string;
    value: any;
}

interface MsgFormat {
    index: number;
    fileContent: Uint8Array;
    messageId: string;
    timestamp: number;
    isLast: boolean;
}

interface StorageState {
    entries: Map<string, any>;
    existingMessageIds: string[];
    hasChanged: boolean;
    // TODO: consider adding timestamp, for "read" staleness
}

export class DiscordStorage {
    client: Client;
    channel: TextBasedChannel;
    constructor(client: Client, channel: TextBasedChannel) {
        this.client = client;
        this.channel = channel;
    }

    async readExistingState(): Promise<StorageState> {
        const channel = this.channel;

        logWithTime(`reading messages`);
        const botMessages = await getUserMessagesInChannel(
            channel,
            this.client.user?.id ?? "",
        );
        logWithTime(`read ${botMessages.length} messages`);

        const encryptedMessages: MsgFormat[] = [];
        for (const botMessage of botMessages) {
            const parsed = await parseFileMessage(botMessage);
            if (parsed !== undefined && parsed.fileContent) {
                encryptedMessages.push(parsed);
            }
        }

        if (encryptedMessages.length == 0) {
            return <StorageState>{
                entries: new Map<string, any>(),
                existingMessageIds: [],
                hasChanged: false,
            };
        }

        const sortedEncryptedMessages = getSortedRelatedMessages(encryptedMessages);

        const buffer = joinUint8Arrays(
            sortedEncryptedMessages.map((msg) => msg.fileContent),
        );

        const fullContent = LZString.decompressFromUint8Array(buffer);

        const pairs = JSON.parse(fullContent) as KeyValuePair[];
        const entries = new Map<string, any>(
            pairs.map((item) => [item.key, item.value]),
        );
        return <StorageState>{
            entries,
            existingMessageIds: encryptedMessages.map((msg) => msg.messageId),
            hasChanged: false,
        };
    }

    async writeState(storageState: StorageState): Promise<void> {
        const items: KeyValuePair[] = Array.from(
            storageState.entries.entries(),
        ).map((entry) => ({
            key: entry[0],
            value: entry[1],
        }));
        // Consider adding some cache layer over this, up to 10 seconds ttl?
        // will I have efficiency issues at all? will this work in parallel? --> I can change this to per channel later, maybe.
        // I can create an "ioc container" per channel. if not efficient, cache?
        const fullContent = LZString.compressToUint8Array(JSON.stringify(items));

        const messagesToSend = splitUint8Array(
            fullContent,
            MAX_DISCORD_FILE_SIZE_BYTES,
        );

        // Throttling issues when pinning messages. How many times can I do message "edits"?
        // need to edit messages. assuming won't overwrite due to (can get throttled, BTW? it's possible!!!) lock issues.
        // maybe I'll need a cache layer, writing once each x time.

        const newMessageIds = await this.writeMessagesAsFiles(
            messagesToSend,
            fullContent.length,
        );
        // State is the same as written.
        storageState.hasChanged = false;

        await this.deleteFileMessages(storageState);
        // Emptying message IDs so it won't retry indefinitely. E.g., if an ID is missing.
        // Next time we read messages, they won't exist anyway.
        storageState.existingMessageIds = newMessageIds;
    }

    async deleteFileMessages(storageState: StorageState): Promise<void> {
        try {
            const textChannel = this.channel as TextChannel;
            if (textChannel) {
                logWithTime(
                    `bulk deleting ${storageState.existingMessageIds.length} storage messages`,
                );

                // Consider editting existing messages (and deleting the redundant ones) if it's more efficient. If deleting fails, this should recover the next time it tries to write (as new messages, marked with "isLast", should exist).
                await textChannel.bulkDelete(storageState.existingMessageIds);
            } else {
                logWithTime(
                    `deleting ${storageState.existingMessageIds.length} storage messages`,
                );
                for (const msgId of storageState.existingMessageIds) {
                    await this.channel.messages.delete(msgId);
                }
            }

            // WARNING: If one fails in the per-msg deletion, then it would still remove all the existingMessageIds. Note when re-reading them it should resolve itself.
            logWithTime(`deleted storage messages`);
        } catch (error) {
            console.error(
                `Failed deleting existing messages. This should resolve itself out eventually.`,
                error,
            );
        }
    }

    async writeMessagesAsFiles(
        messagesToSend: Uint8Array[],
        fullContentLength: number,
    ): Promise<string[]> {
        const newMessageIds: string[] = [];

        logWithTime(
            `sending ${messagesToSend.length} storage messages, total size ${fullContentLength}`,
        );

        // I'm adding timestamp, hopefully this will help resolve issues if writing things at the same time. So no state will override the other. I won't get to a corrupted state, but it's possible we'll lose data.
        let i = 0;
        const timestamp = new Date().getTime();
        for (const binaryMessage of messagesToSend) {
            const isLast = i == messagesToSend.length - 1;
            const message = await this.channel.send({
                // To help sort this, and deal with deletion failures, and write in concurrency.
                content: `${i},${isLast},${timestamp}`,
                files: [Buffer.from(binaryMessage)],
            });
            newMessageIds.push(message.id);
            i++;
        }
        logWithTime(`done sending ${messagesToSend.length} storage messages`);

        return newMessageIds;
    }
}

async function parseFileMessage(
    message?: Message,
    text?: string,
): Promise<MsgFormat | undefined> {
    const contentText = message?.content ?? text ?? "";
    const content = contentText.split(",");
    const index = Number(content[0]);
    if (isNaN(index)) {
        console.log(`Not a storage message (invalid index): ${contentText}`);
        return undefined;
    }
    const isLast = content[1] === "true";
    const timestamp = Number(content[2]);
    const lastMessageEdited =
        message?.editedTimestamp ?? message?.createdTimestamp;
    const lastEdited = timestamp ?? lastMessageEdited;

    const firstAttachment = message?.attachments.at(0);
    if (!firstAttachment) {
        console.log(`No attachment: ${contentText}`);
        return undefined;
    }

    const response = await fetch(firstAttachment.url);

    if (!response.ok) {
        console.log(`Failed fetching attachment URL: ${firstAttachment.url}`);
        return undefined;
    }

    const blob = await response.arrayBuffer();
    return <MsgFormat>{
        index,
        fileContent: new Uint8Array(blob),
        messageId: message.id, // "message" can't be undefined
        timestamp: lastEdited,
        isLast,
    };
}

async function getChannelByName(
    guild: Guild,
    name: string,
): Promise<TextBasedChannel> {
    const channels = await guild.channels.fetch();

    return channels.find(
        (channel) =>
            channel?.name === name && channel?.type === ChannelType.GuildText,
    ) as TextBasedChannel;

    // Create if doesn't exist? Better tell them to create it themselves? To be clear. And so it won't use existing channels.
    // return await this.guild.channels.create({name, type: ChannelType.GuildText});
}

async function getUserMessagesInChannel(
    channel: TextBasedChannel,
    userId: string,
): Promise<Message[]> {
    const messages: Message[] = [];

    let lastMessagesCount;
    let lastMessageId;

    do {
        const fetchedMessages: Message[] = Array.from(
            (
                await channel.messages.fetch({
                    limit: DISCORD_SINGLE_FETCH_LIMIT,
                    before: lastMessageId,
                })
            ).values(),
        );

        messages.push(
            ...fetchedMessages.filter((message) => message.author.id === userId),
        );

        lastMessagesCount = fetchedMessages.length;
        lastMessageId = fetchedMessages.pop()?.id;
    } while (lastMessagesCount === DISCORD_SINGLE_FETCH_LIMIT);

    return messages;
}

// Returns the sorted messages, with the most updated message per index, while stopping when such an updated message is marked as last.
function getSortedRelatedMessages(encryptedMessages: MsgFormat[]): MsgFormat[] {
    const indexToMessage = new Map<number, MsgFormat>();
    encryptedMessages.forEach((msg) => {
        if (
            indexToMessage.has(msg.index) &&
            msg.timestamp < indexToMessage.get(msg.index).timestamp
        ) {
            return; // skip
        }
        indexToMessage.set(msg.index, msg);
    });

    const takenUntilLast: MsgFormat[] = [];
    for (const msg of indexToMessage.values()) {
        takenUntilLast.push(msg);
        if (msg.isLast) {
            break;
        }
    }

    // a,b => a-b, ascending order
    return Array.from(indexToMessage.values()).sort(
        (msgA: MsgFormat, msgB: MsgFormat) => msgA.index - msgB.index,
    );
}

function joinUint8Arrays(arrays: Uint8Array[]): Uint8Array {
    const totalLength = arrays.reduce((acc, arr) => acc + arr.length, 0);
    const joinedArray = new Uint8Array(totalLength);
    let offset = 0;
    for (const arr of arrays) {
        joinedArray.set(arr, offset);
        offset += arr.length;
    }
    return joinedArray;
}

function splitUint8Array(data: Uint8Array, maxByteSize: number): Uint8Array[] {
    const chunks = [];
    for (let start = 0; start < data.length; start += maxByteSize) {
        const chunk = data.slice(start, Math.min(start + maxByteSize, data.length));
        chunks.push(chunk);
    }
    return chunks;
}

function logWithTime(msg: string) {
    const timeWithMilli = new Date().toLocaleTimeString("il", {
        hour12: false,
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        fractionalSecondDigits: 3,
    });
    console.log(`${timeWithMilli} - ${msg}`);
}

function cloneValue(value: any): any {
    if (value == undefined) {
        return value;
    }

    return JSON.parse(JSON.stringify(value));
}

// FYI, admin actions are really slow.
/*
19:01:40.232 - reading messages
19:01:40.684 - read 1 messages

19:01:41.439 - reading messages
19:01:41.849 - read 1 messages
19:01:42.282 - bulk deleting 1 storage messages
19:01:43.090 - deleted storage messages
19:01:43.090 - sending 1 storage messages, total size 310
19:01:43.566 - done sending 1 storage messages
*/
