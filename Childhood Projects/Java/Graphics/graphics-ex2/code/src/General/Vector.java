package General;

/**
 * The class represents a vector in 3D
 *
 */
public class Vector {
	
	//vector =(x,y,z)   ( point+direction)
	
	protected double x,y,z; //direction
	
	private static final double EQUALITY_EPSILON=0.00001;
	
	
	public Vector(double x,double y,double z) 
	{
		this.x=x;
		this.y=y;
		this.z=z;
	}
		
	
	public Vector(Vector vector) 
	{
		this.x=vector.x;
		this.y=vector.y;
		this.z=vector.z;
	}
	
	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}


	public double getZ() {
		return z;
	}
		
	
	public Point toPoint()
	{
		return new Point(x,y,z);
	}
	
	
	public void normalize() //normalization on programmers 
	{
		double div=length();
		x=x/div;
		y=y/div;
		z=z/div;
	}
	
	public double length()
	{
		return Math.sqrt(x*x+y*y+z*z);
	}
	
	
	public boolean equals(Vector v)
	{
		if(Math.abs(x-v.x)>EQUALITY_EPSILON)
			return false;
		
		if(Math.abs(y-v.y)>EQUALITY_EPSILON)
			return false;
		
		if(Math.abs(z-v.z)>EQUALITY_EPSILON)
			return false;
			
		return true;
	}
	
	public boolean coLinear(Vector v)
	{
		Vector thisNormal=new Vector(this);
		Vector vNormal=new Vector(v);
		thisNormal.normalize();
		vNormal.normalize();
		
		//two vectors are co-linear if by normilizing them they are the same, or opposites
		
		return (thisNormal.equals(vNormal)||thisNormal.equals(vNormal.negative()));		
	}
	
	/**
	 * returns the dot product of vectors 'this' and 'd'
	 * @param d - vector
	 * @return
	 */
	public double dotProduct(Vector d)
	{
		return x*d.x+y*d.y+z*d.z;
	}
	
	/**
	 * returns the cross product between vectors 'this' and 'd'
	 * @param d - vector
	 * @return
	 */
	public Vector crossProduct(Vector d)  
	{
		double nx=y*d.z-z*d.y;
		double ny=z*d.x-x*d.z;
		double nz=x*d.y-y*d.x;
		return new Vector(nx,ny,nz);
	}
	


	public Vector product(double s) 
	{
		return new Vector(x*s,y*s,z*s);
	}
	
	public Vector negative() 
	{
		return new Vector(-x,-y,-z);
	}
	
	/**
	 * returns the vector's 2-norm
	 * */
	public double norm()
	{
		return Math.sqrt(this.dotProduct(this));
	}
	
	
	public static Vector subPoints(Point p1,Point p2) 
	{
		return new Vector(p1.getX()-p2.getX(),p1.getY()-p2.getY(),p1.getZ()-p2.getZ());
	}
	
	public boolean isZero()
	{
		Vector zeroVector = new Vector (0,0,0);
		return (this.equals(zeroVector));
	}
	
	
	public String toString()
	{
		return "("+x+","+y+","+z+")";
	}
}
