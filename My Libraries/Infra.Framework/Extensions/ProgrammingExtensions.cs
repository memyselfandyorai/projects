﻿using Infra.Framework.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infra.Framework.Extensions
{
    public static class ProgrammingExtensions
    {
        public static IEnumerable<T> ToSingleton<T>(this T value)
        {
            return new List<T>() { value };
        }

        public static void AddToList<K, V>(this IDictionary<K, List<V>> dictionary, K key, V item)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, new List<V>());
            }

            dictionary[key].Add(item);
        }

        public static void AddToCount<K>(this IDictionary<K, int> dictionary, K key, int item)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, 0);
            }

            dictionary[key] += item;
        }

        public static List<V> GetOrEmpty<K, V>(this IDictionary<K, List<V>> dictionary, K key)
        {
            return dictionary.GetOrDefault(key, new List<V>());
        }

        public static V GetOrDefault<K, V>(this IDictionary<K, V> dictionary, K key, V defaultValue = default)
        {
            if (!dictionary.ContainsKey(key))
            {
                return defaultValue;
            }

            return dictionary[key];
        }


        public static string JoinToString<T>(this IEnumerable<T> enumerable, string separator, Func<T, object> selector = null)
        {
            if (selector == null)
            {
                return string.Join(separator, enumerable);
            }

            return string.Join(separator, enumerable.Select(selector));
        }

        public static void Do<T>(this IEnumerable<T> list, Action<T> action)
        {
            list.ToList().ForEach(action);
        }

        /// <summary>
        /// Returns one item for each distinct value returned by the selector.
        /// </summary>
        public static IEnumerable<T> DistinctBy<T, I>(this IEnumerable<T> elements, Func<T, I> selector)
        {
            var existingValues = new HashSet<I>();
            foreach (var item in elements)
            {
                if(existingValues.Add(selector(item)))
                {
                    yield return item;
                }
            }
        }

        public static bool EqualsIgnoreCase(this string source, string target)
        {
            return String.Equals(source, target, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string Format(this string msgFormat, params object[] args)
        {
            return args.Length == 0 ? msgFormat : string.Format(msgFormat, args);
        }

        public static T AssertType<T>(this object instance)
        {
            if (!(instance is T))
            {
                throw new ProgrammersException("Instance of type {0} is not {1}", instance?.GetType().FullName ?? "null", typeof(T).FullName);
            }

            return (T)instance;
        }
    }
}