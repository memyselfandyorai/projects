import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

//docs dir: check in book (java on coffe cup...)


//   fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//DO_NOTHING_ON_CLOSE 


//exit button?

//http://java.sun.com/docs/books/tutorial/uiswing/components/components.html

//mes-font?

//lists: button attack,use skill, use spell\ability

public class Main extends JPanel implements ActionListener{
	
	/**
	 *Height and width of the frame
	 */
	final static int dimx=1000,dimy=700;
	
	/**
	 *Buttons: create character, connect and disconnect from server
	 */
	private JButton create,connect,disconnect;
	/**
	 *Buttons: save character, load character, and delete character
	 */
	private JButton save,load,delete;
	/**
	 *Buttons: Character page, and updated map
	 */
	private JButton cpage,smap;
	/**
	 *The player
	 */
	private Character player;
	//private Create creation;
	//private JLabel mes; //message
	/**
	 *Messages for the player
	 */
	private JLabel mes; //message
	//private JPanel buttons;
	/**
	 *Texts to choose port and host
	 */
	private JTextField port,host;
	/**
	 *The panel in the center
	 */
	private JPanel center;
	/**
	 *Character page
	 */
	private Page page;
	//private Map map;
	/**
	 *The map
	 */
	static Map map;
	/**
	 *The client class
	 */
	private Client client;
	
	//private JFrame main;
	/**
	 *The frame
	 */
	static JFrame main;
	
	//private int wf; //waiting for
	
	public Main(JFrame m,Client client)
	{
	super();
	main=m;
	this.client=client;
	//center=new JPanel();
	//center.setSize(300,300);
	//wf=-1;
	/*
	Vector v=new Vector();
	Weapon w=new Weapon("Longsword");
	v.add(w);
	w.magic(2,2);
	System.out.println(v.elementAt(0)); same one!
	*/
	
	//for(int i=0;i<20;i++)
	//System.out.println(i+": "+Char.mod(i));
	
	//String s="cool";
	//System.out.println(""+s.lastIndexOf("k")); :  -1
	
	setLayout(new BorderLayout());
	
	//buttons=new JPanel();
	//buttons.setLayout(new GridLayout(1,0));
	
	connect=new JButton("Connect");
	connect.addActionListener(this);
	connect.setToolTipText("Connect to server");
	connect.setEnabled(false);
	
	port=new JTextField("4000");
	port.setToolTipText("Port");
	port.setHorizontalAlignment(JTextField.CENTER);
	port.setFont(new Font("",0,14)); 
          	
	host=new JTextField("Local Host");
	host.setToolTipText("Host Server");
	host.setHorizontalAlignment(JTextField.CENTER);
	host.setFont(new Font("",0,14)); 
	
	disconnect=new JButton("Disconnect");
	disconnect.addActionListener(this);
	disconnect.setToolTipText("Disconnect from server and exit game");
	disconnect.setEnabled(false);
	
	
	create=new JButton("Create");
	create.addActionListener(this);
	create.setToolTipText("Create a Character");

	
	save=new JButton("Save");
	save.addActionListener(this);
	save.setToolTipText("Save your Character");
	
	load=new JButton("Load");
	load.addActionListener(this);
	load.setToolTipText("Load a Character");
	
	delete=new JButton("Delete");
	delete.addActionListener(this);
	delete.setToolTipText("Delete a Character");
		
	cpage=new JButton("Updated Character Page");
	cpage.addActionListener(this);
	cpage.setToolTipText("Shows an updated character page");
	
	smap=new JButton("Updated Map");
	smap.addActionListener(this);
	smap.setToolTipText("Shows an updated map of the realm");
	
	mes=new JLabel("Create or Load a Character");
	mes.setPreferredSize(new Dimension(50,50));
	//mes.setFont(new Font("",Font.BOLD,25));
	//mes.getFont().setSize(25);
	mes.setForeground(Color.red); //change color?
	mes.setBackground(Color.blue);
	
	center=new JPanel();
	center.setLayout(new GridLayout(1,1));
	center.add(new Create(mes,player,this));
	//center.setSize(50,50);
	center.setVisible(false);
		
//	creation=new Create(mes,player);
//	creation.setVisible(false);
	
	//System.out.println(creation.abilities()+"");
	
	//no getcontentpane
	JPanel j=new JPanel();
	j.setLayout(new GridLayout(0,1));
	j.add(smap);
	j.add(create);
	j.add(connect);
	
	JPanel tex=new JPanel(); tex.setLayout(new GridLayout(0,1));
	tex.add(port); tex.add(host);
	j.add(tex);
	j.add(disconnect);
	j.add(save);
	j.add(load);
	j.add(delete);
	j.add(cpage);

	//JToolTip tool=create.createToolTip();
	//tool.setTipText("fdgfd\nhdf");
	//j.add(tool);
	//System.out.println();
	
	cpage.setEnabled(false);
	save.setEnabled(false);
	smap.setEnabled(false);
	
	add(BorderLayout.NORTH,mes);
	
	//add(BorderLayout.NORTH,create);
	//add(BorderLayout.NORTH,save);
	add(BorderLayout.WEST,j);
	
	map=new Map(mes,this,client);
	map.setVisible(false);
	
	//add(BorderLayout.EAST,map);
		
	add(BorderLayout.CENTER,center);
	
	page=new Page(this);
	page.setVisible(false);
	//page.setSize(100,100);
	add(BorderLayout.SOUTH,page);
	
	
		
	}
	
	
	
	/*
	 *paints the component
	 */
	public void paintComponent(Graphics g)
	{
	super.paintComponent(g);
	}
	
	
	public void enabled()
	{
	//map.setPlayer(player);
	cpage.setEnabled(true);
	save.setEnabled(true);
	//smap.setEnabled(true);
	create.setEnabled(false);
	connect.setEnabled(true);
	}
	
	/*
	 *returns a Dimension
	 */
	public Dimension getPreferredSize()
	{
		return new Dimension(dimx,dimy);
	}
	
	
	public Character getPlayer()
	{
		return player;
	}
	
	public Place getPlace(int i,int j)
	{
		return map.getPlace(i,j);
	}
	
	public boolean samePlayer(int x,int y,int ind)
	{
		Place[][] pl=map.getPlaces();
		
		Vector v=pl[y][x].getChars();
		
	//	System.out.println("sp b"); 
		
		//System.out.println(v); 
		if(ind>-1)
		if(v.size()>ind)
		if(v.elementAt(ind)==player)
		return true;
	
	//System.out.println("sp a"); 
	
	return false;	
	}
	
	/*public void CAttack(Character c) //c=attacker,enm=player
	{
		//(in case another character is supposed to attack you attacks you)
			
		
		wf=player.getPlace().getChars().indexOf(c);
		
		map.waitFC(true);
		
		sendCPlace();
		
		//sendCAttack()
			
	}*/
	
	
	
	
	/*
	 *do actions as to the button clicked
	 */
	public void actionPerformed(ActionEvent e)
	{
		setMes("");
		//Create c=new Create();
		String a=((JButton)(e.getSource())).getText();
		
		if(a.equals("Create"))
		{
		//add(c);
		//Create creation=new Create(mes,player);
		//center.add(creation);
		
		//!!! continue
		//center=new Create(mes,player,this);
		map.setVisible(false);
		center.setVisible(true);
		
		//center.add(new JTextField());
		//repaint();
		//creation.setVisible(true);
		//repaint();
		}
		
		if(a.equals("Updated Character Page"))
		{
			//player.toString();
			//page=new Page(player);
			//repaint();
			if(page.isVisible())
			{
				page.setVisible(false);
				setMes("");
				/*
				System.out.println(player.getWeapon());
				System.out.println(player.getShield());
				System.out.println(player.getArmor());
				*/
			}
			else
			{
			page.update(player);
			page.setVisible(true);
			}
		}
		
		
		if(a.equals("Updated Map"))
		{
		//visible(false);
		setVisible(false);
		//System.out.println("playerUM: "+player);
		
		if(map.isVisible())
		map.setVisible(false);
		else
		{
		//player.settle(map.getPlaces());
		map.setVisible(true);
		center.setVisible(false);
		center.removeAll();
		map.update(player);
		center.add(map);
				
		add(BorderLayout.CENTER,map);
		}
		setVisible(true);
		//visible(true);
		
				/*
				if(map.isVisible())
				{
					map.setVisible(false);
				}
				else
				{
				setVisible(false);
				map.update(player);
				add(BorderLayout.EAST,map);
				setVisible(true);
				}
				*/
					
		}
		
			
		if(a.equals("Save"))
		{
		//serialization	
		
//		FileDialog fd=new FileDialog(main,"Save Character",FileDialog.SAVE);
		//fd.setFile(player.getName()+"- "+player.getClas()+" "+player.getRace()+" level "+player.lvl()+".char");
		//fd.setFile(player.getName()+".char");
 		JFileChooser fd = new JFileChooser(System.getProperty("user.dir"));
//		fd.setFile(player.name()+"- "+player.getRace()+" "+player.getClas()+" "+player.lvl());//+".char"
		//that exists
//		fd.show();
		
		fd.setSelectedFile(new File(player.name()+"- "+player.getRace()+" "+player.getClas()+" "+player.lvl()));//+".char"
		
		 fd.setDialogType(JFileChooser.SAVE_DIALOG );
		 fd.addChoosableFileFilter(new CharFileFilter());
		 int returnVal = fd.showSaveDialog(main);

	if(returnVal==0)
	{
		File f = fd.getSelectedFile();
		String add = f.getAbsolutePath();//getName();
		
		//File tr=new File(add);
		//System.out.println(""+tr.isFile());
		//String add=fd.getFile();
		if(add!=""&&add!=null)
		{
			File ff=new File(add+".char");
						
			if(!f.isFile()&&!ff.isFile()) //if file exists and file.char
			{
			
			try{
			FileOutputStream address;
			
			if(add.indexOf(".char")==-1)
			address=new FileOutputStream(add+".char");
			else
			address=new FileOutputStream(add);
						
			ObjectOutputStream file=new ObjectOutputStream(address);
			file.writeObject(player);
			file.flush();
			file.close();			
			
			}catch(IOException eio){System.out.println(eio);}
			
			}
			else
			{
			int op=JOptionPane.showConfirmDialog(main,"Are you sure you want to overwrite the saved file?","Overwrite?",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);	
				if(op==0)
				{
								
				try{	
				FileOutputStream address;
			
				if(add.indexOf(".char")==-1)
				address=new FileOutputStream(add+".char");
				else
				address=new FileOutputStream(add);

				ObjectOutputStream file=new ObjectOutputStream(address);
				file.writeObject(player);
				file.flush();
				file.close();			
				
				}catch(IOException eio){System.out.println(eio);}
				
				}
				
			}
			
			
			
		}
	}
		
		}
		
		
		if(a.equals("Load"))
		{
			
		//FileDialog fd=new FileDialog(main,"Load Character",FileDialog.LOAD);
//		JFileChooser fd=new JFileChooser(main,"Load Character",FileDialog.LOAD);
		//fd.setFile(player.getName()+"- "+player.getRace()+" "+player.getClas()+" "+player.lvl());//+".char"
		
//		fd.setFilenameFilter(new FilenameFilter());

        JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));

          //MyFileFilter filter = new MyFileFilter();

            //chooser.addChoosableFileFilter(filter);

        
	    chooser.addChoosableFileFilter(new CharFileFilter());


         chooser.setDialogType(JFileChooser.OPEN_DIALOG);

         int returnVal = chooser.showOpenDialog(main);

         if(returnVal == JFileChooser.APPROVE_OPTION)

          {

       



		 File f = chooser.getSelectedFile();
         String add = f.getPath();
    	
    	
    	
    	String filename = f.getName();
	    String ex="";
	    int i = filename.lastIndexOf('.');
	    if(i>0 && i<filename.length()-1) 
    	ex=filename.substring(i+1).toLowerCase();
    	    	
		//String add=fd.getFile();
		if(add!=""&&add!=null&&ex.equals("char"))
		{
			
			try{	
			FileInputStream address=new FileInputStream(add);
			ObjectInputStream file=new ObjectInputStream(address);
			
				try{
				player=(Character)(file.readObject());
				}catch(ClassNotFoundException ei){System.out.println(ei);}
			//file.flush();
			file.close();			
			}catch(IOException eio){System.out.println(eio);}
			
			map.setVisible(false);
			page.setVisible(false);
			page.reset();
			
			map.load();
			
			//map.setPlayer(player);
			
			
			player.setPlace(Dice.roll(Map.dim)-1,Dice.roll(Map.dim)-1);
			
			//player.setPlace(Dice.roll(5)-1,Dice.roll(5)-1); //del
			
			//player.getItems().clear();
			
			//player.setPlace(1,1);
			
			//player.settle(map.getPlaces());  //why?
								
			enabled();
					
			//del
			/*
			player.addItem(Item.getItem("Wood",20));
			player.addItem(Item.getItem("Iron",20));
			player.addItem(Item.getItem("Copper",20));
			player.addItem(Item.getItem("Silver",20));
			*/
			//del
			
		}
			
			
			
		}
		
		}
		
		
		
		
		
		
		if(a.equals("Delete"))
		{
		

 		JFileChooser fd = new JFileChooser(System.getProperty("user.dir"));
	
		 fd.setDialogType(JFileChooser.OPEN_DIALOG );
		 fd.addChoosableFileFilter(new CharFileFilter());
		 
		 int returnVal = fd.showOpenDialog(main);
		
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
		File f = fd.getSelectedFile();
		//String add = f.getAbsolutePath();//getName();
		
		String filename = f.getName();
	    String sub="";
	    int i = filename.lastIndexOf('.');
	    if(i>0 && i<filename.length()-1)
		sub=filename.substring(i+1).toLowerCase();
		
		if(sub.equals("char"))
		{
			int op=JOptionPane.showConfirmDialog(main,"Are you sure?","Delete?",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
			//System.out.println(""+op);
			//yes-0,no-1
			
			//System.out.println("delete: "+op);			
			
			if(op==0)
			f.delete();
						
		}
		else
		mes.setText("Not a Character.");
		
		}
		//String add=fd.getFile();
		/*
		if(add!=""&&add!=null)
		{
			try{	
			FileOutputStream address=new FileOutputStream(add+".char");
			ObjectOutputStream file=new ObjectOutputStream(address);
			file.writeObject(player);
			file.flush();
			file.close();			
			
			}catch(IOException eio){System.out.println(eio);}
		}
		*/	
		}
		
		
		if(a.equals("Connect"))
		{
			if(Page.isNatural(port.getText()))
			{
			port.setEnabled(false);
			host.setEnabled(false);
			load.setEnabled(false);
			connect.setEnabled(false);
			client.enterServer(port.getText(),host.getText());
			//disconnect.setEnabled(true);
			}
			else
			setMes("Port needs to be a number.");
		}
		
		
		if(a.equals("Disconnect")) //when\if not exits, change
		{
			//System.out.println("player: "+player);
			disconnect();
		}
		
		//center.removeAll();
		//remove(c);
		
	}
	
	
	
	public void setPlayer(Character c)
	{
	setCh(c);
	map.setPlayer(c);
		
	}
	
	public JFrame getFrame()
	{
	return main;	
	}
	
	public void setCh(Character c)
	{
		player=c;
	}
	
	public Map getMap()
	{
		return map;
	}
	
	public void setMes(String s)
	{
		mes.setText(s);
	}
	
	public String getMes()
	{
		return mes.getText();
	}
	
	public static void visible(boolean n)
	{
		main.setVisible(n);
	}
	
	public void addEast(JComponent p,JComponent d)
	{
		main.setVisible(false); //frame, and not this panel
		//add(BorderLayout.EAST,p); //delete corrent component. how?
		//add(BorderLayout.SOUTH,p);
		if(d!=null)
		d.setVisible(false);
		
		add(BorderLayout.EAST,p);
		
		//map.update(player);
				
		main.setVisible(true);
	}
	
	
	public void unable(boolean t)
	{
	page.unable(t);
	map.unable(t);	
	}
	
	
	public void battle(boolean b,boolean addB)
	{
	//true- battle
	//load.setEnabled(!b); ?
	save.setEnabled(!b);
	disconnect.setEnabled(!b);
	map.setDirs(!b);
	
	if(addB)
	map.addBattle(); //or when?
	
	}
	
	
	public Attack getBattle()
	{
	return map.getBattle();	
	}
	
	public void dead()
	{
		smap.setEnabled(false);
		//cpage.setEnabled(false);
		//load.setEnabled(true);
		save.setEnabled(false);
		
		disconnect.setEnabled(true);
		
		load.setEnabled(false);
		
		page.unable(true);
		
		//System.out.println("save: "+save.isEnabled());
		
	}
	
	public void setMap(String s)
	{
		map.read(s);
		
		smap.setEnabled(true);
		
		disconnect.setEnabled(true);
		//System.out.println("player: "+player);
	
	}
	
	
	public boolean isConnected()
	{
	return client.isConnected();	
	}
	
	public void ifAttacked()
	{
	map.ifAttacked();	
	}
	
	
	public void sendChange(int x,int y,int ind)
	{
		String s="CHANGE*";
		s=s+x+",";
		s=s+y+",";
		s=s+ind+",";
		
		Vector c=map.getPlace(y,x).getChars();
		//System.out.println("ind: "+ind+"\n");
		//System.out.println("v: "+c+"\n");
		//System.out.println("indexof: "+c.elementAt(ind).toString()+"\n");
		
		//System.out.println("chars: "+c);
		
		s=s+map.getPlace(y,x).write();
		
		client.sendMessage(s+"*CHANGE");
	}
	
	
	public void sendMove(int xx,int yy)
	{
		
		//System.out.println("movedbs: "+player.getMoved());
		//System.out.println(player.write());
		
		//System.out.println("cs place: "+player.getPlace().getChars()+"  ; ind: "+player.getPlace().getChars().indexOf(player));
					
		String s="MOVE*";
		s=s+xx+",";
		s=s+yy+",";
		s=s+player.getX()+",";
		s=s+player.getY()+",";
		s=s+player.getPlace().getChars().indexOf(player)+",";
			
		client.sendMessage(s+"*MOVE");
	}
	
	public void sendPick(String chosen,Char sel)
	{
		String s="PICK*";
		s=s+player.getX()+",";
		s=s+player.getY()+",";
		s=s+chosen+",";
		
		Vector chars=player.getPlace().getChars();
		
		s=s+chars.indexOf(player)+",";
		s=s+chars.indexOf(sel)+",";
		
		client.sendMessage(s+"*PICK");
	}
	
	
	public void sendRun()
	{
		String s="RUN*";
		s=s+player.getX()+",";
		s=s+player.getY()+",";
		s=s+player.getPlace().getFighters().indexOf(player)+",";
				
		client.sendMessage(s+"*RUN");
	}
	
	public void sendCPlace()
	{
		//System.out.println("hiding: "+player.getHiding());
		
		String s="PC*";
		
		s+=player.getX()+",";
		s+=player.getY()+",";
		s+=map.getPlace(player.getY(),player.getX()).write();
		
		//System.out.println("cpl");
		
		client.sendMessage(s+"*PC");
	}	
	
	
	
	public void sendUPlace()
	{
		String s="UPDATE*";
		
		s+=player.getX()+",";
		s+=player.getY()+",";
		s+=map.getPlace(player.getY(),player.getX()).write();
		
		//System.out.println("cpl");
		
		client.sendMessage(s+"*UPDATE");
		
		
		//System.out.println("sending update...");
	}
	
	
	public void sendCPlace(int i,int j,int add)
	{
		String s="PCC*";
		if(i>=0&&j>=0&&i<Map.dim&&j<Map.dim&&add>=0)
		{
		s+=j+",";
		s+=i+",";
		s+=add+",";
		
		s+=map.getPlace(i,j).write();
		
		
		//System.out.println("chars. "+map.getPlace(i,j).getChars());
		
		client.sendMessage(s+"*PCC");
		}
					
	}	
		
	
	public void sendMes(String mes,boolean priv,int index)
	{
	String s="TALK*";	
		
	s+=player.getX()+",";
	s+=player.getY()+",";
	s+=index+",";
	s+=priv+",";
	s+=mes+",";
	   			
	client.sendMessage(s+"*TALK");
	
	if(priv)
	map.talk(mes);
	}
	
	
	public void setPlace(int i,int j,Place p)
	{
	map.setPlace(i,j,p);
	}
	
	public void setUPlace(int i,int j,Place p)
	{
	map.setUPlace(i,j,p);
	}
	
	
	public void setPlace(int i,int j,Place p,int add)
	{
	map.setPlace(i,j,p,add);
	}
	
	
	public boolean samePlace(int x,int y)
	{
	if(player.getX()==x&&player.getY()==y)
	return true;
	
	return false;		
	}
	
	
	public void update()
	{
	//main.setVisible(false);
	map.update(player);	
	page.update(player);
	//System.out.println("update");
	//main.setVisible(true);
	}
	
	
	public Page getPage()
	{
	return page;	
	}
	
	
	public void sendMessage(String s) //static?
	{
		//System.out.println("sends attack (main)....");	
		client.sendMessage(s);
	}
	
	
	public void disconnect()
	{
		disconnect.setEnabled(false);
		smap.setEnabled(false);
		load.setEnabled(true);
		//System.out.println("player: "+player);
		String pl=map.removePlayer(player);
		client.disconnect(pl,player.isDead());		
	
		connect.setEnabled(true);
	}
	
	/*
	 *main
	 */
	 
	 /*
	public static void main(String[] args) {
	
	JFrame f=new JFrame();
	Main m=new Main(f);
	f.getContentPane().add(m);
	f.setSize(Main.dimx,Main.dimy);
	f.setVisible(true);
	
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}	
	*/
}
