package Lights;

import General.Color;
import General.NormalVector;
import General.Point;
/**
 * The class represents a directed light
 *
 */
public class DirectedLight extends Light {

	
	private NormalVector direction;
	
	public boolean hasAllValues() {

		if(direction==null)
			return false;
		
		return true;
	}

	public NormalVector getDirection() {
		return direction;
	}

	public void setDirection(NormalVector direction) {
		this.direction = direction;
	}
	
	public double getDistance(Point hit)
	{
		return Double.POSITIVE_INFINITY;
	}
	
	public NormalVector getDirection(Point hit)
	{
		return direction.negative();
	}

	public Color getColor(Point hit){
		return color;
	}
}
