
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.io.*;

//too much attacks: if spell?

//bug with order of attack.

public class Place implements Serializable{
	
	/**
 	*Name (type) of the place
 	*/
	private String name;
	/**
 	*Difficulty class of skill uses in this place
 	*/
	private int DC; //difficulty class
	/**
 	*Items in this place
 	*/
	private Vector items; //items are quite hidden
	/**
 	*Characters in this place
 	*/
	private Vector chars;
	/**
 	*Money in this place
 	*/
	private double gp;
	/**
 	*Order of fight
 	*/
	private int order;
	/**
 	*Fighters (by order) and their initiation score
 	*/
	private Vector fighters,nums;
	/**
 	*Fighters in the surprize round
 	*/
	private Vector surprize;
	
	/**
	*Constructs a new Place
	*@param n : type of place
	*/
	public Place(String n)
	{
		name=n; //town(orange?), village(brown), cave(black),forest(green), mountain(red), river\lake(blue), mines (yellow?)
		
		//color=new Color(5,100,200);
		
		surprize=new Vector();
		
		order=0;
		fighters=new Vector();
		nums=new Vector();
		
		gp=Dice.roll(30)+Dice.roll(2,6)-15;
		if(gp<0)
		gp=0;
		
		int p=Dice.roll(100),c=0;
		if(p<7)
		c=1;
		if(p>=7&&p<25)
		c=2;
		if(p>=25&&p<65)
		c=3;
		if(p>=65&&p<93)
		c=4;
		if(p>=93)
		c=5;
				
		DC=c*5;
		
		items=new Vector();
		chars=new Vector();
		//items, enemies and NPCs:
					
		if(n.equals("Town"))
		{
			//color=Color.cyan; //change
			
			if(c==1)
			DC+=4;
			if(c==2)
			DC+=2;
			if(c>2)
			DC-=3;
			
			int gua=Dice.roll(100)-20;
			gua=gua/30;
			for(int i=0;i<gua;i++)
			chars.add(new Char("Guardian",this));
			
			if(Dice.roll(2)==1)
			chars.add(new Merchant("Blacksmith",Dice.roll(5),this));
			if(Dice.roll(2)==1)
			chars.add(new Merchant("Merchant",Dice.roll(5),this));
			//which merchant, also random. (merchant, brewer(?), blacksmith) two of the, or 3 here?
			
			if(Dice.roll(5)>2)
			chars.add(new Char("Wizard",this));
			
			if(Dice.roll(2)==1)
			for(int i=0;i<2;i++)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=4)
			race="Human";
			if(r>=5&&r<=6)
			race="Elven";
			if(r>=7&&r<=9)
			race="Dwarven";
			if(r==10)
			race="Halfling";
			
			chars.add(new Char(race+" Civilian",this));
			}
			
			if(Dice.roll(100)>80)
			{
				chars.add(new Char("Royal Guard",this));
				if(Dice.roll(100)>65)
				{
					chars.add(new Char("Royal Guard",this));
					chars.add(new Char("Royalty",this));
				}
			}
			
			int nob=Dice.roll(3)-1;
			for(int i=0;i<nob;i++)
			chars.add(new Char("Noble",this));
			
			if(Dice.roll(3)>1)
			chars.add(new Char("Healer",this));
			
			//vampire,zombie
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Vampire",this));		
			if(en>80&&en<95)
			chars.add(new Enemy("Zombie",this));		
					
			
			int it=Dice.roll(100);
			if(it>90)
			items.add(Item.getRing());
			if(it>80&&it<90)
			items.add(Weapon.getWeaponM());
			if(it>65&&it<80)
			items.add(Shield.getShield());
			if(it>50&&it<70)
			items.add(Weapon.getWeaponS());
			if(it>45&&it<52)
			items.add(Armor.getArmor());
			if(it<25)
			items.add(Item.getItemT());
						
			//chars.add(new Enemy("Skeleton",this)); thief?												
			
			gp=gp+c*5;
			
					
		}
		
		
		if(n.equals("Village"))
		{
			//color=Color.cyan; //change
			if(c>2)
			DC-=4;
			
			int gua=Dice.roll(100);
			gua=gua/50;
			for(int i=0;i<gua;i++)
			chars.add(new Char("Guardian",this));
			
			if(Dice.roll(3)!=1)
			chars.add(new Merchant("Merchant",Dice.roll(4),this));
			//which merchant, also random. (merchant, brewer(?), blacksmith)
			
			if(Dice.roll(3)==1)
			chars.add(new Char("Wizard",this));
						
			int civ=Dice.roll(4);
			
			if(Dice.roll(2)==1)
			for(int i=0;i<civ;i++)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=4)
			race="Human";
			if(r>=5&&r<=6)
			race="Elven";
			if(r>=7&&r<=9)
			race="Dwarven";
			if(r==10)
			race="Halfling";
			
			chars.add(new Char(race+" Civilian",this));
			}
						
			if(Dice.roll(2)==1)
			chars.add(new Char("Healer",this));
			
			//snake,wraith
			int en=Dice.roll(100);
			if(en>92)
			chars.add(new Enemy("Wraith",this));		
			if(en>83&&en<92)
			chars.add(new Enemy("Snake",this));		
					
							
			int it=Dice.roll(100);
			if(it>90)
			items.add(Weapon.getWeaponM());
			if(it>75&&it<90)
			items.add(Shield.getShield());
			if(it>60&&it<80)
			items.add(Weapon.getWeaponS());
			if(it>55&&it<62)
			items.add(Armor.getArmor());
			if(it<45)
				for(int i=0;i<it/5+1;i++)
				items.add(Item.getItemV());
						
			//chars.add(new Enemy("Skeleton",this)); thief?												
			
			gp=gp+c*2;
			
					
		}
		
		if(n.equals("Mines"))
		{
			//color=Color.cyan; //change
			if(c<5)
			DC+=3;
			
			if(Dice.roll(100)>80)
			chars.add(new Char("Guardian",this));
			
			if(Dice.roll(4)==1)
			chars.add(new Merchant("Blacksmith",Dice.roll(4),this));
			if(Dice.roll(3)==1)
			chars.add(new Merchant("Merchant",Dice.roll(5),this));
			//which merchant, also random. (merchant, brewer(?), blacksmith)//change
			
			
			if(Dice.roll(3)==1)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=4)
			race="Human";
			if(r>=5&&r<=9)
			race="Dwarven";
			if(r==10)
			race="Halfling";
			
			chars.add(new Char(race+" Civilian",this));
			}
									
			int it=Dice.roll(100);
			if(it>90)
			items.add(Weapon.getWeaponM());
			if(it>70&&it<90)
			items.add(Shield.getShield());
			if(it>55&&it<80)
			items.add(Weapon.getWeaponS());
			if(it>48&&it<58)
			items.add(Armor.getArmor());
			if(it<26)
				for(int i=0;i<it/5+2;i++)
				items.add(Item.getItemM());
			
			if(Dice.roll(4)==1)
			chars.add(new Char("Healer",this));
			
			//ogre,orc,goblin,gnome,gnoll
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Ogre",this));		
			if(en>70&&en<90)
			chars.add(new Enemy("Orc",this));		
			if(en>60&&en<80)
			chars.add(new Enemy("Goblin",this));
			if(en>50&&en<65)
			chars.add(new Enemy("Gnome",this));
			if(en>45&&en<60)
			chars.add(new Enemy("Gnoll",this));		
			if(en>35&&en<45)
			chars.add(new Enemy("Imp",this));		
			
			gp=gp+DC*2;
							
		}
			
			
		if(n.equals("Cave"))
		{
			//color=Color.cyan; //change
			if(c<5)
			DC+=4;
					
			if(Dice.roll(4)==1)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=4)
			race="Human";
			if(r>=5&&r<=10)
			race="Dwarven";
					
			chars.add(new Char(race+" Civilian",this));
			}
			
			if(Dice.roll(4)==1)
			chars.add(new Char("Wizard",this));
								
			int it=Dice.roll(100);
			
			if(it>90)
			items.add(Armor.getArmor());
			if(it>70&&it<90)
			items.add(Shield.getShield());
			if(it>55&&it<75)
			items.add(Weapon.getWeaponS());
			if(it<25)
				for(int i=0;i<it/5+1;i++)
				items.add(Item.getItemC());
			
			if(Dice.roll(5)==1)
			chars.add(new Char("Healer",this));
			
			//ogre,ogre mage,skeleton,ghoul,osyluth,quasit
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Ogre Mage",this));		
			if(en>78&&en<90)
			chars.add(new Enemy("Ogre",this));		
			if(en>60&&en<80)
				for(int i=0;i<(en-55)/5;i++)
				chars.add(new Enemy("Skeleton",this));
			if(en>50&&en<65)
				for(int i=0;i<(en-45)/6;i++)
				chars.add(new Enemy("Ghoul",this));
			if(en>10&&en<23)
			chars.add(new Enemy("Quasit",this));		
			if(en<10)
			chars.add(new Enemy("Osyluth",this));		
			
			gp=gp-c*2;
								
		}	
		
		if(n.equals("River"))
		{
			//color=Color.cyan; //change
			if(c<3)
			DC+=3;
			if(c>3)
			DC-=3;
					
			if(Dice.roll(2)==1)
			{
			int r=Dice.roll(10);
			String race="";
			if(r>=1&&r<=2)
			race="Human";
			if(r==3)
			race="Dwarven";
			if(r>=4&&r<=8)
			race="Halfling";
			if(r>=9&&r<=10)
			race="Elven";
					
			chars.add(new Char(race+" Civilian",this));
			}
			
			if(Dice.roll(5)>3)
			chars.add(new Char("Wizard",this));
			
			if(Dice.roll(2)==1)
			chars.add(new Merchant("Merchant",Dice.roll(5),this));
			if(Dice.roll(5)==1)
			chars.add(new Merchant("Blacksmith",Dice.roll(5),this));
			
			int it=Dice.roll(100);
			
			if(it>95)
			items.add(Armor.getArmor());
			if(it>85&&it<95)
			items.add(Shield.getShield());
			if(it>75&&it<90)
			items.add(Weapon.getWeaponS());
			if(it<40)
				for(int i=0;i<it/5;i++)
				items.add(Item.getItemR());
			
			if(Dice.roll(3)==1)
			chars.add(new Char("Healer",this));
			
			//ogre,orc+archer,goblin,dretch
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Dretch",this));		
			if(en>75&&en<90)
			chars.add(new Enemy("Ogre",this));		
			if(en>55&&en<75)
			chars.add(new Enemy("Orc Archer",this));
			if(en>45&&en<65)
			chars.add(new Enemy("Orc",this));
			if(en>40&&en<50)
			chars.add(new Enemy("Goblin",this));		
			if(en>35&&en<40)
			chars.add(new Enemy("Wyvern",this));
			
			gp=gp-c*3;
								
		}
		
		if(n.equals("Mountain"))
		{
			//orange
			if(c<4)
			DC+=4;
			
			
			if(Dice.roll(3)==1)
			chars.add(new Merchant("Blacksmith",Dice.roll(3),this));
			if(Dice.roll(4)==1)
			chars.add(new Merchant("Merchant",Dice.roll(5),this));
			//which merchant, also random. (merchant, brewer(?), blacksmith)//change
			
			if(Dice.roll(5)==1)
			chars.add(new Char("Wizard",this));
										
			int it=Dice.roll(100);
			
			if(it>95)
			items.add(Armor.getArmor());
			if(it>85&&it<95)
			items.add(Shield.getShield());
			if(it>75&&it<90)
			items.add(Weapon.getWeaponS());
			if(it<20)
				for(int i=0;i<it/5+1;i++)
				items.add(Item.getItemN());
				
			//orc archer,goblin,gnoll,red+black dragon
			int en=Dice.roll(100);
			if(en>95)
			chars.add(new Enemy("Red Dragon",this));		
			if(en>75&&en<90)
			chars.add(new Enemy("Gnoll",this));		
			if(en>55&&en<75)
			chars.add(new Enemy("Goblin",this));
			if(en>45&&en<65)
			chars.add(new Enemy("Orc Archer",this));
			if(en>40&&en<45)
			chars.add(new Enemy("Black Dragon",this));		
			
			gp=gp-c*4;
								
		}
		
		if(n.equals("Forest"))
		{
			//green
								
			if(Dice.roll(2)==1)
			{
			int civ=Dice.roll(3);
				for(int i=0;i<civ;i++)
				{
				int r=Dice.roll(10);
				String race="";
				if(r>=1&&r<=2)
				race="Human";
				if(r==3)
				race="Dwarven";
				if(r>=4&&r<=8)
				race="Elven";
				if(r>=9&&r<=10)
				race="Halfling";
						
				chars.add(new Char(race+" Civilian",this));
				}
			}
			
			
			if(Dice.roll(5)>3)
			chars.add(new Char("Wizard",this));
			
			if(Dice.roll(4)==1)
			chars.add(new Merchant("Merchant",Dice.roll(3),this));
			
									
			int it=Dice.roll(100);
			
			if(it>90)
			items.add(Weapon.getWeaponM());
			if(it>80&&it<95)
			items.add(Shield.getShield());
			if(it>70&&it<85)
			items.add(Weapon.getWeaponS());
			if(it<30)
				for(int i=0;i<it/7+1;i++)
				items.add(Item.getItemF());
			
			if(Dice.roll(5)>3)
			chars.add(new Char("Healer",this));
			
			//orc+archer,lemure,wolf,ghoul,skeleton
			int en=Dice.roll(100);
			if(en>90)
			chars.add(new Enemy("Ghoul",this));		
			if(en>80&&en<95)
			chars.add(new Enemy("Skeleton",this));		
			if(en>60&&en<80)
			chars.add(new Enemy("Orc",this));
			if(en>50&&en<70)
			chars.add(new Enemy("Orc Archer",this));
			if(en>35&&en<50)
			chars.add(new Enemy("Lemure",this));		
			if(en>25&&en<40)
			chars.add(new Enemy("Wolf",this));
								
		}
		
	if(gp<0)
	gp=0;
	
	gp/=10;
	
	
	if(DC>17)
	if(Dice.roll(3)>1)
	DC-=5;
	
	
	}
	
	
	public void addItem(Item item) //+do removeItem
	{
	boolean b=true;
	
	for(int i=0;i<items.size();i++)
	{
	Item m=(Item)(items.elementAt(i));
		if(item.getName().equals(m.getName()))
		{
			m.setAmount(m.getAmount()+item.getAmount());
			b=false;
		}
	}
	
	if(b)
	items.add(item);
	
	}
	
	public void removeItem(String n,int m)
	{
	for(int i=0;i<items.size();i++)
	{
		Item t=(Item)(items.elementAt(i));
		if(n.equals(t.getName()))	
		{
			t.setAmount(t.getAmount()-m);
			if(t.getAmount()<=0)
			items.remove(t);
		}
	}
	
	}
	
	public void removeItem(Item m)
	{
	items.remove(m);	
	}
	
	/**
	*Drops all items and money from a character to this place
	*@param o : the character
	*/
	public void dropItems(Char c)
	{
	Vector pits=c.getItems();
	Vector its=new Vector(pits);
	
	for(int i=0;i<its.size();i++)
	{
	Item it=(Item)(its.elementAt(i));	
	c.removeItem(it);
	addItem(it);
	//System.out.println(it);
	}	
	
	double g=c.gp();
	gp=gp+g;
	c.addMoney(-g);
	
	pits.clear();
	
	c.setWeapon(Weapon.create("Unarmed Strike",c));
	c.setArmor(null);
	c.setShield(null);
	
	}
	
	public double gp()
	{
		return gp;
	}
	
	
	public int dc()
	{
		return DC;
	}
	
	/**
	*Returns the color of this place (by its type)
	*/
	public Color getColor()
	{
		//http://www.pitt.edu/~nisg/cis/web/cgi/rgb.html
		
		if(name.equals("Town"))
		return new Color(255,250,205);
		
		if(name.equals("Village"))
		return Color.orange;
				
		if(name.equals("Mines"))
		return Color.yellow;//new Color(240,255,255);
				
		if(name.equals("Cave"))
		return Color.black;
		
		if(name.equals("River"))
		return Color.blue;
		
		if(name.equals("Mountain"))
		return Color.red;
		
		//if(name.equals("Forest"))
		return Color.green; //forest
		
		//return new Color(50,200,100);
	}
	
	
	
	/*
	public String getRGB()
	{
	
	return "rgb("+color.getRed()+","+color.getGreen()+","+color.getBlue()+")";
	}
	*/
	
	public void addSur(Char c) //to surprize
	{
		surprize.add(c);
	}
	
	
	public String getName()
	{
		return name;
	}
	
	
	/**
	*A function to move a character, returns the index of the previous place
	*@param c : the character
	*@param m[][] : the places array (the map)
	*@param x : the change in the x coordination
	*@param y : the change in the y coordination
	*/
	public static int move(Character c,Place m[][],int x,int y) //data is correct.
	{
		//int d=Map.dim; 
				
		int xx=c.getX(),yy=c.getY();
		
		int add=m[yy][xx].chars.indexOf(c);
		
		m[yy][xx].chars.remove(c);
		m[yy+y][xx+x].chars.add(c);
		
		c.move(x,y,m[c.getY()+y][c.getX()+x]);
		
		//Place cp=m[c.getY()][c.getX()];
		
		
		
		
		
		//more options on move()?
		
		return add;
	}

	
	
	public int removeChar(Char c)
	{
	int ind=chars.indexOf(c);
	chars.remove(c);
	for(int i=0;i<chars.size();i++)
	{
	Char r=(Char)(chars.elementAt(i));	
	//if(r instanceof Character)
	//((Character)(r)).getSeen().remove(c);
	r.getSeen().remove(c);
	}
	return ind;
	}
	
	
	public void updateSeen() //some disappear, so needs to update
	{
		for(int i=0;i<chars.size();i++)
		{
		Char r=(Char)(chars.elementAt(i));	
		Vector s=r.getSeen();
			for(int j=0;j<s.size();j++)
			{
				Char c=(Char)(s.elementAt(j));
				if(chars.indexOf(c)==-1)
				{
					s.remove(c);
					//if(c!=null)
					////System.out.println("him: "+r.write()+"\n\nremoves him: "+c.write()+"\n\nthis place: "+this.write());
				}
			}
		}
	}
	
	
	public Vector getEnemies()
	{
		Vector v=new Vector();
		for(int i=0;i<chars.size();i++)
		{
			Char c=(Char)(chars.elementAt(i));
			if(c instanceof Enemy)
			v.add(c);
		}
	return v;
	}
	
	
	public Vector getCharacters()
	{
		Vector v=new Vector();
		for(int i=0;i<chars.size();i++)
		{
			Char c=(Char)(chars.elementAt(i));
			if(c instanceof Character)
			v.add(c);
		}
	return v;
	}
	
	
	public Vector getGuardians()
	{
		Vector v=new Vector();
		for(int i=0;i<chars.size();i++)
		{
			Char c=(Char)(chars.elementAt(i));
			if(c.getClass().toString().equals("class Char"))
			if(c.name().equals("Guardian")||c.name().equals("Royal Guard"))
			v.add(c);
		}
	return v;
	}
	
	
	public Vector getFighters()
	{
		return fighters;
	}
	
	public int getOrder()
	{
		return order;
	}
	
	public Vector getNums()
	{
		return nums;
	}
		
	public void tryAdd(Char c,int b)
	{
		chars.add(c);
		fighters.add(c);
		nums.add(b+"");
	}
	
	
	/*
	public void sendCAttack(Char c)
	{
		int ind=chars.indexOf(c);
					
		String s="ATTACK*";
		s=s+Main.map.getI(this)+",";
		s=s+Main.map.getJ(this)+",";
		s=s+ind+",";
		Main.sendMessage(s+"*ATTACK");
		
		//sends only to those who are in this place.
		
	}
	*/
	
	/**
	*A function that allows a character to attack a player
	*@param at : the character
	*@param ed : the player
	*@param server : the Server class used
	*/
	public void charAttack(Char at,Character ed,Server server)
	{
	Attack battle=new Attack();
		
	if(fighters.size()==0)
	{
		Vector v=new Vector();
		v.add(at);
		v.add(ed);
		
		fighters=Char.initiation(v,nums); //check this after few attack. with "for"?
		
		battle.addIn(fighters,nums);
					
		order=0;
		int d=fighters.indexOf(ed);
					
		roundCon(d,battle,ed,server,false);
	}
	else
	{
		if(!at.attacked())
		addFighter(at,battle);
		
		if(ed.attacked())
		battle.addText(at.attack(ed)); //adds to attacks.
		else
		{
		addFighter(ed,battle);
		roundCon(fighters.indexOf(at),battle,ed,server,false);
		}			
		
		
	}
	
	}
	
	/**
	*A function to check if a character is attacked after changing a place
	*@param ind : the character's index in the place
	*@param server : the Server class used
	*/
	public void change(int ind,Server server)
	{
		////System.out.println("chars: "+chars);
		Character player=(Character)(chars.elementAt(ind));
	
		//boolean b=true;
	if(fighters.size()==0)
	{
		Vector enms=getChars();//getEnemies();
			
		Vector v=new Vector();
		//Vector num=new Vector();
		
		//Vector surprise=new Vector(); //if not seen.
		
		
		Attack battle=new Attack();
		
		//change it if spotting one another is important
		for(int i=0;i<enms.size();i++) //spots, and surprises.
		{
			Char no=(Char)(enms.elementAt(i));
			//no.spot(enms);
			no.spot(chars);
			if(no instanceof Enemy)
			if(no.seen(player)&&Char.npc(no))
			{
			////System.out.println("saw: "+no);
			
			////System.out.println("nim? "+no.seen(player));
			
				if((!player.seen(no))&&!(player.getWeapon().isRanged()))
				surprize.add(no);
				
				//else
				player.addSeen(no);
			
			if(v.indexOf(player)==-1)
			v.add(player);
		
			v.add(no);
								
			}
		}
		
		
		//character's check.
		Vector pls=getCharacters();
		Vector cha=new Vector();
		cha.add(player);
		for(int i=0;i<pls.size();i++) //spots (of players.)
		{
			Character c=(Character)(pls.elementAt(i));
			////System.out.println("ch: "+c);
			////System.out.println("pl: "+player);
			if(c!=player)
			{
				c.spot(cha);
				////System.out.println("seen: "+c.getSeen());
			}
		}
				
		
		//server.sendPlace(this);
		//server.sendCPlace(player); needed?
		
		
		////System.out.println("change... sur: "+v);
		
		
			if(v.size()!=0) //fighting.     //to try and run?
			{
			fighters=Char.initiation(v,nums); //check this after few attack. with "for"?
				
			battle.addIn(fighters,nums);
			//server.sendMessage("*main.battle(true)*"); //needed? check this.
			
			////System.out.println("main.battle...");
			
			//System.out.println("att: "+player.attacked());
			
			order=0;
			int d=fighters.indexOf(player);
					
					if(surprize.size()!=0) //surprise attack
					{
						roundCon(d,battle,player,server,true);
					}
					
					/*for(int i=0;i<surprise.size();i++)
					{
					Char current=(Char)(surprise.elementAt(i));
					battle.addText("(surprise) "+current.attack(player));//thread, sleep
					////System.out.println("attacker (S): "+current+" , order: "+i+"  , size: "+surprise.size());
					player.addSeen(current);
					checkLife(server,battle);
					}*/
					
					//else?	
					if(NoF("Character")!=0)	
					roundCon(d,battle,player,server,false);
					
			}
		
	}
	else //there are fighters where you moved to.
	{
		Vector enms=getEnemies();
		for(int i=0;i<enms.size();i++) //spots.
		{
			Char no=(Char)(enms.elementAt(i));
			no.spot(chars);
		}
		
						
		Vector pls=getCharacters();
		Vector cha=new Vector();
		cha.add(player);
		for(int i=0;i<pls.size();i++) //spots (of players.)
		{
			Character c=(Character)(pls.elementAt(i));
			////System.out.println("ch: "+c);
			////System.out.println("pl: "+player);
			if(c!=player)
			{
				c.spot(cha);
				////System.out.println("seen: "+c.getSeen());
			}
		}
		
		
		//server.sendCPlace(player); needed?
			
	}
	
	}
	
	
	/**
	*A function that continues the battle rounds
	*@param dd : the character's index
	*@param bat : the battle feedback
	*@param pl : the player
	*@param server : the Server class used
	*@param sur : if it is a surprize round
	*/
	public void roundCon(int dd,Attack bat,Character pl,Server server,boolean sur)//round continue
	{
	////System.out.println("roundCon");
	
	//if(sur)
	//System.out.println("surprize round");
	
	//+ noa. i think.
	
	Place plthis=this;
	
	int d=dd;
		
	Attack battle=bat;
	
	Character player=pl;
 	
 	String surp="";
 	
	Vector n_fighters=null;
		if(sur)
		{
		n_fighters=fighters;
		fighters=surprize;
		surp="(surprise) ";
		}
	
		d=fighters.indexOf(player);
		
		////System.out.println("run-con\nd: "+d+"   ,   order: "+order);
		
		////System.out.println("fighters: "+fighters);
		
		//while(order!=d&&d>=0)
		while(fighters.size()>0)
		{
		Char current=(Char)(fighters.elementAt(order));
		
		if(!(current instanceof Character))
		{
		Character chara=pickPlayer(current,player);
		//change this also: (or not. need to use spells also.)
		//System.out.println("\nst "+player.getHp()+"-"+player.getDam());
		battle.addText(surp+current.attack(chara));//  chara, not player.
		//System.out.println("turn of "+current);
		//System.out.println(battle.convert());
		}
										
		d=fighters.indexOf(player);
		
		
		//System.out.println("d: "+d+" , order: "+order);
		////System.out.println("current: "+current);
		/*//System.out.println("chars-p: "+chars.indexOf(player));
		//System.out.println("hp: "+player.getHp());
		//System.out.println("x: "+player.getX());
		//System.out.println("y: "+player.getY());
		//System.out.println("fighters: "+fighters);
		//System.out.println("chars: "+chars);
		*/
		
		if(surprize.size()>0)
		surprize.remove(0);
		
		//if(d==order)
		//{
		////System.out.println("character?: "+current.write());
		//}
		//System.out.println("bin "+player.getHp()+"-"+player.getDam());
		if(current instanceof Character)
		{
		
		//System.out.println("character");
			
		//wake(plthis,d,battle,server); //not needed, it's outside the while
		
		//System.out.println("breaking: "+current);
		
			if(((Character)(current)).isDying()) //dying...
			{
				sendDying((Character)(current),server,battle);
			}
			else //need to be waken
			{
			checkLife(server,battle);
			break;
			}
		//break? (check with alla)
		}
		
		////System.out.println("order++");
		
		order++;
		if(order>=fighters.size())
		order=0;
		
		checkLife(server,battle);
		//System.out.println("ack "+player.getHp()+"-"+player.getDam());
		}
					
	checkLife(server,battle);
	
	
	if(n_fighters!=null)
	fighters=n_fighters;
	
	if(fighters!=null)
		if(fighters.size()==1)
		{
			//main.battle(false);
			
			checkLife(server,battle);
			
			int index=chars.indexOf(fighters.elementAt(0));
			//fighters=null;
			fighters=new Vector();
			nums=new Vector();
			player.attacked(false);
			order=0;
			player.attacks(0);
			
			wakeByChars(this,index,battle,server);
		}
		
	////System.out.println("wake: "+order+","+d+","+fighters.elementAt(order));
	
	//System.out.println("wake: order: "+order+"\n"+fighters);
	
	//why order = 0?!
	
	//this. not plthis
	if(!(fighters.size()==0)&&!sur)
	wake(this,order,battle,server); //waking...    order, not d!
		
	
	//a check for- if there are character left. (if not, stop.) [istn't  it the "fighter.size==1"?]
	
	
	}	
		
			
	
	
	
	/*
	
	public void roundCon(final int dd,final Attack bat,final Character pl,final Server server,final boolean sur)//round continue
	{
	final Place plthis=this;
	Thread tr=new Thread(new Runnable(){
	
	int d=dd;
		
	Attack battle=bat;
	
	Character player=pl;
	
	public void run()
	{   
	Vector n_fighters=null;
		if(sur)
		{
		n_fighters=fighters;
		fighters=surprize;
		}
	
		d=fighters.indexOf(player);
		
		//System.out.println("run-con\nd: "+d+"   ,   order: "+order);
		
		//System.out.println("fighters: "+fighters);
		
		//while(order!=d&&d>=0)
		while(true)
		{
		Char current=(Char)(fighters.elementAt(order));
		battle.addText(current.attack(player));//thread, sleep
										
		d=fighters.indexOf(player);
		
		order++;
		if(order>=fighters.size())
		order=0;
		
		//System.out.println("d: "+d+" , order: "+order);
		//System.out.println("chars-p: "+chars.indexOf(player));
		//System.out.println("hp: "+player.getHp());
		//System.out.println("x: "+player.getX());
		//System.out.println("y: "+player.getY());
		
		if(surprize.size()>0)
		surprize.remove(0);
		
		if(d==order)
		{
		//System.out.println("character?: "+current.write());
		}
		
		if(current instanceof Character)
		{
		
		//System.out.println("character");
		
		checkLife(server,battle);
		
		//wake(plthis,d,battle,server); //not needed, it's outside the while
		
		
		break;
		//break? (check with alla)
		}
		
		
		
		checkLife(server,battle);
			
		}
					
	checkLife(server,battle);
	
	
	if(n_fighters!=null)
	fighters=n_fighters;
	
	if(fighters!=null)
		if(fighters.size()==1)
		{
			//main.battle(false);
			
			checkLife(server,battle);
			//fighters=null;
			fighters=new Vector();
			player.attacked(false);
			order=0;
			player.attacks(0);
		}
		
	
	wake(plthis,d,battle,server);
		
	
	//a check for- if there are character left. (if not, stop.) [istn't  it the "fighter.size==1"?]
	
	
	}	
		
	});	
	
	tr.start();
		
	}
	
	*/
	/*
	public void roundCons(final int dd,final Attack bat,final Character pl,final Server server,final Vector surp)//round continue+surprize
	{
	final Place plthis=this;
	Thread tr=new Thread(new Runnable(){
	
	int d=dd;
			
	Attack battle=bat;
	
	Character player=pl;
	
	public void run()
	{   
	
	Vector n_fighters=fighters;
	fighters=surp;
	
		d=fighters.indexOf(player);
		
		//System.out.println("run-con\nd: "+d+"   ,   order: "+order);
		
		//System.out.println("fighters: "+fighters);
		
		//while(order!=d&&d>=0)
		while(true)
		{
		Char current=(Char)(fighters.elementAt(order));
		battle.addText(current.attack(player));//thread, sleep
										
		d=fighters.indexOf(player);
		
		order++;
		if(order>=fighters.size())
		order=0;
		
		//System.out.println("d: "+d+" , order: "+order);
		
		if(current instanceof Character)
		{
		
		//System.out.println("character");
		
		checkLife(server,battle);
		
		//wake(plthis,d,battle,server); //not needed, it's outside the while
		
		
		break;
		//break? (check with alla)
		}
		
		
		
			
		}
					
	checkLife(server,battle);

	fighters=n_fighters;

	wake(plthis,d,battle,server);
		
	
	//a check for- if there are character left. (if not, stop.) [istn't  it the "fighter.size==1"?]
	
	}	
		
	});	
	
	tr.start();
		
	}
	*/
	
	
	 /**
	 *A function that wakes a player (meaning he can play- his turn or end of battle)
	 *@param plthis : his place
	 *@param d : his index in the place
	 *@param battle : battle feedback
	 *@param server : the Server class used
	 */
	public void wake(Place plthis,int d,Attack battle,Server server)
	{
		//what if "add"? (PCC?)
		
		
		Character waken=(Character)(plthis.getFighters().elementAt(d));
		//po habag haarur!!!
		
		////System.out.println("wake...");
		/*
		if(fighters!=null)
		if(fighters.size()==1)
		{
		checkLife(server,battle);
		fighters=new Vector();
		waken.attacked(false);
		order=0;
		waken.attacks(0);
		}
		*/
		
					
		//server.sendPlace(plthis);
		server.sendCPlace(waken);
		
		server.sendAttack(d,plthis); //send awaking
		
		
		//battle after pc?
		
		
		//send battle:
		String bf=battle.convert();//battle feedback 
		//battle=new Attack();
		battle.clear();
		server.sendBattle(bf,plthis,waken.getX(),waken.getY());
		
		
		//break?
		
	}


	public void sendDying(Character c,Server server,Attack battle)
	{
		String dim="",s="";
		if(!c.male())
		s="s";
		
		
		if(!c.stable())
		if(Dice.roll(100)>10)
		{
			c.dealDam(1);
				if(c.isDead())
				dim=" ("+s+"he was hurt by 1 point of damage and now "+s+"he is dead)";
				else
				dim=" ("+s+"he was hurt by 1 point of damage)";
		}
		else
		{
			c.stable(true); //-> when healing: stable: false.
			dim=" ("+s+"he was stabilized)";
		}
		
		battle.addText(c.name()+" is dying"+dim+".");
		
		checkLife(server,battle);
		
			if(!c.isDead())
			{
			server.sendCPlace(c);
			
			server.sendAttack(fighters.indexOf(c),this);
			
			//send battle:
			String bf=battle.convert();//battle feedback 
			//battle=new Attack();
			battle.clear();
			server.sendBattle(bf,this,c.getX(),c.getY());
			}
	}
	
	
	public void wakeByChars(Place plthis,int index,Attack battle,Server server)
	{
		
		
		Character waken=(Character)(plthis.getChars().elementAt(index));
			
		
					
		server.sendCPlace(waken);
		
			
		//send battle:
		String bf=battle.convert();//battle feedback 
		//battle=new Attack();
		battle.clear();
		server.sendBattle(bf,plthis,waken.getX(),waken.getY());
		
	}
	


	
	/**
	 *A function that allows a player to try to pick pocket a character
	 *@param player : the player
	 *@param sel : the character
	 *@param money : if tries to steal money (else item)
	 */
	public String pickPocket(Character player,Char sel,boolean money)
	{
		
	if(money) //money	
	{
	int DC=10;
	boolean seen=sel.seen(player);
		if(seen)
		DC=sel.spot();
		
		//if(DC<10)
		//DC=10;    so the seen is 
	
	if(sel.male()!=player.male())
	{
		if(sel.getCON()>player.getCON())
		DC+=2;
		else
		DC-=2;
	}
		
		////System.out.println("pp dc: "+DC);
		
		if(player.getSkills().check("Pick Pocket",DC))
		{
			int per=Dice.roll(100)+player.getSkills().pick();
			if(per>100)
			per=100;
			if(per<20)
			per=20;
			
			double gold=sel.gp();
			
			//gold=gold*per/100;
			String gg=""+(double)(gold*per/100);
			gold=Double.parseDouble(gg);
			if(gg.indexOf(".")!=-1&&gg.indexOf(".")+2<gg.length())
			gold=Double.parseDouble(gg.substring(0,gg.indexOf(".")+2));
			
			
			player.addMoney(gold);
			sel.addMoney(-gold);
			
			return "You succeeded to pick pocket and you got "+gold+" GP.";
		}
		else
		{
			if(seen)
			{
				if(sel instanceof Character)
				return "You failed to pick pocket and you were seen. (the player can attack you in response)"; //seen	
				else
				return "You failed to pick pocket and you were seen. (the character has attacked you in response)"; //seen	
			}
			else
			return "You failed to pick pocket.";
		}
	
	}
	else //item
	{
	int DC=20;
	boolean seen=sel.seen(player);
		if(seen)
		DC=10+sel.spot();
	
	if(sel.male()!=player.male())
	{
		if(sel.getCON()>player.getCON())
		DC+=2;
		else
		DC-=2;
	}	
		//if(DC<10)
		//DC=10;    so the seen is 
	Vector items=sel.getItems();
	
		////System.out.println("pp dc: "+DC);
			
			if(items.size()>0)
			{
		if(player.getSkills().check("Pick Pocket",DC))
		{
			int it=Dice.roll(items.size())-1;
			
			Item item=(Item)(items.elementAt(it));
			
			if(sel.getWeapon()==item)
			sel.setWeapon(Weapon.create("Unarmed Strike",sel));
			
			sel.removeItem(item);
			player.addItem(item);
						
			return "You succeeded to pick pocket and you got "+item.getAmount()+" "+item.getName()+".";
		}
		else
		{
			if(seen)
			{
				if(sel instanceof Character)
				return "You failed to pick pocket and you were seen. (the player can attack you in response)"; //seen	
				else
				return "You failed to pick pocket and you were seen. (the character has attacked you in response)"; //seen	
			}
			else
			return "You failed to pick pocket.";
		}
			}
			else
			return "You don't find any items.";
	}	
			
	} //pickp
	

	public static String getXY(Place[][] map,Place p)
	{
	for(int i=0;i<Map.dim;i++)
		for(int j=0;j<Map.dim;j++)
		if(map[i][j]==p)
		return j+","+i+",";
		
		//stop using this function?
		
		//System.out.println("bug! (xy)");
		
		return "";	
		

		
	
	//bug!!! p doesnt exists sometimes in map!
	
	}
	
	
	public void addMoney(double a)
	{
	gp=gp+a;	
	}
	
	public Vector getItems()
	{
	return items;	
	}
	
	public Vector getChars()
	{
	return chars;	
	}
	
	public Char getChar(int a)
	{
		if(chars.size()>a)
		return (Char)(chars.elementAt(a));
		
	return null;
	}
	
	public int getOrder(Char c)
	{
		if(fighters!=null)
		return fighters.indexOf(c);
		
		return -1;
	}
	
	/**
	 *Adds a fighter to the battle
	 *@param c : the fighter
	 *@param battle : battle feedback
	 */
	public void addFighter(Char c,Attack battle) //fighter sees all combatants.
	{
	c.attacked(true);
	
	int m=c.initiative();
	
	Vector f=new Vector();Vector n=new Vector();f.add(c);n.add(""+m);battle.addIn(f,n);
	
	for(int i=0;i<fighters.size();i++)
	{
		c.addSeen((Char)(fighters.elementAt(i)));
		((Char)(fighters.elementAt(i))).addSeen(c);
	}
		
	boolean b=true;

		for(int j=0;j<nums.size()&&b;j++)
		{
			int g=Integer.parseInt((String)(nums.elementAt(j)));
			if(m>g)
			{
			fighters.add(j,c);
			b=false;
			}
			
			if(m==g)
			{
				int d=((Char)(fighters.elementAt(j))).getDEX();
				int cd=c.getDEX();
				if(cd>d)
				{
				fighters.add(j,c);
				b=false;
				}
				else
					if(cd==d)
						if(Dice.roll(2)==1)
						{
						fighters.add(j,c);
						b=false;
						}	
			}
		}
		
	if(b)
	{
	fighters.add(c);
	}
	
	nums.add(fighters.indexOf(c),""+m); //bug if inserted last or fifth?
	
	}
	
	 /**
	 *A function that checks the lives of all fighters in the place, if they died or alike
	 *@param server : the Server class used
	 *@param battle : battle feedback
	 */
	public void checkLife(Server server,Attack battle)
	{
	for(int i=0;i<fighters.size();i++)	
	{
	Char c=(Char)(fighters.elementAt(i));
	
		if(c.isDead()) //later
		{
			////System.out.println(c+" is dead");
			int k=chars.indexOf(c);
			
			dropItems(c);
			
			if(c instanceof Character)
			{
				String bf=battle.convert();//battle feedback 
				//battle=new Attack();
				battle.clear();
				
				Character cc=(Character)c;
				
				server.sendCharacter(cc,k,this); //or after sendbattle?						
							
				server.sendBattle(bf,this,cc.getX(),cc.getY());
				
			}
			
			nums.remove(fighters.indexOf(c));
			fighters.remove(c);	
			removeChar(c);
			//list.remove(c);
			
			//System.out.println("end? "+fighters);
			
			if(noPlayers(fighters)) //battle end.
			{
			fighters=new Vector();
			nums=new Vector();
			//player.attacked(false);
			endFight();
			order=0;
			((Character)(c)).attacks(0);
			
			//System.out.println("battlend.");
			
			}
			
			server.sendPlace(this,k);
			
			
			//drop items, get XP (if this player only, or all?)
		}
	}
	
	if(order>=fighters.size())
	order=0;
	
	
	}
	
	
	public boolean hasHealer()
	{
	for(int i=0;i<chars.size();i++)
	{
	Char c=(Char)chars.elementAt(i);
	if(c.getClass().toString().equals("class Char"))
	if(c.name().equals("Healer"))
	return true;
	}
	return false;
	}
	
	public void endFight()
	{
		for(int i=0;i<chars.size();i++)
		{
		Char c=(Char)(chars.elementAt(i));
		c.attacked(false);
		}
	}
	
	
	public boolean noPlayers(Vector v)
	{
		for(int i=0;i<v.size();i++)
		{
			if(v.elementAt(i) instanceof Character)
			return false;
		}		
	
	return true;	
	}
	
	/**
	 *A function that returns randomly a player that a character attacks
	 *@param c : the character
	 *@param player : the player
	 */
	public Character pickPlayer(Char c,Character player)
	{
	Vector s=c.getSeen();
	Vector v=new Vector();
	
	//normal
	
	for(int i=0;i<s.size();i++)
	if(s.elementAt(i) instanceof Character)
	if(((Char)(s.elementAt(i))).attacked())
	v.add(s.elementAt(i));
	
	int choice=Dice.rollWZ(v.size())-1;
	
	//guardian and healer ?
	//if(c.name().equals("Guardian")||
	
	/*
	 vector attacked, "remove*" (from fight, if no enemy and no one in ayyacked. could end fight.)
	guardian attacking, healer heals. 2d6+2 healing bonus.
	(healer\guardian- character? so enemies will attack?)
	neh... too much.*/
	
	
	if(choice<0)
	return player;
	else
	return (Character)(v.elementAt(choice));
	
	}
	
	/**
	 *A function that allows a player to attack someone
	 *@param serP : the index of the player
	 *@param lsvE : the selected character
	 *@param spc : the index of the spell to use (-1 means that he attacks with weapon)
	 *@param server : the Server class used
	 */
	public void battle(int serP,int lsvE,int spc,Server server) //serial player, selected enemy, serial spell
	{
		
		Attack battle=new Attack();
				
		Character player=(Character)(chars.elementAt(serP));
                        
        Char sel=(Char)(chars.elementAt(lsvE));//selected value
        
        //needs to do this totally, not only in "while"
        ////System.out.println("battle");
		
            //update these later?
            
            		if(fighters.size()==0) //the fighting begins
					{		
					////System.out.println("nim");
					//System.out.println("fight begins");
					
					//don't fit here, update later if needed
					//(dfh)
					//JComponent del=battle;
					//battle=new Attack();
					//main.addEast(battle,del);
					
					Vector v=new Vector();
					v.add(player);
					v.add(sel);
					
					nums=new Vector();
										
					
					////System.out.println("v: "+v+"\n");
					fighters=Char.initiation(v,nums); //check this after few attack. with "for"?
					
					battle.addIn(fighters,nums); //do it for the joiners too?
					
					//main.battle(true); (dfh)
					
					////System.out.println("order: "+fighters);
					////System.out.println("nums: "+nums);
					
					
					Char current=(Char)(fighters.elementAt(order)),enm;		
						
					//attacks(false);
					
					//if emeny has ranged etc.
					
					////System.out.println("sel class: "+sel.getClass());
					if(sel.getClass().toString().equals("class Char")||sel.getClass().toString().equals("class Merchant"))
					{ //no bugs, right?
					//System.out.println("guardians");
					Vector guard=getGuardians();
					for(int i=0;i<guard.size();i++)
					{
					Char gua=(Char)(guard.elementAt(i));
					if(fighters.indexOf(gua)==-1)
					if(gua.seen(player)||gua.seen(sel))
					addFighter((Char)(guard.elementAt(i)),battle);
					}
					
					}
				
					
					
					boolean style; //player first=true
					
					enm=sel;
					
					player.attacked(true);
					current.attacked(true);
					
					if(player==current) //player first
					{
					if(player.getWeapon().isRanged()||!enm.getWeapon().isRanged())
					style=true;
					else
					style=false;
					}
					else
					{
					if(enm.getWeapon().isRanged()||!player.getWeapon().isRanged())
					style=false;
					else
					style=true;
					}
				
				
				if(enm.seen(player))
				{
					if(style) //player first
					{
					battle.addText(player.attack(enm,spc));
					checkLife(server,battle);
						if(!enm.isDead()&&(player.attacks()>=player.NoA()||spc!=-1))
						{
						player.attacks(0);
							if(!(enm instanceof Character))
							{
							battle.addText(enm.attack(player));
							checkLife(server,battle);
							}
							else
								if(((Character)(enm)).isDying())
								sendDying((Character)(enm),server,battle);
								else //need to be awaken
								wake(this,fighters.indexOf(enm),battle,server); //waking...    order, not d!
						}
					}
					else //enm first
					{
					
						if(!(enm instanceof Character))
						{
							battle.addText(enm.attack(player));
							checkLife(server,battle);
							if(!player.isDead()&&!player.isDying())
							{
							battle.addText(player.attack(enm,spc));
							checkLife(server,battle);
							}
								if(!enm.isDead()&&(player.attacks()>=player.NoA()||spc!=-1))
								{
								battle.addText(enm.attack(player));
								checkLife(server,battle);
								player.attacks(0);
								}
								else
									if(player.attacks()<player.NoA()&&spc==-1)
										if(!player.isDying())
										wake(this,fighters.indexOf(player),battle,server);
										else
										sendDying(player,server,battle);
							//order++;
							//if(order>=fighters.size())
							//	order=0;
						}
						else
						wake(this,fighters.indexOf(enm),battle,server); //waking...    order, not d!
					}
				}
				else
				{
					battle.addText("(surprise) "+player.attack(enm,spc));
					checkLife(server,battle);
					player.attacks(player.NoA());
				}
					//order=0
				
					//player.attacks(0);
					//attacks(true);
							
				//is that needed?
				//roundCon(fighters.indexOf(player),battle,player,server,false);
			
					} //(fighters=null)
					else //there are fighters.
					{
						
					if(player.attacked())
					{
					
						//attacks(false);
						int d=order;				
						
						Char opon=sel;
						
						if(opon.attacked())
						battle.addText(player.attack(opon,spc)); //adds to attacks.
						else //attacking new opponent.
						{
						addFighter(opon,battle);
						
						if(opon.seen(player))
						{						
							if(player.getWeapon().isRanged())
							battle.addText(player.attack(opon,spc));
							else
							{
								
								if(!(opon instanceof Character))
								{
								battle.addText(opon.attack(player));
								checkLife(server,battle);
								battle.addText(player.attack(opon,spc));
								checkLife(server,battle);
								}
								else
								wake(this,fighters.indexOf(opon),battle,server); //waking...
							}
						}
						else //surprise attack	
						{
							battle.addText("(surprise) "+player.attack(opon,spc));
							checkLife(server,battle);
						}  //nort sneaking in middle of battle.
						
						}
						////System.out.println("attack: "+txt);
											
						checkLife(server,battle);				
						
						////System.out.println("NoA:" +player.attacks()+","+player.NoA());
						
						////System.out.println("attacks: "+player.attacks()+", noa: "+player.NoA());
						
						////System.out.println("attacks: "+player.attacks()+", noa: "+player.NoA());
						////System.out.println("f: "+fighters+"     , order: "+order);
					
					}
											
						
					} //fighting (ARE fighters) done.
						
						
					if(!player.attacked())
					{
					Character cur=(Character)(fighters.elementAt(order)); //will it create a bug? (attacking while char attacks?)
					addFighter(player,battle);
					wake(this,fighters.indexOf(cur),battle,server);
					}
					else
						if(player.attacks()>=player.NoA()||spc!=-1) //number of attacks
						{
						player.attacks(0);
						//order++;
																	
						int d=fighters.indexOf(player); //plz?
						
						if(!sel.seen(player)) //sel: enm\opon
						{
						sel.addSeen(player);
						d=-1;			
						}
						order=d+1;
						if(order>=fighters.size())
						order=0;
						////System.out.println("order: "+order+", d: "+d+", I W: "+fighters.indexOf(fighters.elementAt(order)));
						
						roundCon(d,battle,player,server,false);
						/*
							while(order!=d&&d>=0)
							{
							Char current=(Char)(fighters.elementAt(order));
							
							
							battle.addText(current.attack(player));//thread, sleep
							////System.out.println("attack-e: "+txt);
							
							////System.out.println("ch: "+current+", order: "+order);
							//guardian, order: 1,2,3
														
							d=fighters.indexOf(player);
							
							//f.out.println("d: "+d+" , order: "+order); //1,1    -1,0
							
							//why after dead attacks himself?  ;  check if attacks 2, this works?
							
							checkLife(server,battle);
							
							order++;
								if(order>=fighters.size())
								order=0;
							}
							*/
						}
						else
						{	
							order=fighters.indexOf(player);
							roundCon(fighters.indexOf(player),battle,player,server,false);
							
							//roundcon! the order isn't rising!
							//wake(this,fighters.indexOf(player),battle,server);
						}
						
						//attacks(true); //THIS is the function that ruins it!!!
						
						checkLife(server,battle);
						
						//if(player.isDead())
						//attacks(false);
						
						//battle.addText(s);
								
		if(fighters!=null)
		if(fighters.size()==1)
		{
//			main.battle(false);
			
			checkLife(server,battle);
			//fighters=null;
			fighters=new Vector();
			nums=new Vector();
			player.attacked(false);
			order=0;
			player.attacks(0);
		}

		
			
		//send things:	(?)
		//wake(this,order,battle,server); //plthis=this,d=order
		
		
		
		
		//roundCon(order,battle,player,server,false); //d=order   <-- ???why?
				
		
		//do- attack new opponent
		
		//do- when dead\dying\fainted etc.

	}//end battle()
	
	/**
	 *Allows a player to try to run from battle
	 *@param player : the player
	 *@param server : the Server class used
	 */
	
	public void run(Character player,Server server)
	{
	int nu=fighters.size()-1;
	int en=NoF("Enemy");
	int ch=NoF("Char");
	int pl=NoF("Character")-1;
	
	int dc=en*5+ch+pl*3;
		
	if(dc<=10)
	dc*=2;
	
	dc+=5;
	
		
	if(en==1)
	dc+=((Enemy)(getEnemies().elementAt(0))).getLvl()*2;
	
	int d=Dice.roll(20);
	int dd=d+player.dex()+player.con();
	
	boolean run=((dd>=dc&&d!=1)||d==20);
	
	Attack battle=new Attack();
	
	String mes="";
	
	if(run)
	{
	nums.remove(fighters.indexOf(player));
	fighters.remove(player);	
	player.attacked(false);
	player.attacks(0);
	
	
	if(noPlayers(fighters)) //battle end.
	{
	fighters=new Vector();
	nums=new Vector();
	endFight();
	order=0;
	}
	
	
	if(fighters.size()==1)
	{
		Char c=(Char)(fighters.elementAt(0));
		//checkLife(server,battle);
			
				
		fighters=new Vector();
		nums=new Vector();
		c.attacked(false);
		order=0;
				
		if(c instanceof Character)
		{
		((Character)(c)).attacks(0);
		int index=chars.indexOf(c);
		wakeByChars(this,index,battle,server);
		}
	}
	
	
	server.sendCPlace(player);
	
	mes="You succeeded to run from battle.";
	
		if(fighters.size()!=0)
		{
		order++;
			if(order>=fighters.size())
			order=0;
		
		roundCon(-1,battle,null,server,false); //bugs?
		}
	
	}
	else
	{
	//a lot of runs to one person.
	mes="You failed to run from battle."; //bug?
	player.attacks(player.attacks()+1);	
	
		if(player.attacks()>=player.NoA()) //no spc, running
		{
		player.attacks(0);
		order++;
			if(order>=fighters.size())
			order=0;	
		}
	
	roundCon(fighters.indexOf(player),battle,player,server,false);
	}

	server.sendRun(player,mes);
	
	}//run
	
	
	public int NoF(String clas) //number of fighters
	{
	int c=0;
	for(int i=0;i<fighters.size();i++)
	if(fighters.elementAt(i).getClass().toString().equals("class "+clas))
	c++;
	return c;	
	}
	
	
	public void writeSeen() //only characters
	{
	//System.out.println("SEEN:");
		for(int i=0;i<chars.size();i++)
		{
			if(chars.elementAt(i) instanceof Character)
			{
			Character c=(Character)(chars.elementAt(i));
			//System.out.println(c+": "+c.getSeen());
			}
					
		}
		
	//System.out.println("");		
	}
	
	
	
	
	
	//change all of this.
	
	/**
	*Connverts the place to a String (coded)
	*/
	public String write() //name not at start.
	{
		String s="P*";
		s+=DC+",";
		s+=name+",";
		
		s+=";";
		for(int i=0;i<items.size();i++)
		{
		Item m=(Item)(items.elementAt(i));
		s+=m.write();
		}
		s+=";";
										
		s+=gp+",";
		
		s+=order+",";
		
		s+=";";
		for(int i=0;i<fighters.size();i++)
		{
		int a=chars.indexOf(fighters.elementAt(i));
		s+=a+",";
		}
		s+=";";
		
		s+=";";
		for(int i=0;i<surprize.size();i++)
		{
		int a=chars.indexOf(surprize.elementAt(i));
		s+=a+",";
		}
		s+=";";
		
		
		s+=";";
		for(int i=0;i<nums.size();i++)
		{
		String a=(String)(nums.elementAt(i));
		s+=a+",";
		}
		s+=";";
		
		s+=";";
		for(int i=0;i<chars.size();i++)
		{
		////System.out.println(i+" chars: "+chars);
		Char m=(Char)(chars.elementAt(i));
		s+=m.write();
		}
		s+=";";
		
		
		return s+"*P";
	}
	
	
	
	
	
	/**
	*Returns a place by a coded String
	*@param s : the coded String
	*/
	public static Place read(String s)
	{
		Place d=new Place("");
		s=s.substring(2);
	
		int b=s.indexOf(",");
		d.DC=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
		b=s.indexOf(",");
		d.name=s.substring(0,b);
		s=s.substring(b+2);	
		
		
	b=s.indexOf(";");
	String i=s.substring(0,b);
	//items:
	while(i.indexOf("*")!=-1)
	{
	Item t=null;	
	int c=0;
	
	if(i.indexOf("I")==0)
	{
		c=i.indexOf("*I");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*I")+2;	
		}
		
		t=Item.read(i.substring(0,c+1));
	}
	
	if(i.indexOf("W")==0)
	{	
		c=i.indexOf("*W");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*W")+2;	
		}
		t=Weapon.readW(i.substring(0,c+1));
	}
	
	if(i.indexOf("A")==0)
	{
		c=i.indexOf("*A");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*A")+2;	
		}
		t=Armor.readA(i.substring(0,c+1));	
	}
	
	if(i.indexOf("S")==0)
	{
		c=i.indexOf("*S");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*S")+2;	
		}
		t=Shield.readS(i.substring(0,c+1));
	}
	
	d.items.add(t);
	i=i.substring(c+2);
	}
	//end items
	
	////System.out.println("\ns1: "+s+"\n");
	
	s=s.substring(b+1); //+2?
	
	////System.out.println("\ns2: "+s+"\n");
	
	b=s.indexOf(",");
	d.gp=Double.parseDouble(s.substring(0,b));
	s=s.substring(b+1);	

	b=s.indexOf(",");
	d.order=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+2);	
	

	Vector temp=new Vector();
	
	while(s.indexOf(";")!=0)
	{
		b=s.indexOf(",");
		int ind=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
		//d.fighters.add(d.chars.elementAt(ind));
		temp.add(ind+"");
	}
	
	s=s.substring(2);
	
	Vector temp2=new Vector();
	
	while(s.indexOf(";")!=0)
	{
		b=s.indexOf(",");
		int ind=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
		//d.fighters.add(d.chars.elementAt(ind));
		temp2.add(ind+"");
	}
	
	s=s.substring(2);
	
	
	while(s.indexOf(";")!=0)
	{
		b=s.indexOf(",");
		int num=Integer.parseInt(s.substring(0,b));
		s=s.substring(b+1);	
		d.nums.add(num+"");
	}
	
	s=s.substring(2);
	
	
	b=s.lastIndexOf(";");
	
	i=s.substring(0,b);
	//chars:
	
	
	////System.out.println("\ns: "+s);
	////System.out.println("\nchars: "+i);
	
	
	Vector seen=new Vector();
	
	while(i.indexOf("*")!=-1)
	{
	Char t=null;	
	int c=0;
	
	if(i.indexOf("C")==0)
	{
		c=i.indexOf("*C");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*C")+2;	
		}
		
		t=Char.read(i.substring(0,c+1),d,seen);
	}
	
	if(i.indexOf("R")==0)
	{	
		c=i.indexOf("*R");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*R")+2;	
		}
		t=Character.readC(i.substring(0,c+1),d,seen);
	}
	
	if(i.indexOf("H")==0)
	{
		c=i.indexOf("*H");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*H")+2;	
		}
		////System.out.println("\n\nstr: "+i.substring(0,c+1+1));
		t=Merchant.readM(i.substring(0,c+1),d,seen);	
	}
	
	if(i.indexOf("E")==0)
	{
		c=i.indexOf("*E");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*E")+2;	
		}
		t=Enemy.readE(i.substring(0,c+1),d,seen);
	}
	
	//quester also?
	
	
	d.chars.add(t);
	i=i.substring(c+2);
	}
	//end chars
	
	//add the rest:
	
	for(int j=0;j<seen.size();j++)
	{
		Vector sen=(Vector)(seen.elementAt(j));
		Char c=(Char)(d.chars.elementAt(j));	
		c.createSeen(sen);				
	}
	
		
	
	
	s=s.substring(b+1);
	
	
	for(int j=0;j<temp.size();j++)
	{
		String in=(String)(temp.elementAt(j));
		int ind=Integer.parseInt(in);
		d.fighters.add(d.chars.elementAt(ind));
	}
	
	for(int j=0;j<temp2.size();j++)
	{
		String in=(String)(temp2.elementAt(j));
		int ind=Integer.parseInt(in);
		d.surprize.add(d.chars.elementAt(ind));
	}
	
			
	return d;
	}
		
}
