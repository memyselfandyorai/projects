const {RunSaver}= require('../../../../common/saverController.js');
const {DELETE} = require('../../../../common/saver.js');


await RunSaver(context.params.event, _=> DELETE)

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "save-delete",
  "description": "Deletes saved names.",
  "options": [
    {
      "type": 3,
      "name": "name",
      "description": "Comma-seperated list.  Use * to delete all saved names.",
      "required": true
    }
  ]
});
*/