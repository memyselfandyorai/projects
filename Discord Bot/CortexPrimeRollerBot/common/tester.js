
const common = require('./testing/common.js');
const diceTester = require('./testing/diceTester.js');
const rerollTester = require('./testing/rerollTester.js');
const rollTester = require('./testing/rollTester.js');
const saverTester = require('./testing/saverTester.js');
const stepUpTester = require('./testing/stepUpTester.js');
const keepTester = require('./testing/keepTester.js');
const storageTester = require('./testing/storageTester.js');
const poolsTester = require('./testing/poolsTester.js');
const counterTester = require('./testing/counterTester.js');
const appendTester = require('./testing/appendTester.js');

const TEST_MODULES = [
  diceTester,
  rerollTester,
  rollTester,
  saverTester,
  stepUpTester,
  keepTester,
  storageTester,
  poolsTester,
  counterTester,
  appendTester,
]


const compareObjects = common.compareObjects

module.exports = {
  RunTests: runTests,
};

function createTestCases() {
    
  let testCases = []
  for(let m of TEST_MODULES) {
    testCases.push(...m.TestCases)
  }
  
  return testCases
}

/*

inputOptions: {
  namePrefix - prefix of test names to run
}


output: {
  
  messages - list of strings,
  totalSuccess - int count,
  totalFail - int count,
}
*/
async function runTests(inputOptions) {
  const TEST_CASES = createTestCases()
  let successCount = 0
  let failCount = 0
  let results = []
  
  let testsToRun = TEST_CASES
  if(inputOptions?.namePrefix) {
    testsToRun = TEST_CASES.filter(t => t.name().includes(inputOptions.namePrefix))
  }
  
  let names = new Set()
  for (const testCase of testsToRun) {    
    let expectedResult= testCase.expectedResult()
    let name = testCase.name()
    console.log(`Running test '${name}'`);
    
    let testOutput = undefined
    try {
      if(names.has(name)) {
        throw new Error(`Duplicated test name '${name}'`);
      }
      names.add(name)
      
      let expectedError = testCase.expectedError()
      try {
        testOutput = await testCase.run()
        if(expectedError) {
          throw new Error(`Expected error = ${JSON.stringify(expectedError)}`);
        }
      } catch (error) {        
        if(expectedError && expectedError.type.isPrototypeOf(error) && error.message == expectedError.message) {
          testOutput = undefined
        }
        else {
          throw error
        }     
      }
      
    }
    catch(err) {
      console.log(`Error: ${err}`);
      console.log(`Stacktrace: ${err.stack}`);
      results = results.concat([
      `${name}: FAILED`,
      // JSON.stringify(..., null, 2)
      `    Error: ` + err,
      ])     
      failCount = failCount + 1
      continue
    }
    
    if(compareObjects(testOutput, expectedResult)) {
      results = results.concat([`${name }: SUCCESS`])
      successCount = successCount + 1
    } else {
      results = results.concat([
        `${name}: FAILED`,
        // JSON.stringify(..., null, 2)
        `    Expected: ` + JSON.stringify(expectedResult),
        `    Actual     : ` + JSON.stringify(testOutput),
        ])
      failCount = failCount + 1     
    }
  }
  
  return {
    messages: results,
    totalSuccess: successCount,
    totalFail: failCount ,
  }
}





