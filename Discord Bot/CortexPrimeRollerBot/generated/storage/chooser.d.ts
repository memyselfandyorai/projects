import type { BasicDataStorage } from "./base_storage";
import type { DataStorage } from "../api/storage";
import type { StorageInitParams } from "./base_storage";
declare enum StorageType {
    replit = 0,
    discord = 1,
    memory = 2
}
export declare function getDefaultStorage(): DataStorage;
export declare class StorageChooser {
    type: StorageType;
    constructor(type: string);
    getStorage(params: StorageInitParams): Promise<DataStorage>;
    getUnderlyingStorage(params: StorageInitParams): Promise<BasicDataStorage>;
}
export {};
//# sourceMappingURL=chooser.d.ts.map