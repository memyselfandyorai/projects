﻿namespace FolderSyncronizer.Core.Data
{
    public record FolderSource
    {
        public string Source { get; set; }
        public string Target { get; set; }
    }
}
