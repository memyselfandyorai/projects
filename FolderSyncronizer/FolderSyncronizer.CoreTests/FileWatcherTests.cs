﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using FolderSyncronizer.Core.Watchers;
using FolderSyncronizer.Core.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace FolderSyncronizer.Core.Tests
{
    [TestClass()]
    public class FileWatcherTests
    {
        private readonly ModificationsContainer _modificationContainer;
        private readonly IFolderFilter _folderFilter;
        private readonly List<FolderSource> _toDelete;

        private FileWatcherCollection _fileWatcherCollection;

        public FileWatcherTests()
        {
            // TODO: Test all actions, test folder in folder (test "action equals" as well?)
            // TODO: postsharp license in csproj?
            // TODO: test RunActionOptions.

            _folderFilter = new SubFolderFilter();
            _modificationContainer = new ModificationsContainer();
            _toDelete = new List<FolderSource>();
        }

        [TestMethod()]
        public void InitializationTest()
        {
            StartFolderWatchers(CreateRandomSourceFolders());
        }

        [TestMethod()]
        public void RunNextAction_CreateFile_TargetFileExists()
        {
            var sourceFolders = CreateRandomSourceFolders();
            StartFolderWatchers(sourceFolders);

            string content = "Yorai goes to Yulia";
            string path = CreateRandomSourceFile(sourceFolders, content);
            string newPath = Path.Combine(sourceFolders.Target, Path.GetFileName(path));

            AssertActionsNumber(1, 2);
            Assert.IsTrue(File.Exists(newPath));
            Assert.AreEqual(content, File.ReadAllText(newPath));
        }


        [TestMethod()]
        public void RunNextAction_CopyFile_TargetFileExists()
        {
            var temp = CreateRandomSourceFolders();
            _toDelete.Add(temp);
            var sourceFolders = CreateRandomSourceFolders();
            StartFolderWatchers(sourceFolders);

            string content = "Yorai goes to Yulia";
            string path = CreateRandomSourceFile(temp, content);
            File.Copy(path, Path.Combine(sourceFolders.Source, Path.GetFileName(path)));
         
            string newPath = Path.Combine(sourceFolders.Target, Path.GetFileName(path));

            // It's definitely weird copying creates 2 "modification" events.
            AssertActionsNumber(1, 3);
            Assert.IsTrue(File.Exists(newPath));
            Assert.AreEqual(content, File.ReadAllText(newPath));
        }

        [TestMethod()]
        public void RunNextAction_CopyFileSubFolder_TargetFileExistsForSubFolderOnly()
        {
            var sourceFolders = CreateRandomSourceFolders();
            var subSource = new FolderSource 
            { 
                Source = CreateTempDir(sourceFolders.Source, nameof(RunNextAction_CopyFileSubFolder_TargetFileExistsForSubFolderOnly)), 
                Target = CreateTempDir(pathSuffix: nameof(RunNextAction_CopyFileSubFolder_TargetFileExistsForSubFolderOnly)) 
            };
            StartFolderWatchers(sourceFolders, subSource);

            string content = "Yorai goes to Yulia";
            string path = CreateRandomSourceFile(subSource, content);
         
            string newSubPath = Path.Combine(subSource.Target, Path.GetFileName(path));
            string newParentPath = Path.Combine(sourceFolders.Target, Path.GetFileName(path));

            // It's definitely weird copying creates 2 "modification" events.
            AssertActionsNumber(1, 3);
            Assert.IsTrue(File.Exists(newSubPath));
            Assert.IsFalse(File.Exists(newParentPath));
            Assert.AreEqual(content, File.ReadAllText(newSubPath));
        }

        [TestMethod()]
        public void RunNextAction_ModiifiedFile_TargetFileModified()
        {
            var sourceFolders = CreateRandomSourceFolders();
            string content = "Yorai goes to Yulia woo hoo";
            string path = CreateRandomSourceFile(sourceFolders, "null content");
            StartFolderWatchers(sourceFolders);
            string newPath = Path.Combine(sourceFolders.Target, Path.GetFileName(path));

            File.WriteAllText(path, content);

            AssertActionsNumber(1, 2);
            Assert.IsTrue(File.Exists(newPath));
            Assert.AreEqual(content, File.ReadAllText(newPath));
        }
        
        
        [TestMethod()]
        public void RunNextAction_RenameFile_TargetFileRenamed()
        {
            var sourceFolders = CreateRandomSourceFolders();
            string content = "null content";
            string path = CreateRandomSourceFile(sourceFolders, content);
            string targetPath = Path.Combine(sourceFolders.Target, Path.GetFileName(path));
            File.Copy(path, targetPath);

            StartFolderWatchers(sourceFolders);
            string newNamePath = Path.Combine(sourceFolders.Source, Path.GetRandomFileName());
            File.Move(path, newNamePath);

            AssertActionsNumber(1, 2);
            string targetRenamedPath = Path.Combine(sourceFolders.Target, Path.GetFileName(newNamePath));
            Assert.IsTrue(File.Exists(targetRenamedPath));
            Assert.IsFalse(File.Exists(targetPath));
            Assert.AreEqual(content, File.ReadAllText(targetRenamedPath));
        }

        [TestMethod()]
        public void RunNextAction_DeleteFile_TargetFileRenamed()
        {
            var sourceFolders = CreateRandomSourceFolders();
            string path = CreateRandomSourceFile(sourceFolders, "null content");
            string targetOriginalPath = Path.Combine(sourceFolders.Target, Path.GetFileName(path));
            File.Copy(path, targetOriginalPath);
            StartFolderWatchers(sourceFolders);

            File.Delete(path);

            AssertActionsNumber(1, 2);
            Assert.IsFalse(File.Exists(targetOriginalPath));
            CollectionAssert.AreEqual(new string[] { }, Directory.GetFiles(sourceFolders.Target));
        }

        private void AssertActionsNumber(int min, int max)
        {
            Thread.Sleep(100); // Wait for Watcher to respond.
            int actions = RunUntilFinished();
            Assert.IsTrue(actions >= min && actions <= max, $"Actions: Actial = {actions}, min = {min}, max = {max}");
        }

        private int RunUntilFinished()
        {
            int i = 0;
            while(_modificationContainer.RunNextAction())
            {
                i++;
            }

            return i;
        }


        private FolderSource CreateRandomSourceFolders([CallerMemberName] string callingMethod = "")
        {
            // Get call stack
            StackTrace stackTrace = new StackTrace();
            string suffix = (callingMethod ?? stackTrace.GetFrame(1)?.GetMethod().Name) + ".";
            return new FolderSource { Source = CreateTempDir(pathSuffix: suffix), Target = CreateTempDir(pathSuffix: suffix) };
        }


        private string CreateRandomSourceFile(FolderSource sourceFolder, string content)
        {
            string path = Path.Combine(sourceFolder.Source, Path.GetRandomFileName()) + ".txt";
            File.WriteAllText(path, content);
            return path;
        }

        private string CreateTempDir(string directoryPath = null, string pathSuffix = null)
        {
            string dirName = (pathSuffix ?? "") + Path.GetRandomFileName();
            string path = Path.Combine(directoryPath ?? Path.Combine(Path.GetTempPath(), "FolderSyncronizer.CoreTests"), dirName);
            if (Directory.Exists(path))
            {
                throw new SystemException($"Random directory already exists: {path}");
            }
            Directory.CreateDirectory(path);
            return path;
        }


        [TestCleanup()]
        public void Cleanup()
        {
            _fileWatcherCollection.Dispose();
            _toDelete.ForEach(s =>
            {
                TryDeleteFolder(s.Source);
                TryDeleteFolder(s.Target);
            });
            
            _toDelete.Clear();
        }


        private void TryDeleteFolder(string folder)
        {
            try
            {
                Directory.Delete(folder, true);
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Failed deleting {folder}: {e}");
            }
        }

        private void StartFolderWatchers(params FolderSource[] folderSources)
        {
            StartFolderWatchers(null, folderSources);
        }

        private void StartFolderWatchers(EventHandler<FileActionChangedEventArgs> newFileAction, params FolderSource[] folderSources)
        {
            _fileWatcherCollection = new FileWatcherCollection(_modificationContainer, folderSources);
            _fileWatcherCollection.NewFileAction += newFileAction;
            _toDelete.AddRange(folderSources.ToList());
        }
    }
}