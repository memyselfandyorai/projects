const roller = require('../../../../common/roller.js');
const cache = require('../../../../common/cache.js');
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

let userId = context.params.event.member.user.id
let keep = context.params.event.data.options[0]?.value ?? 3

/*
if (keep == -1) {
  let val = JSON.parse('{"rolls":[{"die":6,"result":3,"originalIndex":0},{"die":6,"result":1,"originalIndex":1},{"die":4,"result":2,"originalIndex":2},{"die":8,"result":2,"originalIndex":3},{"die":10,"result":6,"originalIndex":4}],"diceRoll":[6,6,4,8,10]}')
  await cache.CacheValue(userId, val)
  return;
}
*/
var lastRoll = await cache.GetCacheValue(userId)

messageContent = ['No last roll found']

if (lastRoll) {
  let result = roller.RollCortexPrimeKeep(lastRoll, keep)
  messageContent = result.messageContent
} 


// Write some custom code here

if (context.params.event.token == 'A_UNIQUE_TOKEN') {
  await lib.discord.channels['@0.0.6'].messages.create({
    channel_id: context.params.event.channel_id,
    content: 'DEBUG: ' + messageContent.join('\n') ,
  });
} else {
  await lib.discord.interactions['@1.0.0'].responses.create({
    token: `${context.params.event.token}`,
    response_type: 'DEFERRED_CHANNEL_MESSAGE_WITH_SOURCE',
  });
  
  await lib.discord.interactions['@1.0.0'].followups.create({
    token: `${context.params.event.token}`,
    content: messageContent.join('\n') ,
  });
}