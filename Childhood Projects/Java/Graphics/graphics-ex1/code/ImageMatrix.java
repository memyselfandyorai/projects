import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 *The class represents an Image
 */
public class ImageMatrix {

	
	private int rows,cols;
	
	private int red[][]; //matrix of red color 
	private int blue[][]; //matrix of blue color
	private int green[][]; //matrix of green color
	
	/**
	 * constructs an ImageMatrix from a file 
	 */
	public ImageMatrix(File file)
	{
		try{
		BufferedImage buffer=ImageIO.read(file);
		
		rows=buffer.getHeight();
		cols=buffer.getWidth();
		
		red=new int[rows][cols];
		blue=new int[rows][cols];
		green=new int[rows][cols];
		
		for(int i=0;i<rows;i++)
			for(int j=0;j<cols;j++)
			{
				int rgb=buffer.getRGB(j,i); //(x,y) = (j,i)  [i:rows, x: cols]
				
							
				Color color=new Color(rgb,true);
				
				blue[i][j]=color.getBlue();
				red[i][j]=color.getRed();
				green[i][j]=color.getGreen();
								
			}
		
		}catch(IOException e){e.printStackTrace();}
	}
	
	/**
	 * returns number of rows
	 * */
	public int getRows()
	{
		return rows;
	}
	
	/**
	 * 
	 * returns number of columns 
	 */
	public int getCols()
	{
		return cols;
	}
	
	/**
	 * returns RGB color at coordinate (i,j)
	 */
	public int getRGB(int i,int j)
	{
		Color c=new Color(red[i][j],green[i][j],blue[i][j]);
		return c.getRGB();
	}
	
	/**
	 * returns color at coordinate (i,j) according to 'rgb' 
	 */
	public int getColor(int i,int j,RGB rgb)
	{
		
		if(i>=0&&i<rows&&j>=0&&j<cols)
		{
			switch(rgb)
			{
			case RED:
				return red[i][j];
			case BLUE:
				return blue[i][j];
			case GREEN:
				return green[i][j];
			}
		
		}
		
		Main.error("wrong parameters for getcolor: "+i+","+j+"\trows/cols: "+rows+"/"+cols);
			
		return -1;
	}
	
	/**
	 * returns true iff the color in coordinate (i,j) in 'mask' is black
	 */
	public static boolean isMissing(int i,int j,ImageMatrix mask)
	{
		return mask.isBlack(i, j);
	}
	
	/**
	 * returns true iff coordinate (i,j) is black 
	 */
	private boolean isBlack(int i,int j)
	{
		if(i>=0&&i<rows&&j>=0&&j<cols)
		{
			Color c=new Color(red[i][j],green[i][j],blue[i][j]);
			return c.equals(Color.black);
		}
		
		return false;
	}
	
	
	/**
	 * enumerator 'RGB'
	 */
	public static enum RGB
	{
		GREEN,BLUE,RED;
	}
	
}
