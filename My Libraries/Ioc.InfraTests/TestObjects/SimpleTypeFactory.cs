﻿using Ioc.Infra.Ioc;
using Ioc.Infra.Mef;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Ioc.InfraTests.TestObjects
{
    public class SimpleTypeFactory : MefFactory<IKeyedObject, MyType, IKeyedObject>, ITypeFactory
    {
        private readonly Lifecycle _lifecycle;

        public bool DisposedCalled { get; private set; }
        public ISimpleInterface OptionalImplementation { get; private set; }

        private const string ASSEMBLY_PREFIX = "Sync-op-App";

        public SimpleTypeFactory(IIocContainer optionalContainer) : base(convertSingleton: import => CreateNewImport(import, optionalContainer), ASSEMBLY_PREFIX)
        {
            this._lifecycle = Lifecycle.Singleton;
            this.OptionalImplementation = optionalContainer.Resolve<ISimpleInterface>();
        }

        internal SimpleTypeFactory(Lifecycle lifecycle) : base(convertSingleton: null, ASSEMBLY_PREFIX)
        {
            this._lifecycle = lifecycle;
        }

        public override void Dispose()
        {
            base.Dispose();
            this.DisposedCalled = true;
        }

        public IEnumerable<IKeyedObject> GetAll()
        {
            return CreateAll();
        }

        public IKeyedObject GetByType(MyType type)
        {
            return Create(type);
        }

        protected override IKeyedObject ConvertImport(IKeyedObject import)
        {
            switch (_lifecycle)
            {
                case Lifecycle.Singleton:
                    // Reuse object
                    return import;
                case Lifecycle.Transient:
                    // Create new instance each time
                    return CreateNewImport(import);
                default:
                    throw new NotImplementedException();
            }
        }

        protected override MyType GetKey(IKeyedObject import)
        {
            return import.Type;
        }
    }
}
