import java.util.*;
import java.awt.*;
import javax.swing.*;

public class Player {
	
	
	private String name;
	
	private boolean hu;
	
	private Vector<Card> cards;
	
	private Card[] hid,sen;
	
	
	public Player(String n,boolean h)
	{
	hu=h;
		
	name=n;
	
	cards=new Vector<Card>();
	hid=new Card[3];
	sen=new Card[3];
	}
	
	public Vector cards()
	{
	return cards;	
	}
	
	
	public boolean human()
	{
	return hu;		
	}
	
	public String name()
	{
	return name;	
	}
	
	public int getHid()
	{
	int s=0;
	for(int i=0;i<3;i++)
	if(hid[i]!=null)
	s++;
	
	return s;
	}
	
	
	
	public void addLabels(JPanel p)
	{
	JPanel n=new JPanel();
	n.setLayout(new GridLayout(1,0));
	
	for(int i=0;i<3;i++)
	if(sen[i]!=null)
	{
	JLabel j=new JLabel(sen[i].getIcon(p));
	n.add(j);	
	}
	p.add(n);
	}
	
	
	public void addButtons(JPanel p,boolean se,Game game)
	{
	JPanel n=new JPanel();
	n.setLayout(new GridLayout(1,0));
	
	boolean seen=false;
	
	if(se)
	{
		for(int i=0;i<3;i++)
		if(sen[i]!=null)
		{
		JButton b=new JButton(sen[i].getIcon(p));
		b.addActionListener(game);
		b.setToolTipText("Your seen cards");
			if(cards.size()>0)
			b.setEnabled(false);
		b.setActionCommand(""+i);
		n.add(b);
		seen=true;
		}
	}
	else
	{
		for(int i=0;i<3;i++)
		if(hid[i]!=null)
		{
		JButton b=new JButton(Card.getUpside(p));
		b.addActionListener(game);
		b.setToolTipText("Your hidden cards");
			if(cards.size()>0||!noSe())
			b.setEnabled(false);
		b.setActionCommand(""+i);
		n.add(b);	
		}
	}
	
	p.add(n);
	}
	
	
	public void dealStart(Pack p)
	{
	hid[0]=p.draw();
	hid[1]=p.draw();
	hid[2]=p.draw();
	
	sen[0]=p.draw();
	sen[1]=p.draw();
	sen[2]=p.draw();
	
	drawCard(p,3);	
	}
	
	
	public void drawCard(Pack p)
	{
	Card d=p.draw();
	
	int i=0;
	
	for(i=0;i<cards.size();i++)
	{
	Card c=(Card)cards.elementAt(i);	
	if(c.num()>d.num())
	break;		
	}
	
	cards.add(i,d);
	}
	
	public void drawCard(Pack p,int n)
	{
	for(int i=0;i<n;i++)
	if(p.cards().size()>0)
	drawCard(p);
	}
	
	
	public void hit(Pack p,int ind)
	{
	//System.out.println("cards3: "+cards);
	Card d=(Card)cards.elementAt(ind);
	cards.remove(ind);
	p.addCard(d);
	}
	
	
	
	public Vector<String> hit(int ca,int num)
	{
	Vector<String> n=new Vector<String>();
	for(int j=0;j<num;j++)
	{	
		for(int i=0;i<cards.size();i++)
		{
		Card d=(Card)cards.elementAt(i);
			if(d.num()==ca)
			{
			n.add(""+i);
			break;
			}
		}
	}
	return n;
	}
	
	public void hit(Pack p,int ca,int num)
	{
	
	for(int j=0;j<num;j++)
	{	
		for(int i=0;i<cards.size();i++)
		{
		Card d=(Card)cards.elementAt(i);
			if(d.num()==ca)
			{
			hit(p,i);
			break;
			}
		}
	}
	
	}
		
	
	
	public void hit(Pack p,int ca,int num,Vector<String> nums)
	{
	
	for(int j=0;j<num;j++)
	{	
		for(int i=0;i<cards.size();i++)
		{
		Card d=(Card)cards.elementAt(i);
			if(d.num()==ca)
			{
			//hit(p,i);
			nums.add(i+"");
			break;
			}
		}
	}
	
	}
	
	
	public void hitT(Pack p,int ind,boolean s)
	{
	if(s)
	{
		p.addCard(sen[ind]);
		sen[ind]=null;
	}
	else
	{
		p.addCard(hid[ind]);
		hid[ind]=null;
	}
	}
	
	public void hitT(Pack p,int ca,int num)
	{
	for(int i=0;i<3&&num>0;i++)
	{
	Card d=sen[i];
		
		if(sen[i]!=null)
		if(d.num()==ca)
		{
		num--;
		hitT(p,i,true);
		}
	}
	}
	
	public Vector<String> hitT(int ca,int num)
	{
	Vector<String> n=new Vector<String>();
		for(int i=0;i<3&&num>0;i++)
		{
		Card d=sen[i];
			
			if(sen[i]!=null)
			if(d.num()==ca)
			{
			num--;
			n.add(""+i);
			}
		}
	return n;
	}
	
	
	public void hitT(Pack p,int ca,int num,Vector<String> nums)
	{
	for(int i=0;i<3&&num>0;i++)
	{
	Card d=sen[i];
		
		if(sen[i]!=null)
		if(d.num()==ca)
		{
		num--;
		//hitT(p,i,true);
		nums.add(""+i);
		}
	}
	}
	
	
	public int howMany(int n)
	{
	int s=0;	
	
	for(int i=0;i<cards.size();i++)
	if(n==((Card)cards.elementAt(i)).num())
	s++;

	return s;
	}
	
	public int howManyS(int n)
	{
	int s=0;	
	
	for(int i=0;i<3;i++)
	if(sen[i]!=null)
	if(n==sen[i].num())
	s++;

	return s;
	}
	
	
	
	public Card getCard(boolean s,int a)
	{
	if(s)
	return sen[a];
	return hid[a];
	}
	
	
	public boolean noEx()
	{
	for(int i=0;i<3;i++)
	if(sen[i]!=null||hid[i]!=null)
	return false;	
	
	return true;	
	}
	
	public boolean noSe()
	{
	for(int i=0;i<3;i++)
	if(sen[i]!=null)
	return false;	
	
	return true;	
	}
	
	
	public boolean finished()
	{
	return noEx()&&cards.size()==0;		
	}
	
	
	public int choose(int n)
	{
	
	int h=-1,e=-1,w=-1;
	
	for(int i=0;i<cards.size();i++)
	{
	Card c=(Card)cards.elementAt(i);	
	int m=c.num();	
	
	if(m!=3&&m!=2&&m!=10)
	{
		if(n!=7)
		{
			if(m>=n)
			return i;
		}
		else
		{
			if(m<=n)
			return i;
		}
	}
	else
	{
		if(m==3)
		h=i;
		if(m==2)
		w=i;
		if(m==10)
		e=i;
	}
	
	}	
		
	if(w!=-1)
	return w;
	if(h!=-1)
	return h;
	if(e!=-1)
	return e;
		
	return -1;	
	}
	
	
	
	public boolean hasOnlySp()
	{
	for(int i=0;i<cards.size();i++)
	{
	Card c=(Card)cards.elementAt(i);	
	if(c.num()!=3&&c.num()!=2&&c.num()!=10)
	return false;
		
	}	
	
	return true;
	}
	
	
	
	
	
	public int choose(int n,boolean se)
	{
	
	if(se)
	{
		int h=-1,e=-1,w=-1;
		
		for(int i=0;i<3;i++)
		{
		Card c=sen[i];	
		if(c!=null)
		{
			int m=c.num();	
			
			if(m!=3&&m!=2&&m!=10)
			{
				if(n!=7)
				{
					if(m>=n)
					return i;
				}
				else
				{
					if(m<=n)
					return i;
				}
			}
			else
			{
				if(m==3)
				h=i;
				if(m==2)
				w=i;
				if(m==10)
				e=i;
			}
		}
		
		}	
			
		if(w!=-1)
		return w;
		if(h!=-1)
		return h;
		if(e!=-1)
		return e;
			
		return -1;	
	}
	else //!seen, chooses the first one he didn't choose yet.
	{
	for(int i=0;i<3;i++)
	if(hid[i]!=null)
	{
		int mm=hid[i].num();
	
		if(mm==3||mm==2||mm==10||mm==n)
		return i;
		
		if(mm>n&&n!=7)
		return i;
		
		if(mm<n&&n==7) 
		return i;
				
		return -(i+1);	
	}
	
	return -1;
	}
	
	
	
	}
	
	
	public int getHidden(int i)
	{
	return hid[i].num();	
	}
	
	public int getSeen(int i)
	{
	return sen[i].num();	
	}
	
	
	public String write()
	{
	String s="P*";
	for(int i=0;i<3;i++)
	if(sen[i]!=null)
	s+=sen[i].write();
	else
	s+=sen[i]+"*C";
	
	for(int i=0;i<3;i++)
	if(hid[i]!=null)
	s+=hid[i].write();
	else
	s+=sen[i]+"*C";
	
	s+=name+",";	
	s+=hu+",";	
	
	for(int i=0;i<cards.size();i++)
	{
	Card c=(Card)cards.elementAt(i);
	s+=c.write();	
	}
			
	return s+"*P";	
	}
	
	
	public static Player read(String s)
	{
	s=s.substring(2);
	int b;
	Player player=new Player("",false);
	
	for(int i=0;i<3;i++)
	{
	b=s.indexOf("*C");
	String ch=s.substring(0,b);
	if(ch.equals("null"))
	player.sen[i]=null;
	else
    player.sen[i]=Card.read(ch);
   	s=s.substring(b+2);	
	}	
		
	for(int i=0;i<3;i++)
	{
	b=s.indexOf("*C");
	String ch=s.substring(0,b);
	if(ch.equals("null"))
	player.hid[i]=null;
	else
    player.hid[i]=Card.read(ch);
   	s=s.substring(b+2);	
	}		
		
	b=s.indexOf(",");
    player.name=s.substring(0,b);
   	s=s.substring(b+1);	
	
	b=s.indexOf(",");
    player.hu=s.substring(0,b).equals("true");
   	s=s.substring(b+1);		
	
	while((b=s.indexOf("*C"))!=-1)
	{
	player.cards.add(Card.read(s.substring(0,b)));
   	s=s.substring(b+2);			
	}
	
	return player;		
	}
	
}