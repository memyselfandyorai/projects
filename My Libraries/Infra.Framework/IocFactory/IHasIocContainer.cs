﻿using Infra.Framework.Ioc;

namespace Infra.Framework.IocFactory
{
    public interface IHasIocContainer
    {
        IIocContainer Container { get; }
    }
}
