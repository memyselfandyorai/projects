const PAYEE_COLUMN = 4;
const CATEGORY_COLUMN = 3;
const ACCOUNT_COLUMN = 2;
const DATE_COLUMN = 1;
const AMOUNT_COLUMN = 5;
const NOTE_COLUMN = 6;


function onOpen() {
  var sheet = SpreadsheetApp.getActiveSheet(); // Get spreadsheet name 
  var farthestRange = sheet.getRange(sheet.getLastRow() + 1, PAYEE_COLUMN);
  var extra = sheet.getRange(sheet.getLastRow() + 20, PAYEE_COLUMN);
  sheet.setActiveRange(extra);
  SpreadsheetApp.flush();
  sheet.setActiveRange(farthestRange);

  onOpenSummary()
}


// https://developers.google.com/apps-script/guides/triggers/events#google_sheets_events
function onEdit(e) {
  var sheet = SpreadsheetApp.getActiveSheet(); // Get spreadsheet name 
  var sheetName = sheet.getSheetName()

  let valueDefaults = getDefaults()

  if(valueDefaults.nonExpenseSheetNameList.find(substr => sheetName.includes(substr))) {
    return
  }
  if(!e.value) {
    // If more than one cell editted, or the new cell value is empty, no need to do anything.
    return
  }

  var cell = sheet.getActiveCell(); // store active cell name in current spreadsheet 
  var row = cell.getRow();
  var column = cell.getColumn();

  if (column != PAYEE_COLUMN) {
    // Only handle changes in payee.
    return;
  }
  
  var payee = sheet.getRange(row, PAYEE_COLUMN).getValue();
  Logger.log('Set payee: ' + payee)

  // Set date:
  if (payee != null && payee != "" && sheet.getRange(row, DATE_COLUMN).getValue() == "") {
    // Take date from below if exists
    var dateFromBelow = sheet.getRange(row + 1, DATE_COLUMN).getValue();
    if (dateFromBelow != null && dateFromBelow != "") {
      sheet.getRange(row, DATE_COLUMN).setValue(dateFromBelow);
    } else {
      // Set date as "now".
      // The format in the sheet fixes it, but this way it's recognized as real date and not just string
      var date = Utilities.formatDate(new Date(), Session.getScriptTimeZone() , "MM/dd/yyyy"); 
      sheet.getRange(row, DATE_COLUMN).setValue(date);
    }
  }

  Logger.log('Set date: ' + payee)
    
  if (payee != null && payee != "" && sheet.getRange(row, ACCOUNT_COLUMN).getValue() == "") {
    let payeeDefaultAccount = valueDefaults.payeeAccountMap[payee]
    sheet.getRange(row, ACCOUNT_COLUMN).setValue(payeeDefaultAccount ?? valueDefaults.defaultAccount)
  }
  
  
  
  var amount = valueDefaults.payeeAmountMap[payee];
  if (amount != null && sheet.getRange(row, AMOUNT_COLUMN).getValue() == "") {
    sheet.getRange(row, AMOUNT_COLUMN).setValue(amount);
  } 

  Logger.log('Set amount: ' + amount)

  var category = valueDefaults.payeeCategoryMap[payee];
  let categoryCell = sheet.getRange(row, CATEGORY_COLUMN)
  var existingCategory = categoryCell.getValue()
  var note = valueDefaults.payeeDescriptionMap[payee];
  var noteCell = sheet.getRange(row, NOTE_COLUMN)
  var existingNote = noteCell.getValue()

  let existingRow = undefined
  if([category, existingCategory, note, existingNote].every(v => !v)) {
    Logger.log('Getting row')
    existingRow = getExistingRow(sheet, row, payee)
  }
  
  
  if (!existingCategory) {
    if (category) {
      // Take category from map.
      categoryCell.setValue(category);
    } else {
      if(existingRow) {
        sheet.getRange(existingRow, CATEGORY_COLUMN).copyTo(categoryCell)
      }
    }
  }

  if (!existingNote) {
    if (note) {
      // Take note from map.
      noteCell.setValue(note);
    } else {
      if(existingRow) {
        sheet.getRange(existingRow, NOTE_COLUMN).copyTo(noteCell)
      }
    }
  }
}

/**
 * Returns a row number (or undefined if not exists) in which the payee was last seen in.
 * This can be after or before the row (prioritizes after).
 */
function getExistingRow(sheet, row, payee) {
  // Take from last shown.
  var rangeBefore = sheet.getRange(1, PAYEE_COLUMN, row - 1);
  var rangeAfter = sheet.getRange(row + 1, PAYEE_COLUMN, sheet.getLastRow());
  var result = getIndexIfPayeeExists(rangeAfter, row, payee, false);
  if (!result.found) {
    result = getIndexIfPayeeExists(rangeBefore, row, payee, true);
  }

  return result.found ? result.row : undefined
}

// Returns true if set
function getIndexIfPayeeExists(range, row, payee, isPrevious) {
  var foundIndex = range.getValues().reverse().findIndex(([r]) => r == payee);
  Logger.log("previous: " + isPrevious);
  if (foundIndex == -1) {
    return {
      found: false,
    }
  }
  Logger.log("foundIndex: " + foundIndex);
  var lookupRow = isPrevious ? row - foundIndex - 1 : row + range.getValues().length - foundIndex;
  Logger.log("lookupRow: " + lookupRow);
  return {
    found: true,
    row: lookupRow,
  }
}

