import type { BasicDataStorage } from "./base_storage";
import type { StorageItem } from "../api/storage";
import Database from "@replit/database";

export class ReplitDatabase implements BasicDataStorage {
  db: Database;
  constructor() {
    this.db = new Database();
  }

  async setItem(key: string, value: StorageItem): Promise<void> {
    await this.db.set(key, value);
  }

  async getAllItemKeys(): Promise<string[]> {
    return await this.db.list();
  }

  async getItem(key: string): Promise<StorageItem | null | undefined> {
    return (await this.db.get(key)) as StorageItem;
  }

  async deleteItem(key: string): Promise<void> {
    await this.db.delete(key);
  }
}
