const controller = require("./controller.js");
const { ReplyToCommand, ReceivedCommand } = require("./commands.js");

const TYPE_VALUE_TO_NAME = {
  1: "pp",
  2: "xp",
};

/*
input: {
}
*/
async function showCounters(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let inputOptions = {
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  };

  let messageContent = await controller.RunSafe(() =>
    controller.ShowCounters(inputOptions, commandInteraction.getIocContainer()),
  );
  await replyToCommand(commandInteraction, messageContent, "Show counters");
}

/*
input: {
}
*/
async function setCounter(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let inputOptions = {
    who: parsedInput.who,
    counterName: parsedInput.counter_name,
    value: parsedInput.value,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  };

  let messageContent = await controller.RunSafe(() =>
    controller.SetCounter(inputOptions, commandInteraction.getIocContainer()),
  );
  await replyToCommand(commandInteraction, messageContent, "Set counter");
}

async function modifyCounter(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let inputOptions = {
    who: parsedInput.who,
    counterName: parsedInput.counter_name,
    amount: parsedInput.amount,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  };

  let messageContent = await controller.RunSafe(() =>
    controller.ModifyCounter(
      inputOptions,
      commandInteraction.getIocContainer(),
    ),
  );
  await replyToCommand(commandInteraction, messageContent, "Modify counter");
}

/*
input: {
}
*/
async function removeCounter(input, commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let parsedInput = parseInput(commandInteraction);
  let inputOptions = {
    who: parsedInput.who,
    counterName: parsedInput.counter_name,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  };

  let messageContent = await controller.RunSafe(() =>
    controller.RemoveCounter(
      inputOptions,
      commandInteraction.getIocContainer(),
    ),
  );
  await replyToCommand(commandInteraction, messageContent, "Remove counter");
}

async function replyToCommand(commandInteraction, messageContent, commandName) {
  console.log(commandName + " result: " + messageContent.join("\n"));
  await ReplyToCommand(commandInteraction, messageContent);
  await updateSummaryMessage(commandInteraction);
}

/* 
Returns input: {
  who, counter_name
  amount, value
  channel_id, user_id (number - defaults to 0)
  }
  
  Strings default to '', numbers default to undefined.
*/
function parseInput(commandInteraction) {
  var inputMap = commandInteraction.getInputOptionsMap();

  let defaultWho = commandInteraction.user();

  let name = inputMap.get("name")?.trim() ?? "pp";
  let type = inputMap.get("type") ?? 0;
  let overridenName = TYPE_VALUE_TO_NAME[type];
  if (overridenName) {
    name = overridenName;
  }

  return {
    who: inputMap.get("who") ?? defaultWho,
    counter_name: name,
    amount: inputMap.get("amount") ?? 1,
    value: inputMap.get("value") ?? 2,
    channel_id: commandInteraction.getChannelId() ?? "",
    user_id: commandInteraction.getUserId() ?? 0,
  };
}

async function updateSummaryMessage(commandInteraction) {
  let updateInput = {
    event: commandInteraction,
  };
  await controller.UpdatePinnedSummaryMessage(
    updateInput,
    commandInteraction.getIocContainer(),
  );
}

module.exports = {
  ShowCounters: showCounters,
  SetCounter: setCounter,
  RemoveCounter: removeCounter,
  ModifyCounter: modifyCounter,
};
