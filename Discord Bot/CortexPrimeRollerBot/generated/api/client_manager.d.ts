export interface DiscordClientInteraction {
    sendJoinMessage(): Promise<void>;
}
//# sourceMappingURL=client_manager.d.ts.map