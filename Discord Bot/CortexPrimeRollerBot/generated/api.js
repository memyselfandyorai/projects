"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeJsDiscordClientInteraction = exports.NodeJsPinManager = exports.NodeJsPrefixCommandInteraction = exports.NodeJsCommand = void 0;
const discord_js_1 = require("discord.js");
const messages_1 = require("./api/messages");
// REMINDER: manually generate using "npx tsc".
// Docs: https://discord.js.org/docs/packages/discord.js/14.14.1/ChatInputCommandInteraction:Class#channel
class NodeJsCommand {
    constructor(interaction, iocContainer) {
        Object.defineProperty(this, "interaction", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "messageSender", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "iocContainer", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.interaction = interaction;
        this.messageSender = new NodeJsMessageSender(interaction.channel);
        this.iocContainer = iocContainer;
    }
    getIocContainer() {
        return this.iocContainer.getIocContainer();
    }
    getAllInputOptions() {
        return this.interaction.options.data.map((option) => option.name);
    }
    inputOptions(...names) {
        return names.map((name) => ({
            name,
            value: this.interaction.options.get(name)?.value,
        }));
    }
    getInputOptionsMap() {
        var inputMap = new Map();
        this.getAllInputOptions().forEach((optionName) => {
            inputMap.set(optionName, this.inputOptions(optionName)[0].value);
        });
        return inputMap;
    }
    user() {
        return (this.interaction.member?.nickname ??
            this.interaction.user.globalName ??
            this.interaction.user.username);
    }
    getUserId() {
        return this.interaction.user.id;
    }
    getChannelId() {
        return this.interaction.channelId;
    }
    getMessageId() {
        return this.interaction.id;
    }
    getPinManager() {
        return new NodeJsPinManager(this.interaction);
    }
    async send(message, options) {
        return await this.messageSender.send(message, options);
    }
    async prepareCommandReply() {
        await this.interaction.deferReply();
    }
    async reply(message) {
        const msg = await this.interaction.editReply(message.text);
        return msg.id;
    }
    toString() {
        return `${this.interaction.toString()}, token ${this.interaction.token}`;
    }
}
exports.NodeJsCommand = NodeJsCommand;
class NodeJsPrefixCommandInteraction {
    constructor(message, adminId, iocContainer) {
        Object.defineProperty(this, "message", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "messageSender", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "adminId", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "iocContainer", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.message = message;
        this.messageSender = new NodeJsMessageSender(message.channel);
        this.adminId = adminId;
        this.iocContainer = iocContainer;
    }
    getIocContainer() {
        return this.iocContainer.getIocContainer();
    }
    botId() {
        return this.message.client.user.id;
    }
    getAdminId() {
        return this.adminId;
    }
    senderId() {
        return this.message.author.id;
    }
    getMessageId() {
        return this.message.id;
    }
    generateInvite() {
        const link = this.message.client.generateInvite({
            permissions: [
                discord_js_1.PermissionFlagsBits.ViewChannel,
                discord_js_1.PermissionFlagsBits.CreateInstantInvite,
                discord_js_1.PermissionFlagsBits.ChangeNickname,
                discord_js_1.PermissionFlagsBits.SendMessages,
                discord_js_1.PermissionFlagsBits.ManageMessages,
                discord_js_1.PermissionFlagsBits.UseApplicationCommands,
                discord_js_1.PermissionFlagsBits.ReadMessageHistory,
            ],
            scopes: [
                discord_js_1.OAuth2Scopes.Identify,
                discord_js_1.OAuth2Scopes.Bot,
                discord_js_1.OAuth2Scopes.ApplicationsCommands,
            ],
        });
        return link;
    }
    async send(message, options) {
        return await this.messageSender.send(message, options);
    }
}
exports.NodeJsPrefixCommandInteraction = NodeJsPrefixCommandInteraction;
class NodeJsMessageSender {
    constructor(channel) {
        Object.defineProperty(this, "channel", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.channel = channel;
    }
    async send(message, options) {
        const reply = options?.reply_to_message_id
            ? {
                messageReference: options?.reply_to_message_id,
                // failIfNotExists: false,
            }
            : undefined;
        const embeds = options?.embed ? [options.embed] : [];
        const createOptions = {
            content: message.text,
            reply: reply,
            embeds: embeds,
        };
        if (options.extra_data) {
            Object.assign(createOptions, options.extra_data);
        }
        const createdMessage = await this.channel?.send(createOptions);
        return createdMessage?.id ?? "";
    }
}
class NodeJsPinManager {
    constructor(interaction) {
        Object.defineProperty(this, "interaction", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.interaction = interaction;
    }
    async fetchAllPinnedMessages(channelId) {
        const messages = (await this.interaction.channel?.messages.fetchPinned()) ?? [];
        const pinnedMessages = [];
        messages.forEach((message) => pinnedMessages.push(message));
        return pinnedMessages.map((msg) => ({
            id: msg.id,
            timestamp: msg.createdAt,
            content: { text: msg.content },
        }));
    }
    async deletePinnedMessage(channelId, messageId) {
        try {
            await this.interaction.channel?.messages.unpin(messageId);
        }
        catch (error) {
            const discordError = error;
            if (discordError) {
                if (discordError.code == 50013) {
                    throw new messages_1.NoPermissionsError("No UNPIN permissions for bot", {
                        cause: error,
                    });
                }
            }
        }
    }
    async pinMessage(channelId, messageId) {
        await this.interaction.channel?.messages.pin(messageId);
    }
    async updatePinnedMessage(channelId, messageId, message) {
        await this.interaction.channel?.messages.edit(messageId, message.text);
    }
}
exports.NodeJsPinManager = NodeJsPinManager;
class NodeJsDiscordClientInteraction {
    constructor(client, guild) {
        Object.defineProperty(this, "client", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "guild", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.client = client;
        this.guild = guild;
    }
    async sendJoinMessage() {
        const channels = await this.guild.channels.fetch();
        let generalChannel = channels.find((channel) => channel.name.toLowerCase() === "general" &&
            channel.type === discord_js_1.ChannelType.GuildText);
        if (!generalChannel) {
            generalChannel = channels.find((channel) => {
                return channel.type === discord_js_1.ChannelType.GuildText;
            });
        }
        let deployedMessageContact = [
            `Thanks for installing this Discord bot.`,
            `Use the slash-commands to see which commands are available.`,
            `The most basic command is **/r**.`,
            `You can also use **/help** or read more on https://sites.google.com/view/youreye-communityprojects/cortex-prime-discord-bot.`,
            `By using this bot, you're agreeing to its Privacy Agreement (https://sites.google.com/view/youreye-communityprojects/cortex-prime-discord-bot#h.jhkjxyd40jqi).`,
        ];
        if (generalChannel) {
            console.log("Sending deploy message");
            await generalChannel.send(deployedMessageContact.join("\n"));
        }
        else {
            console.log("No text channel found");
        }
    }
}
exports.NodeJsDiscordClientInteraction = NodeJsDiscordClientInteraction;
/*
// iterating existing channels:
const joinDeltaMiliseconds = 10 * 60 * 1000; // 10 minutes

// TODO: client.generateInvite?
this.client.guilds.cache.forEach((guild) => {
  console.log(`Guild Name: ${guild.name} (ID: ${guild.id})`);

  // You can loop through channels here as well
  guild.channels.cache.forEach((channel) => {
    console.log(`  - Channel Name: ${channel.name} (ID: ${channel.id})`);
    const now = new Date().getTime();
    const joined = guild.joinedAt.getTime();
    if (
      channel.type == ChannelType.GuildText &&
      now < joined + joinDeltaMiliseconds
    ) {
      console.log(`text: ` + guild.joinedAt);

      // TODO: need to copy the "joined" logic, checking for "general" or similar text (per guild).
      // TODO: how to catch it when server in ON ?

      // channel.send("Hello from your new bot!"); ??
    }
  });
});
*/
//# sourceMappingURL=api.js.map