package General;

import java.util.HashSet;
import java.util.Set;

import Objects.Surface;

/**
 * 
 * The class represents a Binary Space Partitioning tree
 *
 */
public class BSPTree {

	private Plane plane;
	private BSPTree right;
	private BSPTree left;
	private Set<Surface> primitives;
	private boolean leaf; 
	
	public static final BSPTree EMPTY_TREE=null;
	
	
	//findPartitionPlane() -  the only thing not finished!
	
	
	public BSPTree(Set<Surface> set)
	{
		primitives=set;
		
		if(set.size()>1)
			divideTree();
		else
			leaf=true;
	}
	
	
	public BSPTree getRight() {
		return right;
	}

	public BSPTree getLeft() {
		return left;
	}

	private Intersection intersect(Scene scene,Ray ray) {
		
		return Scene.findIntersectionNaive(scene,primitives, ray);
	}
	
	public boolean isLeaf()
	{
		return leaf;
	}
	
	
	
	private void divideTree()
	{
		plane=findPartitionPlane();
		
		if(plane==null)
		{
			leaf=true;
			return;
		}
		
		Set<Surface> rights=new HashSet<Surface>();
		Set<Surface> lefts=new HashSet<Surface>();
		
		for(Surface s:primitives)
		{
			//same side with normal of place : right side ("up")			
			if(s.onRight(plane))
				rights.add(s);
			else
				lefts.add(s);
		}
		
		if(rights.isEmpty())
			right=EMPTY_TREE;
		else
			right=new BSPTree(rights);
		
		if(lefts.isEmpty())
			left=EMPTY_TREE;
		else
			left=new BSPTree(lefts);

	}
	
	
	/**
	 * finds intersection via tree
	 * */
	public Intersection findIntersection(Scene scene,Ray ray)
	{
		if(isLeaf())
		{
			return intersect(scene,ray);
		}
		
		BSPTree raySide;
		BSPTree otherSide;
		
		if(plane.onRight(ray.getPoint()))
		{
			raySide=right;
			otherSide=left;
		}
		else
		{
			raySide=left;
			otherSide=right;
		}
		
		if(raySide!=EMPTY_TREE)
		{
			Intersection inter=raySide.findIntersection(scene,ray);
			if(inter!=Intersection.MISS)
				return inter;
		}
		else
			return Intersection.MISS;
				
		
		Point newPoint=plane.hit(ray);
		Ray newRay=new Ray(newPoint, ray.getVector());
		//clip the ray at the plane
		if(otherSide!=EMPTY_TREE)
			return otherSide.findIntersection(scene,newRay);
		
		//else
		return Intersection.MISS;
	}
	
	
	/**
	 * 
	 * finds a partition plane:
	 * a plane which seperates primitives and doesn't intersect any of them 
	 * 
	 * impelementation unknown. couldn't find it on the internet, and no one answered mails regarding this
	 * (and of course it had no mention in the lecture slides)
	 * */
	private Plane findPartitionPlane()
	{
		return null;		
	}
	
	
	
}
