import type { BasicDataStorage } from "./base_storage";
import type { StorageItem } from "../api/storage";
import Database from "@replit/database";
export declare class ReplitDatabase implements BasicDataStorage {
    db: Database;
    constructor();
    setItem(key: string, value: StorageItem): Promise<void>;
    getAllItemKeys(): Promise<string[]>;
    getItem(key: string): Promise<StorageItem | null | undefined>;
    deleteItem(key: string): Promise<void>;
}
//# sourceMappingURL=replit_storage.d.ts.map