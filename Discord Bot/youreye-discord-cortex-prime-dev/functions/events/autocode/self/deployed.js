// authenticates you with the API standard library
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});
const GUILD_TEXT_CHANNEL_TYPE = 0;

let guilds = await lib.discord.guilds['@0.0.6'].list({
  limit: 100
});

let channels = await lib.discord.guilds['@0.0.6'].channels.list({
  guild_id: guilds[0].id
});

let generalChannel = channels.find((channel) => {
  return channel.name === 'general' && channel.type == GUILD_TEXT_CHANNEL_TYPE;
});


// Useful URLs:
// (useful for updating avatar) https://discord.com/developers/applications
// https://autocode.com/guides/how-to-build-a-discord-bot/
// https://autocode.com/tools/discord/command-builder
// https://lodash.com/docs/4.17.15#sortBy
// APIs: https://autocode.com/lib/discord/channels/#pins-destroy
/*
- [Official Guide to Building Discord Bots on Autocode](https://autocode.com/guides/how-to-build-a-discord-bot/)
- [The Discord Slash Command Builder](https://autocode.com/discord-command-builder/)
- [Formatting Discord messages](https://discord.com/developers/docs/reference#message-formatting)
- [Discord slash command docs](https://discord.com/developers/docs/interactions/slash-commands)
- [Discord developer portal](https://discord.com/developers/applications)
- [Autocode discord/commands API page for creating slash commands](https://autocode.com/lib/discord/commands/)
- [How to find your Discord guild id](https://support.discord.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-)

If you have any questions or feedback, please join our community Discord server 
from the link in the top bar. You can also follow us on Twitter, [@AutocodeHQ](https://twitter.com/@AutocodeHQ).
*/

// message.guild.members.get(bot.user.id).setNickname("CortextPrime Roller Bot");

/*
Avatar: https://buyourobot.b-cdn.net/storage/edd/2015/10/00573_cute_bot_thumb_alpha.jpg
Nick: CortexPrimeRollerBot
*/

let deployedMessageContact = [
  `Thanks for installing this Autocode-powered Discord bot.`,
]

if (generalChannel) {
  // let botInfo = await lib.discord.users['@0.0.6'].me.list();
  await lib.discord.channels['@0.0.6'].messages.create({
    channel_id: generalChannel.id,
    content: deployedMessageContact.join('\n')
  });
}


/* Status update:

// Using Node.js 14.x +
// use "lib" package from npm
const lib = require('lib')({token: 'tok_dev_upCbyYomDzhz8S3hokhKcQZNWVC1rLmxmvPajP5r77m5Caj8CbQQDZ7GKNnbkZ2U'});

// make API request
let result = await lib.discord.users['@0.2.1'].me.status.update({
  activity_name: `Ready to roll`,
  status: 'ONLINE'
});

*/