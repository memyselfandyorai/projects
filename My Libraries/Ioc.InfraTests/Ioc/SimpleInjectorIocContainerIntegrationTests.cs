﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ioc.Infra.Ioc;
using Ioc.InfraTests.TestObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ioc.Infra.Ioc.Tests
{
    [TestClass()]
    public class SimpleInjectorIocContainerIntegrationTests
    {
        [TestMethod()]
        public void RegisterResolve_UsingImplementationInConstructorAsSingleton_PassesSameObject()
        {
            using (var iocContainer = CreateBasicContainer())
            {
                iocContainer.Register<ISimpleInterface, SimpleImpl>(Lifecycle.Singleton);
                iocContainer.Register<IInner, InnerImpl>(Lifecycle.Singleton);
                iocContainer.Register<IInner2, InnerImpl2>(Lifecycle.Singleton);

                var simpleImpl = iocContainer.Resolve<ISimpleInterface>();
                var innerImpl = iocContainer.Resolve<IInner>();
                var innerImpl2 = iocContainer.Resolve<IInner2>();

                Assert.AreEqual(simpleImpl, innerImpl.Inner);
                Assert.AreEqual(simpleImpl, innerImpl2.Inner);
            }
        }

        [TestMethod()]
        public void RegisterResolve_RegisteringTypeFactory_UsesIocContainerAndPassesSameObject()
        {
            ITypeFactory factory; 
            using (var iocContainer = CreateBasicContainer())
            {
                // Default: Lifecycle.Singleton.
                iocContainer.Register<ISimpleInterface, SimpleImpl>();
                iocContainer.Register<ITypeFactory, SimpleTypeFactory>(); 
                
                factory = iocContainer.Resolve<ITypeFactory>();

                Assert.IsNotNull(factory.OptionalImplementation);
                Assert.AreEqual(iocContainer.Resolve<ISimpleInterface>(), factory.OptionalImplementation);
                Assert.IsFalse(factory.DisposedCalled);
                // Use IocContainer in singleton:
                int expectedId = iocContainer.Resolve<ISimpleInterface>().ID;
                Assert.AreEqual(expectedId, factory.GetByType(MyType.TypeA).Num);
                Assert.AreEqual(expectedId, factory.GetByType(MyType.TypeB).Num);
            }

            Assert.IsTrue(factory.DisposedCalled);
        }

        private IIocContainer CreateBasicContainer()
        {
            var ioc = new SimpleInjectorIocContainer();
            ioc.Register<IIocContainer>(ioc);
            return ioc;
        }
    }

    public interface IInner
    {
        public ISimpleInterface Inner { get; }
    }

    public interface IInner2 : IInner
    {

    }

    public class InnerImpl : IInner
    {
        public ISimpleInterface Inner { get; private set; }

        public InnerImpl(ISimpleInterface inner)
        {
            this.Inner = inner;
        }
    }

    public class InnerImpl2 : IInner2
    {
        public ISimpleInterface Inner { get; private set; }

        public InnerImpl2(IIocContainer container)
        {
            this.Inner = container.Resolve<ISimpleInterface>();
        }
    }
}
