﻿using FolderSyncronizer.Debugging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FolderSyncronizer.Core.Data
{
    internal class TaskBasedPubsub<T> : IPubsub<T>
    {
        private readonly BlockingCollection<QueueItem> _publishedItems;
        private readonly Dictionary<string, Action<T>> _subscribers;
        private readonly List<Task> _consumerTasks;
        private readonly CancellationTokenSource _cancellationTokenSource;

        private bool _disposedValue;

        public TaskBasedPubsub(int parallelism = 1)
        {
            _consumerTasks = new List<Task>();
            _subscribers = new Dictionary<string, Action<T>>();
            _publishedItems = new BlockingCollection<QueueItem>(new ConcurrentQueue<QueueItem>());
            _cancellationTokenSource = new CancellationTokenSource();
            for (int i = 0; i < parallelism; i++)
            {
                Task task = new Task(() => ConsumeFromQueue(_cancellationTokenSource.Token, "ID:" + i), _cancellationTokenSource.Token);
                _consumerTasks.Add(task);
            }

            foreach (var task in _consumerTasks)
            {
                task.Start();
            }
        }

        public void Publish(string publisherId, T item)
        {
            _publishedItems.Add(new QueueItem { PublisherId = publisherId, Item = item });
        }

        /// <summary>
        /// For now, all subscribers are invoked when published
        /// </summary>
        /// <param name="subscriberId"></param>
        /// <param name="itemReceived"></param>
        public void Subscribe(string subscriberId, Action<T> itemReceived)
        {
            _subscribers.Add(subscriberId, itemReceived);
        }


        private void ConsumeFromQueue(CancellationToken token, string consumerId)
        {
            Singleton.Logger.LogDebug($"Consumer '{consumerId}': Task started");
            while (!token.IsCancellationRequested)
            {
                try
                {
                    if (_publishedItems.TryTake(out QueueItem item, 50, token))
                    {
                        foreach (var subscriberPair in _subscribers)
                        {
                            Singleton.Logger.LogDebug($"Consumer '{consumerId}': Subscriber '{subscriberPair.Key}' is consuming item '{item}'");
                            subscriberPair.Value.Invoke(item.Item);
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    Singleton.Logger.LogDebug($"Consumer '{consumerId}': Token Cancelled");
                }
            }

            Singleton.Logger.LogDebug($"Consumer '{consumerId}': Task ending");
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _cancellationTokenSource.Cancel();
                    foreach (var task in _consumerTasks)
                    {
                        try
                        {
                            task.Wait(1000);
                        }
                        catch (Exception e)
                        {
                            Singleton.Logger.LogError($"Failed stopping task {task.Id} after 1 second", e);
                        }
                        task.Dispose();
                    }
                    _cancellationTokenSource.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        private record QueueItem
        {
            public string PublisherId { get; set; }

            public T Item { get; set; }
        }
    }
}