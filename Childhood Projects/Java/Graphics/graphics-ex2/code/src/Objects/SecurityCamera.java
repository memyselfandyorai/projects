package Objects;

import General.Intersection;
import General.Plane;
import General.Point;
import General.Point2D;
import General.Ray;
import General.Scene;

/**
 * 
 * represents a security camera (bonus)
 *
 */
public class SecurityCamera extends Surface{
	
	private Point p0,p1,p2;
	private Point viewPoint;
	
	private boolean findingIntersection; //==false by default
	
	
	public void setP0(Point p0) {
		this.p0 = p0;
	}


	public void setP1(Point p1) {
		this.p1 = p1;
	}


	public void setP2(Point p2) {
		this.p2 = p2;
	}



	public void setViewPoint(Point viewPoint) {
		this.viewPoint = viewPoint;
	}


	public boolean hasAllValues() {
		if (p0==null || p1==null || p2==null || viewPoint==null)
			return false;
		return true;
	}


	private Rectangle getRectangle()
	{
		return new Rectangle(p0,p1,p2);
	}
	

	public Intersection intersect(Scene scene, Ray ray) {

		if(findingIntersection) //to avoid infitine loops
			return Intersection.MISS;
		
		Rectangle rec=getRectangle();
				
		Intersection inter=rec.intersect(scene, ray);
		if(inter==Intersection.MISS)
			return Intersection.MISS;
		
		double distance=inter.getDistance(); //distance between rectangle and eye
		
		Ray newRay=new Ray(viewPoint,ray.getVector());

		findingIntersection=true;
		Intersection newInter=scene.findIntersection(newRay);
		findingIntersection=false;
		
		if(newInter!=Intersection.MISS)
			newInter.setDistance(distance);
	
		return newInter;
	}




	public boolean onRight(Plane plane)
	{
		return getRectangle().onRight(plane);
	}


	/**
	 * shouldn't get here!!
	 * */
	public Point2D parametrization(Point point) {
		
		return null;
	}
	
	
}
