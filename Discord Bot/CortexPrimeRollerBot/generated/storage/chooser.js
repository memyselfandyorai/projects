"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StorageChooser = exports.getDefaultStorage = void 0;
const base_storage_1 = require("./base_storage");
const in_memory_storage_1 = require("./in_memory_storage");
var StorageType;
(function (StorageType) {
    StorageType[StorageType["replit"] = 0] = "replit";
    StorageType[StorageType["discord"] = 1] = "discord";
    StorageType[StorageType["memory"] = 2] = "memory";
})(StorageType || (StorageType = {}));
function getDefaultStorage() {
    return new base_storage_1.BaseDatabase(`Default(${StorageType[StorageType.memory]})`, new in_memory_storage_1.InMemoryDatabase());
}
exports.getDefaultStorage = getDefaultStorage;
class StorageChooser {
    constructor(type) {
        Object.defineProperty(this, "type", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.type = getStorageType(type);
    }
    async getStorage(params) {
        const underlyingStorage = await this.getUnderlyingStorage(params);
        return new base_storage_1.BaseDatabase(`${StorageType[this.type]}`, underlyingStorage);
    }
    async getUnderlyingStorage(params) {
        switch (this.type) {
            case StorageType.replit:
                const replitModule = await Promise.resolve().then(() => __importStar(require("./replit_storage")));
                return new replitModule.ReplitDatabase();
            case StorageType.discord:
                const discordModule = await Promise.resolve().then(() => __importStar(require("./discord_storage")));
                return await discordModule.createDiscordDatabase(params);
            case StorageType.memory:
                return new in_memory_storage_1.InMemoryDatabase();
            default:
                throw new Error(`Storage type not implemented: ${this.type}.`);
        }
    }
}
exports.StorageChooser = StorageChooser;
function getStorageType(type) {
    const storageType = StorageType[type];
    if (storageType !== undefined) {
        return storageType;
    }
    const allValues = Object.keys(StorageType).filter((item) => isNaN(Number(item)));
    throw new Error(`Unknown storage type: ${type}. Should be one of: ${allValues}`);
}
//# sourceMappingURL=chooser.js.map