export interface DiscordClientInteraction {
  // Sends join message to all channels
  sendJoinMessage(): Promise<void>;
}
