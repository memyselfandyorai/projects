﻿using log4net;
using System;
using System.IO;

namespace FolderSyncronizer.Debugging.Logs
{
    class DebugLogger : ICoreLogger
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(DebugLogger));

        public DebugLogger()
        {
            log4net.Repository.ILoggerRepository repository = LogManager.GetRepository();
            foreach (log4net.Appender.IAppender appender in repository.GetAppenders())
            {
                if (appender is log4net.Appender.FileAppender)
                {
                    var fileAppender = (log4net.Appender.FileAppender)appender;
                    string directory = Path.GetDirectoryName(fileAppender.File);
                    string fileSuffix = Path.GetFileName(fileAppender.File);
                    string newFileName = AppDomain.CurrentDomain.FriendlyName;
                    fileAppender.File = Path.Combine(directory, newFileName + fileSuffix);
                    fileAppender.ActivateOptions();
                }
            }
        }

        public void LogDebug(string message)
        {
            logger.Debug(message);
        }

        public void LogWarning(string message, Exception exception = null)
        {
            logger.Warn(message);
        }

        public void LogError(string message, Exception exception = null)
        {
            logger.Error(message, exception);
        }
    }
}
