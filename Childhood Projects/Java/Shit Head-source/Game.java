
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;

public class Game extends JPanel implements ActionListener{
	
	/*	
	exceptions.(?)
		
	bug in ai!!! puts things he shouldn't
	
	first cards: doesn't pressed.	!! doesn't get that an action was performed
		
	option to choose which cards? no.
	Exception in thread "AWT-EventQueue-0" java.lang.ArrayIndexOutOfBoundsException: No such child: 2
	 */
	private Pack pack,deck;
	
	private Vector<Player> players;
	
	//private JPanel player;
	
	//system.exit(0) : close
	
	//private JComboBox plCards;
	
	private JFrame frame;
	
	private int order;
	
	private JPanel pOrder;
	
	private Reading client;
	
	private boolean finish;
	
	private boolean finished;
	
	static Game toCards;
	
	private Server server;
	
	public Game(JFrame f,Vector<Player> pl)
	{
	super();	
	pack=new Pack(false);
	players=pl;
	frame=f;
	finish=false;
	
	frame.setResizable(false);
	toCards=this;
	
	frame.getContentPane().add(this);
		
	frame.setVisible(true);
		
	frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		
	frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
	
	
	deck=new Pack(true);
	
	order=0;
	
	setLayout(new GridLayout(3,3));
		
	for(int i=0;i<players.size();i++)
	{
	Player p=(Player)players.elementAt(i);
	p.dealStart(pack);
	}
	
	order=checkHand();
	
	
	
	pOrder=new JPanel();
	pOrder.setLayout(new GridLayout(2,1));
	
	JPanel por=new JPanel();
	por.setLayout(new GridLayout(0,1));
	
	JLabel lbb=new JLabel("The order of the game:");
	lbb.setForeground(Color.blue.darker().darker());
	
	por.add(lbb);
	int z=players.size();
	int i=order;
	
	while(true)
	{
	Player p=(Player)players.elementAt(i);
	
	JLabel lb=new JLabel(p.name());
	lb.setForeground(Color.blue);
	
	por.add(lb);
	
		i++;
		if(i>=z)
		{
		i=0;	
		}
	
	if(i==order)
	break;
	
	}
	
	pOrder.add(por);
			
	}
	
	
	public Game(JFrame f,Vector<Player> pl,Server s)
	{
	super();	
	pack=new Pack(false);
	players=pl;
	frame=f;
	server=s;
	finished=false;
	
	
	frame.getContentPane().add(this);
		
	frame.setVisible(false); //false here.
		
	frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		
	frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
	
	
	deck=new Pack(true);
	
	order=0;
	
	setLayout(new GridLayout(3,3));
		
	for(int i=0;i<players.size();i++)
	{
	Player p=(Player)players.elementAt(i);
	p.dealStart(pack);
	}
	
	order=checkHand();
	
	
	
	pOrder=new JPanel();
	pOrder.setLayout(new GridLayout(2,1));
	
	JPanel por=new JPanel();
	por.setLayout(new GridLayout(0,1));
	
	JLabel lbb=new JLabel("The order of the game:");
	lbb.setForeground(Color.blue.darker().darker());
	
	por.add(lbb);
	int z=players.size();
	int i=order;
	
	while(true)
	{
	Player p=(Player)players.elementAt(i);
	
	JLabel lb=new JLabel(p.name());
	lb.setForeground(Color.blue);
	
	por.add(lb);
	
		i++;
		if(i>=z)
		{
		i=0;	
		}
	
	if(i==order)
	break;
	
	}
	
	pOrder.add(por);
	
	}
	
	public Game(JFrame f,Reading s)
	{
	super();
	frame=f;
	client=s;
	players=new Vector<Player>();
	finish=false;
	
	f.setVisible(true);
	
	}
	
	public JFrame frame()
	{
		return frame;
	}
	
	public void game()
	{
	
	frame.getContentPane().add(this);
		
	frame.setVisible(true);
		
	frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		
	frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
	setLayout(new GridLayout(3,3));
			
	
	pOrder=new JPanel();
	pOrder.setLayout(new GridLayout(2,1));
	
	JPanel por=new JPanel();
	por.setLayout(new GridLayout(0,1));
	
	JLabel lbb=new JLabel("The order of the game:");
	lbb.setForeground(Color.blue.darker().darker());
	
	por.add(lbb);
	int z=players.size();
	int i=order;
	
	while(true)
	{
	Player p=(Player)players.elementAt(i);
	
	JLabel lb=new JLabel(p.name());
	lb.setForeground(Color.blue);
	
	por.add(lb);
	
		i++;
		if(i>=z)
		{
		i=0;	
		}
	
	if(i==order)
	break;
	
	}
	
	pOrder.add(por);
			
	}
	
	
	public boolean server()
	{
		return (server!=null);
	}
	
	public void start()
	{
	//pack.cards().clear();	
	AI();	
	}
	
	
	public void paintComponent(Graphics g)
	{
	super.paintComponent(g);	
	
	//ImageIcon m=new ImageIcon("pics\\ac.gif");
	//m.paintIcon(this,g,0,0);
	}
	
	
//drawImage(Image img, int x, int y, Color bgcolor, ImageObserver observer) 
  //        Draws as much of the specified image as is currently available.
	
	public Dimension getPreferredSize()
	{
	return new Dimension(300,300);	
	}
	
	
	public void next(boolean missed) //server's
	{
	//if winning!
	Player p=(Player)players.elementAt(order);
	if(p.cards().size()==0&&p.noEx())
	{
		//setVisible(false);
		//JOptionPane.showMessageDialog(this,p.name()+" finished all of his cards.","Winning",JOptionPane.PLAIN_MESSAGE);
		server.sendFinished(p.name());
		//setVisible(true);
		players.remove(p);
		if(order>=players.size())
		order=0;
		
		if(players.size()==1)
		{
		finished=true;
		String lost=((Player)players.elementAt(order)).name();
		//JOptionPane.showMessageDialog(this,"Game over. "+lost+" is the shit head.","Game over",JOptionPane.PLAIN_MESSAGE);
		server.finished(lost);
		}
			
	}
	else
	{
		order++;
		if(order>=players.size())
		order=0;
			if(missed)
			{
			Player mis=(Player)players.elementAt(order);
			//setVisible(false); //works? or see his cards?
			server.sendMissed(mis.name());
			//JOptionPane.showMessageDialog(this,mis.name()+" has missed his turn.","Skip",JOptionPane.PLAIN_MESSAGE);
			//setVisible(true);
			}
	}
	
	}
	
	public int checkHand()
	{
	int n=15;
	int s=-1;
	int play=0;
	
	for(int i=0;i<players.size();i++)
	{
	Player p=(Player)players.elementAt(i);
	Vector c=p.cards();
		for(int j=0;j<c.size();j++)
		{
		Card d=(Card)c.elementAt(j);
		int num=d.num();
		int sn=d.sn();
			if(num!=2&&num!=3&&num<n)
			{
			play=i;	
			n=num;	
			s=sn;
			}
			else
				if(num==n)
				if(sn>s)
				{
				play=i;	
				n=num;	
				s=sn;
				}
		}
	}	
	
	
	return play;	
	}
	
	
	public void burn(String mes,String n) //first one gets 2 messages?
	{
		if(!server())
		JOptionPane.showMessageDialog(frame,"Burn! ("+mes+")","Stack emptied - "+n,JOptionPane.INFORMATION_MESSAGE);
			//JOptionPane.showMessageDialog(frame,"Burn! ("+mes+")"+frame.getTitle()+","+server,"Stack emptied - "+n,JOptionPane.INFORMATION_MESSAGE);
		//change
		//System.out.println("burn1: "+server+","+frame.getTitle());
	}
	
	public void missed(String name,boolean same)
	{
	if(same)
	JOptionPane.showMessageDialog(this,"You missed your turn.","Skip",JOptionPane.PLAIN_MESSAGE);
	else
	JOptionPane.showMessageDialog(this,name+" has missed his turn.","Skip",JOptionPane.PLAIN_MESSAGE);
	}
	
	public void finished(String name,boolean same)
	{
	if(same)
	JOptionPane.showMessageDialog(this,"You finished all of your cards.","Winning",JOptionPane.PLAIN_MESSAGE);
	else
	JOptionPane.showMessageDialog(this,name+" finished all of his cards.","Winning",JOptionPane.PLAIN_MESSAGE);
	}
	
	
	public boolean canExit()
	{
	return finish;	
	}
	
	
	public void finish(String lost)
	{
	JOptionPane.showMessageDialog(this,"Game over. "+lost+" is the shit head.","Game over",JOptionPane.PLAIN_MESSAGE);	
	frame.setVisible(false);
	finish=true;
	}
	
	
	public void update(Player player)
	{
	toCards=this;
	setVisible(false);	
		
		int p=players.size()-1;
		int ind=players.indexOf(player);
		int pp=p;
		
		removeAll();
				
		add(pOrder);
		
		JPanel lab[]=new JPanel[p];
		
		for(int i=0;i<players.size();i++)
		if(i!=ind)
		{
		p--;
		lab[p]=new JPanel();
		lab[p].setLayout(new GridLayout(2,1));
		JPanel up=new JPanel();
		up.setLayout(new GridLayout(0,1));
		Player r=(Player)players.elementAt(i);	
		up.setToolTipText(r.name()+" cards.");
		up.add(new JLabel("Player: "+r.name()));	
		//up.add(new JLabel("Cards:"));	
		up.add(new JLabel("Cards in hand: "+r.cards().size()));
		up.add(new JLabel("Cards hidden: "+r.getHid()));
		up.add(new JLabel("Cards seen:"));
		lab[p].add(up);
		JPanel down=new JPanel();
		down.setLayout(new GridLayout(1,0));
		r.addLabels(down);
		lab[p].add(down);
		//lab[p].setBackground(Color.blue);
		//System.out.println(""+p);
		//lab[p].setPreferredSize(new Dimension(400,400));
		}
		
		
		//deck
		
		JPanel dec=new JPanel();
		dec.setLayout(new GridLayout(2,1));
		
		JPanel tips=new JPanel();
		tips.setLayout(new GridLayout(0,1));
		tips.setToolTipText("The stack");
		Vector v=deck.cards();
		
		JLabel inst=new JLabel(deck.inst());
		inst.setForeground(Color.red);
		inst.setFont(new Font("",0,22));
		tips.add(new JLabel("Number of cards left in the deck: "+pack.cards().size()));
		tips.add(new JLabel("Number of cards in the stack: "+v.size()));
		tips.add(inst);
		
		String row="";
		if(v.size()>0)
		row=" ("+deck.row()+" [card '"+deck.orow()+"'] in a row";
		
		String r3=deck.rowf();
		
		if(!r3.equals(""))
		row=row+r3;
		
		row=row+")";
		
		if(row.equals(")"))
		tips.add(new JLabel("Top card:"));
		else
		tips.add(new JLabel("Top card:"+row));
		
		dec.add(tips);
		
		JLabel top;
		if(v.size()>0)
		top=new JLabel(((Card)v.elementAt(0)).getIcon(this));
		else
		top=new JLabel("Empty");
		
		top.setToolTipText("The stack");
		
		dec.add(top);
				
		dec.setPreferredSize(new Dimension(70,70));
		
		//add(dec,BorderLayout.CENTER);
		///dec		
		
		
		
		if(pp==1)
		{
		add(lab[0]);
		add(new JPanel());
		add(new JPanel());
		add(dec);
		add(new JPanel());
		//add(new JPanel());
		}
		//add(lab[0],BorderLayout.NORTH);
		
		if(pp==2)
		{
		add(new JPanel());
		add(new JPanel());
		add(lab[0]);
		add(dec);
		add(lab[1]);
		//add(new JPanel());
		//add(lab[0],BorderLayout.EAST);
		//add(lab[1],BorderLayout.WEST);
		}
		
		if(pp==3)
		{
		add(lab[0]);
		add(new JPanel());
		add(lab[1]);
		add(dec);
		add(lab[2]);
		//add(new JPanel());
		//add(lab[0],BorderLayout.EAST);
		//add(lab[1],BorderLayout.WEST);
		//add(lab[2],BorderLayout.NORTH);
		}
		
		
		
		//player
		
		player.addButtons(this,true,this);
		
		JPanel pl=new JPanel();
		pl.setLayout(new GridLayout(0,1));
		pl.setToolTipText("Your cards");
		//pl.add(new JLabel("Player: "+player.name()));	
		
		Vector card=player.cards();
		
		/*
		JPanel car=new JPanel();
		car.setLayout(new GridLayout(0,5));
		
		for(int i=0;i<card.size();i++)
		{
		Card ca=(Card)card.elementAt(i);
		JButton b=new JButton(ca.getIcon());
		b.setToolTipText("Your cards");
		b.addActionListener(this);
		//b.setSize(200,200);
		car.add(b);			
		}*/
		
		JComboBox plCards=new JComboBox(card);
		plCards.setToolTipText("Choose a card");
		plCards.setMaximumRowCount(3);
		plCards.addActionListener(this);
		
		final Game main=this;
		
		if(card.size()==0)
		plCards.setEnabled(false);
		else
		plCards.setRenderer(new ListCellRenderer(){
								
									public Component getListCellRendererComponent(
							         JList list,
							         Object value,
							         int index,
							         boolean isSelected,
							         boolean cellHasFocus)
							     {
							     	
							     	//make it list?
							     	//need different color!
							     	
							     	Card d=(Card)value;
							     	//JLabel c=new JLabel(d.getIcon());
							     	JButton c=new JButton(d.getIcon(main));
							     	
							     	//JLabel c=(JLabel)(getListCellRendererComponent(list,value,index,isSelected,cellHasFocus));
							     	//System.out.println("value: "+value);
							         //c.setText(value.toString());
							         if(isSelected)
							         c.setBackground(Color.blue);
							         //c.setBackground(isSelected ? Color.blue : Color.white);
							         
							         //setForeground(Color.blue);
							         
							         //setBackground(list.getSelectionBackground());
									 //setForeground(list.getSelectionForeground());

							         					         
							         //if(((Char)(value)).attacked())
							         //c.setForeground(isSelected ? Color.magenta : Color.red);
							         //if(value instanceof 							         
							         //c.setToolTipText();
							         return c;
							     }
								
										
									});
		
		
		//JScrollPane scr=new JScrollPane(car);
		pl.add(plCards);
		
		//JButton hit=new JButton("Choose");
		//if(player.cards().size()==0)
		//hit.setEnabled(false);
		
		//hit.addActionListener(this);
		
		JButton take=new JButton("Take stack");
		take.addActionListener(this);
		
		if(deck.cards().size()==0)
		take.setEnabled(false);
		
		JPanel but=new JPanel();
		but.setLayout(new GridLayout(1,2));
		//but.add(hit);
		but.add(take);		
		
		JPanel plb=new JPanel();
		plb.setLayout(new GridLayout(2,1));
		//pl.setPreferredSize(new Dimension(400,400));
		plb.add(but);
		plb.add(pl);
		
		//add(pl,BorderLayout.SOUTH);
		add(plb);
		
		///player
		
		player.addButtons(this,false,this);
		
		//add(new JPanel());
		
				
		//scrollpane for cards? (check more cards)
			
	setVisible(true);
	
	}
	
	
	public void act(String name)
	{
	Player player=(Player)players.elementAt(order);
	
	Player you=player(name);
	if(you!=null)
	update(you);
	//else? better this way?
	
	if(!name.equals(player.name()))	
	{	
		//setEnabled(false);
		setVisible(false);
		removeAll();
		setVisible(true);
		JOptionPane.showMessageDialog(this,player.name()+"'s turn.","Wait your turn",JOptionPane.PLAIN_MESSAGE);
	}
	else
	JOptionPane.showMessageDialog(this,"Your turn.","Your turn",JOptionPane.PLAIN_MESSAGE);
		
	}
	
	public void move(String name,Vector nums)
	{
		Player player=player(name);	
			
		Card card=(Card)player.cards().elementAt(Integer.parseInt((String)nums.elementAt(0)));
		int num=nums.size();
			
		//System.out.println("cards2: "+player.cards());
		for(int i=0;i<num;i++)
		{
		int ind=Integer.parseInt((String)nums.elementAt(i));
		player.hit(deck,ind);
		}
				
		//burns
		order=deck.burn(this,player,order,players.size(),server);
						
		//8, skips
				
		while(player.cards().size()<3&&pack.cards().size()>0)
		player.drawCard(pack);
				
							
		if(card.num()==8&&deck.cards().size()>0)
		{
		for(int i=0;i<num;i++)
		next(true);
		}
							
		next(false);
				
		AI();	
		
	}
	
	
	public void movet(String name,Vector nums,boolean sen)
	{
		Player player=player(name);	
			
		Card card=player.getCard(sen,Integer.parseInt((String)nums.elementAt(0)));
		int num=nums.size();
			
		
		for(int i=0;i<num;i++)
		{
		int ind=Integer.parseInt((String)nums.elementAt(i));
		player.hitT(deck,ind,sen);
		}
				
		//burns
		order=deck.burn(this,player,order,players.size(),server);
						
		//8, skips
				
		while(player.cards().size()<3&&pack.cards().size()>0)
		player.drawCard(pack);
				
							
		if(card.num()==8&&deck.cards().size()>0)
		{
		for(int i=0;i<num;i++)
		next(true);
		}
							
		next(false);
				
		AI();	
	
	}
	
	
	public void take(String name)
	{
	Player player=player(name);	
	while(deck.cards().size()>0)
	player.drawCard(deck);	
			
	next(false);
	
	AI();		
		
	}
	
	public void taken(String name,int index)
	{
	Player player=player(name);	
	player.hitT(deck,index,false);
	
	while(deck.cards().size()>0)
	player.drawCard(deck);	
			
	next(false);
	
	AI();		
	}
	
	
	public void stake(String n)
	{
	JOptionPane.showMessageDialog(this,n+" took the whole stack","",JOptionPane.PLAIN_MESSAGE);		
	}
	
	public void staken(String n)
	{
	JOptionPane.showMessageDialog(this,n+" took the whole stack (wrong hidden card)","",JOptionPane.PLAIN_MESSAGE);		
	}
	
	public void actionPerformed(ActionEvent e)
	{
	
	
		//JOptionPane.showMessageDialog(this,"action "+order);
	//JButton b=(JButton)e.getSource();
	//String text=b.getText();
	Player player=(Player)players.elementAt(order);
			
	if(e.getSource() instanceof JComboBox)
	{
	JComboBox plCards=(JComboBox)e.getSource();
	//System.out.println(plCards.getSelectedItem().getClass());
	//JButton c=(JButton)plCards.getSelectedItem();
	int index=plCards.getSelectedIndex();
	//ImageIcon m=(ImageIcon)c.getIcon();
	//Card card=Card.getCard(m.getDescription());
	
	Card card=(Card)plCards.getSelectedItem();
	int n=deck.getNum();
	
	boolean ok=true;
	String mes="You can't put that card. (needs to be special or equals or ";
		if(n!=7)
		{
			if(card.num()<n&&card.num()!=2&&card.num()!=3&&card.num()!=10)
			{
				ok=false;
				mes=mes+"higher from "+Card.getNum(n)+")";
			}
		}
		else
		{
			if(card.num()>n&&card.num()!=2&&card.num()!=3&&card.num()!=10)
			{
			ok=false;
			mes=mes+"lower from "+Card.getNum(n)+")";
			}
			
		}
		
		
		
		if(ok)
		{
			int hm=player.howMany(card.num());	
			int num=1;			
			
			//System.out.println(hm);
			
			Vector<String> nums=new Vector<String>();
			
			if(hm>1)
			{
			String[] o=new String[hm];
			for(int i=0;i<hm;i++)
			o[i]=""+(1+i);
				
			num=JOptionPane.showOptionDialog(this,"How many cards of this kind do you want to put in the stack?","Choose:",0,JOptionPane.QUESTION_MESSAGE,null,o,o[0]);
			num++;
			}
			
			//System.out.println(num);
			
			if(num==1)
			nums.add(""+index);
			//player.hit(deck,index);
			else
			player.hit(deck,card.num(),num,nums);
			//several cards same time
						
			//burns
			//order=deck.burn(this,player,order,players.size());
			
			
			//8, skips
			
			//while(player.cards().size()<3&&pack.cards().size()>0)
			//player.drawCard(pack);
			
						
			//update(player); //del
			/*
			if(card.num()==8&&deck.cards().size()>0)
			{
			for(int i=0;i<num;i++)
			next(true);
			
			}
						
			next(false);
			*/
			//AI();			
			//client.sendGame();	
			client.sendMove(nums);			
			
		}
		else
		JOptionPane.showMessageDialog(this,mes,"Invalid card",JOptionPane.PLAIN_MESSAGE);
		
		
		
	} //end combobox
	
	
	
	if(e.getSource() instanceof JButton)
	{
	JButton b=(JButton)e.getSource();
	String text=b.getText();
	
		if(text.equals("Take stack"))
		{
			//while(deck.cards().size()>0)
			//player.drawCard(deck);	
			
			client.sendTake();

			//update(player);
								
			//next(false);
			//AI();						
		}
			
		
		if(text.equals(""))
		{
			boolean sen=!((ImageIcon)b.getIcon()).getDescription().equals("back");	
			int index=Integer.parseInt(b.getActionCommand());
			Card card=player.getCard(sen,index);
		int n=deck.getNum();
		boolean ok=true;
		String mes="You can't put that card. (needs to be special or equals or ";
		if(n!=7)
		{
			if(card.num()<n&&card.num()!=2&&card.num()!=3&&card.num()!=10)
			{
				ok=false;
				mes=mes+"higher from "+Card.getNum(n)+")";
			}
		}
		else
		{
			if(card.num()>n&&card.num()!=2&&card.num()!=3&&card.num()!=10)
			{
			ok=false;
			mes=mes+"lower from "+Card.getNum(n)+")";
			}
			
		}
		
		
		if(!sen) //try if work!
		{
			b.setIcon(card.getIcon(this));
		}
			
			if(ok)	
			{
				int hm=player.howManyS(card.num());	
				int num=1;			
				
				//System.out.println(hm);
				if(!sen)
				JOptionPane.showMessageDialog(this,"You chose a valid card.","Valid card",JOptionPane.PLAIN_MESSAGE);
				
				Vector<String> nums=new Vector<String>();
								
				if(hm>1)
				{
				String[] o=new String[hm];
				for(int i=0;i<hm;i++)
				o[i]=""+(1+i);
					
				num=JOptionPane.showOptionDialog(this,"How many cards of this kind do you want to put in the stack?","Choose:",0,JOptionPane.QUESTION_MESSAGE,null,o,o[0]);
				num++;
				}
				
				//System.out.println(num);
				
				if(num==1)
				nums.add(""+index);
				//player.hitT(deck,index,sen);
				else
				player.hitT(deck,card.num(),num,nums);
				//several cards same time
							
				//burns
				//order=deck.burn(this,player,order,players.size());
				
				/*			
				if(card.num()==8&&deck.cards().size()>0)
				{
				for(int i=0;i<num;i++)
				next(true);
				
				//setVisible(false);
				//JOptionPane.showMessageDialog(this,num+" players missed their turn","Skips (card number 8)",JOptionPane.PLAIN_MESSAGE);
				//setVisible(true);
				}
			
			next(false);
			AI();*/
			
				client.sendMoveT(nums,sen);
			}
			else //invalid card
			{
				String ex="";
				boolean ai=false;
				if(!sen)
				{
				//player.hitT(deck,index,false);
				ex="\n\nYou took the whole stack.";
				//	while(deck.cards().size()>0)
				//	player.drawCard(deck);
				//next(false);
				ai=true;
				}
				JOptionPane.showMessageDialog(this,mes+ex,"Invalid card",JOptionPane.PLAIN_MESSAGE);
				
				if(ai)
				client.sendTaken(index);
				//AI();
			}
		}
		
		
	}
		
	}
	
	
	public void AI() //server's
	{ //move(name,nums-strings)
	if(!finished)
	{
	Player player=(Player)players.elementAt(order);
	server.sendGame();
	
	if(player.human())
	{
		//update(player);	//del
	}
	else //AI
	{
	
	//setEnabled(false);
	//JOptionPane.showMessageDialog(this,player.name()+"'s turn.","Turn passed",JOptionPane.PLAIN_MESSAGE);
	//setEnabled(true);
	
		if(player.cards().size()!=0)
		{
			int ind=player.choose(deck.getNum());
			
			if(ind!=-1)
			{
			int num=1;
			int m=((Card)player.cards().elementAt(ind)).num();
			int hm=player.howMany(m);			
			
				if(hm>1)
				{
					if((m!=3&&m!=2&&m!=10)||player.hasOnlySp()) //not really right
					num=hm;					
				}
			
			
			Vector<String> nums=player.hit(m,num);
			//System.out.println("nums: "+nums);
			server.sendMove(nums,player.name());
			//System.out.println("cards: "+player.cards());
			move(player.name(),nums);
			/*
			if(num==1)
			player.hit(deck,ind);
			else
			player.hit(deck,m,num);
			
			while(player.cards().size()<3&&pack.cards().size()>0)
			player.drawCard(pack);

			showMiddle(player,num);
			
			order=deck.burn(this,player,order,players.size());
									
			if(m==8&&deck.cards().size()>0)
			{
			for(int i=0;i<num;i++)
			next(true);
			
			//setVisible(false); //works? or see his cards?
			//JOptionPane.showMessageDialog(this,num+" players missed their turn","Skips (card number 8)",JOptionPane.PLAIN_MESSAGE);
			//setVisible(true);
			}
			*/
			}
			else
			{
				/*
				while(deck.cards().size()>0)
				player.drawCard(deck);
				
				setVisible(false); //works? or see his cards?
				JOptionPane.showMessageDialog(this,player.name()+" took the whole stack","Invalid card",JOptionPane.PLAIN_MESSAGE);
				setVisible(true);*/
				server.sendTake(player.name());
				take(player.name());
			}
			
		}
		else
		{
				
			if(!player.noSe()) //game.movet(name,nums,true);
			{
				int ind=player.choose(deck.getNum(),true);
			
				if(ind!=-1)
				{
				int num=1;
				int m=player.getSeen(ind);
				int hm=player.howManyS(m);			
				
					if(hm>1)
					{
						if((m!=3&&m!=2&&m!=10)||player.hasOnlySp())
						num=hm;					
					}
				
				Vector<String> nums=player.hitT(m,num);
				server.sendMoveT(nums,true,player.name());
				movet(player.name(),nums,true);
				/*
				if(num==1)
				player.hitT(deck,ind,true);
				else
				player.hitT(deck,m,num);
			
				showMiddle(player,num);
				
				order=deck.burn(this,player,order,players.size());
										
				if(m==8&&deck.cards().size()>0)
				{
				for(int i=0;i<num;i++)
				next(true);
				
				//setVisible(false); //works? or see his cards?
				//JOptionPane.showMessageDialog(this,num+" players missed their turn","Skips (card number 8)",JOptionPane.PLAIN_MESSAGE);
				//setVisible(true);
				}*/
				
				}
				else
				{
					server.sendTake(player.name());
					take(player.name());
					/*
				while(deck.cards().size()>0)
				player.drawCard(deck);
				
				setVisible(false); //works? or see his cards?
				JOptionPane.showMessageDialog(this,player.name()+" took the whole stack","Invalid card",JOptionPane.PLAIN_MESSAGE);
				setVisible(true);*/	
				}

			
			}
			else  //game.movet(name,nums,false);
			{
				int ind=player.choose(deck.getNum(),false);
				
				if(ind<0)
				{
				server.sendTaken(-ind-1,player.name());
				taken(player.name(),-ind-1);	
				/*
				player.hitT(deck,-ind,false);
					while(deck.cards().size()>0)
					player.drawCard(deck);
				
				setVisible(false); //works? or see his cards?
				JOptionPane.showMessageDialog(this,player.name()+" took the whole stack","Invalid card",JOptionPane.PLAIN_MESSAGE);
				setVisible(true);*/
				
				}
				else
				{
				
				//int m=player.getHidden(ind);
				Vector<String> nums=new Vector<String>();
				nums.add(""+ind);
				server.sendMoveT(nums,false,player.name());
				movet(player.name(),nums,false);
				/*player.hitT(deck,ind,false);
				
				showMiddle(player,1);
				
				order=deck.burn(this,player,order,players.size());
				
										
				if(m==8&&deck.cards().size()>0)
				next(true);*/
				
				
				}
				
								
			}
		}
		
		//next(false);
		//AI();
	} //end computer
	
	} //!finished
	}
	
	
	public void showMiddle(Player player,int num)
	{
		
	setVisible(false);	
		
		removeAll();
		
		setLayout(new GridLayout(3,3));
		
		for(int i=0;i<4;i++)
		add(new JPanel());
		
		
				
		//deck
		
				
		JPanel dec=new JPanel();
		dec.setLayout(new GridLayout(2,1));
		
		//dec.setBackground(Color.red);
		
		JPanel bot=new JPanel();
		
		Vector v=deck.cards();
		
		//String ofa="";
						
		JLabel ms=new JLabel("The card chosen:");
		ms.setForeground(Color.blue);
		ms.setFont(new Font("",0,20));
				
		bot.add(ms);
		
		if(num>1)
		bot.add(new JLabel("("+num+" of a kind)"));
		
		dec.add(bot);
		
		//4 of a kind?
		
		JPanel top=new JPanel();
		if(v.size()>0)
		{
		for(int i=0;i<num;i++)
		top.add(new JLabel(((Card)v.elementAt(i)).getIcon(this)));
		}
		else
		top.add(new JLabel("Empty"));
		
		dec.add(top);
						
		add(dec);
		
			
		for(int i=0;i<4;i++)
		add(new JPanel());
		
		setVisible(true);	
		//frame.toFront();
		JOptionPane.showMessageDialog(this,player.name()+" has made his turn.","Turn passed",JOptionPane.PLAIN_MESSAGE);
	
	}
	
	
	public void showMiddle(Player player,Vector<String> nums)
	{
		
	setVisible(false);	
		
		removeAll();
		
		setLayout(new GridLayout(3,3));
		
		for(int i=0;i<4;i++)
		add(new JPanel());
		
		
				
		//deck
		
				
		JPanel dec=new JPanel();
		dec.setLayout(new GridLayout(2,1));
		
		//dec.setBackground(Color.red);
		
		JPanel bot=new JPanel();
		
		//Vector<Card> v=deck.cards();
		Vector<String> rnums=new Vector<String>();
		
		for(int i=0;i<nums.size();i++)
		{
			rnums.add((Integer.parseInt(nums.elementAt(i))+i)+"");
		}
		
		//String ofa="";
						
		JLabel ms=new JLabel("The card chosen:");
		ms.setForeground(Color.blue);
		ms.setFont(new Font("",0,20));
				
		bot.add(ms);
		
		if(nums.size()>1)
		bot.add(new JLabel("("+nums.size()+" of a kind)"));
		
		dec.add(bot);
		
		//4 of a kind?
		
		JPanel top=new JPanel();
		if(nums.size()>0) //?? v.size?
		{
		//for(int i=0;i<num;i++)
		//top.add(new JLabel(((Card)v.elementAt(i)).getIcon()));
		for(int i=0;i<nums.size();i++)
		top.add(new JLabel(((Card)player.cards().elementAt(Integer.parseInt((String)rnums.elementAt(i)))).getIcon(this)));
		}
		else
		top.add(new JLabel("Empty"));
		
		dec.add(top);
						
		add(dec);
		
			
		for(int i=0;i<4;i++)
		add(new JPanel());
		
		setVisible(true);
		frame.setState(JFrame.NORMAL);
		JOptionPane.showMessageDialog(this,player.name()+" has made his turn.","Turn passed",JOptionPane.PLAIN_MESSAGE);
	
	}
	
	
	
	public void showMiddle(Player player,Vector nums,boolean sen)
	{
		
	setVisible(false);	
		
		removeAll();
		
		setLayout(new GridLayout(3,3));
		
		for(int i=0;i<4;i++)
		add(new JPanel());
		
		
				
		//deck
		
				
		JPanel dec=new JPanel();
		dec.setLayout(new GridLayout(2,1));
		
		//dec.setBackground(Color.red);
		
		JPanel bot=new JPanel();
		
		Vector<Card> v=deck.cards();
		
		//String ofa="";
						
		JLabel ms=new JLabel("The card chosen:");
		ms.setForeground(Color.blue);
		ms.setFont(new Font("",0,20));
				
		bot.add(ms);
		
		if(nums.size()>1)
		bot.add(new JLabel("("+nums.size()+" of a kind)"));
		
		dec.add(bot);
		
		//4 of a kind?
		
		JPanel top=new JPanel();
		if(nums.size()>0) //?? v.size?
		{
		//for(int i=0;i<num;i++)
		//top.add(new JLabel(((Card)v.elementAt(i)).getIcon()));
		for(int i=0;i<nums.size();i++)
		top.add(new JLabel((player.getCard(sen,Integer.parseInt((String)nums.elementAt(i))).getIcon(this))));
		}
		else
		top.add(new JLabel("Empty"));
		
		dec.add(top);
						
		add(dec);
		
			
		for(int i=0;i<4;i++)
		add(new JPanel());
		
		setVisible(true);
		frame.setState(JFrame.NORMAL);
		JOptionPane.showMessageDialog(this,player.name()+" has made his turn.","Turn passed",JOptionPane.PLAIN_MESSAGE);
	
	}
	
	
	
	public Player player(String n)
	{
	for(int i=0;i<players.size();i++)
	{
	Player p=(Player)players.elementAt(i);	
	if(p.name().equals(n))
	return p;	
	}	
	
	return null;
	}
	
	
	public String write()
	{
	String s="G*";	
	
	s+=order+",";
	s+=pack.write();
	s+=deck.write();
	
	for(int i=0;i<players.size();i++)
	{
	Player p=(Player)players.elementAt(i);	
	s+=p.write();	
	}
			
	return s+"*G";
	}
	
	
	public void read(String s)
	{
	s=s.substring(2);	
	int b;
	
	b=s.indexOf(",");
    order=Integer.parseInt(s.substring(0,b));
   	s=s.substring(b+1);	
	
	b=s.indexOf("*K");
    pack=Pack.read(s.substring(0,b));
   	s=s.substring(b+2);	
	
	b=s.indexOf("*K");
    deck=Pack.read(s.substring(0,b));
   	s=s.substring(b+2);	
	
	players.clear();
	
	while((b=s.indexOf("*P"))!=-1)
	{
	players.add(Player.read(s.substring(0,b)));
   	s=s.substring(b+2);			
	}
	
	}
	
	
	/*
	public static void main(String[] args) {
		// 
		
		
		
		
		
		JFrame f=new JFrame();
		
		
				
		
		//f.setSize(700,700);
				
		Game g=new Game(f);
		
			
		//System.out.println("ok");
		
		f.getContentPane().add(g);
		
		f.setVisible(true);
		
		f.setExtendedState(Frame.MAXIMIZED_BOTH);
		
		//Dimension screenSize=f.getToolkit().getDefaultToolkit().getScreenSize();

        //f.setBounds(0,0,screenSize.width,screenSize.height);
	
		
		//GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(f);
	
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		g.start();
		
	}	
	
	*/
	
	
}
