import type { Client, Guild, ChatInputCommandInteraction, Message, PartialTextBasedChannelFields } from "discord.js";
import type { CommandOption, CommandInteraction } from "./api/commands";
import type { PrefixCommandInteraction } from "./api/prefix_commands";
import type { OutputMessage, MessageSender } from "./api/messages";
import type { IocContainer } from "./api/ioc_container";
import type { PinnedMessage, PinManager } from "./api/pin_manager";
import type { DiscordClientInteraction } from "./api/client_manager";
export declare class NodeJsCommand implements CommandInteraction {
    interaction: ChatInputCommandInteraction;
    messageSender: NodeJsMessageSender;
    iocContainer: IocContainer;
    constructor(interaction: ChatInputCommandInteraction, iocContainer: IocContainer);
    getIocContainer(): any;
    getAllInputOptions(): string[];
    inputOptions(...names: string[]): CommandOption[];
    getInputOptionsMap(): Map<string, string | number | boolean>;
    user(): string;
    getUserId(): string;
    getChannelId(): string;
    getMessageId(): string;
    getPinManager(): PinManager;
    send(message: OutputMessage, options: ?MessageOptions): Promise<string>;
    prepareCommandReply(): Promise<void>;
    reply(message: OutputMessage): Promise<string>;
    toString(): string;
}
export declare class NodeJsPrefixCommandInteraction implements PrefixCommandInteraction {
    message: Message;
    messageSender: MessageSender;
    adminId: string;
    iocContainer: IocContainer;
    constructor(message: Message, adminId: string, iocContainer: IocContainer);
    getIocContainer(): any;
    botId(): string;
    getAdminId(): string;
    senderId(): string;
    getMessageId(): string;
    generateInvite(): string;
    send(message: OutputMessage, options: ?MessageOptions): Promise<string>;
}
declare class NodeJsMessageSender implements MessageSender {
    channel: PartialTextBasedChannelFields | null;
    constructor(channel: PartialTextBasedChannelFields | null);
    send(message: OutputMessage, options: ?MessageOptions): Promise<string>;
}
export declare class NodeJsPinManager implements PinManager {
    interaction: ChatInputCommandInteraction;
    constructor(interaction: ChatInputCommandInteraction);
    fetchAllPinnedMessages(channelId: string): Promise<PinnedMessage[]>;
    deletePinnedMessage(channelId: string, messageId: string): Promise<void>;
    pinMessage(channelId: string, messageId: string): Promise<void>;
    updatePinnedMessage(channelId: string, messageId: string, message: OutputMessage): Promise<void>;
}
export declare class NodeJsDiscordClientInteraction implements DiscordClientInteraction {
    client: Client;
    guild: Guild;
    constructor(client: Client, guild: Guild);
    sendJoinMessage(): Promise<void>;
}
export {};
//# sourceMappingURL=api.d.ts.map