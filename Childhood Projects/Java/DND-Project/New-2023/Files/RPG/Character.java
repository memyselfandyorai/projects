
import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;



//bug with the spells. not accurate (page).

//can copy paste.

public class Character extends Char implements Serializable{
	
	
	//deflect arrow-take it?
	
	//when attack, disable "weapon" etc.
	
	
	/*private int con; //costitution
	private int wis; //wisdom
	private int inl; //inteligence
	private int cha; //charisma
	*/
	
	/**
	 *The character coordinations
	 */
	private transient int x,y; //position
	/**
	 *His level
	 */
	private int lvl; //level
	/**
	 *His experience points
	 */
	private int exp; //experience
	/**
	 *Experience needed for next level
	 */
	private int need; //exp needed
	/**
	 *His hiding score
	 */
	private int hiding;
	/**
	 *Number of times he attacked in the current round
	 */
	private int attacks;
	
	
	//speed- turns instead of speed? (it is known by class)
	/**
	 *The level of his hunger, and his speed
	 */
	private int hunger,speed; //meals,
	/**
	 *How much times did he move from place to place while awake
	 */
	private int moved;
	/**
	 *His ability points that are available
	 */
	private int abil;//abilities available
	/**
	 *His skill points that are available
	 */
	private int skil;//skills available
	
	//private String[] langs; //languages
	/*
	common, elven, dwarven, orcish, draconic,Monkic [check]
	*/
	/**
	 *His class
	 */
	private String clas;
	/**
	 *His base attack bonus
	 */
	private int ba; //base attack
	/**
	 *If he is stabe
	 */
	private boolean stable;
	/**
	 *His skills
	 */
	private Skills skills;
	/**
	 *The main menu
	 */
	private Main main;

	/**
	*Constructs a new Character
	*@param name : his name
	*@param hp : his health points
	*@param str : his strength
	*@param dex : his dexterety
	*@param race : his race
	*@param m : if he is male
	*@param co : his constitution
	*@param in : his intelligence
	*@param wi : his wisdom
	*@param ch : his charisma
	*@param c : his class ("proffesion")
	*@param skills : his skills	
	*/
	public Character(String name,int hp,int str,int dex,String race,boolean m/**/,int co,int in,int wi,int ch,String c,Skills skils)
	{
		//check hp
		
		super(name,hp,str,dex,race,m,co,in,wi,ch);
		
		x=Dice.roll(Map.dim)-1;y=Dice.roll(Map.dim)-1;
		//langs=l;
		lvl=1;
		exp=0;
		need=1000;
		gp=2;
		
		attacks=0;
		
		moved=0;
		
		hiding=0;
		
		stable=false;
		
		//items.add(new Item("Meals",0.1,0.1,6));
		items.add(Item.getItem("Meals",6));
		
		abil=0;
		skil=0;
			
		clas=c;
		skills=skils;
		skills.setOwner(this);
	
		//,int r,int w,int f +bs (and attacks?- neh...)

		////System.out.println("cha "+cha);
		
		//gets bonuses every level.(1,2) [not multiplied at level 1]
		if(clas.equals("Barbarian"))//climb*2
		{
		skills.addClimb(1);
		skills.addClimb(1);
		ref=0; //lvl%3==0 (3,6,9: +1)
		will=0; //lvl%3==0 (3,6,9: +1)
		fort=2; //lvl%2==0 (2,4,6: +1)
		ba=1; //+1 every time. attacks: (ba-1)/5+1
		armor=Armor.createa("Leather Armor",this);
		}
		if(clas.equals("Paladin"))//heal,spot
		{
		skills.addHeal(1);
		skills.addSpot(1);
		ref=0; //lvl%3==0 (3,6,9: +1)
		will=0; //lvl%3==0 (3,6,9: +1)
		fort=2; //lvl%2=0= (2,4,6: +1)
		ba=1; //+1 every time. attacks: (ba-1)/5+1
		//like barbarian
		weapon=Weapon.create("Longsword",this);
		}
		if(clas.equals("Rogue"))//pick,hide
		{
		skills.addHide(1);
		skills.addPick(1);
		ref=2; //lvl%2==0 (2,4,6: +1)
		will=0; //lvl%3==0 (3,6,9: +1)
		fort=0; //lvl%3==0 (3,6,9: +1)
		ba=0; //+1 every time, except when lvl%4==1 (stays)
		//attacks: (ba-1)/5+1
		weapon=Weapon.create("Sling",this);
		items.add(Weapon.create("Dagger",this));
		}
		if(clas.equals("Sorcerer"))//search,perf
		{
		skills.addPerf(1);
		skills.addSearch(1);
		ref=0; //lvl%3==0 (3,6,9: +1)
		will=2; //lvl%2==0 (2,4,6: +1)
		fort=0; //lvl%3==0 (3,6,9: +1)
		ba=0; //+1 every 2 (lvl%2==0)
		//attacks: (ba-1)/5+1
		}
		if(clas.equals("Ranger"))//search,dir
		{	
		skills.addCraft(1);
		skills.addDir(1);
		ref=0; //lvl%3==0 (3,6,9: +1)
		will=0; //lvl%3==0 (3,6,9: +1)
		fort=2; //lvl%2==0 (2,4,6: +1)
		ba=1; //+1 every time. attacks: (ba-1)/5+1
		//like barbarian
		weapon=Weapon.create("Longbow",this);
		}
		if(clas.equals("Monk"))//swim
		{
		skills.addSwim(1);
		skills.addSwim(1);
		ref=2; //lvl%2==0 (2,4,6: +1)
		will=2; //lvl%2==0 (2,4,6: +1)
		fort=2; //lvl%2==0 (2,4,6: +1)
		ba=0; //+1 every time, except when lvl%4==1 (stays)
		//attacks: (ba-1)/5+1
		}
		
		
		
		//races
				
		if(race.equals("Human"))
		{
			//bonus with shield or armor +2, if one at least is "Leather"
			//normal ac
			speed=6; //turns per day (at 1,3,5 needs to eat)
						
			addStr(1);
			addCon(1);
		}
		
		if(race.equals("Elf"))
		{
			//bonus with "Bow" +1,+1
			//normal ac
			////System.out.println("dex (create): "+dex);
			speed=6; 
			addDex(2);
			//dex+=2;
			addCon(-2);
			skills.addSpot(2);
			skills.addSearch(2);
			will+=2;


		}
		
		
		if(race.equals("Dwarf"))
		{
			//bonus with "Axe" (and helmet?)x  +1,+1
			//+1 ac\attack
			speed=3; //turns per day (at 1,2,3 needs to eat)	
			addCon(2);
			addCha(-2);
			will+=2;
			//+1 attack orcs and goblins, +4 dodge against giants(?) [maybe just to huge].
		}
		
		if(race.equals("Halfling"))
		{
			//bonus with "Short" (dagger?)x +1,+1
			//+1 ac\attack
			speed=3; 	
			
			addDex(2);
			addStr(-2);
			skills.addHide(2);
			skills.addClimb(2);
			will++;
			ref++;
			fort++;	
		}
		
	
		
		spells=new Spells(this);
		

		//,int meal,int s,String[] l,int x,int y
		
		if(weapon!=null)
		{
			items.add(weapon);
			if(weapon.isRanged())
			items.add(new Item(weapon.kind()));
		}
		else
		weapon=Weapon.create("Unarmed Strike",this);
		
		if(shield!=null)
		items.add(shield);
		
		if(armor!=null)
		items.add(armor);
		
		//items for trial
		/*
		weapon=Weapon.create("Longsword",this);
		//shield=Shield.create("Buckler",this);
		shield=Shield.create("Wooden Shield",this);
		armor=Armor.createa("Chain Shirt",this);
		
		
		items.add(weapon);
		items.add(shield);
		items.add(armor);
		//items.add(new Item("Arrows"));
		
		
		addExp(20000);
		*/
		////System.out.println(spells.getUses(0));
		
		//addExp(200000); //del
		
		////System.out.println(spells.getUses(0));
		
	}//end creating.
	
	
	//not finished
/*	public int AC()
	{
	int s=10;
	if(race.equals("Dwarf")||race.equals("Halfling"))
	s++;
	
	s+=mod(dex);
	
	return s;
	
	}
*/

	public Character()
	{
	
	}
	
	
	public int lvl()
	{
		return lvl;
	}


	public String getClas()
	{
		return clas;
	}
	

	/**
	*Adds experience points, and improves the character as it goes up levels
	*@param name : the amount to add
	*/
	public void addExp(int a)
	{
		exp+=a;
		
		while(exp>=need)
		{
			lvl++;
			need=need+lvl*1000; //1000,3000,6000
			
			//smthing before? (to all classes)
			
			//races:
			if(race.equals("Human"))
			skil++;
			
			
			//classes:
			
			if(lvl%3==0) //?
			abil++;
			
			
			if(clas.equals("Barbarian"))
			{
			skil+=2;
			hp+=Dice.roll(1,12,con());
			skills.addClimb(1);
				if(lvl%3==0)
				{
				ref++;
				will++;
				}
				if(lvl%2==0)
				fort++;
			ba++;
			}

			if(clas.equals("Paladin"))
			{
			skil+=2;
			hp+=Dice.roll(1,10,con());
			
				if(lvl%2==0)
				{
				skills.addHeal(1);
				fort++;
				}
				else
				skills.addSpot(1);
				
				if(lvl%3==0)
				{
				ref++;
				will++;
				}
			ba++;
			}
				
			if(clas.equals("Rogue"))
			{
			skil+=8;
			hp+=Dice.roll(1,6,con());
			
				if(lvl%2==0)
				{
				skills.addPick(1);
				ref++;
				}
				else
				skills.addHide(1);
				
				if(lvl%3==0)
				{
				fort++;
				will++;
				}
			if(lvl%4!=1)
			ba++;
			}
			
			if(clas.equals("Sorcerer"))
			{
			skil+=2;
			hp+=Dice.roll(1,4,con());
				
				if(lvl%2==0)
				{
				skills.addSearch(1);
				will++;
				ba++;
				}
				else
				skills.addPerf(1);
								
				if(lvl%3==0)
				{
				fort++;
				ref++;
				}
			}
			
			if(clas.equals("Ranger"))
			{
			skil+=4;
			hp+=Dice.roll(1,10,con());
				
				if(lvl%2==0)
				{
				skills.addDir(1);
				fort++;
				}
				else
				skills.addCraft(1);
				
				if(lvl%3==0)
				{
				will++;
				ref++;
				}
			ba++;
			}
			
			if(clas.equals("Monk"))
			{
			skil+=4;
			hp+=Dice.roll(1,8,con());
			
			skills.addSwim(1);
			
				if(lvl%2==0)
				{
				fort++;
				will++;
				ref++;
				}
			if(lvl%4!=1)
			ba++;
			
			
			if(weapon.getName().equals("Unarmed Strike"))
			weapon=Weapon.create("Unarmed Strike",this);
			
			}
				
			////System.out.println("bu: "+spells.getUses(0));
			updateSpells();
			////System.out.println("bus: "+spells.getUses(0));
			getSpells().updateSpells();
			////System.out.println("buus: "+spells.getUses(0));
		}
		
		
	}
	
	
		
	/**
	*Returns the attack of the character
	*/
	public int attack()
	{
	//if(lvl<2)
	//addExp(15000);
	int b=ba;
	int ded=5; //deduct
	if(clas.equals("Monk"))
	if(weapon.getName().equals("Unarmed Strike"))
	ded=3;
		
	for(int i=0;i<attacks();i++) //NoA
	b-=ded;
	
	
		if(spells.hasDisease("Holy Sword"))
		if(!weapon.getName().equals("Unarmed Strike"))
		b+=5;
	
	int mf=0; //magic fang
	
	if(spells.hasDisease("Magic Fang"))
	mf=lvl/3;
	
	if(mf>5)
	mf=5;
	
	b=b+mf;
	////System.out.println("lvl: "+lvl()+", ba: "+ba+"  , noa: "+NoA()+", attacks: "+attacks+",  b: "+b);
	return super.attack()+b; //in another place i'll do the next attack.		
	}
	
	
	public int attack(Weapon w)
	{
	//if(lvl<2)
	//addExp(15000);
	int b=ba;
	int ded=5; //deduct
	if(clas.equals("Monk"))
	if(weapon.getName().equals("Unarmed Strike"))
	ded=3;
	
	for(int i=0;i<attacks();i++) //NoA
	b-=ded;
	
		if(spells.hasDisease("Holy Sword"))
		if(!weapon.getName().equals("Unarmed Strike"))
		{
		b+=5;
		}
	
	int mf=0; //magic fang
	
	if(spells.hasDisease("Magic Fang"))
	mf=lvl/3;
	
	if(mf>5)
	mf=5;
	
	b=b+mf;	
	
	
	//System.out.println(w.getName()+",b: "+b);
	
	return super.attack(w)+b;
	}
	
	
	public void setPlace(int xx,int yy)
	{
		x=xx;
		y=yy;
	}
	
	public String getRace()
	{
		return race;
	}
		
	
	public void stable(boolean b)
	{
		stable=b;
	}
	
	public boolean stable()
	{
		return stable;
	}
	
	
	public int getXP()
	{
	return exp;	
	}
	
	public int getNeed()
	{
	return need;	
	}
	
	
	
	public int getBA()
	{
	return ba;	
	}
	
	
	public String bab()
	{
	int b=ba;
	String s="+"+b;
	b-=5;
	while(b>0)
	{
	s+="/+"+b;
	b-=5;
	}
	
	return s;	
	}
	
	
	public int speed()
	{
	int s=speed;	
	
	return s;
	}
	
	
	public Skills getSkills()
	{
	return skills;	
	}
	
	public void setBA(int a)
	{
		ba=a;
	}
	
	/**
	*Updates the character's spells as he goes up levels
	*/	
	public void updateSpells()
	{
	//name,level,uses,turns,save
	////System.out.println("n: "+spells.getNum());
	
			if(clas.equals("Barbarian"))
			{
				//if(lvl==5)
				//spells.add("Uncanny Dodge",-1,-1,2);//+lvl/5 to ac and reflex. /do this all the time? permenant?
								
				if(lvl>=9&&lvl%3==0)
				damr++;
				
				//if(lvl%4==0)
				//spells.updateUses("Rage",1);	
			} //finished

			if(clas.equals("Paladin"))
			{
				if(lvl==2)
				spells.add("Smite Evil",-1,1,0); //+cha-mod to attack, +lvl damage (only on Enemies)
				
				if(lvl==3)
				{
				spells.add("Remove Disease",-1,1,0); //removes all "affected".	
				spells.add("Turn Undead",0,cha()+3,0); //1d6+(lvl-2)+cha(). undead only
				}
				
				//if(lvl%3==0&&lvl!=3)
				//spells.updateUses("Remove Disease",1);
				
				//if(lvl%4==0&&lvl>4)
				//spells.updateUses("Turn Undead",4);
				
				
				switch(lvl) //spells. 10+splvl+wis (DC for saving throws). 10+splvl=<wisdom (to able to cast). +wis.
				{
				case 4:
				if(wis()>0)
				spells.add("Cure Light Wounds",1,wis(),0); //1d8+lvl(max 5)
				break;
				
				case 6:
				if(!spells.exists("Cure Light Wounds"))
				//spells.updateUses("Cure Light Wounds",1);
				//else
					if(wis()>0)
					spells.add("Cure Light Wounds",1,1+wis(),0);
					else
					spells.add("Cure Light Wounds",1,1,0);
				break;								
				
				case 8:
				if(wis()>0)
				spells.add("Protection",2,wis(),10); //+2 ac
				break;
				
				case 10:
				if(!spells.exists("Protection"))
				//spells.updateUses("Protection",1);
				//else
					if(wis()>0)
					spells.add("Protection",2,1+wis(),10);
					else
					spells.add("Protection",2,1,10);
				break;	
				
				
				case 11:
				if(wis()>0)
				spells.add("Cure Moderate Wounds",3,wis(),0); //2d8+lvl (no maximum 10)
				break;
				
				case 12:
				if(!spells.exists("Cure Moderate Wounds"))
				//spells.updateUses("Cure Moderate Wounds",1);
				//else
					if(wis()>0)
					spells.add("Cure Moderate Wounds",3,1+wis(),0);
					else
					spells.add("Cure Moderate Wounds",3,1,0);
				break;	
				
				
				case 14:
				if(wis()>0)
				spells.add("Holy Sword",4,wis(),lvl()); //+5 attack and bonus. (doesn't stack?). double damage (evil) [undead\enemy, check]?
				break;
				
				case 15:
				if(!spells.exists("Holy Sword"))
				//spells.updateUses("Holy Sword",1);
				//else
					if(wis()>0)
					spells.add("Holy Sword",4,1+wis(),lvl());
					else
					spells.add("Holy Sword",4,1,lvl());
				break;	
				
				
				}
				
							
				
				//if(lvl%6==0&&lvl>6)
				//spells.updateUses("Cure Light Wounds",1);
			
				//if(lvl%5==0&&lvl>10)
				//spells.updateUses("Protection",1);
			
				//if(lvl%4==0&&lvl>12)
				//spells.updateUses("Cure Moderate Wounds",1);
			
				//if(lvl%3==0&&lvl>15)
				//spells.updateUses("Holy Sword",1);
			
				//lvl 5: special mount. no?
				//can't attack NPCs.
				
			}
			
			////System.out.println("br: "+spells.getUses(0));
			if(clas.equals("Rogue"))
			{
				//if(lvl==4)
				//spells.add("Uncanny Dodge",-1,-1,2);//+lvl/4 to ac and reflex
				
				//if(lvl==10)
				//spells.add("Skill Mastery",-1,-1,0);//10+int (instead of d20) to any skill check.
				
				if(lvl==12)
				spells.add("Defensive Roll",-1,1,0);//(not choosable) when dam>=hp then ref save>=damage, half damage taken.
			
			}
			////System.out.println("ar: "+spells.getUses(0));
			
			if(clas.equals("Sorcerer")) //only one with concentration? or not.
			{
			//spells. 10+splvl+cha (DC for saving throws). 10+splvl=<charisma (to able to cast). +cha(not).
				
				if(lvl==2)
				{
				spells.updateUses("Distrup Undead",1);
				spells.updateUses("Magic Missile",1);
				}
		
				if(lvl==3)
				{
				spells.add("Sleep",1,1,5,3); //sleeps.
				spells.updateUses("Resistance",1);
				}

				if(lvl==4)
				{
				spells.updateUses("Sleep",1);
				spells.updateUses("Ray of Frost",1); //last lvl 0
				spells.add("Acid Arrow",2,1,1+lvl/3,2); //2d4 damage every round.
				//melf's acid arrow
				}
				
				if(lvl==5)
				{
				spells.updateUses("Magic Missile",1); //last lvl 1
				spells.updateUses("Acid Arrow",1);
				}
				
				if(lvl==6)
				{
				spells.add("Bull's Strength",2,1,10); //1d4+1 STR
				spells.updateUses("Acid Arrow",1);	//4
				spells.add("Lightning Bolt",3,1,0,1); //(lvl)d6, max 10.
				}
				
				if(lvl==7)
				{
				spells.updateUses("Bull's Strength",1);
				spells.updateUses("Acid Arrow",1);	//last lvl 2
				spells.add("Haste",3,1,lvl); //+4 AC
				//fort saving throw(?)-self
				}
				
				if(lvl==8)
				{
				spells.updateUses("Lightning Bolt",1);   //3 from lvl 3
				spells.add("Ice Storm",4,1,0,2); //5d6 damage (*2, too weak),  1 from lvl 4
				}
								
			
			}
			
			if(clas.equals("Ranger"))
			{
			//spells. 10+splvl+wis (DC for saving throws). 10+splvl=<wisdom (to able to cast). +wis
				
				
								
				if(lvl==4)
				{
					if(wis()>0)
					spells.add("Cure Poison",1,wis(),0);
				}
				
				//get animal spells?
			
				//lvl 5: favoured enemy: undead (bonus:lvl/5)
				if(lvl==6)
				{
					spells.add("Resist",1,1,5); //lvl/3 damr
					//add later
				}
			
				
				if(lvl==8)
				{
					if(wis()>0)
					spells.add("Sleep",2,wis(),5,3); //sleeps.
				}
				
				if(lvl==10) //favoured enemy: dragon (bonus:lvl/5-1)
				{
				spells.add("Cure Light Wounds",2,1,0); //1d8+lvl (max 5)
				spells.updateUses("Resist",2);
				}
				
				if(lvl==11)
				{
					if(wis()>0)
					spells.add("Magic Fang",3,wis(),5); //(lvl/3, max 5) attack and damage bonus
				}
				
				if(lvl==12)
				{
					spells.add("Remove Disease",3,1,0); //remove all "affected"
					
					spells.updateUses("Cure Light Wounds",2);
				}
				
				if(lvl==14)
				{
					if(wis()>0)
					spells.add("Wind",4,wis(),5); //+2 ac, immune to ranged.
				}
				
				if(lvl==15)//favoured enemy: orc (bonus:lvl/5-2)
				{
					spells.add("Cure Serious Wounds",4,3,0); //cure 3d8 + lvl(max 15)
					spells.updateUses("Remove Disease",2);
				}
		
			} //favoured enemy done
			
			if(clas.equals("Monk"))
			{
				
				//lvl 2: deflect arrows. dc 20+weapon attack bonus(magic)<=reflex save. (if arrow hits)
				//if not as "spell", then one hand should be free.
								
				//lvl 4-5: slow fall, fall like 20 feet lower.
				//lvl 6-7: slow fall, fall like 30 feet lower.
				//lvl 8: slow fall, fall like 50 feet lower.
				//lvl 18: takes no damage.
				
							
				//lvl 5: immune to diseases
				
				if(lvl==7)
				spells.add("Wholeness of Body",-1,lvl*2,0); //cures 1 hp.
				
				//lvl 10: unarmed strike empowered with Ki. with no consideration to damr
				//13: +2, 16: +3
				
				//lvl 13: spell resistance, lvl+10.
				
				if(lvl==15)
				spells.add("Quivering Palm",-1,1,0); //if hits and takes damage, fortitude save of 10+lvl/2+wis or dies.
				
				if(lvl==20)
				damr+=10;
									
				
				////System.out.println("n: "+spells.getNum());
				
			} //skills later.
		
	}
	
	
	
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	
	public void move(int xx,int yy,Place p)//,Map m)
	{
	y+=yy;
	x+=xx;
	
	seen.clear();

	Vector v=p.getChars();
			
	spot(v);
	
	place=p;
	
	////System.out.println("v: "+v);
	
	}
	
	
	public void movens(int xx,int yy,Place p)//,Map m)
	{
	//no seen
	
	y+=yy;
	x+=xx;
	
	seen=new Vector();

	place=p;
	}
		
	public int getHiding()
	{
		return hiding;	
	}
	
	public void setHiding(int a)
	{
	hiding=a;		
	}
		
	public int hide()
	{
	skills.check("Hide",null);		
	return hiding;	
	}	
		
	
	public int NoA() //number of attacks
	{
	int s=1,b=ba;
	int ded=5; //deduct
	
	if(clas.equals("Monk"))
	if(weapon.getName().equals("Unarmed Strike"))
	ded=3;
	
	b-=ded;
	while(b>0)
	{
	s++;
	b-=ded;	
	}
	
	//System.out.println("NoA: "+s+" , attacks: "+attacks);	
	
	return s;
	}
	
	
	public void settle(Place[][] map)
	{
	Place.move(this,map,0,0);
	}
	
	
	public void attacks(int a)
	{
	attacks=a;	
	}
	
	/**
	*Checks if the character has the item, in the specified amount or more
	*@param n : the name of the item
	*@param num : the amount needed at least
	*/
	public boolean hasItem(String n,int num)
	{
	return Item.howMany(items,n)>=num;
	}
	
	public int attacks() //attacked this round.
	{
	//when attacks, this goes up.
	return attacks;
	}
	
	public void heal(int a)
	{
		super.heal(a);
		if(hp-dam>0)
		stable=false;
	}
	
	
	public void setMoved(int a)
	{
	moved=a;	
	}
	
	
	public int damage()
	{
		int dam=super.damage();
		
		if(spells.hasDisease("Holy Sword"))
		if(!weapon.getName().equals("Unarmed Strike"))
		dam+=5;
		
		return dam;
		
	}
	
	
	public int rangerBon(Char enm)
	{
	if(clas.equals("Ranger"))
	{
	String race=enm.race();
		if(race.equals("Orc")||race.equals("Goblin"))
		return (lvl/5+1);
		
		if(lvl>=5)
			if(race.equals("Undead"))
			return (lvl/5);
			
		if(lvl>=10)
			if(race.equals("Dragon")||race.equals("Ogre"))
			return (lvl/5-1);
			
		if(lvl>=15)
			if(race.equals("Devil")||race.equals("Demon"))
			return (lvl/5-2);
	}	
	
	return 0;	
	}
	
	/**
	*Attacks an enemy, with spells or physically
	*@param enm : the enemy
	*@param ss : the spell index (if -1 then it doesn't use a spell)
	*/
	public String attack(Char enm,int ss)
	{
	if(ss<0||ss>=spells.getNum())
	return attack(enm);
	else
	return attack(ss,enm);	
	}
	
	/**
	*Attacks an enemy
	*@param enm : the enemy
	*/
	public String attack(Char enm)
	{
	String s="",xt="";
	
	int ac=enm.AC();
	int dice=Dice.roll(20);
	int at=attack()+dice;
	
	String dis=spells.addTurn();
	
	if(dis.indexOf("died")!=-1||dis.indexOf("dying")!=-1)
	return dis;
	
	String pl=spells.canPlay();
	if(!pl.equals(""))
	{
	attacks=NoA();
	return name+" cannot make a move because he is affetced by "+pl+"."+dis;
	}
	////System.out.println("attack: "+at+" ("+attack()+"+"+dice+") , ac: "+ac); //nim.
	String def=""; //deflect arrow
		
		////System.out.println("attack: "+at+" ("+attack()+"+"+dice+") , ac: "+ac); //nim.
		if(weapon.isRanged())
		{
		removeItem(weapon.kind(),1);
			if(enm.getSpells().hasDisease("Wind"))
			dice=1;
			
			if((at>=ac&&dice!=1)||dice==20)
			if(enm instanceof Character)
			{
			Character enmy=(Character)(enm);	
				if(enmy.getClas().equals("Monk"))
				if(enmy.lvl()>=2)
				{
				boolean shi=(shield==null);
				if(!shi)
				shi=shield.getName().equals("Buckler"); //no buckler, =false
				
				boolean wea=weapon.getName().equals("Unarmed Strike");
				
				boolean free; //one hand free
				
				if(wea)
				free=true;
				else
					if(shi)
					free=weapon.getName().indexOf("Great")==-1&&!(weapon.isRanged()&&!weapon.getName().equals("Sling"));
					else
					free=false;
								
				if(free)
				{
				boolean save=enmy.save("Reflex",20+weapon.getMagic());
					if(save)
					{
					String k=weapon.kind();
					k=k.substring(0,k.length()-1);
					dice=1;
					def=enmy.name()+" has deflected the "+k;
					}
				}
				//one hand free. check longbow etc.
				//20+magical bonus: vs. reflex.
				} //lvl 2
						
			} //character
			
		} //ranged
		
	if((at>=ac&&dice!=1)||dice==20)
	{
	int dam=damage();
	
	if(spells.hasDisease("Holy Sword"))
	if(!weapon.getName().equals("Unarmed Strike"))
	if(enm instanceof Enemy)
	dice=20;
	
	dam+=rangerBon(enm);
	
	int crit=weapon.critDam(dice,ac);
	dam+=crit;
		if(crit>0)
		xt="(Critial hit!) ";
	
	if(!clas.equals("Monk")||lvl<10)				
	dam-=enm.getDamr();
		
	if(dam<0)
	dam=0;
	
	String rogue="";		
	if(enm.getHp()-enm.getDam()-dam<=0)
		if(enm instanceof Character)
		{
		Character enmy=(Character)(enm);
			if(enmy.getClas().equals("Rogue"))
			if(enmy.lvl()>=12)
			{
				Spells sp=enmy.getSpells();
				int i=sp.serial("Defensive Roll");
				
				if(sp.getUsed(i)<sp.getUses(i))
				if(save("Reflex",dam))
				{
				dam=dam/2;
				rogue=" (half damage taken because of the Rogue's Defensive Roll skill) ";
				}
			}
		}
				
	enm.dealDam(dam);						
	
	String kill="";
		if(enm.isDead())
		{
		String m;
		if(enm.male())
		m="him";
		else
		m="her";
		kill=" and kills "+m;
		
		addExp(enm.calXP());
		}			
	String porp=" [20 on dice]";
	if(dice!=20)
	porp=" ["+at+">="+ac+"]";
	s=xt+name+" attacks with "+weapon.getName()+" and deals "+dam+rogue+" damage to "+enm.name+kill+porp;
	}	
	else
	{
	String porp=" [1 on dice or Wind]";
	if(!def.equals(""))
	porp=" ["+def+"]";
	if(dice!=1)
	porp=" ["+at+"<"+ac+"]";
	s=name+" attacks with "+weapon.getName()+" and misses "+enm.name+porp;
		if(Dice.roll(100)>50&&weapon.isRanged())
		place.addItem(Item.getItem(weapon.kind(),1));
	}
	
	
	attacks++;
				
			
	return s+"."+dis;
	}
	
	
	/**
	*Attacks an enemy with a special ability
	*@param enm : the enemy
	*@param spell : the spell's name
	*/
	public String attack(Char enm,String spell) //need to add turn somewhere. (etc)
	{
	String s="",xt="";
	
	int ac=enm.AC();
	int dice=Dice.roll(20);
	int at=attack()+dice;
		
	if(spell.equals("Smite Evil"))
	{
		int c=cha();
		if(c>0)
		at+=c;
	}
	
	//spells.addTurn();
	
	//String pl=spells.canPlay();
	//if(!pl.equals(""))
	//return name+" cannot make a move because he is affetced by "+pl+".";
		
	////System.out.println("attack: "+at+" ("+attack()+"+"+dice+") , ac: "+ac); //nim.
	String def=""; //deflect arrow
		
		////System.out.println("attack: "+at+" ("+attack()+"+"+dice+") , ac: "+ac); //nim.
		if(weapon.isRanged())
		{
		removeItem(weapon.kind(),1);
			if(enm.getSpells().hasDisease("Wind"))
			dice=1;
			
			if((at>=ac&&dice!=1)||dice==20)
			if(enm instanceof Character)
			{
			Character enmy=(Character)(enm);	
				if(enmy.getClas().equals("Monk"))
				if(enmy.lvl()>=2)
				{
				boolean shi=(shield==null);
				if(!shi)
				shi=shield.getName().equals("Buckler"); //no buckler, =false
				
				boolean wea=weapon.getName().equals("Unarmed Strike");
				
				boolean free; //one hand free
				
				if(wea)
				free=true;
				else
					if(shi)
					free=weapon.getName().indexOf("Great")==-1&&!(weapon.isRanged()&&!weapon.getName().equals("Sling"));
					else
					free=false;
								
				if(free)
				{
				boolean save=enmy.save("Reflex",20+weapon.getMagic());
					if(save)
					{
					String k=weapon.kind();
					k=k.substring(0,k.length()-1);
					dice=1;
					def=enmy.name()+" has deflected the "+k;
					}
				}
				//one hand free. check longbow etc.
				//20+magical bonus: vs. reflex.
				} //lvl 2
						
			} //character
			
		} //ranged
		
	if((at>=ac&&dice!=1)||dice==20)
	{
	int dam=damage();
	
	if(spells.hasDisease("Holy Sword"))
	if(!weapon.getName().equals("Unarmed Strike"))
	if(enm instanceof Enemy)
	dice=20;
	
	dam+=rangerBon(enm);
	
	int crit=weapon.critDam(dice,ac);
	dam+=crit;
		if(crit>0)
		xt="(Critial hit!) ";
	
	if(spell.equals("Sneak Attack"))
	dam=dam+Dice.roll(((lvl()+1)/2),6);
	
	if(spell.equals("Smite Evil"))
	dam=dam+lvl();
	
	if(!clas.equals("Monk")||lvl<10)				
	dam-=enm.getDamr();
	
	if(dam<0)
	dam=0;
	
	
	String rogue="";		
	if(enm.getHp()-enm.getDam()-dam<=0)
		if(enm instanceof Character)
		{
		Character enmy=(Character)(enm);
			if(enmy.getClas().equals("Rogue"))
			if(enmy.lvl()>=12)
			{
				Spells sp=enmy.getSpells();
				int i=sp.serial("Defensive Roll");
				
				if(sp.getUsed(i)<sp.getUses(i))
				if(save("Reflex",dam))
				{
				dam=dam/2;
				rogue=" (half damage taken because of the Rogue's Defensive Roll skill) ";
				}
			}
		}
	
	
	String palm="";
	
	if(spell.equals("Quivering Palm"))
	{
	int qdc=10+lvl/2+wis();		
	int qd=Dice.roll(20);
	int qdd=enm.getFort();	
	qdd+=qd;
	
		if((qdd>=qdc&&qd!=1)||qd==20)
		palm=enm.name()+" resisted the "+spell+" attack.";
		else
		{
		palm=enm.name()+" died from the "+spell+" attack.";
		dam=enm.getHp()+10;
		}
	
	palm=" ["+palm+"]";		
	}
	
	
	enm.dealDam(dam);						
	
	String addend=""; //add at end
	String kill="";
		if(enm.isDead())
		{
		String m;
		if(enm.male())
		m="him";
		else
		m="her";
		kill=" and kills "+m;
		
		addExp(enm.calXP());
		}
		else
			if(spell.equals("Stunning Attack"))
			{
			int dc=10+wis()+lvl()/2;
			int d=Dice.roll(20);
			int dd=d+enm.getFort();
			
				if(!((dd>=dc&&d!=1)||d==20)) //stun
				{
					//enm.getSpells().getAffected().add("Paralysis");
					//enm.getSpells().getTp().add("0");
					//enm.getSpells().getNot().add("1");
					enm.getSpells().addDisease("Paralysis",1,dc); //or dc=0? (deppends if can heal paralysis.)
					addend=" and stuns him ["+dd+"<"+dc+" or 1 on dice]. ";
				}
				else
				addend=" and fails to stun him ["+dd+">="+dc+" or 20 on dice]. ";
			}
			
	String porp=" [20 on dice]";
	if(dice!=20)
	porp=" ["+at+">="+ac+"]";
	s=xt+name+" uses "+spell+" with "+weapon.getName()+" and deals "+dam+rogue+" damage to "+enm.name+kill+porp+addend+palm;
	}	
	else
	{
	String porp=" [1 on dice or Wind]";
	if(!def.equals(""))
	porp=" ["+def+"]";
	if(dice!=1)
	porp=" ["+at+"<"+ac+"]";
	s=name+" uses "+spell+" with "+weapon.getName()+" and misses "+enm.name+porp;
		if(Dice.roll(100)>50&&weapon.isRanged())
		place.addItem(Item.getItem(weapon.kind(),1));
	}
	
	//attacks++;
	attacks=NoA();
				
	return s+".";
	}
	
	
	public int addMoved() //if and how hungry
	{
	moved++;
	
	int b=speed/3; //6:2, 3:1
	
	int m=Item.howMany(items,"Meals");
	
	if(moved%b==0) //time to eat.
	{
		if(m>0)
		{
		removeItem("Meals",1);	
			if(hunger>0)
			hunger--;
		}
		else
		hunger++;
	}	
	
	
	int h=hunger/3;	
	if(h>1)	
	{
	int DC=h+hunger;	
	int dice=Dice.roll(20);
	
		if(dice!=20&&(dice+con()<DC||dice==1))
		{
		dealDam(hunger);	
		return hunger;			
		}
		
	}
	
	
	return 0;
	
	}
	
	public boolean isDying() //and "isFainted()" (or whatever)
	{
	int d=hp-dam;
	if(d>-5&&d<=0)
	return true;
	
	return false;
	}
	
	public boolean isDead()
	{
	if(hp-dam<=-5)
	return true;
	
	return false;
	}
	
	public int getHunger()
	{
	return hunger;	
	}
	
	public int getMoved()
	{
	return moved;	
	}
	
	public int getSpeed()
	{
	return speed;	
	}
	
	public int abil()
	{
	return abil;	
	}
	
	public int skil()
	{
	return skil;	
	}
		
	public void addAbil(int a)
	{
		abil=abil+a;	
	}
	
	public void addSkil(int a)
	{
		skil=skil+a;	
	}
	
	
	public int AC()
	{
	int a=super.AC();
	if(spells.hasDisease("Rage"))
	a-=2;
	
	if(clas.equals("Monk"))
	if(armor==null)
	{
	int b=wis()+lvl/5;
	if(b>0)
	a+=b;		
	}
	
	if(clas.equals("Barbarian"))
	a=a+lvl/5;
	
	if(clas.equals("Rogue"))
	if(lvl>=4)
	a=a+lvl/4;
	
	if(spells.hasDisease("Protection"))
	a+=2;
	
	if(spells.hasDisease("Haste"))
	a+=4;
	
	if(spells.hasDisease("Wind"))
	a+=2;
	
	return a;
	}
			
	
	public int getHp()
	{
	int h=super.getHp();
	
	if(spells.hasDisease("Rage"))
	{
	h=h+2*lvl();
		if(lvl>=12)
		h+=lvl();
	}		
	return h;	
	}
	
	
	public int getSTR()
	{
	int s=super.getSTR();
	
	if(spells.hasDisease("Rage"))
	{
	s+=4;
		if(lvl>=12)
		s+=2;
	}	
	
	if(spells.hasDisease("Bull's Strength"))
	{
	//not efficient (num someplace before: -1 else.
	String num=(String)spells.getSdc().elementAt(spells.affectedSerial("Bull's Strength"));
	int st=Integer.parseInt(num);
	s+=st;		
	}
	
	return s;	
	}	
	
	
	public int getWill()
	{
	int w=super.getWill();
	
	if(clas.equals("Paladin")&&cha()>0)
	w+=cha();
	
	if(spells.hasDisease("Rage"))
	{
	w+=2;
		if(lvl>=12)
		w++;
	}
	
	if(spells.hasDisease("Resistance"))
	{
	w+=2;
	}
	
	return w;	
	}	
	
	
	public int getRef()
	{
	int w=super.getRef();
	
	if(clas.equals("Paladin")&&cha()>0)
	w+=cha();
	
	if(spells.hasDisease("Resistance"))
	{
	w+=2;
	}
	
	if(clas.equals("Barbarian"))
	w=w+lvl/5;
	
	if(clas.equals("Rogue"))
	if(lvl>=4)
	w=w+lvl/4;
	
	
	return w;	
	}
	
	
	public int getFort()
	{
	int w=super.getFort();
		
	if(clas.equals("Paladin")&&cha()>0)
	w+=cha();
	
	if(spells.hasDisease("Resistance"))
	{
	w+=2;
	}
	
	return w;	
	}
	
	public int getDamr()
	{
	int d=super.getDamr();	
	
	if(spells.hasDisease("Resist"))
	d=d+lvl/3;
		
	return d;
	}
	
	public int getSr()
	{
	int r=super.getSr();	
	if(clas.equals("Monk"))
	if(lvl>=13)
	r=r+lvl+10;
			
	return r;	
	}
	
	/**
	*Connverts a character to a String (coded)
	*/
	public String write()
	{
		String s=super.write();	
		s="R"+s.substring(1,s.length()-3)+",";
		s+=x+",";				
		s+=y+",";
		s+=lvl+",";
		s+=exp+",";
		s+=need+",";
		s+=hiding+",";
		s+=attacks+",";
		s+=hunger+",";
		s+=speed+",";
		s+=moved+",";
		s+=abil+",";
		s+=skil+",";
		s+=clas+",";
		s+=ba+",";
		s+=stable+",";
		s+=skills.write()+",";
		return s+"*R";
	}
	
	
	
	/**
	*Returns a character by a coded String
	*@param s : the coded String
	*@param plc : the place it is in
	*@param seens : the characters it has seen
	*/
	public static Character readC(String s,Place plc,Vector seens) //careful of "R*R"
	{
	Character d=new Character();
	d.place=plc;
	s=s.substring(2);
	
	int b=s.indexOf(",");
	d.dam=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);	
	
	b=s.indexOf(",");
	d.name=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hp=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.str=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.dex=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.race=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	
		
	if((s.substring(0,b)).equals("true"))
	d.male=true;
	else
	d.male=false;
	
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.con=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.wis=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.inl=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.cha=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.damr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.nata=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.sr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.gp=Double.parseDouble(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*W"); //have to be.
	d.weapon=Weapon.readW(s.substring(0,b+1));
	s=s.substring(b+3);
	
	if(!d.weapon.getName().equals("Unarmed Strike"))
	d.items.add(d.weapon);
	
	d.weapon.setOwner(d);
	
	String th=s.substring(0,s.indexOf(";"));
	s=s.substring(s.indexOf(";")+1);
	
	////System.out.println("th: "+th);
	b=th.lastIndexOf("*A");
	if(b!=-1)
	{
	d.armor=Armor.readA(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.armor);
	d.armor.setOwner(d);
	}
	
	b=th.lastIndexOf("*S");
	if(b!=-1)
	{
	d.shield=Shield.readS(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.shield);
	d.shield.setOwner(d);
	}

	b=s.indexOf(";");
	////System.out.println("BI: "+b+" , "+s);
	String i=s.substring(0,b);
	//items:
	while(i.indexOf("*")!=-1)
	{
	Item t=null;	
	int c=0;
	
	if(i.indexOf("I")==0)
	{
		c=i.indexOf("*I");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*I")+2;	
		}
		
		t=Item.read(i.substring(0,c+1));
	}
	
	if(i.indexOf("W")==0)
	{	
		c=i.indexOf("*W");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*W")+2;	
		}
		t=Weapon.readW(i.substring(0,c+1));
		((Weapon)t).setOwner(d);
			
	}
	
	if(i.indexOf("A")==0)
	{
		c=i.indexOf("*A");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*A")+2;	
		}
		t=Armor.readA(i.substring(0,c+1));	
		((Armor)t).setOwner(d);
	}
	
	if(i.indexOf("S")==0)
	{
		c=i.indexOf("*S");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*S")+2;	
		}
		t=Shield.readS(i.substring(0,c+1));
		
		((Shield)t).setOwner(d);
	}
	
	
	d.items.add(t);
	i=i.substring(c+2);
	}
	//end items
	
	s=s.substring(b+1);
	
	
	b=s.indexOf(",");
	d.ref=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.will=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.fort=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*L");
	d.spells=Spells.read(s.substring(0,b));
	s=s.substring(b+3);
	d.spells.setOwner(d);
	
	b=s.indexOf(",");
	//d.attacked=Boolean.getBoolean(s.substring(0,b));
	if((s.substring(0,b)).equals("true"))
	d.attacked=true;
	else
	d.attacked=false;
	s=s.substring(b+1);
	
	//seen!
	b=s.indexOf(",");
	int size=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	Vector sen=new Vector();
	for(int j=0;j<size;j++)
	{
	b=s.indexOf(",");
	int index=Integer.parseInt(s.substring(0,b));
	sen.add(index+"");
	//Char c=d.bySerial(index);
	//d.seen.add(c);
	s=s.substring(b+1);
	}
	seens.add(sen);
	//end seen
	
	//character:
	b=s.indexOf(",");
	d.x=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.y=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);	
	b=s.indexOf(",");
	d.lvl=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.exp=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.need=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hiding=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.attacks=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hunger=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.speed=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.moved=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.abil=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.skil=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.clas=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.ba=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.stable=Boolean.getBoolean(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*K");
	d.skills=Skills.read(s.substring(0,b+1));
	s=s.substring(b+3);
	d.skills.setOwner(d);
	
	return d;
	}
	
	
	public static Character readC(String s,Place plc) //careful of "R*R"
	{
	Character d=new Character();
	d.place=plc;
	s=s.substring(2);
	
	int b=s.indexOf(",");
	d.dam=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);	
	
	b=s.indexOf(",");
	d.name=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hp=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.str=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.dex=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.race=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	
		
	if((s.substring(0,b)).equals("true"))
	d.male=true;
	else
	d.male=false;
	
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.con=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.wis=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.inl=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.cha=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.damr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.nata=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.sr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.gp=Double.parseDouble(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*W"); //have to be.
	d.weapon=Weapon.readW(s.substring(0,b+1));
	s=s.substring(b+3);
	
	if(!d.weapon.getName().equals("Unarmed Strike"))
	d.items.add(d.weapon);
	
	d.weapon.setOwner(d);
	
	String th=s.substring(0,s.indexOf(";"));
	s=s.substring(s.indexOf(";")+1);
	
	////System.out.println("th: "+th);
	b=th.lastIndexOf("*A");
	if(b!=-1)
	{
	d.armor=Armor.readA(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.armor);
	d.armor.setOwner(d);
	}
	
	b=th.lastIndexOf("*S");
	if(b!=-1)
	{
	d.shield=Shield.readS(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.shield);
	d.shield.setOwner(d);
	}

	b=s.indexOf(";");
	////System.out.println("BI: "+b+" , "+s);
	String i=s.substring(0,b);
	//items:
	while(i.indexOf("*")!=-1)
	{
	Item t=null;	
	int c=0;
	
	if(i.indexOf("I")==0)
	{
		c=i.indexOf("*I");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*I")+2;	
		}
		
		t=Item.read(i.substring(0,c+1));
	}
	
	if(i.indexOf("W")==0)
	{	
		c=i.indexOf("*W");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*W")+2;	
		}
		t=Weapon.readW(i.substring(0,c+1));
		((Weapon)t).setOwner(d);
			
	}
	
	if(i.indexOf("A")==0)
	{
		c=i.indexOf("*A");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*A")+2;	
		}
		t=Armor.readA(i.substring(0,c+1));	
		((Armor)t).setOwner(d);
	}
	
	if(i.indexOf("S")==0)
	{
		c=i.indexOf("*S");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*S")+2;	
		}
		t=Shield.readS(i.substring(0,c+1));
		
		((Shield)t).setOwner(d);
	}
	
	d.items.add(t);
	i=i.substring(c+2);
	}
	//end items
		
	s=s.substring(b+1);
	
	
	b=s.indexOf(",");
	d.ref=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.will=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.fort=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*L");
	d.spells=Spells.read(s.substring(0,b));
	s=s.substring(b+3);
	d.spells.setOwner(d);
	
	b=s.indexOf(",");
	//d.attacked=Boolean.getBoolean(s.substring(0,b));
	if((s.substring(0,b)).equals("true"))
	d.attacked=true;
	else
	d.attacked=false;
	s=s.substring(b+1);
	
	//seen!
	b=s.indexOf(",");
	int size=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	d.seen=new Vector();
	for(int j=0;j<size;j++)
	{
	b=s.indexOf(",");
	int index=Integer.parseInt(s.substring(0,b));
	Char c=d.bySerial(index);
	d.seen.add(c);
	s=s.substring(b+1);
	}
	//end seen
	
	//character:
	b=s.indexOf(",");
	d.x=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.y=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);	
	b=s.indexOf(",");
	d.lvl=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.exp=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.need=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hiding=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.attacks=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hunger=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.speed=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.moved=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.abil=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.skil=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.clas=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.ba=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.stable=Boolean.getBoolean(s.substring(0,b));
	s=s.substring(b+1);

	b=s.indexOf("*K");
	d.skills=Skills.read(s.substring(0,b+1));
	s=s.substring(b+3);
	d.skills.setOwner(d);
	
	return d;
	}
	
	
	/*
	public String toString()
	{
	
	////System.out.println("Cha");
	
		return "Character";
	}
	*/
		
}