const controller = require('./controller.js');
const {ReplyToCommand, ReceivedCommand}= require('./commands.js');

const TYPE_VALUE_TO_NAME = {
  1: 'pp',
  2: 'xp',
}

/*
input: {
}
*/
async function showCounters(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let inputOptions = {
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  }
  
  let messageContent = await controller.RunSafe(() => controller.ShowCounters(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Show counters")
}

/*
input: {
}
*/
async function setCounter(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let inputOptions = {
    who: parsedInput.who,
    counterName: parsedInput.counter_name,
    value: parsedInput.value,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  }
  
  let messageContent = await controller.RunSafe(() => controller.SetCounter(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Set counter")
}

async function modifyCounter(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let inputOptions = {
    who: parsedInput.who,
    counterName: parsedInput.counter_name,
    amount: parsedInput.amount,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  }
  
  let messageContent = await controller.RunSafe(() => controller.ModifyCounter(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Modify counter")
}

/*
input: {
}
*/
async function removeCounter(input, event) {
  await ReceivedCommand(event)  
  
  let parsedInput = parseInput(event)
  let inputOptions = {
    who: parsedInput.who,
    counterName: parsedInput.counter_name,
    channelId: parsedInput.channel_id,
    userId: parsedInput.user_id,
  }
  
  let messageContent = await controller.RunSafe(() => controller.RemoveCounter(inputOptions, controller.DefaultIocContainer))
  await replyToCommand(event, messageContent, "Remove counter")
}

async function replyToCommand(event, messageContent, commandName) {
  console.log(commandName + ' result: ' + messageContent.join('\n'))
  await ReplyToCommand(event, messageContent)  
  await updateSummaryMessage(event)
}

/* 
Returns input: {
  who, counter_name
  amount, value
  channel_id, user_id (number - defaults to 0)
  }
  
  Strings default to '', numbers default to undefined.
*/
function parseInput(event) {
  let dataOptions = event.data.options
  
  var inputMap = new Map()
  dataOptions.forEach(option => {
    inputMap.set(option.name, option.value)
  }) 
  
  let defaultWho = event.member.nick ?? event.member?.user?.username
  
  let name = inputMap.get('name')?.trim() ?? 'pp'
  let type = inputMap.get('type') ?? 0
  let overridenName = TYPE_VALUE_TO_NAME[type]
  if(overridenName) {
    name = overridenName
  }
  
  return  {
    who: inputMap.get('who') ?? defaultWho,
    counter_name: name,
    amount: inputMap.get('amount') ?? 1,
    value: inputMap.get('value') ?? 2,           
    channel_id : event.channel_id ?? '', 
    user_id: event.member.user.id ?? 0,
  }
}
    

async function updateSummaryMessage(event) {
  let updateInput = {
    event: event,
  }
  await controller.UpdatePinnedSummaryMessage(updateInput, controller.DefaultIocContainer)  
}

module.exports = {
  ShowCounters: showCounters,
  SetCounter: setCounter,
  RemoveCounter: removeCounter,
  ModifyCounter: modifyCounter,
};
