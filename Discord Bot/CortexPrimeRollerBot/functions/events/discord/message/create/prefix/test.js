const rollerTester = require("../../../../../../common/tester.js");
const { SendMessage } = require("../../../../../../common/commands.js");

module.exports = {
  async execute(prefixCommandInteraction, msgContent) {
    await handleCommand(prefixCommandInteraction, msgContent);
  },
};

// params: "name prefix"
async function handleCommand(prefixCommandInteraction, msgContent) {
  let params = msgContent.split(" ");
  console.log(`params: ` + params.join(", "));

  let inputOptions = {};
  if (params[1]) {
    inputOptions.namePrefix = params[1];
  }

  let testResults = await rollerTester.RunTests(inputOptions);

  let messageContent = testResults.messages;
  let totalSuccess = testResults.totalSuccess;
  let totalFail = testResults.totalFail;

  let summaryMessage = `${totalSuccess}/${totalSuccess + totalFail} succeeded`;

  let color = totalFail == 0 ? 0x00aa00 : 0xff0000; // Green / red color
  let summary = {
    title: summaryMessage,
    type: "rich",
    color: color,
  };

  let options = {
    splitMessages: true,
    replyToMessage: true,
    embed: summary,
  };
  await SendMessage(prefixCommandInteraction, messageContent, options);

  console.log(`Summary: ` + summaryMessage);
}
