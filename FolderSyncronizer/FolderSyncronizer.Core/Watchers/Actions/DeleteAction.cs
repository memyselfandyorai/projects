﻿using System;
using System.IO;

namespace FolderSyncronizer.Core.Watchers.Actions
{
    public class DeleteAction : ActionBase
    {
        // Do not use - for serialization only! All properties should not be set externally.
        public DeleteAction() { }

        public DeleteAction(string sourceFile, string targetFile)
        {
            SourceFile = sourceFile;
            TargetFile = targetFile;
        }

        public override bool IsCancelled { get => base.IsManuallyCancelled || GetDeleteType() == DeleteType.Cancelled; set => base.IsCancelled = value; }

        protected override string ActionDescription => $"Delete: ({GetDeleteType()}) '{TargetFile}' ('{SourceFile}')";

        protected override void ApplyAction()
        {
            switch (GetDeleteType())
            {
                case DeleteType.File:
                    File.Delete(TargetFile);
                    break;
                case DeleteType.Directory:
                    Directory.Delete(TargetFile, true);
                    break;
            }
        }

        public override bool ShouldAffectOlderAction(ActionBase olderAction)
        {
            return base.ShouldAffectOlderAction(olderAction)
                // this.SourceFile is a Directory:
                || olderAction.SourceFile.Contains(this.SourceFile + Path.DirectorySeparatorChar, StringComparison.InvariantCultureIgnoreCase);
        }

        protected override void ChangeOlderAction(ActionBase olderAction)
        {
            if(olderAction is RenameAction)
            {
                // It shouldn't happen for renames, since if the source was renamed, it can't be deleted as the original path.
                // BUT it can happen if I create a new file with the same name. In that case, I have no reason to cancel the previous rename - it touches only new files.
            }
            else
            {
                // True for "copy", we don't want to copy it if it was later deleted. True for Delete since the new file shouldn't exist.
                olderAction.IsCancelled = true;
            }
        }

        private DeleteType GetDeleteType()
        {
            if(File.Exists(SourceFile) || File.Exists(TargetFile))
            {
                return DeleteType.File;
            }
            if(Directory.Exists(SourceFile) || Directory.Exists(TargetFile))
            {
                return DeleteType.Directory;
            }

            // Note that due to renames, this might show up as "cancelled" even though it *can* be performed later. This must not be serialized.
            return DeleteType.Cancelled;
        }

        private enum DeleteType
        {
            Cancelled,
            File,
            Directory,
        }
    }
}
