
// https://autocode.com/guides/how-to-build-a-discord-bot/#testing-commands

module.exports = {
  RollCortexPrime: rollCortexPrime,
  RollCortexPrimeKeep: rollCortexPrimeKeep,
};

const MIN_EFFECT = 4

/*
input: {diceRoll - list of numbers, keep (optional) - positive number of dice to keep for score}
output: {messageContent , rolls , scores, maxScores }
*/
function rollCortexPrime(input){
  console.log('input: ' + JSON.stringify(input))

  diceRoll = input.diceRoll
  keep = Math.max(input.keep ?? 2, 1) // Minimum 1

  var rolls = rollDice(diceRoll)
  return getResultsBasedOnRolls(diceRoll, rolls, keep)
}

/*
cachedRolls: {diceRoll, rolls} of previous roll
*/
function rollCortexPrimeKeep (cachedRolls, keep) {
  console.log('input cachedRolls: ' + JSON.stringify(cachedRolls))
  console.log('input keep: ' + keep)
  
  return getResultsBasedOnRolls(cachedRolls.diceRoll, cachedRolls.rolls, keep)
}

/*
input: rolls - list of numbers, keep (optional) - positive number of dice to keep for score}
output: {messageContent , rolls , scores, maxScores }
*/
function getResultsBasedOnRolls(diceRoll, rolls, keep) {
  let messageContent = [ ];

  console.log('rolls: ' + rolls.map(JSON.stringify).join(' ; '))
  var scores = getAllScoreOptions(rolls, Math.max(keep, 1))
  console.log('scoreEffects: ' + scores.map(JSON.stringify).join(' ; '))
  var maxScores = getMaximumScoreEffects(scores )
  console.log('Max scores: ' + maxScores.map(JSON.stringify).join(' ; '))

  messageContent = formatResults(diceRoll, rolls, maxScores)
  console.log('Formatted results: ')
  messageContent.forEach(t => console.log(t))

  return {
    messageContent: messageContent,
    rolls : rolls,
    scores : scores,
    maxScores : maxScores,
  }
}

/*
Rolling: D8 2D10 
D8 : (1) 
D10 : 4 4 
Best Total: 8 (4 + 4) with Effect: D4
*/
function formatResults(dicePool, rolls, maxScores){
  let rolling = new Map()
  dicePool.forEach(die => {
    rolling.set(die, 1 + (rolling.get(die) || 0))
  })
  let dicePoolStr = Array.from(rolling.keys()).sort(intSort).map(d => (rolling.get(d) + 'D' + d)).join(' ')
  
  var messageContent = [
    'Rolling: ' + dicePoolStr,
  ]
  
  let rollResults = new Map()
  rolls.forEach(roll => {
    let resultArray = rollResults.get(roll.die) || []
    resultArray.push(roll.result)
    rollResults.set(roll.die, resultArray)
  })
  
  let rollDiesAsc = Array.from(rollResults.keys()).sort(intSort)
  messageContent = messageContent.concat(rollDiesAsc.map(die => formatDieRoll(die, rollResults)))
   
  
  messageContent = messageContent.concat(['Results ordered by best total:'])
  messageContent = messageContent.concat(
    maxScores.sort((scoreEffect1, scoreEffect2) => (scoreEffect1.score > scoreEffect2.score) ? -1 : 1).map(formatScoreEffect)
  )
  return messageContent
}

function intSort(a, b) {
  return a - b;
}

function formatDieRoll(die, rollResults) {
  // D10 : 4 4 
  let results = rollResults.get(die).sort().map(v => (v!=1) ? v : '**(1)**').join(' ')
  return `D${die} : ${results}`
}

function formatScoreEffect(scoreEffect){
  // 8 (4 + 4) with Effect: D4
  let reason = scoreEffect.sourceRolls.map(r => r.result).join('+')
  return `Score: ${scoreEffect.score } (${reason }) with Effect: D${scoreEffect.effect}`
}

// Input: list of {score, effect, sourceRolls}
// Returns list of {score, effect, sourceRolls}, filtered by scores who make sense
function getMaximumScoreEffects(scoreEffectResults) {
    if (scoreEffectResults.length <= 1){
      return scoreEffectResults
    }
  
    // Key: effect. Value: ScoreEffect of a maximum scores.
    effectToScoreMap = new Map()
    scoreEffectResults.forEach(scoreEffect => {
      let currentMaxScore = effectToScoreMap.get(scoreEffect.effect)?.score || 0
      if (scoreEffect.score > currentMaxScore ){
        effectToScoreMap.set(scoreEffect.effect, scoreEffect)
      }  
    })   

    console.log(effectToScoreMap);
    let scoreEffectList = Array.from(effectToScoreMap.values())
    console.log(scoreEffectList);
    if (scoreEffectList.length <= 1){
      return scoreEffectList 
    }
    
    let sortedByEffectDesc = scoreEffectList.sort((scoreEffect1, scoreEffect2) => (scoreEffect1.effect > scoreEffect2.effect ) ? -1 : 1)
    
    // Highest effect is always an option
    maxScoreEffectResults = [sortedByEffectDesc[0]]
    sortedByEffectDesc.slice(1).forEach(scoreEffect => {
      // We know the effect is strictly lower than anything in maxScoreEffectResults.
      // If we have the highest score, we add ourselves.
      if(scoreEffect.score > Math.max(maxScoreEffectResults.map(se => se.score))){
        maxScoreEffectResults.push(scoreEffect )
      }
    })
    
    return maxScoreEffectResults
  }

// Input: list of { die: / result: } , keep - number of how many score dice
// Returns list of {score, effect, sourceRolls}
// sourceRolls - list of source rolls which got this score
function getAllScoreOptions(dieResults, keep) {
  let validForScore = dieResults.map((roll, index) => { roll.originalIndex = index; return roll;}).filter(d => d.result != 1)
  let length = validForScore.length 
  if (length == 0){
    let allPossibleEffects = dieResults.map(d => d.die)
    let effectWithoutScore = Math.max(...allPossibleEffects , MIN_EFFECT)
    return [{score: 0, effect: effectWithoutScore, sourceRolls: []}]
    }
    
  /*
  if (length == 1){
    let singleResult = validForScore[0]
    effectDicePool = dieResults.filter((element, index) => index != singleResult.originalIndex).map(d => d.die)
    maxEffect = Math.max(...effectDicePool, MIN_EFFECT)
    return [{score: singleResult.result, effect: maxEffect, sourceRolls: [singleResult] }]
  }
  */
  
  let allKeepIndexSets = getAllIndexSets(keep, 0, length)
  if (allKeepIndexSets.length == 0 || allKeepIndexSets[0].size == 0){
    throw 'Unexpected behavior: ' + printIndexSet(allKeepIndexSets) + ' : ' + keep + ',' + length;
  }
  
  var results = []
  allKeepIndexSets.forEach(indexSet => {
    let allValidResults = Array.from(indexSet).map(i => validForScore[i])
    let originalIndexSet = new Set(allValidResults.map(result => result.originalIndex))
    
    let totalScore = allValidResults.map(r => r.result).reduce((x,y) => x+y, 0)
    effectDicePool = dieResults.filter((_, index) => !originalIndexSet.has(index)).map(d => d.die)
    maxEffect = Math.max(...effectDicePool, MIN_EFFECT)
    results.push({score: totalScore  , effect: maxEffect, sourceRolls: allValidResults })
  })
  
  console.log(`allKeepIndexSets: ` + printIndexSet(allKeepIndexSets));
  console.log(`results: ` + JSON.stringify(results));
  
  return results
}

// Returns a list of Set of indexes. Size of each hashset is min(keep, length).
// Algorithm to get all "k out of n" indexes -  list of set / hashset
// length > 0.
// startIndex - when calling this non-recursively, should be 0.
function getAllIndexSets(keep, startIndex, length) {
  if (keep == 0 || length == 0 || startIndex == length) {
    return [new Set()];
  }
  
  countLeft = length - startIndex
  if (keep >= countLeft ) {
    let hash = new Set(range(startIndex, length - 1))
    return [hash]
  }
  
  var resultSets = []
  // Can't take too much now.
  for (var i = startIndex; i < length - keep + 1 ; i++) { 
    let allChildSets = getAllIndexSets(keep - 1, i + 1, length)
    let resultsForIndex = allChildSets.map(hashset => hashset.add(i))
    resultSets = resultSets.concat(resultsForIndex)
  }
  
  return resultSets 
}

function range(startAt, endAt) {  
    return [...Array(endAt-startAt+1).keys()].map(i => i + startAt);
}


// Input: list of dice max values
// Returns { die: / result: }
function rollDice(dice) {
  return dice.map( d => ({ result: rollDie(d) , die:d }))
}

function rollDie(die) {
  return Math.floor(Math.random() * die) + 1;
}

function printIndexSet(listOfSet) {
  return '[' + listOfSet.map(s => '{' +  Array.from(s).join(',')+ '}').join(',') + ']'
}

function debugTest () {

  console.log(`getAllIndexSets A: ` + printIndexSet(getAllIndexSets(1, 0, 5)));
  console.log(`getAllIndexSets B: ` + printIndexSet(getAllIndexSets(1, 3, 5)));
  console.log(`getAllIndexSets C: ` + printIndexSet(getAllIndexSets(2, 0, 3)));
  console.log(`getAllIndexSets D: ` + printIndexSet(getAllIndexSets(3, 0, 3)));
  console.log(`getAllIndexSets E: ` + printIndexSet(getAllIndexSets(3, 0, 4)));
  console.log(`getAllIndexSets F: ` + printIndexSet(getAllIndexSets(4, 0, 4)));
  console.log(`getAllIndexSets G: ` + printIndexSet(getAllIndexSets(5, 0, 4)));
  console.log(`getAllIndexSets H: ` + printIndexSet(getAllIndexSets(1, 0, 1)));
  console.log(`getAllIndexSets I: ` + printIndexSet(getAllIndexSets(0, 0, 1)));
  console.log(`getAllIndexSets J: ` + printIndexSet(getAllIndexSets(1, 0, 0)));
  /*
  getAllIndexSets A: [{0},{1},{2},{3},{4}]
  getAllIndexSets B: [{3},{4}]
  getAllIndexSets C: [{1,0},{2,0},{2,1}]
  getAllIndexSets D: [{0,1,2}]
  getAllIndexSets E: [{2,1,0},{3,1,0},{3,2,0},{2,3,1}]
  getAllIndexSets F: [{0,1,2,3}]
  getAllIndexSets G: [{0,1,2,3}]
  getAllIndexSets H: [{0}]
  getAllIndexSets I: [{}]
  getAllIndexSets J: [{}]
  
  */
  
}