package Lights;

import General.Color;
import General.NormalVector;
import General.Point;

/**
 * 
 * The class represents a point light
 *
 */
public class PointLight extends Light {

	
	private Point pos;
	private double attenuation[]; //kc,kl,kq
	public static final int KC=0,KL=1,KQ=2; //indices
	

	public PointLight()
	{
		attenuation=new double[3];
		setAttenuation(1,0,0);
	}
	
	public Point getPos() {
		return pos;
	}


	public void setPos(Point pos) {
		this.pos = pos;
	}


	public double getKC() {
		return attenuation[KC];
	}
	public double getKL() {
		return attenuation[KL];
	}
	public double getKQ() {
		return attenuation[KQ];
	}
	

	public void setAttenuation(double kc,double kl,double kq) {
		attenuation[KC]=kc;
		attenuation[KL]=kl;
		attenuation[KQ]=kq;
	}

	
	public boolean hasAllValues() {
		if(pos==null)
			return false;
		
		return true;
	}

	
	public NormalVector getDirection(Point hit)
	{
		return NormalVector.subPoints(pos,hit);
	}

	public double getDistance(Point hit)
	{
		return pos.distance(hit);
	}
	
	public Color getColor(Point hit)
	{
		double distance=getDistance(hit);
		double denom = getKC() + getKL()*distance + getKQ()*distance*distance;
		return color.mult(1/denom);
	}
	
}
