import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.net.URL;

import javax.swing.*;


public class Card {
	
	
	private int num; //2=14
	
	private int sn; //0-3
	
	
	
	public Card(int n,int s)
	{
	num=n;
	sn=s;
	}
	
	
	public boolean same(Card c)
	{
	if(c.num==num&&c.sn==sn)
	return true;	
	return false;
	}
	
	
	public boolean same(int n,int s)
	{
	if(n==num&&s==sn)
	return true;	
	return false;
	}
	
	
	public int num()
	{
	return num;	
	}
	
	public String getNum()
	{
	if(num<=10)
	return num+"";
	
	if(num==11)		
	return "J";
	if(num==12)		
	return "Q";
	if(num==13)		
	return "K";
	
	return "A";
	}
	
	public static String getNum(int n)
	{
	if(n==11)
	return "Jack";
	if(n==12)
	return "Queen";
	if(n==13)
	return "King";	
	if(n==14)
	return "Ace";
		
	return n+"";	
	}
	
	
	public int sn()
	{
	return sn;	
	}
	
	public static ImageIcon getUpside(JPanel main)
	{
		URL url=null;
		ImageIcon m;
			try{
				File jar=new File("Poker.jar");
					if(jar.exists())
					{
				    //url = main.getClass().getResource("pics/back.GIF");
					url = Game.toCards.getClass().getResource("pics/back.GIF");
				    m=new ImageIcon(url);
					}
					else
					m=new ImageIcon("pics/back.gif");
		
		m.setDescription("back");
		return m;
		
			}catch(Exception e){return null;}	
	}
	
	
	public ImageIcon getIcon(JPanel main)
	{
		String fil=getFile();
		URL url=null;
		ImageIcon m;
		File jar=new File("Poker.jar");
			try{
			
				if(jar.exists())
				{
			    url = Game.toCards.getClass().getResource("pics/"+fil+".gif");
			    //JOptionPane.showMessageDialog(main,"ok1 "+url+","+fil+","+main.getClass());
			    m=new ImageIcon(url);
			    //JOptionPane.showMessageDialog(main,"ok2");
			    
			    //JOptionPane.showMessageDialog(main,"debugging: wrong");
				}
				else
				{
				m=new ImageIcon("pics/"+fil+".gif");
				
				//JOptionPane.showMessageDialog(Main.forCards,"debugging: right");
				//JOptionPane.showMessageDialog(Main.forCards,m);
				}
				
		//ImageIcon m=new ImageIcon(Main.url.getPath()+"\\"+fil+".gif");
		
			
		m.setDescription(fil);
		
		return m;
			}catch(Exception e){JOptionPane.showMessageDialog(main,e);return null;}
			//}catch(Exception e){JOptionPane.showMessageDialog(main,jar+","+url+","+fil);return null;}
	}
	
	
	public String getFile()
	{
	String s="";
	
	if(num<10)
	s=s+num;
	else
	{
	char c='a';
		if(num==10)		
		c='t';	
		if(num==11)		
		c='j';	
		if(num==12)		
		c='q';	
		if(num==13)		
		c='k';	
	s=s+c;		
	}
	
	char c='s'; //3:spade (ale)
	if(sn==0)		
	c='h';	//0:heart
	if(sn==1)		
	c='d';	//1:diamond
	if(sn==2)	
	c='c'; //2:clib (tiltan)	
	
	
	s=s+c;
	
	return s;
	}
	
	
	public static Card getCard(String m)
	{
	int n=0,s=-1;
	
	char c=m.charAt(0);
	s=Integer.parseInt(m.charAt(1)+"");
	
	if(c=='t')
	n=10;	
	if(c=='j')
	n=11;		
	if(c=='q')
	n=12;	
	if(c=='k')
	n=13;	
	if(c=='a')
	n=14;	
	
	if(n==0)
	n=Integer.parseInt(c+"");
	
	return new Card(n,s);
	}
	
	
	public String write()
	{
	String s="C*";
	s+=num+",";
	s+=sn+",";	
		
	return s+"*C";
	}
	
	public static Card read(String s)
	{
	s=s.substring(2);
	int b;
	Card card=new Card(0,0);
	
	b=s.indexOf(",");
    card.num=Integer.parseInt(s.substring(0,b));
   	s=s.substring(b+1);
	
	b=s.indexOf(",");
    card.sn=Integer.parseInt(s.substring(0,b));
   	s=s.substring(b+1);
	
	return card;
	}
	
	
	
	
	
	public String toString()
	{
	return "("+num+","+sn+")";	
	}
	
}