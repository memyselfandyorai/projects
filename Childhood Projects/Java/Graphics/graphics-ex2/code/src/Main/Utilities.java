package Main;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;

import General.Color;
import General.NormalVector;
import General.Point;

/**
 * 
 * A class with useful static methods for our project
 *
 */
public class Utilities {

	public static final double[] NO_SOLUTION = null;
	
	//null. can't convert something to Utilities.WRONG_FORMAT
	public static final Object WRONG_FORMAT=null; 
	
	/**
	 * solves a quadratic equation: ax^2+bx+c=0
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public static double[] solveQuadraticEquation(double a,double b, double c)
	{
		double delta=Math.pow(b, 2) - 4*a*c;
		
		if (delta<0)
			return NO_SOLUTION;
		double x1,x2;
		double Da=2*a;
		double sqrt=Math.sqrt(delta);
		
		
		x1=(-b + sqrt)/Da;
		x2=(-b - sqrt)/Da;
				
		double result[]=new double[2];
		result[0]=x1;
		result[1]=x2;
		
		return result;
	}
	
	/**
	 * given image data and coordinates x,y :
	 * returns the color in the image in the given coordinates
	 * 
	 * */
	public static Color getColorFromImage(ImageData image,int x,int y)
	{
		int pixel=image.getPixel(x,y);
		RGB rgb = image.palette.getRGB(pixel);
		double r = ((double)rgb.red)/255;
		double g = ((double)rgb.green)/255;
		double b = ((double)rgb.blue)/255;

		return new Color(r,g,b);
	}
	
	
	/**
	 * returns the integer the arguments form, truncated
	 * 
	 * returns null if format is wrong
	 * */
	public static Integer getIntegerTruncated(String args[])
	{
		if(args.length!=1)
			return null;
		
		try{
			double d=Double.parseDouble(args[0]);
			int i=(int)d;
			return i;
		}catch(NumberFormatException e){return null;}
		
	}
	
	/**
	 * returns the integer the arguments form 
	 * 
	 * returns null if format is wrong
	 * */
	public static Integer getInteger(String args[])
	{
		if(args.length!=1)
			return null;
		
		try{
			int i=Integer.parseInt(args[0]);
			return i;
		}catch(NumberFormatException e){return null;}
		
	}
	
	/**
	 * returns the double the arguments form 
	 * 
	 * returns null if format is wrong
	 * */
	public static Double getDouble(String args[])
	{
		if(args.length!=1)
			return null;
		
		try{
			double d=Double.parseDouble(args[0]);
			return d;
		}catch(NumberFormatException e){return null;}
		
	}
	
	
	/**
	 * returns the 3 doubles the arguments form as an array by order 
	 * 
	 * returns null if format is wrong
	 * */
	public static double[] getNumbers(String args[])
	{
		if(args.length!=3)
			return null;
		
		try{
			double d1=Double.parseDouble(args[0]);
			double d2=Double.parseDouble(args[1]);
			double d3=Double.parseDouble(args[2]);
			
			double[] dd={d1,d2,d3};
			
			return dd;
		}catch(NumberFormatException e){return null;}
		
	}
	
	
	
	
	/**
	 * returns the string the arguments form  (including spaces)
	 * 
	 * returns null if string is empty
	 * */
	public static String getString(String args[])
	{
		if(args.length==0)
			return null;
		
		String s=args[0];
		for(int i=1;i<args.length;i++)
			s+=" "+args[i];
	
		return s;
	}
	
	
	/**
	 * returns the color the arguments form  (0-1  "= 0-255")
	 * 
	 * returns null if format is wrong
	 * */
	public static Color getColor(String args[])
	{
		if(args.length!=3)
			return null;
		double r,g,b;
		try{
			r=Double.parseDouble(args[0]);
			g=Double.parseDouble(args[1]);
			b=Double.parseDouble(args[2]);
		}catch(NumberFormatException e){return null;}
	
		return new Color(r,g,b);
	}
	
	/**
	 * returns the point the arguments form 
	 * 
	 * returns null if format is wrong
	 * */
	public static Point getPoint(String args[])
	{
		if(args.length!=3)
			return null;
		double x,y,z;
		try{
			x=Double.parseDouble(args[0]);
			y=Double.parseDouble(args[1]);
			z=Double.parseDouble(args[2]);
		}catch(NumberFormatException e){return null;}
	
		return new Point(x,y,z);
	}
	
	/**
	 * returns the vector the arguments form 
	 * 
	 * returns null if format is wrong
	 * */
	public static NormalVector getVector(String args[])
	{
		if(args.length!=3)
			return null;
		double x,y,z;
		try{
			x=Double.parseDouble(args[0]);
			y=Double.parseDouble(args[1]);
			z=Double.parseDouble(args[2]);
		}catch(NumberFormatException e){return null;}
	
		return new NormalVector(x,y,z);
	}
	
	
}
