const roller = require("./roller.js");
// const cache = require("./cache.js");
const { CacheLimitationError } = require("../generated/api/storage.js");
const { NoPermissionsError } = require("../generated/api/messages.js");

const storage = require("./storage.js");
const saver = require("./saver.js");
const pools = require("./pools.js");
const counters = require("./counters.js");
const { SendMessage } = require("./commands.js");
const { FormatInputDicePool } = require("./messages.js");
const { UpdateSummaryMessage, RateLimitError } = require("./pinnedMessages.js");

// const CacheLimitationError = cache.CacheLimitationError;
const InvalidDiceFormatError = roller.InvalidDiceFormatError;
const KEY_SEPARATOR = storage.KEY_SEPARATOR;
const NAME_SEPARATOR = ",";
const NAME_ALL_WILCARD = "*";

module.exports = {
    RunSafe: runSafe,
    RollCortexPrime: rollCortexPrime,
    RollCortexPrimeKeep: rollCortexPrimeKeep,
    RollCortexPrimeAppend: rollCortexPrimeAppend,
    RerollCortexPrime: rerollCortexPrime,
    SaveDice: saveDice,
    ModifyDicePool: modifyDicePool,
    MoveDicePool: moveDicePool,
    StepDicePool: stepDicePool,
    RollDicePool: rollDicePool,
    ShowDicePools: showDicePools,
    GetSaveInput: getSaveInput,
    ShowCounters: showCounters,
    SetCounter: setCounter,
    RemoveCounter: removeCounter,
    ModifyCounter: modifyCounter,
    UpdatePinnedSummaryMessage: updatePinnedSummaryMessage,
    FormatChannelKey: formatChannelKey,
    SAVE_NAME_SEPARATOR: NAME_SEPARATOR,
    SAVE_NAME_ALL_WILCARD: NAME_ALL_WILCARD,
    POOL_NAME_SEPARATOR: NAME_SEPARATOR,
    POOL_NAME_ALL_WILCARD: NAME_ALL_WILCARD,
};

function formatChannelKey(commandInteraction) {
    let userId = commandInteraction.getUserId();
    let channelId = commandInteraction.getChannelId();
    return `ROLL:${userId},${channelId}`;
}

// Input: async method that returns list of messages
// Output: list of messages
async function runSafe(method) {
    try {
        return await method();
    } catch (error) {
        console.error("Error has occured. Printing.");
        printError(error);
        if (error instanceof CacheLimitationError) {
            // Note: this code is obsolete, as we're no longer using Autocode's storage.
            // This is probably the 1024 Key-Value limitation: https://docs.autocode.com/building-endpoints/keyvalue-storage/
            // If needed, other possible future solutions: https://docs.autocode.com/building-endpoints/using-databases/
            return [
                `${error.message}: Contact developer for other storage solutions, or use the code to create your own Discord bot`,
            ];
        } else if (error instanceof InvalidDiceFormatError) {
            return [
                `Bad dice format: should be either a <number> or <number>D<number>, separated by spaces. ${error.message}`,
            ];
        } else if (error instanceof RateLimitError) {
            return [
                `Failed updating pinned message. Recreate it or use one of the 'show' commands to update it. Reason: ${error.message}.`,
            ];
        } else if (error instanceof NoPermissionsError) {
            return [
                "Missing permissions for bot. Please allow all permissions from the link in the !invite prefix command.",
            ];
        }

        // Other errors:

        /*
        if (error.message.includes("Missing Permissions: code 50013")) {
          return [
            "Missing permissions for bot. Please allow all permissions from the link in the !invite prefix command.",
          ];
        }
        */

        return ["Unexpected error. Contact developer."];
    }
}

function printError(error) {
    console.error(error);
    // This doesn't work in this server, but it works in Chrome console.
    /*
    if (error.cause) {
      printError(error.cause);
    }
    */
}

// input: {event}
// iocContainer: IocContainer
// void
async function updatePinnedSummaryMessage(input, iocContainer) {
    var messages = await runSafe(() => UpdateSummaryMessage(input, iocContainer));
    if (messages?.length > 0) {
        await SendMessage(input.event, messages);
    }
}

/*
   inputOptions: {
    dice, keep, effect_mod, die_mod, hitch, mod, extra_effect_dice, merge, 
    save_opt_out, save_opt_in, pools,
    cache_key, save_input (output of getSaveInput)
    }
   iocContainer: IocContainer
   output: list of messages to show
*/
async function rollCortexPrime(inputOptions, iocContainer) {
    let dice = inputOptions.dice;
    let merge = inputOptions.merge;
    let extraEffectDice = inputOptions.extra_effect_dice;
    let keep = inputOptions.keep;
    let effectModifier = inputOptions.effect_mod;
    let dieModifier = inputOptions.die_mod;
    let hitch = inputOptions.hitch;
    let cacheKey = inputOptions.cache_key;
    let saveOptOut = inputOptions.save_opt_out;
    let saveOptIn = inputOptions.save_opt_in;
    let poolNames = inputOptions.pools;

    console.log("input dice: " + dice);
    console.log("input merge: " + merge);
    console.log("input extraEffectDice:" + extraEffectDice);

    let diceRoll = roller.ParseDiceInput(dice ?? "");
    let mergeDice = roller.ParseDiceInput(merge ?? "");
    let extraEffectDiceRoll = roller.ParseDiceInput(extraEffectDice ?? "");

    diceRoll = diceRoll.concat(extraEffectDiceRoll);
    let effectKeep = 1 + extraEffectDiceRoll.length;

    let messageContent = [];

    let userId = inputOptions?.save_input.userId ?? 0;
    let guildId = inputOptions?.save_input.guildId ?? 0;

    if (inputOptions.save_input) {
        let extraDicePoolNames = parseRollerPoolNameList(poolNames);
        let poolInput = {
            poolNames: extraDicePoolNames,
            userId: userId,
            channelId: guildId,
        };
        let dicePoolResult = await pools.GetDicePool(poolInput, iocContainer);
        if (!dicePoolResult.dicePool) {
            return dicePoolResult.messageContent;
        }
        messageContent.push(...dicePoolResult.messageContent);
        diceRoll = diceRoll.concat(dicePoolResult.dicePool);
    }
    let saveInput = undefined;

    // Getting saved results:
    if (inputOptions.save_input) {
        // Note: no need to cache this, as it affects only the dice roll which is cached.

        let saveOptOuts = parseSaveNameList(saveOptOut, []);
        let saveOptIns = parseSaveNameList(saveOptIn, saver.ALL_NAMES);

        saveInput = {
            userId: userId,
            guildId: guildId,
            saveOptOuts: saveOptOuts,
            saveOptIns: saveOptIns,
        };
    }

    let rollInput = {
        diceRoll: diceRoll,
        nonPoolDice: undefined,
        keep: keep,
        mergeDice: mergeDice,
        effectKeep: effectKeep,
        effectModifier: effectModifier,
        dieModifier: dieModifier,
        hitch: hitch,
        saveInput: saveInput,
        cacheKey: cacheKey,
    };
    let result = await rollCortexPrimeWithDiceSaveAndLastRollCache(
        rollInput,
        iocContainer,
    );
    messageContent = messageContent.concat(result);
    return messageContent;
}

/*
input: {
  saveInput: {
    userId,
    guildId - channelId,
    saveOptOuts - list or string, 
    saveOptIns - list or string, 
  }

    diceRoll - list of dice, 
    mergeDice - list of dice to merge,
    nonPoolDice, 
    keep, effectKeep, effectModifier, dieModifier,
    hitch,
    cacheKey,
  }
iocContainer: IocContainer
output: list of messages to show
*/
async function rollCortexPrimeWithDiceSaveAndLastRollCache(
    input,
    iocContainer,
) {
    let diceRoll = input.diceRoll;

    let messageContent = [];
    if (input.saveInput) {
        let savedResult = await saver.GetSaveForUser(input.saveInput, iocContainer);
        diceRoll.push(...savedResult.additionalPool);
        messageContent.push(...savedResult.messageContent);
    }

    let mergeDice = input.mergeDice;
    let nonPoolDice = input.nonPoolDice ?? [];
    if (mergeDice?.length > 0) {
        let mergeInput = {
            dice: diceRoll,
            mergeDice: mergeDice,
        };
        let mergeResult = roller.MergeDice(mergeInput, iocContainer);

        diceRoll = mergeResult.newDiceRoll;
        nonPoolDice.push(...mergeResult.nonPoolDice);
        messageContent = messageContent.concat(mergeResult.messageContent);
    }

    let rollInput = {
        diceRoll: diceRoll,
        nonPoolDice: nonPoolDice,
        keep: input.keep,
        effectKeep: input.effectKeep,
        effectModifier: input.effectModifier,
        dieModifier: input.dieModifier,
        hitch: input.hitch,
    };

    let result = roller.RollCortexPrime(rollInput, iocContainer);
    messageContent = messageContent.concat(result.messageContent);

    await cacheResult(input.cacheKey, result, iocContainer);

    return messageContent;
}

// inputOptions: {dice, cache_key}
// iocContainer: IocContainer
// output: list of messages to show
async function rollCortexPrimeAppend(inputOptions, iocContainer) {
    let cacheKey = inputOptions.cache_key;
    let dice = inputOptions.dice;

    let diceRoll = roller.ParseDiceInput(dice ?? "");

    var lastRoll = await iocContainer.cache.getCacheValue(cacheKey);
    let messageContent = ["No last roll found"];

    if (lastRoll) {
        let result = roller.RollCortexPrimeAppend(lastRoll, diceRoll, iocContainer);
        messageContent = result.messageContent;

        await cacheResult(cacheKey, result, iocContainer);
    }

    return messageContent;
}

// inputOptions: {dice,step_up, step_up_mod, cache_key}
// iocContainer: IocContainer
// output: list of messages to show
async function rerollCortexPrime(inputOptions, iocContainer) {
    let cacheKey = inputOptions.cache_key;
    let dice = inputOptions.dice ?? "";
    let stepUp = inputOptions.step_up ?? 0;
    let stepUpMod = inputOptions.step_up_mod ?? 1;

    var lastRoll = await iocContainer.cache.getCacheValue(cacheKey);
    let messageContent = ["No last roll found"];

    if (lastRoll) {
        // lastRoll: {messageContent , diceRoll, rolls, nonPoolDice, nonPoolRolls, scores, maxScores, options }
        console.log("input cachedRolls for reroll: " + JSON.stringify(lastRoll));
        let additionalDiceRoll = roller.ParseDiceInput(dice ?? "");
        let additionalNonPoolDice = lastRoll.nonPoolDice ?? [];

        let stepUpResult = roller.StepUp(
            lastRoll.diceRoll,
            stepUp,
            stepUpMod,
            iocContainer,
        );
        let newDiceRoll = stepUpResult.newDiceRoll;
        let nonPoolDice = stepUpResult.nonPoolDice;

        console.log("after step up: " + stepUpResult.newDiceRoll.join(","));
        let rollInput = {
            diceRoll: additionalDiceRoll.concat(newDiceRoll),
            nonPoolDice: additionalNonPoolDice.concat(nonPoolDice),
            keep: lastRoll.options.keep,
            effectKeep: lastRoll.options.effectKeep,
            effectModifier: lastRoll.options.effectModifier,
            dieModifier: lastRoll.options.dieModifier,
            hitch: lastRoll.options.hitch,
        };
        let result = roller.RollCortexPrime(rollInput, iocContainer);

        messageContent = stepUpResult.messageContent.concat(result.messageContent);

        await cacheResult(cacheKey, result, iocContainer);
    }

    return messageContent;
}

/*
   inputOptions: {
    name,
    action,
    dice, 
    guild_id, 
    user_id,
    wildcard_mode,
    }    
   iocContainer: IocContainer
   output: list of messages to show
*/
async function saveDice(inputOptions, iocContainer) {
    let name = inputOptions.name?.trim();
    let action = inputOptions.action;
    let dice = inputOptions.dice ?? "";
    let guildId = inputOptions.guild_id;
    let userId = inputOptions.user_id;
    let wildcardMode = inputOptions.wildcard_mode ?? 0;

    let messageContent = [];

    switch (action) {
        case saver.DELETE:
            let deleteInput = {
                saveNames: parseSaveNameList(name, []),
                guildId: guildId,
            };
            messageContent = messageContent.concat(
                await saver.DeleteSave(deleteInput, iocContainer),
            );
            break;
        case saver.SHOW:
            let showInput = {
                userId: userId,
                guildId: guildId,
            };
            let showResult = await saver.ShowSaves(showInput, iocContainer);
            messageContent = messageContent.concat(showResult.messageContent);
            break;
        case saver.SCENE:
        case saver.SINGLE:
            if (name.includes(NAME_SEPARATOR) || name.includes(NAME_ALL_WILCARD)) {
                let displayName = name.replace(
                    NAME_ALL_WILCARD,
                    `\\${NAME_ALL_WILCARD}`,
                );
                messageContent.push(
                    `Invalid name '${displayName}': must not contain '${NAME_SEPARATOR}' or '\\${NAME_ALL_WILCARD}'.`,
                );
                return messageContent;
            }

            let dicePool = roller.ParseDiceInput(dice);
            let saveInput = {
                guildId: guildId,
                saveName: name,
                action: inputOptions.action,
                dicePool: dicePool,
                userId: userId,
                wildcardMode: wildcardMode,
            };

            messageContent = messageContent.concat(
                await saver.SaveDice(saveInput, iocContainer),
            );

            break;
        default:
            console.error("Bad input for saveDice. Action is: " + action);
    }

    return messageContent;
}

function getSaveInput(commandInteraction) {
    let user_id = commandInteraction.getUserId();
    // let guild_id = event.guild_id; -- guild is the whole server - prefer this as channel. Keeping the old name for legacy reasons.
    let channel_id = commandInteraction.getChannelId();
    return {
        userId: user_id,
        guildId: channel_id,
    };
}

// Returns ALL_NAMES or a list of names
function parseSaveNameList(value, defaultValue) {
    if (!value) {
        return defaultValue;
    }
    return value == NAME_ALL_WILCARD
        ? saver.ALL_NAMES
        : value.split(NAME_SEPARATOR).map((s) => s.trim());
}

// Does not support wildcards.
function parseRollerPoolNameList(value) {
    if (!value) {
        return [];
    }
    return value.split(NAME_SEPARATOR).map((s) => s.trim());
}

// inputOptions: {keep, cache_key}
// iocContainer: IocContainer
// output: list of messages to show
async function rollCortexPrimeKeep(inputOptions, iocContainer) {
    let cacheKey = inputOptions.cache_key;
    let keep = inputOptions.keep;

    let messageContent = ["No last roll found"];

    var lastRoll = await iocContainer.cache.getCacheValue(cacheKey);
    if (lastRoll) {
        let result = roller.RollCortexPrimeKeep(lastRoll, keep, iocContainer);
        messageContent = result.messageContent;

        // Not caching the result on purpose, as "keep" is usually used as "let's see how it goes".
    }
    return messageContent;
}

/*
inputOptions: {
  name,
  names,
  action,
  dice, 
  channelId, 
  userId,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function modifyDicePool(inputOptions, iocContainer) {
    let messageContent = [];
    let errors = [];

    switch (inputOptions.action ?? 0) {
        case pools.POOL_ADD:
        case pools.POOL_REMOVE:
            let name = inputOptions.name?.trim();
            errors = validatePoolName(name);

            let dicePool = roller.ParseDiceInput(inputOptions.dice ?? "");
            if (dicePool.length <= 0) {
                errors.push(`No dice to modify: '${inputOptions.dice}'`);
            }

            if (errors.length > 0) {
                return errors;
            }
            let poolInput = {
                name: name,
                action: inputOptions.action,
                dice: dicePool,
                channelId: inputOptions.channelId,
                userId: inputOptions.userId,
            };

            let result = await pools.ModifyPool(poolInput, iocContainer);
            messageContent.push(...result.messageContent);
            break;

        case pools.POOL_CLEAR:
            let names = inputOptions.names;

            let poolNames = parsePoolNameList(names, []);

            if (poolNames != pools.ALL_NAMES) {
                poolNames.forEach((n) => errors.push(...validatePoolName(n)));
                if (poolNames.length <= 0) {
                    errors.push(`Name list cannot be empty. Original input: '${names}'`);
                }
            }
            if (errors.length > 0) {
                return errors;
            }
            let clearInput = {
                poolNames: poolNames,
                channelId: inputOptions.channelId,
                userId: inputOptions.userId,
            };

            let clearResult = await pools.ClearPools(clearInput, iocContainer);
            messageContent.push(...clearResult);

            break;
    }

    return messageContent;
}

/*
inputOptions: {
  sourceName,
  targetName,
  dice, 
  channelId, 
  userId,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function moveDicePool(inputOptions, iocContainer) {
    let messageContent = [];
    let errors = [];

    let channelId = inputOptions.channelId;
    let userId = inputOptions.userId;
    let sourceName = inputOptions.sourceName?.trim();
    let targetName = inputOptions.targetName?.trim();

    let dicePool = roller.ParseDiceInput(inputOptions.dice ?? "");
    if (dicePool.length <= 0) {
        errors.push(`No dice to modify: '${inputOptions.dice}'`);
    }

    errors.push(...validatePoolName(sourceName), ...validatePoolName(targetName));

    if (errors.length > 0) {
        return errors;
    }

    let removePoolInput = {
        name: sourceName,
        action: pools.POOL_REMOVE,
        dice: dicePool,
        channelId: channelId,
        userId: userId,
    };
    let addPoolInput = {
        name: targetName,
        action: pools.POOL_ADD,
        dice: dicePool,
        channelId: channelId,
        userId: userId,
    };

    let removeResult = await pools.ModifyPool(removePoolInput, iocContainer);
    messageContent.push(...removeResult.messageContent);
    if (!removeResult.success) {
        return messageContent;
    }

    let errorMessage = `Failed adding '${FormatInputDicePool(dicePool)}' to pool '${targetName}': Manually try again or add the dice back to '${sourceName}'.`;
    try {
        let addResult = await pools.ModifyPool(addPoolInput, iocContainer);
        messageContent.push(...addResult.messageContent);
        if (!addResult.success) {
            messageContent.push(errorMessage);
        }
    } catch (e) {
        console.error(errorMessage, e);
        messageContent.push(errorMessage + " Unexpected error.");
    }

    return messageContent;
}

// Returns ALL_NAMES or a list of names
function parsePoolNameList(value, defaultValue) {
    if (!value) {
        return defaultValue;
    }
    return value == NAME_ALL_WILCARD
        ? pools.ALL_NAMES
        : value
            .split(NAME_SEPARATOR)
            .map((s) => s.trim())
            .filter((x) => !!x);
}

/*
inputOptions: {
  name,
  mod - number enum,
  die  - number, 
  steps - number,
  channelId, 
  userId,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function stepDicePool(inputOptions, iocContainer) {
    let mod = inputOptions.mod;
    let die = inputOptions.die;
    let steps = inputOptions.steps;

    let messageContent = [];
    let errors = [];

    if (steps <= 0) {
        errors.push(`Number of steps must be positive. Got: ${steps}`);
    }

    if (die <= 0) {
        errors.push(`Die must be positive. Got: ${die}`);
    }

    let stepModifier = 0;
    switch (mod) {
        case pools.STEP_MOD_UP:
            stepModifier = 1;
            break;
        case pools.STEP_MOD_DOWN:
            stepModifier = -1;
            break;
        default:
            errors.push(`Unknown pool stepup mode: ${mod}`);
            break;
    }

    if (errors.length > 0) {
        return errors;
    }

    let poolInput = {
        stepUp: die,
        stepUpMod: steps * stepModifier,
        poolName: inputOptions.name?.trim(),
        channelId: inputOptions.channelId,
        userId: inputOptions.userId,
    };

    messageContent = messageContent.concat(
        await pools.StepUpPool(poolInput, iocContainer),
    );

    return messageContent;
}

/*
inputOptions: {
  names, 
  dice, keep,
  saveOptOut, saveOptIn, extraEffects,
  channelId, userId,
  cacheKey,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function rollDicePool(inputOptions, iocContainer) {
    let poolInput = {
        poolNames: parsePoolNameList(inputOptions.names),
        channelId: inputOptions.channelId,
        userId: inputOptions.userId,
    };
    let dicePoolResult = await pools.GetDicePool(poolInput, iocContainer);

    if (!dicePoolResult.dicePool) {
        return dicePoolResult.messageContent;
    }

    let dicePool = dicePoolResult.dicePool;
    let messageContent = dicePoolResult.messageContent;

    let saveOptOuts = parseSaveNameList(inputOptions.saveOptOut, []);
    let saveOptIns = parseSaveNameList(inputOptions.saveOptIn, []);
    let extraDice = roller.ParseDiceInput(inputOptions.dice ?? "");

    let saveInput = {
        userId: inputOptions.userId ?? 0,
        guildId: inputOptions.channelId ?? 0,
        saveOptOuts: saveOptOuts,
        saveOptIns: saveOptIns,
    };

    let rollInput = {
        diceRoll: dicePool.concat(extraDice),
        keep: inputOptions.keep,
        effectKeep: Math.max(1, 1 + (inputOptions.extraEffects ?? 0)),
        nonPoolDice: [],
        effectModifier: 0,
        dieModifier: 0,
        hitch: 1,
        saveInput: saveInput,
        cacheKey: inputOptions.cacheKey,
    };
    let rollResult = await rollCortexPrimeWithDiceSaveAndLastRollCache(
        rollInput,
        iocContainer,
    );
    messageContent.push(...rollResult);

    return messageContent;
}

/*
inputOptions: {
  channelId, 
  userId,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function showDicePools(inputOptions, iocContainer) {
    let showPoolsResult = await pools.ShowPools(
        {
            channelId: inputOptions.channelId,
        },
        iocContainer,
    );
    return showPoolsResult.messageContent;
}

// Empty list if valid, other list of error messages.
function validatePoolName(name) {
    if (
        name.includes(NAME_SEPARATOR) ||
        name.includes(NAME_ALL_WILCARD) ||
        name.includes(KEY_SEPARATOR)
    ) {
        let displayName = name.replace(NAME_ALL_WILCARD, `\\${NAME_ALL_WILCARD}`);
        return [
            `Invalid name '${displayName}': must not contain '${NAME_SEPARATOR}', '\\${NAME_ALL_WILCARD}' or '${KEY_SEPARATOR}'.`,
        ];
    }
    return [];
}

/*
inputOptions: {
  who,
  counterName,
  value,  
  channelId, 
  userId,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function setCounter(inputOptions, iocContainer) {
    let who = inputOptions.who;
    let errors = validateCounterWho(who);
    if (errors.length > 0) {
        return errors;
    }

    let messageContent = [];
    let counterInput = {
        who: inputOptions.who,
        counterName: inputOptions.counterName?.trim(),
        value: inputOptions.value,
        channelId: inputOptions.channelId,
        userId: inputOptions.userId,
    };

    let result = await counters.SetCounter(counterInput, iocContainer);
    messageContent.push(...result);

    return messageContent;
}

/*
inputOptions: {
  who,
  counterName,
  amount,  
  channelId, 
  userId,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function modifyCounter(inputOptions, iocContainer) {
    let who = inputOptions.who;
    let errors = validateCounterWho(who);
    if (errors.length > 0) {
        return errors;
    }

    let messageContent = [];
    let counterInput = {
        who: inputOptions.who,
        counterName: inputOptions.counterName?.trim(),
        amount: inputOptions.amount,
        channelId: inputOptions.channelId,
        userId: inputOptions.userId,
    };

    let result = await counters.ModifyCounter(counterInput, iocContainer);
    messageContent.push(...result);

    return messageContent;
}

/*
inputOptions: {
  who,
  counterName,
  channelId, 
  userId,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function removeCounter(inputOptions, iocContainer) {
    let who = inputOptions.who;
    let errors = validateCounterWho(who);
    if (errors.length > 0) {
        return errors;
    }

    let messageContent = [];
    let counterInput = {
        who: inputOptions.who,
        counterName: inputOptions.counterName?.trim(),
        channelId: inputOptions.channelId,
        userId: inputOptions.userId,
    };

    let result = await counters.RemoveCounter(counterInput, iocContainer);
    messageContent.push(...result);

    return messageContent;
}

/*
inputOptions: {
  channelId, 
  userId,
}    
iocContainer: IocContainer
output: list of messages to show
*/
async function showCounters(inputOptions, iocContainer) {
    let who = inputOptions.who;

    let messageContent = [];
    let counterInput = {
        channelId: inputOptions.channelId,
        userId: inputOptions.userId,
    };

    let result = await counters.ShowCounters(counterInput, iocContainer);
    messageContent.push(...result.messageContent);

    return messageContent;
}

function validateCounterWho(name) {
    if (!name) {
        return [
            `Name is empty for this user. Please set a value so the default isn't chosen.`,
        ];
    }
    return [];
}

// Input result is RollCortexPrime... result.
async function cacheResult(key, result, iocContainer) {
    return await iocContainer.cache.cacheValue(
        key,
        {
            rolls: result.rolls,
            diceRoll: result.diceRoll,
            nonPoolDice: result.nonPoolDice,
            nonPoolRolls: result.nonPoolRolls,
            options: result.options,
        },
        iocContainer.options.cacheTimeSecs,
    );
}
