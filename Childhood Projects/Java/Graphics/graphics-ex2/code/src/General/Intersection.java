package General;

import Objects.Surface;

/**
 * 
 * The class represents an intersection between a ray and an object
 *
 */
public class Intersection {

	private double distance; //for closest intersection only
	private Surface object;
	private Point hit;
	private Ray ray;
	private NormalVector normal;
	
	
	public static final Intersection MISS=null; //means that ray does not intersect an object
	
	
	public Intersection(double d,Surface o,Point h,Ray r,NormalVector n)
	{
		distance=d;
		object=o;
		hit=h;
		ray=r;
		normal=n;
	}

	public double getDistance() {
		return distance;
	}

	public Surface getObject() {
		return object;
	}

	public Ray getRay() {
		return ray;
	}

	public Point getHit() {
		return hit;
	}
	
	public NormalVector getNormal() {
		return normal;
	}

	
	public Color getColor(Scene scene)
	{
		return object.getColor(scene,this);
	}
	
	
	public void setObject(Surface s)
	{
		object=s;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
	}

	
	
	public String toString()
	{
		return "intersection: "+hit+","+distance+","+object;
	}
	
}
