
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;


//need to scroll always down.JViewport.JScrollPane

public class Attack extends JScrollPane{//extends JPanel{
	
	
	/**
	 *The list to be displayed
	 */
	
	private JList data; //feedback
	
	/**
	 *The text the list displays
	 */
	
	private Vector text;
	
	
	//private boolean wait;
	//private JTextField txt;
	
	/**
	*Constructs a new Attack
	*/
	public Attack() //a more comfortable one?
	{
		text=new Vector();
		data=new JList(text);
		
		//wait=false;
		//txt=new JTextField();
		//data.setVisibleRowCount(15);
		
		data.setToolTipText("Battle feedback");
		//JScrollPane sc=new JScrollPane(data);
		//add(sc);
		
		data.setSelectionBackground(Color.white);
		
		setViewportView(data);
		//setViewportView(txt);
		
		//setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		
		//setPreferredSize(new Dimension(250,0)); //Main.dimy));
		setPreferredSize(new Dimension(400,0));
		
		
	}
	
	/**
	*Constructs a new Attack with a different tool-tip
	*@param feed : the tool-tip
	*/
	public Attack(String feed)
	{
		text=new Vector();
		data=new JList(text);
		
//		wait=false;
		
		data.setToolTipText(feed);
		
		data.setSelectionBackground(Color.white);
		
		setViewportView(data);
		
		setPreferredSize(new Dimension(400,0));
	}
	
	
	public String getTip()
	{
	return data.getToolTipText();	
	}
	
	//public Dimension()
	
	
	/*public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	}
	*/
	
	public void waiting()
	{
	//for now, leave it as it is. 
	/*
	if(text.size()>0)
	{
		wait=true;	
		//System.out.println("at - waiting");
	}
	else
	//System.out.println("not wait. "+text.size());
	*/
	}
	
	/**
	*Adds a text to the list
	*@param s : the text to add
	*/
	public void addText(String s)
	{
		setVisible(false);
		
		
//		try{
//		Thread.sleep(Char.delay);
//		}catch(InterruptedException e){//System.out.println(e);}
		
//		if(wait)
//		{
//		text.add(text.size()-1,s);
		//wait=false;
//		}
	//	else
	//	{
		text.add(s);
		//}
		
		data.setListData(text);
		
		//data.setSelectedIndex(text.size()-1);
		////System.out.println("nim: "+(text.size()-1)+"");
		data.ensureIndexIsVisible(text.size()-1);
		
				
		setVisible(true);
	}
	
	/**
	*Converts the list to one String
	*/
	public String convert()
	{
		String s="";
		
		for(int i=0;i<text.size();i++)
		{
			String t=(String)(text.elementAt(i));
			s=s+t+"_"; //\n = bug
		}		
	
		return s;
	}
	
	/**
	*Adds the text from a converted string to this list
	*@param feed : the tool-tip
	*/
	public void convert(String s)
	{
	//setVisible(false);
	
		while(!s.equals(""))
		{
		int in=s.indexOf("_");
		String t=s.substring(0,in);
		s=s.substring(in+1);
		
//		if(wait)
		//{
		//text.add(text.size()-1,t);
//		wait=false;
		//}
//		else
		//{
		text.add(t);
		//}
		
		data.setListData(text);
		
		data.ensureIndexIsVisible(text.size()-1);
		}
	
	//setVisible(true); good.
	}
	
	
	/**
	*Clears the list
	*/
	public void clear()
	{
		text=new Vector();
		data=new JList(text);
	}
	
	
	public void addIn(Vector fighters,Vector nums)
	{
	addText("Initiation:");
	
	for(int i=0;i<fighters.size();i++)	
		{
			int a=Integer.parseInt((String)(nums.elementAt(i)));
			String s=fighters.elementAt(i).toString();
			addText(s+ "  ["+a+"]");
		}
	
	addText(" ");
				
	}
	
}