import type { BasicDataStorage } from "./base_storage";
import type { DataStorage } from "../api/storage";
import type { StorageInitParams } from "./base_storage";
import { BaseDatabase } from "./base_storage";
import { InMemoryDatabase } from "./in_memory_storage";

enum StorageType {
  replit, // Uses Replit's database.
  discord, // Uses Discord to store data.
  memory, // Uses an internal map object.
}
export function getDefaultStorage(): DataStorage {
  return new BaseDatabase(
    `Default(${StorageType[StorageType.memory]})`,
    new InMemoryDatabase(),
  );
}

export class StorageChooser {
  type: StorageType;
  constructor(type: string) {
    this.type = getStorageType(type);
  }

  async getStorage(params: StorageInitParams): Promise<DataStorage> {
    const underlyingStorage = await this.getUnderlyingStorage(params);
    return new BaseDatabase(`${StorageType[this.type]}`, underlyingStorage);
  }

  async getUnderlyingStorage(
    params: StorageInitParams,
  ): Promise<BasicDataStorage> {
    switch (this.type) {
      case StorageType.replit:
        const replitModule = await import("./replit_storage");
        return new replitModule.ReplitDatabase();
      case StorageType.discord:
        const discordModule = await import("./discord_storage");
        return await discordModule.createDiscordDatabase(params);
      case StorageType.memory:
        return new InMemoryDatabase();
      default:
        throw new Error(`Storage type not implemented: ${this.type}.`);
    }
  }
}

function getStorageType(type: string): StorageType {
  const storageType: StorageType = StorageType[type];

  if (storageType !== undefined) {
    return storageType;
  }

  const allValues = Object.keys(StorageType).filter((item) =>
    isNaN(Number(item)),
  );

  throw new Error(
    `Unknown storage type: ${type}. Should be one of: ${allValues}`,
  );
}
