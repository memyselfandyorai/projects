﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderSyncronizer.Logic
{
    interface IWatcherHandler
    {
        WatcherChangeTypes SupportedType { get; }

        IFileAction HandleChange(string sourceFolder, string targetFolder, FileSystemEventArgs eventArgs);
    }
}
