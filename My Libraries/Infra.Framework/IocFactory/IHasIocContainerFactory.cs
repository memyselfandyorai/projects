﻿namespace Infra.Framework.IocFactory
{
    public interface IHasIocContainerFactory
    {
        /// <summary>
        /// Creates an instance of type T with the given arguments.
        /// T must have a default ctor and one with the arguments (with IocContainer as first argument).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args"></param>
        /// <returns></returns>
        T CreateInstance<T>(params object[] args) where T : IHasIocContainer;

        /// <summary>
        /// Creates an instance of type T with the given arguments.
        /// T must have a default ctor and one with the arguments (with IocContainer as first argument).
        /// Note T should be IHasIocContainer on runtime, even if not on compile time.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args"></param>
        /// <returns></returns>
        T CreateInstanceNotSafe<T>(params object[] args);
    }
}
