const controller = require("./controller.js");
const { ReplyToCommand, ReceivedCommand } = require("./commands.js");

module.exports = {
  RunSaver: runSaver,
};

/*
event - commandInteraction,
actionResolver - func(input of parseInput) -> saver action.
*/
async function runSaver(commandInteraction, actionResolver) {
  await ReceivedCommand(commandInteraction);

  let inputOptions = parseInput(commandInteraction);
  console.log("parse input: " + JSON.stringify(inputOptions));

  inputOptions.action = actionResolver(inputOptions);

  let messageContent = await controller.RunSafe(() =>
    controller.SaveDice(inputOptions, commandInteraction.getIocContainer()),
  );
  console.log("Saver result: " + messageContent);
  await ReplyToCommand(commandInteraction, messageContent);

  let updateInput = {
    event: commandInteraction,
  };

  await controller.UpdatePinnedSummaryMessage(
    updateInput,
    commandInteraction.getIocContainer(),
  );
}

// Returns input: {name, lifespan, optional wildcard_mode, dice, guild_id, user_id}
function parseInput(commandInteraction) {
  var inputMap = commandInteraction.getInputOptionsMap();

  return {
    name: inputMap.get("name") ?? "",
    dice: inputMap.get("dice") ?? "",
    lifespan: inputMap.get("lifespan") ?? "",
    wildcard_mode: inputMap.get("wildcard-mode") ?? 0,
    guild_id: commandInteraction.getChannelId() ?? "", // Note: using channel ID instead of guild (which is the whole server).
    user_id: commandInteraction.getUserId() ?? 0,
  };
}
