﻿namespace Infra.Framework.Random
{
    public interface IRandomProvider
    {
        IRandom Random { get; set; }
    }
}
