const controller = require("../../../../common/controller.js");
const {
  PinNewSummaryMessage,
} = require("../../../../common/pinnedMessages.js");
const {
  ReplyToCommand,
  ReceivedCommand,
} = require("../../../../common/commands.js");

module.exports = {
  async execute(commandInteraction) {
    await handleCommand(commandInteraction);
  },
};

async function handleCommand(commandInteraction) {
  await ReceivedCommand(commandInteraction);

  let input = {
    event: commandInteraction,
  };
  let messageContent = await controller.RunSafe(() =>
    PinNewSummaryMessage(input, commandInteraction.getIocContainer()),
  );

  // There's an unclear bug with "Interaction has already been acknowledged.: code 40060: (discord/interactions@1.0.0/responses/create)",
  // internet suggests a missing "await" or another bot stealing interactions. I couldn't figure it out, but the pinning works.

  await ReplyToCommand(commandInteraction, messageContent);
}

/*
const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

await lib.discord.commands['@0.0.0'].create({
  "name": "pin-summary",
  "description": "Pins a summary message to the channel that updates dynamically.",
  "options": []
});
*/
