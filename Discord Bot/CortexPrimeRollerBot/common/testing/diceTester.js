const roller = require('../roller.js');
const {TestCase}= require('./common.js');


module.exports = {
  TestCases: createTestCases(),
};


function createTestCases() {
  return [
    new TestCase({
      name: 'testRollerParseDiceInput_dice_diceListGood',
      functionToRun: testRollerParseDiceInput ,
      inputArgs: {
        dice: '4 6 2D6 3D8'
      },
      expectedResult: [4, 6, 6, 6, 8, 8, 8],
    }),
    
  ]

}

/*
input: {dice - string}
*/
async function testRollerParseDiceInput (input) {
  return await roller.ParseDiceInput(input.dice)
}
