
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;

//C:\DND3rd>C:\j2sdk1.4.2_04\bin\javadoc -private c:\DND3rd\Server\*.java -d c:\dn
//d3rd\API

//sendsleep+setopaque?

//other color for the O (in map).

//skills, spells, run


//paladin- can't attack NPCs.!!!

//can't talk if can't see player (if all npc, can't. need "talk" to NPC also)

//limit: healing? (once per turn. only in battle. - because healing also diseases.)
//(after healing disease, think of it.)

/*
//sleeping needs to be hazardous.
 
boolean stable?
{
what of healers? if there's one in town, has chance to survive.
if no healer and no character - dead.
healer? normal char? not merchant... like a civilian- healer.
and some good guys to help a character in need. [can't attack them maybe...?]
"attacking them will turn the whole town against you and you won't have any chance of survivol"
something like that.
}

*/


//merchant: like the craft thing (combo in new window) - to buy


//remove the prints.

//labels clear, shakuf, and behind a picture or the current place.

//spot- opossite sex +1? (any bonuses at all?)

//spells in page- to like in meals.

public class Map extends JPanel implements ActionListener{
	
	/**
	 *All the places in the map
	 */
	private Place map[][];
	/**
	 *How many places
	 */
	static final int dim=15; 
	/**
	 *The player
	 */
	private Character player;
	/**
	 *Battle feedback and conversation
	 */
	private Attack battle;
	/**
	 *List of characters seen
	 */
	private JList list;
	/**
	 *Buttons: go north, south, west and east, attack character, use skill, sleep, talk, run from battle and use spell
	 */
	private JButton north,south,east,west,attack,skills,sleep,talk,run,spells;
	/**
	 *The panel in the center
	 */
	private JPanel center;
	/**
	 *The message label from the main menu
	 */
	private JLabel mes; //make label at main "public"? static?
	/**
	 *The main menu
	 */
	private Main main;
	/**
	 *Fighters and their initiation scores (not in use)
	 */
	private Vector fighters,nums;
	/**
	 *Order of battle (not in use)
	 */
	private int order;
	/**
	 *The client class
	 */
	private Client client; //is this needed?
	/*public Map(JLabel ms,Main m)
	{
	super();
			
	mes=ms;
	main=m;
	
	order=0;
	//fighters=new Vector();
	
	center=new JPanel();
	center.setLayout(new GridLayout(0,1));
	
	attack=new JButton("Attack");
	attack.setToolTipText("Attack the selected character");
	attack.addActionListener(this);
	
	map=new Place[dim][dim];
		
		for(int i=0;i<dim;i++)
		for(int j=0;j<dim;j++)
		{
			int r=Dice.roll(100);
			
			if(r>=1&&r<=20)
			map[i][j]=new Place("Town");
			if(r>=21&&r<=45)
			map[i][j]=new Place("Village");
			if(r>=46&&r<=55)
			map[i][j]=new Place("Cave");
			if(r>=56&&r<=70)
			map[i][j]=new Place("River"); //or whatever
			if(r>=71&&r<=85)
			map[i][j]=new Place("Mountain");
			if(r>=86&&r<=100)
			map[i][j]=new Place("Forest");
		
		
			//more? desert?-yellow
		}
		
		
	//////System.out.println(map[5][5].getName()+"");
	//////System.out.println(map[5][5].getRGB()+"");
	//////System.out.println(map[5][5].getItems().elementAt(0));
	//setBackground(Color.red);
	
	north=new JButton("North");
	south=new JButton("South");
	east=new JButton("East");
	west=new JButton("West");
	north.addActionListener(this);
	east.addActionListener(this);
	west.addActionListener(this);
	south.addActionListener(this);
	
	south.setPreferredSize(new Dimension(0,50));
	north.setPreferredSize(new Dimension(0,50));
	
	}
	*/

	public Map(JLabel ms,Main m,Client cli)
	{
	super();
			
	mes=ms;
	main=m;
	client=cli;
	battle=new Attack();
	
	order=0;
	fighters=new Vector();
	
	center=new JPanel();
	center.setLayout(new GridLayout(0,1));
	
	attack=new JButton("Attack");
	attack.setToolTipText("Attack the selected character");
	attack.addActionListener(this);
	attack.setBackground(Color.orange);
	
	skills=new JButton("Use Skill");
	skills.setToolTipText("Uses the skill selected in your character page");
	skills.addActionListener(this);
	skills.setBackground(Color.red.darker()); //?
	
	spells=new JButton("Use Spell\\Ability");
	spells.setToolTipText("Uses the spell\\ability selected in your character page");
	spells.addActionListener(this);
	spells.setBackground(Color.magenta); //maybe this with spells?
	
	sleep=new JButton("Sleep");
	sleep.setToolTipText("Sleeps - refills energy");
	sleep.addActionListener(this);
	sleep.setBackground(Color.cyan);
	
	talk=new JButton("Talk");
	talk.setToolTipText("Send message to a player, or start an interaction with a character");
	talk.addActionListener(this);
	talk.setBackground(Color.green);
	
	run=new JButton("Run");
	run.setToolTipText("Runs from a fight");
	run.addActionListener(this);
	run.setBackground(Color.yellow);
	
	map=new Place[dim][dim];
		
	north=new JButton("North");
	south=new JButton("South");
	east=new JButton("East");
	west=new JButton("West");
	north.addActionListener(this);
	east.addActionListener(this);
	west.addActionListener(this);
	south.addActionListener(this);
	
	south.setPreferredSize(new Dimension(0,50));
	north.setPreferredSize(new Dimension(0,50));
	
	}


	public Map()
	{
	super();
			
	//mes=ms;
	//main=m;
	
	order=0;
		
	center=new JPanel();
	center.setLayout(new GridLayout(0,1));
	
	attack=new JButton("Attack");
	attack.setToolTipText("Attack the selected character");
	attack.addActionListener(this);
	
	map=new Place[dim][dim];
		
		for(int i=0;i<dim;i++)
		for(int j=0;j<dim;j++)
		{
			int r=Dice.roll(100);
			
			//r=15;//del
			
			//r=30;
			
			if(r>=1&&r<=20)
			map[i][j]=new Place("Town");
			if(r>=21&&r<=45)
			map[i][j]=new Place("Village");
			if(r>=46&&r<=55)
			map[i][j]=new Place("Cave");
			if(r>=56&&r<=70)
			map[i][j]=new Place("River"); //or whatever
			if(r>=71&&r<=85)
			map[i][j]=new Place("Mountain");
			if(r>=86&&r<=100)
			map[i][j]=new Place("Forest");
		
		}
		
	north=new JButton("North");
	south=new JButton("South");
	east=new JButton("East");
	west=new JButton("West");
	north.addActionListener(this);
	east.addActionListener(this);
	west.addActionListener(this);
	south.addActionListener(this);
	
	south.setPreferredSize(new Dimension(0,50));
	north.setPreferredSize(new Dimension(0,50));
	
	}
	

	public void setMM(JLabel ms,Main m)
	{
	mes=ms;
	main=m;
	}
/*
	public Map(Character player,JLabel ms)
	{
		super();
		
		this.player=player;
					
		
		
			
/*	JPanel down=new JPanel(),up=getMap();
	
	down.setLayout(new BorderLayout());
	
	down.add(BorderLayout.NORTH,north);
	down.add(BorderLayout.EAST,east);
	down.add(BorderLayout.WEST,west);
	down.add(BorderLayout.SOUTH,south);
	
	setLayout(new GridLayout(2,1));
	add(up);
	add(down);

*/
//	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
			
	}
	
	
	public Dimension getPreferredSize()
	{
		//return new Dimension(300,200);
		return new Dimension(400,300);
	}
	
	
	public void setPlace(int i,int j,Place p)
	{
			
	if(i>=0&&j>=0&&i<dim&&j<dim)
	{
		//////System.out.println("playerSP: "+player);
		
		//////System.out.println("\nn: "+main.getPlayer().getSeen()+"\n"); 
		
		if(player!=null)
		if(i==player.getY()&&j==player.getX())
		{
		//chs: chars from current place
		//pchs: chars from the place to be
		Vector chs=map[i][j].getChars(),pchs=p.getChars();
		int k=chs.indexOf(player);
		//////System.out.println("player-chars: "+map[i][j].getChars());
		//////System.out.println("player-chars2: "+p.getChars());
		
		
		
		//needed for the references and changes:
		
		//////System.out.println("::: "+chs.size()+","+pchs.size());
		
		//only if it was removed from under him... :another function for that.
		Character exPlayer=player;
		
		//////System.out.println("pl-k: "+k);
		//is "move" ok? may not delete him from the previous place.
		//////System.out.println("chs (previous): "+chs);
		//////System.out.println("pchs (current): "+pchs);
		
		if(!player.isDead())
		player=(Character)(pchs.elementAt(k));
		else
		{
		//////System.out.println("dead: 306"); 
		dead(); //dead
		}
		//player=(Character)(pchs.elementAt(k-1));
		
		if(exPlayer.getXP()!=player.getXP())
		{
		int xp=player.getXP()-exPlayer.getXP();
		battle.addText("You got "+xp+" XP.");   //is it possible to lose xp? no. i think.
				if(exPlayer.lvl()!=player.lvl())
				battle.addText("You are now a level "+player.lvl()+" "+player.getClas());
		battle.waiting();
		}
		
		
		//////System.out.println("\np: "+main.getPlayer().getSeen()+"\n"); 
		
		main.setCh(player); //problem here!!!!
		
		//////System.out.println("\nm: "+main.getPlayer().getSeen()+"\n"); 
		
		}
		
		p.updateSeen();	
		
		map[i][j]=p;
		
		if(player!=null)
		if(!player.attacked()&&!player.isDead()) //+ !dying?
		waitFC(false);
		
		//////System.out.println("\nl: "+main.getPlayer().getSeen()+"\n"); 
				
		//////System.out.println("fighters (pc): "+p.getFighters());
		//////System.out.println("player's (pc): "+player.getPlace().getFighters());
		
		//change: function for the another seen
		//wrong! bug! damn! shit! well... it was OK in "spot"...	
		
		
		// needed.
		/*
		if(player!=null)
		{
			if(i==player.getY()&&j==player.getX())
			{
			
			Char toSpot=(Char)(p.getChars().elementAt(p.getChars().size()-1));
				
				
				//it means that he'll always spot him!
				
				if(player.getSeen().indexOf(toSpot)==-1&&player!=toSpot) //change in spot- can't spot again.			
				{
				Vector ots=new Vector();
				ots.add(toSpot);//one to see   (the last one from place)
				player.spot(ots);
				//////System.out.println("send place....");
				main.sendUPlace(); //this cause the problem
				}
			}
		}
		else
		////System.out.println("player empty, line 284 map. :"+player);
		*/
		
		
		//////System.out.println("ch: "+map[i][j].getChars());
		
		
		//////System.out.println("\no: "+main.getPlayer().getSeen()+"\n"); 
		
	}
	}
	
	
	
	public void setUPlace(int i,int j,Place p)
	{
			
	if(i>=0&&j>=0&&i<dim&&j<dim)
	{
		
		if(player!=null)
		if(i==player.getY()&&j==player.getX())
		{
		//chs: chars from current place
		//pchs: chars from the place to be
		Vector chs=map[i][j].getChars(),pchs=p.getChars();
		int k=chs.indexOf(player);
		
		Character exPlayer=player;
		
		if(!player.isDead())
		player=(Character)(pchs.elementAt(k));
		else
		dead(); //dead
				
		if(exPlayer.getXP()!=player.getXP())
		{
		int xp=player.getXP()-exPlayer.getXP();
		battle.addText("You got "+xp+" XP.");
				if(exPlayer.lvl()!=player.lvl())
				battle.addText("You are now a level "+player.lvl()+" "+player.getClas());
		battle.waiting();	
		}
		
		
		main.setCh(player);
		}
		
		p.updateSeen();	
		
		map[i][j]=p;
			
	}
	}
	
	
	
	
	public void setPlace(int i,int j,Place p,int add)
	{
	//add - index of the one who is removed		
	if(i>=0&&j>=0&&i<dim&&j<dim)
	{
		//////System.out.println("playerSP: "+player);
		if(player!=null)
		if(i==player.getY()&&j==player.getX())
		{
		Vector chs=map[i][j].getChars(),pchs=p.getChars();
		int k=chs.indexOf(player); //k-previous index of player
		
		//////System.out.println("p: "+p.write());
		//////System.out.println("add: "+add+" , k: "+k+"\nchs: "+chs+"\npchs: "+pchs);
		
		Character exPlayer=player;
		
		if(!player.isDead())
		{
		//////System.out.println("add: "+add+" , k: "+k+"\nchs: "+chs+"\npchs: "+pchs);
		if(add>k) //index bigger than player's
		player=(Character)(pchs.elementAt(k));
		if(add<k) //index smaller than player's (need change)
		player=(Character)(pchs.elementAt(k-1));
		if(add==k) //player is dead
		dead(); //dead
		}
		else
		dead(); //dead
		
		if(exPlayer.getXP()!=player.getXP())
		{
		int xp=player.getXP()-exPlayer.getXP();
		battle.addText("You got "+xp+" XP.");
				if(exPlayer.lvl()!=player.lvl())
				battle.addText("You are now a level "+player.lvl()+" "+player.getClas());
		battle.waiting();
		}
		
		
		main.setCh(player);
		}
		
		p.updateSeen();
		map[i][j]=p;
		
		//////System.out.println("fighters (pcc): "+p.getFighters());
		//////System.out.println("player's (pcc): "+player.getPlace().getFighters());
		//////System.out.println("ch: "+map[i][j].getChars());
	}
	}
	
		
		
	
	public void actionPerformed(ActionEvent e)
	{
		main.setMes("");
		//Place pl2=getPlace(player.getY(),player.getX());
		//////System.out.println("if: "+pl2.getChars().indexOf(player));
		
		//////System.out.println("place: "+player.getPlace().write());
		
		////System.out.println("bat: "+battle.isVisible());
		
		
		JButton button=(JButton)(e.getSource());
		
		int x=0,y=0;

		if(button==north)
		{
			if(player.getY()-1<0)
			mes.setText("You can't go any norther");
			else
			y=-1;
			//Place.move(player,map,0,-1);
		}
		
		if(button==south)
		{
			if(player.getY()+1>=dim)
			mes.setText("You can't go any souther");
			else
			y=1;
			//Place.move(player,map,0,+1);
		}
		
		if(button==east)
		{
			if(player.getX()+1>=dim)
			mes.setText("You can't go any easter");
			else
			x=1;
			//Place.move(player,map,+1,0);
		}
		
		if(button==west)
		{
			if(player.getX()-1<0)
			mes.setText("You can't go any wester");
			else
			x=-1;
			//Place.move(player,map,-1,0);
		}
		
		
		boolean b=false;
		
		
		
					
		if(!(x==0&&y==0))    //change place.
		{
		
		if(player.getMoved()<player.getSpeed())
		{
			if(canPass())
			{			
		//////System.out.println("battle is to be false.");
		main.getPage().setSearched(false);
		
		
		//int hun=player.addMoved(); //things that change: moved, hunger,
		
		//////System.out.println("hunger: "+player.getHunger());
		
		
		//damn. it's random. can't here and in server also. [more funcitons... HUNGER*...]
		
			
		//if(hun>0) //dying, also.
		//JOptionPane.showMessageDialog(main.getFrame(),"You are hungry.\nYou were damaged by "+hun+" points.","You need meals",JOptionPane.INFORMATION_MESSAGE);	
						
		if(battle!=null)
		battle.setVisible(false);
		
		//if(!player.isDead())
		main.sendMove(x,y);
		//else
		//{
		//main.sendCPlace();
		//dead();
		//}
		
		//if(!player.isDead())
		Place.movens(player,map,x,y); //so he'll know who he is
		
		//////System.out.println("need to be before update");
		
		//if(!player.isDead())
		b=true;

		
			}
			
		}
		else
		main.setMes("You can't move until you sleep."); //"rest"?
		
		}  //end of move (x!=0,y!=0)
		//setVisible(false);
		
		//setVisible(true);
		
		
		//too much update.
		//if(b)
		//update(player);
		
		
		
		
		
		//Component[] com=center.getComponents();
		//JScrollPane jsc=(JScrollPane)(com[0]);
		//Component[] com2=(jsc).getComponents();
		
				
		//////System.out.println("c: "+c);
		
		//////System.out.println("obj: "+com2[jsc.getComponentCount()-1]);
		
		if(button==skills)
		{
		int ss=main.getPage().getIS();		
		
		if(ss<0)
		ss=0;
		
		Skills sk=player.getSkills();
		String skill=sk.getSkill(ss);
		
			if(skill.equals("Search")&&!player.attacked())
			{
			
			if(!main.getPage().isSearched())
			{
				
			int op=JOptionPane.showConfirmDialog(main.getFrame(),"Are you sure you want to search the place for items and gold? (you can only search once)","Search?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
				
				if(op==0)
				{
				//////System.out.println(player.getPlace().getItems());
				
				String s="";
				
				
				main.getPage().setSearched(true);
							
				if(sk.check("Search",null))			
				{
				s=s+"You searched, and ";
				//gp
				int roll=Dice.roll(1,100,50);	
				Place p=player.getPlace();
				double gp=p.gp();
				
					if(roll>100)
					roll=100;
				String gg=""+(double)(roll*gp/100);
				double g=Double.parseDouble(gg);
				if(gg.indexOf(".")!=-1&&gg.indexOf(".")+2<gg.length())
				g=Double.parseDouble(gg.substring(0,gg.indexOf(".")+2));
				
				player.addMoney(g);
				p.addMoney(-g);	
				
				s=s+"you found "+g+" GP.";
				
				//items
				roll=Dice.roll(1,100,25);
					if(roll>100)
					roll=100;
				
				Vector it=new Vector(p.getItems());	
				int n=(int)(it.size()*roll/100);
					if(n<1&&it.size()>0)
					n=1;
				
				if(it.size()>0)
				s=s+" You also found: ";
									
				for(int i=it.size()-1;i>it.size()-1-n&&i>=0;i--)
				{
				Item m=(Item)(it.elementAt(i));	
				
				player.addItem(m);
				p.removeItem(m);
								
				//////System.out.println(m);
				s=s+m.getAmount()+" "+m.getName()+",";
				}
				
				s=s.substring(0,s.length()-1)+".";		
				
				main.sendCPlace();
				}
				else
				s="You found nothing.";
				
				
				JOptionPane.showMessageDialog(main.getFrame(),s,"Searching...",JOptionPane.INFORMATION_MESSAGE);	
								
						
				//	cancel an option to search again:
				}
				
			
			}
			else
			main.setMes("You already searched.");
			}
			
		
		
			if(skill.equals("Hide")&&!player.attacked())
			{
			
			
			if(player.getHiding()==0)
			{
					
			int op=JOptionPane.showConfirmDialog(main.getFrame(),"Are you sure you want to sneak? (you can only hide once a day)","Sneak?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
				
				if(op==JOptionPane.YES_OPTION)	
				{
				int hid=player.hide();
				main.sendCPlace();
				JOptionPane.showMessageDialog(main.getFrame(),"Your hiding score is: "+hid,"Hiding...",JOptionPane.INFORMATION_MESSAGE);
				}
				
				
			}
			else
			main.setMes("You already hid this day.");
				
			}
			
			
			//add automatic healing.
			if(skill.equals("Heal")) //if poisoned, or diseased? their DC. regular- 15.
			{
				
			Char sel=(Char)(list.getSelectedValue());
			
			
				if(!player.attacked())
				{
					
					if(sel!=null)
					{
					
					int op=JOptionPane.showConfirmDialog(main.getFrame(),"Do you want to heal "+sel+"? (the healing process will take the whole day)","Heal?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
					
					
						if(op==JOptionPane.YES_OPTION) //healing sel.
						{
							
							String chosen=sel.getSpells().chooseDisease();
													
							if(!chosen.equals(""))
							{
							String mes=player.getSpells().heal(sel,chosen);
							JOptionPane.showMessageDialog(main.getFrame(),mes,"Healing...",JOptionPane.INFORMATION_MESSAGE);
							}
							
							if(player.getSkills().check("Heal",sel))
							{
								//+checks to heal diseases and stuff, if succeed.
								main.getPage().setSearched(false);
									
																
								sel.heal(player.lvl()*3);
								
								JOptionPane.showMessageDialog(main.getFrame(),"You succeeded to heal "+(player.lvl()*3)+" damage.","Healing...",JOptionPane.INFORMATION_MESSAGE);
																		
								useTurns(player.getSpeed()); //sendcplace here.
								
							}
							else
							JOptionPane.showMessageDialog(main.getFrame(),"You failed to heal the character.","Healing...",JOptionPane.INFORMATION_MESSAGE);
												
							
						}
					}
					else
					main.setMes("You can't heal yourself.");
																
				}
				else //in battle
				{
					if(sel!=null)
					{
					
					boolean dying=false;
					
						if(sel instanceof Character)
						if(((Character)(sel)).isDying())
						dying=true;
					
						if(dying) //if not dying: trying to heal a disease maybe.
						{
						Character seld=(Character)(sel);
							if(!seld.stable())
							{
							int op=JOptionPane.showConfirmDialog(main.getFrame(),"Do you want to stabilize "+seld+"?","Heal?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
												
								if(op==JOptionPane.YES_OPTION) //healing sel.
								{
									if(player.getSkills().check("Heal",seld))
									{
									seld.stable(true);
									JOptionPane.showMessageDialog(main.getFrame(),"You succeeded to stabolize the player.","Healing...",JOptionPane.INFORMATION_MESSAGE);
									main.sendCPlace();	//will cause bugs?
									}
									else
									JOptionPane.showMessageDialog(main.getFrame(),"You failed to stabolize the player.","Healing...",JOptionPane.INFORMATION_MESSAGE);
								}
							}
							else
							main.setMes("The character doens't need to be stabilized.");
						}
						else
							if(sel.getSpells().getAffected().size()!=0)
							{
							int num=sel.getSpells().NoD();
								if(num!=0)
								{
								String[] dis=sel.getSpells().getDiseases(num);
								String chosen=(String)(JOptionPane.showInputDialog(main.getFrame(),"Which Disease do you want to heal?","Choosing...", JOptionPane.QUESTION_MESSAGE,null,dis,dis[0]));							
								
								String mes=player.getSpells().heal(sel,chosen);
								JOptionPane.showMessageDialog(main.getFrame(),mes,"Healing...",JOptionPane.INFORMATION_MESSAGE);
									if(mes.indexOf("failed")==-1)
									main.sendCPlace();
								}
								else
								main.setMes("You can only try to heal a disease or a dying character.");
														
							}
							else
							main.setMes("You can only stabilize characters (when they are dying) when you are in battle.");
					}
					else
					main.setMes("You can't heal yourself.");
					
				
				}
								
			} //heal
			
			
			
			if(skill.equals("Craft")&&!player.attacked())
			{
				//time... ??
				//don't forget! days! (or time.)
				
				//takes a few days.(every thing, a different number of days)
				String options[]=new String[3];
				options[0]="Weapons";
				options[1]="Shields";
				options[2]="Armors";
				Object chosen=JOptionPane.showInputDialog(main.getFrame(),"What do you want to Craft?","Select", JOptionPane.QUESTION_MESSAGE,null,options,options[0]);
				
				String des="The list for crafting: (DC - difficulty class) [time it takes: DC/3 turns ('moved').]\n";
				int max=10;
				int maxbo=5;
				int min=4;
				
					if(chosen==options[0]) //weapons
					{
				max=10;
				maxbo=5;
				min=4;
				des+="1: Arrows - 1 Wood and 1 Iron for 20 arrows. (DC - 10)\n";
				des+="2: Bolts - 1 Wood and 2 Iron for 10 bolts. (DC - 11)\n";
				des+="3: Bullets - 10 Pebbles for 10 bullets. (DC - 9)\n";
				des+="4: Short Sword - 10 Iron and 2 Wood for 1 sword. (DC - 13)\n";
				des+="5: Longsword - 10 Iron and 5 Silver for 1 sword. (DC - 15)\n";
				des+="6: Short Bow - 5 Wood 1 Bow String for 1 bow. (DC - 14)\n";
				des+="7: Long Bow - 8 Wood 2 Bow String for 1 bow. (DC - 16)\n";
				des+="8: Dagger - 5 Iron and 3 Copper for 1 dagger. (DC - 12)\n";
				des+="9: Sling - 2 Slik for 1 sling. (DC - 12)\n";
				des+="10: Handaxe - 6 Iron and 3 Wood for 1 axe. (DC - 14)\n";
				if(player.getSkills().craft()>20)
				{
				des+="(for every weapon above, you can get a +1 attack and damage bonus for additional (bonus*the components needed) components,\nand adding the DC- DC*bonus and the bonus itself,up to a maximum of +5, by typing the wanted bonus [e.x. '5+3' - Longsword +3 bonus])\n";
				des+="11: Great Sword - 1 Gold and 10 Silver for 1 sword. (DC - 25)\n";	
				des+="12: Battleaxe - 8 Iron, 10 Silver and 2 Wood for 1 axe. (DC - 26)\n";
				des+="13: Greataxe - 15 Silver and 5 Gold for 1 axe. (DC - 30)\n";
				des+="14: Heavy Crossbow - 20 Wood and 10 Bow String for 1 crossbow. (DC - 28)\n";
				des+="15: Katana - 10 Gold, 15 Silver, 5 Copper and 5 Iron for 1 sword (with another +1 bonus to attack and damage). (DC - 35)\n";
				}
					}
				
				
					if(chosen==options[1]) //Shields
					{
				max=3;
				maxbo=2;
				min=1;
				des+="1: Buckler - 3 Wood and 3 Silk for 1 Shield. (DC - 16)\n";
				des+="2: Wooden Shield - 5 Wood and 1 Silk for 1 Shield. (DC - 14)\n";
				des+="3: Steel Shield - 10 Iron and 3 Silver for 1 Shield. (DC - 18)\n";
				if(player.getSkills().craft()>20)
				{
				des+="(for every shield above, you can get a +1 armor bonus for additional (bonus*the components needed) components,\nand adding the DC- DC*bonus and the bonus itself,up to a maximum of +2, by typing the wanted bonus [e.x. '1+2' - Buckler +2 bonus])\n";
				des+="4: Tower Shield - 10 Iron and 10 Silver and 5 Gold for 1 Shield. (DC - 30)\n";
				}
					}
					
					if(chosen==options[2]) //Armors
					{
				max=3;
				maxbo=4;
				min=1;
				des+="1: Scale Mail - 10 Iron and 2 Silver and 5 Copper for 1 Armor. (DC - 16)\n";
				des+="2: Chain Shirt - 15 Iron and 5 Silver for 1 Armor. (DC - 18)\n";
				des+="3: Chainmail - 20 Iron and 10 Silver and 2 Gold for 1 Armor. (DC - 22)\n";
				if(player.getSkills().craft()>20)
				{
				des+="(for every armor above, you can get a +1 armor bonus for additional (bonus*the components needed) components,\nand adding the DC- DC*bonus and the bonus itself,up to a maximum of +4, by typing the wanted bonus [e.x. '1+2' - Scale Male +2 bonus])\n";
				des+="4: Splint Male - 5 Iron and 15 Silver and 10 Gold and 10 Copper for 1 Armor. (DC - 30)\n";
				des+="5: Half Plate - 15 Gold and 15 Silver and 5 Pebbles for 1 Armor. (DC - 35)\n";
				}
					}
					
				des+="\nChoose the number for what you want to craft.";
				
				
				String cho=JOptionPane.showInputDialog(main.getFrame(),des,"Craft",JOptionPane.INFORMATION_MESSAGE);
				
				String bon="0";
				
			if(cho!=null)
			{	
				
				int bb=cho.indexOf('+'); //check only "+".
				if(bb!=-1)
				{
				bon=cho.substring(bb+1); //?
				cho=cho.substring(0,bb);
				}
			}	
				
				if(Page.isNatural(cho)&&Page.isNatural(bon))
				{
					int ind=Integer.parseInt(cho);
					
					int bonus=Integer.parseInt(bon);
					
					if(player.getSkills().craft()>20)
					{
						if(chosen==options[0])
						max=15;
						if(chosen==options[1])
						max=4;
						if(chosen==options[2])
						max=5;
					}
					if(bonus==0||max==maxbo) //need to change
					{
					
					if((ind>=1&&ind<=max&&bonus==0)||(ind>=min&&ind<=max&&bonus>0&&bonus<=maxbo))
					{
						
						int op=JOptionPane.showConfirmDialog(main.getFrame(),"Are you sure you want to craft the selected item? (the substances will be used even if you fail)","Craft?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
						
						if(op==JOptionPane.YES_OPTION)
						{
						//crafting...	
						int cdc=craft(ind,bonus,(String)(chosen));
							
									
						useTurns(cdc/3);
						}
						
						
					}
					else
					main.setMes("Not a valid number.");
					
					}
					else
					main.setMes("You don't have 20 in Craft.");
					
				}
				else
				main.setMes("Not a number (cancelled).");
				
					
				
			}//craft
			else
				if(skill.equals("Craft"))
				main.setMes("You can't craft while in a battle.");
			
			if(skill.equals("Pick Pocket"))
			{
				
			Char sel=(Char)(list.getSelectedValue());	
				if(sel!=null)
				{
					String options[]=new String[2];
					options[0]="Money";
					options[1]="Item";
					Object chosen=JOptionPane.showInputDialog(main.getFrame(),"What do you want to Steal from "+sel.name()+"?","Select", JOptionPane.QUESTION_MESSAGE,null,options,options[0]);
					
					if(chosen!=null) //what does it get if canacel?
					main.sendPick((String)(chosen),sel);

				}
				else
				main.setMes("You have to pick a character to steal from.");
				
			}//pickp
								
			
			if(skill.equals("Perform"))
			{
			Place p=player.getPlace();
			
				if(p.getName().equals("Town")||p.getName().equals("Village"))
				{
					if(player.getMoved()==0)
					{	
						int op=JOptionPane.showConfirmDialog(main.getFrame(),"Are you sure you want to perform here? (performing will take the whole day)","Perform?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
						if(op==JOptionPane.YES_OPTION)
						{
							int dc;
							if(p.getName().equals("Town"))
							dc=15;
							else
							dc=10;
							
							if(player.getSeen().size()>=6)
							dc=dc+5;
							
							String mes="";
							
							if(player.getSkills().check("Perform",dc))
							{
								double gp=0;
								
								if(dc==10)
								gp=(double)Dice.roll(10)*0.01;
								if(dc==15)
								gp=(double)Dice.roll(10)*0.1;
								if(dc==20)
								gp=(double)Dice.roll(3,10)*0.1;	
								
								player.addMoney(gp);
								
								mes="You performed the whole day and you earned "+gp+" GP.";
								
							}
							else
							mes="You performed the whole day and you earned nothing.";
							
							JOptionPane.showMessageDialog(main.getFrame(),mes,"Performing...",JOptionPane.INFORMATION_MESSAGE);	
							
							useTurns(player.getSpeed());
							
							
							//if(!player.isDead()) not needed. i think.
							main.sendCPlace();
						}
					}
					else
					main.setMes("You need the whole day to do a performance.");
				}
				else
				main.setMes("You can only perform in a town or a village.");
			}
			
		} //skills
				
		
		
		
		if(button==spells)
		{
		int ss=main.getPage().getIA();
		
		if(ss<0)
		ss=0;
		
		Spells sp=player.getSpells();
		String spell=sp.getSpell(ss);
		
		Char sel=(Char)(list.getSelectedValue());

	if((sp.getUsed(ss)<sp.getUses(ss)||sp.getUses(ss)==-1)&&!spell.equals("Defensive Roll"))
	{
		
		//////System.out.println("uses.");
			
		if(spell.equals("Lay On Hands")||spell.equals("Wholeness of Body"))
		{ //choose how many?
			if(sel==null)
			sel=player;
				
				if(spell.equals("Lay On Hands")&&sel!=player)
				{
				int op=JOptionPane.showConfirmDialog(main,"Do you want to use "+spell+" on "+sel+" (pressing 'no' means you want to use it on yourself)?","Use it?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
				if(op==JOptionPane.NO_OPTION)
				sel=player;
				}
			
			String mes=sp.useSpell(ss,sel);
			
			JOptionPane.showMessageDialog(main.getFrame(),mes,"Healing...",JOptionPane.INFORMATION_MESSAGE);	
			
		//////System.out.println("crap.");	
			main.sendCPlace();			
		}
		else
		{
		boolean send=false;
		
			if(spell.indexOf("Cure")!=-1)
			{
				if(sel==null)
				sel=player;
				
				if(sel!=player)
				{
				int op=JOptionPane.showConfirmDialog(main,"Do you want to use "+spell+" on "+sel+" (pressing 'no' means you want to use it on yourself)?","Use it?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
				if(op==JOptionPane.NO_OPTION)
				sel=player;
				}
				
		//		////System.out.println("crap2.");
				
				if(player.attacked())
				send=true;
				else
				{
				String mes=sp.useSpell(ss,sel);
			
				JOptionPane.showMessageDialog(main.getFrame(),mes,"Healing...",JOptionPane.INFORMATION_MESSAGE);	
			
				main.sendCPlace();	
				}								
				
			}
			else
			{
		//		////System.out.println("bvvv.");
				if(Spells.self(spell))
				{
					if(player.attacked())
					send=true;
					else
					main.setMes("You can't use this spell\\ability outside of battle.");		
				}
				
		//		////System.out.println("not target");
				
				if(Spells.target(spell))
				{
		//		////System.out.println("target");
					if(sel!=null)
					{
					send=true;
						if(spell.equals("Sneak Attack"))
						{
						////System.out.println("sneak");
							if(player.attacked())
							{
							
							int nm=0;
							if(sel instanceof Character)
							nm=1;
							
							if(player.getPlace().getFighters().size()<5&&player.getPlace().NoF("Character")-nm<=1)
							{
							send=false;
							main.setMes("You can't sneak attack- there are too few fighters to flank (at least 4 are needed).");		
							}
							
							}
							else //not attacked
							{
								if(sel.seen(player))
								{
								send=false;
								main.setMes("You can't sneak attack- you were seen.");		
								}
								
							}
						}
					
					}
					else
					main.setMes("You must choose a character in order to use this spell\\ability.");
				}
			}
		
		
		
		
		
			if(send) //sending attack
			{
			fighters=player.getPlace().getFighters();
			
			main.sendCPlace();
			sendSpell(ss,sel);
			
			waitFC(true);
								
			if(fighters.size()==0) //the fighting begins
			{		
			addBattle();
			}
							
			}
		
		}		
		/*
		if(player.attacked()) //attacked.
		{
			boolean at=false; //attacking
			boolean nif=false; //not in fight (not uses a turn)
			
			if((Spells.target(spell)&&!(list.isSelectionEmpty()))||(!Spells.target(spell))) //need target
			at=true;
			
			if(Spells.fight(spell))
			{
			at=false;
			nif=true;	
			}		
								
				if(at)
				{
					if(sp.equals("Sneak Attack")&&player.getPlace().getFighters().size()>4||!sp.equals("Sneak Attack"))	
					{//or sleep\parlz					
						main.sendCPlace();
						sendSpell(ss);
						
						waitFC(true);
						
						fighters=player.getPlace().getFighters();
						
							if(fighters.size()==0) //the fighting begins
							{		
							addBattle();
							}
					}
					else
						if(sp.equals("Sneak Attack"))
						main.setMes("You can't sneak attack, there are too few fighters to flank.");		
				}
				
				
				if(nif) //here. not in server.
				{
					
					
					
					
				}
				
		}
		else //not attacked.
		{
			/*
					if(sp.equals("Sneak Attack")&&!sel.seen(player)||!sp.equals("Sneak Attack"))	
					{//or sleep\parlz
			
					}
					else
						if(sp.equals("Sneak Attack"))
						main.setMes("You can't sneak attack if you were seen.");
			*/
			
		//}
	}
	else
		if(spell.equals("Defensive Roll"))
		main.setMes("You can't use that skill- it is automatic.");
		else
		main.setMes("The spell\\ability cannot be used again today.");
	
		} //spells
		
		
		
		
		
		if(button==sleep)
		{
		boolean slp=false;	
		
		if(player.attacked())
		main.setMes("You can't sleep when you're attacked.");
		else
		{
			
			if(player.getMoved()>=player.getSpeed())
			{
			int op=JOptionPane.showConfirmDialog(main.getFrame(),"Are you sure you want to sleep?","Sleep?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
				
				if(op==JOptionPane.YES_OPTION)
				{
				player.setMoved(0);	
				
				player.setHiding(0);
								
				//etc. spells etc.
				
				player.getSpells().sleep();
				
				
				main.sendCPlace();
				
				
				JOptionPane.showMessageDialog(main.getFrame(),"You slept and you refilled your energies.","Sleeping...",JOptionPane.INFORMATION_MESSAGE);	
				slp=true;
				}
			}
			else
				if(player.getMoved()==0)
				{
					int op=JOptionPane.showConfirmDialog(main.getFrame(),"Resting the whole day will recover some of the damage you have taken. Do you want to rest?","Resting?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
				
					if(op==JOptionPane.YES_OPTION)
					{
						player.heal(player.lvl());
										
						player.setHiding(0);
						
						player.getSpells().sleep();
						//etc. spells etc.
										
						main.sendCPlace();
						slp=true;
					}
				}
				else
				main.setMes("You can't sleep yet.");
		}
		
			if(slp)
			{
				if(player.hasItem("Tent",1))
				if(Dice.roll(100)>50)
				slp=false;
				
					if(slp)
					sendSleep();
			}
			
		}
		
		
		if(button==talk)
		{
		
		Char sel=(Char)(list.getSelectedValue());
		boolean all=false;
		boolean priv=false;
		
		if(sel!=null)
		{
		
			if(sel instanceof Character)
			{
			//"conversation"
			
			int op=JOptionPane.showConfirmDialog(main.getFrame(),"Do you want to send a private message to "+sel+"? (pressing no means the message won't be private)","Private?",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);	
			
			
			if(op==JOptionPane.YES_OPTION)
			priv=true;
						
			if(op==JOptionPane.NO_OPTION)
			all=true;
			
			}		
			else //in the future
			{
			
		if(!player.attacked())
		{
			int op=JOptionPane.showConfirmDialog(main.getFrame(),"Do you want to communicate with "+sel+"? (pressing no means you want to send a message to all players here)","Communicate?",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);		
			if(op==JOptionPane.NO_OPTION)
			all=true;	
			
				if(op==JOptionPane.YES_OPTION) //communication
				{
					if(sel instanceof Enemy)
					{
					main.setMes("You don't want to make an enemy notice you.");						
					}
					else
					{
					
						if(sel instanceof Merchant)
						{
							Merchant mer=(Merchant)(sel);
							Vector inv=mer.getInv();
							//Item[] its=(Item[])(inv.toArray());
							Object[] its=inv.toArray();
							if(its.length>0)
							{
							Item chosen=(Item)(JOptionPane.showInputDialog(main.getFrame(),mer+": What do you want to buy?","Buying...", JOptionPane.QUESTION_MESSAGE,null,its,its[0]));
								if(chosen!=null)
								{
								double val=chosen.getValue();
										if(chosen.getAmount()==1&&player.gp()>=val)
										{
											int con=JOptionPane.showConfirmDialog(main.getFrame(),"Are you sure you want to buy "+chosen.getName()+"? (it costs "+val+" GP and you have "+player.gp()+" GP)","Buy?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);		
											if(con==JOptionPane.YES_OPTION) //buying...
											{
												mer.addMoney(val);
												player.addMoney(-val);
												player.addItem(chosen);
												inv.remove(chosen);				
												
												String gg=""+(double)(val*80/100); //value goes down.
												double g=Double.parseDouble(gg);
												if(gg.indexOf(".")!=-1&&gg.indexOf(".")+2<gg.length())
												g=Double.parseDouble(gg.substring(0,gg.indexOf(".")+2));
												
												chosen.setValue(g);
												
												main.sendCPlace(); //could bugs...
											}
										}
										else //choose amount
										{
										String am=JOptionPane.showInputDialog(main.getFrame(),"How many do you want?","1");
										//////System.out.println("am: "+am);
										int amount=1;
										if(Page.isNatural(am))
										{
										amount=Integer.parseInt(am);
										//////System.out.println("nat "+amount);
										}
										if(amount<1)
										{
										amount=1;
										//////System.out.println("1");
										}
										
										//////System.out.println(amount+">"+chosen.getAmount());
										if(amount>chosen.getAmount())
										{
										//////System.out.println(amount+">"+chosen.getAmount());
										amount=chosen.getAmount();
										}
										
										val=(double)((val/chosen.getAmount())*amount);
										
										//////System.out.println("am: "+amount+" , ch: "+chosen.getName()+" , ga: "+chosen.getAmount());
										
									if(player.gp()>=val)
									{
											int con=JOptionPane.showConfirmDialog(main.getFrame(),"Are you sure you want to buy "+amount+" "+chosen.getName()+"? (it costs "+val+" GP and you have "+player.gp()+" GP)","Buy?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);		
											if(con==JOptionPane.YES_OPTION) //buying...
											{
												mer.addMoney(val);
												player.addMoney(-val);
												chosen=Item.createItem(chosen.getName(),chosen.getClass().toString(),amount);
												player.addItem(chosen);
												mer.removeIfInv(chosen.getName(),amount);
												
												String gg=""+(double)(val*80/100); //value goes down.
												double g=Double.parseDouble(gg);
												if(gg.indexOf(".")!=-1&&gg.indexOf(".")+2<gg.length())
												g=Double.parseDouble(gg.substring(0,gg.indexOf(".")+2));
												
												chosen.setValue(g);
												
												main.sendCPlace(); //could bugs...
											}
									}								
									else
									JOptionPane.showMessageDialog(main.getFrame(),mer+": Sorry, you don't have enough money.","Buying...",JOptionPane.PLAIN_MESSAGE);
							
										}
										
								}
							}
							else
							JOptionPane.showMessageDialog(main.getFrame(),mer+": Sorry, I don't have anything to sell.","Buying...",JOptionPane.PLAIN_MESSAGE);
						}
						else
						{ //change when quester? (rumors)
						
							if(sel.name().equals("Healer"))
							{
								int max=player.getDam();
								
								String am=JOptionPane.showInputDialog(main.getFrame(),sel+": I can heal you for 0.1 GP per 1 point of damage.\nHow many damage points do you want to heal?","1");
								int amount=1; 
								if(Page.isNatural(am))
								{
								amount=Integer.parseInt(am);
								}
								
								if(amount>max)
								amount=max;
								
								double val=0.1*amount;
								
								String gg=""+val;
								if(gg.indexOf(".")!=-1&&gg.indexOf(".")+2<gg.length())
								val=Double.parseDouble(gg.substring(0,gg.indexOf(".")+2));
								
								if(player.gp()>=val)							
								{
								sel.addMoney(val);
								player.addMoney(-val);
								player.heal(amount);
								JOptionPane.showMessageDialog(main.getFrame(),sel+" healed "+amount+" points of damage for "+val+" GP.","Healing...",JOptionPane.PLAIN_MESSAGE);
								main.sendCPlace();
								}
								else
								JOptionPane.showMessageDialog(main.getFrame(),sel+": Sorry, you don't have enough money.","Healing...",JOptionPane.PLAIN_MESSAGE);
							}
							else
							{
							String mes=sel+": ";
							
							switch(Dice.roll(3))
							{
							case 1:
							mes=mes+"How are you today?";
							break;
							case 2:
							mes=mes+"Be careful, evil beings lurk around.";
							break;
							case 3:
							mes=mes+"Sorry, I'm busy now...";
							break;
							}	
							
							JOptionPane.showMessageDialog(main.getFrame(),mes,"Talking...",JOptionPane.PLAIN_MESSAGE);
							}
						}				
					
					}
				}	
		}
		else		
		main.setMes("You can't talk to non player characters while you are in a battle.");
			}
	
		}
		else
		{
			int op=JOptionPane.showConfirmDialog(main.getFrame(),"Do you want to send a message to all of the players in this place?","All?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
						
			if(op==JOptionPane.YES_OPTION)
			all=true;			
		}
		
		if(all||priv)
		{
		
		String mes=JOptionPane.showInputDialog(main.getFrame(),"The message:","Message",JOptionPane.PLAIN_MESSAGE);
		
		if(priv)
		mes="(private) "+mes;
		
		//if sel=null?
		if(mes.indexOf("*TALK")!=0)
		main.sendMes(player.name()+": "+mes,priv,player.getPlace().getChars().indexOf(sel));				
		else
		main.setMes("Invalid message. '*TALK' not allowed.");
		
		}
				
		}
		
		if(button==run)
		{
			if(!player.attacked())
			main.setMes("You can't run from battle if you're not in a fight.");
			else
			{
			int op=JOptionPane.showConfirmDialog(main.getFrame(),"Are you sure you want to run from battle? (it will use up your turn)","Run?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);	
			if(op==JOptionPane.YES_OPTION)
			main.sendRun();		
			}
		}
		
		
		
		if(button==attack&&!(list.isSelectionEmpty()))
		{
			
		//function that returns 2 objects, (or function that changes a variable -V)
		
		
		
		
			if((!player.getWeapon().isRanged())||(player.getWeapon().isRanged()&&Item.howMany(player.getItems(),player.getWeapon().kind())>0))
			{
				
				if(player.attacks()==0)
				main.unable(true);
				
				
				main.sendCPlace();
				sendAttack();
				
				waitFC(true);
				
				fighters=player.getPlace().getFighters();
				
					if(fighters.size()==0) //the fighting begins
					{		
					//////System.out.println("nim");
					addBattle();
					}
					/*
					Vector v=new Vector();
					v.add(player);
					v.add(list.getSelectedValue());
					
					nums=new Vector();
					
					//////System.out.println("v: "+v+"\n");
					fighters=Char.initiation(v,nums); //check this after few attack. with "for"?
					main.battle(true);
					
					//////System.out.println("order: "+fighters);
					//////System.out.println("nums: "+nums);
					
					
					Char current=(Char)(fighters.elementAt(order)),enm;		
						
					attacks(false);
					
					//if emeny has ranged etc.
					
					boolean style; //player first=true
					
					enm=(Char)(list.getSelectedValue());
											
					if(player==current) //player first
					{
					if(player.getWeapon().isRanged()||!enm.getWeapon().isRanged())
					style=true;
					else
					style=false;
					}
					else
					{
					if(enm.getWeapon().isRanged()||!player.getWeapon().isRanged())
					style=false;
					else
					style=true;
					}
				
				
				if(enm.seen(player))				
				{
					if(style) //player first
					{
					battle.addText(player.attack(enm));
					checkLife();
						if(!enm.isDead()&&player.attacks()>=player.NoA())
						{
						battle.addText(enm.attack(player));
						checkLife();
						}
					}
					else //enm first
					{
					battle.addText(enm.attack(player));
					checkLife();
					if(!player.isDead())
					{
					battle.addText(player.attack(enm));
					checkLife();
					}
						if(!enm.isDead()&&player.attacks()>=player.NoA())
						{
						battle.addText(enm.attack(player));
						checkLife();
						}
					order++;
					if(order>=fighters.size())
						order=0;
					}
				}
				else
				{
					battle.addText("(surprise) "+player.attack(enm));
					checkLife();
				}
					//order=0
					
					
					
					//player.attacks(0);
					attacks(true);
								
					} //(fighters=null)
					else //there are fighters.
					{
					
						attacks(false);
						int d=order;				
						
						Char opon=(Char)(list.getSelectedValue());
						
						if(opon.attacked())
						battle.addText(player.attack(opon)); //adds to attacks.
						else //attacking new opponent.
						{
						addFighter(opon);
						
						if(opon.seen(player))
						{						
							if(player.getWeapon().isRanged())
							battle.addText(player.attack(opon));
							else
							{
							battle.addText(opon.attack(player));
							checkLife();
							battle.addText(player.attack(opon));
							checkLife();
							}
						}
						else //surprise attack	
						{
							battle.addText("(surprise) "+player.attack(opon));
							checkLife();
						}  //nort sneaking in middle of battle.
						
						}
						//////System.out.println("attack: "+txt);
											
						checkLife();				
						
						//////System.out.println("NoA:" +player.attacks()+","+player.NoA());
						
						//////System.out.println("attacks: "+player.attacks()+", noa: "+player.NoA());
						
						//////System.out.println("attacks: "+player.attacks()+", noa: "+player.NoA());
						//////System.out.println("f: "+fighters+"     , order: "+order);
						
						if(player.attacks()>=player.NoA()) //number of attacks
						{
						player.attacks(0);
						//order++;
																	
						d=fighters.indexOf(player); //plz?
						
						order=d+1;
						if(order>=fighters.size())
						order=0;
						//////System.out.println("order: "+order+", d: "+d+", I W: "+fighters.indexOf(fighters.elementAt(order)));
						
						roundCon(d);
						
						}
						
						attacks(true); //THIS is the function that ruins it!!!
						
						checkLife();
					
						
						//if(player.isDead())
						//attacks(false);
						
						//battle.addText(s);
								
					} //fighting (ARE fighters) done.
			*/
			}	
			else
			mes.setText("You don't have enough "+player.getWeapon().kind());
		
		//:(this:) ?
		
		if(fighters!=null)
		if(fighters.size()==1)
		{
			main.battle(false,false);
			
			checkLife();
			//fighters=null;
			fighters=new Vector();
			player.attacked(false);
			order=0;
			player.attacks(0);
		}

		
		//do- attack new opponent
		
		//do- when dead\dying\fainted etc.
		
		} //end attack button
	
	//if(player.isDead())
	//dead();
			
	}
	
	
	public void addBattle()
	{
	JComponent del=battle;
	battle=new Attack();
	//////System.out.println("new fight:");
	//////System.out.println("fighters: "+fighters);
	main.addEast(battle,del);
	
	main.battle(true,false);
	}
	
	public void roundCon(final int dd)//round continue
	{
	Thread tr=new Thread(new Runnable(){
	
	int d=dd;
		
	public void run()
	{
		
	
		while(order!=d&&d>=0)
		{
		Char current=(Char)(fighters.elementAt(order));
		battle.addText(current.attack(player));//thread, sleep
										
		d=fighters.indexOf(player);
		
		order++;
		if(order>=fighters.size())
		order=0;
		
				
		if(current instanceof Character)
		{
		waitFC(true);
		
		//////System.out.println("Waiting...");
		
		try{
			synchronized(this){
		wait();
		}
		}catch(InterruptedException e){System.out.println(e);}
		//////System.out.println("order: "+order);
		
		////System.out.println("not waiting.");
		
		}
		
		checkLife();
		
		waitFC(false);
		
		//////System.out.println("Done waiting.");
				
		}
					
	checkLife();
	
	if(fighters!=null)
		if(fighters.size()==1)
		{
			main.battle(false,false);
			
			checkLife();
			//fighters=null;
			fighters=new Vector();
			player.attacked(false);
			order=0;
			player.attacks(0);
		}
	
	//a check for- if there are character left. (if not, stop.)
	
	//if(player.isDead())
	//dead();
	
		
	}	
		
	});	
	
	tr.start();
		
	}
	
	
	public void setDirs(boolean b)
	{
	//////System.out.println("dirs: "+b); 
	north.setEnabled(b);	
	east.setEnabled(b);
	west.setEnabled(b);
	south.setEnabled(b);
	}
	
	public synchronized void update(Character c)
	{
	////System.out.println("update");	
	//////System.out.println("update, player: "+c+", current: "+player);
	int selInd=-1;
	if(list!=null)
	selInd=list.getSelectedIndex();
	//else
	////System.out.println("list = null");
	
	Character tem=player;
	//////System.out.println("update"+tem);
	player=c;
	player.setPlace(map[player.getY()][player.getX()]); //needed?
	
	//////System.out.println("pl "+tem);
	
		if(tem==null) //first time
		{
	////System.out.println("first time (tem==)");	
		
	Place pl=getPlace(player.getY(),player.getX());
	//Vector enms=pl.getEnemies();
	Vector chars=pl.getChars();
	
	pl.getChars().add(player);
	
	Vector v=new Vector();
	nums=new Vector();
		
	for(int i=0;i<chars.size();i++) //chars, no enms
	{
	Char no=(Char)(chars.elementAt(i)); //chars, no enms
	no.spot(chars);
	}
	
	player.move(0,0,pl);
	
	main.sendCPlace();
	
		}

	setVisible(false);
		
		
		removeAll();
		
		
		JPanel down=new JPanel(),up=new JPanel();
		
		//down.setBackground(Color.blue);
		

			up=getMap();
									
			down.setLayout(new BorderLayout());
		
			down.add(BorderLayout.NORTH,north);
			down.add(BorderLayout.EAST,east);
			down.add(BorderLayout.WEST,west);
			down.add(BorderLayout.SOUTH,south);
			
			
		
		Vector v=player.getSeen();
		
		center.removeAll();
		
		list=new JList(v);
						
		
		list.setCellRenderer(new ListCellRenderer(){
								
									public Component getListCellRendererComponent(
							         JList list,
							         Object value,
							         int index,
							         boolean isSelected,
							         boolean cellHasFocus)
							     {
							     	JLabel c=new JLabel();
							         c.setText(value.toString());
							         c.setBackground(isSelected ? Color.red : Color.white);
							         c.setForeground(isSelected ? Color.blue : Color.black);
							         
							         if(((Char)(value)).attacked())
							         c.setForeground(isSelected ? Color.magenta : Color.red);
							         							         
							         c.setToolTipText(((Char)(value)).info());
							         return c;
							     }
								
										
									});
									
		
		
		
		
		//////System.out.println("list: "+v); //del
		
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//list.setSelectedIndex(0);
		
		list.setToolTipText("Characters you have spotted");
		
		if(player.isDead())
		{
		list=new JList();
		list.setToolTipText("You are dead");
		}
		
		JScrollPane sp=new JScrollPane(list);
					
		list.setVisibleRowCount(5);
		
		center.add(sp);
		
		JPanel buttons=new JPanel();
		buttons.setLayout(new GridLayout(0,1));
		
		buttons.add(attack);
		buttons.add(skills);
		buttons.add(spells);
		buttons.add(talk);
		buttons.add(run);
		buttons.add(sleep);
				
		
		//boolean tr=player.attacks()>0; //no consideration to spat=true, not needed. (or maybe attacks there=noa?)
		//skills.setEnabled(!tr);
		//spells.setEnabled(!tr);
		//run.setEnabled(!tr);
		
				
		center.add(buttons);
	
	/*if(player.isDead())
	{
	dead();
	//////System.out.println("en: "+north.isEnabled());
	}*/
	
			down.add(BorderLayout.CENTER,center);
			
				setLayout(new GridLayout(2,1));
				add(up);
				add(down);
		
	
	
	
	setVisible(true);
	
	
	//checkLife(); //the dead thing. (is needed?)
	
	
	//improvement needed!
	/*if(selInd!=-1)
	if(list.getLastVisibleIndex()>=selInd)
	list.setSelectedIndex(selInd);*/
	//////System.out.println("cc: "+list.getComponentCount()+", sel: "+selInd);
	
	if(selInd!=-1)
	if(list.getModel().getSize()>selInd)
	list.setSelectedIndex(selInd);
	
	
	//////System.out.println("attack,en: "+attack.isEnabled());
	
	
	//////System.out.println("end update");
	
	} //end update
	
	
	public void checkAttacks()
	{
	boolean tr=player.attacks()>0; //no consideration to spat=true, not needed. (or maybe attacks there=noa?)
	skills.setEnabled(!tr);
	spells.setEnabled(!tr);
	run.setEnabled(!tr);
	}
	
	
	public Place[][] getPlaces()
	{
	return map;	
	}
	
	public JPanel getMap()
	{
		JPanel p=new JPanel();
		p.setLayout(new GridLayout(dim,dim));
		
		for(int i=0;i<dim;i++)
		for(int j=0;j<dim;j++)
		{
		
		//JLabel l=new JLabel("<html><body bgcolor="+map[i][j].getRGB()+">&nbsp&nbsp&nbsp");
		//if(player.getX()==j&&player.getY()==i) 
		//l.setText("<html><body bgcolor="+map[i][j].getRGB()+">O");
		
		JButton l=new JButton("");
		l.setBackground(map[i][j].getColor());
		l.setEnabled(false);
		
		l.setMargin(new Insets(0,0,0,0));
		
		//l.setOpaque(false);
		
		if(player.getX()==j&&player.getY()==i) 
		{
			l.setText("O");
			//l.setForeground(Color.red);
		}
		
		//and quest (X- target). if both then smting else?
		l.setToolTipText(map[i][j].getName()+" ["+j+","+i+"]");
		
		p.add(l);
		}
		
		//p.setBackground(new Icon());
		return p;
	}
	
	
	public void checkLife()
	{
	for(int i=0;i<fighters.size();i++)	
	{
	Char c=(Char)(fighters.elementAt(i));
	
		if(c.isDead()) //later
		{
			//////System.out.println("dead: "+c.info());
			
			fighters.remove(c);	
			map[player.getY()][player.getX()].removeChar(c);
			//list.remove(c);
			
			//drop items, get XP (if this player only, or all?)
			//////System.out.println("instance: "+c);
			/*
			if(c instanceof Character)
			{
				dead();
			}
			*/
			update(player);
		}
		
	main.sendCPlace(); //(y,x) ? 
	
	}
	
	if(order>=fighters.size())
	order=0;
	
	}
	
	
	public void dead()
	{
		attack.setEnabled(false);
		skills.setEnabled(false);
		sleep.setEnabled(false);
		spells.setEnabled(false);
		talk.setEnabled(false);
		run.setEnabled(false);
		
		String s="You are dead";
		
		if(battle!=null&&!mes.getText().equals("You are dead")&&player.attacked())
		battle.addText(s);
		
		mes.setText(s);
		
		
		setDirs(false);
				
		waitFC(true); //no attack
		
		//////System.out.println("Dead!!!");
		
		if(player.getPlace().getChars().indexOf(player)!=-1)
		{
		int ind=player.getPlace().removeChar(player);
		player.getPlace().dropItems(player);
		main.sendCPlace(player.getY(),player.getX(),ind);	
		}
		
		main.dead();
		
		//main.enabled
		//battle(true)
		
	}
	
	
	public Vector getFighters()
	{
		return fighters;
	}
	
	public void addFighter(Char c)
	{
	c.attacked(true);
	
	//////System.out.println("before: fi: "+fighters+"\nnum: "+nums+"\nch: "+c);
	
	int m=c.initiative();
	
	boolean b=true;
	
	//change n to universaler.
	
		for(int j=0;j<nums.size()&&b;j++)
		{
			//////System.out.println(i+","+j);
			int g=Integer.parseInt((String)(nums.elementAt(j)));
			if(m>g)
			{
			fighters.add(j,c);
			b=false;
			}
			
			if(m==g)
			{
				int d=((Char)(fighters.elementAt(j))).getDEX();
				int cd=c.getDEX();
				if(cd>d)
				{
				fighters.add(j,c);
				b=false;
				}
				else
					if(cd==d)
						if(Dice.roll(2)==1)
						{
						fighters.add(j,c);
						b=false;
						}	
			}
		}
		
	if(b)
	{
	fighters.add(c);
	}
	
	
	//////System.out.println("fi: "+fighters+"\nnum: "+nums+"\nch: "+c);
	nums.add(fighters.indexOf(c),""+m); //bug if inserted last or fifth?
	
	
	}
	
	
	public boolean canPass()
	{
	//System.out.println(""+player.getDam());
	Place p=player.getPlace();
	String n=p.getName();	
	int dc=p.dc();
	
	if(n.equals("River"))
	{
		if(player.getSkills().check("Swim",dc))
		{
		JOptionPane.showMessageDialog(main.getFrame(),"You succeeded to swim and move from this river.","Swimming...",JOptionPane.INFORMATION_MESSAGE); //cause bugs? doesn't send until "OK".
		}		
		else
		{
			if(Dice.roll(20)+player.con()>=6+dc/5)
			JOptionPane.showMessageDialog(main.getFrame(),"You failed to swim and therefore are still near the river.","Swimming...",JOptionPane.INFORMATION_MESSAGE);
			else
			{
			JOptionPane.showMessageDialog(main.getFrame(),"You failed to swim and you drowned.","Swimming...",JOptionPane.INFORMATION_MESSAGE);	
			player.dealDam(player.hp()+10);
			checkPLife(p);
			}
		return false;	
		}
		
	}
	
	if(n.equals("Mountain"))
	{
		if(player.getSkills().check("Climb",dc))
		{
		JOptionPane.showMessageDialog(main.getFrame(),"You succeeded to climb and move from this mountain.","Climbing...",JOptionPane.INFORMATION_MESSAGE); //cause bugs? doesn't send until "OK".
		}		
		else
		{
		int dam=Dice.roll(dc/5,6);
		
		if(player.getClas().equals("Monk"))
		{
			int l=player.lvl();
			if(l>=4&&l<=5)
			dam=Dice.roll(dc/5-1,6);
			if(l>=6&&l<=7)
			dam=Dice.roll(dc/5-2,6);
			if(l>=8&&l<18)
			dam=Dice.roll(dc/5-3,6);
			if(l>=18)
			dam=0;
		}
			
			if(dam!=0)
			{
			JOptionPane.showMessageDialog(main.getFrame(),"You failed to climb and you fell, suffering "+dam+" damage.","Climbing...",JOptionPane.INFORMATION_MESSAGE);
			player.dealDam(dam);
			checkPLife(p);
			}
			else
			JOptionPane.showMessageDialog(main.getFrame(),"You failed to climb and you fell.","Climbing...",JOptionPane.INFORMATION_MESSAGE);
			
		return false;	
		}
		
	}
	
	
	if(n.equals("Forest")) //cave
	{
		if(player.getSkills().check("Intuit Direction",dc))
		{
		JOptionPane.showMessageDialog(main.getFrame(),"You succeeded to find your way through the forest.","Finding direction...",JOptionPane.INFORMATION_MESSAGE); //cause bugs? doesn't send until "OK".
		}		
		else
		{
		JOptionPane.showMessageDialog(main.getFrame(),"You failed to find your way, and you were lost for a third of your day.","Finding direction...",JOptionPane.INFORMATION_MESSAGE);
		int hun=0;
		for(int i=0;i<player.getSpeed()/3;i++)
		{
		int h=player.addMoved();
		if(h>hun)
		hun=h; //not hun+=h?
		}
			if(hun>0)
			JOptionPane.showMessageDialog(main.getFrame(),"You are hungry.\nYou were damaged by "+hun+" points.","You need meals",JOptionPane.WARNING_MESSAGE);	
		checkPLife(p);
					
		return false;	
		}
		
	}
	
	if(n.equals("Cave")) //cave
	{
	//not sending yet?
	if(player.hasItem("Torch",1)&&player.hasItem("Flint and Steel",1))
	{
	dc-=4;	
	player.removeItem("Torch",1);	
	}
		
		if(player.getSkills().check("Intuit Direction",dc))
		{
		JOptionPane.showMessageDialog(main.getFrame(),"You succeeded to find your way through the cave.","Finding direction...",JOptionPane.INFORMATION_MESSAGE); //cause bugs? doesn't send until "OK".
		}		
		else
		{
		JOptionPane.showMessageDialog(main.getFrame(),"You failed to find your way, and you were lost for a third of your day.","Finding direction...",JOptionPane.INFORMATION_MESSAGE);
		int hun=0;
		for(int i=0;i<player.getSpeed()/3;i++)
		{
		int h=player.addMoved();
		if(h>hun) //if twice, one is 0.
		hun=h;
		}
			if(hun>0)
			JOptionPane.showMessageDialog(main.getFrame(),"You are hungry.\nYou were damaged by "+hun+" points.","You need meals",JOptionPane.WARNING_MESSAGE);	
		//System.out.println(""+player.getDam());
		checkPLife(p);
		
		//System.out.println(""+player.getDam());
			
		return false;	
		}
		
	}
	
	
	return true;	
	}
	
	
	
	public void checkPLife(Place p)
	{
		boolean send=true;
		
		if(player.isDying())
		{
		main.getMap().waitFC(true);
		main.getMap().setDirs(false);
   		main.setMes("You are unconscious and are dying.");
//   		send=true;
   		checkDead();
   		}
		
		if(player.isDead())
		{
		//p.dropItems(player);
		//p.removeChar(player);
		send=false;
		dead();				
		//send=true;
		}
		
		if(send)
		main.sendCPlace();
		
	}
	
	
	public void checkDead()
	{
	
	Character player=main.getPlayer();
   	Place p=player.getPlace();
   	Vector pls=p.getCharacters();
   	
	   	
	  	if(pls.size()==1)
	  	{
	  		boolean dead=false;
	  		
	  		//hunger - forest
	  		
	  		if(!p.hasHealer())
	  		{
	  		dead=true;
	  		}
	  		else //healer heals
	  		{
	  		int heal=Dice.roll(2,6,2);
	  		 	if(Dice.roll(20)+heal>=15)
	  		 	{
	  		 	player.stable(true);
	  		 	int h=heal*2;
	  		 	player.heal(h);
	  		 	JOptionPane.showMessageDialog(main.getFrame(),heal+" has stabolized you and healed up to "+h+" damage.","Healed...",JOptionPane.INFORMATION_MESSAGE);	
	  		 	main.getMap().useTurns(player.getSpeed());
	  		 	}
	  		 		
	  		}
	  		
	  		if(dead)
	  		{
	  		player.dealDam(6);
	  		int ind=p.removeChar(player);
			p.dropItems(player);
			main.sendCPlace(player.getY(),player.getX(),ind);
	  		main.dead();
	  		}
	  		
	  	}//checking if dead 
	
	}
	
	
	
	public void attackChar(int or)
	{
	list.setSelectedValue(player.getPlace().getChars().elementAt(or),true);
	attack.doClick();
	}
	
	
	public void waitFC(boolean tr) //wait for character (true-disabled)
	{
	attack.setEnabled(!tr);
	skills.setEnabled(!tr);
	sleep.setEnabled(!tr);
	spells.setEnabled(!tr);
	talk.setEnabled(!tr);
	run.setEnabled(!tr);
	//all the others also
	
	////System.out.println("FC: "+tr+", "+player.attacked());
	
	if(!player.attacked()&&!player.isDead()) //bugs (isdead, to battle)?
	{
	setDirs(true);
	main.battle(false,false);
	}
	
	}
	
	
	public void sendSleep()
	{
		String s="SLEEP*";
		s+=player.getX()+",";
		s+=player.getY()+",";
		s+=serial(player)+",";
		
		main.sendMessage(s+"*SLEEP");
	}
		
	
	public void attacks(boolean b)
	{
	attack.setEnabled(b);
	skills.setEnabled(b);
	sleep.setEnabled(b);
	spells.setEnabled(b);
	talk.setEnabled(b);
	run.setEnabled(b);
	}
	
	public int getI(Place p)
	{
		for(int i=0;i<dim;i++)
		for(int j=0;j<dim;j++)
		if(map[i][j]==p)
		return i;

	return -1;
	}
	
	
	public int getJ(Place p)
	{
		for(int i=0;i<dim;i++)
		for(int j=0;j<dim;j++)
		if(map[i][j]==p)
		return j;
		
	return -1;
	}
	
	public int getPlayerOrder()
	{
		return player.getPlace().getOrder(player);
	}
	
	public void unable(boolean tr)
	{
	spells.setEnabled(!tr);	
	skills.setEnabled(!tr);			
	}
	
	public void talk(String mes)
	{
	
	////System.out.println("talk: "+battle.isVisible());
	
	if(battle.isVisible())
	battle.addText(mes);	
	else
	{
	JComponent del=battle;
	battle=new Attack("Conversation");
	//battle.setVisible(true);
	main.addEast(battle,del);
	battle.addText(mes);
	}
	
	////System.out.println("talk2: "+battle.isVisible());
	////System.out.println(battle.convert());
	
	}
	
	
	public Attack getBattle()
	{
		return battle;
	}
	
	public void sendAttack()
	{
		
		//////System.out.println("sends attack (map)....");	
		String s="ATTACK*";
		s+=player.getX()+",";
		s+=player.getY()+",";
		s+=serial(player)+",";
		
		s+=map[player.getY()][player.getX()].getChars().indexOf(list.getSelectedValue())+",";
		
		main.sendMessage(s+"*ATTACK");
	}
	
	
	public void sendSpell(int ss,Char sel)
	{
		
		//////System.out.println("sends attack (map)....");	
		String s="ABILITY*";
		s+=player.getX()+",";
		s+=player.getY()+",";
		s+=serial(player)+",";
		s+=ss+",";
		s+=map[player.getY()][player.getX()].getChars().indexOf(sel)+",";
		
		main.sendMessage(s+"*ABILITY");
		
		////System.out.println(s);	
	}
	
	
	public int serial(Char c)
	{
		return 	c.getPlace().getChars().indexOf(c);
	}
	
	
	public int serialFi(Char c) //serial in fighters
	{
		for(int i=0;i<fighters.size();i++)
		{
			Char ch=(Char)(fighters.elementAt(i));
			if(ch==c)
			return i;
		}
		
	return -1;
	
	}
	
	
	public int getOrder()
	{
		return order;
	}
	
	public Place getPlace(int i,int j)
	{
	if(i>=0&&j>=0&&i<dim&&j<dim)
	return map[i][j];
	
	return null;
	}
	
	
	public void updateBattle(String bt)
	{
		//////System.out.println("converting...");
		//////System.out.println(bt);
		ifAttacked();
		battle.convert(bt);
	}
	
	
	public int craft(int ind,int bonus,String chosen) //weapons,shields,armors
	{
	int DC=0;
	boolean comps;
	Item item=null;
	int num=0;
	String items[]=null;
	int nums[]=null;
	
	if(chosen.equals("Weapons"))
	{
		if(ind==1)
		{
			item=new Item("Arrows");
			DC=10;
			num=2;
			items=new String[num];
			nums=new int[num];
			items[0]="Wood";
			nums[0]=1;
			items[1]="Iron";
			nums[1]=1;
		}	
		//etc.
		if(ind==2)
		{
			item=new Item("Bolts");
			DC=11;
			num=2;
			items=new String[num];
			nums=new int[num];
			items[0]="Wood";
			nums[0]=1;
			items[1]="Iron";
			nums[1]=2;
		}
		
		if(ind==3)
		{
			item=new Item("Bullets");
			DC=10;
			num=1;
			items=new String[num];
			nums=new int[num];
			items[0]="Pebbles";
			nums[0]=10;
			//item2="";
			//num2=1;
		}
		
		if(ind==4)
		{
			if(bonus==0)
			item=new Weapon("Short Sword");
			else
			item=Weapon.createImproved("Short Sword",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=13;
			items[0]="Iron";
			nums[0]=10;
			items[1]="Wood";
			nums[1]=2;
		}
		
		if(ind==5)
		{
			if(bonus==0)
			item=new Weapon("Longsword");
			else
			item=Weapon.createImproved("Longsword",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=15;
			items[0]="Iron";
			nums[0]=10;
			items[1]="Silver";
			nums[1]=5;
		}
		
		if(ind==6)
		{
			if(bonus==0)
			item=new Weapon("Short Bow");
			else
			item=Weapon.createImproved("Short Bow",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=14;
			items[0]="Wood";
			nums[0]=5;
			items[1]="Bow String";
			nums[1]=1;
		}
		
		if(ind==7)
		{
			if(bonus==0)
			item=new Weapon("Long Bow");
			else
			item=Weapon.createImproved("Long Bow",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=16;
			items[0]="Wood";
			nums[0]=8;
			items[1]="Bow String";
			nums[1]=2;
		}
		
		if(ind==8)
		{
			if(bonus==0)
			item=new Weapon("Dagger");
			else
			item=Weapon.createImproved("Dagger",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=12;
			items[0]="Iron";
			nums[0]=5;
			items[1]="Copper";
			nums[1]=3;
		}
		
		if(ind==9)
		{
			if(bonus==0)
			item=new Weapon("Sling");
			else
			item=Weapon.createImproved("Sling",bonus);
			
			num=1;
			items=new String[num];
			nums=new int[num];
			DC=12;
			items[0]="Silk";
			nums[0]=2;
		}
		
		if(ind==10)
		{
			if(bonus==0)
			item=new Weapon("Handaxe");
			else
			item=Weapon.createImproved("Handaxe",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=14;
			items[0]="Iron";
			nums[0]=6;
			items[1]="Wood";
			nums[1]=3;
		}
		
		if(ind==11)
		{
			if(bonus==0)
			item=new Weapon("Great Sword");
			else
			item=Weapon.createImproved("Great Sword",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=25;
			items[0]="Gold";
			nums[0]=1;
			items[1]="Silver";
			nums[1]=10;
		}
		
		if(ind==12)
		{
			if(bonus==0)
			item=new Weapon("Battleaxe");
			else
			item=Weapon.createImproved("Battleaxe",bonus);
			
			num=3;
			items=new String[num];
			nums=new int[num];
			DC=26;
			items[0]="Iron";
			nums[0]=8;
			items[1]="Silver";
			nums[1]=10;
			items[1]="Wood";
			nums[1]=2;
		}
		
		if(ind==13)
		{
			if(bonus==0)
			item=new Weapon("Greataxe");
			else
			item=Weapon.createImproved("Greataxe",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=30;
			items[0]="Silver";
			nums[0]=15;
			items[1]="Gold";
			nums[1]=5;
		}
		
		if(ind==14)
		{
			if(bonus==0)
			item=new Weapon("Heavy Crossbow");
			else
			item=Weapon.createImproved("Heavy Crossbow",bonus);
			
			num=2;
			items=new String[num];
			nums=new int[num];
			DC=28;
			items[0]="Wood";
			nums[0]=20;
			items[1]="Bow String";
			nums[1]=10;
		}
		
		if(ind==15)
		{
			if(bonus==0)
			item=Weapon.createImproved("Katana",1);
			else
			item=Weapon.createImproved("Katana",1+bonus);
			
			num=4;
			items=new String[num];
			nums=new int[num];
			DC=35;
			items[0]="Gold";
			nums[0]=10;
			items[1]="Silver";
			nums[1]=15;
			items[2]="Copper";
			nums[2]=5;
			items[3]="Iron";
			nums[3]=5;
		}

	}//end weapons.
	
	
	
	if(chosen.equals("Shields"))
	{
		
		if(ind==1)
		{
			if(bonus==0)
			item=new Shield("Buckler");
			else
			item=Shield.createImproved("Buckler",bonus);
			
			DC=16;
			num=2;
			items=new String[num];
			nums=new int[num];
			items[0]="Wood";
			nums[0]=3;
			items[1]="Silk";
			nums[1]=3;
		}	
		
		if(ind==2)
		{
			if(bonus==0)
			item=new Shield("Wooden Shield");
			else
			item=Shield.createImproved("Wooden Shield",bonus);
			
			DC=14;
			num=2;
			items=new String[num];
			nums=new int[num];
			items[0]="Wood";
			nums[0]=5;
			items[1]="Silk";
			nums[1]=1;
		}
		
		if(ind==3)
		{
			if(bonus==0)
			item=new Shield("Steel Shield");
			else
			item=Shield.createImproved("Steel Shield",bonus);
			
			DC=18;
			num=2;
			items=new String[num];
			nums=new int[num];
			items[0]="Iron";
			nums[0]=10;
			items[1]="Silver";
			nums[1]=3;
		}
		
		if(ind==4)
		{
			if(bonus==0)
			item=new Shield("Tower Shield");
			else
			item=Shield.createImproved("Tower Shield",bonus);
			
			DC=18;
			num=3;
			items=new String[num];
			nums=new int[num];
			items[0]="Iron";
			nums[0]=10;
			items[1]="Silver";
			nums[1]=10;
			items[2]="Gold";
			nums[2]=5;
		}
	} //end shields
	
	
	if(chosen.equals("Armors")) //armors
	{
		
		if(ind==1)
		{
			if(bonus==0)
			item=new Armor("Scale Mail");
			else
			item=Armor.createImprovedA("Scale Mail",bonus);
			
			DC=16;
			num=3;
			items=new String[num];
			nums=new int[num];
			items[0]="Iron";
			nums[0]=10;
			items[1]="Silver";
			nums[1]=2;
			items[2]="Copper";
			nums[2]=5;
		}	
		
		if(ind==2)
		{
			if(bonus==0)
			item=new Armor("Chain Shirt");
			else
			item=Armor.createImprovedA("Chain Shirt",bonus);
			
			DC=18;
			num=2;
			items=new String[num];
			nums=new int[num];
			items[0]="Iron";
			nums[0]=15;
			items[1]="Silver";
			nums[1]=5;
		}
		
		if(ind==3)
		{
			if(bonus==0)
			item=new Armor("Chainmail");
			else
			item=Armor.createImprovedA("Chainmale",bonus);
			
			DC=22;
			num=3;
			items=new String[num];
			nums=new int[num];
			items[0]="Iron";
			nums[0]=20;
			items[1]="Silver";
			nums[1]=10;
			items[2]="Gold";
			nums[2]=2;
		}
		
		if(ind==4)
		{
			if(bonus==0)
			item=new Armor("Splint Male");
			else
			item=Armor.createImprovedA("Splint Male",bonus);
			
			DC=30;
			num=4;
			items=new String[num];
			nums=new int[num];
			items[0]="Iron";
			nums[0]=5;
			items[1]="Silver";
			nums[1]=15;
			items[2]="Gold";
			nums[2]=10;
			items[3]="Copper";
			nums[3]=10;
		}
		
		if(ind==4)
		{
			if(bonus==0)
			item=new Armor("Half Plate");
			else
			item=Armor.createImprovedA("Half Plate",bonus);
			
			DC=35;
			num=3;
			items=new String[num];
			nums=new int[num];
			items[0]="Gold";
			nums[0]=15;
			items[1]="Silver";
			nums[1]=15;
			items[2]="Pebbles";
			nums[2]=5;
		}
		
	} //end armors
	
			
	if(bonus>0&&nums!=null)
	{
		for(int i=0;i<num;i++)
		nums[i]+=nums[i]*bonus;    //maybe need to change
		
		DC=DC+DC*bonus+bonus;
	}	
	
	if(num!=0)
	comps=true;
	else
	comps=false;
	
	for(int i=0;i<num;i++)
	if(!player.hasItem(items[i],nums[i]))
	comps=false;
	
	
	if(comps)
	{
	for(int i=0;i<num;i++)
	player.removeItem(items[i],nums[i]);
			
	String mes="";
	
	if(player.getSkills().check("Craft",DC))
	{
	player.addItem(item);
	mes="You've succeeded in crafting "+item.getAmount()+" "+item.getName()+".";
	}
	else
	mes="You've failed to craft the item.";
	
	mes=mes+" ("+(DC/3)+" turns have passed)";
	
	JOptionPane.showMessageDialog(main.getFrame(),mes,"Crafting...",JOptionPane.INFORMATION_MESSAGE);		
	
	
	
	//main.sendCPlace();	 elsewhere
	}			
	else
	{
		String mess="";
		for(int i=0;i<num;i++)
		if(i==0)
		mess+=nums[i]+" "+items[i];
		else
		mess+=", "+nums[i]+" "+items[i];
		
		main.setMes("You don't have enough components. ("+mess+")");
	}
	
	return DC;
	}
	
	
	public void load()
	{
		setDirs(true);
		attacks(true);
			
		if(battle!=null)		
		battle.setVisible(false);
		
		////System.out.println("battle false or null.");
	}
	
	
	public void ifAttacked()
	{
		//System.out.println("ifat "+player.attacked()+" "+north.isEnabled());
		if(north.isEnabled()&&player.attacked())
		{
			addBattle();
			
			//System.out.println("baymn");
			
			if(!battle.isVisible())
			{
				battle.setVisible(true);
				//System.out.println("batvis");
			}
			
		
		}
			
	}
	
	
	
	public void useTurns(int times)
	{
	
	//time passes:
	for(int i=0;i<times;i++)
	{
	int hun=player.addMoved();	
	if(hun>0)
	JOptionPane.showMessageDialog(main.getFrame(),"You are hungry.\nYou were damaged by "+hun+" points.","You need meals",JOptionPane.WARNING_MESSAGE);	
	
	int hp=main.getPlayer().hp()-main.getPlayer().getDam()-hun;
	
	if(hp<=-5)
	{
	main.getMap().dead();
	JOptionPane.showMessageDialog(main.getFrame(),"You died from hunger","Dead",JOptionPane.ERROR_MESSAGE);	
	}
	
	if(player.isDying())
	{
	main.getMap().waitFC(true);
	main.getMap().setDirs(false);
   	main.setMes("You are unconscious and are dying.");
   	checkDead();
   	}
		
	
	}
	
	while(player.getMoved()>=player.getSpeed())
	player.setMoved(player.getMoved()-player.getSpeed());	
	
	player.setHiding(0);
	
	player.getSpells().sleep();
	//etc. spells			
	
	main.sendCPlace();	
		
	}
	
	
	
	public String write() //put fighters in character?
	{
		String s="M*";
		
		//character already exists.
		
		Vector f=null;
		//int ii,jj;
		
		int x=-1,y=-1;
		if(player!=null)
		{
		x=player.getX();
		y=player.getY();
		}
				
		//////System.out.println(y+","+x);
		
		//here
		
		
		for(int i=0;i<dim;i++)
			for(int j=0;j<dim;j++)
			{
				s=s+map[i][j].write();
				
				if(i==y&&j==x)
				{
					f=map[i][j].getChars();
				}
			}
		
		
		//to here
		
						
		//fighters serials
		
		s+=";";
		
		//////System.out.println(y+","+x);
		
		//////System.out.println(f);
		//////System.out.println(fighters);
		
		if(f!=null&&fighters!=null)
		{
			for(int i=0;i<fighters.size();i++)
			{
			int a=f.indexOf(fighters.elementAt(i));
			s+=a+",";
			//////System.out.println(a+",");
			}
		}
		
		s+=";";
		
		
		//and nums
		
		
		s+=";";
		
		if(nums!=null)
		for(int i=0;i<nums.size();i++)
		{
			s+=(String)(nums.elementAt(i))+",";
		}
				
		
		s+=";";
		
		
		
		s+=order+",";
	
		return s+"*M";
	}
	
	
	//write and read of (i,j) [place]
	
	
	public void read(String s) //it is an update.
	{
	Map d=this;
	s=s.substring(2);
	
	//d.map=new Place[dim][dim]; //del
	
	//here
	
	
	for(int i=0;i<dim;i++)
		for(int j=0;j<dim;j++)
		{
		int b=s.indexOf("*P")+1;
		String p=s.substring(0,b);	
		map[i][j]=Place.read(p);
		
		//if(map[i][j].getName().equals("Town"))
		//////System.out.println("mnnmnm: "+((Char)map[i][j].getChars().elementAt(0)).info());
			
		s=s.substring(b+1);
		}
	
	
	
	//s=s.substring(1);
	
	//to here
	
	
	//fighters:
	Vector cs=null;
	if(player!=null)
	cs=map[player.getY()][player.getX()].getChars();
	
	
	//////System.out.println("\nssssss "+s);
	
	s=s.substring(1);
		
	//////System.out.println("s1: "+s);
	
	int b=s.indexOf(";");
	String f=s.substring(0,b);
	
	
	//////System.out.println("s: "+s);
	//////System.out.println("f: "+f);
	
	//////System.out.println("\nn "+b+"     "+s+"      "+f);
	
	
	//////System.out.println("\n\n"+s+"\n");
	//////System.out.println("s10 "+s);
	
	s=s.substring(b+1);
	
	d.fighters=new Vector();
	
	//////System.out.println("\n\ns "+s+"\nf "+f+"\n");
			
	while(f.indexOf(",")!=-1&&cs!=null)
	{
	b=f.indexOf(",");
	int a=Integer.parseInt(f.substring(0,b));
	d.fighters.add(cs.elementAt(a));
	f=f.substring(b+1);	
	}
	
	
	
	//////System.out.println("plz "+s);
	
	//nums:
	s=s.substring(1);
	
	//////System.out.println("s11 "+s);

	b=s.indexOf(";");
	f=s.substring(0,b);
	
	s=s.substring(b+1); //from b+1 and forward. 
	
	
	//////System.out.println("sssssssssss "+s);
	//////System.out.println("fffffffffff "+f);
	
	d.nums=new Vector();
	while(f.indexOf(",")!=-1)
	{
	b=f.indexOf(",");
	int a=Integer.parseInt(f.substring(0,b));
	d.nums.add(""+a);
	f=f.substring(b+1);	
	}
	
	b=s.indexOf(",");
	d.order=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	//////System.out.println("\n\norder: "+d.order+"\n"+s);
	
	//more efficient? (not needed? cause only once.)
			
	}
	
	
	public String removePlayer(Character pl)
	{
		
		//main.sendCPlace(player.getY()-y,player.getX()-x,add); :
		
		//////System.out.println("player: "+player);
		if(player==null)
		player=pl;
		
		Place p=map[player.getY()][player.getX()];
		int add=p.getChars().indexOf(player);
		p.getChars().remove(player);
		
		
		String s="PCC*";
		
		s+=player.getX()+",";
		s+=player.getY()+",";
		s+=add+",";
		s+=p.write();
		
		return s+="*PCC";
	}
	
	
	public void setPlayer(Character c) //not usable.
	{
		player=c;
		//map[player.getY()][player.getX()]=new Place("Town");
	}
	
	
	public void nisui() //not usable.
	{
	Vector cs=map[player.getY()][player.getX()].getChars();
	fighters=new Vector();
	nums=new Vector();
	//////System.out.println("v "+map[player.getY()][player.getY()].write());
	//////System.out.println(cs);
	
	fighters.add(cs.elementAt(0));
	fighters.add(cs.elementAt(1));
	nums.add("15");
	nums.add("20");
	}
	
}
