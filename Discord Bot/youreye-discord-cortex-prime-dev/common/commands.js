const lib = require('lib')({token: process.env.STDLIB_SECRET_TOKEN});

/*
event - context.params.event
messageContent - list of messages
options - {
  splitMessages - bool
  replyToMessage - bool (takes event.id)
  embed - embed object ({title, type, color, ... })
}
returns a list of discord messages
*/
async function sendMessage(event, messageContent, options = undefined) {  
  let content = messageContent.join('\n')
  let messages = [content]
  console.log(`Sent message: ` + content);
  if (options?.splitMessages) {
    messages = splitToSeparateMessages(messageContent)
  }
  
  let reference = undefined
  if (options?.replyToMessage) {
    reference = {
      message_id: event.id
    }
  }
  
  let discordMessages = []
  
  let shouldWait = false
  for (let index = 0; index < messages.length; index++) {
    let msg = messages[index]
    let lastMessageEmbedSummary = index < messages.length - 1 ? undefined : options?.embed
    console.log(`Send Message - size: ${msg.length}, summary: ${lastMessageEmbedSummary?.length ?? 0}`);
    let createInput = {
      channel_id: event.channel_id,
      content: msg,
      message_reference: reference,
      embed: lastMessageEmbedSummary,    
    }
    
    try {
      if(shouldWait) {
        await sleep(1000)
        shouldWait = false
      }
      let m = await lib.discord.channels['@0.3.0'].messages.create(createInput);
      discordMessages.push(m)
    } catch (error) {
      console.error(`Failed sending message normally: '${JSON.stringify(createInput)}'`, error);
      if(error.message.includes('You are being rate limited')) {
        shouldWait = true
        console.log(`trying again`);
        index = index - 1
        continue
      }
    }
    
  }  
  
  return discordMessages
}

// async function.
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


// Returns lists of strings, each one with max characters
// Reason: do avoid errors of "Must be 2000 or fewer in length."
function splitToSeparateMessages(messageContentList, joinChar = '\n', maxChars = 2000) {
  if (messageContentList.length == 0) {
    return ['']
  }
  let result = []
  
  let currentMessageContent = messageContentList[0]
  messageContentList.slice(1).forEach(msg => {
    let newMsg = currentMessageContent + joinChar + msg
    if(newMsg.length > maxChars) {
      result = result.concat([currentMessageContent])
      currentMessageContent = msg
    } else {
      currentMessageContent = newMsg 
    }
  });
  
  if (currentMessageContent.length > 0) {
    result = result.concat([currentMessageContent])
  }
   
  return result
}

// Should run at the start of every command handling, since token is valid for only 2 seconds:
// https://autocode.com/lib/discord/interactions/#responses-create
async function receivedCommand (event) {
  console.log(`Received command: ${JSON.stringify(event)}`);
  try {
    await lib.discord.interactions['@1.0.0'].responses.create({
      token: `${event.token}`,
      response_type: 'DEFERRED_CHANNEL_MESSAGE_WITH_SOURCE',
    });
  } catch(error) {
    console.error(`Failed receving command token '${event.token}'`, error);
    await sendMessage(event, ["Unexpected error when starting to handle command. Contact developer for more info."])
  }  
}

/*
event - context.params.event
messageContent - list of messages
void
*/
async function replyToCommand(event, messageContent) {
  let content = messageContent.join('\n')
  console.log(`Reply to command: ` + content);
  if (event.token == 'A_UNIQUE_TOKEN') {
    await lib.discord.channels['@0.0.6'].messages.create({
      channel_id: event.channel_id,
      content: 'DEBUG MSG: ' + content,
    })
    return
  } 
  
  try {
    await lib.discord.interactions['@1.0.0'].followups.create({
      token: `${event.token}`,
      content: content ,
    }); 
  } catch(error) {
    console.error(`Failed sending message to token '${event.token}'`, error);
    await sendMessage(event, ["Unexpected error when replying to command. Contact developer for more info."])
  }  
}


module.exports = {
  ReceivedCommand: receivedCommand ,
  ReplyToCommand: replyToCommand,
  SendMessage: sendMessage,
};