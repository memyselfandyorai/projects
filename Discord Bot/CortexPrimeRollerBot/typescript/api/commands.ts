import type { OutputMessage, MessageSender } from "./messages";
import type { PinManager } from "./pin_manager";

export interface CommandOption {
  name: string;
  value: string | number | boolean;
}

export interface CommandInteraction extends MessageSender {
  getIocContainer(): any;

  getAllInputOptions(): string[];
  inputOptions(...names: string[]): CommandOption[];
  getInputOptionsMap(): Map<string, string | number | boolean>;

  getMessageId(): string;
  // User name
  user(): string;
  getUserId(): string;
  getChannelId(): string;

  getPinManager(): PinManager;

  // "Defers" the reply to indicate Discord we're working on it. (otherwise the token is invalidated within 3 seconds)
  prepareCommandReply(): Promise<void>;
  // Edits the deffered reply.
  // Returns message ID
  reply(messages: OutputMessage): Promise<string>;
}
