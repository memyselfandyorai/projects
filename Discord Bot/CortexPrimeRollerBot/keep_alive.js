const http = require("http");
const marked = require("marked");
const fs = require("fs");

const SIMPLE_KEEP_ALIVE = false; // When false, renders the readme.

if (SIMPLE_KEEP_ALIVE) {
    createServer("I'm alive");
} else {
    fs.readFile("./README.md", "utf8", (err, markdown) => {
        if (err) {
            console.error(err);
        } else {
            const html = renderMarkdown(markdown);
            createServer(html);
            console.log("Keep alive is ready");
        }
    });
}


function renderMarkdown(markdown) {
  const options = {
    gfm: true, // Enable GitHub Flavored Markdown support
    breaks: true, // Enable automatic wrapping of long lines
    // ... other options
  };
  return marked.parse(markdown, options);
}

function createServer(html) {
  http
    .createServer((req, res) => {
      console.log("Rendering keep alive");
      res.write(html);
      res.end();
      console.log("Rendered keep alive");
    })
    .listen(8080);
}
