import type { OutputMessage, MessageSender } from "./messages";
import type { PinManager } from "./pin_manager";
export interface CommandOption {
    name: string;
    value: string | number | boolean;
}
export interface CommandInteraction extends MessageSender {
    getIocContainer(): any;
    getAllInputOptions(): string[];
    inputOptions(...names: string[]): CommandOption[];
    getInputOptionsMap(): Map<string, string | number | boolean>;
    getMessageId(): string;
    user(): string;
    getUserId(): string;
    getChannelId(): string;
    getPinManager(): PinManager;
    prepareCommandReply(): Promise<void>;
    reply(messages: OutputMessage): Promise<string>;
}
//# sourceMappingURL=commands.d.ts.map