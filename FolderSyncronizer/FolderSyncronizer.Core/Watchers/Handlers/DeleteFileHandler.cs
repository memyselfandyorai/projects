﻿using FolderSyncronizer.Core.Watchers.Actions;
using Infra.Framework.Extensions;
using Infra.Framework.Mef;
using System.Collections.Generic;
using System.IO;

namespace FolderSyncronizer.Core.Watchers.Handlers
{
    [ExportForMef(typeof(IWatcherHandler))]
    class DeleteFileHandler : IWatcherHandler
    {
        public WatcherChangeTypes SupportedType => WatcherChangeTypes.Deleted;

        public IEnumerable<IFileAction> HandleChange(string sourceFolder, string targetFolder, FileSystemEventArgs eventArgs)
        {
            string fileInSource = eventArgs.FullPath;
            string relative = Path.GetRelativePath(sourceFolder, fileInSource);
            return new DeleteAction(fileInSource, Path.Combine(targetFolder, relative)).ToSingleton();
        }
    }
}
