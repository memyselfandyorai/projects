package Objects;


import General.Intersection;
import General.NormalVector;
import General.Plane;
import General.Point;
import General.Point2D;
import General.Ray;
import General.Scene;
import General.Vector;

/**
 * 
 * represents a rectangle
 *
 */
public class Rectangle extends Surface {

	
	/*
	 * put the constant rec variables here as privates? time \ memory? 
	 * */
	
	private Point p0,p1,p2;

	/**
	 * default constructor
	 */
	public Rectangle() {
	}
	
	public Rectangle(Point p0, Point p1, Point p2) {
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
	}

	public Point getP0() {
		return p0;
	}

	public void setP0(Point p0) {
		this.p0 = p0;
	}

	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}

	
	private NormalVector getNormal(){
		NormalVector vec1 = NormalVector.subPoints(p1,p0);
		NormalVector vec2 = NormalVector.subPoints(p2,p0);
		NormalVector normal = vec1.crossProduct(vec2);
		return normal;
	}
	
		
	public Intersection intersect(Scene scene,Ray ray) {
		//Vector vec1 = Vector.subPoints(p1,p0);
		//Vector vec2 = Vector.subPoints(p2, p0);
		
		NormalVector normal = getNormal();
		Plane plane = new Plane (normal,p0);
		
		Point intersectPoint = plane.hit(ray);
		if (intersectPoint == Plane.MISS)
			return Intersection.MISS;
				
		//triangle1: p0,p1,p2 vs. intersectPoint
		if(!sameSide(intersectPoint,p2,p1,p0))
			return Intersection.MISS;
		
		//triangle2: p0,p2,p1 vs. intersectPoint
		if(!sameSide(intersectPoint,p1,p2,p0))
			return Intersection.MISS;
		
		Point p3 =  p1.add(Vector.subPoints(p2, p0).toPoint());  //(p2-p0)+p1

		//triangle3: p3,p2,p1 vs. intersectPoint
		if(!sameSide(intersectPoint,p1,p2,p3))
			return Intersection.MISS;
		
		//triangle4: p3,p1,p2 vs. intersectPoint
		if(!sameSide(intersectPoint,p2,p1,p3))
			return Intersection.MISS;		
		
		double dist = ray.getPoint().distance(intersectPoint);
		
		return new Intersection (dist, this, intersectPoint,ray,plane.getNormal(ray));
	}

		
	public boolean hasAllValues() {
		if (p0==null || p1==null || p2==null)
			return false;
		return true;
	}
	
	/**
	 * returns true iff the points are co-linear
	 * @return
	 */
	public boolean coLinear(){
		return Point.coLinear(p0, p1, p2);
	}
	
	
	/**
	 * returns true iff p1 and p2 are on the same side as ab
	 * */
	private boolean sameSide(Point p1,Point p2,Point a,Point b)
	{
		Vector ab=Vector.subPoints(b,a);
		Vector cp1=ab.crossProduct(Vector.subPoints(p1,a));
		Vector cp2=ab.crossProduct(Vector.subPoints(p2,a));
		
		return cp1.dotProduct(cp2)>=0;
	}
	

	public boolean isOnPlane(Point point){
		Vector vec = Vector.subPoints(p0, point);
		NormalVector normal = getNormal();
		double result = normal.dotProduct(vec);
		if(Math.abs(result) < 0.000001) //if result is 0 then point is on plane
			return true;
		else
			return false;
	}

	
	public Point2D parametrization(Point point) {
		Vector P1P0 = Vector.subPoints(p1, p0);
		Vector P2P0 = Vector.subPoints(p2, p0);
		Vector P1Point = Vector.subPoints(p1, point);
		Vector P2Point = Vector.subPoints(p2, point);
		double angle = P1Point.dotProduct(P1P0)/(P1Point.length() * P1P0.length());	
		double length_angle1 = P1Point.length() * angle;
		angle = P2Point.dotProduct(P2P0)/(P2Point.length() * P2P0.length());
		double length_angle2 = P2Point.length() * angle;
		double u = length_angle2 / P2P0.length();
		double v = length_angle1 / P1P0.length();
		
		return new Point2D(u,v);
	}
		
	
	public boolean onRight(Plane plane)
	{
		return plane.onRight(p0);
	}
	
	
	public String toString()
	{
		return "{"+p0+";"+p1+";"+p2+"}";
	}
	
}
