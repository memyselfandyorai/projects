import java.awt.*;
import javax.swing.*;
import java.util.*;

//make a work. "proffesion", a chance to gain money with no quests. (how?) [or not.]

//to extend to...? (quest npc) [more no reason npc's?]


//problem- weapon\armor... not in items! (in inventory only)


import java.io.*;

public class Char implements Serializable{
	
	//static final int delay=10;
	
	
	/**
	 *The name of the character
	 */
	protected String name;
	/**
	 *His health points
	 */
	protected int hp; //health points
	/**
	 *The damage that affects him
	 */
	protected int dam; //damage
	
	/**
	 *The strength of the character
	 */
	protected int str; //strength
	/**
	 *The dexterety of the character
	 */
	protected int dex; //dexterety
	/**
	 *The race of the character
	 */
	protected String race; //elf,human,dwarf,halfling
	/**
	 *If the character is male of female
	 */
	protected boolean male; //male\female
	
	/**
	 *The constitution of the character
	 */
	protected int con; //costitution
	/**
	 *The wisdom of the character
	 */
	protected int wis; //wisdom
	/**
	 *The ingelligence of the character
	 */
	protected int inl; //intelligence
	/**
	 *The charisma of the character
	 */
	protected int cha; //charisma
	/**
	 *The damage reduction of the character
	 */
	protected int damr; //damage reduction
	/**
	 *The natural armor of the character
	 */
	protected int nata; //natural armor
	/**
	 *The spell resistance of the character
	 */
	protected int sr; //spell resistance. 1d20+lvl>=sr (to spell to succeed)
	/**
	 *His money
	 */
	protected double gp; //money
	/**
	 *His weapon
	 */
	protected Weapon weapon;
	/**
	 *His armor
	 */
	protected Armor armor;
	/**
	 *His shield
	 */
	protected Shield shield;
	/**
	 *His items
	 */
	protected Vector items;
	/**
	 *His reflex,will and fortitude saves.
	 */
	protected int ref,will,fort; //reflex, will, fortitude saves. -fort?
	
	//protected int hiding; ?  or character only?
	/**
	 *His spells, abilities and disease
	 */
	protected Spells spells;		
	/**
	 *If he is in a fight
	 */
	protected boolean attacked;
	/**
	 *The characters he has seen
	 */
	protected Vector seen; //chars he sees
	/**
	 *The place he is in
	 */
	protected Place place;
	
	/**
	*Constructs a new Char
	*@param name : his name
	*@param hp : his health points
	*@param str : his strength
	*@param dex : his dexterety
	*@param race : his race
	*@param m : if he is male
	*@param co : his constitution
	*@param in : his intelligence
	*@param wi : his wisdom
	*@param ch : his charisma
	*/
	public Char(String name,int hp,int str,int dex,String race,boolean m,int co,int in,int wi,int ch)
	{
		gp=0;
		dam=0;
		this.name=name;
		this.hp=hp;
		this.str=str;
		this.dex=dex;
		this.race=race;
		items=new Vector();
		male=m;
		damr=0;
		attacked=false;
		nata=0;
		sr=0;
		spells=new Spells();
		
		ref=0;will=0;fort=0;
		//con=10;inl=10;wis=10;wis=10;
				
		inl=in;
		con=co;
		wis=wi;
		cha=ch;
		seen=new Vector();
		
	}
	
	/**
	*Constructs a new Char
	*@param name : his name
	*@param hp : his health points
	*@param str : his strength
	*@param dex : his dexterety
	*@param race : his race
	*@param m : if he is male
	*@param nat : his natural armor
	*/
	public Char(String name,int hp,int str,int dex,String race,boolean m,int nat)
	{
		gp=0;
		dam=0;
		this.name=name;
		this.hp=hp;
		this.str=str;
		this.dex=dex;
		this.race=race;
		items=new Vector();
		male=m;
		damr=0;
		attacked=false;
		nata=nat;
		sr=0;
		spells=new Spells(this);
		
		seen=new Vector();
		
		//change: ?
		ref=0;will=0;fort=0;con=10;inl=10;wis=10;cha=10;
	}
	
	
	public Char()
	{
	seen=new Vector();
	dam=0;
	damr=0;
	items=new Vector();
	attacked=false;
	nata=0;
	sr=0;
	spells=new Spells(this);
	}
	
	public Char(Place plc)
	{
	seen=new Vector();
	dam=0;
	damr=0;
	items=new Vector();
	attacked=false;
	nata=0;
	sr=0;
	spells=new Spells(this);
	place=plc;
	}
	
	/**
	*Constructs a new Char
	*@param n : his name (type of character)
	*@param plc : his current place
	*/
	public Char(String n,Place plc)
	{
	seen=new Vector();
	name=n;
	dam=0; //do one wounded?
	damr=0;
	items=new Vector();
	attacked=false;
	nata=0;
	sr=0;
	spells=new Spells(this);
	place=plc;
	race="";
	//ref=0;will=0;fort=0;con=10;inl=10;wis=10;cha=10;//change in all the others:
	
	int r=Dice.roll(2);
	if(r==1)
	male=true; //+1 persuade against opposite sex. (or +2?) or bonus to another thing (-1 hide, +1 pickp? dc smaller? could be.)
	else
	male=false;
	
		if(n.equals("Human Civilian"))
		{
			hp=6;
			str=10;
			dex=9;
			race="Human";
			gp=Dice.roll(4,5);
			con=13;
			inl=11;
			wis=10;
			cha=11;
			ref=0;
			will=2;
			fort=1;
			
			
			
			if(Dice.roll(3)==1)
			{
			weapon=new Weapon("Dagger");
			items.add(weapon);
			}
						
			
			if(Dice.roll(4)==1)
			{
			shield=new Shield("Leather Shield");
			items.add(shield);
			}		
		}
		
		if(n.equals("Dwarven Civilian"))
		{
			hp=4;
			str=11;
			dex=8;
			race="Dwarf";
			con=14;
			inl=9;
			wis=10;
			cha=9;
			ref=0;
			will=1;
			fort=3;
			
			if(Dice.roll(3)==1)
			{
			weapon=new Weapon("Handaxe");
			items.add(weapon);
			}
			
			gp=Dice.roll(5,5,4);
		}
		
		if(n.equals("Elven Civilian"))
		{
			hp=5;
			str=9;
			dex=11;
			race="Elf";
			con=9;
			inl=12;
			wis=13;
			cha=12;
			ref=2;
			will=1;
			fort=0;
			
			if(Dice.roll(3)==1)
			{
			weapon=new Weapon("Short Bow");
			items.add(weapon);
			items.add(new Item("Arrows"));
			}
			
			gp=Dice.roll(3,4,2);
		}
		
		
		if(n.equals("Halfling Civilian"))
		{
			hp=4;
			str=8;
			dex=12;
			race="Halfling";
			con=9;
			inl=11;
			wis=11;
			cha=10;
			ref=3;
			will=0;
			fort=0;
			
			if(Dice.roll(3)==1)
			{
			weapon=new Weapon("Short Sword");
			items.add(weapon);
			}
			
			gp=Dice.roll(2,6,5);
		}
	
	if(n.lastIndexOf("Civilian")!=-1)
	{
	if(Dice.roll(4)==1)
	items.add(new Item("Flint and Steel"));
	if(Dice.roll(4)==1)
	items.add(new Item("Wood"));
	
	gp=(double)gp/100;
	}
	
	
		if(n.equals("Guardian"))
		{
		hp=10+Dice.roll(2,6);
		str=10+Dice.roll(2,3);
		dex=10+Dice.roll(5);
		gp=(double)Dice.roll(6,5,10)/20;
		
		con=10+Dice.roll(1,6);
		inl=10+Dice.roll(1,4);
		wis=10+Dice.roll(1,3);
		cha=10+Dice.roll(1,2);
		ref=Dice.roll(1,6);
		will=Dice.roll(1,4);
		fort=Dice.roll(2,2);
		
		r=Dice.roll(10);
		if(r>=1&&r<=4)
		race="Human";
		if(r>=5&&r<=6)
		race="Elf";
		if(r>=7&&r<=9)
		race="Dwarf";
		if(r==10)
		race="Halfling";
		
		//name=name+" ("+race+")";
		
		if(Dice.roll(100)>90)
		items.add(Item.getRing());
		
		weapon=Weapon.getWeaponS();
		items.add(weapon);
			
		if(Dice.roll(50)>30) 
		shield=Shield.getShield();
		
		//shield=Shield.create("Steel Shield",this);// del
		//weapon=Weapon.create("Longsword",this); //del
		}
	
	
		if(n.equals("Royal Guard"))
		{
		hp=18+Dice.roll(3,4);
		str=12+Dice.roll(3,2);
		dex=12+Dice.roll(4);
		gp=(double)Dice.roll(10,5,10)/20;
		con=10+Dice.roll(2,4);
		inl=10+Dice.roll(2,3);
		wis=10+Dice.roll(2,2);
		cha=10+Dice.roll(2,2);
		ref=Dice.roll(2,3);
		will=Dice.roll(2,3);
		fort=Dice.roll(2,4);
		
		r=Dice.roll(10);
		if(r>=1&&r<=4)
		{
		race="Human";
		gp+=5;
		}
		if(r>=5&&r<=7)
		{
		race="Dwarf";
		gp+=10;
		}
		if(r>=8&&r<=9)
		{
		race="Elf";
		gp-=5;
		}
		if(r==10)
		{
		race="Halfling";
		gp+=Dice.roll(12);
		}
				
		//name=name+" ("+race+")";
		
		for(int i=0;i<2;i++)
		if(Dice.roll(100)>=90)
		items.add(Item.getRing());
		
		weapon=Weapon.getWeaponM();
		items.add(weapon);
		
		if(weapon.isRanged())
		items.add(new Item(weapon.kind()));
		
		if(Dice.roll(50)>25) 
		shield=Shield.getShield();
		
		if(shield!=null)
		if((weapon.getName().lastIndexOf("bow")!=-1||weapon.getName().lastIndexOf("Bow")!=-1||weapon.getName().lastIndexOf("Great")!=-1)&&!shield.getName().equals("Buckler"))
		shield=null;
		
		
		if(Dice.roll(70)>40)
		armor=Armor.getArmor();
		}
	
	
		////System.out.println(n);
		//if(n.indexOf("Roy")!=-1)
		////System.out.println(n+"\n");
		
		if(n.equals("Royalty"))
		{
		
		int roy=Dice.roll(10);
		if(roy>=1&&roy<=3)
		{
		name="Princess";
		male=false;
		}
		if(roy>=4&&roy<=6)
		{
		name="Prince";
		male=true;
		}	
		if(roy>=7&&roy<=8)
		{
		name="Queen";
		male=false;
		}
		if(roy>=9&&roy<=10)
		{
		name="King";
		male=true;
		}
		
		hp=15+roy*3;
		str=10+roy/2;
		dex=12+Dice.roll(roy);
		con=8+roy;
		inl=13+roy/2;
		wis=15+roy/2;
		cha=12+Dice.roll(roy);
		ref=roy+Dice.roll(4);
		will=roy;
		fort=roy;
				
		gp=Dice.roll(10,12,20);
		
		r=Dice.roll(10);
		if(r>=1&&r<=4)
		{
		race="Human";
		gp+=10;
		}
		if(r>=5&&r<=7)
		{
		race="Dwarf";
		gp+=15;
		}
		if(r>=8&&r<=9)
		{
		race="Elf";
		gp-=15;
		}
		if(r==10)
		{
		race="Halfling";
		gp+=Dice.roll(2,12);
		}
		
		gp/=10;
			
		name=race+" "+name;
		
		if(roy>6)
		{
		if(Dice.roll(2)==1)
		spells.add("Lightning Bolt",3,1,0,1); //(lvl)d6
		else
		spells.add("Magic Missile",1,2,0,1); //1d4+1 damage. [(lvl+1)\2]d4+(lvl+1)\2 :max of 5
		}
		
		for(int i=0;i<4;i++)
		if(Dice.roll(100)>=50)
		items.add(Item.getRing());
		
		if(male)
		{
		fort+=3;	
		str+=2;
		weapon=Weapon.getWeaponM();
		items.add(weapon);
		}
		else
		{
		cha+=2;
		ref+=3;	
		weapon=Weapon.getWeaponS();
		items.add(weapon);
		}
		
		////System.out.println("name: "+name);
				
		if(weapon.isRanged())
		items.add(new Item(weapon.kind()));
		
		
		if(Dice.roll(100)>40)
		shield=Shield.getShield();
		
		if(shield!=null)
		if((weapon.getName().lastIndexOf("bow")!=-1||weapon.getName().lastIndexOf("Bow")!=-1||weapon.getName().lastIndexOf("Great")!=-1)&&!shield.getName().equals("Buckler"))
		shield=null;
		
		
		if(Dice.roll(100)>60)
		armor=Armor.getArmor();
		}
		
		
		if(n.equals("Wizard"))
		{
		hp=4*Dice.roll(2,3);
		str=7+Dice.roll(3);
		dex=9+Dice.roll(3);
		con=11+Dice.roll(3);
		inl=13+Dice.roll(4);
		wis=14+Dice.roll(3);
		cha=12+Dice.roll(3);
		ref=Dice.roll(4)-1;
		will=Dice.roll(3)*3;
		fort=Dice.roll(3)-1;
				
		gp=Dice.roll(2,6);
		
		gp/=10;
		
		r=Dice.roll(10);
		if(r>=1&&r<=4)
		{
		race="Human";
		}
		if(r>=5&&r<=6)
		{
		race="Dwarf";
		}
		if(r>=7&&r<=9)
		{
		race="Elf";
		}
		if(r==10)
		{
		race="Halfling";
		}
		

		if(Dice.roll(5)>2)
		spells.add("Lightning Bolt",3,1,0,1);
		else
		spells.add("Magic Missile",1,2,0,1);
		
		
		if(Dice.roll(2)==1)
		spells.add("Poison",4,3,4,2); //1d6 damage.
		else
		spells.add("Sleep",5,3,Dice.roll(2,4));
				
		if(Dice.roll(100)>=65)
		items.add(Item.getRing());
				
		weapon=Weapon.getWeaponS();
		items.add(weapon);
							
		if(weapon.isRanged())
		items.add(new Item(weapon.kind()));
						
		}//wizard
		
		
		
		
		if(n.equals("Noble"))
		{
			hp=8+Dice.roll(2,10);
			str=11+Dice.roll(3);
			dex=10+Dice.roll(2);
			con=10+Dice.roll(1,4);
			inl=10+Dice.roll(2,2);
			wis=10+Dice.roll(1,2);
			cha=10+Dice.roll(1,4);
			ref=Dice.roll(1,2);
			will=Dice.roll(2,2);
			fort=Dice.roll(1,3);
			
			r=Dice.roll(6);
			if(r==1||r==2||r==6)
			race="Human";
			if(r==3)
			race="Elf";
			if(r==4)
			race="Halfling";
			if(r==5)
			race="Dwarf";
			
			
			
			if(Dice.roll(3)<3)
			{
				int w=Dice.roll(4);
				if(w==1)
				weapon=new Weapon("Short Sword");
				if(w==2)
				weapon=new Weapon("Longsword");
				if(w==3)
				weapon=new Weapon("Dagger");
				if(w==4)
				weapon=new Weapon("Handaxe");
			
			
			items.add(weapon);
			}
			
			gp=(double)Dice.roll(10,4,10)/15;
			
		
			if(Dice.roll(2)==1)
			items.add(Item.getRing());
			
			if(Dice.roll(10)<3)
			items.add(Item.getRing());
				
		}
		
		
		if(n.equals("Healer"))
		{
			hp=6+Dice.roll(2,4);
			str=6+Dice.roll(6);
			dex=8+Dice.roll(4);
			con=9+Dice.roll(2,2);
			inl=11+Dice.roll(2,2);
			wis=12+Dice.roll(2,3);
			cha=10+Dice.roll(1,6);
			ref=Dice.roll(1,2);
			will=Dice.roll(2,4);
			fort=Dice.roll(2,3);
			
			r=Dice.roll(10);
			if(r>=1&&r<=3)
			race="Human";
			if(r>=4&&r<=6)
			race="Elf";
			if(r==7)
			race="Halfling";
			if(r>=8&&r<=10)
			race="Dwarf";
			
			
			
			if(Dice.roll(3)>1)
			{
				int w=Dice.roll(2);
				if(w==1)
				weapon=new Weapon("Short Sword");
				else
				weapon=new Weapon("Dagger");
			
			items.add(weapon);
			}
			
			gp=(double)Dice.roll(1,6,2)/10;
			
		
			if(Dice.roll(10)>8)
			items.add(Item.getRing());
			
			
			items.add(new Item("Healer's Kit"));
			items.add(new Item("Meals"));
									
		}
		
	
	
	if(weapon!=null)
	{
	//items.add(weapon);
	weapon.setOwner(this);
	}
	else
	weapon=Weapon.create("Unarmed Strike",this);
	
	if(shield!=null)
	if(weapon.isRanged()&&!shield.getName().equals("Buckler"))
	shield=null;
	
	if(shield!=null)
	{
	shield.setOwner(this);
	items.add(shield);
	}
	
	if(armor!=null)
	{
	armor.setOwner(this);
	items.add(armor);
	}
	
	if(weapon.isRanged())
	items.add(new Item(weapon.kind()));
	
	
	}
	
	
	/**
	*Computes and returns value of the attribute's modifier
	*@param a : the attribute
	*/
	static public int mod(int a)//modifier
	{
		if(a-10>=0)
		return (int)((a-10)/2);
		else
			if((a-10)%2==0)
			return (int)((a-10)/2);
			else
			return (int)((a-10)/2-1);
	}
	
	/**
	*Computes and returns the total value of the attack
	*/
	public int attack() //to change.    +if changes here, change attack(weapon)
	{
	int s;
	
	s=mod(getSTR());
		if(!weapon.getName().equals("Unarmed Strike"))
		{
			if(!(weapon.kind()).equals(""))
			s=mod(getDEX());
		
		s+=weapon.getAB();
		
		
		if(race.equals("Elf")&&(weapon.getName().equals("Short Bow")||weapon.getName().equals("Longbow")))
		s++;
		if(race.equals("Dwarf")&&(weapon.getName().equals("Handaxe")||weapon.getName().equals("Battleaxe")))
		s++;
		if(race.equals("Halfling")&&(weapon.getName().equals("Short Bow")||weapon.getName().equals("Short Sword"))) //not dagger
		s++;
		}
		else
		{
			s-=2; //penalty on unarmed. no proficient. -to exclude monk?
			if(this instanceof Character)
			if(((Character)(this)).getClas().equals("Monk"))
			s+=2;
		}
	
	int a=Item.howMany(items,"Ring of Attack");
	if(a>3)
	a=3;
	
	s+=a;
	
	if(race.equals("Dwarf")||race.equals("Halfling")) //+1 attack bonus
	s++;
	
	return s;
	}
	
	
	
	public int attack(Weapon weapon)
	{
	int s;
	s=mod(getSTR());
		if(!weapon.getName().equals("Unarmed Strike"))
		{
			if(!(weapon.kind()).equals(""))
			s=mod(getDEX());
		s+=weapon.getAB();
		if(race.equals("Elf")&&(weapon.getName().equals("Short Bow")||weapon.getName().equals("Longbow")))
		s++;
		if(race.equals("Dwarf")&&(weapon.getName().equals("Handaxe")||weapon.getName().equals("Battleaxe")))
		s++;
		if(race.equals("Halfling")&&(weapon.getName().equals("Short Bow")||weapon.getName().equals("Short Sword"))) //not dagger
		s++;
		}
		else
		{
			s-=2; //penalty on unarmed. no proficient. -to exclude monk?
			if(this instanceof Character)
			if(((Character)(this)).getClas().equals("Monk"))
			s+=2;
		}
		
	int a=Item.howMany(items,"Ring of Attack");
	if(a>3)
	a=3;
	s+=a;
	if(race.equals("Dwarf")||race.equals("Halfling")) //+1 attack bonus
	s++;
	return s;
	}
	
	
	/**
	 *Computes and returns the total value of the damage
	 */
	public int damage()
	{
		int dam=0;
		
		int a=Item.howMany(items,"Ring of Damage");
		if(a>3)
		a=3;
		
		/*
		if(race.equals("Dwarf")||race.equals("Halfling"))
		dam=Dice.roll(1,2,mod(str)+a);
		else
		dam= Dice.roll(1,3,mod(str)+a);
		*/
		//if(weapon!=null)
		dam=weapon.damage();
	
		
		return dam+a;
		
	}
	
	/*
	 *returns str
	 */
	public int getSTR()
	{
		int s=str;
		
		int a=Item.howMany(items,"Ring of Strength");
		if(a>5)
		a=5;
		
		s+=a;
		
		return s;
	}
	
	public int getDEX()
	{
		
		////System.out.println("dex(dex): "+dex);
		
		
		int s=dex;
		////System.out.println("dex: "+dex+"     "+s);
		
		int a=Item.howMany(items,"Ring of Dexterety"); //agility?
		if(a>5)
		a=5;
		
		s+=a;
		
		////System.out.println("dex: "+dex+"     "+s);
		
		return s;
	}
	
	public int getSr()
	{
	return sr;	
	}
	
	
	public Vector getItems()
	{
		return items;
	}
	/**
	*Computes the character's armor class
	*/
	public int AC() //"Dwarf","Elf",etc
	{
		int s=10;
		
		if(race.equals("Dwarf")||race.equals("Halfling"))
		s++;
		
		int d=dex();
		//if(armor!=null)
		//	if(d>armor.getMaxD())
		//	d=armor.getMaxD();
			
		if(!spells.canPlay().equals(""))
		d=0;	
		
		s+=d; //max dexterety bonus applies here.
		
		s+=nata;
					
		int hb=0; //human bonus
		
		if(armor!=null)
		{
			s+=armor.getProtection();
			if(armor.humanBonus())
			hb=2;
		}
		
		if(shield!=null)
		{
			s+=shield.getProtection();
			if(shield.humanBonus())
			hb=2;
		}
		
		s+=hb;
					
		return s;
	}
		
	
	
	public void addDex(int a)
	{
		////System.out.println("dex (set): "+dex);
		dex=dex+a;	
		////System.out.println("dex (set2): "+dex);
	}

	public void addStr(int a)
	{
		str=str+a;	
	}

	public void addCon(int a)
	{
		con=con+a;	
	}
	
	public void addCha(int a)
	{
		cha=cha+a;	
	}
	
	public void addWis(int a)
	{
		wis=wis+a;	
	}
	
	public void addInt(int a)
	{
		inl=inl+a;	
	}
	
	public int dex()
	{
		int d=mod(getDEX());
		//if(armor!=null)
		//	if(d>armor.getMaxD())
		//	d=armor.getMaxD();
		
		int p=getMax();
		
		if(d>p)
		d=p;
		
		return d;
	}
	
	
	public int str()
	{
		return mod(getSTR());
	}
	
	
	public int getMax() //weight penalty and more
	{
	double w=0;
	Vector it=getItems();
	for(int i=0;i<it.size();i++)
	{
	Item item=(Item)(it.elementAt(i));
	w+=item.getWeight();
	}
	
	int s=getSTR()+1;
	int m=s*3;
	int h=s*6;
		
	int max=0;
	
	if(w>m&&w<=h)
	max=3;
	if(w>h)
	max=1;
	
	
	int p=0;
	if(getArmor()!=null)
	p=getArmor().getMaxD();
	
	
	if(p>max)
	max=p;
		
	return max;
	}
	
	
	
	public int getDamr()
	{
	int d=damr;
	
	int a=Item.howMany(items,"Ring of Damage Reduction"); 
	if(a>4)
	a=4;
	d+=a;
	
	return d;
	}
	
	
	public int getDam()
	{
	return dam;	
	}
	
	public int getHp()
	{
	int h=hp;
	
	int a=Item.howMany(items,"Ring of Health"); 
	if(a>8)
	a=8;
	h+=a;
	
	return h;
	}
	
	
	
	public int natural()
	{
	int n=nata;
	
	int a=Item.howMany(items,"Ring of Natural Armor");
	if(a>4)
	a=4;
	n+=a;
	
	return n;
	}
	
	public int getRes()
	{
	int r=sr;
	
	int a=Item.howMany(items,"Ring of Spell Resistance");
	if(a>5)
	a=5;
	r+=a;
	
	return r;
	}

	/**
	*Adds an item to the character
	*@param item : the item
	*/
	public void addItem(Item item) //bugs?
	{
	if(item.getClass().toString().equals("class Item"))
	{			
		boolean b=true;
		
		for(int i=0;i<items.size();i++)
		{
		Item m=(Item)(items.elementAt(i));
			if(item.getName().equals(m.getName()))
			{
				m.setAmount(m.getAmount()+item.getAmount());
				b=false;
			}
		}
		
		if(b)
		items.add(item);
	}
	else
	{
	if(item instanceof Weapon)
	((Weapon)(item)).setOwner(this);
	if(item instanceof Shield)
	((Shield)(item)).setOwner(this);
	if(item instanceof Armor)
	((Armor)(item)).setOwner(this);
	
	items.add(item);
	}
	
	if(this instanceof Character)
	getSpells().updateSpells(); //efficient?		
	}
	
	public void removeItem(String n,int m) //bugs?
	{
	for(int i=0;i<items.size();i++)
	{
		Item t=(Item)(items.elementAt(i));
		if(n.equals(t.getName()))	
		{
			t.setAmount(t.getAmount()-m);
			if(t.getAmount()<=0)
			items.remove(t);
			
			i=items.size();
			
			
			if(t instanceof Weapon)
			((Weapon)(t)).setOwner(null);
			if(t instanceof Shield)
			((Shield)(t)).setOwner(null);
			if(t instanceof Armor)
			((Armor)(t)).setOwner(null);
		}
	}
	
	if(this instanceof Character)
	getSpells().updateSpells(); //efficient?		
	}
	
	/**
	*Removes an item from the character
	*@param m : the item
	*/
	public void removeItem(Item m)
	{
	items.remove(m);
	if(this instanceof Character)
	getSpells().updateSpells(); //efficient?			
	}
	
	public String race()
	{
		return race;
	}
	
	public String name()
	{
		return name;
	}
	
	public double gp()
	{
		return gp;
	}
	
	public void addMoney(double a)
	{
	gp=gp+a;	
	}
	
	
	public Spells getSpells()
	{
	return spells;	
	}
	
	
	public void setWeapon(Weapon w)
	{
	weapon=w;	
	}
	
	public void setShield(Shield s)
	{
	shield=s;	
	}
	
	public void setArmor(Armor a)
	{
	armor=a;	
	}
	
	
	public boolean male()
	{
	return male;	
	}
	
	public String gender()
	{
	if(male)
	return "him";
	return "her";
	}
	
	public int getRef()
	{
		int s=ref;
		
		s+=dex();
		
		int a=Item.howMany(items,"Ring of Reflexes");
		if(a>4)
		a=4;
		
		s+=a;
		
		return s;
	}

	
	public int getFort()
	{
		int s=fort;
		
		s+=con();
		
		int a=Item.howMany(items,"Ring of Fortitude");
		if(a>4)
		a=4;
		
		s+=a;
					
		return s;
	}
	
	
	public int getWill()
	{
		int s=will;
		
		s+=wis();
		
		int a=Item.howMany(items,"Ring of Will Power");
		if(a>4)
		a=4;
		
		s+=a;
						
		return s;
	}
	
	
	
	public int getCON()
	{
		int s=con;
		
		int a=Item.howMany(items,"Ring of Health");
		if(a>5)
		a=5;
		
		s+=a;
		
		return s;
		
	}
	
	public int getINT()
	{
		int s=inl;
		
		int a=Item.howMany(items,"Ring of Intelligence");
		if(a>5)
		a=5;
		
		s+=a;
		
		return s;
		
	}
	
	public int getWIS()
	{
		int s=wis;
		
		int a=Item.howMany(items,"Ring of Wisdom");
		if(a>5)
		a=5;
		
		s+=a;
		
		return s;
		
	}
	
	public int getCHA()
	{
		int s=cha;
		
		int a=Item.howMany(items,"Ring of Charisma");
		if(a>5)
		a=5;
		
		s+=a;
		
		return s;
		
	}
	
	public int con()
	{
		return Char.mod(getCON());
	}
	
	public int cha()
	{
		return Char.mod(getCHA());
	}
	
	public int inl()
	{
	 return Char.mod(getINT());	
	}
	
	public int wis()
	{
		return Char.mod(getWIS());	
	}
	
	
	public boolean hasW()
	{
	if(weapon!=null)
	return true;
	return false;	
	}
	
	public Weapon getWeapon()
	{
	return weapon;	
	}

	public Shield getShield()
	{
	return shield;	
	}
	
	public Armor getArmor()
	{
	return armor;	
	}
	
	//drop item
	
	
	//	return(Dice damage);
	
	/*public static int attack(Char a)
	{
		
	}*/
	
	
	
	public int initiative()
	{
		int s=dex();
				
		int a=Item.howMany(items,"Ring of Initiation"); //another thing to this?
		if(a>4)
		a=4;
		
		s+=a;
						
		return Dice.roll(20)+s; //or with no dice?
		
	}
	
	
	public Place getPlace()
	{
	return place;	
	}
	
	public void attacked(boolean d)
	{
	attacked=d;	
	}
	
	public boolean attacked()
	{
	return attacked;	
	}
	
	public int calXP() //calculate xp
	{
	int xp=0;
	
	int m=modAv();
	
	xp=hp*hp*4+m*m;
	
		if(this instanceof Enemy)
		{
		Enemy e=(Enemy)(this);
		xp=(int)(e.getLvl()*e.getLvl()*100)+hp*m;
		}
		
		if(this instanceof Character)
		{
		Character c=(Character)(this);	
		xp=c.lvl()*c.lvl()*100+hp*m;
		}
				
	return xp;
	}
	
	public int modAv()
	{
	int a=getWIS()+getINT()+getSTR()+getCHA()+getCON()+getDEX();
		
	return (int)(a/6);
	}
	
	
	/**
	*Returns a vector with the order of fighters by their initiation
	*@param v : the fighters
	*@param n : their initiation (usually blank at start)
	*/
	public static Vector initiation(Vector v,Vector n)
	{
	Vector f=new Vector();	
	//Vector n=new Vector();
	//n=new Vector();
	
	//int chs=v.size();
	
	for(int i=0;i<v.size();i++)
	{
	Char c=(Char)(v.elementAt(i));
	c.attacked(true);
	
	int m=c.initiative();
	boolean b=true;
	
		for(int j=0;j<n.size()&&b;j++)
		{
			int g=Integer.parseInt((String)(n.elementAt(j)));
			
			
			////System.out.println("m: "+m+"  g: "+g);
			
			if(m>g)
			{
			f.add(j,c);
			b=false;
			}
			
			//if(!b)
			////System.out.println("i (false) : "+i);
			
			if(m==g)
			{
				int d=((Char)(f.elementAt(j))).getDEX();
				int cd=c.getDEX();
				if(cd>d)
				{
				f.add(j,c);
				b=false;
				}
				else
					if(cd==d)
						if(Dice.roll(2)==1)
						{
						f.add(j,c);
						b=false;
						}	
			}
			
			//if(!b)
			////System.out.println("i (false) : "+i); //!shoud not be twice
			
		}
	
	
	if(b)
	{
	f.add(c);
	//v.add(j,c);
	//n.add(j,""+m);
	}
	
	n.add(f.indexOf(c),""+m);
	
	
	////System.out.println("\n"+v+" inside \n");
	
	}
	
	////System.out.println("chars: "+v+"\ninit: "+f+"\nnums: "+n);
	
	return f;
	}
	
	
	
	public Vector getWeapons()
	{
		Vector v=new Vector();
		for(int i=0;i<items.size();i++)
		{
			Item c=(Item)(items.elementAt(i));
			if(c instanceof Weapon)
			v.add(c);
		}
	return v;
	}
	
	
	public boolean save(String get,int dc)
	{
	int bon=0;
	if(get.equals("Reflex"))
	bon=getRef();	
	if(get.equals("Fortitude"))
	bon=getFort();	
	if(get.equals("Will"))
	bon=getWill();	
	
	int d=Dice.roll(20);
	int dd=d+bon;
	
	if((dd>=dc&&d!=1)||d==20)
	return true;
	
	return false;	
	}
	
	/**
	*Attacks another character and returns the feedback of the attack
	*@param enm : the other character
	*/
	public String attack(Char enm)
	{
	String s="",xt="";
	int op=2+spells.getNum(); //number of options.
	int d=Dice.roll(op);//1 to (1+n)
	
	//op=1; //del
	////System.out.println("attack? ");
	
			
	if(((!weapon.isRanged())||(weapon.isRanged()&&Item.howMany(items,weapon.kind())>0))&&(d==op||d==op-1))
	{	
		
		
		String dis=spells.addTurn();
		
		if(dis.indexOf("died")!=-1||dis.indexOf("dying")!=-1)
		return dis;
	
		String pl=spells.canPlay();
		if(!pl.equals(""))
		return name+" cannot make a move because he is affetced by "+pl+"."+dis;
		
		
		//if(d==op) //weapon attack.
		//{
		int ac=enm.AC();
		int dice=Dice.roll(20);
		int at=attack()+dice;
		
		String def=""; //deflect arrow
		
		////System.out.println("attack: "+at+" ("+attack()+"+"+dice+") , ac: "+ac); //nim.
		if(weapon.isRanged())
		{
		removeItem(weapon.kind(),1);
			if(enm.getSpells().hasDisease("Wind"))
			dice=1;
			
			if((at>=ac&&dice!=1)||dice==20)
			if(enm instanceof Character)
			{
			Character enmy=(Character)(enm);	
				if(enmy.getClas().equals("Monk"))
				if(enmy.lvl()>=2)
				{
				boolean shi=(shield==null);
				if(!shi)
				shi=shield.getName().equals("Buckler"); //no buckler, =false
				
				boolean wea=weapon.getName().equals("Unarmed Strike");
				
				boolean free; //one hand free
				
				if(wea)
				free=true;
				else
					if(shi)
					free=weapon.getName().indexOf("Great")==-1&&!(weapon.isRanged()&&!weapon.getName().equals("Sling"));
					else
					free=false;
								
				if(free)
				{
				boolean save=enmy.save("Reflex",20+weapon.getMagic());
					if(save)
					{
					String k=weapon.kind();
					k=k.substring(0,k.length()-1);
					dice=1;
					def=enmy.name()+" has deflected the "+k;
					}
				}
				//one hand free. check longbow etc.
				//20+magical bonus: vs. reflex.
				} //lvl 2
						
			} //character
			
		} //ranged
				
			if((at>=ac&&dice!=1)||dice==20)
			{
			int dam=damage();
			int crit=weapon.critDam(dice,ac);
			dam+=crit;
			if(crit>0)
			xt="(Critial hit!) ";
									
			dam-=enm.getDamr();
			
				if(dam<0)
				dam=0;
			
			String rogue="";
			if(enm.getHp()-enm.getDam()-dam<=0)
				if(enm instanceof Character)
				{
				Character enmy=(Character)(enm);
					if(enmy.getClas().equals("Rogue"))
					if(enmy.lvl()>=12)
					{
						Spells sp=enmy.getSpells();
						int i=sp.serial("Defensive Roll");
						
						if(sp.getUsed(i)<sp.getUses(i))
						if(save("Reflex",dam))
						{
						dam=dam/2;
						rogue=" (half damage taken because of the Rogue's Defensive Roll skill) ";
						}
					}
				}
			
			enm.dealDam(dam);
			String kill="";
				if(enm.isDead())
				{
				String m;
				if(enm.male())
				m="him";
				else
				m="her";
				kill=" and kills "+m;
				}								
			String porp=" [20 on dice]";
			if(dice!=20)
			porp=" ["+at+">="+ac+"]";
			s=xt+name+" attacks with "+weapon.getName()+" and deals "+dam+rogue+" damage to "+enm.name+kill+porp;
			}	
			else
			{
			String porp=" [1 on dice or Wind]";
			if(!def.equals(""))
			porp=" ["+def+"]";
			
			if(dice!=1)
			porp=" ["+at+"<"+ac+"]";
			s=name+" attacks with "+weapon.getName()+" and misses "+enm.name+porp;
				if(Dice.roll(100)>50&&weapon.isRanged())
				place.addItem(Item.getItem(weapon.kind(),1));
			}
		////System.out.println("enm: "+s);
		
		//System.out.println("attacking (f)... s: "+s);
		//place.sendCAttack(this);
		
		return s+"."+dis;
		//}
	
	}
	else
	{
		//if(d!=op)
		//return attack(enm);
		//d=op-1);
		//else
		
		if(d==op||d==op-1)
		{
		weapon=Weapon.create("Unarmed Strike",this);	
		////System.out.println("enm2: ");
		
		//place.sendCAttack(this);
		
		return attack(enm);
		}
		
	}
	
	
	////System.out.println("enm3: ");
	if(spells.getUses(d-1)>spells.getUsed(d-1)||spells.getUses(d-1)==-1)
	return attack(d-1,enm);
	else
	return attack(enm);
	} //end attack
	
	/**
	*Uses a spell and returns the feedback of the use
	*@param ss : the spell's index
	*@param enm : a character to attack
	*/
	public String attack(int ss,Char enm) //spells
	{
	
	String dis=spells.addTurn();
	
	if(dis.indexOf("died")!=-1||dis.indexOf("dying")!=-1)
	return dis;
	
	String pl=spells.canPlay();
	if(!pl.equals(""))
	{
		if(this instanceof Character)
		{
		Character me=(Character)(this);	
		me.attacks(me.NoA());
		}
	return name+" cannot make a move because he is affetced by "+pl+"."+dis;
	}
		
	String s=spells.useSpell(ss,enm);
			
	return s+dis;
	}
	
	
	
	public static boolean npc(Char c)
	{
		if(c instanceof Character)
		return false;
		
		return true;
	}
	
	public void dealDam(int a)
	{
	dam+=a;
	}
	
	
	public void heal(int a)
	{
	dam-=a;
	if(dam<0)
	dam=0;
	}
	
	
	public boolean isDead()
	{
	if(dam>=hp)
	return true;
	
	return false;		
	}
	
	public int hp()
	{
	return hp;	
	}
	
	
	public String info()
	{
	String s="",at="";	
	if(attacked())
	at=", fighting.";
	
	String dis=" ["+spells.string()+"]";
	
	s="HP: "+(hp-dam)+"/"+hp+", AC: "+AC()+", Weapon: "+weapon.getName()+", Attack: "+attack()+",  Damage: "+weapon.getDamage().getString(weapon.damBonus())+""+at+dis;
	return s;
	}
	
	
	public boolean spellFailure() //failure if true
	{
	int per=0;
	if(shield!=null)
	per+=shield.getSf();
	if(armor!=null)
	per+=armor.getSf();
	
	if(Dice.roll(100)>per)
	return false;			
	else
	return true;
	}
		
	
	/**
	*Trying to spot a bunch of people
	*@param ch : the people to spot
	*/
	public void spot(Vector ch) //chars
	{
		for(int i=0;i<ch.size();i++)
		{
		Char c=(Char)(ch.elementAt(i));
		
		////System.out.println("foor ");
		if(c!=this)
		if(seen.indexOf(c)==-1)  //if not exists
		{
			if((this instanceof Enemy)||(this instanceof Character))
			{
				if(this instanceof Enemy)
				if(spot(c))
				seen.add(c);
				
				if(this instanceof Character)
				if(((Character)(this)).getSkills().check("Spot",c))
				seen.add(c);
			}
			else
				if(spot(c))
				seen.add(c);
		}//if exists
		//else
		//System.out.println("exists in seen");
		
		}
	}
	
	
	public boolean spot(Char c) //another change and spot(bonus,char); or instanceof etc.
	{
	int d=Dice.roll(20)+wis(),dc=0;
		
		if(c instanceof Enemy)
		dc=((Enemy)(c)).getHiding();
		else
			if(c instanceof Character)
			dc=((Character)(c)).getHiding();
			
	if(dc==0)
	dc=Dice.roll(10)+Dice.roll(5);
	
	
	////System.out.println("this: "+this+"    ,d: "+d+" dc: "+dc);
	
	if(d>=dc)
	return true;		
	
	return false;	
	}
	
	public boolean seen(Char c)
	{
	for(int i=0;i<seen.size();i++)
	if((Char)(seen.elementAt(i))==c)
	return true;
	
	//if(c instanceof Character)
	////System.out.println("false.");
	
	return false;	
	}
	
	public Vector getSeen()
	{
	return seen;	
	}
	
	public void addSeen(Char c)
	{
	if(seen.indexOf(c)==-1)
	seen.add(c);
	}
	
	public int spot()
	{
	int t=wis();
	
	if(this instanceof Enemy)
	t=((Enemy)(this)).getSpot();
	if(this instanceof Character)
	t=((Character)(this)).getSkills().spot();
	
	return Dice.roll(20)+t;
	}
	
	
	public void setPlace(Place p)
	{
		place=p;
	}
		
	public int serial(Char c)
	{
		Vector v=place.getChars();
		for(int i=0;i<v.size();i++)
		if(v.elementAt(i)==c)
		return i;
	
	return -1;
	}
	
	public Char bySerial(int i)
	{
		Vector v=place.getChars();
		if(i<v.size()&&i>=0)
		return (Char)(v.elementAt(i));
		
		return null;
	}
	
	
	public void createSeen(Vector s)
	{
	seen=new Vector();
	for(int i=0;i<s.size();i++)
	{
	int index=Integer.parseInt((String)(s.elementAt(i)));
	Char c=bySerial(index);
	seen.add(c);
	}
	}
	
	/**
	*Connverts a char to a String (coded)
	*/
	public String write()
	{
	String s="C*";
	s+=dam+",";
	s+=name+",";
	s+=hp+",";
	s+=str+",";
	s+=dex+",";
	s+=race+",";
	s+=male+",";
	s+=con+",";
	s+=wis+",";
	s+=inl+",";
	s+=cha+",";
	s+=damr+",";
	s+=nata+",";
	s+=sr+",";
	s+=gp+",";
	s+=weapon.write()+",";
	if(armor!=null)	
	s+=armor.write()+",";
	if(shield!=null)
	s+=shield.write()+",";
	
	
	////System.out.println("items: "+items);
	s+=";";
	for(int i=0;i<items.size();i++)
	{
	Item m=(Item)(items.elementAt(i));
	if(m!=weapon&&m!=armor&&m!=shield)
	s+=m.write();
	}
	s+=";";
	
	s+=ref+",";
	s+=will+",";
	s+=fort+",";	
	
	s+=spells.write()+",";
	s+=attacked+",";
	
	//seen. -references. (serial index in Place)
	s+=seen.size()+",";
	for(int i=0;i<seen.size();i++)
	s+=serial((Char)(seen.elementAt(i)))+",";
	
	
	return s+"*C";
	}
	
			
	/**
	*Returns a char by a coded String
	*@param s : the coded String
	*@param plc : the place it is in
	*@param seens : the characters it has seen
	*/
	public static Char read(String s,Place plc,Vector seens) //careful of "C*C"
	{
	Char d=new Char();
	d.place=plc;
	s=s.substring(2);
	
	int b=s.indexOf(",");
	d.dam=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);	
	
	b=s.indexOf(",");
	d.name=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.hp=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.str=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.dex=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.race=s.substring(0,b);
	s=s.substring(b+1);
	b=s.indexOf(",");
	
	
	////System.out.println("boolean: -"+s.substring(0,b)+"-");
	//if("true".equals("true"))
	////System.out.println("damn: sd");
	
	//d.male=Boolean.getBoolean(s.substring(0,b));
	
	
	if((s.substring(0,b)).equals("true"))
	d.male=true;
	else
	d.male=false;
	
	
	////System.out.println("male: "+d.male);
	
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.con=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.wis=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.inl=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.cha=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.damr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.nata=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.sr=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.gp=Double.parseDouble(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*W"); //have to be.
	d.weapon=Weapon.readW(s.substring(0,b+1));
	s=s.substring(b+3);
	
	if(!d.weapon.getName().equals("Unarmed Strike"))
	d.items.add(d.weapon);
	
	d.weapon.setOwner(d);
	
	String th=s.substring(0,s.indexOf(";"));
	s=s.substring(s.indexOf(";")+1);
	
	////System.out.println("th: "+th);
	b=th.lastIndexOf("*A");
	if(b!=-1)
	{
	d.armor=Armor.readA(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.armor);
	d.armor.setOwner(d);
	}
	
	b=th.lastIndexOf("*S");
	if(b!=-1)
	{
	d.shield=Shield.readS(th.substring(0,b+1));
	th=th.substring(b+3);
	d.items.add(d.shield);
	d.shield.setOwner(d);
	}
	
	
	
	b=s.indexOf(";");
	////System.out.println("BI: "+b+" , "+s);
	String i=s.substring(0,b);
	//items:
	while(i.indexOf("*")!=-1)
	{
	Item t=null;	
	int c=0;
	
	if(i.indexOf("I")==0)
	{
		c=i.indexOf("*I");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*I")+2;	
		}
		
		t=Item.read(i.substring(0,c+1));
	}
	
	if(i.indexOf("W")==0)
	{	
		c=i.indexOf("*W");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*W")+2;	
		}
		t=Weapon.readW(i.substring(0,c+1));
		((Weapon)t).setOwner(d);
	}
	
	if(i.indexOf("A")==0)
	{
		c=i.indexOf("*A");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*A")+2;	
		}
		t=Armor.readA(i.substring(0,c+1));	
		((Armor)t).setOwner(d);
	}
	
	if(i.indexOf("S")==0)
	{
		c=i.indexOf("*S");
		if(c==1)
		{
		String de=i.substring(2);
		c=de.indexOf("*S")+2;	
		}
		t=Shield.readS(i.substring(0,c+1));
		
		((Shield)t).setOwner(d);
	}
	
	
	d.items.add(t);
	i=i.substring(c+2);
	}
	//end items
	
	s=s.substring(b+1);
	
	
	b=s.indexOf(",");
	d.ref=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.will=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	b=s.indexOf(",");
	d.fort=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	b=s.indexOf("*L");
	d.spells=Spells.read(s.substring(0,b));
	s=s.substring(b+3);
	d.spells.setOwner(d);
	
	b=s.indexOf(",");
	//d.attacked=Boolean.getBoolean(s.substring(0,b));
	if((s.substring(0,b)).equals("true"))
	d.attacked=true;
	else
	d.attacked=false;
	s=s.substring(b+1);
	
	
	//seen!
	b=s.indexOf(",");
	int size=Integer.parseInt(s.substring(0,b));
	s=s.substring(b+1);
	
	
	Vector sen=new Vector();
	for(int j=0;j<size;j++)
	{
	b=s.indexOf(",");
	int index=Integer.parseInt(s.substring(0,b));
	sen.add(index+"");
	//Char c=d.bySerial(index);
	//d.seen.add(c);
	s=s.substring(b+1);
	}
	seens.add(sen);
	//end seen
	
	return d;
	}
	
	
		
	public String toString()
	{
	String m="";
	if(male)
	m="male";
	else
	m="female";
	return name+"   {"+race+" , "+m+"}";
	}
	
}
