﻿using FolderSyncronizer.Core.Watchers;
using FolderSyncronizer.Debugging;
using System;
using System.Collections.Concurrent;

namespace FolderSyncronizer.Core.Data
{
    public class ModificationsContainer
    {
        private readonly ConcurrentQueue<IFileAction> _actionList;

        public ModificationsContainer()
        {
            _actionList = new ConcurrentQueue<IFileAction>();
        }


        public void Add(IFileAction action)
        {
            foreach (var existingAction in _actionList)
            {
                existingAction.OnNewAction(action);
            }
            _actionList.Enqueue(action);
        }

        /// <summary>
        /// Throws exception if next action fails.
        /// If action list is empty, returns false.
        /// </summary>
        public bool RunNextAction(RunActionOptions actionOptions = null)
        {
            if (_actionList.TryPeek(out IFileAction action))
            {
                if (actionOptions?.ShouldSkipAction?.Invoke(action) == true)
                {
                    Singleton.Logger.LogDebug($"Skippig action {action.Description}: Filtered");
                }
                else
                {
                    ApplyAction(action);
                }

                // Remove only after success:
                _actionList.TryDequeue(out var ignore);
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void ApplyAction(IFileAction action)
        {
            try
            {
                Singleton.Logger.LogDebug($"Applying action {action.Description}");
                action.Apply();
            }
            catch (Exception e)
            {
                throw new OperationCanceledException($"Failed running action ({action.Description}): {e.Message}");
            }
        }

        public void Clear()
        {
            _actionList.Clear();
        }
    }

    public class RunActionOptions
    {
        public Predicate<IFileAction> ShouldSkipAction { get; set; }
    }
}
