const roller = require('../roller.js');
const {TestCase, }= require('./common.js');


module.exports = {
  TestCases: createTestCases(),
};


function createTestCases() {
  return [
    new TestCase({
      name: 'testRollerStepUp_existingNumberOnce_resultOk ',
      functionToRun: testRollerStepUp,
      inputArgs: {
        dice: [6, 8, 8],
        stepUp: 6, 
        stepUpMod: 1, 
      },
      expectedResult: {newDiceRoll: [8, 8, 8], nonPoolDice: [], messageContent : ['Modified roll die: D6 -> D8']},
    }),
    new TestCase({
      name: 'testRollerStepUp_numberDoesntExist_missingMessage ',
      functionToRun: testRollerStepUp,
      inputArgs: {
        dice: [6, 8, 8],
        stepUp: 4, 
        stepUpMod: 1, 
      },
      expectedResult: {newDiceRoll: [6, 8, 8], nonPoolDice: [], messageContent : ['No step up/down found for D4: did not step up/down']},
    }),
    new TestCase({
      name: 'testRollerStepUp_existingNumber3Times_resultOk ',
      functionToRun: testRollerStepUp,
      inputArgs: {
        dice: [6, 8, 8],
        stepUp: 6, 
        stepUpMod: 3, 
      },
      expectedResult: {newDiceRoll: [12, 8, 8], nonPoolDice: [], messageContent : ['Modified roll die: D6 -> D12']},
    }),
    new TestCase({
      name: 'testRollerStepUp_existingNumberDecreaseTwice_resultOk ',
      functionToRun: testRollerStepUp,
      inputArgs: {
        dice: [6, 8, 8],
        stepUp: 8, 
        stepUpMod: -2, 
      },
      expectedResult: {newDiceRoll: [6, 4, 8], nonPoolDice: [], messageContent : ['Modified roll die: D8 -> D4']},
    }),
    new TestCase({
      name: 'testRollerStepUp_existing4DecreaseOnce_staysSameWithMessage ',
      functionToRun: testRollerStepUp,
      inputArgs: {
        dice: [4, 8, 8],
        stepUp: 4, 
        stepUpMod: -1, 
      },
      expectedResult: {newDiceRoll: [4, 8, 8], nonPoolDice: [], messageContent : []},
    }),
    new TestCase({
      // Can't step up more than one beyond D4.
      name: 'testRollerStepUp_existing12IncreaseOnce_staysSameWithMessage',
      functionToRun: testRollerStepUp,
      inputArgs: {
        dice: [4, 8, 12],
        stepUp: 12, 
        stepUpMod: 2, 
      },
      expectedResult: {newDiceRoll: [4, 8, 12], nonPoolDice: [4], messageContent : ['Modified roll die: D12 -> D12+D4']},
    }),    
    new TestCase({
      // Can step up beyond D4 when settings is right.
      name: 'testRollerStepUp_existing12IncreaseOnce_increaseAccordingly',
      functionToRun: testRollerStepUp,
      inputArgs: {
        dice: [4, 8, 12],
        stepUp: 12, 
        stepUpMod: 2, 
        extraRollDieIncreaseModeOn: true, 
      },
      expectedResult: {newDiceRoll: [4, 8, 12], nonPoolDice: [6], messageContent : ['Modified roll die: D12 -> D12+D6']},
    }),    
    
    new TestCase({
      name: 'testRollerStepUp_stepUpNotFound_notFound',
      functionToRun: testRollerStepUp,
      inputArgs: {
        dice: [4, 8, 12],
        stepUp: 6, 
        stepUpMod: 2, 
      },
      expectedResult: {newDiceRoll: [4, 8, 12], nonPoolDice: [], messageContent : ['No step up/down found for D6: did not step up/down']},
    }),  
  ]
  
  

}




/*
input: {
  dice - list of numbers,
  stepUp , 
  stepUpMod 
  extraRollDieIncreaseModeOn, 
}

Output: newDiceRoll, messageContent - empty if no step up}
*/
async function testRollerStepUp (input) {  
  let iocContainer = {
    options: {
      extraRollDieIncreaseModeOn: input.extraRollDieIncreaseModeOn ?? false,
    }
  }

  return await roller.StepUp(input.dice, input.stepUp, input.stepUpMod, iocContainer)
}
